/* sui_defines.h
 *
 * SXML automatic SUITK definitions
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#include "sui_defines.h"

#include "ul_log.h"

UL_LOG_CUST(ulogd_suixml)

SUI_CANBE_CONST
suiuser_define_table_t user_suitk_defines[] = {
/* NULL */
  {"NULL", SU_VALNUM(0)},

/* DINFO types */
  SU_DEF_ONE_ITEM(SUI_TYPE_UNKNOWN),
  SU_DEF_ONE_ITEM(SUI_TYPE_CHAR),
  SU_DEF_ONE_ITEM(SUI_TYPE_UCHAR),
  SU_DEF_ONE_ITEM(SUI_TYPE_SHORT),
  SU_DEF_ONE_ITEM(SUI_TYPE_USHORT),
  SU_DEF_ONE_ITEM(SUI_TYPE_INT),
  SU_DEF_ONE_ITEM(SUI_TYPE_UINT),
  SU_DEF_ONE_ITEM(SUI_TYPE_LONG),
  SU_DEF_ONE_ITEM(SUI_TYPE_ULONG),
  SU_DEF_ONE_ITEM(SUI_TYPE_LLONG),
  SU_DEF_ONE_ITEM(SUI_TYPE_ULLONG),
  SU_DEF_ONE_ITEM(SUI_TYPE_FIXED),
  SU_DEF_ONE_ITEM(SUI_TYPE_UFIXED),
  SU_DEF_ONE_ITEM(SUI_TYPE_UTF8),

/* FINFO flags */
  SU_DEF_ONE_ITEM(SUFI_TIME),
  SU_DEF_ONE_ITEM(SUFI_TEXT),
  SU_DEF_ONE_ITEM(SUFI_PREFIX),
  SU_DEF_ONE_ITEM(SUFI_SUFFIX),
  SU_DEF_ONE_ITEM(SUFI_DECIMALZEROS),
  SU_DEF_ONE_ITEM(SUFI_SMALLLETTERS),
  SU_DEF_ONE_ITEM(SUFI_CHECKLIMITS),
  SU_DEF_ONE_ITEM(SUFI_RELATIVETIME),
  SU_DEF_ONE_ITEM(SUFI_HMSONLYTIME),
  SU_DEF_ONE_ITEM(SUFI_TZAPPLY2TIME),
  SU_DEF_ONE_ITEM(SUFI_TIME_LOCAL),
  SU_DEF_ONE_ITEM(SUFI_TIME_UTC),
  SU_DEF_ONE_ITEM(SUFI_TIME_INTERVAL),
  SU_DEF_ONE_ITEM(SUFI_TIME_HMS),

/* Colortable indexes */

/* Image */
  SU_DEF_ONE_ITEM(SUIM_BITMAP_1BIT),
  SU_DEF_ONE_ITEM(SUIM_BITMAP_2BIT),
  SU_DEF_ONE_ITEM(SUIM_BITMAP_4BIT),
  SU_DEF_ONE_ITEM(SUIM_BITMAP_8BIT),
  SU_DEF_ONE_ITEM(SUIM_TRANSPARENT),
  SU_DEF_ONE_ITEM(SUIM_DYNAMIC_DATA),

/* ALIGN flags */
  SU_DEF_ONE_ITEM(SUAL_CENTER),
  SU_DEF_ONE_ITEM(SUAL_TOP),
  SU_DEF_ONE_ITEM(SUAL_BOTTOM),
  SU_DEF_ONE_ITEM(SUAL_BASELINE),
  SU_DEF_ONE_ITEM(SUAL_LEFT),
  SU_DEF_ONE_ITEM(SUAL_RIGHT),
  SU_DEF_ONE_ITEM(SUAL_BLOCK),
  SU_DEF_ONE_ITEM(SUAL_INSIDE),
  SU_DEF_ONE_ITEM(SUAL_COLUMN),
  SU_DEF_ONE_ITEM(SUAL_CLIP),
  SU_DEF_ONE_ITEM(SUAL_WRAP),

  /* STYLE flags */
  SU_DEF_ONE_ITEM(SUSFL_FRAME),
  SU_DEF_ONE_ITEM(SUSFL_ROUNDEDFRAME),
  SU_DEF_ONE_ITEM(SUSFL_SHADOW),
  SU_DEF_ONE_ITEM(SUSFL_SEEMS3D),
  SU_DEF_ONE_ITEM(SUSFL_TRANSPARENT),
  SU_DEF_ONE_ITEM(SUSFL_LABELHASFRAME),
  SU_DEF_ONE_ITEM(SUSFL_FOCUSLABEL),
  SU_DEF_ONE_ITEM(SUSFL_FOCUSFRAME),
  SU_DEF_ONE_ITEM(SUSFL_TITLE),
  SU_DEF_ONE_ITEM(SUSFL_LABELHASBACK),
  SU_DEF_ONE_ITEM(SUSFL_BLINKEDITED),

/* Widget flags */
  SU_DEF_ONE_ITEM(SUFL_NORMAL),
  SU_DEF_ONE_ITEM(SUFL_HIGHLIGHTED),
  SU_DEF_ONE_ITEM(SUFL_SHADY),
  SU_DEF_ONE_ITEM(SUFL_INACTIVE),
  SU_DEF_ONE_ITEM(SUFL_ACTIVE),
  SU_DEF_ONE_ITEM(SUFL_SELECTED),
  SU_DEF_ONE_ITEM(SUFL_DISABLED),
  SU_DEF_ONE_ITEM(SUFL_FOCUSEN),
  SU_DEF_ONE_ITEM(SUFL_FOCUSED),
  SU_DEF_ONE_ITEM(SUFL_EDITEN),
  SU_DEF_ONE_ITEM(SUFL_EDITED),
  SU_DEF_ONE_ITEM(SUFL_INVISIBLE),
  SU_DEF_ONE_ITEM(SUFL_AUTORESIZE),
  SU_DEF_ONE_ITEM(SUFL_EDITAUTO),
  SU_DEF_ONE_ITEM(SUFL_BOTTOMACTIVE),
//  SU_DEF_ONE_ITEM(SUFL_LABEL),
  SU_DEF_ONE_ITEM(SUFL_WIDGETISDOWN),
  SU_DEF_ONE_ITEM(SUFL_BLINKFULL),
  SU_DEF_ONE_ITEM(SUFL_BLINKCONTENT),

/* Widget - BITMAP flags */
  SU_DEF_ONE_ITEM(SUIF_MASKED),
  SU_DEF_ONE_ITEM(SUIF_INV_MASK),
  SU_DEF_ONE_ITEM(SUIF_INV_IMAGE),
  SU_DEF_ONE_ITEM(SUIF_MIRROR),
  SU_DEF_ONE_ITEM(SUIF_ROTATE_0),
  SU_DEF_ONE_ITEM(SUIF_ROTATE_90),
  SU_DEF_ONE_ITEM(SUIF_ROTATE_180),
  SU_DEF_ONE_ITEM(SUIF_ROTATE_270),
  SU_DEF_ONE_ITEM(SUIF_TRANSPARENT),
  SU_DEF_ONE_ITEM(SUIF_MULTIIMAGE),

/* Widget - BUTTON flags */
  SU_DEF_ONE_ITEM(SUBF_PUSH_BTN),
  SU_DEF_ONE_ITEM(SUBF_LIGHT_BTN),
  SU_DEF_ONE_ITEM(SUBF_RADIO_BTN),
  SU_DEF_ONE_ITEM(SUBF_CHECK_BTN),
  SU_DEF_ONE_ITEM(SUBF_REPEAT),
  SU_DEF_ONE_ITEM(SUBF_PUSHED),
  SU_DEF_ONE_ITEM(SUBF_CHECKED),
  SU_DEF_ONE_ITEM(SUBF_GROUPED),
  SU_DEF_ONE_ITEM(SUBF_USEIMAGE),
  SU_DEF_ONE_ITEM(SUBF_USEVALUE),
  SU_DEF_ONE_ITEM(SUBF_FOLLOWVALUE),

/* Widget - GROUP flags */
  SU_DEF_ONE_ITEM(SUGF_LAYOUT_NONE),
  SU_DEF_ONE_ITEM(SUGF_LAYOUT_BORDER),
  SU_DEF_ONE_ITEM(SUGF_LAYOUT_CARD),
  SU_DEF_ONE_ITEM(SUGF_NOTIFY),
  SU_DEF_ONE_ITEM(SUGF_CANBEFOCUSED), /* SUGF_EMPTYISFOCUSEN */
  SU_DEF_ONE_ITEM(SUGF_FOCUSONLYINSIDE),

/* Widget - LISTBOX flags */
  SU_DEF_ONE_ITEM(SULF_NOSELECT),
  SU_DEF_ONE_ITEM(SULF_SELECT),
  SU_DEF_ONE_ITEM(SULF_SINGLE),
  SU_DEF_ONE_ITEM(SULF_SINGLE_FRAME),
  SU_DEF_ONE_ITEM(SULF_MULTI),
  SU_DEF_ONE_ITEM(SULF_LOOP),
  SU_DEF_ONE_ITEM(SULF_REVERSEKEYS),
  SU_DEF_ONE_ITEM(SULF_SELECT_ARRAY),
  SU_DEF_ONE_ITEM(SULF_SHOW_HIDDEN),
  SU_DEF_ONE_ITEM(SULF_LISTFRAME),
  SU_DEF_ONE_ITEM(SULF_SORTED),
  SU_DEF_ONE_ITEM(SULF_CHANGE_FOCUS),
  SU_DEF_ONE_ITEM(SULF_WITH_ICON),
  SU_DEF_ONE_ITEM(SULF_FILTERING),
  SU_DEF_ONE_ITEM(SULF_MOBILKBD),

  SU_DEF_ONE_ITEM(SULI_USER_FLAG), /* for filtering according to a user flag */
  SU_DEF_ONE_ITEM(SULF_PAT_FLAGS),
  SU_DEF_ONE_ITEM(SULF_PAT_USERIDX),
  SU_DEF_ONE_ITEM(SULF_PAT_IMGIDX),


/* Widget - NUMBER flags */
  SU_DEF_ONE_ITEM(SUNF_CLEARNOEDITEN),
  SU_DEF_ONE_ITEM(SUNF_ARROWKEY),
  SU_DEF_ONE_ITEM(SUNF_SUFFIX),
  SU_DEF_ONE_ITEM(SUNF_HIDEPOINT),
  SU_DEF_ONE_ITEM(SUNF_DECIMALZEROS),
  SU_DEF_ONE_ITEM(SUNF_FILLUPBYZERO),
  SU_DEF_ONE_ITEM(SUNF_SHOWLIMITS),
  SU_DEF_ONE_ITEM(SUNF_CONFIRMARROW),
  SU_DEF_ONE_ITEM(SUNF_CONFIRMEDIT),
  SU_DEF_ONE_ITEM(SUNF_ONLINEEDIT),
  SU_DEF_ONE_ITEM(SUNF_CHECKLIMITS),
  SU_DEF_ONE_ITEM(SUNF_MUSTBEVALID),

/* Widget - SCROLL flags */
  SU_DEF_ONE_ITEM(SUSF_VERTICAL),
  SU_DEF_ONE_ITEM(SUSF_HORIZONTAL),
  SU_DEF_ONE_ITEM(SUSF_MODE_AUTOSIZE),
  SU_DEF_ONE_ITEM(SUSF_MODE_FIXEDSIZE),
  SU_DEF_ONE_ITEM(SUSF_MODE_PROGRESS),
//  SU_DEF_ONE_ITEM(SUSF_PARENT_SB),
  SU_DEF_ONE_ITEM(SUSF_SHOWLINE),
  SU_DEF_ONE_ITEM(SUSF_NOFILLLINE),
  SU_DEF_ONE_ITEM(SUSF_LEFTTICK),
  SU_DEF_ONE_ITEM(SUSF_TOPTICK),
  SU_DEF_ONE_ITEM(SUSF_RIGHTTICK),
  SU_DEF_ONE_ITEM(SUSF_BOTTOMTICK),
  SU_DEF_ONE_ITEM(SUSF_BOTHTICKS),
  SU_DEF_ONE_ITEM(SUSF_HIDEBUTTONS),
  SU_DEF_ONE_ITEM(SUSF_REVERSE),
  SU_DEF_ONE_ITEM(SUSF_NOTHUMBLINE),

/* Widget - TEXT flags */
  SU_DEF_ONE_ITEM(SUTF_SINGLELINE),
  SU_DEF_ONE_ITEM(SUTF_MULTILINE),
  SU_DEF_ONE_ITEM(SUTF_AUTOMULTI),
  SU_DEF_ONE_ITEM(SUTF_CURSOR),
  SU_DEF_ONE_ITEM(SUTF_PASSWORD),
  SU_DEF_ONE_ITEM(SUTF_UPPERCASE),
  SU_DEF_ONE_ITEM(SUTF_LOWERCASE),
  SU_DEF_ONE_ITEM(SUTF_CONFIRMEDIT),
  SU_DEF_ONE_ITEM(SUTF_ONLINEEDIT),
  SU_DEF_ONE_ITEM(SUTF_MBKBDHELP),
  SU_DEF_ONE_ITEM(SUTF_MOBILKBD_C),
  SU_DEF_ONE_ITEM(SUTF_MOBILKBD_S),
  SU_DEF_ONE_ITEM(SUTF_MOBILKBD_N),

/* Widget - TIME flags */
  SU_DEF_ONE_ITEM(SUDF_99HOURS),
  SU_DEF_ONE_ITEM(SUDF_ONLINEEDIT),
  SU_DEF_ONE_ITEM(SUDF_CHECKLIMITS),
  SU_DEF_ONE_ITEM(SUDF_MUSTBEVALID),
  SU_DEF_ONE_ITEM(SUDF_LOCALTIME),

/* Widget - TABLE flags */
  SU_DEF_ONE_ITEM(SUAF_SELECT_ONE),
  SU_DEF_ONE_ITEM(SUAF_SELECT_ROW),
  SU_DEF_ONE_ITEM(SUAF_SELECT_COL),
  SU_DEF_ONE_ITEM(SUAF_VIEW_SELECTED),
  SU_DEF_ONE_ITEM(SUAF_DIV_ROWS),
  SU_DEF_ONE_ITEM(SUAF_DIV_COLS),
  SU_DEF_ONE_ITEM(SUAF_DIV_GRID),
  SU_DEF_ONE_ITEM(SUAF_TITLES),
  SU_DEF_ONE_ITEM(SUAF_ROTATED),
  SU_DEF_ONE_ITEM(SUAF_AUTOINDEX),
  SU_DEF_ONE_ITEM(SUAF_HIDEEMPTY),
  SU_DEF_ONE_ITEM(SUAF_CHANGE_FOCUS),
  SU_DEF_ONE_ITEM(SUAF_AUTO_DRANGE),

/* Widget - DYNTEXT flags */
  SU_DEF_ONE_ITEM(SUYF_FNC_HIDDEN),
  SU_DEF_ONE_ITEM(SUYF_FNC_STATIC),
  SU_DEF_ONE_ITEM(SUYF_FNC_MULTILN),
  SU_DEF_ONE_ITEM(SUYF_FNC_DYNTEXT),
  SU_DEF_ONE_ITEM(SUYF_NOCHANGE),
  SU_DEF_ONE_ITEM(SUYF_ROTATE),
  SU_DEF_ONE_ITEM(SUYF_NOSWITCH),

/* Widget - GRAPHXY flags */
  SU_DEF_ONE_ITEM(SUXF_AXIS_CHNG),
  SU_DEF_ONE_ITEM(SUXF_AXIS_XREV),
  SU_DEF_ONE_ITEM(SUXF_AXIS_YREV),
  SU_DEF_ONE_ITEM(SUXF_VAR_INDICES),
  SU_DEF_ONE_ITEM(SUXF_RX_AUTO),
  SU_DEF_ONE_ITEM(SUXF_RX_FIXABS),
  SU_DEF_ONE_ITEM(SUXF_RX_FIXRELO),
  SU_DEF_ONE_ITEM(SUXF_RX_FIXRELC),
  SU_DEF_ONE_ITEM(SUXF_RY_AUTO),
  SU_DEF_ONE_ITEM(SUXF_RY_FIXABS),
  SU_DEF_ONE_ITEM(SUXF_RY_FIXRELO),
  SU_DEF_ONE_ITEM(SUXF_RY_FIXRELC),
  SU_DEF_ONE_ITEM(SUXF_DRAW_AXES),
  SU_DEF_ONE_ITEM(SUXF_DRAW_CURPOS),
  SU_DEF_ONE_ITEM(SUXF_KEYS_CHOFS),

  /* Graph data-set flags */
  SU_DEF_ONE_ITEM(SUGDS_AXIS_X),
  SU_DEF_ONE_ITEM(SUGDS_AXIS_Y),
  SU_DEF_ONE_ITEM(SUGDS_XYDATA),
  SU_DEF_ONE_ITEM(SUGDS_TYPE_ABS),
  SU_DEF_ONE_ITEM(SUGDS_TYPE_INC),
  SU_DEF_ONE_ITEM(SUGDS_TYPE_DIF),
  SU_DEF_ONE_ITEM(SUGDS_USE_COLOR),
  SU_DEF_ONE_ITEM(SUGDS_SHOW_VALUES),
  SU_DEF_ONE_ITEM(SUGDS_SHOW_AXIS),
  SU_DEF_ONE_ITEM(SUGDS_SHOW_AXVALS),
  SU_DEF_ONE_ITEM(SUGDS_DRAW_NONE),
  SU_DEF_ONE_ITEM(SUGDS_DRAW_POINT),
  SU_DEF_ONE_ITEM(SUGDS_DRAW_LINE),
  SU_DEF_ONE_ITEM(SUGDS_DRAW_DOTLINE),
  SU_DEF_ONE_ITEM(SUGDS_DRAW_STEP),
  SU_DEF_ONE_ITEM(SUGDS_DRAW_BOX),
  SU_DEF_ONE_ITEM(SUGDS_DRAW_OUTLINE),
  SU_DEF_ONE_ITEM(SUGDS_DRAW_OLFX),
  SU_DEF_ONE_ITEM(SUGDS_DRAW_OLFY),
  SU_DEF_ONE_ITEM(SUGDS_DRAW_PIE),
  SU_DEF_ONE_ITEM(SUGDS_CONTINUE),


/**************************************/
/* Events */
  SU_DEF_ONE_ITEM(SUEV_MDOWN),
  SU_DEF_ONE_ITEM(SUEV_MUP),
  SU_DEF_ONE_ITEM(SUEV_MMOVE),
  SU_DEF_ONE_ITEM(SUEV_MAUTO),
  SU_DEF_ONE_ITEM(SUEV_KDOWN),
  SU_DEF_ONE_ITEM(SUEV_KUP),
  SU_DEF_ONE_ITEM(SUEV_DRAW),
  SU_DEF_ONE_ITEM(SUEV_REDRAW),
  SU_DEF_ONE_ITEM(SUEV_COMMAND),
  SU_DEF_ONE_ITEM(SUEV_BROADCAST),
  SU_DEF_ONE_ITEM(SUEV_SIGNAL),
  SU_DEF_ONE_ITEM(SUEV_GLOBAL),
  SU_DEF_ONE_ITEM(SUEV_FREE),
  SU_DEF_ONE_ITEM(SUEV_NOTHING),

/* Event - commands */
/* TODO: add all commands ... */
  SU_DEF_ONE_ITEM(SUCM_NEXT),
  SU_DEF_ONE_ITEM(SUCM_PREV),
  SU_DEF_ONE_ITEM(SUCM_CLOSE),
  SU_DEF_ONE_ITEM(SUCM_OK),
  SU_DEF_ONE_ITEM(SUCM_CANCEL),
  SU_DEF_ONE_ITEM(SUCM_CONFIRM),
  SU_DEF_ONE_ITEM(SUCM_DISCARD),
  SU_DEF_ONE_ITEM(SUCM_PUSHED),
  SU_DEF_ONE_ITEM(SUCM_NEXT_GROUP),
  SU_DEF_ONE_ITEM(SUCM_PREV_GROUP),
  SU_DEF_ONE_ITEM(SUCM_CHANGE),
  SU_DEF_ONE_ITEM(SUCM_CHANGED),
  SU_DEF_ONE_ITEM(SUCM_STARTEDIT),
  SU_DEF_ONE_ITEM(SUCM_DEADKEY),
  SU_DEF_ONE_ITEM(SUCM_EXTINPUT),
  SU_DEF_ONE_ITEM(SUCM_SETSTATE),

/**************************************/
/* Screen flags */

/* Condition flags */

/**************************************/
/* Suitk print layouts and filters */
  SU_DEF_ONE_ITEM(SUI_PRN_END_NONE),
  SU_DEF_ONE_ITEM(SUI_PRN_END_CR),
  SU_DEF_ONE_ITEM(SUI_PRN_END_LF),
  SU_DEF_ONE_ITEM(SUI_PRN_END_CRLF),

/* end of table */
  {NULL, {0,{0}}}
};

/**
 * suiuser_register_defines_from_table - Add definitions from specified user table to the global namespace
 * @tab: Pointer to a user define table.
 *
 * The function adds all named values from the TAB table to the global namespace (DEFINE subnamespace).
 *
 * Return Value: The function does not return any value.
 *
 * File: sui_defines.c
 */
void suiuser_register_defines_from_table(suiuser_define_table_t *tab)
{
  ns_subns_t *psns;
  psns = ns_find_subnamespace_by_type(&ns_global_namespace, SUI_TYPE_DEFINE);
  if (!psns) {    /* create subns*/
    psns = ns_create_subnamespace(SUI_TYPE_DEFINE);
    if (!ns_add_subnamespace(&ns_global_namespace, psns)) {
      ul_logerr("create define subnamespace (suiuser)\n");
      return;
    }
  }

  while(tab->name) {
    ns_object_t *obj = ns_find_object_by_name(psns, tab->name);
    if (obj) {
      ul_logerr("added object '%s' is already in namespace\n", tab->name);
      break;
    }
    obj = ns_create_object(tab->name, &tab->sxmlvalue,
                            SUI_TYPE_DEFINE, NSOF_STATIC_ALL);
    if (!ns_add_object(psns, obj)) {
      ul_logerr("add common SUITK defines\n");
      break;
    }
    tab++;
  }
}

/**
 * suiuser_register_all_suitk_defines - Add SUITK's constants to the global namespace
 *
 * The function adds all SUITK's constants with name to the global namespace.
 *
 * Return Value: The function does not return any value.
 *
 * File: sui_defines.c
 */
inline
void suiuser_register_all_suitk_defines(void)
{
  suiuser_register_defines_from_table(user_suitk_defines);
}
