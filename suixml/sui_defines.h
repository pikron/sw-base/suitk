/* sui_defines.h
 *
 * SXML automatic SUITK definitions
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#ifndef _SXML_DEFINES_HEADER_
#define _SXML_DEFINES_HEADER_

#include <suitk/suitk.h>
#include <suixml/sui_sxml.h>

/**
 * struct suiuser_define_table - Description of named sxml value for adding to a namespace
 * @name: Name of a sxml value.
 * @sxmlvalue: Structure with a sxml value (value and its type).
 *
 * File: sui_defines.h
 */
typedef struct suiuser_define_table {
    char *name;
    sui_sxml_value_t sxmlvalue;
} suiuser_define_table_t;

#define SU_VALNUM(__n__) {valtype:SUI_SXML_VALUE_NUMBER, data:{number: __n__}}
#define SU_DEF_ONE_ITEM(__item__) { #__item__ , SU_VALNUM(__item__)}
/* FIXME: patch for registering negative numbers - need change */
#define SU_NEGNUM(__n__) {valtype:SUI_SXML_VALUE_SIGNUM, data:{number: -( __n__ ) }}
#define SU_DEF_ONE_NEG_ITEM(__item__) { #__item__ , SU_NEGNUM(__item__)}

void suiuser_register_defines_from_table(suiuser_define_table_t *tab);
void suiuser_register_all_suitk_defines(void);

#endif /* _SXML_DEFINES_HEADER_ */
