/* sui_sxml.h
 *
 * SUITK parsing and processing SXML.
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#ifndef _SXML_PARSER_HEADER_
#define _SXML_PARSER_HEADER_

#include <suitk/suitk.h>
#include <suiut/sui_files.h>

ul_log_domain_t ulogd_suixml;

/******************************************************************************
 * tag information structure
 ******************************************************************************/
struct sui_sxml_doc;
struct sui_sxml_tag;
struct sui_sxml_attrib attr;
struct sui_sxml_value value;

/**
 * enum sui_sxml_known_tag_ids - Known IDs of SXML tags
 * @SUI_SKT_UNKNOWN: dummy ID for skipping index 0
 * @SUI_SKT_INCLUDE: ID of tag ('include') for inclusion another SXML file
 * @SUI_SKT_DEFINE: ID of tag ('define') for defining IDs (text string) of numbers, texts and another IDs
 * @SUI_SKT_DIV: ID of auxiliary tag ('div') for dividing SXML into groups
 * File: sui_sxml.h
 */
enum sui_sxml_known_tag_ids {
  SUI_SKT_UNKNOWN = 0,
  SUI_SKT_INCLUDE,
  SUI_SKT_DEFINE,
  SUI_SKT_DIV,
  SUI_SKT_COMMON_MAXID,
};

/**
 * struct sui_sxml_tag_info_child - Description of tag child within container SXML tag
 * @tag_id: The tag unique identifier.
 * @ptr_offset: Zero or offset to structure where is pointer to sum nested substructure.
 * @offset: Offset to memory where object is placed in structure. Object created from SXML is saved to memory addressed by offset
 *              if 'ptr_offset' is non-zero then addr=*(obj_addr+ptr_offset)+offset else addr=obj_addr+offset.
 * @objvmt: Pointer to an object description structure which describes an object represented by a tag.
 * @flags: Special tag parsing flags - skip tag, object under tag can have got subtags or can be a pointer.
 *
 * These structures are grouped into arrays end by entry with @tag_id equal
 * to %SUI_SXML_ENDOFCHILDREN and forms list of possible tags which are allowed
 * within given tag.
 *
 * File: sui_sxml.h
 */
typedef struct sui_sxml_tag_info_child {
  int          tag_id;
  long         ptr_offset; /* if it is different from zero - object is in substructure referenced by pointer */
  long         offset; /* offset + flag if child is only pointer */

/*  sui_typeid_t   type; */  /* type of object space in parent structure if it containt SUI_TYPE_PTR_FL, in parent structure is only pointer to object and no copy of object */
  const struct sui_obj_vmt *objvmt; /* vmt of child's object */

  sui_flags_t  flags;
} sui_sxml_tag_info_child_t;

/**
 * enum sui_sxml_tag_info_child_flags - Flags for tag child description
 * @SUI_SXML_ENDOFCHILDREN: Mark for tag children ending.
 * @SUI_SXML_NO_ADD_TO_TAG: Don't add child tag to the parent tag.
 * @SUI_SXML_PROCESS_FCN: Child tag must be processed by special function.
 * @SUI_SXML_CHFLG_SKIP: Tag child will be skipped.
 * @SUI_SXML_CHFLG_POINTER: Tag child contains pointer to structure.
 * @SUI_SXML_CHFLG_ARRAY: Tag child contains array.
 * @SUI_SXML_CHFLG_SUBTAGS: Tag child contains subtags (sub-structure).
 * @SUI_SXML_CHFLG_FUNCTION: Tag child contains pointer to function.
 *
 * File: sui_sxml.h
 */
enum sui_sxml_tag_info_child_flags {
  SUI_SXML_ENDOFCHILDREN = -1,
  SUI_SXML_NO_ADD_TO_TAG = -1,
  SUI_SXML_PROCESS_FCN   = -2,

  SUI_SXML_CHFLG_SKIP    = 0x0001,
  SUI_SXML_CHFLG_POINTER = 0x0002,
  SUI_SXML_CHFLG_ARRAY   = 0x0004,
  SUI_SXML_CHFLG_SUBTAGS = 0x0008, /* tag can contain subtags */
  SUI_SXML_CHFLG_FUNCTION= 0x0010,

  SUI_SXML_CHFLG_NO_OBJID= 0x0020, /* don't try find it in namespace for attribute - it is always a text */
};

/**
 * struct sui_sxml_tag_info - SXML tag information structure
 * @tag: String which represented tag (tag name).
 * @child: Pointer to an array of tag children - each record of subtag is sui_sxml_tag_info_child structure.
 *
 * File: sui_sxml.h
 */
typedef struct sui_sxml_tag_info {
  const char                      *tag;
/*  sui_typeid_t                     assigned_type; */ /* it is read from parent child structure */
/*  unsigned short                   flags; */
  sui_sxml_tag_info_child_t const *child;
} sui_sxml_tag_info_t;

#define SUI_OBJ_VMT_FROM_VMT(x) ((sui_obj_vmt_t *)(void *) x)

extern const sui_sxml_tag_info_t sui_sxml_common_known_tags[];

/******************************************************************************/
/* structures and functions for generalized SXML */
/******************************************************************************/
struct sui_sxml_doc;
typedef int sui_sxml_special_process_fcn_t(struct sui_sxml_doc *doc, struct sui_sxml_tag *tag);
typedef int sui_sxml_process_attrib_fcn_t(struct sui_sxml_doc *doc);

/**
 * struct sui_sxml_doc_description - descriptive structure of array of known tags
 * @idsize : number of tags in array
 * @idhead : ID of the head (the main) tag (e.g. SXML tag which starts each file)
 * @tags: pointer to array of defined tags
 * @spfcn: pointer to special function for tags which cannot be resolved automatically
 */
typedef struct sui_sxml_doc_description {
  int                             idsize;
  int                             idhead;
  sui_sxml_tag_info_t      const *tags;
  sui_sxml_special_process_fcn_t *spfcn;
  sui_sxml_process_attrib_fcn_t  *pafcn;
} sui_sxml_doc_description_t;


const sui_sxml_tag_info_t *sui_sxml_find_taginfo_by_name(sui_sxml_doc_description_t *ddesc, const char *tagname);
const sui_sxml_tag_info_t *sui_sxml_find_taginfo_by_id(sui_sxml_doc_description_t *ddesc, int ti_id);
sui_sxml_tag_info_child_t const *sui_sxml_find_parents_child(
                        const sui_sxml_tag_info_t *parent, const char *tagname);
sui_sxml_tag_info_t const *sui_sxml_find_childs_taginfo(
                        const sui_sxml_tag_info_t *parent, const char *tagname);

/******************************************************************************
 * sxml value structure
 ******************************************************************************/
/**
 * struct sui_sxml_value - Description of covered value in SXML (value is covered with its type)
 * @valtype: Type of a covered value.
 * @data.number: Integer number.
 * @data.string: Text string.
 * @data.ch: One character.
 *
 * File: sui_sxml.h
 */
typedef struct sui_sxml_value {
  unsigned short    valtype;
  union sxml_value_union {
    unsigned long   number;  /* integer number */
/*    float           fpnum; */   /* float-point number */
    char           *string;  /* string */
/*    void           *ptr; */    /* pointer */
    int             ch;      /* one character */
  } data;
} sui_sxml_value_t;

/**
 * enum sxml_value_types - Types of sxml covered value
 * @SUI_SXML_VALUE_NONE: No valid value.
 * @SUI_SXML_VALUE_STRING: SXML value contains text string.
 * @SUI_SXML_VALUE_ID: SXML value contains ID (text string).
 * @SUI_SXML_VALUE_NUMBER: SXML value contains unsigned integer.
 * @SUI_SXML_VALUE_SIGNUM: SXML value contains signed integer.
 * @SUI_SXML_VALUE_CHARACTER: SXML value contains one character.
 *
 * File: sui_sxml.h
 */
enum sxml_value_types {
  SUI_SXML_VALUE_NONE,
  SUI_SXML_VALUE_STRING,
  SUI_SXML_VALUE_ID,
  SUI_SXML_VALUE_NUMBER,
  SUI_SXML_VALUE_SIGNUM,
/*  SUI_SXML_VALUE_FPNUM, */
/*  SUI_SXML_VALUE_POINTER, */
  SUI_SXML_VALUE_CHARACTER,
};

void sui_sxml_value_clear(sui_sxml_value_t *value);
int sui_sxml_value_translate(sui_sxml_value_t *val, char **buffer, int bufsize);
void sui_sxml_value_resolve_id(sui_sxml_value_t *val);
int sui_sxml_value_to_object(sui_sxml_value_t *val, void **objptr,
                              sui_obj_vmt_t *objvmt);


/******************************************************************************
 * sxml attribute structure
 ******************************************************************************/
/**
 * struct sui_sxml_attrib - Description of a SXML attribute
 * @name: Attribute's name.
 * @value: Attribute's value as a string.
 *
 * File: sui_sxml.h
 */
typedef struct sui_sxml_attrib {
  char *name;
  char *value;
} sui_sxml_attrib_t;

void sui_sxml_attrib_clear(sui_sxml_attrib_t *attr);


/******************************************************************************
 * main tag structure - based on LIFO stack
 ******************************************************************************/
/**
 * struct sui_sxml_tag - Description of SXML's tag
 * @id: Tag ID.
 * @tag_name: Tag name of an unknown tag.
 * @flags: Tag flags.
 * @parchild: Pointer to an array of tag children structures.
 * @prev: Pointer to the previous tag (parent tag).
 * @obj: Pointer to an object created from tag.
 * @vmt: Pointer to an object VMT.
 * @tag_value: SXML tag's value.
 * @tag_valcomb: The last combination character for combining values.
 * @ns_name: A name of an object (described by tag) for saving in the namespace.
 * @user: User data, which are used for tag processing(list index,...).
 *
 * File: sui_sxml.h
 */
typedef struct sui_sxml_tag {
  int                              id;       /* tag id, if tag is unknown, compare tag_name */
  char                            *tag_name; /* tag name for skipping unknown tags */
  unsigned short                   flags;    /* tag flags */
  sui_sxml_tag_info_child_t const *parchild; /* pointer to parent children structure ... */

  struct sui_sxml_tag             *prev;     /* pointer to chained previous tag structure */

  void                            *obj;      /* object created from tag */
  sui_obj_vmt_t                   *vmt;      /* vmt of created object */

  sui_sxml_value_t                 tag_value;   /* tag value */
  char                             tag_valcomb; /* TODO: check if it is zero on tag ending - warning:syntax error */

  char                            *ns_name;  /* FIXME: ... howitworks...only for reading - if tag had 'name' attribute - don't touch */
  long                             user;     /* user data - e.g. index of added list item */

} sui_sxml_tag_t;

/**
 * enum sxml_tag_flags_enum - Tag flags
 * @SUI_SXML_FLG_UNKNOWN: Tag is unknown.
 * @SUI_SXML_FLG_SAVE_IN_NS: Tag object is saved in namespace.
 * @SUI_SXML_FLG_SAVE_IN_PAR: Tag object is used in parent tag object.
 * @SUI_SXML_FLG_NAMESPACE: Namespace was set(and changed) for tag object.
 * @SUI_SXML_FLG_SKIP: Skip tag in tag stack.
 * @SUI_SXML_FLG_NO_PROCESS: Don't process tag - when tag condition didn't passed.
 * @SUI_SXML_FLG_REDEF2NS: Object can be redefined in namespace, if it already exist there.
 * @SUI_SXML_FLG_REDEFCONTENT: Object content can be redefined in object existing in namespace.
 * @SUI_SXML_FLG_USER_VALUE: User data value was set.
 * @SUI_SXML_FLG_USER_ATTR1: Flag indicates user attribute.
 * @SUI_SXML_FLG_USER_SPECFLG: Special user flag for some tags.
 *
 * File: sui_sxml.h
 */
enum sxml_tag_flags_enum {
  SUI_SXML_FLG_UNKNOWN     = 0x0001, /* tag is unknown - redundant information with parchild==NULL */
  SUI_SXML_FLG_SAVE_IN_NS  = 0x0002, /* tag object was saved in namespace */
  SUI_SXML_FLG_SAVE_IN_PAR = 0x0004, /* tag object was saved in parent (no only copied) */
/*  SUI_SXML_FLG_LOCAL_NS    = 0x0008, */ /* before tag was local namespace - return it after tag */
/*  SUI_SXML_FLG_GLOBAL_NS   = 0x0010, */ /* before tag was global namespace - return it after tag */
  SUI_SXML_FLG_NAMESPACE   = 0x0008, /* namespace was set for this tag */

  SUI_SXML_FLG_SKIP        = 0x0020, /* skip tag in stack */
  SUI_SXML_FLG_NO_PROCESS  = 0x0040, /* don't process tag - when tag condition didn't passed */
  SUI_SXML_FLG_REDEF2NS    = 0x0080, /* object can be redefined in namespace, if it exist there */
  SUI_SXML_FLG_REDEFCONTENT= 0x0100, /* object content can be redefined in object in namespace */

  SUI_SXML_FLG_USER_VALUE  = 0x0800, /* user data value was set */
  SUI_SXML_FLG_USER_ATTR1  = 0x1000, /* flag indicates user attribute */
  SUI_SXML_FLG_USER_SPECFLG= 0x2000, /* special user flag for some tags */
};

sui_sxml_tag_t *sui_sxml_create_tag(char **tagname);
int sui_sxml_destroy_tag(sui_sxml_tag_t *tag);

sui_sxml_tag_t *sui_sxml_get_prev_tag(sui_sxml_tag_t *tag);

/******************************************************************************
 * tokenize sxml
 ******************************************************************************/
/**
 * struct sui_sxml_token - SXML token descriptor
 * @bsep: Begin token separator.
 * @esep: End token separator.
 * @size: Token size.
 * @buffer: Pointer to a buffer with last token.
 *
 * File: sui_sxml.h
 */
typedef struct sui_sxml_token {
  int   bsep;   /* begin separator */
  int   esep;   /* ending separator */
  long  size;   /* token size */
  char *buffer; /* buffer with last token */
} sui_sxml_token_t;

long sui_sxml_tokenize(struct sui_sxml_doc *doc, char *separs);

/******************************************************************************
 * sxml document
 ******************************************************************************/
/**
 * struct sui_sxml_doc - SXML document description structure
 * @source: Pointer to a file source with sxml document.
 * @state: Parsing state - IDLE, BEGIN_TAG,END_TAG, IGNORE up to XXX,...
 * @clevel: Level of a commentary (for nested commentary).
 * @ilevel: Level of an ignored tag - number of included tags with the same name.
 * @now: The last read character.
 * @tag: Pointer to the current in-processing tags.
 * @defs: Pointer to the DEFINE sub-namespace (shortcut).
 * @token: Structure with the current SXML token.
 * @curval: Structure with the currently processed tag value.
 * @curattr: Structure with the currently processed tag attribute.
 *
 * File: sui_sxml.h
 */
typedef struct sui_sxml_doc {
  sui_sxml_doc_description_t *ddesc; /* pointer to description of sxml tags */
  sui_file_source_t      *source;  /* pointer to chain of sxml sources */
  unsigned short          state;   /* parsing state - IDLE, BTAG, ETAG, IGNORE TO xxx, */
  unsigned char           clevel;  /* comment level */
  unsigned char           ilevel;  /* ignore tag level - number of included tags with the same name */
  int                     now;     /* last read character */
  struct sui_sxml_tag    *tag;     /* current in-processing tags */
  ns_subns_t             *defs;    /* pointer to define subnamespace */

  sui_sxml_token_t       token;    /* last read document token */
  struct sui_sxml_value  curval;
  struct sui_sxml_attrib curattr;
} sui_sxml_doc_t;

/**
 * enum sui_sxml_states - States of SXML parsing
 * @SUI_SXML_STATE_ERROR:  Parsing error.
 * @SUI_SXML_STATE_OUTTAG: Outside tag - value,btag,etag reading.
 * @SUI_SXML_STATE_INBTAG: Inside begin tag - attribute reading.
 * @SUI_SXML_STATE_INETAG: Inside end tag - ignore all attributes.
 * @SUI_SXML_STATE_IGNORE: All tokens will be ignored up to ETAG for last processed BTAG without 'SUI_SXML_FLG_NO_PROCESS' flag.
 *
 * File: sui_sxml.h
 */
enum sui_sxml_states {
  SUI_SXML_STATE_ERROR    = 0x0000, /* automat error */
  SUI_SXML_STATE_OUTTAG   = 0x0001, /* outside tag - value,btag,etag reading */
  SUI_SXML_STATE_INBTAG   = 0x0002, /* inside begin tag - attribute reading */
  SUI_SXML_STATE_INETAG   = 0x0003, /* inside end tag - ignore all attributes */
  SUI_SXML_STATE_IGNORE   = 0x0004, /* all tokens will be ignored up to ETAG for last processed BTAG without 'SUI_SXML_FLG_NO_PROCESS' flag */
};

/**
 * enum sui_sxml_error_codes - Error codes of SXML parsing
 * @SUI_SXML_ERR_OK:
 * @SUI_SXML_ERR_UNKNOWN:
 * @SUI_SXML_ERR_TAGNAME:
 * @SUI_SXML_ERR_TAGCROSS:
 * @SUI_SXML_ERR_ADD2NS:
 * @SUI_SXML_ERR_BADARGS:
 * @SUI_SXML_ERR_NOFILE:
 * @SUI_SXML_ERR_BTAGEXP:
 * @SUI_SXML_ERR_MEMORY:
 * @SUI_SXML_ERR_ADDTAG:
 * @SUI_SXML_ERR_REMTAG:
 * @SUI_SXML_ERR_SYNTAX:
 * @SUI_SXML_ERR_BADTAG:
 * @SUI_SXML_ERR_ISINNS:
 * @SUI_SXML_ERR_BADATTR:
 * @SUI_SXML_ERR_INTERNAL:
 * @SUI_SXML_ERR_TYPECONV:
 * @SUI_SXML_ERR_BADTERM:
 * @SUI_SXML_ERR_NONSOBJ:
 * @SUI_SXML_ERR_ADD2ARRAY:
 * @SUI_SXML_ERR_NOPROCESS:
 * @SUI_SXML_ERR_FIRSTTAG:
 * @SUI_SXML_ERR_DIRTAGVAL:
 * @SUI_SXML_ERR_INFILE:
 * @SUI_SXML_ERR_VALTYPE:
 * @SUI_SXML_ERR_ADDWIDGET:
 * @SUI_SXML_ERR_INHERIT:
 * @SUI_SXML_ERR_MISSTAG:
 * @SUI_SXML_ERR_BADCHKSUM:
 * @SUI_SXML_ERR_BADCOMBINE:
 * @SUI_SXML_ERR_MASK:
 * @SUI_SXML_ERR_WARNING:
 *
 * File: sui_sxml.h
 */
enum sui_sxml_error_codes {
  SUI_SXML_ERR_OK       = 0x00,
  SUI_SXML_ERR_UNKNOWN,
  SUI_SXML_ERR_TAGNAME,
  SUI_SXML_ERR_TAGCROSS,
  SUI_SXML_ERR_ADD2NS,
  SUI_SXML_ERR_BADARGS,
  SUI_SXML_ERR_NOFILE,

  SUI_SXML_ERR_BTAGEXP,
  SUI_SXML_ERR_MEMORY,
  SUI_SXML_ERR_ADDTAG,
  SUI_SXML_ERR_REMTAG,
  SUI_SXML_ERR_SYNTAX,
  SUI_SXML_ERR_BADTAG,
  SUI_SXML_ERR_ISINNS,
  SUI_SXML_ERR_BADATTR,

  SUI_SXML_ERR_INTERNAL,
  SUI_SXML_ERR_TYPECONV,
  SUI_SXML_ERR_BADTERM,
  SUI_SXML_ERR_NONSOBJ,
  SUI_SXML_ERR_ADD2ARRAY,
  SUI_SXML_ERR_NOPROCESS,

  SUI_SXML_ERR_FIRSTTAG,
  SUI_SXML_ERR_DIRTAGVAL,
  SUI_SXML_ERR_INFILE,
  SUI_SXML_ERR_VALTYPE,
  SUI_SXML_ERR_ADDWIDGET,
  SUI_SXML_ERR_INHERIT,
  SUI_SXML_ERR_MISSTAG,

  SUI_SXML_ERR_BADCHKSUM,

  SUI_SXML_ERR_BADCOMBINE,

  SUI_SXML_ERR_MASK    = 0x7F,
  SUI_SXML_ERR_WARNING = 0x8000,
};

/******************************************************************************
 * sxml parser error and warning functions
 ******************************************************************************/
void sui_sxml_doc_error(sui_sxml_doc_t *doc, int err, char *addstr);
void sui_sxml_doc_warning(sui_sxml_doc_t *doc, int warn, char *addstr);


/******************************************************************************
 * sxml main parse function
 ******************************************************************************/
int sui_sxml_tag_add_to_doc(sui_sxml_doc_t *doc, sui_sxml_tag_t *tag);
sui_sxml_tag_t *sui_sxml_tag_remove_from_doc(sui_sxml_doc_t *doc);
void sui_sxml_clear_tag_lifo(sui_sxml_doc_t *doc);

long sui_sxml_tokenize(sui_sxml_doc_t *doc, char *separs);
/******************************************************************************
 * sxml main parsing functions
 ******************************************************************************/
#if defined(SXML_TIME_PROFILE)
  extern unsigned long get_current_time(void);
#endif

int sui_sxml_parse_one_file(sui_sxml_doc_t *doc, char *fname_memory, long size);
int sui_sxml_parse_document(sui_sxml_doc_description_t *ddesc, char *fname, char *path);


/******************************************************************************
 * sxml term id search and resolve function
 ******************************************************************************/
int sui_term_resolve_id(char *id, sui_term_value_t *value);


/******************************************************************************
 * sxml reloading local namespace
 ******************************************************************************/
int sui_change_local_namespace_from_sxml(utf8 *name, void *par);


#endif /* _SXML_PARSER_HEADER_ */
