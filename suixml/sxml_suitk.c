/* sxml_suitk.c
 *
 * SXML for SuiTk.
 *
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#include <suixml/sui_sxml.h>
#include <suixml/sxml_suitk.h>

#include <suiut/sui_typemethods.h>
#include <suiut/sui_typebase.h>

#include <ul_log.h>

UL_LOG_CUST(ulogd_sxml)

/******************************************************************************/

/*
 * sui_link_vmt_done - Release all nested objects in a temporary link structure
 * @self: Pointer to a link object.
 *
 * The function releases all nested objects in the link object. The function
 * is called, if a link object is destroyed.
 *
 * Return Value: The function does not return any value.
 *
 * File: sui_sxml.c
 */
void sui_link_vmt_done(sui_obj_t *self)
{
  sui_sxml_link_t *l = (sui_sxml_link_t *) self;
  if (l->source) sui_utf8_dec_refcnt(l->source);
  l->source = NULL;
  if (l->signal) sui_utf8_dec_refcnt(l->signal);
  l->signal = NULL;
  if (l->target) sui_utf8_dec_refcnt(l->target);
  l->target = NULL;
  if (l->slot) sui_utf8_dec_refcnt(l->slot);
  l->slot = NULL;
}

SUI_TYPE_DECLARE_SIMPLE_EX(link, SUI_TYPE_LINK, sui_sxml_link_t, NULL,
                            NULL, sui_link_vmt_done);

/*
 * sui_varf_vmt_done - Release all nested objects in a temporary formated dinfo structure
 * @self: Pointer to a varf object (formated data).
 *
 * The function releases all nested objects in the varf object. The function
 * is called, if a varf object is destroyed.
 *
 * Return Value: The function does not return any value.
 *
 * File: sui_sxml.c
 */
void sui_varf_vmt_done(sui_obj_t *self)
{
  sui_varf_t *vf = (sui_varf_t *) self;
  if (vf->di) sui_dinfo_dec_refcnt(vf->di);
  vf->di = NULL;
  if (vf->fi) sui_finfo_dec_refcnt(vf->fi);
  vf->fi = NULL;
}

SUI_TYPE_DECLARE_SIMPLE_EX(varf, SUI_TYPE_VARF, sui_varf_t, NULL,
                            NULL, sui_varf_vmt_done);

/*
 * sui_varl_vmt_done - Release all nested objects in a temporary structure with dinfo and list structure
 * @self: Pointer to a varl object (list with variable).
 *
 * The function releases all nested objects in the varl object. The function
 * is called, if a varl object is destroyed.
 *
 * Return Value: The function does not return any value.
 *
 * File: sui_sxml.c
 */
void sui_varl_vmt_done(sui_obj_t *self)
{
  sui_varl_t *vl = (sui_varl_t *) self;
  if (vl->di) sui_dinfo_dec_refcnt(vl->di);
  vl->di = NULL;
  if (vl->li) sui_list_dec_refcnt(vl->li);
  vl->li = NULL;
}

SUI_TYPE_DECLARE_SIMPLE_EX(varl, SUI_TYPE_VARL, sui_varl_t, NULL,
                            NULL, sui_varl_vmt_done);

/******************************************************************************
 * tag information structures
 ******************************************************************************/

/* list of children - end of list is by tag_id = negative value */
sui_sxml_tag_info_child_t const sui_skt_sxml_children[] =
 {{SUI_SKT_WIDGETSET, 0, SUI_SXML_NO_ADD_TO_TAG, NULL, SUI_SXML_CHFLG_SUBTAGS},
  {SUI_SKT_OBJECTSET, 0, SUI_SXML_NO_ADD_TO_TAG, NULL, SUI_SXML_CHFLG_SUBTAGS},
  {SUI_SKT_SCREENSET, 0, SUI_SXML_NO_ADD_TO_TAG, NULL, SUI_SXML_CHFLG_SUBTAGS},
  {SUI_SKT_TITLE,     0, SUI_SXML_NO_ADD_TO_TAG, NULL, SUI_SXML_CHFLG_SKIP},
  {SUI_SKT_AUTHOR,    0, SUI_SXML_NO_ADD_TO_TAG, NULL, SUI_SXML_CHFLG_SKIP},
  {SUI_SKT_DATE,      0, SUI_SXML_NO_ADD_TO_TAG, NULL, SUI_SXML_CHFLG_SKIP},
  {SUI_SKT_VERSION,   0, SUI_SXML_NO_ADD_TO_TAG, NULL, SUI_SXML_CHFLG_SKIP},
  {SUI_SXML_ENDOFCHILDREN, 0, 0, NULL, 0}};

sui_sxml_tag_info_child_t const sui_skt_widgetset_children[] = 
 {{SUI_SKT_COLOR,      0, SUI_SXML_NO_ADD_TO_TAG, &sui_color_vmt_data, 0},
  {SUI_SKT_COLORTABLE, 0, SUI_SXML_NO_ADD_TO_TAG, &sui_colortable_vmt_data, SUI_SXML_CHFLG_SUBTAGS},
  {SUI_SKT_FONT,       0, SUI_SXML_NO_ADD_TO_TAG, &sui_fontinfo_vmt_data, SUI_SXML_CHFLG_SUBTAGS},
  {SUI_SKT_STYLE,      0, SUI_SXML_NO_ADD_TO_TAG, &sui_style_vmt_data, SUI_SXML_CHFLG_SUBTAGS},
  {SUI_SXML_ENDOFCHILDREN, 0, 0, NULL, 0}};

sui_sxml_tag_info_child_t const sui_skt_objectset_children[] = 
 {{SUI_SKT_VAR,         0, SUI_SXML_NO_ADD_TO_TAG, &sui_dinfo_vmt_data, SUI_SXML_CHFLG_SUBTAGS},
  {SUI_SKT_SUFFIX,      0, SUI_SXML_NO_ADD_TO_TAG, &sui_suffix_vmt_data, SUI_SXML_CHFLG_SUBTAGS},
  {SUI_SKT_LIST,        0, SUI_SXML_NO_ADD_TO_TAG, SUI_OBJ_VMT_FROM_VMT(&sui_list_vmt_data), SUI_SXML_CHFLG_SUBTAGS},
  {SUI_SKT_KEYTABLE,    0, SUI_SXML_NO_ADD_TO_TAG, &sui_keytable_vmt_data, SUI_SXML_CHFLG_SUBTAGS},
  {SUI_SKT_IMAGE,       0, SUI_SXML_NO_ADD_TO_TAG, &sui_image_vmt_data, SUI_SXML_CHFLG_SUBTAGS},
  {SUI_SKT_FORMAT,      0, SUI_SXML_NO_ADD_TO_TAG, &sui_format_vmt_data, SUI_SXML_CHFLG_SUBTAGS},
  {SUI_SKT_TEXT,        0, SUI_SXML_NO_ADD_TO_TAG, &sui_utf8_vmt_data, 0},
  {SUI_SKT_CHOICETABLE, 0, SUI_SXML_NO_ADD_TO_TAG, &sui_choicetable_vmt_data, SUI_SXML_CHFLG_SUBTAGS},
  {SUI_SKT_TABLE,       0, SUI_SXML_NO_ADD_TO_TAG, &sui_table_vmt_data, SUI_SXML_CHFLG_SUBTAGS},
  {SUI_SKT_PRNFILTER,   0, SUI_SXML_NO_ADD_TO_TAG, &sui_prnfilter_vmt_data, SUI_SXML_CHFLG_SUBTAGS},
  {SUI_SKT_PRNLAYOUT,   0, SUI_SXML_NO_ADD_TO_TAG, &sui_prnlayout_vmt_data, SUI_SXML_CHFLG_SUBTAGS},
  {SUI_SXML_ENDOFCHILDREN, 0, 0, NULL, 0}};

sui_sxml_tag_info_child_t const sui_skt_screenset_children[] = 
 {{SUI_SKT_WIDGET,   0, SUI_SXML_NO_ADD_TO_TAG, SUI_OBJ_VMT_FROM_VMT(&sui_widget_vmt_data), SUI_SXML_CHFLG_SUBTAGS},
  {SUI_SKT_WGROUP,   0, SUI_SXML_NO_ADD_TO_TAG, SUI_OBJ_VMT_FROM_VMT(&sui_wgroup_vmt_data), SUI_SXML_CHFLG_SUBTAGS},
  {SUI_SKT_WBUTTON,  0, SUI_SXML_NO_ADD_TO_TAG, SUI_OBJ_VMT_FROM_VMT(&sui_wbutton_vmt_data), SUI_SXML_CHFLG_SUBTAGS},
  {SUI_SKT_WBITMAP,  0, SUI_SXML_NO_ADD_TO_TAG, SUI_OBJ_VMT_FROM_VMT(&sui_wbitmap_vmt_data), SUI_SXML_CHFLG_SUBTAGS},
  {SUI_SKT_WLISTBOX, 0, SUI_SXML_NO_ADD_TO_TAG, SUI_OBJ_VMT_FROM_VMT(&sui_wlistbox_vmt_data), SUI_SXML_CHFLG_SUBTAGS},
  {SUI_SKT_WNUMBER,  0, SUI_SXML_NO_ADD_TO_TAG, SUI_OBJ_VMT_FROM_VMT(&sui_wnumber_vmt_data), SUI_SXML_CHFLG_SUBTAGS},
  {SUI_SKT_WSCROLL,  0, SUI_SXML_NO_ADD_TO_TAG, SUI_OBJ_VMT_FROM_VMT(&sui_wscroll_vmt_data), SUI_SXML_CHFLG_SUBTAGS},
  {SUI_SKT_WTEXT,    0, SUI_SXML_NO_ADD_TO_TAG, SUI_OBJ_VMT_FROM_VMT(&sui_wtext_vmt_data), SUI_SXML_CHFLG_SUBTAGS},
  {SUI_SKT_WTIME,    0, SUI_SXML_NO_ADD_TO_TAG, SUI_OBJ_VMT_FROM_VMT(&sui_wtime_vmt_data), SUI_SXML_CHFLG_SUBTAGS},
  {SUI_SKT_WTABLE,   0, SUI_SXML_NO_ADD_TO_TAG, SUI_OBJ_VMT_FROM_VMT(&sui_wtable_vmt_data), SUI_SXML_CHFLG_SUBTAGS},
  {SUI_SKT_WDYNTEXT, 0, SUI_SXML_NO_ADD_TO_TAG, SUI_OBJ_VMT_FROM_VMT(&sui_wdyntext_vmt_data), SUI_SXML_CHFLG_SUBTAGS},
  {SUI_SKT_WGRAPHXY, 0, SUI_SXML_NO_ADD_TO_TAG, SUI_OBJ_VMT_FROM_VMT(&sui_wgraphxy_vmt_data), SUI_SXML_CHFLG_SUBTAGS},

  {SUI_SKT_CONNECTIONS, 0, SUI_SXML_NO_ADD_TO_TAG, NULL, SUI_SXML_CHFLG_SUBTAGS},
  {SUI_SKT_SCENARIO, 0, SUI_SXML_NO_ADD_TO_TAG, SUI_OBJ_VMT_FROM_VMT(&sui_scenario_vmt_data), SUI_SXML_CHFLG_SUBTAGS},
  {SUI_SXML_ENDOFCHILDREN, 0, 0, NULL, 0}};

/* widgetset */
sui_sxml_tag_info_child_t const sui_skt_colortable_children[] =
 {{SUI_SKT_COLOR, 0, SUI_SXML_PROCESS_FCN, &sui_color_vmt_data, SUI_SXML_CHFLG_ARRAY},
  {SUI_SXML_ENDOFCHILDREN, 0, 0, NULL, 0}};

sui_sxml_tag_info_child_t const sui_skt_font_children[] =
 {{SUI_SKT_NAME,  0, offsetof(sui_font_info_t, font), &sui_utf8_vmt_data, SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_SIZE,  0, offsetof(sui_font_info_t, size), &sui_int_vmt_data, 0},
  {SUI_SKT_FLAGS, 0, offsetof(sui_font_info_t, flag), &sui_ushort_vmt_data, 0},
  {SUI_SXML_ENDOFCHILDREN, 0, 0, NULL, 0}};

sui_sxml_tag_info_child_t const sui_skt_style_children[] =
 {{SUI_SKT_FLAGS,      0, offsetof(sui_style_t, flags), &sui_flags_vmt_data, 0},
  {SUI_SKT_COLORTABLE, 0, offsetof(sui_style_t, coltab), &sui_colortable_vmt_data, SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_LABELFONT,  0, offsetof(sui_style_t, labfonti), &sui_fontinfo_vmt_data, SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_FONT,       0, offsetof(sui_style_t, fonti), &sui_fontinfo_vmt_data, SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_FRAME,      0, offsetof(sui_style_t, frame), &sui_coord_vmt_data, 0},
  {SUI_SKT_GAP,        0, offsetof(sui_style_t, gap), &sui_coord_vmt_data, 0},
  {SUI_SKT_LABALIGN,   0, offsetof(sui_style_t, labalign), &sui_align_vmt_data, 0}, \
  {SUI_SXML_ENDOFCHILDREN, 0, 0, NULL, 0}};

/* objectset */
sui_sxml_tag_info_child_t const sui_skt_var_children[] =
 {{SUI_SKT_TYPE,   0, SUI_SXML_PROCESS_FCN, &sui_type_vmt_data, 0},
  //{SUI_SKT_DATA,   0, SUI_SXML_NO_ADD_TO_TAG, &sui_dbuff_vmt_data, SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_MIN,    0, offsetof(sui_dinfo_t, minval), &sui_long_vmt_data, 0},
  {SUI_SKT_MAX,    0, offsetof(sui_dinfo_t, maxval), &sui_long_vmt_data, 0},
  {SUI_SKT_DIGITS, 0, offsetof(sui_dinfo_t, fdigits), &sui_int_vmt_data, 0},
//  {SUI_SKT_MAXINDEX, offsetof(sui_dinfo_t, idxsize), &sui_long_vmt_data, 0},
  {SUI_SKT_INIT,   0, SUI_SXML_PROCESS_FCN, &sui_long_vmt_data, 0}, /* initial signed long value */
  {SUI_SKT_UINIT,  0, SUI_SXML_PROCESS_FCN, &sui_ulong_vmt_data, 0}, /* initial unsigned long value */
  {SUI_SKT_TEXT,   0, SUI_SXML_PROCESS_FCN, &sui_utf8_vmt_data, 0}, /* initial text */
  {SUI_SXML_ENDOFCHILDREN, 0, 0, NULL, 0}};

sui_sxml_tag_info_child_t const sui_skt_suffix_children[] =
 {{SUI_SKT_TEXT,         0, offsetof(sui_suffix_t, suffix), &sui_utf8_vmt_data, SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_DECIMALALIGN, 0, offsetof(sui_suffix_t, decalign), &sui_align_vmt_data, 0},
  {SUI_SKT_SUFFIXALIGN,  0, offsetof(sui_suffix_t, sufalign), &sui_align_vmt_data, 0},
  {SUI_SKT_DECIMALFONT,  0, offsetof(sui_suffix_t, decfonti), &sui_fontinfo_vmt_data, SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_SUFFIXFONT,   0, offsetof(sui_suffix_t, suffonti), &sui_fontinfo_vmt_data, SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SXML_ENDOFCHILDREN, 0, 0, NULL, 0}};

sui_sxml_tag_info_child_t const sui_skt_varf_children[] =
 {{SUI_SKT_VAR, 0, offsetof(sui_varf_t, di), &sui_dinfo_vmt_data, SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_FORMAT, 0, offsetof(sui_varf_t, fi), &sui_format_vmt_data, SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SXML_ENDOFCHILDREN, 0, 0, NULL, 0}};

sui_sxml_tag_info_child_t const sui_skt_varl_children[] =
 {{SUI_SKT_VAR, 0, offsetof(sui_varl_t, di), &sui_dinfo_vmt_data, SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_LIST, 0, offsetof(sui_varl_t, li), SUI_OBJ_VMT_FROM_VMT(&sui_list_vmt_data), SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SXML_ENDOFCHILDREN, 0, 0, NULL, 0}};

sui_sxml_tag_info_child_t const sui_skt_list_children[] =
 {{SUI_SKT_TEXT, 0, SUI_SXML_PROCESS_FCN, &sui_utf8_vmt_data, SUI_SXML_CHFLG_ARRAY | SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_VARF, 0, SUI_SXML_PROCESS_FCN, &sui_varf_vmt_data, SUI_SXML_CHFLG_SUBTAGS},
  {SUI_SKT_VARL, 0, SUI_SXML_PROCESS_FCN, &sui_varl_vmt_data, SUI_SXML_CHFLG_SUBTAGS},
  {SUI_SKT_LIPRINT,0, SUI_SXML_PROCESS_FCN, SUI_OBJ_VMT_FROM_VMT(&sui_list_vmt_data), SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SXML_ENDOFCHILDREN, 0, 0, NULL, 0}};

sui_sxml_tag_info_child_t const sui_skt_keytable_children[] =
 {{SUI_SKT_KEY,      0, SUI_SXML_PROCESS_FCN, &sui_key_vmt_data, SUI_SXML_CHFLG_ARRAY | SUI_SXML_CHFLG_SUBTAGS},
  {SUI_SKT_KEYTABLE, 0, SUI_SXML_PROCESS_FCN, &sui_keytable_vmt_data, SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SXML_ENDOFCHILDREN, 0, 0, NULL, 0}};

sui_sxml_tag_info_child_t const sui_skt_key_children[] =
// {{SUI_SKT_VALUE, 0, offsetof(sui_key_action_t, keycode), &sui_ushort_vmt_data, 0},
 {{SUI_SKT_KEYCODE, 0, offsetof(sui_key_action_t, keycode), &sui_ushort_vmt_data, 0},
  {SUI_SKT_FCN,   0, offsetof(sui_key_action_t, hkey), &sui_keyfcn_vmt_data, 0},
  {SUI_SKT_ARG,   0, offsetof(sui_key_action_t, info), &sui_long_vmt_data, 0},
  {SUI_SXML_ENDOFCHILDREN, 0, 0, NULL, 0}};

sui_sxml_tag_info_child_t const sui_skt_image_children[] =
 {{SUI_SKT_FILE,       0, SUI_SXML_PROCESS_FCN, &sui_utf8_vmt_data, 0}, //sui_ascii_vmt_data
  {SUI_SKT_COUNT,      0, SUI_SXML_PROCESS_FCN, &sui_int_vmt_data, 0},
  {SUI_SKT_COLORTABLE, 0, offsetof(sui_image_t, cmap), &sui_colortable_vmt_data, SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SXML_ENDOFCHILDREN, 0, 0, NULL, 0}};

sui_sxml_tag_info_child_t const sui_skt_format_children[] =
 {{SUI_SKT_BASE,        0, offsetof(sui_finfo_t, base), &sui_int_vmt_data, 0},
  {SUI_SKT_DIGITS,      0, offsetof(sui_finfo_t, digits), &sui_int_vmt_data, 0},
  {SUI_SKT_MIN,         0, offsetof(sui_finfo_t, litstep), &sui_long_vmt_data, 0},
  {SUI_SKT_MAX,         0, offsetof(sui_finfo_t, bigstep), &sui_long_vmt_data, 0},
  {SUI_SKT_TEXT,        0, offsetof(sui_finfo_t, ftext), &sui_utf8_vmt_data, SUI_SXML_CHFLG_POINTER | SUI_SXML_CHFLG_NO_OBJID},
  {SUI_SKT_CHOICETABLE, 0, offsetof(sui_finfo_t, choices), &sui_choicetable_vmt_data, SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_FLAGS,       0, offsetof(sui_finfo_t, flags), &sui_ushort_vmt_data, 0},
  {SUI_SXML_ENDOFCHILDREN, 0, 0, NULL, 0}};

sui_sxml_tag_info_child_t const sui_skt_choicetable_children[] =
 {{SUI_SKT_CHOICE, 0, SUI_SXML_PROCESS_FCN, &sui_choice_vmt_data, SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_ARRAY},
  {SUI_SXML_ENDOFCHILDREN, 0, 0, NULL, 0}};

sui_sxml_tag_info_child_t const sui_skt_choice_children[] =
 {{SUI_SKT_MASK,  0, offsetof(sui_choice_t, mask), &sui_long_vmt_data, 0},
  {SUI_SKT_VALUE, 0, offsetof(sui_choice_t, value), &sui_long_vmt_data, 0},
  {SUI_SKT_TEXT,  0, offsetof(sui_choice_t, text), &sui_utf8_vmt_data, SUI_SXML_CHFLG_POINTER | SUI_SXML_CHFLG_NO_OBJID},
  {SUI_SXML_ENDOFCHILDREN, 0, 0, NULL, 0}};

sui_sxml_tag_info_child_t const sui_skt_table_children[] =
 {{SUI_SKT_COLUMN, 0, SUI_SXML_PROCESS_FCN, &sui_column_vmt_data, SUI_SXML_CHFLG_ARRAY | SUI_SXML_CHFLG_SUBTAGS},
  {SUI_SXML_ENDOFCHILDREN, 0, 0, NULL, 0}};

sui_sxml_tag_info_child_t const sui_skt_column_children[] =
 {{SUI_SKT_TITLE,        0, offsetof(sui_table_column_t, title), &sui_utf8_vmt_data, SUI_SXML_CHFLG_POINTER | SUI_SXML_CHFLG_NO_OBJID},
  {SUI_SKT_FORMAT,       0, offsetof(sui_table_column_t, format), &sui_format_vmt_data, SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_VAR,          0, offsetof(sui_table_column_t, data), &sui_dinfo_vmt_data, SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_LIST,         0, offsetof(sui_table_column_t, list), SUI_OBJ_VMT_FROM_VMT(&sui_list_vmt_data), SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_SPAN,         0, offsetof(sui_table_column_t, span), &sui_dinfo_vmt_data, SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_FONT,         0, offsetof(sui_table_column_t, font), &sui_fontinfo_vmt_data, SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_ALIGN,        0, offsetof(sui_table_column_t, align), &sui_align_vmt_data, 0},
  {SUI_SKT_WIDTH,        0, offsetof(sui_table_column_t, width), &sui_coord_vmt_data, 0},
  {SUI_SKT_HEIGHT,       0, offsetof(sui_table_column_t, height), &sui_coord_vmt_data, 0},
  {SUI_SXML_ENDOFCHILDREN, 0, 0, NULL, 0}};

sui_sxml_tag_info_child_t const sui_skt_prnfilter_children[] =
 {{SUI_SKT_FCN,          0, SUI_SXML_PROCESS_FCN, &sui_utf8_vmt_data, SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_FLAGS,        0, offsetof(sui_print_filter_t, flags), &sui_ulong_vmt_data, 0},
  {SUI_SKT_TEXT,         0, offsetof(sui_print_filter_t, text), &sui_utf8_vmt_data, SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_VALUE,        0, offsetof(sui_print_filter_t, value), &sui_long_vmt_data, 0},
  {SUI_SXML_ENDOFCHILDREN, 0, 0, NULL, 0}};

sui_sxml_tag_info_child_t const sui_skt_prnlayout_children[] =
 {{SUI_SKT_DEVICE,       0, offsetof(sui_print_layout_t, devices), &sui_utf8_vmt_data, SUI_SXML_CHFLG_POINTER | SUI_SXML_CHFLG_NO_OBJID},
  {SUI_SKT_WIDTH,        0, offsetof(sui_print_layout_t, paperwidth), &sui_int_vmt_data, 0},
  {SUI_SKT_HEIGHT,       0, offsetof(sui_print_layout_t, paperheight), &sui_int_vmt_data, 0},
  {SUI_SKT_FLAGS,        0, offsetof(sui_print_layout_t, flags), &sui_ulong_vmt_data, 0},
  {SUI_SKT_PRNFILTER,    0, SUI_SXML_PROCESS_FCN, &sui_prnfilter_vmt_data, SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_DATA,         0, offsetof(sui_print_layout_t, layout), &sui_utf8_vmt_data, SUI_SXML_CHFLG_POINTER},
  {SUI_SXML_ENDOFCHILDREN, 0, 0, NULL, 0}};

sui_sxml_tag_info_child_t const sui_skt_gdata_children[] =
 {{SUI_SKT_GSET, 0, SUI_SXML_PROCESS_FCN, &sui_gset_vmt_data, SUI_SXML_CHFLG_ARRAY | SUI_SXML_CHFLG_SUBTAGS},
  {SUI_SXML_ENDOFCHILDREN, 0, 0, NULL, 0}};

sui_sxml_tag_info_child_t const sui_skt_gset_children[] =
 {{SUI_SKT_LABEL,  0, offsetof(sui_graph_dataset_t, label), &sui_utf8_vmt_data, SUI_SXML_CHFLG_POINTER | SUI_SXML_CHFLG_NO_OBJID},
  {SUI_SKT_ALIGN,  0, offsetof(sui_graph_dataset_t, align), &sui_align_vmt_data, 0},
  {SUI_SKT_FONT,   0, offsetof(sui_graph_dataset_t, font), &sui_fontinfo_vmt_data, SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_VAR,    0, offsetof(sui_graph_dataset_t, data), &sui_dinfo_vmt_data, SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_FORMAT, 0, offsetof(sui_graph_dataset_t, format), &sui_format_vmt_data, SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_FLAGS,  0, offsetof(sui_graph_dataset_t, flags), &sui_ulong_vmt_data, 0},
  {SUI_SKT_OFFSET, 0, offsetof(sui_graph_dataset_t, offset), &sui_long_vmt_data, 0},
  {SUI_SKT_STEP,   0, offsetof(sui_graph_dataset_t, step), &sui_long_vmt_data, 0},
  {SUI_SKT_COLOR,  0, offsetof(sui_graph_dataset_t, color), &sui_color_vmt_data, 0},
  {SUI_SKT_WIDTH,  0, offsetof(sui_graph_dataset_t, width), &sui_coord_vmt_data, 0},
  {SUI_SXML_ENDOFCHILDREN, 0, 0, NULL, 0}};

/* screenset */

#define SUI_SXML_TAG_INFO_CHILD_WIDGET_BASE \
  {SUI_SKT_STYLE,    0, offsetof(sui_widget_t, style), &sui_style_vmt_data, SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER}, \
  {SUI_SKT_X,        0, offsetof(sui_widget_t, place.x), &sui_coord_vmt_data, 0}, \
  {SUI_SKT_Y,        0, offsetof(sui_widget_t, place.y), &sui_coord_vmt_data, 0}, \
  {SUI_SKT_WIDTH,    0, offsetof(sui_widget_t, place.w), &sui_coord_vmt_data, 0}, \
  {SUI_SKT_HEIGHT,   0, offsetof(sui_widget_t, place.h), &sui_coord_vmt_data, 0}, \
  {SUI_SKT_FLAGS,    0, offsetof(sui_widget_t, flags), &sui_flags_vmt_data, 0}, \
  {SUI_SKT_KEYTABLE, 0, offsetof(sui_widget_t, key_table), &sui_keytable_vmt_data, SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER}, \
  {SUI_SKT_LABEL,    0, offsetof(sui_widget_t, label), &sui_utf8_vmt_data, SUI_SXML_CHFLG_POINTER | SUI_SXML_CHFLG_NO_OBJID}, \
  {SUI_SKT_LABALIGN, 0, offsetof(sui_widget_t, labalign), &sui_align_vmt_data, 0}, \
  {SUI_SKT_TIP,      0, offsetof(sui_widget_t, tooltip), &sui_utf8_vmt_data, SUI_SXML_CHFLG_POINTER | SUI_SXML_CHFLG_NO_OBJID}

sui_sxml_tag_info_child_t const sui_skt_widget_children[] =
  {SUI_SXML_TAG_INFO_CHILD_WIDGET_BASE,
  {SUI_SXML_ENDOFCHILDREN, 0, 0, NULL, 0}};

sui_sxml_tag_info_child_t const sui_skt_wgroup_children[] =
  {SUI_SXML_TAG_INFO_CHILD_WIDGET_BASE,
  {SUI_SKT_WIDGET,   0, SUI_SXML_PROCESS_FCN, SUI_OBJ_VMT_FROM_VMT(&sui_widget_vmt_data), SUI_SXML_CHFLG_POINTER}, /* SUI_SXML_CHFLG_SUBTAGS | */
  {SUI_SKT_STATUS,   0, SUI_SXML_PROCESS_FCN, SUI_OBJ_VMT_FROM_VMT(&sui_widget_vmt_data), SUI_SXML_CHFLG_POINTER}, /* SUI_SXML_CHFLG_SUBTAGS | */
  {SUI_SKT_WGROUP,   0, SUI_SXML_PROCESS_FCN, SUI_OBJ_VMT_FROM_VMT(&sui_wgroup_vmt_data), SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_WBUTTON,  0, SUI_SXML_PROCESS_FCN, SUI_OBJ_VMT_FROM_VMT(&sui_wbutton_vmt_data), SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_WBITMAP,  0, SUI_SXML_PROCESS_FCN, SUI_OBJ_VMT_FROM_VMT(&sui_wbitmap_vmt_data), SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_WLISTBOX, 0, SUI_SXML_PROCESS_FCN, SUI_OBJ_VMT_FROM_VMT(&sui_wlistbox_vmt_data), SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_WNUMBER,  0, SUI_SXML_PROCESS_FCN, SUI_OBJ_VMT_FROM_VMT(&sui_wnumber_vmt_data), SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_WSCROLL,  0, SUI_SXML_PROCESS_FCN, SUI_OBJ_VMT_FROM_VMT(&sui_wscroll_vmt_data), SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_WTEXT,    0, SUI_SXML_PROCESS_FCN, SUI_OBJ_VMT_FROM_VMT(&sui_wtext_vmt_data), SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_WTIME,    0, SUI_SXML_PROCESS_FCN, SUI_OBJ_VMT_FROM_VMT(&sui_wtime_vmt_data), SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_WTABLE,   0, SUI_SXML_PROCESS_FCN, SUI_OBJ_VMT_FROM_VMT(&sui_wtable_vmt_data), SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_WDYNTEXT, 0, SUI_SXML_PROCESS_FCN, SUI_OBJ_VMT_FROM_VMT(&sui_wdyntext_vmt_data), SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_WGRAPHXY, 0, SUI_SXML_PROCESS_FCN, SUI_OBJ_VMT_FROM_VMT(&sui_wgraphxy_vmt_data), SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SXML_ENDOFCHILDREN, 0, 0, NULL, 0}};

sui_sxml_tag_info_child_t const sui_skt_wbutton_children[] =
  {SUI_SXML_TAG_INFO_CHILD_WIDGET_BASE,
  {SUI_SKT_SIZE,     offsetof(sui_widget_t, data), offsetof(suiw_button_t, size), &sui_int_vmt_data, 0},
  {SUI_SKT_COLOR,    offsetof(sui_widget_t, data), offsetof(suiw_button_t, color), &sui_color_vmt_data, 0},
  {SUI_SKT_IMAGE,    offsetof(sui_widget_t, data), offsetof(suiw_button_t, image), &sui_image_vmt_data, SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_VAR,      offsetof(sui_widget_t, data), offsetof(suiw_button_t, di), &sui_dinfo_vmt_data, SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_ID,       offsetof(sui_widget_t, data), offsetof(suiw_button_t, wid), &sui_int_vmt_data, 0},
  {SUI_SKT_VALUE,    offsetof(sui_widget_t, data), offsetof(suiw_button_t, value), &sui_long_vmt_data, 0},
  {SUI_SXML_ENDOFCHILDREN, 0, 0, NULL, 0}};

sui_sxml_tag_info_child_t const sui_skt_wbitmap_children[] =
  {SUI_SXML_TAG_INFO_CHILD_WIDGET_BASE,
  {SUI_SKT_VAR,      offsetof(sui_widget_t, data), offsetof(suiw_bitmap_t, imidx), &sui_dinfo_vmt_data, SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_IMAGE,    offsetof(sui_widget_t, data), offsetof(suiw_bitmap_t, image), &sui_image_vmt_data, SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_MASK,     offsetof(sui_widget_t, data), offsetof(suiw_bitmap_t, mask), &sui_image_vmt_data, SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SXML_ENDOFCHILDREN, 0, 0, NULL, 0}};

sui_sxml_tag_info_child_t const sui_skt_wlistbox_children[] =
  {SUI_SXML_TAG_INFO_CHILD_WIDGET_BASE,
  {SUI_SKT_LIST,     offsetof(sui_widget_t, data), offsetof(suiw_listbox_t, list), SUI_OBJ_VMT_FROM_VMT(&sui_list_vmt_data), SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_VAR,      offsetof(sui_widget_t, data), offsetof(suiw_listbox_t, pos), &sui_dinfo_vmt_data, SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_ALIGN,    offsetof(sui_widget_t, data), offsetof(suiw_listbox_t, align), &sui_align_vmt_data, 0},
  {SUI_SKT_GAP,      offsetof(sui_widget_t, data), offsetof(suiw_listbox_t, gap), &sui_coord_vmt_data, 0},
  {SUI_SKT_COLS,     offsetof(sui_widget_t, data), offsetof(suiw_listbox_t, cols), &sui_int_vmt_data, 0},
  {SUI_SKT_ROWS,     offsetof(sui_widget_t, data), offsetof(suiw_listbox_t, lines), &sui_int_vmt_data, 0},
  {SUI_SKT_IMAGE,    offsetof(sui_widget_t, data), offsetof(suiw_listbox_t, imglib), &sui_image_vmt_data, SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SXML_ENDOFCHILDREN, 0, 0, NULL, 0}};

sui_sxml_tag_info_child_t const sui_skt_wnumber_children[] =
  {SUI_SXML_TAG_INFO_CHILD_WIDGET_BASE,
  {SUI_SKT_VAR,      offsetof(sui_widget_t, data), offsetof(suiw_number_t, data), &sui_dinfo_vmt_data, SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER}, //306},
  {SUI_SKT_FORMAT,   offsetof(sui_widget_t, data), offsetof(suiw_number_t, format), &sui_format_vmt_data, SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_SUFFIX,   offsetof(sui_widget_t, data), offsetof(suiw_number_t, suffix), &sui_suffix_vmt_data, SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_ALIGN,    offsetof(sui_widget_t, data), offsetof(suiw_number_t, align), &sui_align_vmt_data, 0},
  {SUI_SXML_ENDOFCHILDREN, 0, 0, NULL, 0}};

sui_sxml_tag_info_child_t const sui_skt_wscroll_children[] =
  {SUI_SXML_TAG_INFO_CHILD_WIDGET_BASE,
  {SUI_SKT_VAR,      offsetof(sui_widget_t, data), offsetof(suiw_scroll_t, datai), &sui_dinfo_vmt_data, SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_FORMAT,   offsetof(sui_widget_t, data), offsetof(suiw_scroll_t, format), &sui_format_vmt_data, SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_LINE,     offsetof(sui_widget_t, data), offsetof(suiw_scroll_t, wline), &sui_coord_vmt_data, 0},
  {SUI_SKT_SIZE,     offsetof(sui_widget_t, data), offsetof(suiw_scroll_t, wpuller), &sui_coord_vmt_data, 0},
  {SUI_SXML_ENDOFCHILDREN, 0, 0, NULL, 0}};

sui_sxml_tag_info_child_t const sui_skt_wtext_children[] =
  {SUI_SXML_TAG_INFO_CHILD_WIDGET_BASE,
  {SUI_SKT_TEXT,     0, SUI_SXML_PROCESS_FCN, &sui_utf8_vmt_data, SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_VAR,      offsetof(sui_widget_t, data), offsetof(suiw_text_t, di), &sui_dinfo_vmt_data, SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_FORMAT,   offsetof(sui_widget_t, data), offsetof(suiw_text_t, format), &sui_format_vmt_data, SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_ALIGN,    offsetof(sui_widget_t, data), offsetof(suiw_text_t, txtalign), &sui_align_vmt_data, 0},
  {SUI_SXML_ENDOFCHILDREN, 0, 0, NULL, 0}};

sui_sxml_tag_info_child_t const sui_skt_wtime_children[] =
  {SUI_SXML_TAG_INFO_CHILD_WIDGET_BASE,
  {SUI_SKT_VAR,      offsetof(sui_widget_t, data), offsetof(suiw_time_t, data),    &sui_dinfo_vmt_data, SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_FORMAT,   offsetof(sui_widget_t, data), offsetof(suiw_time_t, format), &sui_utf8_vmt_data, SUI_SXML_CHFLG_POINTER | SUI_SXML_CHFLG_NO_OBJID},
  {SUI_SKT_ALIGN,    offsetof(sui_widget_t, data), offsetof(suiw_time_t, align), &sui_align_vmt_data, 0},
//  {SUI_SKT_DAYS,   0, offsetof(suiw_time_t, day_name), sui_dinfo_vmt_data, SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
//  {SUI_SKT_MONTHS, 0, offsetof(suiw_time_t, month_name), sui_dinfo_vmt_data, SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
//  {SUI_SKT_AMPM,   0, offsetof(suiw_time_t, ampm_name), sui_dinfo_vmt_data, SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SXML_ENDOFCHILDREN, 0, 0, NULL, 0}};

sui_sxml_tag_info_child_t const sui_skt_wtable_children[] =
  {SUI_SXML_TAG_INFO_CHILD_WIDGET_BASE,
  {SUI_SKT_TABLE,    offsetof(sui_widget_t, data), offsetof(suiw_table_t, tdata), &sui_table_vmt_data, SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_VAR,      offsetof(sui_widget_t, data), offsetof(suiw_table_t, currow), &sui_dinfo_vmt_data, SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_VARCOL,   offsetof(sui_widget_t, data), offsetof(suiw_table_t, curcol), &sui_dinfo_vmt_data, SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_COLS,     offsetof(sui_widget_t, data), offsetof(suiw_table_t, cols), &sui_int_vmt_data, 0},
  {SUI_SKT_ROWS,     offsetof(sui_widget_t, data), offsetof(suiw_table_t, rows), &sui_int_vmt_data, 0},
  {SUI_SKT_HEIGHT,   offsetof(sui_widget_t, data), offsetof(suiw_table_t, htitle), &sui_coord_vmt_data, 0},
  {SUI_SXML_ENDOFCHILDREN, 0, 0, NULL, 0}};

sui_sxml_tag_info_child_t const sui_skt_wdyntext_children[] =
  {SUI_SXML_TAG_INFO_CHILD_WIDGET_BASE,
  {SUI_SKT_TEXT,   0, SUI_SXML_PROCESS_FCN, &sui_utf8_vmt_data, SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_VAR,    0, SUI_SXML_PROCESS_FCN, &sui_dinfo_vmt_data, SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_LIST,   0, SUI_SXML_PROCESS_FCN, SUI_OBJ_VMT_FROM_VMT(&sui_list_vmt_data), SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_CTIME,  0, SUI_SXML_PROCESS_FCN, &sui_long_vmt_data, 0},
  {SUI_SKT_DTIME,  0, SUI_SXML_PROCESS_FCN, &sui_dinfo_vmt_data, SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_DINDEX, 0, SUI_SXML_PROCESS_FCN, &sui_dinfo_vmt_data, SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_ALIGN,  offsetof(sui_widget_t, data), offsetof(suiw_dyntext_t, align), &sui_align_vmt_data, 0},
  {SUI_SKT_GAP,    offsetof(sui_widget_t, data), offsetof(suiw_dyntext_t, gap), &sui_coord_vmt_data, 0},
  {SUI_SXML_ENDOFCHILDREN, 0, 0, NULL, 0}};

sui_sxml_tag_info_child_t const sui_skt_wgraphxy_children[] =
  {SUI_SXML_TAG_INFO_CHILD_WIDGET_BASE,
  {SUI_SKT_GDATA,    offsetof(sui_widget_t, data), offsetof(suiw_graphxy_t, data), &sui_gdata_vmt_data, SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_POSX,     offsetof(sui_widget_t, data), offsetof(suiw_graphxy_t, curx), &sui_dinfo_vmt_data, SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_POSY,     offsetof(sui_widget_t, data), offsetof(suiw_graphxy_t, cury), &sui_dinfo_vmt_data, SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_ORIGINX,  offsetof(sui_widget_t, data), offsetof(suiw_graphxy_t, originx), &sui_dinfo_vmt_data, SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_ORIGINY,  offsetof(sui_widget_t, data), offsetof(suiw_graphxy_t, originy), &sui_dinfo_vmt_data, SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_RANGEX,   offsetof(sui_widget_t, data), offsetof(suiw_graphxy_t, rangex), &sui_dinfo_vmt_data, SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_RANGEY,   offsetof(sui_widget_t, data), offsetof(suiw_graphxy_t, rangey), &sui_dinfo_vmt_data, SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_GAP,      offsetof(sui_widget_t, data), offsetof(suiw_graphxy_t, gap), &sui_coord_vmt_data, 0},
  {SUI_SXML_ENDOFCHILDREN, 0, 0, NULL, 0}};


sui_sxml_tag_info_child_t const sui_skt_connections_children[] =
 {{SUI_SKT_LINK, 0, SUI_SXML_PROCESS_FCN, &sui_link_vmt_data, SUI_SXML_CHFLG_SUBTAGS},
  {SUI_SXML_ENDOFCHILDREN, 0, 0, NULL, 0}};

sui_sxml_tag_info_child_t const sui_skt_link_children[] =
 {{SUI_SKT_SOURCE,   0, offsetof(sui_sxml_link_t, source), &sui_utf8_vmt_data, SUI_SXML_CHFLG_POINTER | SUI_SXML_CHFLG_NO_OBJID},
  {SUI_SKT_SIGNAL,   0, offsetof(sui_sxml_link_t, signal), &sui_utf8_vmt_data, SUI_SXML_CHFLG_POINTER | SUI_SXML_CHFLG_NO_OBJID},
  {SUI_SKT_TARGET,   0, offsetof(sui_sxml_link_t, target), &sui_utf8_vmt_data, SUI_SXML_CHFLG_POINTER | SUI_SXML_CHFLG_NO_OBJID},
  {SUI_SKT_SLOT,     0, offsetof(sui_sxml_link_t, slot), &sui_utf8_vmt_data, SUI_SXML_CHFLG_POINTER | SUI_SXML_CHFLG_NO_OBJID},
  {SUI_SXML_ENDOFCHILDREN, 0, 0, NULL, 0}};

sui_sxml_tag_info_child_t const sui_skt_scenario_children[] =
 {//{SUI_SKT_SCREEN,   0, offsetof(sui_scenario_t, screen), &sui_screen_vmt_data, SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_STATE,    0, SUI_SXML_PROCESS_FCN, &sui_state_vmt_data, SUI_SXML_CHFLG_POINTER | SUI_SXML_CHFLG_SUBTAGS},
  {SUI_SKT_INIT,     0, offsetof(sui_scenario_t, init), &sui_utf8_vmt_data, SUI_SXML_CHFLG_POINTER | SUI_SXML_CHFLG_NO_OBJID},
  {SUI_SKT_TRANSITION, 0, SUI_SXML_PROCESS_FCN, &sui_transition_vmt_data, SUI_SXML_CHFLG_POINTER | SUI_SXML_CHFLG_SUBTAGS},
  {SUI_SKT_STATEEXT, 0, SUI_SXML_NO_ADD_TO_TAG, &sui_stateext_vmt_data, SUI_SXML_CHFLG_POINTER | SUI_SXML_CHFLG_SUBTAGS},
  {SUI_SXML_ENDOFCHILDREN, 0, 0, NULL, 0}};

sui_sxml_tag_info_child_t const sui_skt_state_children[] =
 {{SUI_SKT_NAME, 0, offsetof(sui_state_t, name), &sui_utf8_vmt_data, SUI_SXML_CHFLG_POINTER | SUI_SXML_CHFLG_NO_OBJID},
  {SUI_SKT_FILE, 0, offsetof(sui_state_t, nsname), &sui_utf8_vmt_data, SUI_SXML_CHFLG_POINTER | SUI_SXML_CHFLG_NO_OBJID},
  {SUI_SXML_ENDOFCHILDREN, 0, 0, NULL, 0}};

sui_sxml_tag_info_child_t const sui_skt_stateext_children[] =
 {//{SUI_SKT_NAME,       0, offsetof(sui_stateext_t, state), &sui_utf8_vmt_data, SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_SCREEN,     0, offsetof(sui_stateext_t, widget_root), &sui_utf8_vmt_data, SUI_SXML_CHFLG_POINTER | SUI_SXML_CHFLG_NO_OBJID},
  {SUI_SKT_INIT,       0, offsetof(sui_stateext_t, init_substate), &sui_utf8_vmt_data, SUI_SXML_CHFLG_POINTER | SUI_SXML_CHFLG_NO_OBJID},
  {SUI_SKT_FCN,        0, offsetof(sui_stateext_t, procs), &sui_utf8_vmt_data, SUI_SXML_CHFLG_POINTER | SUI_SXML_CHFLG_NO_OBJID},
  {SUI_SKT_TRANSITION, 0, SUI_SXML_PROCESS_FCN, &sui_transition_vmt_data, SUI_SXML_CHFLG_POINTER | SUI_SXML_CHFLG_SUBTAGS},
  {SUI_SKT_SUBSTATE,   0, SUI_SXML_PROCESS_FCN, &sui_substate_vmt_data, SUI_SXML_CHFLG_POINTER | SUI_SXML_CHFLG_SUBTAGS},
  {SUI_SXML_ENDOFCHILDREN, 0, 0, NULL, 0}};

sui_sxml_tag_info_child_t const sui_skt_substate_children[] =
 {{SUI_SKT_NAME,       0, offsetof(sui_substate_t, name), &sui_utf8_vmt_data, SUI_SXML_CHFLG_POINTER | SUI_SXML_CHFLG_NO_OBJID},
  {SUI_SKT_INIT,       0, offsetof(sui_substate_t, init_focus), &sui_utf8_vmt_data, SUI_SXML_CHFLG_POINTER | SUI_SXML_CHFLG_NO_OBJID},
  {SUI_SKT_TRANSITION, 0, SUI_SXML_PROCESS_FCN, &sui_transition_vmt_data, SUI_SXML_CHFLG_POINTER | SUI_SXML_CHFLG_SUBTAGS},
  {SUI_SKT_ENTRY,      0, SUI_SXML_NO_ADD_TO_TAG, NULL, 0},
  {SUI_SKT_ATFIRST,    0, SUI_SXML_NO_ADD_TO_TAG, NULL, 0},
//  {SUI_SKT_LOOP,       0, SUI_SXML_NO_ADD_TO_TAG, NULL, 0},
  {SUI_SKT_EXIT,       0, SUI_SXML_NO_ADD_TO_TAG, NULL, 0},

  {SUI_SKT_DIALOG,     0, SUI_SXML_PROCESS_FCN, &sui_ssdialog_vmt_data, SUI_SXML_CHFLG_POINTER | SUI_SXML_CHFLG_SUBTAGS},
  {SUI_SXML_ENDOFCHILDREN, 0, 0, NULL, 0}};

sui_sxml_tag_info_child_t const sui_skt_actionslot_children[] =
 {{SUI_SKT_ACTION, 0, SUI_SXML_PROCESS_FCN, &sui_action_vmt_data, SUI_SXML_CHFLG_SUBTAGS},
  {SUI_SXML_ENDOFCHILDREN, 0, 0, NULL, 0}};

sui_sxml_tag_info_child_t const sui_skt_condition_children[] =
 {{SUI_SKT_TIME,   0, offsetof(sui_condition_t, state_time), &sui_long_vmt_data, 0},
  {SUI_SKT_USERTIME,0,offsetof(sui_condition_t, user_time), &sui_long_vmt_data, 0},
  {SUI_SKT_TERM,   0, offsetof(sui_condition_t, str_term), &sui_utf8_vmt_data, SUI_SXML_CHFLG_POINTER | SUI_SXML_CHFLG_NO_OBJID},
  {SUI_SKT_WIDGET, 0, offsetof(sui_condition_t, str_focus), &sui_utf8_vmt_data, SUI_SXML_CHFLG_POINTER | SUI_SXML_CHFLG_NO_OBJID},
  {SUI_SKT_EVENT,  0, offsetof(sui_condition_t, event), &sui_ushort_vmt_data, 0},
  {SUI_SKT_EVCMD,  0, offsetof(sui_condition_t, evcmd), &sui_ushort_vmt_data, 0},
  {SUI_SKT_EVKEY,  0, offsetof(sui_condition_t, evkey), &sui_ushort_vmt_data, 0},
  {SUI_SKT_EVINFO, 0, SUI_SXML_PROCESS_FCN, &sui_long_vmt_data, 0}, /* process for flag setting */
  {SUI_SKT_STATE,  0, offsetof(sui_condition_t, lastst), &sui_utf8_vmt_data, SUI_SXML_CHFLG_POINTER | SUI_SXML_CHFLG_NO_OBJID},
  {SUI_SKT_SUBSTATE,0,offsetof(sui_condition_t, lastsubst), &sui_utf8_vmt_data, SUI_SXML_CHFLG_POINTER | SUI_SXML_CHFLG_NO_OBJID},
  {SUI_SXML_ENDOFCHILDREN, 0, 0, NULL, 0}};

sui_sxml_tag_info_child_t const sui_skt_action_children[] =
 {{SUI_SKT_CONDITION, 0, offsetof(sui_action_t, condition), &sui_condition_vmt_data, SUI_SXML_CHFLG_POINTER | SUI_SXML_CHFLG_SUBTAGS},
  {SUI_SKT_FCN,   0, offsetof(sui_action_t, function), &sui_action_fcn_vmt_data, 0}, //SUI_SXML_CHFLG_FUNCTION},
  {SUI_SKT_WDGSRC,0, offsetof(sui_action_t, wdg_src), SUI_OBJ_VMT_FROM_VMT(&sui_widget_vmt_data), SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_WDGDST,0, offsetof(sui_action_t, wdg_dst), SUI_OBJ_VMT_FROM_VMT(&sui_widget_vmt_data), SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_DISRC, 0, offsetof(sui_action_t, data_src), &sui_dinfo_vmt_data, SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_DIDST, 0, offsetof(sui_action_t, data_dst), &sui_dinfo_vmt_data, SUI_SXML_CHFLG_SUBTAGS | SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_TEXT,  0, offsetof(sui_action_t, text), &sui_utf8_vmt_data, SUI_SXML_CHFLG_POINTER | SUI_SXML_CHFLG_NO_OBJID},
  {SUI_SKT_LIST,  0, offsetof(sui_action_t, list), SUI_OBJ_VMT_FROM_VMT(&sui_list_vmt_data), SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_DATA,  0, offsetof(sui_action_t, value), &sui_long_vmt_data, 0},
  {SUI_SKT_FLAGS, 0, offsetof(sui_action_t, flags), &sui_ulong_vmt_data, 0},
  {SUI_SKT_EVCMD, 0, offsetof(sui_action_t, cmd), &sui_short_vmt_data, 0},
  {SUI_SXML_ENDOFCHILDREN, 0, 0, NULL, 0}};

sui_sxml_tag_info_child_t const sui_skt_transition_children[] =
 {{SUI_SKT_SCENARIO, 0, offsetof(sui_transition_t, scenario), &sui_utf8_vmt_data, SUI_SXML_CHFLG_POINTER | SUI_SXML_CHFLG_NO_OBJID},
  {SUI_SKT_STATE, 0, offsetof(sui_transition_t, tostate), &sui_utf8_vmt_data, SUI_SXML_CHFLG_POINTER | SUI_SXML_CHFLG_NO_OBJID},
  {SUI_SKT_SUBSTATE, 0, offsetof(sui_transition_t, tosubst), &sui_utf8_vmt_data, SUI_SXML_CHFLG_POINTER | SUI_SXML_CHFLG_NO_OBJID},
  {SUI_SKT_WIDGET, 0, offsetof(sui_transition_t, towidget), &sui_utf8_vmt_data, SUI_SXML_CHFLG_POINTER | SUI_SXML_CHFLG_NO_OBJID},
  {SUI_SKT_CONDITION, 0, offsetof(sui_transition_t, cond), &sui_condition_vmt_data, SUI_SXML_CHFLG_POINTER | SUI_SXML_CHFLG_SUBTAGS},
  {SUI_SKT_BEFORE, 0, SUI_SXML_NO_ADD_TO_TAG, NULL, 0},
  {SUI_SKT_AFTER, 0, SUI_SXML_NO_ADD_TO_TAG, NULL, 0},
  {SUI_SXML_ENDOFCHILDREN, 0, 0, NULL, 0}};

sui_sxml_tag_info_child_t const sui_skt_ssdialog_children[] =
 {{SUI_SKT_WIDGET, 0, offsetof(sui_ssdialog_t, dialog), SUI_OBJ_VMT_FROM_VMT(&sui_widget_vmt_data), SUI_SXML_CHFLG_POINTER},
  {SUI_SKT_CONDITION, 0, offsetof(sui_ssdialog_t, cond), &sui_condition_vmt_data, SUI_SXML_CHFLG_POINTER | SUI_SXML_CHFLG_SUBTAGS},
  {SUI_SKT_EXITCOND, 0, offsetof(sui_ssdialog_t, exitcond), &sui_condition_vmt_data, SUI_SXML_CHFLG_POINTER | SUI_SXML_CHFLG_SUBTAGS},
  {SUI_SKT_BEFORE, 0, SUI_SXML_NO_ADD_TO_TAG, NULL, 0},
  {SUI_SKT_AFTER, 0, SUI_SXML_NO_ADD_TO_TAG, NULL, 0},
  {SUI_SXML_ENDOFCHILDREN, 0, 0, NULL, 0}};

/******************************************************************************/
const sui_sxml_tag_info_t sxml_suitk_known_tags[] = {
  [SUI_SKT_SXML]      = {"SXML", sui_skt_sxml_children},

  [SUI_SKT_WIDGETSET] = {"widgetset", sui_skt_widgetset_children,},
  [SUI_SKT_OBJECTSET] = {"objectset", sui_skt_objectset_children,},
  [SUI_SKT_SCREENSET] = {"screenset", sui_skt_screenset_children,},
  [SUI_SKT_TITLE]     = {"title", NULL,},
  [SUI_SKT_AUTHOR]    = {"author", NULL,},
  [SUI_SKT_DATE]      = {"date", NULL,},
  [SUI_SKT_VERSION]   = {"version", NULL,},

  [SUI_SKT_COLOR]     = {"color", NULL,},
  [SUI_SKT_COLORTABLE]= {"colortable", sui_skt_colortable_children,},
  [SUI_SKT_FONT]      = {"font", sui_skt_font_children,},
  [SUI_SKT_STYLE]     = {"style", sui_skt_style_children,},
  [SUI_SKT_VAR]       = {"var", sui_skt_var_children,},
  [SUI_SKT_VARCOL]    = {"varcolumn", sui_skt_var_children,},
  [SUI_SKT_SUFFIX]    = {"suffix", sui_skt_suffix_children,},
  [SUI_SKT_LIST]      = {"list", sui_skt_list_children,},
  [SUI_SKT_KEY]       = {"key", sui_skt_key_children,},
  [SUI_SKT_KEYTABLE]  = {"keytable", sui_skt_keytable_children,},
  [SUI_SKT_IMAGE]     = {"image", sui_skt_image_children,},
  [SUI_SKT_FORMAT]    = {"format", sui_skt_format_children,},
  [SUI_SKT_TEXT]      = {"text", NULL,},
  [SUI_SKT_CHOICE]    = {"choice", sui_skt_choice_children,},
  [SUI_SKT_CHOICETABLE]={"choicetable", sui_skt_choicetable_children,},
  [SUI_SKT_WIDGET]    = {"widget", sui_skt_widget_children,},
//  [SUI_SKT_SCREEN]    = {"screen", sui_skt_screen_children,},
  [SUI_SKT_NAME]      = {"name", NULL,},
  [SUI_SKT_SIZE]      = {"size", NULL,},
  [SUI_SKT_FLAGS]     = {"flags", NULL,},
  [SUI_SKT_LABELFONT] = {"labfont", NULL,},
  [SUI_SKT_FRAME]     = {"frame", NULL,},
  [SUI_SKT_GAP]       = {"gap", NULL,},
  [SUI_SKT_TYPE]      = {"type", NULL,},
  [SUI_SKT_DATA]      = {"data", NULL,},
  [SUI_SKT_MIN]       = {"min", NULL,},
  [SUI_SKT_MAX]       = {"max", NULL,},
  [SUI_SKT_DIGITS]    = {"digits", NULL,},
  [SUI_SKT_MAXINDEX]  = {"maxidx", NULL,},
  [SUI_SKT_INIT]      = {"init", NULL},
  [SUI_SKT_UINIT]     = {"uinit", NULL}, /* init dinfo with unsigned value */
  [SUI_SKT_DECIMALALIGN]={"decalign", NULL,},
  [SUI_SKT_SUFFIXALIGN]={"sufalign", NULL,},
  [SUI_SKT_DECIMALFONT]={"decfont", NULL,},
  [SUI_SKT_SUFFIXFONT]= {"suffont", NULL,},
  [SUI_SKT_VALUE]     = {"value", NULL,},
  [SUI_SKT_KEYCODE]   = {"keycode", NULL,},
  [SUI_SKT_FCN]       = {"func", NULL,},
  [SUI_SKT_ARG]       = {"arg", NULL,},
  [SUI_SKT_FILE]      = {"file", NULL,},
  [SUI_SKT_BASE]      = {"base", NULL,},
  [SUI_SKT_MASK]      = {"mask", NULL,},
  [SUI_SKT_X]         = {"x", NULL,},
  [SUI_SKT_Y]         = {"y", NULL,},
  [SUI_SKT_WIDTH]     = {"width", NULL,},
  [SUI_SKT_HEIGHT]    = {"height", NULL,},
  [SUI_SKT_LABEL]     = {"label", NULL,},
  [SUI_SKT_LABALIGN]  = {"labalign", NULL,},
  [SUI_SKT_TIP]       = {"tip", NULL,},

  [SUI_SKT_WGROUP]    = {"wgroup", sui_skt_wgroup_children,},
  [SUI_SKT_WBUTTON]   = {"wbutton", sui_skt_wbutton_children,},
  [SUI_SKT_WBITMAP]   = {"wbitmap", sui_skt_wbitmap_children,},
  [SUI_SKT_WLISTBOX]  = {"wlistbox", sui_skt_wlistbox_children,},
  [SUI_SKT_WNUMBER]   = {"wnumber", sui_skt_wnumber_children,},
  [SUI_SKT_WSCROLL]   = {"wscroll", sui_skt_wscroll_children,},
  [SUI_SKT_WTEXT]     = {"wtext", sui_skt_wtext_children,},
  [SUI_SKT_WTIME]     = {"wtime", sui_skt_wtime_children,},
  [SUI_SKT_WTABLE]    = {"wtable", sui_skt_wtable_children},

  [SUI_SKT_ID]        = {"id", NULL,},
  [SUI_SKT_ALIGN]     = {"align", NULL,},
  [SUI_SKT_COLS]      = {"cols", NULL,},
  [SUI_SKT_ROWS]      = {"rows", NULL,},
  [SUI_SKT_LINE]      = {"line", NULL,},

  [SUI_SKT_TABLE]     = {"table", sui_skt_table_children},
  [SUI_SKT_COLUMN]    = {"column", sui_skt_column_children},
  [SUI_SKT_SPAN]      = {"span", sui_skt_var_children},

  [SUI_SKT_COUNT]     = {"count", NULL},

  [SUI_SKT_DAYS]      = {"dayname", NULL,},
  [SUI_SKT_MONTHS]    = {"monthname", NULL,},
  [SUI_SKT_AMPM]      = {"ampmname", NULL,},

  [SUI_SKT_CONNECTIONS]={"connections", sui_skt_connections_children},
  [SUI_SKT_LINK]      = {"link", sui_skt_link_children},
  [SUI_SKT_SOURCE]    = {"source", NULL},
  [SUI_SKT_SIGNAL]    = {"signal", NULL},
  [SUI_SKT_TARGET]    = {"target", NULL},
  [SUI_SKT_SLOT]      = {"slot", NULL},

  [SUI_SKT_SCENARIO]  = {"scenario", sui_skt_scenario_children},
  [SUI_SKT_STATE]     = {"state", sui_skt_state_children},
  [SUI_SKT_STATEEXT]  = {"stateext", sui_skt_stateext_children},
  [SUI_SKT_SUBSTATE]  = {"substate", sui_skt_substate_children},
  [SUI_SKT_SCREEN]    = {"screen", NULL},

  [SUI_SKT_TRANSITION]= {"transition", sui_skt_transition_children},
  [SUI_SKT_CONDITION] = {"condition", sui_skt_condition_children},
  [SUI_SKT_EXITCOND]  = {"exitcondition", sui_skt_condition_children},
  [SUI_SKT_TIME]      = {"time", NULL},
  [SUI_SKT_USERTIME]  = {"usertime", NULL},
  [SUI_SKT_TERM]      = {"term", NULL},
  [SUI_SKT_EVENT]     = {"events", NULL},
  [SUI_SKT_EVCMD]     = {"eventcmd", NULL},
  [SUI_SKT_EVKEY]     = {"eventkey", NULL},
  [SUI_SKT_EVINFO]    = {"eventinfo", NULL},

  [SUI_SKT_ENTRY]     = {"entry", sui_skt_actionslot_children},
  [SUI_SKT_LOOP]      = {"loop", sui_skt_actionslot_children},
  [SUI_SKT_EXIT]      = {"exit", sui_skt_actionslot_children},
  [SUI_SKT_ATFIRST]   = {"atfirst", sui_skt_actionslot_children},
  [SUI_SKT_BEFORE]    = {"before", sui_skt_actionslot_children},
  [SUI_SKT_AFTER]     = {"after", sui_skt_actionslot_children},

  [SUI_SKT_ACTION]    = {"action", sui_skt_action_children},
  [SUI_SKT_WDGSRC]    = {"fromwidget", NULL},
  [SUI_SKT_WDGDST]    = {"towidget", NULL},
  [SUI_SKT_DISRC]     = {"fromvar", sui_skt_var_children},
  [SUI_SKT_DIDST]     = {"tovar", sui_skt_var_children},

  [SUI_SKT_DIALOG]    = {"dialog", sui_skt_ssdialog_children},
  [SUI_SKT_STATUS]    = {"status", sui_skt_widget_children},

  [SUI_SKT_VARF]      = {"formvar", sui_skt_varf_children},
  [SUI_SKT_VARL]      = {"listvar", sui_skt_varl_children},
  [SUI_SKT_LIPRINT]   = {"liprint", sui_skt_list_children},

  [SUI_SKT_WDYNTEXT]  = {"wdyntext", sui_skt_wdyntext_children},
  [SUI_SKT_CTIME]     = {"ctime", NULL},
  [SUI_SKT_DTIME]     = {"dtime", sui_skt_var_children},
  [SUI_SKT_DINDEX]    = {"dindex", sui_skt_var_children},

  [SUI_SKT_PRNFILTER] = {"prnfilter", sui_skt_prnfilter_children},
  [SUI_SKT_PRNLAYOUT] = {"prnlayout", sui_skt_prnlayout_children},
  [SUI_SKT_DEVICE]    = {"device", NULL},

  [SUI_SKT_WGRAPHXY]  = {"wgraphxy", sui_skt_wgraphxy_children},
  [SUI_SKT_GDATA]     = {"gdata", sui_skt_gdata_children},
  [SUI_SKT_GSET]      = {"gset", sui_skt_gset_children},

  [SUI_SKT_POSX]      = {"posx", sui_skt_var_children},
  [SUI_SKT_POSY]      = {"posy", sui_skt_var_children},
  [SUI_SKT_ORIGINX]   = {"originx", sui_skt_var_children},
  [SUI_SKT_ORIGINY]   = {"originy", sui_skt_var_children},
  [SUI_SKT_RANGEX]    = {"rangex", sui_skt_var_children},
  [SUI_SKT_RANGEY]    = {"rangey", sui_skt_var_children},
  [SUI_SKT_OFFSET]    = {"offset", NULL},
  [SUI_SKT_STEP]      = {"step", NULL},
};

/******************************************************************************/
/* Search tag by name */

#if 1
/* Fast binary search for tags */
#include <ul_gsacust.h>

/* Process sui_sxml_known_tags entries by next command */
/* 
   sed -n -e 's/^[ \t]*\[\([^]]*\)\][ \]*= *{ *"\([^"]\+\)".*$/\2 \1/p' | \
   sort | sed -n -e 's/^[^ ]* \([^ ]*\)$/  \&sui_sxml_known_tags[\1],/p'
*/

const sui_sxml_tag_info_t * const
sui_sxml_tags_by_name_array[]={
  &sxml_suitk_known_tags[SUI_SKT_SXML],
  &sxml_suitk_known_tags[SUI_SKT_ACTION],
  &sxml_suitk_known_tags[SUI_SKT_AFTER],
  &sxml_suitk_known_tags[SUI_SKT_ALIGN],
  &sxml_suitk_known_tags[SUI_SKT_AMPM],
  &sxml_suitk_known_tags[SUI_SKT_ARG],
  &sxml_suitk_known_tags[SUI_SKT_ATFIRST],
  &sxml_suitk_known_tags[SUI_SKT_AUTHOR],
  &sxml_suitk_known_tags[SUI_SKT_BASE],
  &sxml_suitk_known_tags[SUI_SKT_BEFORE],
  &sxml_suitk_known_tags[SUI_SKT_CHOICE],
  &sxml_suitk_known_tags[SUI_SKT_CHOICETABLE],
  &sxml_suitk_known_tags[SUI_SKT_COLOR],
  &sxml_suitk_known_tags[SUI_SKT_COLORTABLE],
  &sxml_suitk_known_tags[SUI_SKT_COLS],
  &sxml_suitk_known_tags[SUI_SKT_COLUMN],
  &sxml_suitk_known_tags[SUI_SKT_CONDITION],
  &sxml_suitk_known_tags[SUI_SKT_CONNECTIONS],
  &sxml_suitk_known_tags[SUI_SKT_COUNT],
  &sxml_suitk_known_tags[SUI_SKT_CTIME],
  &sxml_suitk_known_tags[SUI_SKT_DATA],
  &sxml_suitk_known_tags[SUI_SKT_DATE],
  &sxml_suitk_known_tags[SUI_SKT_DAYS],
  &sxml_suitk_known_tags[SUI_SKT_DECIMALALIGN],
  &sxml_suitk_known_tags[SUI_SKT_DECIMALFONT],
  &sui_sxml_common_known_tags[SUI_SKT_DEFINE],
  &sxml_suitk_known_tags[SUI_SKT_DEVICE],
  &sxml_suitk_known_tags[SUI_SKT_DIALOG],
  &sxml_suitk_known_tags[SUI_SKT_DIGITS],
  &sxml_suitk_known_tags[SUI_SKT_DINDEX],
  &sui_sxml_common_known_tags[SUI_SKT_DIV],
  &sxml_suitk_known_tags[SUI_SKT_DTIME],
  &sxml_suitk_known_tags[SUI_SKT_ENTRY],
  &sxml_suitk_known_tags[SUI_SKT_EVCMD],
  &sxml_suitk_known_tags[SUI_SKT_EVINFO],
  &sxml_suitk_known_tags[SUI_SKT_EVKEY],
  &sxml_suitk_known_tags[SUI_SKT_EVENT],
  &sxml_suitk_known_tags[SUI_SKT_EXIT],
  &sxml_suitk_known_tags[SUI_SKT_EXITCOND],
  &sxml_suitk_known_tags[SUI_SKT_FILE],
  &sxml_suitk_known_tags[SUI_SKT_FLAGS],
  &sxml_suitk_known_tags[SUI_SKT_FONT],
  &sxml_suitk_known_tags[SUI_SKT_FORMAT],
  &sxml_suitk_known_tags[SUI_SKT_VARF],
  &sxml_suitk_known_tags[SUI_SKT_FRAME],
  &sxml_suitk_known_tags[SUI_SKT_DISRC],
  &sxml_suitk_known_tags[SUI_SKT_WDGSRC],
  &sxml_suitk_known_tags[SUI_SKT_FCN],
  &sxml_suitk_known_tags[SUI_SKT_GAP],
  &sxml_suitk_known_tags[SUI_SKT_GDATA],
  &sxml_suitk_known_tags[SUI_SKT_GSET],
  &sxml_suitk_known_tags[SUI_SKT_HEIGHT],
  &sxml_suitk_known_tags[SUI_SKT_ID],
  &sxml_suitk_known_tags[SUI_SKT_IMAGE],
  &sui_sxml_common_known_tags[SUI_SKT_INCLUDE],
  &sxml_suitk_known_tags[SUI_SKT_INIT],
  &sxml_suitk_known_tags[SUI_SKT_KEY],
  &sxml_suitk_known_tags[SUI_SKT_KEYCODE],
  &sxml_suitk_known_tags[SUI_SKT_KEYTABLE],
  &sxml_suitk_known_tags[SUI_SKT_LABALIGN],
  &sxml_suitk_known_tags[SUI_SKT_LABEL],
  &sxml_suitk_known_tags[SUI_SKT_LABELFONT],
  &sxml_suitk_known_tags[SUI_SKT_LINE],
  &sxml_suitk_known_tags[SUI_SKT_LINK],
  &sxml_suitk_known_tags[SUI_SKT_LIPRINT],
  &sxml_suitk_known_tags[SUI_SKT_LIST],
  &sxml_suitk_known_tags[SUI_SKT_VARL],
  &sxml_suitk_known_tags[SUI_SKT_LOOP],
  &sxml_suitk_known_tags[SUI_SKT_MASK],
  &sxml_suitk_known_tags[SUI_SKT_MAX],
  &sxml_suitk_known_tags[SUI_SKT_MAXINDEX],
  &sxml_suitk_known_tags[SUI_SKT_MIN],
  &sxml_suitk_known_tags[SUI_SKT_MONTHS],
  &sxml_suitk_known_tags[SUI_SKT_NAME],
  &sxml_suitk_known_tags[SUI_SKT_OBJECTSET],
  &sxml_suitk_known_tags[SUI_SKT_OFFSET],
  &sxml_suitk_known_tags[SUI_SKT_ORIGINX],
  &sxml_suitk_known_tags[SUI_SKT_ORIGINY],
  &sxml_suitk_known_tags[SUI_SKT_POSX],
  &sxml_suitk_known_tags[SUI_SKT_POSY],
  &sxml_suitk_known_tags[SUI_SKT_PRNFILTER],
  &sxml_suitk_known_tags[SUI_SKT_PRNLAYOUT],
  &sxml_suitk_known_tags[SUI_SKT_RANGEX],
  &sxml_suitk_known_tags[SUI_SKT_RANGEY],
  &sxml_suitk_known_tags[SUI_SKT_ROWS],
  &sxml_suitk_known_tags[SUI_SKT_SCENARIO],
  &sxml_suitk_known_tags[SUI_SKT_SCREEN],
  &sxml_suitk_known_tags[SUI_SKT_SCREENSET],
  &sxml_suitk_known_tags[SUI_SKT_SIGNAL],
  &sxml_suitk_known_tags[SUI_SKT_SIZE],
  &sxml_suitk_known_tags[SUI_SKT_SLOT],
  &sxml_suitk_known_tags[SUI_SKT_SOURCE],
  &sxml_suitk_known_tags[SUI_SKT_SPAN],
  &sxml_suitk_known_tags[SUI_SKT_STATE],
  &sxml_suitk_known_tags[SUI_SKT_STATEEXT],
  &sxml_suitk_known_tags[SUI_SKT_STATUS],
  &sxml_suitk_known_tags[SUI_SKT_STEP],
  &sxml_suitk_known_tags[SUI_SKT_STYLE],
  &sxml_suitk_known_tags[SUI_SKT_SUBSTATE],
  &sxml_suitk_known_tags[SUI_SKT_SUFFIXALIGN],
  &sxml_suitk_known_tags[SUI_SKT_SUFFIX],
  &sxml_suitk_known_tags[SUI_SKT_SUFFIXFONT],
  &sxml_suitk_known_tags[SUI_SKT_TARGET],
  &sxml_suitk_known_tags[SUI_SKT_TERM],
  &sxml_suitk_known_tags[SUI_SKT_TEXT],
  &sxml_suitk_known_tags[SUI_SKT_TIME],
  &sxml_suitk_known_tags[SUI_SKT_TIP],
  &sxml_suitk_known_tags[SUI_SKT_TITLE],
  &sxml_suitk_known_tags[SUI_SKT_DIDST],
  &sxml_suitk_known_tags[SUI_SKT_WDGDST],
  &sxml_suitk_known_tags[SUI_SKT_TABLE],
  &sxml_suitk_known_tags[SUI_SKT_TRANSITION],
  &sxml_suitk_known_tags[SUI_SKT_TYPE],
  &sxml_suitk_known_tags[SUI_SKT_UINIT],
  &sxml_suitk_known_tags[SUI_SKT_USERTIME],
  &sxml_suitk_known_tags[SUI_SKT_VALUE],
  &sxml_suitk_known_tags[SUI_SKT_VAR],
  &sxml_suitk_known_tags[SUI_SKT_VARCOL],
  &sxml_suitk_known_tags[SUI_SKT_VERSION],
  &sxml_suitk_known_tags[SUI_SKT_WBITMAP],
  &sxml_suitk_known_tags[SUI_SKT_WBUTTON],
  &sxml_suitk_known_tags[SUI_SKT_WDYNTEXT],
  &sxml_suitk_known_tags[SUI_SKT_WGRAPHXY],
  &sxml_suitk_known_tags[SUI_SKT_WGROUP],
  &sxml_suitk_known_tags[SUI_SKT_WIDGET],
  &sxml_suitk_known_tags[SUI_SKT_WIDGETSET],
  &sxml_suitk_known_tags[SUI_SKT_WIDTH],
  &sxml_suitk_known_tags[SUI_SKT_WLISTBOX],
  &sxml_suitk_known_tags[SUI_SKT_WNUMBER],
  &sxml_suitk_known_tags[SUI_SKT_WSCROLL],
  &sxml_suitk_known_tags[SUI_SKT_WTABLE],
  &sxml_suitk_known_tags[SUI_SKT_WTEXT],
  &sxml_suitk_known_tags[SUI_SKT_WTIME],
  &sxml_suitk_known_tags[SUI_SKT_X],
  &sxml_suitk_known_tags[SUI_SKT_Y]
};

typedef struct sui_sxml_tags_by_name_t {
  /*gsa_static_array_field_t domains;*/
  struct {
    const sui_sxml_tag_info_t * const* items;
    int count;
  } tags;
} sui_sxml_tags_by_name_t;

typedef const char *sui_sxml_tags_by_name_key_t;

/* Custom array declarations */

GSA_CONST_CUST_DEC(sui_sxml_tags_by_name, sui_sxml_tags_by_name_t, const sui_sxml_tag_info_t,
        const sui_sxml_tags_by_name_key_t, tags, tag, sui_sxml_tags_by_name_cmp_fnc);

static inline int
sui_sxml_tags_by_name_cmp_fnc(const sui_sxml_tags_by_name_key_t *a, const sui_sxml_tags_by_name_key_t *b)
{
  return strcmp(*a,*b);
}

GSA_CONST_CUST_IMP(sui_sxml_tags_by_name, sui_sxml_tags_by_name_t, const sui_sxml_tag_info_t,
        const sui_sxml_tags_by_name_key_t, tags, tag, sui_sxml_tags_by_name_cmp_fnc, 0);

const sui_sxml_tags_by_name_t sui_sxml_tags_by_name={
  {
    .items=sui_sxml_tags_by_name_array,
    .count=sizeof(sui_sxml_tags_by_name_array)/sizeof(*sui_sxml_tags_by_name_array)
  }
};

/**
 * sui_sxml_find_taginfo_by_name - Find tag_info structure according to tag name
 * @tagname: Pointer to a tag name.
 *
 * The function tries to find and return pointer to sui_sxml_tag_info structure, which describes
 * tag and its dependencies.
 *
 * Return Value: The function returns pointer to sui_sxml_tag_info structure or NULL.
 *
 * File: sxml_suitk.c
 */
const sui_sxml_tag_info_t *sui_sxml_find_taginfo_by_name(sui_sxml_doc_description_t *ddesc, const char *tagname)
{
  return sui_sxml_tags_by_name_find(&sui_sxml_tags_by_name,&tagname);
}

#else

/* Original slower but proven search for tags */
const sui_sxml_tag_info_t *sui_sxml_find_taginfo_by_name(sui_sxml_doc_description_t *ddesc, const char *tagname)
{
  int i;
  for (i=0; i < ddesc->idsize; i++) {
    if ((ddesc->tags+i)->tag && !strcmp((ddesc->tags+i)->tag, tagname))
      return (ddesc->tags+i);
  }
  return NULL;
}

#endif




/******************************************************************************/
/**
 * sxml_suitk_special_process - Process all tags which haven't set process function
 * @doc: Pointer to a sui_sxml_doc structure which describes sxml documents.
 * @tag: Pointer to a sui_sxml_tag structure for processing.
 *
 * The function processes tag and its value. It is used for tags which cannot be
 * processed by common universal function.
 *
 * Return Value: The function returns SUI_SXML_ERR_OK as success, SUI_SXML_ERR_... code as warning,
 * and -SUI_SXML_ERR_... (negative value) as critical error.
 *
 * File: sui_sxml.c
 */
int sxml_suitk_special_process(sui_sxml_doc_t *doc, sui_sxml_tag_t *tag)
{
  sui_sxml_tag_t *parent = sui_sxml_get_prev_tag(tag);
  int ret = SUI_SXML_ERR_OK;

  switch (parent->id) {
    case SUI_SKT_COLORTABLE:
      if (tag->id == SUI_SKT_COLOR)
        sui_color_add_to_colortable(parent->obj, *(sui_color_t *)tag->obj);
//        tag->flags |= SUI_SXML_FLG_SAVE_IN_PAR; // only for referenced values, it isn't for direct value
      else
        ret = -SUI_SXML_ERR_ADD2ARRAY;
      break;

    case SUI_SKT_VAR:
    case SUI_SKT_VARCOL:
    case SUI_SKT_SPAN:
    case SUI_SKT_DTIME:
    case SUI_SKT_DINDEX:
    case SUI_SKT_POSX:
    case SUI_SKT_POSY:
    case SUI_SKT_ORIGINX:
    case SUI_SKT_ORIGINY:
    case SUI_SKT_RANGEX:
    case SUI_SKT_RANGEY:
    case SUI_SKT_DISRC:
    case SUI_SKT_DIDST:
      if (tag->id == SUI_SKT_TYPE) {/* tag isn't full tag, but it is only special structure with 'id' and 'obj' to its type */
        sui_dinfo_t *di = parent->obj;
        di->tinfo = *(sui_typeid_t *)tag->obj;
        if (!di->rdval && !di->wrval) {
          switch (di->tinfo) {
            case SUI_TYPE_CHAR:   di->rdval=sui_char_rdval; di->wrval=sui_char_wrval; di->ptr = &di->info; break;
            case SUI_TYPE_UCHAR:  di->rdval=sui_uchar_rdval; di->wrval=sui_uchar_wrval; di->ptr = &di->info; break;
            case SUI_TYPE_SHORT:  di->rdval=sui_short_rdval; di->wrval=sui_short_wrval; di->ptr = &di->info; break;
            case SUI_TYPE_USHORT: di->rdval=sui_ushort_rdval; di->wrval=sui_ushort_wrval; di->ptr = &di->info; break;
            case SUI_TYPE_INT:    di->rdval=sui_int_rdval; di->wrval=sui_int_wrval; di->ptr = &di->info; break;
            case SUI_TYPE_UINT:   di->rdval=sui_uint_rdval; di->wrval=sui_uint_wrval; di->ptr = &di->info; break;
            case SUI_TYPE_LONG:   di->rdval=sui_long_rdval; di->wrval=sui_long_wrval; di->ptr = &di->info; break;
            case SUI_TYPE_ULONG:  di->rdval=sui_ulong_rdval; di->wrval=sui_ulong_wrval; di->ptr = &di->info; break;
            case SUI_TYPE_UTF8:   di->rdval=sui_utf8_rdval; di->wrval=sui_utf8_wrval;
                                  di->hevent = sui_dinfo_hevent_utf8; di->evmask |= SUEV_COMMAND; break;
          }
        }
      } else if (tag->id == SUI_SKT_INIT) {
//        ul_logdeb("initiate LONG data...\n");
        sui_dinfo_t *di = parent->obj;
        if (di) {
          if (di->tinfo==SUI_TYPE_UNKNOWN) {
            di->tinfo=SUI_TYPE_LONG; di->rdval=sui_long_rdval; di->wrval=sui_long_wrval; di->ptr = &di->info;
          }
          ret = sui_wr_long(di, 0, tag->obj);
          if (ret) {
            ul_logerr("Conversion (VI) error...\n");
            ret = SUI_SXML_ERR_NOPROCESS;
          }
        } else {
          ul_logerr("tag object isn't prepared (VI)\n");
          ret = SUI_SXML_ERR_NOPROCESS;
        }
      } else if (tag->id == SUI_SKT_UINIT) {
//        ul_logdeb("initiate ULONG data...\n");
        sui_dinfo_t *di = parent->obj;
        if (di) {
          if (di->tinfo==SUI_TYPE_UNKNOWN) { /* uinit is an unsigned long value */
            di->tinfo=SUI_TYPE_ULONG; di->rdval=sui_ulong_rdval; di->wrval=sui_ulong_wrval; di->ptr = &di->info;
          }
          ret = sui_wr_ulong(di, 0, tag->obj);
          if (ret) {
            ul_logerr("Conversion (VUI) error...\n");
            ret = SUI_SXML_ERR_NOPROCESS;
          }
        } else {
          ul_logerr("tag object isn't prepared (VUI)\n");
          ret = SUI_SXML_ERR_NOPROCESS;
        }
      } else if (tag->id == SUI_SKT_TEXT) {
//        ul_logdeb("initiate UTF8 data...\n");
        sui_dinfo_t *di = parent->obj;
        if (di) {
          if (di->tinfo==SUI_TYPE_UNKNOWN) {
            di->tinfo=SUI_TYPE_UTF8; di->rdval=sui_utf8_rdval; di->wrval=sui_utf8_wrval;
            di->hevent = sui_dinfo_hevent_utf8; di->evmask |= SUEV_COMMAND;
          }
          if (di->tinfo==SUI_TYPE_UTF8) {
            ret = sui_wr_utf8(di, 0, (utf8 *const *)&tag->obj);
            if (ret)
              ul_logerr("Conversion (VAR) error...\n");
          } else {
            ul_logerr("VAR data wasn't initiated\n");
            ret = SUI_SXML_ERR_NOPROCESS;
          }
        } else {
          ul_logerr("tag object isn't prepared (VU8)\n");
          ret = SUI_SXML_ERR_NOPROCESS;
        }
      } else
        ret = -SUI_SXML_ERR_NOPROCESS;
      break;

    case SUI_SKT_IMAGE:
      if (tag->id == SUI_SKT_FILE) {/* TODO: this is only for images in files, memory isn't supported */
        sui_image_t *img = parent->obj;
        sui_file_source_t *imgsource;
        char *imgfile = NULL;
        long imgfilesize = 0;
        sui_file_type type = 0;
        int rft = sui_file_srctable_find(sui_utf8_get_text(tag->obj), sui_utf8_length(tag->obj),
                                         doc->source->source.file.filename, -1,
                                         &imgfile, &imgfilesize, &type);
        if (rft<0) return -SUI_SXML_ERR_NOFILE;
//        ul_logdeb("get image from file '%s'\n", imgfile);
        imgsource = sui_file_open(imgfile, imgfilesize, NULL, SUI_FM_READ);
        if (rft>0) sui_free(imgfile);

        if (!imgsource) {
          ret = -SUI_SXML_ERR_NOFILE;
        } else {
          sui_file_assign_type(imgsource, type);
          if (type==(SUI_FILE_TGRP_IMAGE|SUI_FILE_TID_BMP)) {
            ret = sui_file_load_bmp_image(imgsource, img, tag->flags & SUI_SXML_FLG_USER_ATTR1); /* load BMP image */
          } else if (type==(SUI_FILE_TGRP_IMAGE|SUI_FILE_TID_PCX)) {
            ret = sui_file_pcx_load_image(imgsource, img, tag->flags & SUI_SXML_FLG_USER_ATTR1); /* load PCX image */
          } else
            ret = -SUI_SXML_ERR_UNKNOWN;
          imgsource = sui_file_close(imgsource); /* filename is released inside sui_file_close ... */
        }
      } else if (tag->id == SUI_SKT_COUNT) {
        sui_image_t *img = parent->obj;
        int cnt = *(int *)tag->obj;
        if (img->height && cnt) {
          img->count = cnt;
          img->height /= cnt;
        } else
          ret = -SUI_SXML_ERR_BADARGS;
      } else
        ret = -SUI_SXML_ERR_NOPROCESS;
      break;

    case SUI_SKT_LIPRINT:
    case SUI_SKT_LIST:
      if ((tag->id == SUI_SKT_TEXT) || (tag->id == SUI_SKT_VARF) ||
          (tag->id == SUI_SKT_VARL) || (tag->id == SUI_SKT_LIPRINT)) {
        sui_listcoord_t idx = -1;
        sui_list_item_t *li;
        switch (tag->id) {
          case SUI_SKT_TEXT:
            li = sui_list_item_create(tag->obj, NULL, NULL, NULL);
            break;
          case SUI_SKT_LIPRINT:
            li = sui_list_item_create(NULL, NULL, NULL, tag->obj);
            break;
          case SUI_SKT_VARF:
            {
              sui_varf_t *varf = (sui_varf_t *)tag->obj;
              li = sui_list_item_create(NULL, varf->di, varf->fi, NULL);
            }
            break;
          case SUI_SKT_VARL:
            {
              sui_varl_t *varl = (sui_varl_t *)tag->obj;
              li = sui_list_item_create(NULL, varl->di, NULL, varl->li);
            }
            break;
          default:
            li = NULL; break;
        }
        if (li) {
          if (tag->flags & SUI_SXML_FLG_USER_SPECFLG) {
            li->flags |= SULI_INDEXED;
          }
          if (tag->flags & SUI_SXML_FLG_USER_VALUE) {
            idx = tag->user;
            tag->flags &= ~SUI_SXML_FLG_USER_VALUE; /* use this flag only one */
          }
          if (sui_list_item_add(parent->obj, li, idx)<0)
            ret = -SUI_SXML_ERR_ADD2ARRAY;
          else {
//ul_logdeb("Added item (%p) with index %d to list (%p)\n", li, idx, parent->obj);
            li->index = li->idx;
          }
        } else
          ret = -SUI_SXML_ERR_TYPECONV;
      } else
        ret = -SUI_SXML_ERR_NOPROCESS;
      break;

    case SUI_SKT_TABLE:
      if (tag->id==SUI_SKT_COLUMN) {
        if (sui_table_add_column(parent->obj, tag->obj, -1) < 0)
          ret = -SUI_SXML_ERR_MEMORY;
      } else
        ret = -SUI_SXML_ERR_NOPROCESS;
      break;

    case SUI_SKT_GDATA:
      if (tag->id==SUI_SKT_GSET) {
        if (sui_graph_data_add_dset(parent->obj, tag->obj) < 0)
          ret = -SUI_SXML_ERR_MEMORY;
      } else
        ret = -SUI_SXML_ERR_NOPROCESS;
      break;
    case SUI_SKT_WTEXT:
      if (tag->id == SUI_SKT_TEXT) {
//ul_logdeb("TEXT to WTEXT...\n");
        sui_dinfo_t *dit = sui_dinfo_create_utf8(tag->obj);
        if (dit) {
          suiw_text_t *wtxt = ((sui_widget_t *)parent->obj)->data;
          if (wtxt->di) sui_dinfo_dec_refcnt(wtxt->di);
          wtxt->di = dit;
          // sui_dinfo_inc_refcnt(dit);
        } else
          ret = -SUI_SXML_ERR_TYPECONV;
      } else
        ret = -SUI_SXML_ERR_NOPROCESS;
      break;

    case SUI_SKT_WDYNTEXT:
      {
        switch (tag->id) {
          case SUI_SKT_TEXT:
            if (sui_wdyntext_set_datasrc(parent->obj, tag->obj, SUYF_SRC_UTF8) == SUI_RET_ERR)
              ret = -SUI_SXML_ERR_TYPECONV;
            break;
          case SUI_SKT_VAR:
            if (sui_wdyntext_set_datasrc(parent->obj, tag->obj, SUYF_SRC_DINFO) == SUI_RET_ERR)
              ret = -SUI_SXML_ERR_TYPECONV;
            break;
          case SUI_SKT_LIST:
            if (sui_wdyntext_set_datasrc(parent->obj, tag->obj, SUYF_SRC_LIST) == SUI_RET_ERR)
              ret = -SUI_SXML_ERR_TYPECONV;
            break;
          case SUI_SKT_CTIME:
            if (sui_wdyntext_set_timesrc(parent->obj, NULL, *(long *)tag->obj) == SUI_RET_ERR)
              ret = -SUI_SXML_ERR_TYPECONV;
              break;
          case SUI_SKT_DTIME:
            if (sui_wdyntext_set_timesrc(parent->obj, tag->obj, 0) == SUI_RET_ERR)
              ret = -SUI_SXML_ERR_TYPECONV;
              break;
          case SUI_SKT_DINDEX:
              {
                sui_widget_t *widget = parent->obj;
                suiw_dyntext_t *wdyn = ((sui_widget_t *)parent->obj)->data;
                if ((widget->flags & SUYF_INDEXMASK) == SUYF_EXTINDEX) {
                  sui_dinfo_dec_refcnt(wdyn->index.external);
                  wdyn->index.external = NULL;
                }
                sui_dinfo_inc_refcnt(tag->obj);
                wdyn->index.external = tag->obj;
                widget->flags = (widget->flags & ~SUYF_INDEXMASK) | SUYF_EXTINDEX;
              }
              break;
          default:
            ret = -SUI_SXML_ERR_NOPROCESS;
            break;
        }
      }
      break;

    case SUI_SKT_KEYTABLE:
      if (tag->id == SUI_SKT_KEY) {
        sui_key_table_t *ktab = parent->obj;

        sui_key_action_t *key = sui_add_item_to_array((void **)((void *)&(ktab->actions)),
                        sizeof(sui_key_action_t), &ktab->actcnt, &ktab->maxcnt);
        if (key) {
//ul_logdeb("add key to table\n");
          memcpy(key, tag->obj, sizeof(sui_key_action_t));
        } else
          ret = -SUI_SXML_ERR_MEMORY;
      } else if (tag->id == SUI_SKT_KEYTABLE) {
        sui_key_table_t *ktab = parent->obj;
        sui_key_table_t *kadd = tag->obj;
        if (ktab && kadd) {
          if (ktab->next) sui_key_table_dec_refcnt(ktab->next);
          sui_key_table_inc_refcnt(kadd);
          ktab->next = kadd;
        } else
          ret = -SUI_SXML_ERR_NOPROCESS;
      } else
        ret = -SUI_SXML_ERR_NOPROCESS;
      break;

    case SUI_SKT_CHOICETABLE:
      if (tag->id == SUI_SKT_CHOICE) {
//ul_logdeb("add choice to table\n");
        if (sui_choice_add_to_choicetable(parent->obj, tag->obj))
          ret = -SUI_SXML_ERR_MEMORY;
      } else
        ret = -SUI_SXML_ERR_NOPROCESS;
      break;
    case SUI_SKT_WGROUP:

      if (tag->vmt && 
         (sui_obj_dynamic_cast_vmt((sui_obj_vmt_t*)&sui_widget_vmt_data, parent->obj, tag->vmt) != NULL)) {
//ul_logdeb("Add to group tid=%d\n",tag->id);
        if (sui_group_add_widget(parent->obj, tag->obj, SUGO_FIRST))
          ret = SUI_SXML_ERR_ADDWIDGET;
        if (tag->id == SUI_SKT_STATUS) {/* set widget as group status */
          if (sui_group_set_status(parent->obj, tag->obj))
            ret = SUI_SXML_ERR_ADDWIDGET;
        }
      } else
        ret = -SUI_SXML_ERR_NOPROCESS;
      break;
    case SUI_SKT_CONNECTIONS:
      if (!tag->obj) {
        ret = -SUI_SXML_ERR_MISSTAG;
      } else {
        sui_sxml_link_t *link = tag->obj;
        sui_signalid_t signalid = 0;
        sui_slotid_t slotid = 0;
        ns_object_t *objsrc, *objdst;
        ns_subns_t *subns;
        
        if (!link->source || !link->signal)
          return -SUI_SXML_ERR_MISSTAG;

        subns=NULL;
        objsrc = ns_find_any_object(sui_utf8_get_text(link->source), &subns);
        if (!objsrc) return -SUI_SXML_ERR_NONSOBJ;

        signalid = objsrc->vmt->vmt_find_signalid_byname((sui_obj_t*)objsrc->ptr,
              sui_utf8_get_text(link->signal), sui_utf8_length(link->signal));

        if (signalid < 0) {
          ul_logerr("... no \"%s\" signal\n", sui_utf8_get_text(link->signal));
          return -SUI_SXML_ERR_INTERNAL;
        }
        
        if (!link->target || !link->slot)
          return -SUI_SXML_ERR_MISSTAG;

        subns = NULL;
        objdst = ns_find_any_object(sui_utf8_get_text(link->target), &subns);
        if (!objdst) return -SUI_SXML_ERR_NONSOBJ;

        slotid = objdst->vmt->vmt_find_slotid_byname((sui_obj_t*)objdst->ptr,
              sui_utf8_get_text(link->slot), sui_utf8_length(link->slot));

        if (slotid < 0){
          ul_logerr("... no \"%s\" slot\n", sui_utf8_get_text(link->slot));
          return -SUI_SXML_ERR_INTERNAL;
        }

        ret = ul_obj_connect((sui_obj_t *)objsrc->ptr, signalid, (sui_obj_t *)objdst->ptr, slotid);

        if (ret != 0) {
          ul_logerr("Signal-slot connection failed!\n");
          return -SUI_SXML_ERR_INTERNAL;
        }
      }
      break;

    case SUI_SKT_SCENARIO:
      if (tag->id == SUI_SKT_STATE) {
        sui_scenario_t *sco = parent->obj;
        sui_state_inc_refcnt(tag->obj);
        if (sui_sco_state_insert(sco, tag->obj)<0)
          ret = -SUI_SXML_ERR_MEMORY;
// // /*    } else if (tag->id == SUI_SKT_STATEEXT) {
// // ul_logdeb("add stateext to scenario - save somewhere for adding to state\n");*/
      } else if (tag->id == SUI_SKT_TRANSITION) {
        sui_scenario_t *sco = parent->obj;
        if (sui_transition_add_next(&(sco->trans_head), tag->obj) < 0)
          ret = -SUI_SXML_ERR_ADD2ARRAY;
      } else
        ret = -SUI_SXML_ERR_NOPROCESS;
      break;

    case SUI_SKT_SUBSTATE:
      if (tag->id == SUI_SKT_TRANSITION) {
        sui_substate_t *subst = parent->obj;
        if (sui_transition_add_next(&(subst->trans_head), tag->obj) < 0)
          ret = -SUI_SXML_ERR_ADD2ARRAY;
      } else if (tag->id == SUI_SKT_DIALOG) {
        sui_substate_t *subst = parent->obj;
        if (sui_ssdialog_add_next(&(subst->dialog_head), tag->obj) < 0)
          ret = -SUI_SXML_ERR_ADD2ARRAY;
      } else
        ret = -SUI_SXML_ERR_NOPROCESS;
      break;

    case SUI_SKT_STATEEXT:
      if (tag->id == SUI_SKT_SUBSTATE) {
        sui_stateext_t *steext = parent->obj;

        sui_substate_inc_refcnt(tag->obj);
        if (sui_ste_insert(steext, tag->obj) < 0)
          ret = -SUI_SXML_ERR_ADD2ARRAY;
      } else if (tag->id == SUI_SKT_TRANSITION) {
        sui_stateext_t *steext = parent->obj;
        if (sui_transition_add_next(&(steext->trans_head), tag->obj) < 0)
          ret = -SUI_SXML_ERR_ADD2ARRAY;
      } else
        ret = -SUI_SXML_ERR_NOPROCESS;
      break;

    case SUI_SKT_ENTRY:
      if (tag->id == SUI_SKT_ACTION && parent->prev) {
        if (sui_substate_add_action(parent->prev->obj, tag->obj, SUI_SSACT_ENTRY) < 0)
          ret = -SUI_SXML_ERR_ADD2ARRAY;
      } else
        ret = -SUI_SXML_ERR_NOPROCESS;
      break;
    case SUI_SKT_ATFIRST:
      if (tag->id == SUI_SKT_ACTION && parent->prev) {
        if (sui_substate_add_action(parent->prev->obj, tag->obj, SUI_SSACT_ATFIRST) < 0)
          ret = -SUI_SXML_ERR_ADD2ARRAY;
      } else
        ret = -SUI_SXML_ERR_NOPROCESS;
      break;
/*
    case SUI_SKT_LOOP:
      if (tag->id == SUI_SKT_ACTION && parent->prev) {
        if (sui_substate_add_action(parent->prev->obj, tag->obj, SUI_SSACT_LOOP) < 0)
          ret = -SUI_SXML_ERR_ADD2ARRAY;
      } else
        ret = -SUI_SXML_ERR_NOPROCESS;
      break;
*/
    case SUI_SKT_EXIT:
      if (tag->id == SUI_SKT_ACTION && parent->prev) {
        if (sui_substate_add_action(parent->prev->obj, tag->obj, SUI_SSACT_EXIT) < 0)
          ret = -SUI_SXML_ERR_ADD2ARRAY;
      } else
        ret = -SUI_SXML_ERR_NOPROCESS;
      break;

    case SUI_SKT_BEFORE:
      if (tag->id == SUI_SKT_ACTION && parent->prev) {
        if (parent->prev->id == SUI_SKT_TRANSITION) {
          sui_transition_t *trans = parent->prev->obj;
          if (sui_action_add_next(&(trans->abefore_head), tag->obj) < 0)
            ret = -SUI_SXML_ERR_ADD2ARRAY;
        } else if (parent->prev->id == SUI_SKT_DIALOG) {
          sui_ssdialog_t *ssd = parent->prev->obj;
          if (sui_action_add_next(&(ssd->abefore_head), tag->obj) < 0)
            ret = -SUI_SXML_ERR_ADD2ARRAY;
        } else
          ret = -SUI_SXML_ERR_NOPROCESS;
      } else
        ret = -SUI_SXML_ERR_NOPROCESS;
      break;

    case SUI_SKT_AFTER:
      if (tag->id == SUI_SKT_ACTION && parent->prev) {
        if (parent->prev->id == SUI_SKT_TRANSITION) {
          sui_transition_t *trans = parent->prev->obj;
          if (sui_action_add_next(&(trans->aafter_head), tag->obj) < 0)
            ret = -SUI_SXML_ERR_ADD2ARRAY;
        } else if (parent->prev->id == SUI_SKT_DIALOG) {
          sui_ssdialog_t *ssd = parent->prev->obj;
          if (sui_action_add_next(&(ssd->aafter_head), tag->obj) < 0)
            ret = -SUI_SXML_ERR_ADD2ARRAY;
        } else
          ret = -SUI_SXML_ERR_NOPROCESS;
      } else
        ret = -SUI_SXML_ERR_NOPROCESS;
      break;
/*
    case SUI_SKT_ACTION:
      if (tag->id == SUI_SKT_FCN) {
        sui_action_t *act = parent->obj;
        ns_object_t *obj = ns_find_any_object_by_type_and_name(SUI_TYPE_ACTIONFCN, tag->obj);
        if (obj) {
          act->function = obj->ptr;
        } else
          ret = -SUI_SXML_ERR_NOPROCESS;
      } else
        ret = -SUI_SXML_ERR_NOPROCESS;
      break;
*/
    case SUI_SKT_CONDITION:
      if (tag->id == SUI_SKT_EVINFO) {
        sui_condition_t *cond = parent->obj;
        cond->evinfo = *(long *)(tag->obj);
        cond->conflags |= SUI_COND_FLG_EVINFO;
      }
      break;

    case SUI_SKT_PRNFILTER:
      if (tag->id == SUI_SKT_FCN) {
        sui_print_filter_t *pflt = parent->obj;
        pflt->ffnc = sui_pfilter_from_name(sui_utf8_get_text(tag->obj));
        if (!pflt->ffnc) {
          ul_logerr("Printing filter '%s' doesn't exist.\n", sui_utf8_get_text(tag->obj));
          sui_sxml_doc_warning(doc, SUI_SXML_ERR_UNKNOWN, "non existing printing filter");
        }
      } else
        ret = -SUI_SXML_ERR_NOPROCESS;
      break;
    case SUI_SKT_PRNLAYOUT:
      if (tag->id == SUI_SKT_PRNFILTER) {
        sui_print_layout_t *play = parent->obj;
        sui_print_filter_t *pflt = tag->obj;
        if (!pflt || !pflt->ffnc || sui_playout_add_filter(play, tag->obj)) {
          ul_logerr("Printing filter wasn't added to the layout.\n");
          sui_sxml_doc_warning(doc, SUI_SXML_ERR_UNKNOWN, "printing filter wasn't used");
        }
      } else
        ret = -SUI_SXML_ERR_NOPROCESS;
      break;
      
    default:
      ul_logerr("Special Processing... ptid=%d | tid=%d\n", parent->id, tag->id);
      ret = -SUI_SXML_ERR_NOPROCESS;
      break;
  }
  return ret;
}


/* process attributes special for some tags */
/**
 * sxml_suitk_process_attrib - Process attributes specific for application
 * @doc: Pointer to a sui_sxml_doc structure which describes sxml documents.
 *
 * The function processes attribute and its value.
 *
 * Return Value: The function returns SUI_SXML_ERR_OK as successfully processed
 * attribute, -SUI_SXML_ERR_NOPROCESS if attribute is not processed,
 * other SUI_SXML_ERR_... code as warning and -SUI_SXML_ERR_... (negative value) as critical error.
 *
 * File: sxml_suitk.c
 */
int sxml_suitk_process_attrib(struct sui_sxml_doc *doc)
{
  int ret = -SUI_SXML_ERR_NOPROCESS;
  sui_sxml_tag_t *curtag = doc->tag;
  if (curtag->prev && (curtag->prev->id == SUI_SKT_CONDITION) &&
                      !strcmp(doc->curattr.name,"negative")) {
    sui_condition_t *cond = curtag->prev->obj;
    if (!strcmp(doc->curattr.value, "yes")) {
      switch(curtag->id) {
        case SUI_SKT_WIDGET:
          cond->negflags |= SUI_COND_FLG_FOCUS;
          break;
        case SUI_SKT_STATE:
          cond->negflags |= SUI_COND_FLG_LASTST;
          break;
        case SUI_SKT_SUBSTATE:
          cond->negflags |= SUI_COND_FLG_LASTSST;
          break;
      }
// set current cond struct - field negflags //
    }
    ret = SUI_SXML_ERR_OK;
  } else if (curtag->id == SUI_SKT_FILE) {
    if (!strcmp(doc->curattr.name, "loadpal") && doc->curattr.value &&
         !strcmp(doc->curattr.value, "yes")) {
      curtag->flags |= SUI_SXML_FLG_USER_ATTR1;
      ret = SUI_SXML_ERR_OK;
    }
  } else if (curtag->prev->id == SUI_SKT_LIST) {
    if (!strcmp(doc->curattr.name, "index")) {
      if (sui_typeid_convert(SUI_TYPE_LONG, &curtag->user, SUI_TYPE_UTF8, &doc->curattr.value, 1) != SUI_RET_OK) {
        ns_object_t *obj = ns_find_any_object_by_type_and_name(SUI_TYPE_DEFINE, doc->curattr.value);
        sui_sxml_value_t *aval;
        if (!obj) return -SUI_SXML_ERR_BADATTR;
        aval = (sui_sxml_value_t *)obj->ptr;
        if (aval->valtype == SUI_SXML_VALUE_NUMBER) {
          sui_typeid_convert(SUI_TYPE_LONG, &curtag->user, SUI_TYPE_ULONG, &aval->data.number, 1);
        } else if (aval->valtype == SUI_SXML_VALUE_SIGNUM) {
          sui_typeid_convert(SUI_TYPE_LONG, &curtag->user, SUI_TYPE_ULONG, &aval->data.number, 1);
          curtag->user = -curtag->user;
        } else
          return -SUI_SXML_ERR_BADATTR;
      }
      curtag->flags |= SUI_SXML_FLG_USER_VALUE;
      ret = SUI_SXML_ERR_OK;
    } else if (((curtag->id == SUI_SKT_VARF) || (curtag->id==SUI_SKT_VARL)) &&
                !strcmp(doc->curattr.name, "propindex") &&
                !strcmp(doc->curattr.value, "yes")) {
      curtag->flags |= SUI_SXML_FLG_USER_SPECFLG;
      ret = SUI_SXML_ERR_OK;
    }
  }
  return ret;
}

/******************************************************************************/
sui_sxml_doc_description_t sxml_suitk_ddesc = {
  .idsize = SUI_SKT_SUITK_MAXARRAY,
  .idhead = SUI_SKT_SXML,
  .tags = sxml_suitk_known_tags,
  .spfcn = sxml_suitk_special_process,
  .pafcn = sxml_suitk_process_attrib,
};

