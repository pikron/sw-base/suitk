/* sxml_suitk.h
 *
 * SXML for SuiTk
 *
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#ifndef _SXML_SUITK_TAGS_HEADER_
#define _SXML_SUITK_TAGS_HEADER_

#include <suixml/sui_sxml.h>


/******************************************************************************/
/* special structures */
/**
 * struct sui_sxml_link - Special temporary structure for signal-slot connection from SXML
 * @source: Name of a source object.
 * @signal: Name of a signal.
 * @target: Name of a target object.
 * @slot: Name of a slot.
 *
 * File: sui_sxml.h
 */
typedef struct sui_sxml_link {
  utf8 *source;
  utf8 *signal;
  utf8 *target;
  utf8 *slot;
} sui_sxml_link_t;



/******************************************************************************/
/* IDs of SXML tags for SuiTk */

enum sxml_suitk_known_tag_ids {
  SUI_SKT_SXML = SUI_SKT_COMMON_MAXID,

  SUI_SKT_WIDGETSET,
  SUI_SKT_OBJECTSET,
  SUI_SKT_SCREENSET,
  SUI_SKT_TITLE,
  SUI_SKT_AUTHOR,
  SUI_SKT_DATE,
  SUI_SKT_VERSION, /* 11 */

  SUI_SKT_COLOR,
  SUI_SKT_COLORTABLE,
  SUI_SKT_FONT,
  SUI_SKT_STYLE,
  SUI_SKT_VAR,
  SUI_SKT_SUFFIX,
  SUI_SKT_LIST,
  SUI_SKT_KEY,
  SUI_SKT_KEYTABLE,
  SUI_SKT_IMAGE,    /* 21 */
  SUI_SKT_FORMAT,
  SUI_SKT_TEXT,
  SUI_SKT_CHOICE,
  SUI_SKT_CHOICETABLE,
  SUI_SKT_WIDGET,
  /*	SUI_SKT_SCREEN, */
  SUI_SKT_NAME,
  SUI_SKT_SIZE,
  SUI_SKT_FLAGS,
  SUI_SKT_LABELFONT, /* 30 */
  SUI_SKT_FRAME,
  SUI_SKT_GAP,
  SUI_SKT_TYPE,
  SUI_SKT_DATA,
  SUI_SKT_MIN,
  SUI_SKT_MAX,
  SUI_SKT_DIGITS,
  SUI_SKT_MAXINDEX,
  SUI_SKT_INIT,
  SUI_SKT_COUNT,
  SUI_SKT_DECIMALALIGN, /* 40 */
  SUI_SKT_SUFFIXALIGN,
  SUI_SKT_DECIMALFONT,
  SUI_SKT_SUFFIXFONT,
  SUI_SKT_VALUE,
  SUI_SKT_KEYCODE,
  SUI_SKT_FCN,
  SUI_SKT_ARG,
  SUI_SKT_FILE,
  SUI_SKT_BASE,
  SUI_SKT_MASK, /* 50 */
  SUI_SKT_X,
  SUI_SKT_Y,
  SUI_SKT_WIDTH,
  SUI_SKT_HEIGHT,
  SUI_SKT_LABEL,
  SUI_SKT_LABALIGN,
  SUI_SKT_TIP,
  SUI_SKT_WGROUP,
  SUI_SKT_WBUTTON,
  SUI_SKT_WBITMAP, /* 60 */
  SUI_SKT_WLISTBOX,
  SUI_SKT_WNUMBER,
  SUI_SKT_WSCROLL,
  SUI_SKT_WTEXT,
  SUI_SKT_WTIME,
  SUI_SKT_ID,
  SUI_SKT_ALIGN,
  SUI_SKT_COLS,
  SUI_SKT_ROWS,
  SUI_SKT_LINE, /* 70 */
  SUI_SKT_DAYS,
  SUI_SKT_MONTHS,
  SUI_SKT_AMPM,

  SUI_SKT_CONNECTIONS,
  SUI_SKT_LINK,
  SUI_SKT_SOURCE,
  SUI_SKT_SIGNAL,
  SUI_SKT_TARGET,
  SUI_SKT_SLOT,

  SUI_SKT_SCENARIO, /* 80 */
  SUI_SKT_STATE,
  SUI_SKT_STATEEXT,
  SUI_SKT_SUBSTATE,
  SUI_SKT_SCREEN,

  SUI_SKT_CONDITION,
  SUI_SKT_EXITCOND,
  SUI_SKT_TRANSITION,
  SUI_SKT_TIME,
  SUI_SKT_USERTIME, /* 89 */
  SUI_SKT_TERM,
  SUI_SKT_EVENT,
  SUI_SKT_EVCMD,
  SUI_SKT_EVKEY,
  SUI_SKT_EVINFO,

  SUI_SKT_ENTRY,
  SUI_SKT_LOOP,
  SUI_SKT_EXIT,
  SUI_SKT_ATFIRST,
  SUI_SKT_BEFORE, /* 99 */
  SUI_SKT_AFTER,

  SUI_SKT_ACTION,
  SUI_SKT_WDGSRC,
  SUI_SKT_WDGDST,
  SUI_SKT_DISRC,
  SUI_SKT_DIDST,

  SUI_SKT_DIALOG,
  SUI_SKT_STATUS, /* 107 */

  SUI_SKT_WTABLE,
  SUI_SKT_TABLE,
  SUI_SKT_COLUMN, /* 110 */
  SUI_SKT_VARCOL,
  SUI_SKT_SPAN,

  SUI_SKT_VARF,
  SUI_SKT_VARL,
  SUI_SKT_LIPRINT,

  SUI_SKT_WDYNTEXT,
  SUI_SKT_CTIME,
  SUI_SKT_DTIME,
  SUI_SKT_DINDEX,

  SUI_SKT_PRNFILTER, /* 120 */
  SUI_SKT_PRNLAYOUT,

  SUI_SKT_DEVICE,

  SUI_SKT_WGRAPHXY,
  SUI_SKT_GDATA,
  SUI_SKT_GSET,

  SUI_SKT_POSX,
  SUI_SKT_POSY,
  SUI_SKT_ORIGINX,
  SUI_SKT_ORIGINY,
  SUI_SKT_RANGEX, /* 130 */
  SUI_SKT_RANGEY,
  SUI_SKT_OFFSET,
  SUI_SKT_STEP,

  SUI_SKT_UINIT, /* 134 */

  SUI_SKT_SUITK_MAXARRAY,
};


extern sui_sxml_doc_description_t sxml_suitk_ddesc;

#endif /* _SXML_SUITK_TAGS_HEADER_ */
