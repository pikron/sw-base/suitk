/* sui_sxml.c
 *
 * SUITK parsing and processing SXML.
 *
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#define SXML_TIME_PROFILE

#include <stddef.h>

#include "sui_sxml.h"

#include <suiut/sui_typemethods.h>
#include <suiut/sui_typebase.h>

#include <ul_log.h>

UL_LOG_CUST(ulogd_sxml)

/******************************************************************************
 * objects and VMTs
 ******************************************************************************/
SUI_TYPE_DECLARE_SIMPLE(define, SUI_TYPE_DEFINE, sui_sxml_value_t, NULL);

ns_list_t *sui_sxml_current_ns = &ns_local_namespace;

/******************************************************************************/
const sui_sxml_tag_info_t sui_sxml_common_known_tags[] = {
  [SUI_SKT_UNKNOWN]   = {"", NULL},
  [SUI_SKT_DEFINE]    = {"define", NULL},
  [SUI_SKT_INCLUDE]   = {"include", NULL},
  [SUI_SKT_DIV]       = {"div", NULL},
};

/******************************************************************************/
/**
 * sui_sxml_get_prev_tag - Return parent tag in the tag list
 * @tag: Pointer to a sui_sxml_tag structure.
 *
 * The function finds parent tag, which hasn't got set SKIP flag.
 *
 * Return Value: The function returns pointer to sui_sxml_tag structure or NULL.
 *
 * File: sui_sxml.c
 */
inline
sui_sxml_tag_t *sui_sxml_get_prev_tag(sui_sxml_tag_t *tag)
{
  sui_sxml_tag_t *prevtag = tag->prev;
  while(prevtag && (prevtag->flags & SUI_SXML_FLG_SKIP))
    prevtag = prevtag->prev;
  return prevtag;
}

/******************************************************************************
 * tag_info functions
 ******************************************************************************/
/**
 * sui_sxml_find_taginfo_by_id - Find tag_info structure according to tag ID
 * @ti_id: A tag ID.
 *
 * The function tries to find sui_sxml_tag_info structure according to tag ID.
 *
 * Return Value: The function returns pointer to sui_sxml_tag_info structure or NULL.
 *
 * File: sui_sxml.c
 */
const sui_sxml_tag_info_t *sui_sxml_find_taginfo_by_id(sui_sxml_doc_description_t *ddesc, int ti_id)
{
  if (ti_id < SUI_SKT_COMMON_MAXID) {
    return (&sui_sxml_common_known_tags[ti_id]);
  } else if (ti_id < ddesc->idsize) {
    return (ddesc->tags+ti_id);
  } else
    return NULL;
}

/**
 * sui_sxml_find_child_taginfo - Find structure describing child tag according to parent tag and child tag name
 * @parent: Pointer to a sui_sxml_tag structure describing parent tag.
 * @tagname: Pointer to a searched child tag name.
 *
 * The function tries to find sui_sxml_tag_info_child structure according to child tag name (from parent tag structure).
 *
 * Return Value: The function returns pointer to sui_sxml_tag_info_child structure or NULL.
 *
 * File: sui_sxml.c
 */
sui_sxml_tag_info_child_t const *sui_sxml_find_child_taginfo(sui_sxml_doc_description_t *ddesc, const sui_sxml_tag_t *parent, const char *tagname)
{
  sui_sxml_tag_info_child_t const *ret = NULL;
  if (!parent || !tagname) return NULL;
  if (parent->id == SUI_SKT_UNKNOWN) return NULL;
  ret = (sui_sxml_find_taginfo_by_id(ddesc, parent->id))->child;
  while (ret && (ret->tag_id != SUI_SXML_ENDOFCHILDREN)) {
    if (!strcmp((sui_sxml_find_taginfo_by_id(ddesc, ret->tag_id))->tag, tagname))
      return ret;
    ret++;
  }
  return NULL;
}


/******************************************************************************
 * sxml parser error and warning functions
 ******************************************************************************/
/**
 * sui_sxml_error_string - Translate sxml error code to text description
 * @err: A SXML error code.
 *
 * The function translates SXML error code to text description of the error.
 *
 * Return Value: The function returns pointer to static text description of SXML error.
 * (The pointer mustn't be released.)
 *
 * File: sui_sxml.c
 */
const char *sui_sxml_error_string(int err)
{
  switch (err & SUI_SXML_ERR_MASK) {
    case SUI_SXML_ERR_OK:        return "all OK (no error)";
    case SUI_SXML_ERR_UNKNOWN:   return "unspecified error";
    case SUI_SXML_ERR_BTAGEXP:   return "BTAG was expected";
    case SUI_SXML_ERR_TAGNAME:   return "tagname";
    case SUI_SXML_ERR_TAGCROSS:  return "another ETAG was expected";
    case SUI_SXML_ERR_ADD2NS:    return "object wasn't added to namespace";
    case SUI_SXML_ERR_BADARGS:   return "bad argument";
    case SUI_SXML_ERR_NOFILE:    return "file wasn't found";
    case SUI_SXML_ERR_MEMORY:    return "no free memory";
    case SUI_SXML_ERR_ADDTAG:    return "tag wasn't added to stack";
    case SUI_SXML_ERR_REMTAG:    return "tag wasn't removed from stack";
    case SUI_SXML_ERR_SYNTAX:    return "syntax error";
    case SUI_SXML_ERR_BADTAG:    return "unknown tag (child tag)";
    case SUI_SXML_ERR_ISINNS:    return "object is already in namespace";
    case SUI_SXML_ERR_BADATTR:   return "bad attribute value or format";
    case SUI_SXML_ERR_INTERNAL:  return "internal";
    case SUI_SXML_ERR_TYPECONV:  return "type conversion";
    case SUI_SXML_ERR_BADTERM:   return "error in term (syntax error, dinfo doesn't exist, ...)";
    case SUI_SXML_ERR_NONSOBJ:   return "object isn't in namespace";
    case SUI_SXML_ERR_ADD2ARRAY: return "object wasn't added to parent object";
    case SUI_SXML_ERR_NOPROCESS: return "tag wasn't processed";
    case SUI_SXML_ERR_FIRSTTAG:  return "first tag must be 'SXML'";
    case SUI_SXML_ERR_DIRTAGVAL: return "tag cann't have direct value";
    case SUI_SXML_ERR_INFILE:    return "error in included file";
    case SUI_SXML_ERR_VALTYPE:   return "bad value type";
    case SUI_SXML_ERR_ADDWIDGET: return "widget wasn't added to group";
    case SUI_SXML_ERR_INHERIT:   return "predecessor isn't in namespace";
    case SUI_SXML_ERR_MISSTAG:   return "missing child tag";
    case SUI_SXML_ERR_BADCHKSUM: return "bad file checksum";
    case SUI_SXML_ERR_BADCOMBINE:return "unsupported value combination";
    default: return "???";
  }
}

/**
 * sui_sxml_doc_error - Prints SXML error/warning message
 * @doc: Pointer to a SXML document structure.
 * @err: Code of a SXML error/warning.
 * @addstr: Pointer to an additional error text.
 *
 * The function prints sxml error/warning message with correct log level and
 * position of error in a SXML document (SXML file name and line in the file),
 * if the position is known.
 *
 * Return Value: The function doesn't return any value.
 *
 * File: sui_sxml.c
 */
void sui_sxml_doc_error(sui_sxml_doc_t *doc, int err, char *addstr)
{
  int loglevel;
  char *addstr1, *addstr2, *addstr3;
  
  if(!addstr) {
    addstr1 = addstr2 = addstr3="";
  } else {
    addstr1 = " (";
    addstr2 = addstr;
    addstr3=")";
  }
  
  if (err & SUI_SXML_ERR_WARNING) {
    loglevel = UL_LOGL_MSG;
  } else {
    if (doc) doc->state = SUI_SXML_STATE_ERROR;
    loglevel = UL_LOGL_ERR;
  }
  if (doc && doc->source) {
    if ((doc->source->flags & SUI_FSRC_TYPEMASK) == SUI_FSRC_FILE) {
      ul_loglev(loglevel, "%s at line %d in doc:'%s'%s%s%s.\n",
        sui_sxml_error_string(err), doc->source->line,
        doc->source->source.file.filename, addstr1, addstr2, addstr3);
    } else {
      ul_loglev(loglevel, "%s at line %d in mem:0x%p%s%s%s.\n",
        sui_sxml_error_string(err), doc->source->line,
        doc->source->source.memory.addr, addstr1, addstr2, addstr3);
    }
  } else {
    ul_loglev(loglevel, "%s%s%s%s.\n", sui_sxml_error_string(err), addstr1, addstr2, addstr3);
  }
}

/**
 * sui_sxml_doc_warning - Print sxml warning message
 * @doc: Pointer to a SXML document structure.
 * @warn: Code of a SXML warning.
 * @addstr: Pointer to an additional warning text.
 *
 * The function prints sxml warning message with position of warning in a SXML document.
 *
 * Return Value: The function doesn't return any value.
 *
 * File: sui_sxml.c
 */
inline
void sui_sxml_doc_warning(sui_sxml_doc_t *doc, int warn, char *addstr)
{
  sui_sxml_doc_error(doc, warn | SUI_SXML_ERR_WARNING, addstr);
}


/******************************************************************************
 * sxml attribute structure
 ******************************************************************************/
/**
 * sui_sxml_attrib_clear - Clear attribute structure
 * @attr: Pointer to a sui_sxml_attribute structure.
 *
 * The function releases sui_sxml_attribute structure fields.
 *
 * Return Value: The function doesn't return any value.
 *
 * File: sui_sxml.c
 */
void sui_sxml_attrib_clear(sui_sxml_attrib_t *attr)
{
  if (!attr) return;
  if (attr->name) sui_free(attr->name);
  attr->name = NULL;
  if (attr->value) sui_free(attr->value);
  attr->value = NULL;
}


/******************************************************************************
 * sxml value structure
 ******************************************************************************/
/**
 * sui_sxml_value_clear - Clear value structure
 * @value: Pointer to a sui_sxml_value structure.
 *
 * The function releases sui_sxml_value structure fields.
 *
 * Return Value: The function doesn't return any value.
 *
 * File: sui_sxml.c
 */
void sui_sxml_value_clear(sui_sxml_value_t *value)
{
  if (!value) return;
  if ((value->valtype == SUI_SXML_VALUE_STRING ||
      value->valtype == SUI_SXML_VALUE_ID) && value->data.string)
    sui_free(value->data.string);
  memset(value, 0, sizeof(sui_sxml_value_t));
}

/**
 * sui_sxml_value_translate - Translate text string to value
 * @val: Pointer to an output value structure.
 * @buffer: Pointer to a text string pointer.
 * @bufsize: Size of a text buffer.
 *
 * The function decodes text string from the buffer to the value. If the text buffer begins
 * with a quotation mark the output value will be a text. If the text buffer begins with
 * a number or the minus mark the output value will be a number. Otherwise the output value
 * is an identifier.
 *
 * Return Value: The function returns zero as success and a negative value as an error.
 *
 * File: sui_sxml.c
 */
int sui_sxml_value_translate(sui_sxml_value_t *val, char **buffer, int bufsize)
{
  char ch = *(*buffer);
  if (bufsize == 0) return -1;
  if (bufsize < 0 && buffer && *buffer) bufsize = strlen(*buffer);

  if (ch == '"') {/* maybe string */
    if (*(*buffer+bufsize-1)=='"') {/* string*/
      int i = 0;
      val->valtype = SUI_SXML_VALUE_STRING;
      while(i < bufsize-2) {/* trim double-quotes */
        *(*buffer+i) = *(*buffer+i+1);
        i++;
      }
      *(*buffer+bufsize-2)=0;
      val->data.string = *buffer;
      *buffer = NULL;
    } else {/* error */
      return -2;
    }
  } else if ((ch >='0' && ch <='9') || (ch =='-')) {/*number */
    char *str = *buffer;
    if (ch=='-') {/* negative - long */
      str++;
      sui_str_to_long(&str, (long *) &val->data.number);
      val->valtype = SUI_SXML_VALUE_SIGNUM;
    } else {
      sui_str_to_ulong(&str, &val->data.number);
      val->valtype = SUI_SXML_VALUE_NUMBER;
    }
    sui_free(*buffer);
    *buffer = NULL;
  } else if (bufsize == 1) {   /* character */
    val->valtype = SUI_SXML_VALUE_CHARACTER;
    val->data.ch = ch;
    sui_free(*buffer);
    *buffer = NULL;
  } else {/* IDentifier */
    val->valtype = SUI_SXML_VALUE_ID;
    val->data.string = *buffer;
    *buffer = NULL;
  }
  return 0;
}

/**
 * sui_sxml_value_resolve_id - Try to translate value with identifier, which is DEFINEd, to defined value
 * @val: Pointer to an input/output value structure.
 *
 * The function tries to find identifier (given from a value) between defined values (sxml tag DEFINE)
 * and translates the value to the found defined value.
 *
 * Return Value: The function doesn't return any value.
 *
 * File: sui_sxml.c
 */
void sui_sxml_value_resolve_id(sui_sxml_value_t *val)
{
  while (val->valtype == SUI_SXML_VALUE_ID) {/* try find ID in namespace */
    ns_object_t *obj = ns_find_any_object_by_type_and_name(SUI_TYPE_DEFINE, val->data.string);
    if (obj && obj->ptr) {
      if (val->valtype == SUI_SXML_VALUE_ID || val->valtype == SUI_SXML_VALUE_STRING) {
        sui_free(val->data.string);
        val->data.string = NULL;
      }
      val->valtype = ((sui_sxml_value_t *)obj->ptr)->valtype;
      if (((sui_sxml_value_t *)obj->ptr)->valtype == SUI_SXML_VALUE_STRING) {
        val->data.string = strdup(((sui_sxml_value_t *)obj->ptr)->data.string);
      } else {
        val->data.number = ((sui_sxml_value_t *)obj->ptr)->data.number;
      }
    } else {/* it isn't define ID but it can be ID of another object type */
      break;
    }
  }
}

/**
 * sui_sxml_value_from_dinfo - translate value ID to value of dinfo with name==ID
 * @val: pointer to tag value
 *
 * Return Value: The functions returns 0 if no translation was processed, 1 if some translation was processed
 * and -1 if any error occured.
 */
int sui_sxml_value_from_dinfo(sui_sxml_value_t *val)
{
  sui_dinfo_t *di = NULL;
  ns_object_t *obj;

  if(val->valtype!=SUI_SXML_VALUE_ID) /* no ID in value - no translation */
    return 0;

  obj = ns_find_any_object_by_type_and_name(SUI_TYPE_DINFO, val->data.string);
  if (!obj) /* no object -> no translation */
    return 0;

  di = obj->ptr;
  switch (di->tinfo) {
    case SUI_TYPE_CHAR:
    case SUI_TYPE_SHORT:
    case SUI_TYPE_INT:
    case SUI_TYPE_LONG:
      sui_sxml_value_clear(val);
      sui_rd_long(di, 0, (long *) &val->data.number);
      val->valtype = SUI_SXML_VALUE_SIGNUM;
      return 1;
    case SUI_TYPE_UCHAR:
    case SUI_TYPE_USHORT:
    case SUI_TYPE_UINT:
    case SUI_TYPE_ULONG:
      sui_sxml_value_clear(val);
      sui_rd_ulong(di, 0, &val->data.number);
      val->valtype = SUI_SXML_VALUE_NUMBER;
      return 1;
//    case SUI_TYPE_UTF8:
//      sui_sxml_value_clear(val);
//     /* TODO: from utf8 dinfo create dynamic string */
//      return 1;
  }
  return 0;
}

/**
 * sui_sxml_value_to_object - Translate value to object according to object's type
 * @val: Pointer to an input value.
 * @objptr: Pointer to an output buffer to pointer to object.
 * @objvmt: Pointer to an object VMT description.
 *
 * The function translates sxml value to object (ULONG, LONG, STRING).
 *
 * Return Value: The function returns negative value of the sxml error code.
 *
 * File: sui_sxml.c
 */
int sui_sxml_value_to_object(sui_sxml_value_t *val, void **objptr, sui_obj_vmt_t *objvmt)
{
  int ret = SUI_SXML_ERR_OK, searching = 1;
  sui_typeid_t tid = objvmt->vmt_typeid;

  while (searching) {
    sui_sxml_value_resolve_id(val);
  /* FIXME: !!! */
    if (val && objptr && objvmt) {
      switch(val->valtype) {
      case SUI_SXML_VALUE_NUMBER:
        ret = sui_typeid_convert(tid, *objptr, SUI_TYPE_ULONG, &val->data.number, 1);
        if (ret) {
          ul_logerr("Conversion (ULONG) error... (tid=%d)\n", tid);
          ret = -SUI_SXML_ERR_TYPECONV;
        }
        break;
      case SUI_SXML_VALUE_SIGNUM:
        {
          long negval = -val->data.number;
          ret = sui_typeid_convert(tid, *objptr, SUI_TYPE_LONG, &negval, 1);
          if (ret) {
            ul_logerr("Conversion (LONG) error... (tid=%d)\n", tid);
            ret = -SUI_SXML_ERR_TYPECONV;
          }
        }
        break;

      case SUI_SXML_VALUE_STRING:
        if (tid == SUI_TYPE_UTF8) {
  //        ret = sui_typeid_convert(type, *objptr, SUI_TYPE_ASCII, val->data.string, 1);
          /* special conversion, because utf8 structure already exist */
          sui_utf8_t *ustr = *objptr;
          //if (ustr->refcnt > 1) {error - two string }
          ustr->type = UTF8_TYPE_STRUCT;
          ustr->bytes = strlen(val->data.string);
          ustr->cap_bytes = ustr->bytes+1; /* ending zero */
          ustr->chars = sui_utf8_length((utf8 *)val->data.string);
          ustr->data.ptr = val->data.string;
          ustr->refcnt = 1;
    
          val->valtype = SUI_SXML_VALUE_NONE;
          val->data.string = NULL;

        } else if (tid == SUI_TYPE_ASCII) {
          if (*objptr) sui_free(*objptr); /* FIXME: really release ??? */
          *(void **)objptr = val->data.string;
          val->data.string = NULL;
          val->valtype = SUI_SXML_VALUE_NONE;

        } else {
          ret = -SUI_SXML_ERR_VALTYPE;
        }
        break;

      case SUI_SXML_VALUE_ID:
        {
          ns_object_t *obj = NULL;
          if (tid == SUI_TYPE_WIDGET) {
            obj = ns_find_widget_in_ns(val->data.string);
          } else {
            obj = ns_find_any_object_by_type_and_name(tid, val->data.string);
            if (!obj && tid<SUI_TYPEBASE_NEXT_TYPE) { /* ID doesn't point to a valid object of the required type - we can try dinfos for basic types */
              if (sui_sxml_value_from_dinfo(val)==1) /* only one attempt to translate ID as value of dinfo(ID) */
                continue;
            }
          }
          if (obj) {
  /* FIXME: really copy ??? */
  //          ret = sui_typeid_copy(*objptr, obj->ptr, tid);

            if (objvmt->inc_refcnt(obj->ptr) == SUI_REFCNTIMPOSIBLE) {/* no reference -> copy */
              memcpy(*objptr, obj->ptr, objvmt->vmt_obj_tinfo->obj_size);
  // /*            objvmt->dec_refcnt(**(void ***)objptr);
  //             objvmt->inc_refcnt(obj->ptr);
  //             **(void ***)objptr = obj->ptr;*/
            } else {
              objvmt->dec_refcnt(*objptr);
              *(void **)objptr = obj->ptr;
            }

          } else {
            ret = -SUI_SXML_ERR_NONSOBJ;
  //           if (type == SUI_TYPE_UTF8) {/* patch for UTF8 text included as attribute - maybe it will not work properly */
  //             ret = sui_typeid_convert(type, *objptr, SUI_TYPE_ASCII, val->data.string, 1); /* FIXME: maybe problems with changing of utf8 to tag->pobj */
  //           } else {
  //             ret = -SUI_SXML_ERR_NONSOBJ;
  //           }
          }
          sui_sxml_value_clear(val);
        }
        break;
      default:
        ret = -SUI_SXML_ERR_INTERNAL;
      }
      return ret;
    }
    searching = 0;
  }
  return -SUI_SXML_ERR_VALTYPE;
}

/******************************************************************************
 * main tag structure - based on LIFO stack
 ******************************************************************************/
/**
 * sui_sxml_create_tag - Create a new tag structure
 * @tagname: Pointer to the pointer to a tagname of the created tag structure.
 *
 * The function creates a new sxml tag structure with entered tag name. The tag name is
 * moved from its entered position to the created structure (input pointer to the tag name
 * is cleared).
 *
 * Return Value: The function returns pointer to a new sxml tag structure or NULL.
 *
 * File: sui_sxml.c
 */
sui_sxml_tag_t *sui_sxml_create_tag(char **tagname)
{
  sui_sxml_tag_t *newtag = NULL;
  if (!tagname || !(*tagname)) return NULL;
  /* create new tag structure */
  newtag = sui_malloc(sizeof(sui_sxml_tag_t));
  if (!newtag) return NULL;
  
  newtag->tag_name = *tagname;
  *tagname = NULL;
  
  return newtag;
}

/**
 * sui_sxml_destroy_tag - Release tag structure from memory
 * @tag: Pointer to a tag structure.
 *
 * The function releases tag and namespace name and clears tag value
 * and then it removes the tag structure from memory.
 *
 * Return Value: The function returns zero as success.
 *
 * File: sui_sxml.c
 */
int sui_sxml_destroy_tag(sui_sxml_tag_t *tag)
{
  if (!tag) return 0;
  if (tag->tag_name) sui_free(tag->tag_name);
  if (tag->ns_name) sui_free(tag->ns_name);
  sui_sxml_value_clear(&tag->tag_value);
  sui_free(tag);
  return 0;
}

/**
 * sui_sxml_tag_add_to_doc - Add tag structure to the current sxml_doc structure
 * @doc: Pointer to a sxml document structure.
 * @tag: Pointer to a tag structure.
 *
 * The function adds tag structure to the sxml document and link the previous tag structure.
 *
 * Return Value: The function returns zero as success and a negative value as an error.
 *
 * File: sui_sxml.c
 */
int sui_sxml_tag_add_to_doc(sui_sxml_doc_t *doc, sui_sxml_tag_t *tag)
{
  if (!doc || !tag) return -1;
  if (tag->tag_name == NULL) {
    sui_sxml_doc_error(doc, SUI_SXML_ERR_TAGNAME, NULL);
    return -1;
  }
  tag->prev = doc->tag;
  doc->tag = tag;
  return 0;
}

/**
 * sui_sxml_tag_remove_from_doc - Remove the last tag structure from the current sxml_doc structure
 * @doc: Pointer to a sxml document structure.
 *
 * The function removes the last tag structure from the sxml document and returns the previous
 * tag structure to the sxml document. The removed tag structure isn't destroyed.
 *
 * Return Value: The function returns pointer to the removed tag structure (or NULL).
 *
 * File: sui_sxml.c
 */
sui_sxml_tag_t *sui_sxml_tag_remove_from_doc(sui_sxml_doc_t *doc)
{
  sui_sxml_tag_t *last = NULL;
  if (!doc || !doc->tag) return NULL;
  last = doc->tag;
  doc->tag = last->prev;
  last->prev = NULL;
  return last;
}

/**
 * sui_sxml_clear_tag_lifo - Remove and destroy all tag structures nested under a sxml document
 * @doc: Pointer to a sxml document structure.
 *
 * The function removes and destroys all tag structures saved under the sxml document.
 *
 * Return Value: The function doesn't return any value.
 *
 * File: sui_sxml.c
 */
void sui_sxml_clear_tag_lifo(sui_sxml_doc_t *doc)
{
  sui_sxml_tag_t *tmp;
  if (!doc) return;
  while (doc->tag) {
    tmp = doc->tag->prev;
    sui_sxml_destroy_tag(doc->tag);
    doc->tag = tmp;
  }
}


/******************************************************************************
 * sxml help parsing automate functions
 ******************************************************************************/
/**
 * sui_sxml_combine_values - Combine two values according to combination mark (plus, or, ...)
 * @first_to: Pointer to an input/output value.
 * @second: Pointer to a second input value.
 * @ch: A combination mark.
 *
 * The function combines two values according to combination mark (plus, or, and, ...).
 *
 * Return Value: The function returns zero as success and a negative value as an error.
 *
 * File: sui_sxml.c
 */
int sui_sxml_combine_values(sui_sxml_value_t *first_to, sui_sxml_value_t *second, int ch)
{
  switch (ch) {
    case '|':
      if ((first_to->valtype == SUI_SXML_VALUE_NUMBER) &&
          (second->valtype == SUI_SXML_VALUE_NUMBER)) {
        first_to->data.number |= second->data.number;
        return SUI_SXML_ERR_OK;
      }
      break;
    case '+':
      if ((first_to->valtype == SUI_SXML_VALUE_NUMBER) &&
          (second->valtype == SUI_SXML_VALUE_NUMBER)) {
        first_to->data.number += second->data.number;
        return SUI_SXML_ERR_OK;
      }
      break;
    case '-':
      if ((first_to->valtype == SUI_SXML_VALUE_NUMBER) &&
          (second->valtype == SUI_SXML_VALUE_NUMBER)) {
        first_to->data.number -= second->data.number;
        return SUI_SXML_ERR_OK;
      }
      break;
    case ',':
      if ((first_to->valtype == SUI_SXML_VALUE_STRING) &&
          (second->valtype == SUI_SXML_VALUE_STRING)) {
        char *tempstr = realloc(first_to->data.string,
              strlen(first_to->data.string) + strlen(second->data.string) + 1);
        if (tempstr) {
          strcat(tempstr, second->data.string);
          first_to->data.string = tempstr;
          sui_sxml_value_clear(second);
          return SUI_SXML_ERR_OK;
        } else
          return -SUI_SXML_ERR_MEMORY;
      }
      break;
    default:
      break;
  }
  return -SUI_SXML_ERR_BADCOMBINE;
}

/**
 * sui_sxml_open_tag - Process BTAG token (opening tag)
 * @doc: Pointer to a sxml document structure.
 *
 * The function processes BTAG and creates/prepares space for tag data
 *
 * Return Value: The function returns negative value of SUI_SXML_ERR_ error code.
 *
 * File: sui_sxml.c
 */
int sui_sxml_open_tag(sui_sxml_doc_t *doc)
{
  sui_sxml_tag_t *newtag;
  
  if ((newtag = sui_sxml_create_tag(&(doc->token.buffer))) == NULL)    /* create tag */
    return -SUI_SXML_ERR_MEMORY;

  doc->state = SUI_SXML_STATE_INBTAG;
  if (sui_sxml_tag_add_to_doc(doc, newtag))   /* add tag to stack */
    return -SUI_SXML_ERR_ADDTAG;
//ul_logdeb("Open tag '%s'...\n", newtag->tag_name);
/* fill tag - find it in taginfo tree */
  if (!strcmp(newtag->tag_name, "include")) {
    newtag->id = SUI_SKT_INCLUDE;
  } else if (!strcmp(newtag->tag_name, "div")) {
    newtag->id = SUI_SKT_DIV;
    newtag->flags = SUI_SXML_FLG_SKIP;
  } else if (!strcmp(newtag->tag_name, "define")) {
    newtag->id = SUI_SKT_DEFINE;
    newtag->obj = sui_obj_new_vmt(UL_CAST_UNQ1(sui_obj_vmt_t *, &sui_define_vmt_data));
    if (!newtag->obj)
      return -SUI_SXML_ERR_MEMORY;
    newtag->vmt = UL_CAST_UNQ1(sui_obj_vmt_t *, &sui_define_vmt_data);
  } else {
    sui_sxml_tag_t *stack = newtag->prev;
    if (!strcmp(newtag->tag_name, (doc->ddesc->tags[doc->ddesc->idhead]).tag)) {
      newtag->id = doc->ddesc->idhead;
      if (stack) /* not first tag in stack */
        newtag->flags = SUI_SXML_FLG_SKIP;
      return 0;
    }

    while (stack && (stack->flags & SUI_SXML_FLG_SKIP))   /* skip all special tags in stack to find tag parent */
      stack = stack->prev;
    if (!stack)
      return -SUI_SXML_ERR_UNKNOWN;

    newtag->parchild = sui_sxml_find_child_taginfo(doc->ddesc, stack, newtag->tag_name);   /* fill taginfo */
    if (!newtag->parchild) {
      newtag->flags |= SUI_SXML_FLG_UNKNOWN;
      return -SUI_SXML_ERR_BADTAG; /* Won't we skip unknown tag and only warn about it ??? */
    }

    newtag->id = newtag->parchild->tag_id;
    if (newtag->parchild->flags & SUI_SXML_CHFLG_SKIP)
      newtag->flags |= SUI_SXML_FLG_SKIP;

    newtag->vmt = UL_CAST_UNQ1(sui_obj_vmt_t *, newtag->parchild->objvmt);
    if (newtag->vmt) {
      newtag->obj = sui_obj_new_vmt(newtag->vmt);
//ul_logdeb("New Obj (%p) from VMT(%s)\n",newtag->obj,newtag->vmt->vmt_obj_tinfo->name);
      if (!newtag->obj)
        return -SUI_SXML_ERR_MEMORY;
    }
  }
  return 0;
}


/**
 * sui_sxml_process_tag_value - Process tag value
 * @doc: Pointer to a sxml document structure.
 * @tag: Pointer to a tag structure.
 *
 * The function processes tag value in special processing function or in common universal function
 * according to tag description.
 *
 * Return Value: The function returns negative value of SUI_SXML_ERR_ error code.
 *
 * File: sui_sxml.c
 */
int sui_sxml_process_tag_value(sui_sxml_doc_t *doc, sui_sxml_tag_t *tag)
{
  sui_sxml_tag_t *prevtag = sui_sxml_get_prev_tag(tag);
  int ret = 0;
  if (!tag || !tag->parchild) return -1; /* skip with error */
  if (tag->vmt == NULL) return 0; /* skip special tags without error */

  if (tag->parchild->flags & SUI_SXML_CHFLG_SUBTAGS) {
    if (tag->tag_value.valtype == SUI_SXML_VALUE_ID) { /* process link to namespace */
      ns_object_t *obj = ns_find_any_object_by_type_and_name(
                        tag->vmt->vmt_typeid, tag->tag_value.data.string);
      if (!obj) return -SUI_SXML_ERR_NONSOBJ;
      /* remove old object - dec refcnt */
      tag->vmt->dec_refcnt(tag->obj);
      /* set object from namespace */
      tag->vmt->inc_refcnt(obj->ptr);
      tag->obj = obj->ptr;
    }

  } else {/* process only direct value */
    ret = sui_sxml_value_to_object(&tag->tag_value, &tag->obj, tag->vmt);
    if (ret) return ret;
  }

/* add to parent, translate value id to pointer to object in namespace */
  if (tag->parchild->offset == SUI_SXML_PROCESS_FCN) {
    //ul_logdeb("saving object to parent special processing\n");
    ret = doc->ddesc->spfcn(doc, tag);
  } else if (prevtag && prevtag->obj &&
             tag->parchild->offset != SUI_SXML_NO_ADD_TO_TAG) { /* save poiters or copy data to the parent */
//    sui_typeid_t tid = tag->vmt->vmt_typeid;
    void *parptr = prevtag->obj;
    if (tag->parchild->ptr_offset > 0)
      parptr = *(void **)(parptr + tag->parchild->ptr_offset);
    parptr += tag->parchild->offset;

    if (tag->parchild->flags & SUI_SXML_CHFLG_POINTER) {
    //ul_logdeb("save object data pointer to the parent\n");
      tag->vmt->dec_refcnt(*(void **)parptr);
      tag->vmt->inc_refcnt(tag->obj);
      *(void **)parptr = tag->obj;
//      tag->flags |= SUI_SXML_FLG_SAVE_IN_PAR;
    } else {
      //sui_typeid_info_t const *typei = sui_typeid_get_info(tag->parchild->type);
      int size = tag->vmt->vmt_obj_tinfo->obj_size;
      //ul_logdeb("copy %d bytes of data to the parent (0x%p) from (0x%p)\n", size, parptr, tag->obj);
      memcpy(parptr, tag->obj, size);
    }
  }
  return ret;
}


/**
 * sui_sxml_process_value - Process the current value in the sxml document
 * @doc: Pointer to a sxml document structure.
 *
 * The function saves the last read value to tag or processes one item of an array.
 *
 * Return Value: The function returns negative value of SUI_SXML_ERR_ error code.
 *
 * File: sui_sxml.c
 */
int sui_sxml_process_value(sui_sxml_doc_t *doc)
{
  sui_sxml_tag_t *curtag = doc->tag;
  int ret = 0;
  
  if (curtag->id == SUI_SKT_INCLUDE) {
    sui_sxml_value_clear(&curtag->tag_value);
    /* no action - action is in close tag fcn */
    memcpy(&curtag->tag_value, &doc->curval, sizeof(sui_sxml_value_t));
    doc->curval.valtype = SUI_SXML_VALUE_NONE;
    doc->curval.data.string = NULL;

  } else if (curtag->id == SUI_SKT_DEFINE) {
    sui_sxml_value_clear(curtag->obj);
    memcpy(curtag->obj, &doc->curval, sizeof(sui_sxml_value_t));
    doc->curval.valtype = SUI_SXML_VALUE_NONE;
    doc->curval.data.string = NULL;

  } else {
//ul_logdeb("Process value valtype='%d'\n", doc->curval.valtype);
    /* process normal tag value - if object is dirval and it will not be add to namespace and it isn't pointer - add value directly to parent object */
  
    if (!curtag->parchild) /* is it internal or syntax error ??? */
      return -SUI_SXML_ERR_INTERNAL;

    if (curtag->tag_value.valtype == SUI_SXML_VALUE_NONE) {
      if (curtag->tag_valcomb || doc->curval.valtype == SUI_SXML_VALUE_CHARACTER) {
        ret = -SUI_SXML_ERR_SYNTAX;
      } else if (doc->curval.valtype != SUI_SXML_VALUE_NONE) {/* move doc value to tag value */
        memcpy(&curtag->tag_value, &doc->curval, sizeof(sui_sxml_value_t));
        memset(&doc->curval, 0, sizeof(sui_sxml_value_t));
      } else {
        ret = -SUI_SXML_ERR_INTERNAL; /* VALUE_NONE ... */
      }
    } else {
      if (doc->curval.valtype == SUI_SXML_VALUE_CHARACTER) {
        if (curtag->tag_valcomb) {
          ret = -SUI_SXML_ERR_SYNTAX;
        } else {
          sui_sxml_value_resolve_id(&curtag->tag_value);
          curtag->tag_valcomb = doc->curval.data.ch;
        }
      } else if (doc->curval.valtype != SUI_SXML_VALUE_NONE) {
        if (curtag->tag_valcomb) {
          /* ARRAY */
          if ((curtag->tag_valcomb == ',') &&
              (curtag->parchild->flags & SUI_SXML_CHFLG_ARRAY)) {/* process tag_value and save doc value to tag value */
//ul_logdeb("array item\n");
            ret = sui_sxml_process_tag_value(doc, curtag);
            if (curtag->vmt->dec_refcnt(curtag->obj) != SUI_REFCNTIMPOSIBLE) {
              curtag->obj = sui_obj_new_vmt(curtag->vmt);
            }
            memcpy(&curtag->tag_value, &doc->curval, sizeof(sui_sxml_value_t));
            memset(&doc->curval, 0, sizeof(sui_sxml_value_t));
          } else {/* combine */
            sui_sxml_value_resolve_id(&doc->curval);
            ret = sui_sxml_combine_values(&curtag->tag_value, &doc->curval, curtag->tag_valcomb);
            memset(&doc->curval, 0, sizeof(sui_sxml_value_t));
            curtag->tag_valcomb = 0;
          }
        } else {
          ret = -SUI_SXML_ERR_SYNTAX;
        }
      } else {
        ret = -SUI_SXML_ERR_INTERNAL;
      }
    }
  }
  return ret;
}


/**
 * sui_sxml_inherit_object - Clone inherited object to a new one
 * @totag: Pointer to an output tag.
 * @from: Name of an object for inheriting.
 *
 * The function tries to find object in namespace by its name and creates a new object
 * as a copy of the searched object.
 *
 * Return Value: The function returns negative value of SUI_SXML_ERR_ error code.
 *
 * File: sui_sxml.c
 */
int sui_sxml_inherit_object(sui_sxml_doc_description_t *ddesc, sui_sxml_tag_t *totag, char const *from)
{
  ns_object_t *obj = ns_find_any_object_by_type_and_name(totag->vmt->vmt_typeid, from);
  sui_sxml_tag_info_t const *taginfo = sui_sxml_find_taginfo_by_id(ddesc, totag->id);
  if (!obj) return -SUI_SXML_ERR_NONSOBJ;
  
  if (taginfo && taginfo->child) {/* structure */
    void *pfrom, *pto;
    sui_sxml_tag_info_child_t const *tchi = taginfo->child;
    while (tchi && (tchi->tag_id!=SUI_SXML_ENDOFCHILDREN)) {/* for each item */
      if (tchi->offset >= 0) {       /* copy only field with real pointer - TODO: support for other fields */
        pfrom = obj->ptr;
        pto = totag->obj;
        if (tchi->ptr_offset > 0) {
          pfrom = *(void **)(pfrom+tchi->ptr_offset);
          pto = *(void **)(pto+tchi->ptr_offset);
        }
        pfrom += tchi->offset;
        pto += tchi->offset;
        if (tchi->flags & SUI_SXML_CHFLG_POINTER) {
          memcpy(pto, pfrom, sizeof(void *));
          tchi->objvmt->inc_refcnt(*(void **)pfrom);
        } else {
          memcpy(pto, pfrom, tchi->objvmt->vmt_obj_tinfo->obj_size);
        }
      }
      tchi++;
    }
  } else {/* direct value */
    memcpy(totag->obj, obj->ptr, totag->vmt->vmt_obj_tinfo->obj_size);
  }
  return 0;
}

/**
 * sui_sxml_process_attrib - Process tag attribute
 * @doc: Pointer to a sxml document structure.
 *
 * The function processes an attribut of the current tag (global or tag special).
 * 
 * Return Value: The function returns negative value of SUI_SXML_ERR_ error code.
 *
 * File: sui_sxml.c
 */
int sui_sxml_process_attrib(sui_sxml_doc_t *doc)
{
  sui_sxml_tag_t *curtag = doc->tag;
  sui_sxml_tag_info_t const *taginfo;
  int ret = 0;

  if (!curtag) return -SUI_SXML_ERR_INTERNAL;
  taginfo = sui_sxml_find_taginfo_by_id(doc->ddesc, curtag->id);

  if ((curtag->id == SUI_SKT_UNKNOWN) || (curtag->id == SUI_SKT_INCLUDE))
    return ret;
  if ((curtag->flags & SUI_SXML_FLG_SKIP) && (curtag->id!= SUI_SKT_DIV))
    return ret;

/* common attributes */
  /* add tag object to namespace */
  if (!strcmp(doc->curattr.name, "name")) {
/* set name of object for saving into NS */
    curtag->ns_name = doc->curattr.value;
    doc->curattr.value = NULL;

  } else if (!strcmp(doc->curattr.name, "namespace")) {/* change current namespace local/global */
    curtag->flags |= SUI_SXML_FLG_NAMESPACE;
    if (!strcmp(doc->curattr.value, "global")) {
      sui_sxml_current_ns = &ns_global_namespace;
    } else if (!strcmp(doc->curattr.value, "local")) {
      sui_sxml_current_ns = &ns_local_namespace;
    }

  } else if (!strcmp(doc->curattr.name, "redefname")) {/* value is name in namespace */
//ul_logdeb("-redef attribute\n");
    curtag->flags |= SUI_SXML_FLG_REDEF2NS;
    curtag->ns_name = doc->curattr.value;
    doc->curattr.value = NULL;

  } else if (!strcmp(doc->curattr.name, "redefcontent")) {
    curtag->flags |= SUI_SXML_FLG_REDEFCONTENT;
    curtag->ns_name = doc->curattr.value;
    doc->curattr.value = NULL;

  } else if (!strcmp(doc->curattr.name, "cond")) { /* tag conditioned on result of term */
    sui_term_value_t res;
    sui_term_t *term = sui_term_create_from_prefix_string(doc->curattr.value);
    sui_term_value_init(&res);
    if (term && (sui_term_compute_term(term, &res) == SUI_RET_OK)) {
      if ((res.type==SUI_TERMVAL_LONG && res.value.slong==0) ||
          (res.type==SUI_TERMVAL_ULONG && res.value.ulong==0) ||
          (res.type==SUI_TERMVAL_UTF8 && sui_utf8_length(res.value.u8)==0))
        curtag->flags |= SUI_SXML_FLG_NO_PROCESS;  /* set tag as don't resolve */
    } else {
      ul_logdeb("compute term (%p)=%d\n", term, sui_term_compute_term(term, &res));
      sui_sxml_doc_warning(doc, SUI_SXML_ERR_BADTERM, doc->curattr.value);
    }
    sui_term_value_clear(&res);
    sui_term_dec_refcnt(term);
    sui_sxml_attrib_clear(&doc->curattr);
    return ret;

  } else if (taginfo && !taginfo->child && !strcmp(doc->curattr.name, "value")) {/* only dirval - simple tags */
    /* create value from attribute string and clear string */
    sui_sxml_value_clear(&doc->curval);
    sui_sxml_value_translate(&doc->curval, &doc->curattr.value, -1);
    ret = sui_sxml_process_value(doc);

//   } else if (curtag->parchild && (curtag->parchild->flags & SUI_SXML_CHFLG_ARRAY) && !strcmp(doc->curattr.name, "from")) {
//     /* TODO: 'from' for arrays ... */
//     sui_typeid_convert(SUI_TYPE_LONG, &curtag->user, SUI_TYPE_UTF8, &doc->curattr.value, 1);
//     curtag->flags |= SUI_SXML_FLG_USER_VALUE;
// ul_logdeb("-from attribute\n");

  } else if (curtag->id != doc->ddesc->idhead) {
    ret = doc->ddesc->pafcn(doc);
    if (ret==-SUI_SXML_ERR_NOPROCESS) {
      ret = SUI_SXML_ERR_OK;
      //ul_logdeb("Process other attribute '%s'\n", doc->curattr.name);
      if (curtag->obj && curtag->vmt && !strcmp(doc->curattr.name, "inherit")) {
        //ul_logdeb("-inherit attribute (%s)\n",doc->curattr.value);
        ret = sui_sxml_inherit_object(doc->ddesc, curtag, doc->curattr.value);
      } else {
      /* all other attributes - test attribute name to child name ... simplified properties */
        sui_sxml_tag_info_child_t const *child =
              sui_sxml_find_child_taginfo(doc->ddesc, curtag, doc->curattr.name);
        if (child && doc->curattr.value && (child->offset>=0)) {/* attribute is one of tag chilren - only simple fields */
          sui_sxml_value_clear(&curtag->tag_value);
          int rc = sui_sxml_value_translate(&curtag->tag_value,
                            &doc->curattr.value, strlen(doc->curattr.value));
          if (!rc) {
            sui_obj_vmt_t *vmt = UL_CAST_UNQ1(sui_obj_vmt_t *, child->objvmt);
            if ((curtag->tag_value.valtype == SUI_SXML_VALUE_ID) &&
                (child->flags & SUI_SXML_CHFLG_NO_OBJID)) {
              curtag->tag_value.valtype = SUI_SXML_VALUE_STRING;
            }
            void *ptr = curtag->obj;
            //ul_logdeb("known attrb - translated ...\n");
            if (child->ptr_offset > 0)
              ptr = *(void **)(ptr + child->ptr_offset);
            ptr += child->offset;

            sui_sxml_value_resolve_id(&curtag->tag_value);
            switch (curtag->tag_value.valtype) {
              case SUI_SXML_VALUE_ID:
                {
                  ns_object_t *nsobj = NULL;
                  if (vmt->vmt_typeid == SUI_TYPE_WIDGET)
                    nsobj = ns_find_widget_in_ns(curtag->tag_value.data.string);
                  else
                    nsobj = ns_find_any_object_by_type_and_name(
                                vmt->vmt_typeid, curtag->tag_value.data.string);
                  if (nsobj) {
                    //ul_logdeb("To %p , from %p\n",ptr,nsobj->ptr);
                    vmt->dec_refcnt(*(void **)ptr);
                    vmt->inc_refcnt(nsobj->ptr);
                    *(void **)ptr = nsobj->ptr;
                  } else {
                    ul_logerr("Attr.conversion (ID->NSOBJ) error... (tid=%ld,id=%s)\n",
                              (long)vmt->vmt_typeid, curtag->tag_value.data.string);
                    ret = -SUI_SXML_ERR_BADATTR;
                  }
                }
                break;
              case SUI_SXML_VALUE_NUMBER:
                ret = sui_typeid_convert(vmt->vmt_typeid, ptr, SUI_TYPE_ULONG,
                                          &curtag->tag_value.data.number, 1);
                if (ret) {
                  ul_logerr("Attr.conversion (ULONG) error... (tid=%ld,num=%lu)\n",
                            (long)vmt->vmt_typeid, curtag->tag_value.data.number);
                  ret = -SUI_SXML_ERR_TYPECONV;
                }
                break;
              case SUI_SXML_VALUE_SIGNUM:
                {
                  long negval = -curtag->tag_value.data.number;
                  ret = sui_typeid_convert(vmt->vmt_typeid, ptr, SUI_TYPE_LONG,
                                            &negval, 1);
                  if (ret) {
                    ul_logerr("Attr.conversion (LONG) error... (tid=%ld,num=%ld)\n",
                              (long)vmt->vmt_typeid, negval);
                    ret = -SUI_SXML_ERR_TYPECONV;
                  }
                }
                break;
              case SUI_SXML_VALUE_STRING:
                ret = sui_typeid_convert(vmt->vmt_typeid, ptr, SUI_TYPE_ASCII,
                                          curtag->tag_value.data.string, 1);
                if (ret) {
                  ul_logerr("Attr.conversion (TEXT) error... (tid=%ld,str=%s)\n",
                            (long)vmt->vmt_typeid, curtag->tag_value.data.string);
                  ret = -SUI_SXML_ERR_TYPECONV;
                }
                break;
              default:
                return -SUI_SXML_ERR_BADATTR;
            }
          }
          sui_sxml_value_clear(&curtag->tag_value);
        } else {
          return -SUI_SXML_ERR_BADATTR;
        }
      }
    }
  }
  sui_sxml_attrib_clear(&doc->curattr);
  return ret;
}

/**
 * sui_sxml_close_tag - Process ETAG token and closes tag
 * @doc: Pointer to a sxml document structure.
 * @etagname: Name of a closing tag in sxml file.
 *
 * The function processes ETAG token. It check if the closing tag is the same as currently opened
 * and then does all necessary steps to closing tag (removes rag from stack, destroys it and
 * saves tag object to namespace and to parent object).
 *
 * Return Value: The function returns negative value of SUI_SXML_ERR_ error code.
 *
 * File: sui_sxml.c
 */
int sui_sxml_close_tag(sui_sxml_doc_t *doc, char *etagname)
{
  sui_sxml_tag_t *curtag;
  int ret = 0;

// ul_logdeb("Close tag '%s'...\n", etagname);
  
  curtag = doc->tag;
  if (etagname && strcmp(curtag->tag_name, etagname)) {/* ERROR tags crossover */
    return -SUI_SXML_ERR_TAGCROSS;
  }

/* SXML file including and processing */
  if (curtag->id == SUI_SKT_INCLUDE) {
    curtag = sui_sxml_tag_remove_from_doc(doc);
    if (curtag->tag_value.valtype == SUI_SXML_VALUE_STRING && curtag->tag_value.data.string) {
      char *newfile = NULL;
      long newfilesize = 0;
      int rnf = sui_file_srctable_find(curtag->tag_value.data.string, -1,
                                       doc->source->source.file.filename, -1,
                                       &newfile, &newfilesize, NULL);
      sui_sxml_destroy_tag(curtag);
      if (rnf>=0) {
        ret = sui_sxml_parse_one_file(doc, newfile, newfilesize);
        if (rnf>0) sui_free(newfile);
        doc->token.size = 1;
      } else
        ret = -SUI_SXML_ERR_NOFILE;
    } else
      ret = -SUI_SXML_ERR_NOFILE; /* otherwise ERROR */

  } else {
/* process value and save it to the parent object */
    if (!(curtag->flags & SUI_SXML_FLG_NO_PROCESS) &&
         curtag->id != SUI_SKT_DEFINE && curtag->id != doc->ddesc->idhead &&
         curtag->id != SUI_SKT_DIV) {
      ret = sui_sxml_process_tag_value(doc, curtag);
    }

/* add to namespace */
    if (!(curtag->flags & SUI_SXML_FLG_NO_PROCESS) &&
         curtag->ns_name && curtag->obj) {/* add tag object into namespace */
      ns_object_t *nsobj = NULL;
      sui_typeid_t objtype = 0;

      if (curtag->id == SUI_SKT_DEFINE) {
        objtype = SUI_TYPE_DEFINE;
        sui_sxml_current_ns = &ns_global_namespace;
        curtag->flags |= SUI_SXML_FLG_NAMESPACE;
      } else {
        objtype = curtag->vmt->vmt_typeid;
      }
      /* try find object with the same name in NS (the same NS) */
//      nsobj = ns_find_object_by_type_and_name(sui_sxml_current_ns, objtype,
//                                               curtag->ns_name);
      nsobj = ns_find_object_by_type_and_path(sui_sxml_current_ns, objtype,
                                               curtag->ns_name);

      if (nsobj && !(curtag->flags & (SUI_SXML_FLG_REDEF2NS | SUI_SXML_FLG_REDEFCONTENT))) {
        ret = SUI_SXML_ERR_ISINNS;
      } else {
        if (nsobj) { // SUI_SXML_FLG_REDEF2NS
          if ((curtag->flags & SUI_SXML_FLG_REDEFCONTENT)) {/* it can be dangerous, if someone will hold pointer to raw data ... */
            if (objtype == SUI_TYPE_UTF8) {
              utf8 *pu8new = curtag->obj;
              utf8 *pu8old = nsobj->ptr;
              int u8tnew = sui_utf8_get_type(pu8new);
              int u8told = sui_utf8_get_type(pu8old);
              if ((u8tnew == UTF8_DYNAMIC_STRUCT || u8tnew == UTF8_DYNAMIC_FULL) &&
                  (u8told == UTF8_DYNAMIC_STRUCT || u8told == UTF8_DYNAMIC_FULL)) {
                sui_utf8_t u8tmp;
                sui_refcnt_t rctmp;
                memcpy(&u8tmp, pu8old, sizeof(sui_utf8_t));
                u8tmp.refcnt = pu8new->refcnt;
                rctmp = pu8old->refcnt;
                memcpy(pu8old, pu8new, sizeof(sui_utf8_t));
                pu8old->refcnt = rctmp;
                memcpy(pu8new, &u8tmp, sizeof(sui_utf8_t));
              } else
                ret = -SUI_SXML_ERR_NOPROCESS;
            } else
              ret = -SUI_SXML_ERR_BADATTR;
          } else {/* redef object */
            if (nsobj->vmt->dec_refcnt(nsobj->ptr) == SUI_REFCNTIMPOSIBLE) {
              sui_obj_destroy_vmt(nsobj->ptr, nsobj->vmt);
            }
            nsobj->ptr = curtag->obj;
            nsobj->vmt->inc_refcnt(curtag->obj);
            curtag->flags |= SUI_SXML_FLG_SAVE_IN_NS;
          }
        } else {
/* split name to path and name */
          int p2n = 0;
          int addret;
          while (*(curtag->ns_name+p2n)) p2n++; /* find end */
          while ((p2n>0) && !sui_is_pathdiv(*(curtag->ns_name+p2n))) p2n--; /* find last part */
          if (sui_is_pathdiv(*(curtag->ns_name+p2n))) {
            addret = 1; p2n++;
          } else
            addret = 0;

          if (p2n) {
            nsobj = ns_create_object(curtag->ns_name+p2n, curtag->obj, objtype, NSOF_DYNAMIC_NAME | NSOF_DYNAMIC_DATA);
            if (addret) p2n--;
            addret = ns_add_object_to_ns_by_path(sui_sxml_current_ns, nsobj, curtag->ns_name, p2n);
          
          } else {
            nsobj = ns_create_object(curtag->ns_name, curtag->obj, objtype,
                                      NSOF_COPY_DYNAMIC_NAME | NSOF_DYNAMIC_DATA);
            if (nsobj) curtag->ns_name = NULL; /* it is directly used in namespace */
            addret = ns_add_object_to_ns(sui_sxml_current_ns, nsobj);
          }
          if (addret>=0) {
              curtag->flags |= SUI_SXML_FLG_SAVE_IN_NS;
          } else {
            sui_sxml_doc_warning(doc, SUI_SXML_ERR_ADD2NS, curtag->ns_name);
            if (nsobj) {
              nsobj->ptr = NULL;
              ns_destroy_object(nsobj);
            }
          }
        }
      }
    }

/* return original namespace before tag */
    if (curtag->flags & SUI_SXML_FLG_NAMESPACE) {
      sui_sxml_current_ns = &ns_local_namespace;
    }

    if (curtag->vmt && curtag->obj) {
      if (curtag->vmt->dec_refcnt(curtag->obj) == SUI_REFCNTIMPOSIBLE) {
        if (!(curtag->flags & SUI_SXML_FLG_SAVE_IN_PAR) &&
            !(curtag->flags & SUI_SXML_FLG_SAVE_IN_NS)) {

          sui_obj_destroy_vmt(curtag->obj, curtag->vmt);
        }
      }
    }

    sui_sxml_destroy_tag(sui_sxml_tag_remove_from_doc(doc));
  }
  return ret;
}

/******************************************************************************
 * sxml main parse function
 ******************************************************************************/
/**
 * sui_sxml_tokenize - Read one token (delimited by one of separating characters)
 * @doc: Pointer to a sxml document structure.
 * @separs: String of separating characters.
 *
 * The function parses sxml file and reads one token delimited by one of separating characters.
 * The read token is returned in doc->token structure.
 *
 * Return Value: The function return length of read token or a negative value as an error.
 *
 * File: sui_sxml.c
 */
long sui_sxml_tokenize(sui_sxml_doc_t *doc, char *separs)
{
  long size = 0, sizemax = SUI_FILE_BUFFER_SIZE;
  int ch;
  int token = 0, string = 0;
  char **buf = NULL;

  if (!doc || !doc->source || !separs) return -1;
  if (sui_file_isfile(doc->source)) {
    if (!doc->source->source.file.file) return -1;
  } else {
    if (!doc->source->source.memory.addr || !doc->source->source.memory.size) return -1;
  }

  buf = &doc->token.buffer;
  doc->token.size = 0;
  if (doc->token.buffer) {
    sui_free(doc->token.buffer);
    doc->token.buffer = NULL;
  }

  while (1) {
    if (sui_file_isfile(doc->source)) {
      ch = getc(doc->source->source.file.file);
    } else {
      ch = (doc->source->position < doc->source->source.memory.size) ?
           doc->source->source.memory.addr[doc->source->position]:
           EOF; /* EOF */
    }
    if (ch == EOF) break;

    doc->source->position++;
    if (ch == '\n') doc->source->line++;
    if (doc->clevel) {
      char *csep = *buf;
      *csep = *(csep+1); *(csep+1) = *(csep+2); *(csep+2)=*(csep+3); *(csep+3)=ch;
      if (ch=='>' && *(csep+1)=='-' && *(csep+2)=='-') {
        doc->clevel--;
        if (!doc->clevel) {
          size = 0;
          token = 0;
          doc->token.bsep = ' ';
        }
      }
      if (*csep=='<' && ch=='-' && *(csep+1)=='!' && *(csep+2)=='-') {
        doc->clevel++;
      }
    } else {
      if ((sui_file_token_separator(separs, ch)) && (!string || (string && ch=='"')) ) {// char is separator
        if (token) {/* return buffer and size */
          /* clear white spaces */
          while ((ch == ' ') || (ch < 0x20)) {
            if (sui_file_isfile(doc->source)) {
              ch = getc(doc->source->source.file.file);
            } else {
              ch = (doc->source->position < doc->source->source.memory.size) ?
                    doc->source->source.memory.addr[doc->source->position]:
                    EOF; /* EOF */
            }
            doc->source->position++;
            if (ch == '\n') doc->source->line++;
          }
          *(*buf+size) = 0;
          doc->token.esep = ch;
          if (sui_file_isfile(doc->source)) {
            ungetc(ch, doc->source->source.file.file);
//          } else {
//            /* unget for file in memory */
          }
          doc->source->position--;
          if (ch == '\n') doc->source->line--; /* repair number of lines and position */
          doc->token.size = size;
          return size;
        } else {/* skip separators before token */
          doc->token.bsep = ch; /* save last separator before token */
          if (ch == '"') string = 1;
          else string = 0;
          /* nothing */
        }
      } else {/* add character into buffer */
        if (!token) token = 1;
        if (!(*buf) || size == sizemax-2) {/* realocate buffer (-2 => space for ending zero) */
          *buf = realloc(*buf, sizemax*2);
          if (*buf)
            sizemax = sizemax*2;
          else {
            doc->token.size = 0;
            return -2;
          }
        }
        /* string */
        if (ch=='"') {
          if (!string && size && *(*buf+size-1)=='"') {/* two quotation marks are represented as quotation mark and no as beginning or ending of string */
            string = 1;
            size--;
          } else {
            if (string) string = 0;
            else string = 1;
          }
        }
        *(*buf+size) = ch;
        size++;
        if (size==3 && **buf=='!' && *(*buf+1)=='-' && ch=='-') {
          doc->clevel = 1;
          size = 0;
          strcpy(*buf, "____");
        }
      }
    }
  }
  doc->token.size = size;
  return size;
}


/******************************************************************************
 * sxml main parsing functions
 ******************************************************************************/
/**
 * sui_sxml_parse_one_file - Parse one sxml file or memory block with sxml code
 * @doc: Pointer to a sxml document structure.
 * @fname_memory: Name of a sxml file or pointer to memory with sxml code.
 * @size: Size of memory with sxml code or SUI_FILE_SOURCE_FILE for parsing sxml file.
 *
 * The function parses one sxml file or memory block with sxml code.
 *
 * Return Value: The function returns zero as success or a negative sxml error code.
 *
 * File: sui_sxml.c
 */
int sui_sxml_parse_one_file(sui_sxml_doc_t *doc, char *fname_memory, long size)
{
  int isclosed=0;
  int isattrib=0; /* is one if we wait for attribute value */
  int ret = 0;

/* check SHA1 of file */
//  if (sui_file_check_consistency(fname_memory, 1)<0) {
//    ul_logerr("SXML file '%s' is inconsistent!\n", fname_memory);
//    return -SUI_SXML_ERR_BADCHKSUM;
//  }

/* check inputs */
  if (!doc || !fname_memory || !size)
    return -SUI_SXML_ERR_BADARGS;
/* open sxml file and add it into doc stack */
  if (!(doc->source = sui_file_open(fname_memory, size, doc->source, SUI_FM_READ))) {
    ul_logerr("SXML doc '%s' wasn't found.\n", fname_memory);
    return -SUI_SXML_ERR_NOFILE;
  }
  sui_file_assign_type(doc->source, SUI_FILE_TGRP_SOURCE | SUI_FILE_TID_SXML);
// strip first white spaces before first tag
  doc->token.size = sui_file_skip_upto(doc->source, "<");
  doc->state = SUI_SXML_STATE_OUTTAG;
/* parsing cycle */
  do {
    switch (doc->state) {
      case SUI_SXML_STATE_ERROR:
        doc->token.size = 0;
        break;
      case SUI_SXML_STATE_OUTTAG:
        doc->token.size = sui_sxml_tokenize(doc, "<> \t\n\r=,");
        break;
      default:
        doc->token.size = sui_sxml_tokenize(doc, "<> \t\n\r=");
        break;
    }

    if (doc->token.buffer && doc->token.size > 0) {
      isclosed = 0;
//ul_logdeb(" . Token(l.%d) : '%s' (size=%d,b='%c',a='%c')\n", doc->source->line, doc->token.buffer, (int) doc->token.size, doc->token.bsep, doc->token.esep);
      /* process token - determine next state */
      switch (doc->state) {
      case SUI_SXML_STATE_ERROR:
        /* ??? somewhere in xml parsing was error */
        break;
      case SUI_SXML_STATE_OUTTAG: /* value, btag, etag was read ... */
        if (doc->token.bsep=='<') {/* BTAG or ETAG was read */
          isclosed=0;
// ETAG
          if (*doc->token.buffer == '/') {/* ETAG */
            if (doc->token.esep == '>')
              doc->state = SUI_SXML_STATE_OUTTAG;
            else
              doc->state = SUI_SXML_STATE_INETAG; /* ??? */

            if ((ret = sui_sxml_close_tag(doc, doc->token.buffer+1))<0) {
              doc->state = SUI_SXML_STATE_ERROR;
              doc->token.size = -ret;
              break;
            }
            break;
          }
// BTAG or ended BTAG
          /* now, because we will lose token buffer */
          if (*(doc->token.buffer+doc->token.size-1)=='/') {/* tag is BTAG and ETAG without attributes */
            isclosed=1;
            doc->token.size--;
            *(doc->token.buffer+doc->token.size)=0;
          }
/* call open tag function */
//ul_logdeb("Process begin tag '%s'\n",doc->tag->tag_name);
          if ((ret = sui_sxml_open_tag(doc)) < 0) {
            doc->state = SUI_SXML_STATE_ERROR;
            doc->token.size = -ret;
            break;
          }
  
          if (doc->token.esep == '>') {/* tag is closed */
            if (isclosed) {/* close tag - */
              if ((ret = sui_sxml_close_tag(doc, doc->token.buffer+1))<0) {
                doc->state = SUI_SXML_STATE_ERROR;
                doc->token.size = -ret;
                break;
              }
            }
            doc->state = SUI_SXML_STATE_OUTTAG;
          } else {/* tag still opened */
            doc->state = SUI_SXML_STATE_INBTAG;
          }
        } else {/* no '<' - value or syntax error */
          if (doc->tag) {/* some tag is opened*/
            sui_sxml_value_clear(&doc->curval);
            /* save and process value string to value */
            sui_sxml_value_translate(&doc->curval, &doc->token.buffer, doc->token.size);
/* process value */
            if (!(doc->tag->flags & SUI_SXML_FLG_UNKNOWN)) {
//ul_logdeb("Process value type %d\n", doc->curval.valtype);
              if (doc->token.bsep == ',' && doc->tag) {/* for arrays */
                doc->tag->tag_valcomb = ',';
              }
              if ((ret = sui_sxml_process_value(doc)) < 0) {
                doc->state = SUI_SXML_STATE_ERROR;
                doc->token.size = -ret;
                break;
              }
              if (doc->token.esep == ',' && doc->tag) {/* for arrays */
                doc->tag->tag_valcomb = ',';
              }
            }
            doc->state = SUI_SXML_STATE_OUTTAG;
            break;
          } else {/* syntag error - no tag opened */
            doc->state = SUI_SXML_STATE_ERROR;
            doc->token.size = SUI_SXML_ERR_SYNTAX;
          }
        }
        break;

      case SUI_SXML_STATE_INBTAG: /* attribute name was read ... */

        /* now, because we will lose token buffer */
        if (*(doc->token.buffer+doc->token.size-1)=='/') {/* tag is BTAG and ETAG without attributes */
          isclosed=1;
          doc->token.size--;
          *(doc->token.buffer+doc->token.size)=0;
        }

        /* save attribute to doc->curattr, if it has value - wait for it, else process and wait for another */
        if (!isattrib) {/* wait for attribute name */
          sui_sxml_attrib_clear(&doc->curattr);
  
          doc->curattr.name = doc->token.buffer;
          doc->token.buffer = NULL;
        /* attribute has got value */
          if (doc->token.esep=='=') {
            if (isclosed) {
              doc->state = SUI_SXML_STATE_ERROR;
              doc->token.size = SUI_SXML_ERR_SYNTAX;
            } else {
              doc->state = SUI_SXML_STATE_INBTAG;
              isattrib = 1;
            }
            break;
          }
            /* process attribute without attribute value - TODO: don't support this feature */
          if (doc->token.size) {
/* process attribute */
            if ((ret = sui_sxml_process_attrib(doc)) < 0) {
              doc->state = SUI_SXML_STATE_ERROR;
              doc->token.size = -ret;
              break;
            }
          } else {
            doc->token.size = 1;
          }
        } else {/* read attribute value */
          if (!doc->token.size) {
            doc->state = SUI_SXML_STATE_ERROR;
            doc->token.size = SUI_SXML_ERR_SYNTAX;
            break;
          }

          if ((*doc->token.buffer == '"') &&
              (*(doc->token.buffer+doc->token.size-1)=='"')) {/* trim value quotes */
                int i=0;
            while (i < doc->token.size-2) {
              *(doc->token.buffer+i) = *(doc->token.buffer+i+1);
              i++;
            }
            *(doc->token.buffer+doc->token.size-2)=0;
          }

          doc->curattr.value = doc->token.buffer;
          doc->token.buffer = NULL;
/* proccess attribute */
          if ((ret = sui_sxml_process_attrib(doc)) < 0) {
            doc->state = SUI_SXML_STATE_ERROR;
            doc->token.size = -ret;
            break;
          }
          isattrib = 0;
        }

        if (doc->token.esep=='>') {
          if (isattrib) {
            doc->state = SUI_SXML_STATE_ERROR;
            doc->token.size = SUI_SXML_ERR_SYNTAX;
            break;
          }
          if (isclosed) {/* closing BTAG */
            if ((ret = sui_sxml_close_tag(doc, NULL)) < 0) {
              doc->state = SUI_SXML_STATE_ERROR;
              doc->token.size = -ret;
              break;
            }
          }
          if (doc->tag->flags & SUI_SXML_FLG_NO_PROCESS) {
            doc->state = SUI_SXML_STATE_IGNORE; /* ignore all tokens up to closing ETAG */
            doc->ilevel = 0;
          } else {
            doc->state = SUI_SXML_STATE_OUTTAG; /* tag is open */
          }
        } else {/* else continue in attribute reading */
          doc->state = SUI_SXML_STATE_INBTAG;
        }
        break;

      case SUI_SXML_STATE_INETAG: /* skip all attribute up to end of tag ... */
        if (doc->token.esep== '>') {
          doc->state = SUI_SXML_STATE_OUTTAG;
        }
        break;

      case SUI_SXML_STATE_IGNORE:
        if (doc->token.bsep== '<') {
          if (!strcmp(doc->token.buffer, doc->tag->tag_name)) {
            doc->ilevel++;
          } else if ((*doc->token.buffer == '/') &&
              (!strcmp(doc->token.buffer+1, doc->tag->tag_name))) {/* ETAG */
            if (doc->ilevel)
              doc->ilevel--;
            else {
              if (doc->token.esep == '>')
                doc->state = SUI_SXML_STATE_OUTTAG;
              else
                doc->state = SUI_SXML_STATE_INETAG; /* ??? */
  
              if ((ret = sui_sxml_close_tag(doc, doc->token.buffer+1)) < 0) {
                doc->state = SUI_SXML_STATE_ERROR;
                doc->token.size = -ret;
                break;
              }
            }
          }
        }
        break;
      }
    }
// /*/*
//     if (doc->token.size & SUI_SXML_ERR_WARNING) {/* err code haven't to be in token.size */
//       sui_sxml_doc_warning(doc, doc->token.size, "(SXML parsing)");
//       doc->token.size = 1;
//     }
// */*/
  } while ((doc->state != SUI_SXML_STATE_ERROR) && doc->token.size > 0);

/* write error */
  if (doc->state  == SUI_SXML_STATE_ERROR) {
    sui_sxml_doc_error(doc, doc->token.size, NULL);
    isclosed = -SUI_SXML_ERR_INFILE;
  } else
    isclosed = 0;

/* end of parsing - destroy temporary variables */
  doc->source = sui_file_close(doc->source);
/* clear common attribute and value buffers */
  sui_sxml_value_clear(&doc->curval);
  sui_sxml_attrib_clear(&doc->curattr);
  return isclosed;
}


/**
 * sui_sxml_parse_document - Parse all sxml documents from file or memory block with sxml code
 * @fname: Name of a sxml file or a name in a registered table of sources.
 * @path: String with a document path or NULL. The SXML filename (if it is a file) is combined
 * with the path.
 *
 * The function prepares and creates sxml document structure and DEFINE subnamespace and then
 * it calls parsing function for starting sxml file or memory block with sxml code.
 * After parsing the function destroy sxml document structure.
 *
 * Return Value: The function returns zero as success or a negative sxml error code.
 *
 * File: sui_sxml.c
 */
int sui_sxml_parse_document(sui_sxml_doc_description_t *ddesc, char *fname, char *path)
{
  char *src = NULL;
  long srcsize = 0;
  int ret = 0, rfs = -1;
  sui_sxml_doc_t doc;
/* initiate sxml_doc */
  memset(&doc, 0, sizeof(sui_sxml_doc_t));
  doc.state = SUI_SXML_STATE_OUTTAG;
  doc.ddesc = ddesc;

  if (!ddesc) {
    ul_logerr("SXML tag description must be defined\n");
  }

  sui_sxml_current_ns = &ns_local_namespace;

  if (!(doc.defs = ns_find_subnamespace_by_type(&ns_global_namespace, SUI_TYPE_DEFINE))) {
    if (!ns_add_subnamespace(&ns_global_namespace, doc.defs = ns_create_subnamespace(SUI_TYPE_DEFINE))) {
      sui_sxml_doc_error(&doc, SUI_SXML_ERR_ADD2NS, "define subns");
    }
  }

  rfs = sui_file_srctable_find(fname, -1, path, -1, &src, &srcsize, NULL);
  if (rfs>=0) {
#if defined(SXML_TIME_PROFILE)
    unsigned long btime = get_current_time();
#endif
    /* start parsing */
    ret = sui_sxml_parse_one_file(&doc, src, srcsize);
#if defined(SXML_TIME_PROFILE)
    btime = get_current_time() - btime;
    ul_logdeb("SXML (%s) parsing time = %lu ms\n", src, btime);
#endif
    if (rfs>0) sui_free(src);
  } else
    ret = -SUI_SXML_ERR_NOFILE;
/* finish sxml_doc */
  sui_sxml_clear_tag_lifo(&doc); /* destroy all tags after error */
/* destroy doc - sources, token, curval, curattr */
  if (doc.token.buffer) sui_free(doc.token.buffer);

  return ret;
}




/******************************************************************************
 * sxml term id search and resolve function
 ******************************************************************************/
/**
 * sui_term_resolve_id - Translate identifier to object from namespace
 * @id: An identifier name.
 * @value: Pointer to an output value structure.
 *
 * The function is called from function which creates term structure to resolve
 * identifier and translates it to object from namespace.
 *
 * Return Value: The function returns SUI_RET_.. code.
 *
 * File: sui_sxml.c
 */
int sui_term_resolve_id(char *id, sui_term_value_t *value)
{
  ns_object_t *obj = NULL;
  /* try find id in 'dinfo' subnamespace - if it is here, return it */
  do {
    obj = ns_find_any_object_by_type_and_name(SUI_TYPE_DINFO, id);
    if (obj && obj->ptr) {/* dinfo was found */
      value->type = SUI_TERMVAL_DINFO;
      value->value.dinfo = obj->ptr;
      sui_dinfo_inc_refcnt(value->value.dinfo);
      return SUI_RET_OK;
    }
    obj = ns_find_any_object_by_type_and_name(SUI_TYPE_UTF8, id);
    if (obj && obj->ptr) {/* utf8 was found */
      value->type = SUI_TERMVAL_UTF8;
      value->value.u8 = obj->ptr;
      sui_utf8_inc_refcnt(value->value.u8);
      return SUI_RET_OK;
    }
/* TODO: add number objects searching */

    /* try find define with number (or define to dinfo) */
    obj = ns_find_any_object_by_type_and_name(SUI_TYPE_DEFINE, id);
    if (obj && obj->ptr) {
      sui_sxml_value_t *val = obj->ptr;
      switch (val->valtype) {
        case SUI_SXML_VALUE_SIGNUM:
          value->type = SUI_TERMVAL_LONG;
          value->value.slong = (long) -val->data.number;
          return SUI_RET_OK;
        case SUI_SXML_VALUE_NUMBER:
          value->type = SUI_TERMVAL_ULONG;
          value->value.ulong = (unsigned long) val->data.number;
          return SUI_RET_OK;
        case SUI_SXML_VALUE_STRING:
          value->value.u8 = sui_utf8_dynamic(val->data.string, -1, -1);
          if (value->value.u8) {
            value->type = SUI_TERMVAL_UTF8;
            return SUI_RET_OK;
          } else {
            return SUI_RET_ERR;
          }
        case SUI_SXML_VALUE_ID: /* continue */
          id = val->data.string;
          break;
        default:
          return SUI_RET_ERR;
      }
    }

  } while(obj);
  return SUI_RET_ERR;
}

/******************************************************************************
 * sxml reloading local namespace
 ******************************************************************************/
/**
 * sui_change_local_namespace_from_sxml - Change local namespace from sxml file
 * @name: Name of a sxml file.
 * @par: pointer to structure with SXML application specific tags (sui_sxml_doc_description_t)
 *
 * The function is called as callback function and creates new local namespace, parses sxml document
 * and then destroys created local namespace.
 *
 * Return Value: The function returns zero as success and -1 as an error.
 *
 * File: sui_sxml.c
 */
int sui_change_local_namespace_from_sxml(utf8 *name, void *par)
{
  ns_list_t temp_ns;
  temp_ns = ns_local_namespace;
  ns_local_namespace.subns_root = NULL;
  ul_logdeb("Parse SXML file '%s'\n", sui_utf8_get_text(name));
  if (sui_sxml_parse_document((sui_sxml_doc_description_t *)par, (char *)sui_utf8_get_text(name), NULL)) {
    ul_logerr("Parsing SXML document error!\n");
    // return old local namespace
    ns_clear_namespace(&ns_local_namespace);
    ns_local_namespace.subns_root = temp_ns.subns_root;
    return -1;
  }
  ns_clear_namespace(&temp_ns); // release old local_namespace
  return 0;
}
