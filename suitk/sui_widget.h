/* sui_widget.h
 *
 * SUITK basic widget - header file
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

/* Simple flat UI TK, names of some widget fields are taken from FLTK */
#ifndef _SUITK_WIDGET_H_
  #define _SUITK_WIDGET_H_

#include <device.h>
#include <mwtypes.h>

#include <suiut/sui_objbase.h>
#include <suitk/sui_signalslot.h>
#include <suitk/sui_comdefs.h>
#include <suiut/sui_types.h>
#include <suiut/sui_utf8.h>
#include <suitk/sui_gdi.h>
#include <suitk/sui_event.h>

extern sui_flags_t sui_widget_current_focus_flags;
extern sui_flags_t sui_widget_current_blink_flags;

/******************************************************************************
 * Widget structures
 ******************************************************************************/
/* hevent type */
typedef void sui_hevent_t(struct sui_widget *widget, struct sui_event *event);

/* key-function structure */
struct sui_key_table;

/**
 * struct sui_widget - Widget main descriptive structure
 * @base: Nested basic structure for using signal-slot technic.
 * @refcnt: Widget reference counter.
 * @type: Widget type (See to 'enum widget_type' with 'SUWT_' prefix)
 * @style: widget style (See to sui_style_t struct)
 * @evmask: event mask for event function calling
 * @hevent: widget event function
 * @place: place(x,y) and size(w,h) of widget client area. Width and height can be -1 on entry for initialize by widget contents
 * @flags: Widget flags. In low word(16bit) are common flags and in high word are specific flags for every widget type. (See to enum with SUFL_ prefix and SUxF_ prefix)
 * @damage: widget damage flags.
 * @labalign: Alignment of the widget label relatively to the widget. Flags with 'SUAL_' prefix.
 * @label: widget label in utf8 (See to utf8 section).
 * @tooltip: widget tooltip
 * @key_table: widget key functions
 * @parent: pointer to parent widget (group)
 * @prev: pointer to previous widget
 * @next: pointer to next widget
 // * @user_data: pointer to widget user data
 * @data: pointer to widget specific structure
 *
 * Main widget structure describes widget and its behaviour. Widget specific properties
 * are described in their sections.
 *
 * File: sui_widget.h
 */
typedef struct sui_widget {
  struct sui_obj        base;

  sui_refcnt_t          refcnt;      /* reference counter */
  int                   type;        /* type of widget */

  /* common fields for all widgets of the same type */
  sui_style_t          *style;

  unsigned short        evmask;      /* event mask for event function */
  sui_hevent_t         *hevent;      /* widget event function */

  /* fields specific for each instance */
  sui_rect_t            place;
  sui_flags_t           flags;
  int                   damage;      /* damage flags for future partial redraws */

  sui_align_t           labalign;
  utf8                 *label;

  utf8                 *tooltip;

  struct sui_key_table *key_table;    /* widget key function */

  struct sui_widget    *parent;  /* pointer to parent widget */
  struct sui_widget    *prev;    /* siblings */
  struct sui_widget    *next;

/*  void           *user_data;  */  /* widget user data */
  void                 *data;       /* chain to spec. data struct */
} sui_widget_t;


/* widget flags */
/**
 * enum widget_flags - Common widget flags for all widget types
 * @SUFL_NORMAL, SUFL_INACTIVE: Widget is neither highlighted nor selected nor disabled.
 * @SUFL_HIGHLIGHTED, SUFL_ACTIVE: Widget is activated (highlighted).
 * @SUFL_SELECTED: Widget is selected. It influences how widget will look and selection of the widget color subset.
 * @SUFL_SHADY, SUFL_DISABLED: Widget is disabled and shaded.
 * @SUFL_FOCUSEN: Widget can have got keyboard focus.
 * @SUFL_FOCUSED: Widget has got keyboard focus just now.
 * @SUFL_EDITEN: Widget can be edited (Its content can be changed).
 * @SUFL_EDITED: Widget is in edit mode (Its content is changed).
 * @SUFL_INVISIBLE: Widget is hidden and cannot have got focus, but it can receive events.
 * @SUFL_AUTORESIZE: Widget size is dynamically changed according its content.
 * @SUFL_EDITAUTO: Widget switches to edit mode when it has focus and any known key is pressed
 * @SUFL_BOTTOMACTIVE: Widget is active even when its parent group shows modal widget
 * @SUFL_WIDGETISDOWN: Widget looks like in pressed position when 3D style is used.
 * @SUFL_BLINKFULL: Widget flashing (i.e. It internally turns highlight flag when it is drawing).
 * @SUFL_BLINKCONTENT: Widget content flashing.
 * @SUFL_COLORMASK: Mask for selection flags for changing color style of widget drawing.
 * @SUFL_TYPEFLAGS: Mask for flags specific for each widget type.
 *
 * File: sui_widget.h
 */
enum widget_flags {
  SUFL_NORMAL        = 0x00000000, /* normal inactive things */
  SUFL_HIGHLIGHTED   = 0x00000001, /* active things */
//  SUFL_SELECTED      = 0x00000002, /* selected things */
  SUFL_SHADY         = 0x00000004, /* shaded things */

  SUFL_INACTIVE      = 0x00000000, /* normal inactive things */
  SUFL_ACTIVE        = 0x00000001, /* active things */
  SUFL_SELECTED      = 0x00000002, /* selected things */

  SUFL_DISABLED      = 0x00000008, /* disabled things */


  SUFL_FOCUSEN       = 0x00000010, /* widget can obtain focus */
  SUFL_FOCUSED       = 0x00000020, /* widget has focus */
  SUFL_EDITEN        = 0x00000040, /* widget content can be edited */
  SUFL_EDITED        = 0x00000080, /* widget content is edited */

  SUFL_INVISIBLE     = 0x00000100, /* widget isn't visible */
  SUFL_AUTORESIZE    = 0x00000800, /* special flag for automatic widget resize according to content */
  SUFL_EDITAUTO      = 0x00000200, /* widget is automatically switch to edit mode when it has focus and any known key is pressed */
  SUFL_BOTTOMACTIVE  = 0x00000400, /* widget is active even when its parent group shows modal widget */

  SUFL_WIDGETISDOWN  = 0x00002000, /* widget (button) is down(pressed) - use FRAME2 color instead of FRAME color */
  SUFL_BLINKFULL     = 0x00004000, /* blinking widget */
  SUFL_BLINKCONTENT  = 0x00008000,
//  SUFL_ALIGNTOLABEL  = 0x00000100, /* widget is aligned to label */

  SUFL_COLORMASK     = 0x00000007, /* HIGHLIGHT | SELECTED | SHADY mask */
  SUFL_TYPEFLAGS     = 0xFFFF0000, /* widget specific flags mask */
};

/* Widget damage */
/**
 * enum widget_damage - Widget damage flags
 * @SUI_DAMAGE_CHILD: ?
 * @SUI_DAMAGE_EXPOSE: ?
 * @SUI_DAMAGE_REDRAW: ?
 * @SUI_DAMAGE_CHILD_LABEL: ?
 * @SUI_DAMAGE_LAYOUT: ?
 * @SUI_DAMAGE_ALL: ?
 * @SUI_DAMAGE_MASK: ?
 *
 * File: sui_widget.h
 */
enum widget_damage {
  /* suggested meanings */
  SUI_DAMAGE_CHILD       = 0x01,
  SUI_DAMAGE_EXPOSE      = 0x02,
  SUI_DAMAGE_REDRAW      = 0x04,
  // defined
  SUI_DAMAGE_CHILD_LABEL = 0x10,
  SUI_DAMAGE_LAYOUT      = 0x20,
  SUI_DAMAGE_ALL         = 0x40,
  SUI_DAMAGE_MASK        = 0x7f
};


/******************************************************************************
 * Basic widget functions
 ******************************************************************************/
/* widget flag macros */
#define sui_is_flag(wdg, flgs, isflgs) ((wdg->flags & (flgs)) == (isflgs))
#define sui_test_flag(wdg, flgs) (wdg->flags & (flgs))
#define sui_set_flag(wdg, flgs) do { wdg->flags |= (flgs); } while(0)
#define sui_clear_flag(wdg, flgs) do { wdg->flags &= ~(flgs); } while(0)
#define sui_switch_flag(wdg, flgs) do { wdg->flags ^= (flgs); } while(0)
#define sui_wdgclr_state(wdg) (wdg->flags & SUFL_COLORMASK)

sui_refcnt_t sui_inc_refcnt(sui_widget_t *widget);
sui_refcnt_t sui_dec_refcnt(sui_widget_t *widget);


/******************************************************************************
 * Widget drawing
 ******************************************************************************/
typedef int sui_draw_content_fnc_t(sui_dc_t *dc, sui_widget_t *widget);

void sui_draw_init(sui_dc_t *dc, struct sui_widget *widget);
void sui_draw_done(sui_dc_t *dc, struct sui_widget *widget);

void sui_set_fgcolor(sui_dc_t *dc, sui_widget_t *wdg, int color);
void sui_set_bgcolor(sui_dc_t *dc, sui_widget_t *wdg, int color);

void sui_set_widget_fgcolor(sui_dc_t *dc, sui_widget_t *wdg, int color);
void sui_set_widget_bgcolor(sui_dc_t *dc, sui_widget_t *wdg, int color);
void sui_draw_frame(sui_dc_t *dc, sui_widget_t *wdg, sui_rect_t *box,
                    sui_coordinate_t size);
void sui_draw_widget_background(sui_dc_t *dc, sui_widget_t *widget);
void sui_draw_widget_frame(sui_dc_t *dc, sui_widget_t *widget);
void sui_draw_widget_label(sui_dc_t *dc, sui_widget_t *widget);
void sui_widget_draw(sui_dc_t *dc, sui_widget_t *widget,
                    sui_draw_content_fnc_t *fnc);


/******************************************************************************
 * Widget support functions
 ******************************************************************************/
int sui_widget_get_label_size(sui_dc_t *dc, sui_widget_t *widget,
                                    sui_textdims_t *size);
int sui_widget_get_placement(sui_widget_t *widget, sui_rect_t *box);
int sui_widget_compute_size(sui_dc_t *dc, sui_widget_t *widget,
                            sui_point_t *size);
sui_align_t sui_widget_get_item_align(sui_widget_t *widget);

void sui_damage(sui_widget_t *widget);
void sui_damage_parent(sui_widget_t *widget);

int sui_widget_is_focusable(sui_widget_t *widget);

static inline
sui_align_t sui_get_widget_labalign(sui_widget_t *widget)
{
  sui_align_t la = widget->labalign;
  if (widget->style)
    la |= widget->style->labalign;
  return la;
}


/******************************************************************************
 * Widget key-func structures and functions
 ******************************************************************************/
/* hkey function type */
typedef void sui_hkey_t(struct sui_widget *widget, long info);

/**
 * struct sui_key_action - Key-action descriptive structure
 * @keycode: Action key code.
 * @keymask: Action key modifiers maks.
 * @keymod: Action key modifiers value.
 * @hkey: Action key callback function.
 * @info: Parameter of action key callback function.
 *
 * The structure describes key transformation to an action.
 *
 * File: sui_widget.h
 */
typedef struct sui_key_action {
  unsigned short  keycode; /* action key code */
  unsigned short  keymask; /* action key modifiers mask */ /* added at 2003/9/7 */
  unsigned short  keymod; /* action key modifiers value */
  sui_hkey_t     *hkey;   /* action callback */
  long            info;    /* parameter */
} sui_key_action_t;

/**
 * struct sui_key_table - List of key-action structures
 * @refcnt: Reference counter of list.
 * @maxcnt: size of allocated space for a dynamic $action array as a number of actions.
 *          If it is zero and the $action!=NULL than pointer to a static action array is in the $action.
 * @actcnt: number of actions in the $action array
 * @actions: Pointer to first item in array of key action structure.(See 'sui_key_action' structure)
 *
 * Items in list are chained by key action structure with keycode=0, hkey=SUI_HKEY_CHAIN and
 * info=pointer to chained list.
 *
 * File: sui_widget.h
 */
typedef struct sui_key_table {
  sui_refcnt_t          refcnt;           /* reference counter */
  int                   maxcnt; /* only for keytable dynamic building */
  int                   actcnt; /* only for keytable dynamic building */
  sui_key_action_t     *actions;          /* key action struct */
  struct sui_key_table *next;
/*  int                   actions_dyn:1;    // ??? */
} sui_key_table_t;

#define SUI_HKEY_CHAIN ((sui_hkey_t*)1)
#define SUI_KEY_CHAIN(actions) {0,0,0,SUI_HKEY_CHAIN,(long)actions,NULL}

sui_refcnt_t sui_key_table_inc_refcnt(sui_key_table_t *key_table);
sui_refcnt_t sui_key_table_dec_refcnt(sui_key_table_t *key_table);

sui_key_table_t *sui_key_table_create(sui_key_action_t *actions);

void sui_hkey_trans_key(sui_widget_t *widget, long info);
void sui_hkey_trans_cmd(sui_widget_t *widget, long info);
void sui_hkey_trans_global(sui_widget_t *widget, long info);


/******************************************************************************
 * Widget event structures and functions
 ******************************************************************************/
void sui_draw_request(sui_widget_t *widget);
void sui_redraw_request(sui_widget_t *widget);

/**
 * sui_clear_event - Clean event
 * @widget: Pointer to widget, which cleans event.
 * @event: Pointer to cleaned event.
 *
 * Inline function which clears event structure.
 *
 * File: sui_widget.h
 */
static inline
void sui_clear_event(sui_widget_t *widget, sui_event_t *event)
{
  event->what = SUEV_NOTHING;
  event->message.ptr = widget;
}
  
/**
 * sui_hevent - Send an event to the widget's event processing function.
 * @widget: Pointer to widget, which hevent function is called.
 * @event: Pointer to event.
 *
 * Inline function for directly calling widget event handler function
 *
 * File: sui_widget.h
 */
static inline
void sui_hevent(sui_widget_t *widget, sui_event_t *event)
{
  if (widget == NULL)
    return;

  if((!(widget->evmask & event->what) & 0/*FIXME*/) || (sui_test_flag(widget, SUFL_DISABLED)
      && !is_init_event(event)))
    return;
  widget->hevent(widget, event);
}

/**
 * sui_hevent_command - Send a command event to the widget's event processing function.
 * @widget: Pointer to widget, where will be an event pushed
 * @cmd:    command event (number of command event - see SUCM_xxx constants)
 * @ptr:    command event pointer (the first event argument)
 * @info:   command event info (the second event argument)
 *
 * The function creates event and pushes it to the widget. If the event is
 * processed the function returns zero otherwise it returns negative value.
 * Return Value: The functions returns zero if event was processed successfully
 * or negative value when an error occured.
 *
 * File: sui_widget.h
 */
static inline
int sui_hevent_command(sui_widget_t *widget, short cmd, void *ptr, long info)
{
  sui_event_t ev;
  ev.what = SUEV_COMMAND;
  ev.message.command = cmd;
  ev.message.ptr = ptr;
  ev.message.info = info;
  if (widget) sui_hevent(widget, &ev);
  if (ev.what == SUEV_NOTHING) return 0;    /* successfully performed */
  return -1;
}

/******************************************************************************
 * Widget slot functions
 ******************************************************************************/
int sui_widget_show(sui_widget_t *widget);
int sui_widget_hide(sui_widget_t *widget);
int sui_widget_enable(sui_widget_t *widget);
int sui_widget_disable(sui_widget_t *widget);
int sui_widget_setfocus(sui_widget_t *widget);
int sui_widget_clearfocus(sui_widget_t *widget);
int sui_widget_move(sui_widget_t *widget, sui_coordinate_t x, sui_coordinate_t y);
int sui_widget_resize(sui_widget_t *widget, sui_coordinate_t w, sui_coordinate_t h);
int sui_widget_setfocusen(sui_widget_t *widget, int state);
int sui_widget_blink_on(sui_widget_t *widget);
int sui_widget_blink_off(sui_widget_t *widget);
int sui_widget_blinkcontent_on(sui_widget_t *widget);
int sui_widget_blinkcontent_off(sui_widget_t *widget);


/******************************************************************************
 * Basic widget assignment function
 ******************************************************************************/
int sui_widget_assign_key_table(sui_widget_t *widget, sui_key_table_t *newkeys);


/******************************************************************************
 * Basic widget vmt & signal-slot mechanism
 ******************************************************************************/
#define sui_widget_vmt_fields(new_type, parent_type) \
  SUI_INS_INHERITED_VMT(new_type, parent_type, sui_obj) \
  int (*show)(new_type##_t *self); \
  int (*hide)(new_type##_t *self); \
  int (*enable)(new_type##_t *self); \
  int (*disable)(new_type##_t *self); \
  int (*setfocus)(new_type##_t *self); \
  int (*clearfocus)(new_type##_t *self); \
  int (*move)(new_type##_t *self, sui_coordinate_t x, sui_coordinate_t y); \
  int (*resize)(new_type##_t *self, sui_coordinate_t w, sui_coordinate_t h); \
  int (*setfocusen)(new_type##_t *self, int state); \
  int (*blinkon)(new_type##_t *self); \
  int (*blinkoff)(new_type##_t *self); \
  int (*kdownslot)(new_type##_t *self, int key); \
  int (*blinkconton)(new_type##_t *self); \
  int (*blinkcontoff)(new_type##_t *self);

/*
 * struct sui_widget_vmt - common widget vmt
 */
typedef struct sui_widget_vmt {
  sui_widget_vmt_fields(sui_widget, sui_obj)
} sui_widget_vmt_t;

static inline sui_widget_vmt_t *sui_widget_vmt(sui_widget_t *self)
{
  return (sui_widget_vmt_t*)(self->base.vmt);
}

//SUI_TYPE_NEW_VMT_FCN_DEC(sui_widget)

extern sui_widget_vmt_t sui_widget_vmt_data;

/* widget fcns */
void sui_widget_init(sui_widget_t *widget, sui_dc_t *dc);

void sui_widget_hevent(sui_widget_t *widget, sui_event_t *event);

sui_widget_t *sui_widget_create_new(void *vmt_data, const sui_rect_t *aplace,
                    utf8 *alabel, sui_style_t *astyle, unsigned long aflags);

sui_widget_t *sui_widget(const sui_rect_t *aplace, utf8 *alabel,
                    sui_style_t *astyle, unsigned long aflags);

#define sui_widget_connect(src, sig, dst, slot) \
  ul_obj_connect((sui_obj_t *)src, sig, (sui_obj_t *)dst, slot)


#endif /* _SUITK_WIDGET_H_ */
