/* sui_print.h
 *
 * SUITK export to printers (text based).
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#include "sui_print.h"

#include <ul_log.h>
UL_LOG_CUST(ulogd_suitk)

/******************************************************************************/
/* auxiliary functions */

/**
 * sui_print_create_line - Prepare structure which describes one line
 */
sui_print_line_t *sui_print_create_line(void)
{
  sui_print_line_t *ret = sui_malloc(sizeof(sui_print_line_t));
  if (ret) {
    ul_dbuff_init(&ret->content,0);
    ret->align = 'l'; /* default alignment */
  }
  return ret;
}

/**
 * sui_print_destroy_line - Clear and destroy structure which describes one line
 */
sui_print_line_t *sui_print_destroy_line(sui_print_line_t *line)
{
  sui_print_line_t *next;
  if (!line) return NULL;
  next = line->nextline;
  ul_dbuff_destroy(&line->content);
  return next;
}



/**
 * sui_pfilter_command_parser - internal function for parsing commands and args
 */
int sui_pfilter_command_parser(char *inbuf, char *outcmd, int maxcmd,
                                            char *outargs, int maxargs)
{
  int i, c;
  if (*inbuf++!='\\') return -1;
  i = 0;
  c = 0;
  if (*inbuf=='\\') { /* command is backslash */
    *outcmd++='\\';
    *outcmd='\0';
    return 1;
  }
  while (i<maxcmd-1) {
    if (*inbuf=='{' || *inbuf=='\0') break;
    *outcmd++ = *inbuf++;
    i++;
  }
  c = i + 2;
  *outcmd = '\0';
  if (*inbuf!='{') return -2;
  inbuf++;
  i = 0;
  while (i<maxargs-1) {
    if (*inbuf=='}' || *inbuf=='\0') break;
    *outargs++ = *inbuf++;
    i++;
  }
  *outargs = '\0';
  if (*inbuf!='}') return -3;
  c += i + 1;
  return c;
}

/**
 * sui_pfilter_args_parser - Parse arguments
 *
 */
int sui_pfilter_args_parser(char *args, char *parray[], int maxarray)
{
  int iarg = 0;
  int idx = 0;
  int ilastidx = 0;
          
  /* split argument string into separated array of arguments */
  if (!maxarray) return 0;
  
  while (args[idx]!='\0') {
    if (args[idx]==',') {
      parray[iarg] = args + ilastidx;
      ilastidx = idx+1;
      args[idx] = '\0';
      iarg++;
      if (iarg==maxarray) break;
    }
    idx++;
  }
  if (iarg<maxarray) {
    parray[iarg++] = args + ilastidx;
  }
  return iarg;
}

/******************************************************************************/
/* main print filter functions */
/**
 * sui_pfilter_inc_refcnt - Increment printing filter structure reference counter
 * @spf: Pointer to a printing filter structure.
 */
sui_refcnt_t sui_pfilter_inc_refcnt(sui_print_filter_t *spf)
{
  if (!spf) return SUI_REFCNTERR;
  if (spf->refcnt>=0) spf->refcnt++;
  return spf->refcnt;
}

/**
 * sui_pfilter_dec_refcnt - Decrement printing filter structure reference counter
 * @spf: Pointer to a printing filter structure.
 */
sui_refcnt_t sui_pfilter_dec_refcnt(sui_print_filter_t *spf)
{
  sui_refcnt_t ref;
  if (!spf) return SUI_REFCNTERR;
  if (spf->refcnt > 0) spf->refcnt--;
  if (!(ref = spf->refcnt)) {
    if (spf->text) sui_utf8_dec_refcnt(spf->text);
    sui_obj_done_vmt((sui_obj_t *)spf, UL_CAST_UNQ1(sui_obj_vmt_t *, &sui_prnfilter_vmt_data));
    sui_free(spf);
  }
  return ref;
}


/******************************************************************************/
/* filter functions */

/**
 * sui_prnflt_addhdr - Add header to the printed data
 * @prlay: Pointer to the used printing layout.
 * @pfilt: Pointer to the structure of the used filter.
 * @dbuf: Pointer to an input/output dbuff buffer.
 *
 * The filter function adds header defined in filter->text to the printed data stream.
 *
 * Return Value: The filter function returns zero as success or a negative value as an error.
 *
 * File: sui_print.c
 */
int sui_prnflt_addhdr(struct sui_print_layout *prlay,
                        struct sui_print_filter *pfilt, ul_dbuff_t *dbuf)
{
  ul_dbuff_t dbtmp;
  if (!pfilt || !pfilt->text || !dbuf) return -1;
  ul_dbuff_init(&dbtmp, 0);
  ul_dbuff_cpy(&dbtmp, sui_utf8_get_text(pfilt->text),
              sui_utf8_bytesize(pfilt->text));
  ul_dbuff_cat(&dbtmp, dbuf->data, dbuf->len);
  ul_dbuff_cpy(dbuf, dbtmp.data, dbtmp.len);
  ul_dbuff_destroy(&dbtmp);
  return 0;
}


/**
 * sui_prnflt_addfooter - Add foot to the printed data
 * @prlay: Pointer to the used printing layout.
 * @pfilt: Pointer to the structure of the used filter.
 * @dbuf: Pointer to an input/output dbuff buffer.
 *
 * The filter function adds foot defined in filter->text to the printed data stream.
 *
 * Return Value: The filter function returns zero as success or a negative value as an error.
 *
 * File: sui_print.c
 */
int sui_prnflt_addfooter(struct sui_print_layout *prlay,
                        struct sui_print_filter *pfilt, ul_dbuff_t *dbuf)
{
  if (!pfilt || !pfilt->text || !dbuf) return -1;
  ul_dbuff_cat(dbuf, sui_utf8_get_text(pfilt->text),
              sui_utf8_bytesize(pfilt->text));
  return 0;
}


/**
 * sui_prnflt_resolve_dinfos - Switch all links to dinfos to their values
 * @prlay: Pointer to the used printing layout.
 * @pfilt: Pointer to the structure of the used filter.
 * @dbuf: Pointer to an input/output dbuff buffer.
 *
 * The filter function searches all '\d{}' commands and replace them with their values.
 * Possible formats of the command are :
 *    '\var{di_name}' - simple replace dinfo with its value in standard format
 *    '\var{di_name,fi_name}' - replace dinfo with specified format
 *    '\time{di_name,format[,{local|utc|hms|interval}]} - replace dinfo with formated time
 *
 * Return Value: The filter function returns zero as success or a negative value as an error.
 *
 * File: sui_print.c
 */
int sui_prnflt_resolve_dinfos(struct sui_print_layout *prlay,
                        struct sui_print_filter *pfilt, ul_dbuff_t *dbuf)
{
  char cmd[32];
  char args[64];
  char *arrargs[10];
  int pos = 0, add, ret = 0;
  char *ptr;
  int cntarg = 0;

  ul_dbuff_t dbtmp;
    
  if (!pfilt || !dbuf) return -1;
/* initialization */
  ul_dbuff_init(&dbtmp, 0);
  ptr = (char *)dbuf->data;
  pos = 0;

  while (pos < dbuf->len) {
    add = ul_str_pos((unsigned char *)ptr, (unsigned char *)"\\", 0);
    
    if (add>=0) { /* add the previous part to the output buffer */
      ul_dbuff_cat(&dbtmp, ptr, add);
      pos += add;
      ptr += add;

      add = sui_pfilter_command_parser(ptr, cmd, 32, args, 64);
      if (add > 0) {
        cntarg = sui_pfilter_args_parser(args, arrargs, 10);
        
        if (!strcmp(cmd,"var")) {
          utf8 *txtdi = NULL;
          ns_object_t *obj;
          sui_dinfo_t *di = NULL;
          sui_finfo_t *fi = NULL;
          
          //ul_logdeb("DINFO !!! - textove nebo numericke (s formatem)\n");
          if (!cntarg) {
            ul_logerr("PRNDI: command 'var' needs one or more arguments\n");
            ret = -1;
            break;
          }
          obj = ns_find_any_object_by_type_and_name(SUI_TYPE_DINFO, arrargs[0]);
          if (!obj) {
            ul_logerr("PRNDI: variable '%s' isn't in a namespace\n", arrargs[0]);
            ret = -1;
            break;
          }
          di = obj->ptr;
          if (cntarg>1) {
            obj = ns_find_any_object_by_type_and_name(SUI_TYPE_FORMAT, arrargs[1]);
            if (!obj) {
              ul_logerr("PRNDI: format '%s' (dinfo '%s') isn't in a namespace\n", arrargs[1], arrargs[0]);
              ret = -1;
              break;
            }
            fi = obj->ptr;
          }
          
          if (di->tinfo==SUI_TYPE_UTF8) {
            if (sui_rd_utf8(di, 0, &txtdi)!=SUI_RET_OK) txtdi = NULL;
          } else {
            txtdi = sui_dinfo_2_utf8(di, 0, fi, SUTR_CHECKLIMITS);
          }
          if (!txtdi) {
            if (cntarg>2) { /* the third argument is temp. text/var */
              txtdi = sui_utf8_dynamic(arrargs[2], -1, -1);
            } else {
              txtdi = U8 "---";
            }
          }
          if (!txtdi) {
            ul_logerr("PRNDI: var.text '%s' is NULL", arrargs[0]);
            ret = -1;
            break;
          }
          ul_dbuff_cat(&dbtmp, sui_utf8_get_text(txtdi),
                      sui_utf8_count_bytes(sui_utf8_get_text(txtdi),
                                          sui_utf8_length(txtdi)));
          sui_utf8_dec_refcnt(txtdi);

        } else if (!strcmp(cmd,"time")) {
          utf8 *txttime = NULL;
          unsigned long time;
          ns_object_t *obj;
          unsigned short flags = 0;

          //ul_logdeb("DINFO !!! - time (s formatem)\n");
          if (cntarg<2) {
            ul_logerr("PRNDI: command 'time' needs two or more arguments\n");
            ret = -1;
            break;
          }

          obj = ns_find_any_object_by_type_and_name(SUI_TYPE_DINFO, arrargs[0]);
          if (!obj) {
            ul_logerr("PRNDI: dinfo '%s' isn't in a namespace\n", arrargs[0]);
            ret = -1;
            break;
          }
          if (sui_rd_ulong(obj->ptr, 0, &time)!= SUI_RET_OK) {
            ul_logerr("PRNDI: reading 'time' dinfo failed\n");
            ret = -1;
            break;
          }

          if (*arrargs[2]) {
            if (!strcmp(arrargs[2], "local"))
              flags |= SUTR_LOCALTIME;
            else if (!strcmp(arrargs[2], "utc"))
              flags |= SUTR_UTCTIME;
            else if (!strcmp(arrargs[2], "interval"))
              flags |= SUTR_RELATIVETIME;
            else if (!strcmp(arrargs[2], "hms"))
              flags |= SUTR_RELATIVETIME | SUTR_HMSONLYTIME;
            else {
              ul_logerr("PRNDI: time format '%s' for dinfo '%s' is not known\n", arrargs[2], arrargs[0]);
              ret = -1;
            }
          }

          txttime = sui_di2utf8_time(time, U8(arrargs[1]), flags);
          if (!txttime) {
            ul_logerr("PRNDI: translation time->text failed\n");
            ret = -1;
            break;
          }
          ul_dbuff_cat(&dbtmp, sui_utf8_get_text(txttime),
                      sui_utf8_count_bytes(sui_utf8_get_text(txttime),
                                          sui_utf8_length(txttime)));
          sui_utf8_dec_refcnt(txttime);

        } else if (!strcmp(cmd,"text")) {
          ns_object_t *obj;
          utf8 *txt;

          //ul_logdeb("Replace text cmd by argument...\n");
          // \text(text) or \text(var_text)
          if (cntarg<1) {
            ul_logerr("PRNDI: command 'text' needs one or more arguments\n");
            ret = -1;
            break;
          }
          obj = ns_find_any_object_by_type_and_name(SUI_TYPE_UTF8, arrargs[0]);
          if (!obj) {
            ul_logerr("PRNDI: text '%s' isn't in a namespace\n", arrargs[0]);
            ret = -1;
            break;
          }
          txt = obj->ptr;
          if (!txt) {
            ul_logerr("PRNDI: text '%s' is NULL\n", arrargs[0]);
            ret = -1;
            break;
          }
          ul_dbuff_cat(&dbtmp, sui_utf8_get_text(txt),
                      sui_utf8_count_bytes(sui_utf8_get_text(txt),
                                          sui_utf8_length(txt)));

        } else if (!strcmp(cmd,"suffix")) {
          ns_object_t *obj;
          sui_suffix_t *suff;
          
          //ul_logdeb("Replace suffix cmd by argument...\n");
          if (cntarg<1) {
            ul_logerr("PRNDI: command 'suffix' needs one or more arguments\n");
            ret = -1;
            break;
          }
          obj = ns_find_any_object_by_type_and_name(SUI_TYPE_SUFFIX, arrargs[0]);
          if (!obj) {
            ul_logerr("PRNDI: suffix '%s' isn't in a namespace\n", arrargs[0]);
            ret = -1;
            break;
          }
          suff = obj->ptr;
          if (!suff || !suff->suffix) {
            ul_logerr("PRNDI: suffix '%s' is NULL\n", arrargs[0]);
            ret = -1;
            break;
          }
          ul_dbuff_cat(&dbtmp, sui_utf8_get_text(suff->suffix),
                      sui_utf8_count_bytes(sui_utf8_get_text(suff->suffix),
                                          sui_utf8_length(suff->suffix)));

        } else if (!strcmp(cmd,"list")) {
          utf8 *txtli = NULL;
          ns_object_t *obj;
          sui_list_t *li = NULL;
          sui_list_item_t *liitem = NULL;
          
          //ul_logdeb("Replace list cmd by argument ...\n");
          // \list(list) -> entire list or \list(list,dinfo) - one item from list
          if (cntarg<1) {
            ul_logerr("PRNDI: command 'list' needs one or more arguments\n");
            ret = -1;
            break;
          }
          obj = ns_find_any_object_by_type_and_name(SUI_TYPE_LIST, arrargs[0]);
          if (!obj) {
            ul_logerr("PRNDI: list '%s' isn't in a namespace\n", arrargs[0]);
            ret = -1;
            break;
          }
          li = obj->ptr;
          if (cntarg > 1) { /* second argument is dinfo */
            long diindex = 0;
            sui_listcoord_t liindex;
            obj = ns_find_any_object_by_type_and_name(SUI_TYPE_DINFO, arrargs[1]);
            if (!obj) {
              ul_logerr("PRNDI: (list)dinfo '%s' isn't in a namespace\n", arrargs[1]);
              ret = -1;
              break;
            }
            if (sui_rd_long(obj->ptr, 0, &diindex)!= SUI_RET_OK) {
              ul_logerr("PRNDI: reading dinfo(l) '%s' index reading failed\n", arrargs[1]);
              ret = -1;
              break;
            }
            liindex = diindex;
            liitem = sui_list_gavl_find(li, &liindex);
            if (!liitem) {
              ul_logerr("PRNDI: list item #%d hasn't been found\n", liindex);
              ret = -1;
              break;
            }
            ret = sui_list_item_get_text(liitem, &txtli);
            if (!txtli) {
              ul_logerr("PRNDI: list item content is NULL\n");
              ret = -1;
              break;
            }
            ul_dbuff_cat(&dbtmp, sui_utf8_get_text(txtli),
                        sui_utf8_count_bytes(sui_utf8_get_text(txtli),
                                            sui_utf8_length(txtli)));
            sui_utf8_dec_refcnt(txtli);

          } else {
            sui_list_item_t *liitem;

            liitem = sui_list_gavl_first(li);
            while (liitem) {
              ret = sui_list_item_get_text(liitem, &txtli);
              if (!txtli) {
                ul_logerr("PRNDI: list item content is NULL\n");
                ret = -1;
                break;
              }
              ul_dbuff_cat(&dbtmp, sui_utf8_get_text(txtli),
                          sui_utf8_count_bytes(sui_utf8_get_text(txtli),
                                              sui_utf8_length(txtli)));
              sui_utf8_dec_refcnt(txtli);
              liitem = sui_list_gavl_next(li, liitem);
              ul_dbuff_cat(&dbtmp, "\\n{}", 4);
            }
          }
        
        } else if (!strcmp(cmd,"table")) {
          // \table(table)
          utf8 *txtitem = NULL;
          int ret;
          ns_object_t *obj;
          sui_table_t *tab = NULL;
          sui_listcoord_t row;
          sui_table_column_t *column;
          long colspan;

          //ul_logdeb("Replace table cmd by argument ...\n");
          if (cntarg<1) {
            ul_logerr("PRNDI: command 'list' needs one or more arguments\n");
            ret = -1;
            break;
          }

          obj = ns_find_any_object_by_type_and_name(SUI_TYPE_TABLE, arrargs[0]);
          if (!obj) {
            ul_logerr("PRNDI: table '%s' isn't in a namespace\n", arrargs[0]);
            ret = -1;
            break;
          }
          tab = obj->ptr;
          if (!tab) {
            ul_logerr("PRNDI: table '%s' is in a namespace\n", arrargs[0]);
            ret = -1;
            break;
          }
          /* update table maxidx - FIXME remove if changes in dinfo will be propagate to table */
          if (tab->firstcol && tab->firstcol->data) {
            tab->maxidx = tab->firstcol->data->idxsize;
          }
/* long format */
          if (cntarg>1) {
            row = 0;
            while (row < tab->maxidx) {
              column = tab->firstcol;
              ul_dbuff_cat(&dbtmp,"\\hl{.}",6);
              while (column) {
                
                if (column->title) {
                  ul_dbuff_cat(&dbtmp, sui_utf8_get_text(column->title),
                                sui_utf8_count_bytes(sui_utf8_get_text(column->title),
                                                    sui_utf8_length(column->title)));
                }
                ul_dbuff_cat(&dbtmp,": \\a{r}",7);
  
                ret = sui_table_column_item_get_text(column, row, &txtitem);
                if (!ret) {
                  ul_dbuff_cat(&dbtmp, sui_utf8_get_text(txtitem),
                                sui_utf8_count_bytes(sui_utf8_get_text(txtitem),
                                                    sui_utf8_length(txtitem)));
                  sui_utf8_dec_refcnt(txtitem);
                } else {
                  ul_dbuff_cat(&dbtmp,"???",3);
                }
                if (column->span) {
                  if (sui_rd_long(column->span, row, &colspan)==SUI_RET_OK) {
                    colspan--;
                    while(column && colspan) {
                      column = column->next;
                      colspan--;
                    }
                  }
                }
                column = column->next;
                if (column) ul_dbuff_cat(&dbtmp,"\\n{}",4);
              }
              row++;
            }
            // ul_dbuff_cat(&dbtmp,"\\n{}",4);
          } else {
/* short format */
            int colsz[20];
            int fullsz = 0, idx;
          /* rozdelit sirku papiru pomerne na jednotlive sloupce */
            column = tab->firstcol;
            while (column) {
              fullsz += column->width;
              column = column->next;
            }
            idx = 0;
            column = tab->firstcol;
            while (column) {
              colsz[idx++] = (column->width * prlay->paperwidth) / fullsz;
              column = column->next;
            }
          /* add row with titles into the table */
            column = tab->firstcol;
            idx = 0;
            if (column->title) {
              while(column) {
                int sz = sui_utf8_length(column->title);
                if (sz > colsz[idx]) sz = colsz[idx];
                ul_dbuff_cat(&dbtmp, sui_utf8_get_text(column->title),
                      sui_utf8_count_bytes(sui_utf8_get_text(column->title), sz));
                while (sz < colsz[idx]) {
                  ul_dbuff_append_byte(&dbtmp, ' ');
                  sz++;
                }
                idx++;
                column = column->next;
              }
              ul_dbuff_cat(&dbtmp,"\\hl{.}",6);
            }
          /* add rows into the table*/
            row = 0;
            while (row<tab->maxidx) {
              int curcolsz, icol, sz;
              
              column = tab->firstcol;
              idx = 0;
              while(column) {
                
                if (column->span) {
                  if (sui_rd_long(column->span, row, &colspan)!=SUI_RET_OK)
                    colspan = 1;
                } else
                  colspan = 1;

                curcolsz = 0;
                for(icol=0;icol<colspan;icol++)
                  curcolsz += colsz[idx+icol];
                
                ret = sui_table_column_item_get_text(column, row, &txtitem);
                if (!ret) {
                  sz = sui_utf8_length(txtitem);
                  if (sz > curcolsz) sz = curcolsz;
                  ul_dbuff_cat(&dbtmp, sui_utf8_get_text(txtitem),
                               sui_utf8_count_bytes(sui_utf8_get_text(txtitem), sz));
                  sui_utf8_dec_refcnt(txtitem);
                } else {
                  sz = (curcolsz < 3) ? curcolsz : 3;
                  ul_dbuff_cat(&dbtmp, "???", sz);
                }
                curcolsz -= sz;
                while (curcolsz--) {
                  ul_dbuff_append_byte(&dbtmp,' ');
                }
                
                while (column && colspan) {
                  idx++;
                  colspan--;
                  column = column->next;
                }
              }
              ul_dbuff_cat(&dbtmp,"\\n{}",4);
              row++;
            }
          }

        } else { /* return the unknown command back into the stream */
          ul_dbuff_cat(&dbtmp, ptr, add);
        }
        ptr += add;
        pos += add;
      
      } else {
        ul_logerr("PRNDI: Bad format of command\n");
        add = 0;
      }
    } else {
      ul_dbuff_cat(&dbtmp, ptr, dbuf->len-pos);
      pos = dbuf->len;
    }
  }

  ul_dbuff_cpy(dbuf, dbtmp.data, dbtmp.len);
  ul_dbuff_destroy(&dbtmp);

  return ret;
}



/**
 * comparing function for translational array :
 *  struct { _UTF_UCS_TYPE from; _UTF_UCS_TYPE to; }
 */
typedef struct sui_prnflt_trans {
  _UTF_UCS_TYPE from;
  _UTF_UCS_TYPE to;
} sui_prnflt_trans_t;

int sui_prnflt_trans_comp_fnc(const void *a, const void *b)
{
  _UTF_UCS_TYPE ua = ((sui_prnflt_trans_t *)a)->from;
  _UTF_UCS_TYPE ub = ((sui_prnflt_trans_t *)b)->from;
  return (ua<ub) ? -1 : ((ua>ub) ? 1 : 0);
}
 
/**
 * sui_prnflt_2ascii - Translate UTF8 text to plain ASCII text
 *
 * The function gets translational table from filter text argument. It must be arranged
 * in the following format: The first character from each pair is character which
 * will be searched and changed and the second character is characters which will
 * replace the searched first characters.
 * e.g. <text>"AaBbCcDdEeFfGgHh"</text> switches characters 'A'-'H' (excluding commands
 * and theirs arguments) into characters 'a'-'h'.
 */
int sui_prnflt_2ascii(struct sui_print_layout *prlay,
                      struct sui_print_filter *pfilt, ul_dbuff_t *dbuf)
{
  ul_dbuff_t dbtmp;
  int incmd = 0;
  utf8char *pfrom;
  int chcnt, i, cnt;
  int ret = -1;
  sui_prnflt_trans_t *transtab = NULL, *pttab;
  char nbuf[6];
  sui_prnflt_trans_t key;
    
  if (!pfilt || !pfilt->text || !dbuf) return -1;

  ul_dbuff_init(&dbtmp, 0);

  do {
  /* Initialization: - prepare table */
    chcnt = sui_utf8_length(pfilt->text)/2;
    if (!chcnt) {
      ul_logerr("PRN2A: Filter needs a text argument with translational array\n");
      break;
    }
    transtab = sui_malloc(sizeof(sui_prnflt_trans_t)*chcnt);
    if (!transtab) {
      ul_logerr("PRN2A: There isn't free memory for translational table\n");
      break;
    }
    pfrom = sui_utf8_get_text(pfilt->text);
    pttab = transtab;
    for(i=0;i<chcnt;i++) {
      pfrom += sui_utf8_to_ucs(pfrom, &(pttab->from));
      pfrom += sui_utf8_to_ucs(pfrom, &(pttab->to));
      pttab++;
    }
  /* sort the table */
    qsort(transtab, chcnt, sizeof(sui_prnflt_trans_t),
          sui_prnflt_trans_comp_fnc);

  /* translation */
    cnt = dbuf->len;
    pfrom = (utf8char *)dbuf->data;
    while(cnt>0) {
      i = sui_utf8_to_ucs(pfrom, &key.from); /* get character from buffer */
      pfrom += i;
      cnt -= i;
      if (!incmd) { /* translate */
        if ((key.from=='\\') && (*pfrom!='\\')) {
          pttab = NULL;
          incmd = 1;
        } else {
          pttab = bsearch(&key, transtab, chcnt, sizeof(sui_prnflt_trans_t),
                          sui_prnflt_trans_comp_fnc);
        }
        if (!pttab) {
          pttab = &key;
          key.to = key.from;
        }
      } else {
        if (key.from=='}') {
          incmd = 0;
        }
        pttab = &key;
        key.to = key.from;
      }
      i = sui_utf8_from_ucs(nbuf, pttab->to);
      ul_dbuff_cat(&dbtmp, nbuf, i);
    }

    ul_dbuff_append_byte(&dbtmp, 0);
    ul_dbuff_cpy(dbuf, dbtmp.data, dbtmp.len);
    ret = 0;
  } while (0);
  
  if (transtab) sui_free(transtab);
  ul_dbuff_destroy(&dbtmp);
  return ret;
}

/******************************************************************************/
/**
 * sui_pfilter_from_name - Translate printing filter name to filter function
 * @fnc_name: Pointer to a filter function name.
 *
 * The function tries to find filter function between all known according its name.
 *
 * Return Value: The function returns pointer to the printing filter function or NULL.
 *
 * File: sui_print.c
 */
sui_print_filter_fnc_t *sui_pfilter_from_name(char *fnc_name)
{
  if (!strcmp(fnc_name,"addheader")) {
    return sui_prnflt_addhdr;
  } else if (!strcmp(fnc_name,"addfooter")) {
    return sui_prnflt_addfooter;
  } else if (!strcmp(fnc_name,"resolvedi")) {
    return sui_prnflt_resolve_dinfos;
  } else if (!strcmp(fnc_name,"conv2ascii")) {
    return sui_prnflt_2ascii;
  } else if (!strcmp(fnc_name,"conv2plain")) {
  }
  return NULL;
}


/******************************************************************************/

/**
 * sui_prnflt_basic_filtering - Filter common commands (new line, alignment, ...)
 * @spl: Pointer to the used printing layout.
 * @dbuf: Pointer to an input/output dbuff buffer.
 *
 * The function removes all unprocessed commands and resolves the following commands:
 * \n{} - new line
 * \hl{}, \hl{ch} - add horizontal line from specified characters $ch or from '-' as default character
 * \a{l/c/r} - line alignment
 * \hex{xxxxxx} - add byte(s) defined by its(theirs) hexadecimal value(s) (e.g. '\hex{03AB}' adds two bytes into output - the first byte will be 03 and the second will be AB
 *
 * Return Value: The filter function returns zero as success or a negative value as an error.
 *
 * File: sui_print.c
 */
int sui_prnflt_basic_filtering(struct sui_print_layout *spl, ul_dbuff_t *dbuf)
{
  sui_print_line_t *lines = NULL;
  int cntlines = 0;
  
/* rozumny postup by asi mohl byt:
    1) rozkuchat na radky - do jednosmerneho seznamu
    2) zpracovat zname a vykuchat nezname commandy
    3) provest dorozdeleni dlouhych radku a zarovani na danou delku
    4) pospojovani do vysledneho retezce
    5) uvolnit a zrusit docasne radky
*/

  if (!spl || !dbuf) return -1;

/* 1) disassembly stream into a linked list of lines */
  {
    int idx = 0;
    char *ptr = (char *)dbuf->data;
    char *plast = ptr;
    sui_print_line_t **plastline = &lines;
    
    while (idx<=dbuf->len) {
      if ((idx <= (dbuf->len-4) && !strncmp(ptr,"\\n{}",4)) ||
          (idx == dbuf->len)) {
        *plastline = sui_print_create_line();
        if (!*plastline) {
          ul_logerr("PRNBF: No memory for a new line (add)\n");
          break;
        }
//        ul_logdeb("PRN: newline size=%d\n",ptr-plast);
        ul_dbuff_cpy(&(*plastline)->content, plast, ptr-plast);
        ul_dbuff_append_byte(&(*plastline)->content, 0);
//        ul_logdeb("PRN: newline and zero len=%d\n",(*plastline)->content.len);
        ptr += 3;
        idx += 3;
        plast = ptr+1;
        plastline = &(*plastline)->nextline;
        cntlines++;
      }
      ptr++;
      idx++;
    }
  }
/* 2) processing of all known commands */
  {
    sui_print_line_t *curline = lines;
    char *ptr;
    int llen, add;
    char cmd[32];
    char args[64];
    
    while (curline) {
      ptr = (char *)curline->content.data;
      llen = curline->content.len;
      while (llen) {
        if (*ptr=='\\') {
          add = sui_pfilter_command_parser(ptr, cmd, 32, args, 64);
          if (add<0) {
            ul_logerr("PRNBF:Command parsing error #%d\n",add);
            break;
          }
          if (!strcmp(cmd,"\\")) { /* backslash - remove only one character */
            int idx;
            //ul_logdeb("PRNBF:backslash %d\n", add);
            llen--;
            curline->content.len--;
            for(idx=0;idx<llen;idx++)
              *(ptr+idx) = *(ptr+idx+1);

          } else if (!strcmp(cmd,"hl")) { /* add one or two new lines */
            sui_print_line_t *newline;
            int idx;
            int hlatbeg = (llen==curline->content.len);
            int hlatend = (llen-add<=1);
              
            //ul_logdeb("PRNBF:horizontal line %d (ll=%d)\n", add, llen);
            /* split line into two lines, if there is some content after hor.line */

            if (!hlatend) { /* beggining or inside - create new line with the end of line */
              newline = sui_print_create_line();
              if (!newline) {
                ul_logerr("PRNBF: No memory for a new line (hl-data)\n");
                break;
              }
              ul_dbuff_cpy(&newline->content, ptr + add, llen - add);
              newline->nextline = curline->nextline;
              curline->nextline = newline;
              cntlines++;              
            }
            if (!hlatbeg) { /* inside or end - create new line for '----'*/
              curline->content.len -= llen-1;
              *ptr = '\0';

              /* add horizontal line */
              newline = sui_print_create_line();
              if (!newline) {
                ul_logerr("PRNBF: No memory for a new line (hl-line)\n");
                break;
              }
              newline->nextline = curline->nextline;
              curline->nextline = newline;
              cntlines++;

              curline = newline;
            }

            /* replace current line with '---' */
            if (!args[0]) args[0]='-';
            ul_dbuff_prep(&curline->content, spl->paperwidth+1);
            curline->content.len = spl->paperwidth+1;
            for (idx=0;idx<spl->paperwidth;idx++)
              *(curline->content.data+idx) = args[0];
            *(curline->content.data+idx) = '\0';
            
            break; /* skip a new line with horizontal line */

          } else { /* some known and all unknown commands will be removed */
            int idx;
            
            if (!strcmp(cmd, "a")) { /* set alignment for the current line */
              /* each line can have got only one alignment */
              if (args[0]=='r')
                curline->align = 'r';
              curline->idx_align = ptr - (char *)curline->content.data;

            } else if (!strcmp(cmd,"hex")) { /* add hexadecimal number */
              unsigned char byte = 0, st = 0;
              //ul_logdeb("PRNBF: Add Hex numbers '%s'\n", args);
              idx = 0;
              while(args[idx]) {
                if (!st) byte = 0;
                else byte <<= 4;
  
                if (args[idx]>='0' && args[idx]<='9')
                  byte |= (args[idx]-'0');
                else if (args[idx]>='A' && args[idx]<='F')
                  byte |= (args[idx]-'A'+10);
                else if (args[idx]>='a' && args[idx]<='f')
                  byte |= (args[idx]-'a'+10);
                else {
                  ul_logerr("PRNBF: bad hex number (pos=%d)\n", idx);
                  break;
                }
                idx++;
                if (st) {
                  *ptr++ = byte; /* one byte is less than two as hexadecimal */
                  add--;
                }
                st = !st;
              }
            
            } else {
              ul_logerr("PRNBF:unknown command '%s' with arguments '%s'\n", cmd, args);
            }
            llen -= add;
            curline->content.len -= add;
            for(idx=0;idx<llen;idx++)
              *(ptr+idx) = *(ptr+idx+add);            
          }
        }
        ptr++;
        llen--;
      }
      curline = curline->nextline;
    }
  }


/* split long lines and align lines (or their parts) to right if they need it */
  {
    sui_print_line_t *curline = lines;
/* FIXME: don't split part which is aligned to right */
    while (curline) {
      if (curline->content.len > spl->paperwidth+1) { /* dbuff.len contains ending zero */
        sui_print_line_t *newline;
        newline = sui_print_create_line();
        if (!newline) {
          ul_logerr("PRNBF: No memory for a new line (split)\n");
          break;
        }
        ul_dbuff_cpy(&newline->content, curline->content.data + spl->paperwidth,
                      curline->content.len - spl->paperwidth);
        curline->content.len = spl->paperwidth + 1;
        *(curline->content.data+spl->paperwidth) = '\0';
        newline->align = curline->align;
        if (curline->idx_align < curline->content.len) {
          newline->idx_align = 0;
        } else {
          newline->idx_align = curline->idx_align - spl->paperwidth;
          curline->align = 'l'; /* no special alignment at the old line */
          curline->idx_align = 0;
        }
        newline->nextline = curline->nextline;
        curline->nextline = newline;
        cntlines++;
      } else if ((curline->content.len < spl->paperwidth+1) &&
                (curline->align=='r')) {
        int add = spl->paperwidth + 1 - curline->content.len; /* number of spaces which must be add */
        int idx;
        char *ptr;
        //ul_logdeb("PRTBF: alignment to right (%p)...\n", curline);
        idx = curline->content.len - 1;
        ul_dbuff_set_len(&curline->content, spl->paperwidth+1);
        ptr = (char *)curline->content.data;
        while (idx >= curline->idx_align) {
          *(ptr + idx + add) = *(ptr + idx);
          idx--;
        }
        idx++;
        add += curline->idx_align;
        while (idx < add) {
          *(ptr + idx++) = ' ';
        }
      }
      curline = curline->nextline;
    }    
  }


/* assembly stream from lines into output buffer and destroy temporary lines */
  {
    int size = 0, endcr = 0, endlf = 0;
    sui_print_line_t *curln = lines;
    
    while (curln) {
      size += curln->content.len; /* add other extra bytes for longer line separator than '\n' */
      curln = curln->nextline;
    }
    ul_dbuff_set_len(dbuf, size+1); /* prepare buffer for entire stream with ending zero */
    size = 0;
    ul_dbuff_set_len(dbuf, 0); /* clear output stream */
    curln = lines;
    if (spl->flags & SUI_PRN_END_CR) endcr = 1;
    if (spl->flags & SUI_PRN_END_LF) endlf = 1;
    while (curln) {
      ul_dbuff_cat(dbuf, curln->content.data, curln->content.len-1);
      if (endcr) ul_dbuff_append_byte(dbuf, '\n');
      if (endlf) ul_dbuff_append_byte(dbuf, '\r');
      curln = curln->nextline;
    }
    ul_dbuff_append_byte(dbuf, '\0');
  }


//   { /* debug print all lines for printing ...*/
//     sui_print_line_t *curln = lines;
//     ul_logdeb("PRNBF: Stream contains %d lines\n", cntlines);
//     while(curln) {
//       ul_logdeb("PRNBF: '%s'(a='%c'/%d)\n",curln->content.data, curln->align, curln->idx_align);
//       curln = curln->nextline;
//     }
//     
//   }

/* release and destroy temporary lines */
  { /* destroy all temporary lines */
    while (lines) {
      lines = sui_print_destroy_line(lines);
    }
  }
  return 0;
}



/******************************************************************************/
/* function for printing layout */

/**
 * sui_playout_inc_refcnt - Increment print_layout structure reference counter
 * @spl: Pointer to a sui_print_layout structure.
 */
sui_refcnt_t sui_playout_inc_refcnt(sui_print_layout_t *spl)
{
  if (!spl) return SUI_REFCNTERR;
  if (spl->refcnt>=0) spl->refcnt++;
  return spl->refcnt;
}
/**
 * sui_playout_dec_refcnt - Decerement print_layout structure reference coutner
 * @spl: Pointer to a sui_print_layout structure.
 */
sui_refcnt_t sui_playout_dec_refcnt(sui_print_layout_t *spl)
{
  sui_refcnt_t ref;
  if (!spl) return SUI_REFCNTERR;
  if (spl->refcnt > 0) spl->refcnt--;
  if (!(ref = spl->refcnt)) {
    if (spl->devices) sui_utf8_dec_refcnt(spl->devices);
    if (spl->filters) {
      int i=0;
      while(i < spl->numfilters) {
        sui_pfilter_dec_refcnt(spl->filters[i]);
        i++;
      }
      spl->numfilters = 0;
      sui_free(spl->filters);
      spl->filters = NULL;
    }
    if (spl->layout) sui_utf8_dec_refcnt(spl->layout);
    sui_obj_done_vmt((sui_obj_t *)spl, UL_CAST_UNQ1(sui_obj_vmt_t *, &sui_prnlayout_vmt_data));
    sui_free(spl);
  }
  return ref;
}

/**
 * sui_playout_add_filter - Add filter to a printing layout
 * @spl: Pointer to a sui_print_layout structure, where a filter will be added.
 * @spf: Pointer to a sui_print_filter structure, which will be added.
 *
 * The function realloc memory space for array of printing filter, sets a new
 * filter as the last used filter and increases reference counter of the filter.
 *
 * Return Value: The function returns a negative value if some error occurs or
 * it returns zero as success.
 *
 * File: sui_print.c
 */
int sui_playout_add_filter(sui_print_layout_t *spl, sui_print_filter_t *spf)
{
  if (!spl || !spf) return -1;
  sui_print_filter_t **new = realloc(spl->filters,
                          (spl->numfilters + 1) * sizeof(sui_print_filter_t *));
  if (new) {
    spl->filters = new;
    sui_pfilter_inc_refcnt(spf);
    spl->filters[spl->numfilters] = spf;
    spl->numfilters++;
    return 0;
  }
  return -1;
}


/**
 * sui_playout_print - Fill buffer with data to print
 * @outbuf: Pointer to an output data buffer.
 * @spl: Pointer to a structure with description of print layout.
 *
 * The function prepare data stream for printing according to print layout
 * and required filters. The output ul_dbuff buffer must be initialized.
 *
 * Return Value: The function returns zero if it is fully successful. If one or more
 * filter functions wasn't successful the function returns a negative value.
 *
 * File: sui_print.c
 */
int sui_playout_print(ul_dbuff_t *outbuf, sui_print_layout_t *spl)
{
  ul_dbuff_t stream;
  sui_print_filter_t *pflt;
  int i = 0, ret, allret = 0;

  if (!outbuf || !spl || !spl->layout) return -1;

/* prepare buffer - fill it from layout */
  ul_dbuff_init(&stream, 0);
  ul_dbuff_cpy(&stream, sui_utf8_get_text(spl->layout), sui_utf8_bytesize(spl->layout));
  
/* implement all filters sequentially */
  while (i < spl->numfilters) {
    pflt = spl->filters[i];
    if (pflt && pflt->ffnc) {
      ret = pflt->ffnc(spl, pflt, &stream);
      if (ret<0) {
        allret = -1;
        ul_logerr("PRINT:Print filter #%d didn't process the data stream correctly\n", i);
      }
    } else {
      ul_logerr("PRINT:Null print filter #%d\n", i);
    }
    i++;
  }

/* process data stream in the basic filter - remove all unknown commands from the stream and add newline characters */
  ret = sui_prnflt_basic_filtering(spl, &stream);
  if (ret<0) {
    allret = -1;
    ul_logerr("PRINT:Basic filter processing failed\n");
  }
   
/* fill output buffer */
  ul_dbuff_cpy(outbuf, stream.data, stream.len);
  ul_dbuff_destroy(&stream);
  
  return allret;
}


/**
 * sui_playout_debug_print - testing print function for linux stub
 */
int sui_playout_debug_print(sui_print_layout_t *spl)
{
  ul_dbuff_t dbuf;
  if (!spl) {
    ul_logerr("DEBUGPRINT:No print layout specified\n");
    return -1;
  }
  ul_logdeb("DEBUGPRINT: Test printing from layout %p\n", spl);
  ul_dbuff_init(&dbuf, 0);
  do {
    /* prepare printer data */
    if (sui_playout_print(&dbuf, spl)) {
      ul_logerr("DEBUGPRINT: Preparing printer data failed\n");
      break;
    }
    
    /* print buffer */
    ul_loginf("--- PRINTING ---\n%s--- END of PRINTING ---\n",dbuf.data);

  } while(0);
  ul_dbuff_destroy(&dbuf);
  return 0;
}
