/* sui_wdyntext.h
 *
 * SUITK dynamic text widget.
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#ifndef _SUI_DYNAMIC_TEXT_WIDGET_
#define _SUI_DYNAMIC_TEXT_WIDGET_

#include <suitk/suitk.h>

#define SUI_WDYNTEXT_ROTATION_FIRSTTIME 1000
#define SUI_WDYNTEXT_ROTATION_NEXTTIME   400
#define SUI_WDYNTEXT_ROTATION_ADDOFFSET    2


/******************************************************************************
 * Dynamic text widget extension
 ******************************************************************************/
/**
 * struct sui_dyntext_rtfpart - structure for description text with icon part
 * @str: pointer to utf8 text
 * @dinfo: pointer to dinfo, which can be variable
 * @image: pointer to image which is before/behind text
 * @font: pointer to font used for text or NULL
 * @size: size of text part
 * @flags: flags for RTF text part
 * @gap: gap before content
 * @next: pointer to next RTF part
 */
typedef struct sui_dyntext_rtfpart {
  union {
    utf8           *str;
    sui_dinfo_t    *dinfo;
    sui_image_t    *image;
    struct {
      sui_list_t   *list;
      sui_dinfo_t  *didx;
    } dilist;
  } content;
  sui_font_info_t  *font;
  sui_textdims_t    size;
  unsigned short    flags;
  sui_coordinate_t  gap;

  struct sui_dyntext_rtfpart *next;
} sui_dyntext_rtfpart_t;

/**
 * enum sui_dyntext_rtfflags - flags of part
 * @SUDTX_RTF_EOL: last part of RTF text on the line (EOL)
 * @SUDTX_RTF_MEASURED: size of text is valid
 */
enum sui_dyntext_rtfflags {
  SUDTX_RTF_EOL      = 0x0010,
  SUDTX_RTF_MEASURED = 0x0020,
  SUDTX_RTF_GAP_AFTER= 0x0040,
  SUDTX_RTF_INVERTED = 0x0080,
  SUDTX_RTF_BLINKING = 0x0100,
  SUDTX_RTF_INVBLINK = 0x0200,

  SUDTX_RTF_CON_NONE = 0x0000,
  SUDTX_RTF_CON_TEXT = 0x0001,
  SUDTX_RTF_CON_DINFO= 0x0002,
  SUDTX_RTF_CON_IMAGE= 0x0003,
  SUDTX_RTF_CON_LIST = 0x0004,
  SUDTX_RTF_CON_MASK = 0x000F,
};


/**
 * struct suiw_dyntext - Additional dynamic text widget structure
 * @align
 * File: sui_dyntext.h
 */
typedef struct suiw_dyntext {
  union {
    utf8          *text;
    sui_list_t    *list;
    sui_dinfo_t   *dinfo;
  } data;
  union {
    long           internal;
    sui_dinfo_t   *external;
  } index;
  union {
    long           constant;
    sui_dinfo_t   *dynamic;
  } time;
  
  sui_align_t      align; /* align or shift/rotate */
  sui_coordinate_t gap;  /* gap between two texts */


/* internal fields */
  unsigned long    nexttime; /* time of last change for dynamic contents */

  unsigned long    offsettime;
  sui_coordinate_t offset;   /* internal text offset for rotation */
  

  sui_dyntext_rtfpart_t *rtfhead;
} suiw_dyntext_t;

#define sui_dytwdg( wdg)  ((suiw_dyntext_t *)wdg->data)

/**
 * enum dyntext_flags - Dynamic text widget special flags ['SUYF_' prefix]
 * @SUYF_FNC_HIDDEN:  widget doesn't draw anything
 * @SUYF_FNC_STATIC:  widget contains one static text
 * @SUYF_FNC_MULTILN: widget contains multiline text(which can scroll)
 * @SUYF_FNC_DYNLIST: widget contains list of items which are dynamically switched (by time)
 * @SUYF_FNC_MASK: flag's mask of widget function
 * @SUYF_FNC_INTER: widget
 * @SUYF_ICON: icon will be inserted into text (where the character '$' is)
 * @SUYF_NOSWITCH: current text cannot be switched to another when this flag is set
 * File: sui_dyntext.h
 */
enum dyntext_flags {
  SUYF_FNC_HIDDEN  = 0x00000000,
  SUYF_FNC_STATIC  = 0x00010000,
  SUYF_FNC_MULTILN = 0x00020000,
  SUYF_FNC_DYNTEXT = 0x00030000,
  SUYF_FNC_MASK    = 0x00070000,

  SUYF_NOCHANGE    = 0x01000000, /* text won't be changed by new one */
  SUYF_ROTATE      = 0x02000000, /* align doesn't describe alignment but direction to rotate */
  SUYF_NOSWITCH    = 0x00100000, /* texts won't be switched together */

/* internal flags */
  SUYF_INTINDEX    = 0x00000000,
  SUYF_EXTINDEX    = 0x04000000,
  SUYF_INDEXMASK   = 0x04000000,

  SUYF_CONSTIME    = 0x00000000,
  SUYF_DYNTIME     = 0x08000000,
  SUYF_TIMEMASK    = 0x08000000,

  SUYF_SRC_UTF8    = 0x00000000,
  SUYF_SRC_DINFO   = 0x00100000,
  SUYF_SRC_LIST    = 0x00200000,
  SUYF_SRC_MASK    = 0x00300000,
  
};


/******************************************************************************
 * Dynamic text slot functions
 ******************************************************************************/

//int sui_wtext_set_text( sui_widget_t *widget, utf8 *text);

/******************************************************************************
 * Dynamic text widget vmt & signal-slot mechanism
 ******************************************************************************/
typedef sui_widget_t sui_wdyntext_t;

#define sui_wdyntext_vmt_fields(new_type, parent_type) \
        sui_widget_vmt_fields( new_type, parent_type) \
        int (*update)(parent_type##_t *self); \
        int (*settext)(parent_type##_t *self, utf8 *newtext, int index);
//        int (*clear)(parent_type##_t *self); 

// functions have sui_widget_t* as first argument

typedef struct sui_wdyntext_vmt {
  sui_wdyntext_vmt_fields(sui_wdyntext, sui_widget)
} sui_wdyntext_vmt_t;

static inline sui_wdyntext_vmt_t *sui_wdyntext_vmt(sui_widget_t *self)
{
  return (sui_wdyntext_vmt_t*)(self->base.vmt);
}

extern sui_wdyntext_vmt_t sui_wdyntext_vmt_data;

/******************************************************************************
 * Dynamic text widget basic functions
 ******************************************************************************/
int sui_wdyntext_set_datasrc(sui_widget_t *wdg, void *ptr, unsigned long type);
int sui_wdyntext_set_timesrc(sui_widget_t *wdg, sui_dinfo_t *dtime, long ctime);
int sui_wdyntext_update(sui_widget_t *widget);
int sui_wdyntext_settext(sui_widget_t *widget, utf8 *newtext, int index);

sui_widget_t *sui_wdyntext( sui_rect_t *aplace, utf8 *alabel, sui_style_t *astyle, 
    sui_flags_t aflags, sui_dinfo_t *adi, sui_finfo_t *aformat, unsigned char aalign);

#endif
