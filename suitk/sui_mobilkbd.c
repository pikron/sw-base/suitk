/* sui_mobilkbd.c
 *
 * SUITK mobile keyboard support
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#include "sui_mobilkbd.h"

#include <ulut/ul_gavl.h>
#include <ulut/ul_gavlcust.h>

#include <ul_log.h>

UL_LOG_CUST(ulogd_suitk)

/* last used key and mode */
sui_mk_list_key_t sui_mk_last;

/* time flag for mobil keyboard */
unsigned long sui_mk_last_key_time = 0;
unsigned long sui_mk_max_key_time = 1000; /* maximal wait time to next press of the same key */
//int sui_mk_pressed_in_time = 0;

/* external function to get current (relative) time in milliseconds */
extern unsigned long get_current_time(void);

/* the current mobil keyboard map */
sui_mk_keyboard_map_t sui_mk_current_map;
sui_mk_list_key_t sui_mk_switch;


/******************************************************************************/
GAVL_CUST_NODE_INT_IMP(sui_mk_gavl, sui_mk_keyboard_map_t, sui_mk_key_map_t,
                       sui_mk_list_key_t, map_root, map_node, key_mode,
                       sui_mk_key_map_cmp)

/**
 * sui_mk_create_key_map_item - Create one dynamic item for mapping of mobile keyboard to character
 * @key: Key identifier.
 * @mode: Mode flags.
 * @string: String with characters assign to the key.
 *
 * The function creates a new dynamic mobil keyboard item for key mapping
 *
 * Return Value: The pointer to a key mapping item.
 *
 * File: sui_mobilkbd.c
 */
sui_mk_key_map_t *sui_mk_create_key_map_item(
                          unsigned short key, unsigned short mode, utf8 *string)
{
  sui_mk_key_map_t *ret = sui_malloc(sizeof(sui_mk_key_map_t));
  if (ret) {
    ret->key_mode.key = key;
    ret->key_mode.mode = mode;
    sui_utf8_inc_refcnt(string);
    ret->chars = string;
  }
  return ret;
}

/**
 * sui_mk_destroy_key_map_item - Removes dynamic item for mapping of mobile keyboard to character
 * @item: Pointer to item with key mapping,
 *
 * The function removes item from key mapping list.
 *
 * Return Value: The function returns zero as success otherwise it returns a negative value.
 *
 * File: sui_mobilkbd.c
 */
int sui_mk_destroy_key_map_item(sui_mk_key_map_t *item)
{
  if (item) {
    sui_utf8_dec_refcnt(item->chars);
    free(item);
    return 0;
  }
  return -1;
}

/******************************************************************************/
/* internal initial mobil keyboard map */
typedef struct sui_mk_init_map {
  unsigned short  key;
  unsigned short  mode;
  utf8           *chars;
} sui_mk_init_map_t;

/* default internal mobil keyboard */
SUI_CANBE_CONST
sui_mk_init_map_t sui_mk_default_map[] = {
  {0, 1, U8"0"}, /* special item with identifier (and position) of key to switch the current mode */
  {'0', SUKM_CAPITALS | SUKM_SMALL | SUKM_NUMBER, U8"0_"}, /* second character is dummy character for mode switching */
  {'1', SUKM_CAPITALS | SUKM_SMALL, U8" .,?!1:;-+#*()'""_@&$%/<>[]{}\\|=~"},
  {'1', SUKM_NUMBER,   U8"1"},
  {'2', SUKM_CAPITALS, U8"ABC2ÁČ"},
  {'2', SUKM_SMALL,    U8"abc2áč"},
  {'2', SUKM_NUMBER,   U8"2"},
  {'3', SUKM_CAPITALS, U8"DEF3ĎÉĚ"},
  {'3', SUKM_SMALL,    U8"def3ďéě"},
  {'3', SUKM_NUMBER,   U8"3"},
  {'4', SUKM_CAPITALS, U8"GHI4Í"},
  {'4', SUKM_SMALL,    U8"ghi4í"},
  {'4', SUKM_NUMBER,   U8"4"},
  {'5', SUKM_CAPITALS, U8"JKL5"},
  {'5', SUKM_SMALL,    U8"jkl5"},
  {'5', SUKM_NUMBER,   U8"5"},
  {'6', SUKM_CAPITALS, U8"MNO6ŇÓ"},
  {'6', SUKM_SMALL,    U8"mno6ňó"},
  {'6', SUKM_NUMBER,   U8"6"},
  {'7', SUKM_CAPITALS, U8"PQRS7ŘŠ"},
  {'7', SUKM_SMALL,    U8"pqrs7řš"},
  {'7', SUKM_NUMBER,   U8"7"},
  {'8', SUKM_CAPITALS, U8"TUV8ŤÚŮ"},
  {'8', SUKM_SMALL,    U8"tuv8ťúů"},
  {'8', SUKM_NUMBER,   U8"8"},
  {'9', SUKM_CAPITALS, U8"WXYZ9ÝŽ"},
  {'9', SUKM_SMALL,    U8"wxyz9ýž"},
  {'9', SUKM_NUMBER,   U8"9"},
  {'.', SUKM_NUMBER,   U8"."},
  {'.', SUKM_CAPITALS | SUKM_SMALL, U8".,?!1&-'@"},
  {0,0,NULL},
};

/**
 * sui_mk_create_default_map - Create default mobil keyboard mapping list
 *
 * The function creates default mapping list.
 *
 * Return Value: The function doesn't return any value.
 *
 * File: sui_mobilkbd.c
 */
int sui_mk_create_default_map(void)
{
  int i = 0;
  sui_mk_key_map_t *item = NULL;

  while (sui_mk_default_map[i].chars != NULL) {
    if (sui_mk_default_map[i].key==0) { /* as mode is saved position of */
      _UTF_UCS_TYPE ucs_char;
      sui_utf8_to_ucs((utf8char *)sui_mk_default_map[i].chars, &ucs_char);
      sui_mk_switch.key = ucs_char;
      sui_mk_switch.mode = sui_mk_default_map[i].mode; /* here it means number of repeating the key to switch */
    } else {
      item = sui_mk_create_key_map_item(sui_mk_default_map[i].key,
                                        sui_mk_default_map[i].mode,
                                        sui_mk_default_map[i].chars);
      if (sui_mk_gavl_insert(&sui_mk_current_map, item) <= 0) {
        sui_mk_destroy_key_map_item(item);
        return -1;
      }
    }
    i++;
  }
  return 0;
}

/**
 * sui_mk_init - Initiation of mobile-keyboard system
 *
 * File: sui_mobilkbd.c
 */
int sui_mk_init(void)
{
  sui_mk_gavl_init_root_field(&sui_mk_current_map);
  return sui_mk_create_default_map();
}

/**
 * sui_mk_clear - Remove all mapped keys from the current mobile-keyboard system
 *
 * Return Value: The function returns zero as success or a negative value as an error.
 *
 * File: sui_mobilkbd.c
 */
int sui_mk_clear(void)
{
  sui_mk_key_map_t *item = NULL;
  while((item=sui_mk_gavl_cut_first(&sui_mk_current_map))){
//ul_logdeb("mk_clear key %d(%c) for mode %d - str='%s'\n",item->key_mode.key,item->key_mode.key,item->key_mode.mode,sui_utf8_get_text(item->chars));
    if (sui_mk_destroy_key_map_item(item)<0) return -1;
  }
  return 0;
}

/**
 * sui_mk_done - Stop and remove mobil-keyboard system
 *
 * Return Value: The function returns zero as success or a negative value as an error.
 *
 * File: sui_mobilkbd.c
 */
int sui_mk_done(void)
{
  return sui_mk_clear();
}

/******************************************************************************/
/**
 * sui_mk_translate_key2char - Translate key identifier to a character
 * @key: Key to translation (as returned in event).
 * @mode: Mode of mobile phone keyboard (Capital letters, small letters, numbers)
 * and last key counter. It is updated in this function.
 * @retchar: Pointer to output buffer for returned character.
 *
 * The function translates desired key to a character from mobile-keyboard.
 *
 * Return Value: The function returns processing flags. The returned pointer can
 * points to string, therefore the character needn't be followed by zero.
 *
 * File: sui_mobilkbd.c
 */
int sui_mk_translate_key2char(unsigned short key, unsigned short *mode, utf8char **retchar)
{
  int ret = SUI_MK_RET_NOKEY; /* default - don't remove the previous character */
  utf8char *chtmp = NULL;
  sui_mk_key_map_t *item = NULL;
  unsigned long curtime = get_current_time(); /* get current time */

/* check key time */
  if ((key!=sui_mk_last.key)||((curtime-sui_mk_last_key_time)>sui_mk_max_key_time)) {
    *mode = *mode & ~SUKM_CNTMASK; /* position in string = 0 */
  }

/* mode switching */
  if ((key==sui_mk_switch.key)&&((*mode & SUKM_CNTMASK)==sui_mk_switch.mode)) {
    unsigned short nm;
    if ((sui_mk_switch.mode>0)&&!(*mode & SUKM_MODECHNG)) {
      ret |= SUI_MK_RET_REMOVE;
    }
    switch (*mode & SUKM_MODEMASK) {
      case SUKM_CAPITALS: nm = SUKM_SMALL; break;
      case SUKM_SMALL: nm = SUKM_NUMBER; break;
      default: nm = SUKM_CAPITALS; break;
    }

    *mode = nm | SUKM_MODECHNG | (*mode & SUKM_CNTMASK);
    ret |= SUI_MK_RET_OK;

  } else {
/* find the key */
    gavl_cust_for_each(sui_mk_gavl, &sui_mk_current_map, item) {
//ul_logdeb("mk_test key %d(%c) for mode %d - str='%s'\n",item->key_mode.key,item->key_mode.key,item->key_mode.mode,sui_utf8_get_text(item->chars));
      if ((item->key_mode.key == key) && (item->key_mode.mode & (*mode & SUKM_MODEMASK))) break;
    }
    if (!item) return ret;

    if ((sui_mk_last.key==key)&&((curtime-sui_mk_last_key_time)<sui_mk_max_key_time)) {
      if (!(*mode & SUKM_MODECHNG))
        ret |= SUI_MK_RET_REMOVE;
    }
    chtmp = sui_utf8_get_text(item->chars);
    chtmp += sui_utf8_count_bytes(chtmp, *mode & SUKM_CNTMASK);
    *mode = *mode + 1;
    if((*mode & SUKM_CNTMASK) >= sui_utf8_length(item->chars))
      *mode &= ~SUKM_CNTMASK;
    
    *mode &= ~SUKM_MODECHNG;
    ret |= SUI_MK_RET_OK | SUI_MK_RET_CHAR;
    *retchar = chtmp;
  }

  sui_mk_last.key = key;
  sui_mk_last.mode = *mode;
  sui_mk_last_key_time = curtime;
  return ret;
}

/**
 * sui_mk_clear_state - Clear position of the last returned character and last key time
 *
 * The function should be called as reaction to SUCM_FOCUSSET command event.
 *
 * Return Value: The function returns always zero.
 *
 * File: sui_mobilkbd.c
 */
int sui_mk_clear_state(void)
{
  sui_mk_last_key_time = 0;
  sui_mk_last.key = 0;
  sui_mk_last.mode = SUKM_CAPITALS;
  return 0;
}

/**
 * sui_mk_in_repeat_mode - Test if key repeating time doesn't go out
 *
 * The function tests if mobil-keyboard timeout was reached.
 *
 * Return Value: The function returns zero if key pressing time is out otherwise it returns one
 *
 * File: sui_mobilkbd.c
 */
int sui_mk_in_repeat_mode(void)
{
  if (sui_mk_last_key_time == 0) return 0; /* there can be an error - if get_current_time can return 0 as valid value */
  return ((get_current_time() - sui_mk_last_key_time)>sui_mk_max_key_time)?0:1;
}

/******************************************************************************/
/**
 * sui_mk_set_repeat_time - Set maximal repeat time of mobile keyboard
 * Time_ms: New maximal key repeat timeout in [ms].
 *
 * The function sets a new value of key-repeating timeout.
 *
 * Return Value: The function returns zero if the key repeting timeout was set.
 *
 * File: sui_mobilkbd.c
 */
int sui_mk_set_repeat_time(unsigned long time_ms)
{
  if (time_ms) {
    sui_mk_max_key_time = time_ms;
    return 0;
  } else
    return -1;
}

/**
 * int sui_mk_set_mapping - Set new mobile keyboard map
 * @srclist: Pointer to a source list which contains information for creating new keyboard mapping.
 *
 * The function sets new keyboard map or it sets default map if $srclist is NULL.
 *
 * Return Value: The function returns zero as success.
 *
 * File: sui_mobilkbd.c
 */
int sui_mk_set_mapping(sui_list_t *srclist)
{
  int ret = 0;

  if ((ret=sui_mk_clear())<0) return ret;

  if (!srclist) {
    return sui_mk_create_default_map();
  } else {
    _UTF_UCS_TYPE ucs_char;
    sui_mk_key_map_t *keyitem = NULL;
    sui_list_item_t *item = NULL;
    utf8 *chars = NULL;

    gavl_cust_for_each(sui_list_gavl, srclist, item) {

      if ((item->idx & 0xff) == 0) { /* set current switch */
        ret = sui_list_item_get_text(item, &chars);
        sui_utf8_to_ucs(sui_utf8_get_text(chars), &ucs_char);
        sui_utf8_dec_refcnt(chars);
        sui_mk_switch.key = ucs_char;
        sui_mk_switch.mode = (item->idx & 0xff00) >> 8;
      } else {
        ret = sui_list_item_get_text(item, &chars);
        if (ret<0) return ret;
        keyitem = sui_mk_create_key_map_item(item->idx & 0xff,
                        (item->idx & 0xff00), chars);
        sui_utf8_dec_refcnt(chars); chars = NULL;
        if (!keyitem) return -1;
        if (sui_mk_gavl_insert(&sui_mk_current_map, keyitem) <= 0) {
          sui_mk_destroy_key_map_item(keyitem);
          return -1;
//        } else {
//ul_logdeb("mk_added key %d(%c) for mode %d - str='%s'\n",keyitem->key_mode.key,keyitem->key_mode.key,keyitem->key_mode.mode,sui_utf8_get_text(keyitem->chars));
        }
      }
    }
  }
  return ret;
}

/******************************************************************************/
/* FIXME: this solution is only for drawing mobilkey to one widget at the same time */

sui_align_t   sui_mk_draw_align = SUAL_CENTER; /* MK drawing align */
sui_hevent_t *sui_mk_draw_hevent = NULL; /* original drawing handle event */

/**
 * sui_mk_draw - Draw mobile keyboard help frame
 * @dc: Pointer to a device context.
 * @widget: Pointer to a widget with mobile keyboard.
 *
 * The function draws mobil-keyboard indicator for the specified widget
 *
 * Return Value: The function always returns zero.
 *
 * File: sui_mobilkbd.c
 */
int sui_mk_draw(sui_dc_t *dc, sui_widget_t *widget)
{
  sui_textdims_t txts;
  sui_mk_key_map_t *curkmap;
  utf8char *text;
  int first, pos, count;
  sui_rect_t box = {.x=0, .y=0, .w=widget->place.w, .h=widget->place.h};
  sui_dcfont_t *pdcfont;

/* get text from mobil keyboard */
//    curkmap = sui_mk_gavl_find(&sui_mk_current_map, &sui_mk_last);
  /* find the key */
  gavl_cust_for_each(sui_mk_gavl, &sui_mk_current_map, curkmap) {
    if ((curkmap->key_mode.key == sui_mk_last.key) && (curkmap->key_mode.mode & (sui_mk_last.mode & SUKM_MODEMASK))) break;
  }

  if (curkmap) {
    count = sui_utf8_length(curkmap->chars);
    pos = sui_mk_last.mode & SUKM_CNTMASK;
  /* draw maximally 5 characters ... */
    if (count > 5) {
      first = pos-2; if (first < 0) first = 0;
      if (first+5>count) first = count-5;
      count = 5;
    } else {
      first = 0;
    }
    text = sui_utf8_get_text(curkmap->chars);
    text+= sui_utf8_count_bytes(text, first);

/* drawing */
    sui_draw_init(dc, widget);
    sui_gdi_set_carea(dc, &box);

    pdcfont = sui_font_prepare(dc, widget->style->fonti->font,
                                widget->style->fonti->size);
    sui_font_set(dc, pdcfont);

    sui_text_get_size(dc, pdcfont, &txts, count, U8 text, sui_mk_draw_align);
/* text aligning ... */
    {
      sui_point_t tbox = {.x=box.w,.y=box.h};
      sui_point_t tsize = {.x=txts.w+2,.y=txts.h+2};
      sui_point_t tpos = {.x=0,.y=0};
      sui_align_object(&tbox, &tpos, &tsize, txts.b, widget->style, sui_mk_draw_align);
      box.x = tpos.x; box.y = tpos.y;
      box.w = txts.w+2; box.h = txts.h+2;
    }

/* frame drawing */
    sui_set_fgcolor(dc, widget, SUC_FRAME);
    sui_draw_rect_box(dc, &box, 1);

/* text drawing */
    sui_set_bgcolor(dc, widget, SUC_LABELBACK);
    sui_set_fgcolor(dc, widget, SUC_LABEL);
    sui_draw_text(dc, box.x+1, box.y+1, count, U8 text, sui_mk_draw_align);
/* mark current character */
    sui_text_get_size(dc, pdcfont, &txts, pos-first-1, U8 text, sui_mk_draw_align);
    text+= sui_utf8_count_bytes(text, pos-first-1);
    box.x += txts.w; box.y += 1;
    sui_text_get_size(dc, pdcfont, &txts, 1, U8 text, sui_mk_draw_align);
    box.w = txts.w; box.h = txts.h;
  
    sui_set_fgcolor(dc, widget, SUC_FRAME);
    sui_draw_rect_box(dc, &box, 1);
  
    sui_gdi_clear_carea(dc);
    sui_draw_done(dc, widget);
  }
  return 0;
}

/**
 * sui_mk_hevent_drawing - Include mobil keyboard drawing to the existing handle event function
 * @widget: Pointer to a widget.
 * @event: Pointer to widget's incomming event.
 *
 * The function is included by function 'sui_mk_set_drawing'
 * to the widget handle event function as additional processing function.
 *
 * Return Value: The function doesn't return any value.
 *
 * File: sui_mobilkbd.c
 */
void sui_mk_hevent_drawing(sui_widget_t *widget, sui_event_t *event)
{
  sui_dc_t *dc = NULL;

  if (event->what == SUEV_DRAW || event->what == SUEV_REDRAW) {
    dc = event->draw.dc;
  }

  if (sui_mk_draw_hevent) sui_mk_draw_hevent(widget, event);

  if ((event->what == SUEV_DRAW || event->what == SUEV_REDRAW) &&
      sui_mk_in_repeat_mode() && dc) {
    sui_mk_draw(dc, widget);
  }
}

/**
 * sui_mk_set_drawing - Set widget handle event function to mobil keyboard drawing function
 * @widget: Pointer to a widget for modifying handle-event function.
 * @align: Aligning of mobile keyboard indicator.
 *
 * The function adds special handle_event function for drawing MK indicator to the widget.
 *
 * Return Value: The function returns zero if all is processed correctly.
 *
 * File: sui_mobilkbd.c
 */
int sui_mk_set_drawing(sui_widget_t *widget, sui_align_t align)
{
  if (widget->hevent==sui_mk_hevent_drawing || sui_mk_draw_hevent) return -1;
  sui_mk_draw_hevent = widget->hevent;
  widget->hevent = sui_mk_hevent_drawing;
  sui_mk_draw_align = align | SUAL_INSIDE;
  return 0;
}

/**
 * sui_mk_remove_drawing - Remove mobil keyboard drawing function from widget handle event function
 * @widget: Pointer to a widget with modified handle event function.
 *
 * The function removes special handle-event function for drawing MK indicator from the widget.
 *
 * Return Value: The function returns zero if all is correctly processed.
 *
 * File: sui_mobilkbd.c
 */
int sui_mk_remove_drawing(sui_widget_t *widget)
{
  if ((widget->hevent == sui_mk_hevent_drawing) && sui_mk_draw_hevent) {
    widget->hevent = sui_mk_draw_hevent;
    sui_mk_draw_hevent = NULL;
    return 0;
  }
  return -1;
}

/**
 * sui_mk_mode_rdval - dinfo read-value function for reading text representation of the current mobile keyboard mode
 * @datai: pointer to dinfo
 * @idx: required index of value
 * @buf: pointer to buffer for UTF8 string
 *
 */
int sui_mk_mode_rdval(sui_dinfo_t *datai, long idx, void *buf)
{
  //sui_utf8_t *mode = NULL;
  if (datai->tinfo != SUI_TYPE_UTF8) return SUI_RET_ERR;
  switch(sui_mk_last.mode & SUKM_MODEMASK) {
    case SUKM_CAPITALS:
      *(utf8 **)buf = U8 "ABC";
      break;
    case SUKM_SMALL:
      *(utf8 **)buf = U8 "abc";
      break;
    case SUKM_NUMBER:
      *(utf8 **)buf = U8 "123";
      break;
    case SUKM_SELECT:
      *(utf8 **)buf = U8 ".?!";
      break;
    case SUKM_NONE:
    default:
      *(utf8 **)buf = U8 "---";
      break;
  }
  return SUI_RET_OK;
}
