/* sui_environment.c
 *
 * SUITK environment - describes application environment's specific structures and functions
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#include "sui_environment.h"
#include <suiut/support.h>

#include <suitk/sui_widget.h>
#include <suitk/sui_wgroup.h>

#include <suiut/sui_typemethods.h>
#include <suiut/namespace.h>

#include <ulut/ul_gavl.h>
#include <ulut/ul_gavlcust.h>

#include <ul_log.h>

UL_LOG_CUST(ulogd_suitk)

extern unsigned long get_current_time(void);

struct sui_dc *sui_globdc = NULL;

/* Here is saved return code of processing of the last action */
int sui_environment_last_action_retcode = 0;

/* FIXME: change global sui_old_gsi_fnc variable to environment structure field */
static gsi_fnc_t *sui_old_gsi_fnc = NULL;
static MWCOREFONT *sui_gsi_cached_fonts_ptr;
static int sui_gsi_cached_fonts_count;

/* fnc replaces count of core fonts in original get screen info */
/**
 * sui_env_get_screen_info - Replace MW 'GetScreenInfo' function
 * @psd: Pointer to a PSD structure.
 * @psi: Pointer to a screen info structure.
 *
 * The function is application's specific 'GetScreenInfo' function which replaces MW's function.
 *
 * Return Value: The function does not return any value.
 *
 * File: sui_environment.c
 */
static void sui_env_get_screen_info(PSD psd, PMWSCREENINFO psi)
{
  sui_old_gsi_fnc(psd, psi);
  if (psd->builtin_fonts == sui_gsi_cached_fonts_ptr) {
    psi->fonts = sui_gsi_cached_fonts_count;
  } else {
    int fonts = 0;
    while (psd->builtin_fonts[fonts].fontprocs != NULL)
      fonts++;
    psi->fonts = fonts;
    sui_gsi_cached_fonts_ptr = psd->builtin_fonts;
    sui_gsi_cached_fonts_count = fonts;
  }
}

/**
 * sui_env_set_new_fonts - Set new built-in fonts to screen device
 * @new_fonts: Pointer to an array of new built-in fonts.
 * @psd: Pointer to a screen device, where will be new fonts saved.
 *
 * The function sets new application built-in fonts to the screen device.
 * It sets new get_screen_info function to the PSD and returns pointer to the original function.
 *
 * Return Value: The function returns pointer to the original PSD get_screen_info function.
 *
 * File: sui_environment.c
 */
gsi_fnc_t *sui_env_set_new_fonts(MWCOREFONT *new_fonts, PSD psd)
{
  psd->builtin_fonts = new_fonts;
  sui_gsi_cached_fonts_ptr = NULL;
  if (psd->GetScreenInfo != sui_env_get_screen_info) {
    sui_old_gsi_fnc = psd->GetScreenInfo;
    psd->GetScreenInfo = sui_env_get_screen_info;
  }
  return sui_old_gsi_fnc;
}

/******************************************************************************/
/* Environment */
/******************************************************************************/

/******************************************************************************
 * Condition
 ******************************************************************************/
/**
 * sui_condition_inc_refcnt - Increment condition reference counter
 * @cond: Pointer to a condition structure.
 *
 * The function increments condition structure reference counter.
 *
 * Return Value: The function returns value of the condition's reference counter after incrementing.
 *
 * File: sui_environment.c
 */
sui_refcnt_t sui_condition_inc_refcnt(sui_condition_t *cond)
{
  if (!cond) return SUI_REFCNTERR;
  if (cond->refcnt >= 0) cond->refcnt++;
  return cond->refcnt;
}

/**
 * sui_condition_dec_refcnt - Decrement condition reference counter
 * @cond: Pointer to a condition structure.
 *
 * The function decrements the condition structure reference counter.
 * If the counter is decremented to zero condition structure is deleted.
 *
 * Return Value: The function returns value of the condition's reference counter after decrementing.
 *
 * File: sui_environment.c
 */
sui_refcnt_t sui_condition_dec_refcnt(sui_condition_t *cond)
{
  sui_refcnt_t ref;
  if (!cond) return SUI_REFCNTERR;
  if (cond->refcnt > 0) cond->refcnt--;
  if (!(ref = cond->refcnt)) {
    if (cond->str_term) sui_utf8_dec_refcnt(cond->str_term);
    if (cond->str_focus) sui_utf8_dec_refcnt(cond->str_focus);
    if (cond->term) sui_term_dec_refcnt(cond->term);
    if (cond->focus) sui_dec_refcnt(cond->focus);
    if (cond->lastst) sui_utf8_dec_refcnt(cond->lastst);
    if (cond->lastsubst) sui_utf8_dec_refcnt(cond->lastsubst);
    sui_obj_done_vmt((sui_obj_t *)cond, UL_CAST_UNQ1(sui_obj_vmt_t *, &sui_condition_vmt_data));
    free(cond);
  }
  return ref;
}

SUI_TYPE_DECLARE_REFCNT(condition, SUI_TYPE_CONDITION, struct sui_condition, NULL,
                         sui_condition_inc_refcnt, sui_condition_dec_refcnt)

/**
 * sui_condition_init - Initiate condition structure
 * @cond: Pointer to a condition structure.
 *
 * The function initiates condition structure and sets default values to the condition structure fields.
 *
 * Return Value: The function returns zero as success and a negative value if any error occurs.
 *
 * File: sui_environment.c
 */
int sui_condition_init(sui_condition_t *cond)
{
  if (!cond) return 0;
  
  if (cond->term) {
    sui_term_dec_refcnt(cond->term);
    cond->term = NULL;
  }
  if (cond->str_term) {
    cond->term = sui_term_create_from_prefix_string(sui_utf8_get_text(cond->str_term));
    if (cond->term)
      cond->conflags |= SUI_COND_FLG_TERM;
    else {
      ul_logerr("COND_INIT:The term '%s' wasn't initialized because the term contains an error\n", sui_utf8_get_text(cond->str_term));
      return -1;
    }
  }
  if (cond->focus) {
    sui_dec_refcnt(cond->focus);
    cond->focus = NULL;
  }
  if (cond->str_focus) {
    ns_object_t *obj = ns_find_widget_in_ns(sui_utf8_get_text(cond->str_focus));
    if (obj) {
      sui_inc_refcnt(obj->ptr);
      cond->focus = obj->ptr;
      cond->conflags |= SUI_COND_FLG_FOCUS;
    } else {
      ul_logerr("COND_INIT: Widget '%s' used in condition wasn't found in namespace\n", sui_utf8_get_text(cond->str_focus));
      return -1;
    }
  }
  if (cond->state_time)
    cond->conflags |= SUI_COND_FLG_TIME;
  if (cond->user_time)
    cond->conflags |= SUI_COND_FLG_USERTIME;
  if (cond->evkey) {
    cond->event = SUEV_KDOWN;
    cond->conflags = (cond->conflags & ~(SUI_COND_FLG_EVCMD)) | SUI_COND_FLG_EVKEY | SUI_COND_FLG_EVENT;
  }
  if (cond->evcmd) {
    cond->event = SUEV_COMMAND;
    cond->conflags = (cond->conflags & ~(SUI_COND_FLG_EVKEY)) | SUI_COND_FLG_EVCMD | SUI_COND_FLG_EVENT;
  }
  if (cond->event)
    cond->conflags |= SUI_COND_FLG_EVENT;

  if (cond->lastst)
    cond->conflags |= SUI_COND_FLG_LASTST;
  if (cond->lastsubst)
    cond->conflags |= SUI_COND_FLG_LASTSST;

  return 0;
}

/******************************************************************************
 * Action function
 ******************************************************************************/
  SUI_TYPE_DECLARE_SIMPLE(action_fcn, SUI_TYPE_ACTIONFCN, sui_action_fcn_t *, NULL)

/******************************************************************************
 * Action
 ******************************************************************************/
/**
 * sui_action_inc_refcnt - Increment action reference counter
 * @action: Pointer to an action structure.
 *
 * The function increments action structure reference counter.
 *
 * Return Value: The function returns value of the action's reference counter after incrementing.
 *
 * File: sui_environment.c
 */
sui_refcnt_t sui_action_inc_refcnt(sui_action_t *action)
{
  if (!action) return SUI_REFCNTERR;
  if (action->refcnt >= 0) action->refcnt++;
  return action->refcnt;
}

/**
 * sui_action_dec_refcnt - Decrement action reference counter
 * @action: Pointer to an action structure.
 *
 * The function decrements the action structure reference counter.
 * If the counter is decremented to zero action structure is deleted.
 *
 * Return Value: The function returns value of the action's reference counter after decrementing.
 *
 * File: sui_environment.c
 */
sui_refcnt_t sui_action_dec_refcnt(sui_action_t *action)
{
  sui_refcnt_t ref;
  if (!action) return SUI_REFCNTERR;
  if (action->refcnt > 0) action->refcnt--;
  if (!(ref = action->refcnt)) {
    if (action->condition) sui_condition_dec_refcnt(action->condition);
    if (action->wdg_src) sui_dec_refcnt(action->wdg_src);
    if (action->wdg_dst) sui_dec_refcnt(action->wdg_dst);
    if (action->data_src) sui_dinfo_dec_refcnt(action->data_src);
    if (action->data_dst) sui_dinfo_dec_refcnt(action->data_dst);
    if (action->text) sui_utf8_dec_refcnt(action->text);
    if (action->list) sui_list_dec_refcnt(action->list);
    sui_obj_done_vmt((sui_obj_t *)action, UL_CAST_UNQ1(sui_obj_vmt_t *, &sui_action_vmt_data));
    free(action);
  }
  return ref;
}

SUI_TYPE_DECLARE_REFCNT(action, SUI_TYPE_ACTION, struct sui_action, NULL,
                         sui_action_inc_refcnt, sui_action_dec_refcnt)

/**
 * sui_action_init - Initiate action structure
 * @action: Pointer to an action structure.
 *
 * The function initiates action structure and sets default values to the action structure fields.
 *
 * Return Value: The function returns zero as success and a negative value if any error occurs.
 *
 * File: sui_environment.c
 */
static inline
int sui_action_init(sui_action_t *action)
{
  return sui_condition_init(action->condition);
}

/**
 * sui_action_init_all - Initiate each action in action list
 * @acthead: Pointer to an array of actions.
 *
 * The function initiates each action structure in the action list.
 *
 * Return Value: The function returns zero as success and a negative value if any error occurs.
 *
 * File: sui_environment.c
 */
static inline
int sui_action_init_all(sui_action_t *acthead)
{
  int ret = 0;
  while(acthead && !ret) {
    ret |= sui_action_init(acthead);
    acthead = acthead->next_action;
  }
  return ret;
}

/**
 * sui_action_add_next - Add new action structure to the end of an action list
 * @head: Pointer to a pointer to action list.
 * @add: Pointer to a new action for adding.
 *
 * The function adds a new action structure to the action list which is specified by its pointer.
 *
 * Return Value: The function returns zero as success and a negative value if any error occurs.
 *
 * File: sui_environment.c
 */
int sui_action_add_next(sui_action_t **head, sui_action_t *add)
{
  if (!head || !add) return -1;
  if (*head) {
    sui_action_t *par = *head;
    while (par->next_action)
      par = par->next_action;
    par->next_action = add;
  } else
    *head = add;
  sui_action_inc_refcnt(add);
  add->next_action = NULL;
  return 0;
}

/**
 * sui_action_process - Run action function defined by action structure if the action conditions is fulfilled
 * @appl: Pointer to an application structure.
 * @action: Pointer to an action structure.
 *
 * The function tests action conditions and if they are fulfilled, the action function is called.
 * If the action hasn't got any conditions,the action function is called.
 *
 * Return Value: The function returns zero as success and a negative value if any error occurs.
 *
 * File: sui_environment.c
 */
int sui_action_process(struct sui_application *appl, sui_action_t *action)
{
  if (!appl || !action || !action->function) return -1;
  if (!sui_appl_check_condition(appl, action->condition, NULL)) { /* event is NULL ... it cannot be used in action's condition */
    int ret = action->function(appl, action);
    sui_environment_last_action_retcode = (ret) ? -1 : 0;
    return ret;
  } else
    sui_environment_last_action_retcode = 1;
  return 0; /* action won't be invoked because condition isn't fulfilled */
}

/**
 * sui_action_process_all - Process all actions in an action array
 * @appl: Pointer to an application structure.
 * @action_head: Pointer to an action array.
 *
 * The function tries to call all actions in the array.
 *
 * Return Value: The function returns zero as success and a negative value if any error occurs.
 *
 * File: sui_environment.c
 */
int sui_action_process_all(struct sui_application *appl, sui_action_t *action_head)
{
  int i = 0;
  for (; action_head; action_head = action_head->next_action) {
    i++;
    if (sui_action_process(appl, action_head)<0)
      ul_logerr("ACT: !!! Action(#%d)(under '%s'/'%s') wasn't successful !!!\n", i,
                sui_utf8_get_text(appl->cstate->name),
                sui_utf8_get_text(appl->csubstate->name));
  }
  return 0;
}


/******************************************************************************
 * Transition
 ******************************************************************************/
/**
 * sui_transition_inc_refcnt - Increment transition reference counter
 * @transition: Pointer to a transition structure.
 *
 * The function increments transition structure reference counter.
 *
 * Return Value: The function returns value of the transition's reference counter after incrementing.
 *
 * File: sui_environment.c
 */
sui_refcnt_t sui_transition_inc_refcnt(sui_transition_t *transition)
{
  if (!transition) return SUI_REFCNTERR;
  if (transition->refcnt >= 0) transition->refcnt++;
  return transition->refcnt;
}

/**
 * sui_transition_dec_refcnt - Decrement transition reference counter
 * @transition: Pointer to a transition structure.
 *
 * The function decrements the transition structure reference counter.
 * If the counter is decremented to zero transition structure is deleted.
 *
 * Return Value: The function returns value of the transition's reference counter after decrementing.
 *
 * File: sui_environment.c
 */
sui_refcnt_t sui_transition_dec_refcnt(sui_transition_t *transition)
{
  sui_refcnt_t ref;
  if (!transition) return SUI_REFCNTERR;
  if (transition->refcnt > 0) transition->refcnt--;
  if (!(ref = transition->refcnt)) {
    if (transition->scenario) sui_utf8_dec_refcnt(transition->scenario);
    if (transition->tostate) sui_utf8_dec_refcnt(transition->tostate);
    if (transition->tosubst) sui_utf8_dec_refcnt(transition->tosubst);
    if (transition->towidget) sui_utf8_dec_refcnt(transition->towidget);
    if (transition->cond) sui_condition_dec_refcnt(transition->cond);

    if (transition->abefore_head) {
      sui_action_t *par, *next = NULL;
      for(par=transition->abefore_head; par; par=next) {
        next = par->next_action;
        sui_action_dec_refcnt(par);
      }
    }
    transition->abefore_head = NULL;

    if (transition->aafter_head) {
      sui_action_t *par, *next = NULL;
      for(par=transition->aafter_head; par; par=next) {
        next = par->next_action;
        sui_action_dec_refcnt(par);
      }
    }
    transition->aafter_head = NULL;
    sui_obj_done_vmt((sui_obj_t *)transition, UL_CAST_UNQ1(sui_obj_vmt_t *, &sui_transition_vmt_data));
    free(transition);
  }
  return ref;
}

SUI_TYPE_DECLARE_REFCNT(transition, SUI_TYPE_TRANSITION, struct sui_transition, NULL,
                         sui_transition_inc_refcnt, sui_transition_dec_refcnt)

/**
 * sui_transition_add_next - Add new transition structure to the end of a transition list
 * @head: Pointer to a pointer to transition list.
 * @add: Pointer to a new transition for adding.
 *
 * The function adds a new transition structure to the transition list which is specified by its pointer.
 *
 * Return Value: The function returns zero as success and a negative value if any error occurs.
 *
 * File: sui_environment.c
 */
int sui_transition_add_next(sui_transition_t **head, sui_transition_t *add)
{
  if (!head || !add) return -1;
  if (*head) {
    sui_transition_t *par = *head;
    while (par->next_trans)
      par = par->next_trans;
    par->next_trans = add;
  } else
    *head = add;
  sui_transition_inc_refcnt(add);
  add->next_trans = NULL;
  return 0;
}

/**
 * sui_transition_init - Initiate transition structure
 * @trans: Pointer to a transition structure.
 *
 * The function initiates the transition structure and sets default values to the transition structure fields.
 *
 * Return Value: The function returns zero as success and a negative value if any error occurs.
 *
 * File: sui_environment.c
 */
int sui_transition_init(sui_transition_t *trans)
{
  int ret = sui_condition_init(trans->cond);
  ret |= sui_action_init_all(trans->abefore_head);
  ret |= sui_action_init_all(trans->aafter_head);
  return ret;
}

/**
 * sui_transition_init_all - Initiate each transition in a transition list
 * @thead: Pointer to an array of transitions.
 *
 * The function initiates each transition structure in the transition list.
 *
 * Return Value: The function returns zero as success and a negative value if any error occurs.
 *
 * File: sui_environment.c
 */
int sui_transition_init_all(sui_transition_t *thead)
{
  int ret = 0;
  while(thead && !ret) {
    ret |= sui_transition_init(thead);
    thead = thead->next_trans;
  }
  return ret;
}


/******************************************************************************
 * SubState Dialog
 ******************************************************************************/
/**
 * sui_ssdialog_inc_refcnt - Increment substate dialog reference counter
 * @ssd: Pointer to a substate dialog structure.
 *
 * The function increments substate dialog structure reference counter.
 *
 * Return Value: The function returns value of the substate-dialog's reference counter after incrementing.
 *
 * File: sui_environment.c
 */
sui_refcnt_t sui_ssdialog_inc_refcnt(sui_ssdialog_t *ssd)
{
  if (!ssd) return SUI_REFCNTERR;
  if (ssd->refcnt >= 0) ssd->refcnt++;
  return ssd->refcnt;
}

/**
 * sui_ssdialog_dec_refcnt - Decrement substate dialog reference counter
 * @ssd: Pointer to a substate dialog structure.
 *
 * The function decrements the substate dialog structure reference counter.
 * If the counter is decremented to zero substate dialog structure is deleted.
 *
 * Return Value: The function returns value of the substate-dialog's reference counter after decrementing.
 *
 * File: sui_environment.c
 */
sui_refcnt_t sui_ssdialog_dec_refcnt(sui_ssdialog_t *ssd)
{
  sui_refcnt_t ref;
  if (!ssd) return SUI_REFCNTERR;
  if (ssd->refcnt > 0) ssd->refcnt--;
  if (!(ref = ssd->refcnt)) {
    if (ssd->cond) sui_condition_dec_refcnt(ssd->cond);
    ssd->cond = NULL;
    if (ssd->exitcond) sui_condition_dec_refcnt(ssd->exitcond);
    ssd->exitcond = NULL;
    if (ssd->dialog) sui_dec_refcnt(ssd->dialog);
    ssd->dialog = NULL;

    if (ssd->abefore_head) {
      sui_action_t *par, *next = NULL;
      for(par=ssd->abefore_head; par; par=next) {
        next = par->next_action;
        sui_action_dec_refcnt(par);
      }
    }
    ssd->abefore_head = NULL;

    if (ssd->aafter_head) {
      sui_action_t *par, *next = NULL;
      for(par=ssd->aafter_head; par; par=next) {
        next = par->next_action;
        sui_action_dec_refcnt(par);
      }
    }
    ssd->aafter_head = NULL;
    sui_obj_done_vmt((sui_obj_t *)ssd, UL_CAST_UNQ1(sui_obj_vmt_t *, &sui_ssdialog_vmt_data));
    free(ssd);
  }
  return ref;
}

SUI_TYPE_DECLARE_REFCNT(ssdialog, SUI_TYPE_SSDIALOG, struct sui_ssdialog, NULL,
                         sui_ssdialog_inc_refcnt, sui_ssdialog_dec_refcnt)

/**
 * sui_ssdialog_add_next - Add new substate dialog structure to the end of a substate dialog list
 * @head: Pointer to a pointer to substate dialog list.
 * @add: Pointer to a new substate dialog for adding.
 *
 * The function adds a new substate dialog structure to the substate dialog list which is specified by its pointer.
 *
 * Return Value: The function returns zero as success and a negative value if any error occurs.
 *
 * File: sui_environment.c
 */
int sui_ssdialog_add_next(sui_ssdialog_t **head, sui_ssdialog_t *add)
{
  if (!head || !add) return -1;
  if (*head) {
    sui_ssdialog_t *par = *head;
    while (par->next_dlg)
      par = par->next_dlg;
    par->next_dlg = add;
  } else
    *head = add;
  sui_ssdialog_inc_refcnt(add);
  add->next_dlg = NULL;
  return 0;
}

/**
 * sui_ssdialog_init - Initiate substate dialog structure
 * @ssd: Pointer to a substate dialog structure.
 *
 * The function initiates the substate dialog structure and sets default values to the substate dialog structure fields.
 *
 * Return Value: The function returns zero as success and a negative value if any error occurs.
 *
 * File: sui_environment.c
 */
int sui_ssdialog_init(sui_ssdialog_t *ssd)
{
  int ret = sui_condition_init(ssd->cond);
  ret |= sui_condition_init(ssd->exitcond);
  ret |= sui_action_init_all(ssd->abefore_head);
  ret |= sui_action_init_all(ssd->aafter_head);
  return ret;
}

/**
 * sui_ssdialog_init_all - Initiate each substate dialog in a substate dialog list
 * @ssdhead: Pointer to an array of substate dialogs.
 *
 * The function initiates each substate dialog structure in the substate dialog list.
 *
 * Return Value: The function returns zero as success and a negative value if any error occurs.
 *
 * File: sui_environment.c
 */
int sui_ssdialog_init_all(sui_ssdialog_t *ssdhead)
{
  int ret = 0;
  while(ssdhead && !ret) {
    ret |= sui_ssdialog_init(ssdhead);
    ssdhead = ssdhead->next_dlg;
  }
  return ret;
}

/******************************************************************************
 * State function
 ******************************************************************************/
SUI_TYPE_DECLARE_SIMPLE(state_fcn, SUI_TYPE_STATEFCN, sui_state_fcn_t *, NULL)

/******************************************************************************
 * Substate
 ******************************************************************************/
/**
 * sui_substate_inc_refcnt - Increment substate reference counter
 * @substate: Pointer to a substate structure.
 *
 * The function increments substate structure reference counter.
 *
 * Return Value: The function returns value of the substate's reference counter after incrementing.
 *
 * File: sui_environment.c
 */
sui_refcnt_t sui_substate_inc_refcnt(sui_substate_t *substate)
{
  if (!substate) return SUI_REFCNTERR;
  if (substate->refcnt >= 0) substate->refcnt++;
  return substate->refcnt;
}

/**
 * sui_substate_dec_refcnt - Decrement substate reference counter
 * @substate: Pointer to a substate structure.
 *
 * The function decrements the substate structure reference counter.
 * If the counter is decremented to zero substate structure is deleted.
 *
 * Return Value: The function returns value of the substate's reference counter after decrementing.
 *
 * File: sui_environment.c
 */
sui_refcnt_t sui_substate_dec_refcnt(sui_substate_t *substate)
{
  sui_refcnt_t ref;
  if (!substate) return SUI_REFCNTERR;
  if (substate->refcnt > 0) substate->refcnt--;
  if (!(ref = substate->refcnt)) {
    if (substate->name) sui_utf8_dec_refcnt(substate->name);
    substate->name = NULL;
    if (substate->init_focus) sui_utf8_dec_refcnt(substate->init_focus);
    substate->init_focus = NULL;

    if (substate->trans_head) {
      sui_transition_t *par, *next = NULL;
      for(par=substate->trans_head; par; par=next) {
        next = par->next_trans;
        sui_transition_dec_refcnt(par);
      }
    }
    substate->trans_head = NULL;

    if (substate->dialog_head) {
      sui_ssdialog_t *par, *next = NULL;
      for(par=substate->dialog_head; par; par=next) {
        next = par->next_dlg;
        sui_ssdialog_dec_refcnt(par);
      }
    }
    substate->dialog_head = NULL;

// entry actions
  if (substate->aentry_head) {
    sui_action_t *par, *next = NULL;
    for(par=substate->aentry_head; par; par=next) {
      next = par->next_action;
      sui_action_dec_refcnt(par);
    }
  }
  substate->aentry_head = NULL;
// at_first actions
  if (substate->aatfirst_head) {
    sui_action_t *par, *next = NULL;
    for(par=substate->aatfirst_head; par; par=next) {
      next = par->next_action;
      sui_action_dec_refcnt(par);
    }
  }
  substate->aatfirst_head = NULL;
/*
// loop actions
    if (substate->aloop_head) {
      sui_action_t *par, *next = NULL;
      for(par=substate->aloop_head; par; par=next) {
        next = par->next_action;
        sui_action_dec_refcnt(par);
      }
    }
    substate->aloop_head = NULL;
*/
// exit actions
    if (substate->aexit_head) {
      sui_action_t *par, *next = NULL;
      for(par=substate->aexit_head; par; par=next) {
        next = par->next_action;
        sui_action_dec_refcnt(par);
      }
    }
    substate->aexit_head = NULL;
    sui_obj_done_vmt((sui_obj_t *)substate, UL_CAST_UNQ1(sui_obj_vmt_t *, &sui_substate_vmt_data));
    free(substate);
  }
  return ref;
}

SUI_TYPE_DECLARE_REFCNT(substate, SUI_TYPE_SUBSTATE, struct sui_substate, NULL,
                         sui_substate_inc_refcnt, sui_substate_dec_refcnt)

/**
 * sui_substate_add_action - Add an action to a substate
 * @substate: Pointer to a substate structure.
 * @add: Pointer to an action for adding.
 * @type: Type of an action for adding (entry,loop,exit) [SUI_SSACT_].
 *
 * The function adds an action to one of the substate action lists. The substate list
 * is selected by @type argument.
 *
 * Return Value: The function returns zero as success and a negative value if any error occurs.
 *
 * File: sui_environment.c
 */
int sui_substate_add_action(sui_substate_t *substate, sui_action_t *add, int type)
{
  switch (type) {
    case SUI_SSACT_ENTRY: return sui_action_add_next(&(substate->aentry_head), add);
    case SUI_SSACT_ATFIRST: return sui_action_add_next(&(substate->aatfirst_head), add);
//  case SUI_SSACT_LOOP: return sui_action_add_next(&(substate->aloop_head), add);
    case SUI_SSACT_EXIT: return sui_action_add_next(&(substate->aexit_head), add);
  }
  return -1;
}

/**
 * sui_substate_process_actions - Process all actions of a substate with the specified type
 * @appl: Pointer to an application structure.
 * @substate: Pointer to a substate structure.
 * @type: Type of a substate action for processing (entry,atfirst,loop,exit) [SUI_SSACT_].
 *
 * The function tries process all actions of the specified type under the substate.
 *
 * Return Value: The function returns zero as success and a negative value if any error occurs.
 *
 * File: sui_environment.c
 */
int sui_substate_process_actions(sui_application_t *appl, sui_substate_t *substate, int type)
{
  sui_action_t *act;
  int i=0;
  if (!substate) return -1;
  switch (type) {
    case SUI_SSACT_ENTRY: act = substate->aentry_head; break;
    case SUI_SSACT_ATFIRST: act = substate->aatfirst_head; break;
//  case SUI_SSACT_LOOP:  act = substate->aloop_head; break;
    case SUI_SSACT_EXIT:  act = substate->aexit_head; break;
    default: return -1;
  }
  for(; act; act = act->next_action) {
    i++;
    if (sui_action_process(appl, act) < 0)
      ul_logerr("SUBS_ACT: !!! Action(#%d)(t=%d)(under '%s'/'%s') wasn't successful !!!\n",
                i, type,
                sui_utf8_get_text(appl->cstate->name),
                sui_utf8_get_text(appl->csubstate->name));
  }
  return 0;
}

/******************************************************************************
 * StateExtension
 ******************************************************************************/
GAVL_CUST_NODE_INT_IMP(sui_ste, struct sui_stateext, sui_substate_t, utf8 *,
                        substate_root, substate_node, name, sui_name_cmp)

/**
 * sui_stateext_inc_refcnt - Increment state extension reference counter
 * @stext: Pointer to a state extension structure.
 *
 * The function increments state extension structure reference counter.
 *
 * Return Value: The function returns value of the state-extension's reference counter after incrementing.
 *
 * File: sui_environment.c
 */
sui_refcnt_t sui_stateext_inc_refcnt(sui_stateext_t *stext)
{
  if (!stext) return SUI_REFCNTERR;
  if (stext->refcnt >= 0) stext->refcnt++;
  return stext->refcnt;
}

/**
 * sui_stateext_dec_refcnt - Decrement state extension reference counter
 * @stext: Pointer to a state extension structure.
 *
 * The function decrements the state extension structure reference counter.
 * If the counter is decremented to zero state extension structure is deleted.
 *
 * Return Value: The function returns value of the state-extension's reference counter after decrementing.
 *
 * File: sui_environment.c
 */
sui_refcnt_t sui_stateext_dec_refcnt(sui_stateext_t *stext)
{
  sui_refcnt_t ref;
  if (!stext) return SUI_REFCNTERR;
  if (stext->refcnt > 0) stext->refcnt--;
  if (!(ref = stext->refcnt)) {
    sui_substate_t *subst;

    if(stext->widget_root) sui_utf8_dec_refcnt(stext->widget_root);
    stext->widget_root = NULL;
    if(stext->init_substate) sui_utf8_dec_refcnt(stext->init_substate);
    stext->init_substate = NULL;

    if (stext->trans_head) {
      sui_transition_t *par, *next = NULL;
      for(par=stext->trans_head; par; par=next) {
        next = par->next_trans;
        sui_transition_dec_refcnt(par);
      }
    }
    stext->trans_head = NULL;

    while((subst=sui_ste_cut_first(stext))) {
      sui_substate_dec_refcnt(subst);
    }
    sui_obj_done_vmt((sui_obj_t *)stext, UL_CAST_UNQ1(sui_obj_vmt_t *, &sui_stateext_vmt_data));
    free(stext);
  }
  return ref;
}

extern SUI_CANBE_CONST_VMT sui_obj_vmt_t sui_stateext_vmt_data;

SUI_TYPE_DECLARE_REFCNT(stateext, SUI_TYPE_STATEEXT, struct sui_stateext, NULL,
                         sui_stateext_inc_refcnt, sui_stateext_dec_refcnt)


/**
 * sui_stateext_find - Find state extension in the namespace according to its name
 * @state: Pointer to a state name.
 *
 * The function tries to find state extension structure in the current namespace according to its name.
 *
 * Return Value: The function returns pointer to the found state extension structure or NULL.
 *
 * File: sui_environment.c
 */
sui_stateext_t *sui_stateext_find(utf8 *state)
{
  if (!state) return NULL;
  ns_object_t *obj;
  obj = ns_find_any_object_by_type_and_name(SUI_TYPE_STATEEXT, sui_utf8_get_text(state));
  if (obj) return obj->ptr;
  return NULL;
}

/**
 * sui_stateext_find_substate - Find substate in the state extension according substate name
 * @ste: Pointer to a state extension structure.
 * @subst: Pointer to a substate UTF8 name.
 *
 * The function tries to find substate in the specified state extension according to substate's name.
 *
 * Return Value: The function returns pointer to the found substate structure or NULL.
 *
 * File: sui_environment.c
 */
sui_substate_t *sui_stateext_find_substate(sui_stateext_t *ste, utf8 *subst)
{
  if (!ste || !subst) return NULL;
  return sui_ste_find(ste, (utf8 **) &subst);
}

/******************************************************************************
 * State
 ******************************************************************************/
/**
 * sui_state_inc_refcnt - Increment state reference counter
 * @state: Pointer to a state structure.
 *
 * The function increments state structure reference counter.
 *
 * Return Value: The function returns value of the state's reference counter after incrementing.
 *
 * File: sui_environment.c
 */
sui_refcnt_t sui_state_inc_refcnt(sui_state_t *state)
{
  if (!state) return SUI_REFCNTERR;
  if (state->refcnt >= 0) state->refcnt++;
  return state->refcnt;
}

/**
 * sui_state_dec_refcnt - Decrement state reference counter
 * @state: Pointer to a state structure.
 *
 * The function decrements the state structure reference counter.
 * If the counter is decremented to zero state structure is deleted.
 *
 * Return Value: The function returns value of the state's reference counter after decrementing.
 *
 * File: sui_environment.c
 */
sui_refcnt_t sui_state_dec_refcnt(sui_state_t *state)
{
  sui_refcnt_t ref;
  if (!state) return SUI_REFCNTERR;
  if (state->refcnt > 0) state->refcnt--;
  if (!(ref = state->refcnt)) {
    if (state->name) sui_utf8_dec_refcnt(state->name);
    state->name = NULL;
    if(state->nsname) sui_utf8_dec_refcnt(state->nsname);
    state->nsname = NULL;
    if (state->state_ext) sui_stateext_dec_refcnt(state->state_ext);
    state->state_ext = NULL;
    sui_obj_done_vmt((sui_obj_t *)state, UL_CAST_UNQ1(sui_obj_vmt_t *, &sui_state_vmt_data));
    free(state);
  }
  return ref;
}

extern SUI_CANBE_CONST_VMT sui_obj_vmt_t sui_state_vmt_data;

SUI_TYPE_DECLARE_REFCNT(state, SUI_TYPE_STATE, struct sui_state, NULL,
                         sui_state_inc_refcnt, sui_state_dec_refcnt)

/**
 * sui_state_set_extension - Set a state extension to a state
 * @state: Pointer to a state structure.
 * @steext: Pointer to a state extension structure.
 *
 * The function sets the state extension to the state.
 *
 * Return Value: The function returns zero as success and a negative value if any error occurs.
 *
 * File: sui_environment.c
 */
int sui_state_set_extension(sui_state_t *state, sui_stateext_t *steext)
{
  if (!state || !steext) return -1;
  if (state->state_ext) sui_stateext_dec_refcnt(state->state_ext);
  sui_stateext_inc_refcnt(steext);
  state->state_ext = steext;
  return 0;
}

/******************************************************************************
 * Scenario
 ******************************************************************************/
GAVL_CUST_NODE_INT_IMP(sui_sco_state, sui_scenario_t, sui_state_t, utf8 *,
                        state_root, state_node, name, sui_name_cmp)

/**
 * sui_scenario_inc_refcnt - Increment scenario reference counter
 * @sco: Pointer to a scenario structure.
 *
 * The function increments scenario structure reference counter.
 *
 * Return Value: The function returns value of the scenario's reference counter after incrementing.
 *
 * File: sui_environment.c
 */
sui_refcnt_t sui_scenario_inc_refcnt(sui_scenario_t *sco)
{
  if (!sco) return SUI_REFCNTERR;
  if (sco->refcnt >= 0) sco->refcnt++;
  return sco->refcnt;
}

/**
 * sui_scenario_dec_refcnt - Decrement scenario reference counter
 * @sco: Pointer to a scenario structure.
 *
 * The function decrements the scenario structure reference counter.
 * If the counter is decremented to zero scenario structure is deleted.
 *
 * Return Value: The function returns value of the scenario's reference counter after decrementing.
 *
 * File: sui_environment.c
 */
sui_refcnt_t sui_scenario_dec_refcnt(sui_scenario_t *sco)
{
  sui_refcnt_t ref;
  if (!sco) return SUI_REFCNTERR;
  if (sco->refcnt > 0) sco->refcnt--;
  if (!(ref = sco->refcnt)) {
    sui_state_t *state;
//    if (sco->screen) sui_screen_dec_refcnt(sco->screen);
    if (sco->init) sui_utf8_dec_refcnt(sco->init);
    sco->init = NULL;

    while((state=sui_sco_state_cut_first(sco))) {
      sui_state_dec_refcnt(state);
    }

    if (sco->trans_head) {
      sui_transition_t *par, *next = NULL;
      for(par=sco->trans_head; par; par=next) {
        next = par->next_trans;
        sui_transition_dec_refcnt(par);
      }
    }
    sco->trans_head = NULL;
    sui_obj_done_vmt((sui_obj_t *)sco, UL_CAST_UNQ1(sui_obj_vmt_t *,
                     UL_CAST_UNQ1(sui_obj_vmt_t *, &sui_scenario_vmt_data)));
    free(sco);
  }
  return ref;
}

SUI_TYPE_DECLARE_REFCNT(scenario, SUI_TYPE_SCENARIO, struct sui_scenario, NULL,
                         sui_scenario_inc_refcnt, sui_scenario_dec_refcnt)


/**
 * sui_scenario_find_state - Find a state in a scenario according to state's name
 * @sco: Pointer to a scenario.
 * @state: UTF8 State's name.
 *
 * The function tries to find a state under the scenario according to state's name.
 *
 * Return Value: The function returns pointer to the found state structure or NULL.
 *
 * File: sui_environment.c
 */
sui_state_t *sui_scenario_find_state(sui_scenario_t *sco, utf8 *state)
{
  if (!sco || !state) return NULL;
  return sui_sco_state_find(sco, (utf8 **) &state);
}

/******************************************************************************
 * Application
 ******************************************************************************/

/******************************************************************************/
/* application slot functions */
/**
 * sui_application_show_tip - Show a widget's tip in root group's status widget
 * @appl: Pointer to an application.
 * @wdg: Pointer to a widget.
 *
 * The slot function shows widget's tooltip in the application widget root's status.
 *
 * Return Value: The slot function returns zero as success and -1 as an error.
 *
 * File: sui_environment.c
 */
int sui_application_show_tip(sui_application_t *appl, struct sui_widget *wdg)
{
  if (appl && wdg) {
//    ul_logdeb("APPL: show_tip('%s')\n", sui_utf8_get_text(wdg->tooltip));
    return sui_group_status_message(appl->wdgroot, wdg->tooltip);
  }
  return 0;
}

/**
 * sui_application_hide_tip - Hide a widget's tip in root group's status widget
 * @appl: Pointer to an application.
 * @wdg: Pointer to a widget.
 *
 * The slot function hides widget's tooltip.
 *
 * Return Value: The slot function returns zero as success and -1 as an error.
 *
 * File: sui_environment.c
 */
int sui_application_hide_tip(sui_application_t *appl, struct sui_widget *wdg)
{
  if (appl && wdg) {
//    ul_logdeb("APPL: hide_tip\n");
    return sui_group_status_clear(appl->wdgroot);
  }
  return 0;
}

/******************************************************************************/
/* Basic application vmt */
SUI_CANBE_CONST
sui_obj_signal_tinfo_t sui_application_signal_tinfo[] = { 
/*
	{ "change_screen", SUI_SIG_CHANGESCREEN, &sui_args_tinfo_utf8},
*/
};

SUI_CANBE_CONST
sui_obj_slot_tinfo_t sui_application_slot_tinfo[] = { 
  { "show_tip", SUI_SLOT_APP_SHOW_TIP, &sui_args_tinfo_widget,
    .target_kind=SUI_STGT_METHOD_OFFSET,
    .target.method_offset=UL_OFFSETOF(sui_application_vmt_t, show_tip)},
  { "hide_tip", SUI_SLOT_APP_HIDE_TIP, &sui_args_tinfo_widget,
    .target_kind=SUI_STGT_METHOD_OFFSET,
    .target.method_offset=UL_OFFSETOF(sui_application_vmt_t, hide_tip)},
};

SUI_CANBE_CONST
sui_obj_tinfo_t sui_application_vmt_obj_tinfo = {
  .name = "sui_application",
  .obj_size = sizeof(sui_application_t),
  .obj_align = UL_ALIGNOF(sui_application_t),
  .signal_count = MY_ARRAY_SIZE(sui_application_signal_tinfo),
  .signal_tinfo = UL_CAST_UNQ1(sui_obj_signal_tinfo_t *, sui_application_signal_tinfo),
  .slot_count = MY_ARRAY_SIZE(sui_application_slot_tinfo),
  .slot_tinfo = UL_CAST_UNQ1(sui_obj_slot_tinfo_t *, sui_application_slot_tinfo)
};


/**
 * sui_application_vmt_init - Initiate application structure
 * @pw: Pointer to an application structure.
 * @params: Initial function's input arguments.
 *
 * The function initiates application structure and fills its structure fields.
 *
 * Return Value: The function return 0 as success and -1 as an error.
 *
 * File: sui_environment.c
 */
int sui_application_vmt_init(sui_application_t *appl, void *params)
{
  appl->focus_mask = &sui_widget_current_focus_flags; /* FIXME: for more applications it must be solved in another way */
  *appl->focus_mask = SUFL_FOCUSED | SUFL_HIGHLIGHTED;
  appl->blink_mask = &sui_widget_current_blink_flags; /* FIXME: for more applications it must be solved in another way */
  *appl->blink_mask = SUFL_HIGHLIGHTED;
  appl->refcnt = 1;
  appl->appl_time = get_current_time();
  appl->blink_time = appl->appl_time + SUI_GLOBAL_BLINK_TIME;
  return 0;
}

/**
 * sui_application_vmt_done - Clear application structure
 * @pw: Pointer to an application.
 *
 * The function releases each allocated memory for the application structure field.
 *
 * Return Value: The function does not return any value.
 *
 * File: sui_environment.c
 */
void sui_application_vmt_done(sui_application_t *appl)
{
  if (appl->base_path) sui_utf8_dec_refcnt(appl->base_path);
  appl->base_path = NULL;

  if (appl->cscenario) sui_scenario_dec_refcnt(appl->cscenario);
  appl->cscenario = NULL;

  if (appl->cstate) sui_state_dec_refcnt(appl->cstate);
  appl->cstate = NULL;
  if (appl->laststate) sui_utf8_dec_refcnt(appl->laststate);
  appl->laststate = NULL;

  if (appl->csubstate) sui_substate_dec_refcnt(appl->csubstate);
  appl->csubstate = NULL;
  if (appl->lastsubstate) sui_utf8_dec_refcnt(appl->lastsubstate);
  appl->lastsubstate = NULL;

  if (appl->mdialog) sui_ssdialog_dec_refcnt(appl->mdialog);
  appl->mdialog = NULL;
  if (appl->wdgroot) sui_dec_refcnt(appl->wdgroot);
  appl->wdgroot = NULL;
  if (appl->focused) sui_dec_refcnt(appl->focused);
  appl->focused = NULL;
}

/**
 * sui_appl_inc_refcnt - Increment application reference counter
 * @appl: Pointer to an application structure.
 *
 * The function increments application structure reference counter.
 *
 * Return Value: The function returns value of the application's reference counter after incrementing.
 *
 * File: sui_environment.c
 */
sui_refcnt_t sui_appl_inc_refcnt(sui_application_t *appl)
{
  if (!appl) return SUI_REFCNTERR;
  if(appl->refcnt>=0) appl->refcnt++;
  return appl->refcnt;
}

/**
 * sui_appl_dec_refcnt - Decrement application reference counter
 * @appl: Pointer to a application structure.
 *
 * The function decrements the application structure reference counter.
 * If the counter is decremented to zero application structure is deleted.
 *
 * Return Value: The function returns value of the application's reference counter after decrementing.
 *
 * File: sui_environment.c
 */
sui_refcnt_t sui_appl_dec_refcnt(sui_application_t *appl)
{
  sui_refcnt_t ref;
  if (!appl) return SUI_REFCNTERR;
  if(appl->refcnt>0) appl->refcnt--;
  if(!(ref=appl->refcnt)) {
//ul_logdeb("application done\n");
    sui_obj_done((sui_obj_t *) appl);
    free(appl);
  }
  return ref;
}

sui_application_vmt_t sui_application_vmt_data = {
  .vmt_size = sizeof(sui_application_vmt_t),
  .vmt_refcnt = SUI_STATIC,
  .vmt_parent = &sui_obj_vmt_data,
  .vmt_init   = sui_application_vmt_init,
  .vmt_done   = sui_application_vmt_done,
  .vmt_obj_tinfo = UL_CAST_UNQ1(sui_obj_tinfo_t *, &sui_application_vmt_obj_tinfo),

  .inc_refcnt = sui_appl_inc_refcnt,
  .dec_refcnt = sui_appl_dec_refcnt,

  .show_tip = sui_application_show_tip,
  .hide_tip = sui_application_hide_tip,
};

/**
 * sui_appl_check_condition - Check action/transition/substate dialog condition
 * @appl: Pointer to an application.
 * @cond: Pointer to a condition for testing.
 * @event: Pointer to an event.
 *
 * The function tests one condition and returns zero if the condition is fulfilled.
 *
 * Return Value: The function returns zero if the condition is fulfilled or -1 if it isn't.
 *
 * File: sui_environment.c
 */
int sui_appl_check_condition(sui_application_t *appl, sui_condition_t *cond, sui_event_t *event)
{
  int ret;
  if (!cond || cond->conflags == SUI_COND_FLG_UNCOND) return 0;
/* state time */
  if (cond->conflags & SUI_COND_FLG_TIME) {
    if ((long)(appl->cur_time-appl->state_time) < cond->state_time)
      return -1;
  }

/* state time */
  if (cond->conflags & SUI_COND_FLG_USERTIME) {
    if ((long)(appl->cur_time-appl->activity_time) < cond->user_time)
      return -1;
  }

  if (cond->conflags & SUI_COND_FLG_EVENT) {
    if (!event)
      return -1;

    if (event->what != cond->event)
      return -1;

    if (cond->conflags & SUI_COND_FLG_EVKEY) {
      if ((event->what != SUEV_KDOWN) ||
          (event->keydown.keycode != (unsigned short)cond->evkey))
        return -1;
    }
    if (cond->conflags & SUI_COND_FLG_EVCMD) {
      if ((event->what != SUEV_COMMAND) ||
          (event->message.command != (unsigned short)cond->evcmd))
        return -1;
    }

    if (cond->conflags & SUI_COND_FLG_EVINFO) {
      if ((event->what == SUEV_KDOWN) || (event->what == SUEV_KUP)) {
        if ((short)cond->evinfo != event->keydown.repeats) //event->keydown.modifiers)
          return -1;
      } else if ((event->what == SUEV_COMMAND) || (event->what == SUEV_BROADCAST)) {
        if (cond->evinfo != event->message.info)
          return -1;
      } else
        return -1;
    }
  }

  if (cond->conflags & SUI_COND_FLG_LASTST) {
    ret = sui_utf8_cmp(appl->laststate, cond->lastst);
    if (ret && !(cond->negflags & SUI_COND_FLG_LASTST))
      return -1;
    if (!ret && (cond->negflags & SUI_COND_FLG_LASTST))
      return -1;
  }
  if (cond->conflags & SUI_COND_FLG_LASTSST) {
    ret = sui_utf8_cmp(appl->lastsubstate, cond->lastsubst);
    if (ret && !(cond->negflags & SUI_COND_FLG_LASTSST))
      return -1;
    if (!ret && (cond->negflags & SUI_COND_FLG_LASTSST))
      return -1;
  }

  if (cond->conflags & SUI_COND_FLG_TERM) {
    sui_term_value_t res;
    sui_term_value_init(&res);
    if (cond->term && sui_term_compute_term(cond->term, &res) == SUI_RET_OK) {
      if ((res.type==SUI_TERMVAL_LONG && res.value.slong==0) ||
          (res.type==SUI_TERMVAL_ULONG && res.value.ulong==0) ||
          (res.type==SUI_TERMVAL_UTF8 && sui_utf8_length(res.value.u8)==0)) {
        sui_term_value_clear(&res);
        return -1;
      }
    }
    sui_term_value_clear(&res);
  }

  if (cond->conflags & SUI_COND_FLG_FOCUS) {
    if (cond->focus) {
      if ((cond->negflags & SUI_COND_FLG_FOCUS) && (appl->focused==cond->focus))
        return -1;
      if (!(cond->negflags & SUI_COND_FLG_FOCUS) && (appl->focused!=cond->focus))
        return -1;
    }
  }
  return 0;
}

/**
 * sui_appl_check_transitions - Check all the current state's transitions
 * @appl: Pointer to an application.
 * @event: Pointer to an event.
 *
 * The function tests all transitions stepwise in the current state until all one of them is fulfilled.
 *
 * Return Value: The function returns pointer to the transition which can be fired or NULL.
 *
 * File: sui_environment.c
 */
sui_transition_t *sui_appl_check_transitions(sui_application_t *appl, sui_event_t *event)
{
  sui_transition_t *trans;

  if (!appl) return NULL;
/* the check global transitions */
  if (!appl->cstate) return NULL;
  if (appl->cstate->state_ext) {
    trans = appl->cstate->state_ext->trans_head;
    while(trans) {
      if (!sui_appl_check_condition(appl, trans->cond, event)) { // !(trans->conflags & SUI_TRANS_FLG_SKIP) &&
        ul_logdeb("APPL: State Transition fired\n");
        return trans;
      }
      trans = trans->next_trans;
    }
  }
/* then check local transitions */
  if (appl->csubstate) {
    trans = appl->csubstate->trans_head;
    while(trans) {
      if (!sui_appl_check_condition(appl, trans->cond, event)) {
        ul_logdeb("APPL: Substate Transition fired\n");
        return trans;
      }
      trans = trans->next_trans;
    }
  }
  return NULL;
}

/**
 * sui_appl_check_ssdialog - Check all the current substate transitions
 * @appl: Pointer to an application.
 * @event: Pointer to an event.
 * @type: Type of substate dialog's condition which will be tested (ENTRY/EXIT) [SUI_SSD_COND_...].
 *
 * The function tests all substate dialogs stepwise in the current substate until all one of them is fulfilled.
 *
 * Return Value: The function returns pointer to the substate dialog which can be have fulfilled all conditions or NULL.
 *
 * File: sui_environment.c
 */
sui_ssdialog_t *sui_appl_check_ssdialog(sui_application_t *appl, sui_event_t *event, int type)
{
  sui_ssdialog_t *ssd;
  if (!appl || !appl->cstate) return NULL;
  if (type==SUI_SSD_COND_ENTRY) {
    if (appl->csubstate) {
      ssd = appl->csubstate->dialog_head;
      while(ssd) {
        if (!sui_appl_check_condition(appl, ssd->cond, event)) {
          ul_logdeb("APPL: SubState Dialog condition fired\n");
          return ssd;
        }
        ssd = ssd->next_dlg;
      }
    }
  } else {
    if (appl->mdialog && appl->mdialog->exitcond) {
      if (!sui_appl_check_condition(appl, appl->mdialog->exitcond, event)) {
        ul_logdeb("APPL: SubState Dialog EXIT condition fired\n");
        return appl->mdialog;
      }
    }
  }
  return NULL;
}

/******************************************************************************/
/**
 * sui_appl_change_state - Change application state in the current scenario
 * @appl: Pointer to an application.
 * @trans: Pointer to a transition (field with state and substate name).
 * @chns_fcn: Pointer to a function for reloading local namespace.
 *
 * The function tries to change state/substate to a new one. If the state is changed
 * the function loads and parses SXML file with a new state. After the change, the function
 * tries to find and set focus to a widget.
 *
 * Return Value: The function returns zero as success and a negative value if any error occurs.
 *
 * File: sui_environment.c
 */
int sui_appl_change_state(sui_application_t *appl, sui_transition_t *trans,
                          sui_appl_reload_namespace_fcn *chns_fcn, void *par)
{
  int chng_state, chng_subst;
  ns_object_t *obj = NULL;

  if (!appl || !trans) return -1;
  ul_logdeb("APPL: Change application screen state to '%s'/'%s'\n",
           sui_utf8_get_text(trans->tostate),sui_utf8_get_text(trans->tosubst));

  sui_transition_inc_refcnt(trans); // keep transition alive inside this function

  chng_state = (trans->tostate != NULL);
  chng_subst = (trans->tosubst != NULL) || chng_state;

  if (chng_subst) {
    sui_appl_clear_focus(appl);
// /*    if (appl->focused) {
//       sui_dec_refcnt(appl->focused); appl->focused = NULL;
//     }*/
  }
/* substate changing - process EXIT actions */
  if (chng_subst && appl->csubstate && appl->csubstate->aexit_head) {
//    ul_logdeb("- Process substate EXIT actions.\n");
    sui_substate_process_actions(appl, appl->csubstate, SUI_SSACT_EXIT);
  }
/* state changing - process EXIT state actions */
  if (chng_state && appl->proc_fcn) {
//    ul_logdeb("- Process state EXIT procedure.\n");
    appl->proc_fcn(appl, appl->cstate, SUI_STATE_FCN_EXIT);
  }

/* process transition actions (before) */
  if (trans->abefore_head) {
//    ul_logdeb("- Process transition BEFORE actions.\n");
    sui_action_process_all(appl, trans->abefore_head);
  }

/* substate is changed */
  if (chng_subst) {
    if (appl->lastsubstate) {
      sui_utf8_dec_refcnt(appl->lastsubstate);
      appl->lastsubstate = NULL;
    }
    if (appl->csubstate) {
      sui_utf8_inc_refcnt(appl->csubstate->name);
      appl->lastsubstate = appl->csubstate->name;

      sui_substate_dec_refcnt(appl->csubstate);
      appl->csubstate = NULL;
    }

    if (appl->mdialog) { /* FIXME: really destroy ??? */
      sui_ssdialog_dec_refcnt(appl->mdialog);
      appl->mdialog = NULL;
    }
  }

/* state is changed */
  if (chng_state) {
    if (appl->wdgroot) {
      sui_dec_refcnt(appl->wdgroot);
      appl->wdgroot = NULL;
    }

    appl->proc_fcn = NULL;

    if (appl->laststate) {
      sui_utf8_dec_refcnt(appl->laststate);
      appl->laststate = NULL;
    }
    if (appl->cstate) {
      sui_utf8_inc_refcnt(appl->cstate->name);
      appl->laststate = appl->cstate->name;
      /* remove state_extension to reload it from SXML in future */
      if (appl->cstate->state_ext) {
        sui_stateext_dec_refcnt(appl->cstate->state_ext);
        appl->cstate->state_ext = NULL;
      }
      sui_state_dec_refcnt(appl->cstate);
      appl->cstate = NULL;
    }
  }

/* temporary - scenario changing */
  if (trans->scenario) {
    ul_logdeb("APPL: Change application scenario to '%s'\n",
              sui_utf8_get_text(trans->scenario));
    if (sui_appl_replace_scenario(appl, trans->scenario) < 0) {
      ul_logerr("APPL: Application scenario '%s' wasn't changed!\n",
                sui_utf8_get_text(trans->scenario));
      return -1;
    }
  }

/* transition - first read and parse SXML and set new state, if new state is set */
  if (chng_state) {
    ul_logdeb("APPL: Change application state to '%s'.\n",
              sui_utf8_get_text(trans->tostate));
    sui_state_t *newstate = sui_scenario_find_state(appl->cscenario,
                                                    trans->tostate);
    if (!newstate) {
      ul_logerr("APPL: Application state '%s' wasn't found in current scenario!\n",
                sui_utf8_get_text(trans->tostate));
      return -1;
    }

    if (newstate->nsname) { /* change local namespace - external function - parsing sxml */
      int ret;
      utf8 *newfile = sui_utf8(NULL, -1, sui_utf8_bytesize(appl->base_path)+
                      sui_utf8_bytesize(newstate->nsname)+1);
      if (!chns_fcn) return -1;
      if ((sui_utf8_cat(newfile, appl->base_path) <= 0) ||
          (sui_utf8_cat(newfile, newstate->nsname) <= 0)) {
        ul_logerr("APPL: TODO!?! Cat new state namespace name!\n");
        return -1;
      }
      ret = chns_fcn(newfile, par);
      if (ret < 0) {
        ul_logerr("APPL: SXML file '%s' wasn't parsed!\n", sui_utf8_get_text(newfile));
        return -1;
      }
      sui_utf8_dec_refcnt(newfile);
    }

    sui_state_inc_refcnt(newstate);
    appl->cstate = newstate;
    appl->cstflg = SUI_APPL_FLAGS_NONE;

  /* fill state_extension if it is empty */
    if (!newstate->state_ext) {
      sui_stateext_t *ste = sui_stateext_find(newstate->name);
      if (!ste) {
        ul_logerr("APPL: State extension wasn't found and set!\n");
        return -1;
      }
      sui_state_set_extension(newstate, ste);
    }
  /* set widget root */
    if (!newstate->state_ext->widget_root) {
      ul_logerr("APPL: Root widget must be set in state extension !\n");
      return -1;
    }
  /* find new root widget */
    obj = ns_find_widget_in_ns(sui_utf8_get_text(newstate->state_ext->widget_root));
    if (!obj) { /* init widget doesn't exist */
      ul_logerr("APPL: Root widget (%s) isn't in namespace!\n",
                sui_utf8_get_text(newstate->state_ext->widget_root));
      return -1;
    }
    sui_inc_refcnt(obj->ptr);
    appl->wdgroot = obj->ptr;

  /* set state proc_fcn */
    if (newstate->state_ext->procs) {
      obj = ns_find_any_object_by_type_and_name(SUI_TYPE_STATEFCN,
                      sui_utf8_get_text(newstate->state_ext->procs));
      if (!obj) {
        ul_logerr("APPL: State function '%s' isn't in namespace!CONTINUE\n",
                  sui_utf8_get_text(newstate->state_ext->procs));
      } else {
        appl->proc_fcn = obj->ptr;
      }
    }

  /* state was exchanged - init state transitions */
    if (sui_transition_init_all(newstate->state_ext->trans_head) < 0) {
      ul_logerr("APPL: Initiation state transitions error!\n");
      return -1;
    }
  }

  /* set new substate, if it is needed */
  if (chng_subst) {
    sui_substate_t *newsubst = NULL;
    utf8 *substname = NULL;
  /* get new substate name */
    if (trans->tosubst)
      substname = trans->tosubst;
    else if (appl->cstate->state_ext->init_substate)
      substname = appl->cstate->state_ext->init_substate;
    else
      substname = (sui_ste_first(appl->cstate->state_ext))->name;
    if (!substname) {
      ul_logerr("APPL: Substate name wasn't determined!\n");
      return -1;
    }
  /* change substate */
    ul_logdeb("APPL: Change application substate to '%s'.\n", sui_utf8_get_text(substname));
    newsubst = sui_stateext_find_substate(appl->cstate->state_ext, substname);
    if (!newsubst) {
      ul_logerr("APPL: Substate '%s' wasn't found in current state!\n",
                sui_utf8_get_text(substname));
      return -1;
    }
    sui_substate_inc_refcnt(newsubst);
    appl->csubstate = newsubst;

  /* substate was exchanged - init substate transitions */
    if (sui_transition_init_all(newsubst->trans_head) < 0) {
      ul_logerr("APPL: Initiation substate transitions error!\n");
      return -1;
    }
    if (sui_ssdialog_init_all(newsubst->dialog_head) < 0) {
      ul_logerr("APPL: Initiation substate dialogs error!\n");
      return -1;
    }
    if (sui_action_init_all(newsubst->aentry_head)<0) {
      ul_logerr("APPL: Initiation entry actions error!\n");
      return -1;
    }
    if (sui_action_init_all(newsubst->aatfirst_head)<0) {
      ul_logerr("APPL: Initiation at first actions error!\n");
      return -1;
    }
    if (sui_action_init_all(newsubst->aexit_head)<0) {
      ul_logerr("APPL: Initiation exit actions error!\n");
      return -1;
    }
  }

/* global settings in new state/substate */
  /* set state_time */
  appl->state_time = get_current_time(); /* FIXME: for every change_state ??? */
  //ul_logdeb("set st = %u\n", appl->state_time);
  /* widgets initialization */
  if (chng_state) {
    sui_widget_init(appl->wdgroot, sui_globdc);
  }

/* process transition actions (before) */
  if (trans->aafter_head) {
//    ul_logdeb("- Process transition AFTER actions.\n");
    sui_action_process_all(appl, trans->aafter_head);
  }

/* state changing - process ENTRY state actions */
  if (chng_state && appl->proc_fcn) {
//    ul_logdeb("- Process state ENTRY procedure.\n");
    appl->proc_fcn(appl, appl->cstate, SUI_STATE_FCN_ENTRY);
  }

/* substate changing - process ENTRY actions */
  if (chng_subst && appl->csubstate && appl->csubstate->aentry_head) {
//    ul_logdeb("- Process substate ENTRY actions.\n");
    sui_substate_process_actions(appl, appl->csubstate, SUI_SSACT_ENTRY);
  }

/* find and initiate focus widget - order in searching as follows :
    1) transition->towidget
    2) newsubstate->init_focus
    3) first widget in new widget tree with SUFL_FOCUSED flag
    default - root widget
*/
  if (chng_subst) {
    sui_widget_t *foc = NULL;
    /* get focus from transition 'towidget' name */
    if (trans->towidget) {
      obj = ns_find_widget_in_ns(sui_utf8_get_text(trans->towidget));
      if (!obj) {
        ul_logerr("APPL: Widget from transition isn't in namespace (in new widget tree)!CONTINUE\n");
      } else
        foc = obj->ptr;
    }
    /* get focus from substate */
    if (!foc && appl->csubstate->init_focus) {
      obj = ns_find_widget_in_ns(sui_utf8_get_text(appl->csubstate->init_focus));
      if (!obj) {
        ul_logerr("APPL: Subspace initial focus widget isn't in namespace!CONTINUE\n");
      } else {
        foc = obj->ptr;
      }
    }
    /* get focus from widget root - SUFL_FOCUSED */
    if (!foc) {
      foc = sui_appl_init_focus(appl->wdgroot);
      ul_logdeb("APPL: Focus will be set to widget (%p) by SUFL_FOCUSED flag.\n", foc);
    }
    /* set focus to NULL (focus to main screen widget) */
    if (!foc) {
      sui_event_t ev;
      ev.what = SUEV_COMMAND;
      ev.message.command = SUCM_FOCUSASK;
      sui_hevent(appl->wdgroot, &ev);
      if (ev.what == SUEV_NOTHING)
        foc = appl->wdgroot;
      else
        ul_loginf("APPL: Focus isn't set to anything!CONTINUE\n");
    } else {
      sui_clear_flag(foc, SUFL_FOCUSED); /* FIXME: another flag for indicating FOCUS in SXML file ??? */
    }
  /* set focus */
    if (foc) {
//      ul_logdeb("Set initial focus to widget [%p]\n", foc);
      sui_appl_set_focus(appl, foc);
    }
  }

/* state changing - process FIRST state actions */
  if (chng_state && appl->proc_fcn) { // screen state entry function
//    ul_logdeb("- Process state FIRST procedure.\n");
    appl->proc_fcn(appl, appl->cstate, SUI_STATE_FCN_FIRST_PROC);
  }
/* substate changing - process ATFIRST substate actions */
  if (chng_subst && appl->csubstate && appl->csubstate->aatfirst_head) {
//    ul_logdeb("- Process substate ATFIRST actions.\n");
    sui_substate_process_actions(appl, appl->csubstate, SUI_SSACT_ATFIRST);
  }

/* release transition */
  sui_transition_dec_refcnt(trans);
  return 0;
}

/******************************************************************************/
/**
 * sui_appl_replace_scenario - Replace application scenario
 * @appl: Pointer to an application.
 * @sco: UTF8 name of a new scenario.
 *
 * The function tries to find a new scenario in the namespace according to its name.
 * If the scenario is found, the function replaces the current scenario to the found one.
 *
 * Return Value: The function returns zero as success and a negative value if any error occurs.
 *
 * File: sui_environment.c
 */
int sui_appl_replace_scenario(sui_application_t *appl, utf8 *sco)
{
  ns_object_t *scoobj;
  sui_scenario_t *scenario;

  if (!appl || !sco) return -1;

  scoobj = ns_find_any_object_by_type_and_name(SUI_TYPE_SCENARIO, sui_utf8_get_text(sco));
  if (!scoobj) {
          ul_logerr("APPL: Scenario '%s' isn't in namespace\n", sui_utf8_get_text(sco));
          return -1;
  }
  scenario=scoobj->ptr;
  /*FIXME:/CHECKME: This is not problem for now, 
    actual scenarios are defined in the global namespace*/
  sui_scenario_inc_refcnt(scenario);

  if (appl->cscenario) sui_scenario_dec_refcnt(appl->cscenario);
  appl->cscenario = scenario;
/* scenario was exchanged - init scenario transitions */
  if (sui_transition_init_all(appl->cscenario->trans_head) < 0) {
    ul_logerr("APPL: Initiation scenario transitions error!\n");
    return -1;
  }
  return 0;
}

/**
 * sui_appl_change_scenario - Change scenario and change scenario state to INIT state
 * @appl: Pointer to an application.
 * @name: UTF8 name of a new scenario.
 *
 * The function tries to change scenario and then it changes scenario state to the init state.
 *
 * Return Value: The function returns zero as success and a negative value if any error occurs.
 *
 * File: sui_environment.c
 */
int sui_appl_change_scenario(sui_application_t *appl, const utf8 *name, void *par)
{
  sui_transition_t trans;

//  ul_logdeb("Change scenario (from program) to '%s'\n", sui_utf8_get_text(U8(name)));
  if (sui_appl_replace_scenario(appl, U8(name)) < 0) {
    ul_logerr("APPL: New scenario wasn't set\n");
    return -1;
  }
  memset(&trans, 0, sizeof(sui_transition_t));
  trans.refcnt = -1;
  // initiate appliciation with init state
  trans.tostate = appl->cscenario->init;
  if (sui_appl_change_state(appl, &trans, sui_change_local_namespace_from_sxml, par)) {
    ul_logerr("APPL: Initial state wasn't set\n");
    return -1;
  }
  return 0;
}

/******************************************************************************/
/* application focus functions */
/**
 * sui_appl_init_focus - Find first focused (non initialized) widget in the widget tree
 * @root: Pointer to an application widget root.
 *
 * The function tries to find the first widget which can be focused and has got flag SUFL_FOCUSED.
 *
 * Return Value: The function returns pointer to the found widget or NULL.
 *
 * File: sui_environment.c
 */

struct sui_widget *sui_appl_init_focus(struct sui_widget *root)
{
  sui_widget_t *wdg = sui_group_find_next(root, NULL, 0, 1);
//  ul_logdeb("Initiate focus into %p '%s'\n", wdg, wdg ? sui_utf8_get_text(wdg->label) : "-");
  return wdg;
}

/**
 * sui_appl_set_focus - Set focus widget under application
 * @appl: Pointer to an application.
 * @wdg: Pointer to a widget for focusing.
 *
 * The function sets focus from appliaction widget root to the widget and the application focus to the widget.
 *
 * Return Value: The function returns zero as success and a negative value if any error occurs.
 *
 * File: sui_environment.c
 */
int sui_appl_set_focus(sui_application_t *appl, sui_widget_t *wdg)
{
//  int ret = sui_group_only_set_focus(wdg, -1);
  int ret = sui_group_change_focus(appl->wdgroot, appl->focused, wdg);
  if (!ret) {
    if (appl->focused) sui_dec_refcnt(appl->focused);
    sui_inc_refcnt(wdg);
    appl->focused = wdg;
  } else
    ul_logerr("APPL: Focus to widget %p wasn't set!\n", wdg);
  return ret;
}

/**
 * sui_appl_clear_focus - Clear focus from the focused application widget.
 * @appl: Pointer to an application.
 *
 * The function clear focus which is focused under application.
 *
 * Return Value: The function returns zero as success and a negative value if any error occurs.
 *
 * File: sui_environment.c
 */
int sui_appl_clear_focus(sui_application_t *appl)
{
  int ret = -1;
  if (appl->focused) {
    ret = sui_group_only_clear_focus(appl->focused, -1);
    if (!ret) {
      sui_dec_refcnt(appl->focused);
      appl->focused = NULL;
    }
  }
  return ret;
}

/**
 * sui_appl_change_focus - Change application focus to the next/previous widget in the application's widget tree
 * @appl: Pointer to an application.
 * @dir: Direction of changing - 1=next/ -1=previous.
 *
 * The function tries to find next/previous widget for focusing from the currently focused widget and
 * if some widget is found, the application focus is changed to it.
 *
 * Return Value: The function returns zero as success and a negative value if any error occurs.
 *
 * File: sui_environment.c
 */
int sui_appl_change_focus(sui_application_t *appl, int dir)
{
  int ret = -1;
  sui_widget_t *wdg = NULL, *from = sui_group_find_focused_widget(appl->wdgroot, NULL);
// ul_logdeb(" -- We are searching from %p '%s' (todir=%d)\n", from, from ? sui_utf8_get_text(from->label) : "-", dir);
  wdg = sui_group_find_next(appl->wdgroot, from, (dir<0) ? 1: 0, 0);
  if (wdg != from) {
// ul_logdeb("*** Change focus to : %p '%s'\n", wdg, wdg ? sui_utf8_get_text(wdg->label) : "-");
    ret = sui_group_change_focus(appl->wdgroot, NULL, wdg);
    if (!ret) {
      if (appl->focused) sui_dec_refcnt(appl->focused);
      if (wdg) sui_inc_refcnt(wdg);
      appl->focused = wdg;
    }
  } else
    ret = -1;
  return ret;
}

/******************************************************************************/
/**
 * sui_appl_open_dialog - Open application substate dialog
 * @appl: Pointer to an application.
 * @ssd: Pointer to a substate dialog.
 *
 * The function adds modal widget to the application's root widget and sets the substate dialog
 * to the application.
 *
 * Return Value: The function returns zero as success and a negative value if any error occurs.
 *
 * File: sui_environment.c
 */
int sui_appl_open_dialog(sui_application_t *appl, sui_ssdialog_t *ssd)
{
//  sui_event_t ev;
  sui_widget_t *foc = NULL;

  if (!appl || !ssd) return -1;
  if (appl->mdialog || !ssd->dialog) return -1; /* some dialog is already opened */

  sui_flush_event();
  sui_flush_global_event();

/* process dialog actions (before) */
  if (ssd->abefore_head) {
//    ul_logdeb("- Process dialog BEFORE actions.\n");
    sui_action_process_all(appl, ssd->abefore_head);
  }

  sui_ssdialog_inc_refcnt(ssd);
  appl->mdialog = ssd;

  if (sui_group_modal_open(appl->wdgroot, ssd->dialog))
    ul_logerr("DIALOG: Modal dialog wasn't added into WdgRoot\n");

  foc = sui_group_find_next(ssd->dialog, NULL, 0, 0); // appl->wdgroot
// ul_logdeb("First focusable widget under modal : %p '%s'\n", foc, foc ? sui_utf8_get_text(foc->label) : "-");
  sui_group_change_focus(ssd->dialog, NULL, foc);

  return 0;
}

/**
 * sui_appl_close_dialog - Close the current application's substate dialog
 * @appl: Pointer to an application.
 * @retcode: Dialog's return code.
 *
 * The function removes modal widget from the application's root widget and removes the substate dialog
 * from the application. The modal widget is closes with $retcode code.
 *
 * Return Value: The function returns zero as success and a negative value if any error occurs.
 *
 * File: sui_environment.c
 */
int sui_appl_close_dialog(sui_application_t *appl, long retcode)
{
//  sui_event_t ev;
  if (!appl || !appl->mdialog) return -1;

  sui_flush_event();
  sui_flush_global_event();

/* process dialog actions (after) */
  if (appl->mdialog->aafter_head) {
//    ul_logdeb("- Process dialog AFTER actions.\n");
    sui_action_process_all(appl, appl->mdialog->aafter_head);
  }
  sui_ssdialog_dec_refcnt(appl->mdialog);
  appl->mdialog = NULL;

  if (sui_group_modal_close(appl->wdgroot, retcode))
    ul_logerr("DIALOG: Modal dialog wasn't removed\n");
  return 0;
}


/******************************************************************************
 * Environment
 ******************************************************************************/
/**
 *  sui_environment_open - Open all input/output evironment's devices
 * @params: Pointer to opening parameters
 *
 * The function creates and fills environment structure. It opens all necessary input/output devices
 * and sets list of environment's fonts.
 *
 * Return Value: The function returns pointer to the created environment or NULL.
 *
 * File: sui_environment.c
 */
sui_environment_t *sui_environment_open(sui_environment_open_params_t *params)
{
  sui_environment_open_params_t defparams = {0, sui_fonts};

  sui_environment_t *env = sui_malloc(sizeof(sui_environment_t));
  if (!env) return NULL;

  if (!params) params= &defparams;

  do {
    env->sui_global_dc.psd = GdOpenScreen();
    if(env->sui_global_dc.psd == NULL)
      break;

    /* set new built-in fonts if they are set */
    if (params->new_fonts) {
      env->built_in_fonts = params->new_fonts;
      sui_env_set_new_fonts(params->new_fonts, env->sui_global_dc.psd);
    }

    /* get global structures */
    env->sui_global_evar = &sui_globev;
    /* set file-handles to values marking them as non-existent source */
    env->sui_global_evar->sui_kbd_fd=-2;
    env->sui_global_evar->sui_mouse_fd=-2;

    /* initiate mouse and keyboard */
    if ((params->flags & SUI_ENVOPEN_FL_KEYBMASK)!=SUI_ENVOPEN_FL_KEYB_OFF) {
      env->sui_global_evar->sui_kbd_fd = GdOpenKeyboard();
      if (env->sui_global_evar->sui_kbd_fd == -1) {
        if ((params->flags & SUI_ENVOPEN_FL_KEYBMASK)==SUI_ENVOPEN_FL_KEYB_MANDATORY) {
          ul_logerr("Keyboard device open failed\n");
          break;
        }
        ul_loginf("Keyboard device open failed\n"); /* keyboard is optional */
      }
    }

    if ((params->flags & SUI_ENVOPEN_FL_MOUSEMASK)!=SUI_ENVOPEN_FL_MOUSE_OFF) {
      env->sui_global_evar->sui_mouse_fd = GdOpenMouse();
      if (env->sui_global_evar->sui_mouse_fd == -1) {
        if ((params->flags & SUI_ENVOPEN_FL_MOUSEMASK)==SUI_ENVOPEN_FL_MOUSE_MANDATORY) {
          ul_logerr("Mouse device open failed\n");
          break;
        }
        ul_loginf("Mouse device open failed\n"); /* mouse is opitonal */
      }
    }

    /* set global pointers */
    sui_globdc = &(env->sui_global_dc);

    return env;
  } while(0);

  if (env->sui_global_dc.psd)
      GdCloseScreen(env->sui_global_dc.psd);
  if (env->sui_global_evar && (env->sui_global_evar->sui_kbd_fd >= 0))
      GdCloseKeyboard();
  if (env->sui_global_evar && (env->sui_global_evar->sui_mouse_fd >= 0))
      GdCloseMouse();
  free(env);
  return NULL;
}

/**
 * sui_environment_close - Close and destroy environment
 * @env: Pointer to an environment.
 *
 * The function closes all environment's devices (keyboard,mouse, display) and then it releases
 * memory for the environment structure.
 *
 * Return Value: The function returns zero as success and a negative value if any error occurs.
 *
 * File: sui_environment.c
 */
int sui_environment_close(sui_environment_t *env)
{
  if (!env) return -1;

  if (env->curapp) sui_appl_dec_refcnt(env->curapp);
  env->curapp = NULL;

  GdCloseScreen(env->sui_global_dc.psd);
  if (env->sui_global_evar) {
    if (env->sui_global_evar->sui_mouse_fd >= 0)
      GdCloseMouse();
    if (env->sui_global_evar->sui_kbd_fd >= 0)
      GdCloseKeyboard();
  }

  free(env);

  return 0;
}


/**
 * sui_environment_create_application - Create application
 * @env: Pointer to an environment.
 * @nsname: Name of the application for adding to namespace (It can be NULL).
 * @apath: Pointer to a UTF8 application path (all paths in scenario are relative to this path).
 *
 * The function creates new application and initiates it.
 *
 * Return Value: The function returns zero as success and a negative value if any error occurs.
 *
 * File: sui_environment.c
 */
int sui_environment_create_application(sui_environment_t *env, char *nsname, utf8 *apath)
{
  sui_application_t *newappl;
  if (!env || !apath) return -1;

  newappl = (sui_application_t *)
                    sui_obj_new_vmt((void *) &sui_application_vmt_data);
  if (!newappl) return -1;
  env->curapp = newappl;
// the following variables must be set if application will be switched 
//  sui_curapp = env->curapp;
  curevbufs = &(env->curapp->events);

  sui_utf8_inc_refcnt(apath);
  newappl->base_path = apath;

  if (nsname)
    ns_add_object_to_ns(&ns_global_namespace, ns_create_object(nsname, newappl, SUI_TYPE_APPLICATION, NSOF_DYNAMIC_NAME | NSOF_DYNAMIC_DATA));

  return 0;
}
