/* sui_coordtype.h
 *
 * SUITK coordinete type definition
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#ifndef _SUITK_COORDTYPE_H_
  #define _SUITK_COORDTYPE_H_

#ifdef __cplusplus
extern "C" {
#endif

/* screen coordinate type */
typedef short sui_coordinate_t; // coordinate type

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /*_SUITK_COORDTYPE_H_*/
