/* sui_wbutton.c
 *
 * SUITK Button widget.
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#include "sui_wbutton.h"

#include <ul_log.h>

UL_LOG_CUST(ulogd_suitk)


/******************************************************************************
 * Button widget slot functions
 ******************************************************************************/
int sui_button_set_state(sui_widget_t *widget, int state);

int sui_wbutton_toggle(sui_widget_t *widget)
{
  ul_logdeb("wbutton toggle\n");
  if (widget && !sui_is_flag(widget, SUBF_BTNTYPEMASK, SUBF_PUSH_BTN)) {
    int state;
    state = sui_test_flag(widget, SUBF_CHECKED) ? 1 : 0;
    return sui_button_set_state(widget, !state);
  }
  return 0;
}

int sui_wbutton_check(sui_widget_t *widget)
{
  ul_logdeb("wbutton check\n");
  if (widget && !sui_is_flag(widget, SUBF_BTNTYPEMASK, SUBF_PUSH_BTN)) {
    return sui_button_set_state(widget, 1);
  }
  return 0;
}

int sui_wbutton_uncheck(sui_widget_t *widget)
{
  ul_logdeb("wbutton uncheck\n");
  if (widget && !sui_is_flag(widget, SUBF_BTNTYPEMASK, SUBF_PUSH_BTN)) {
    return sui_button_set_state(widget, 0);
  }
  return 0;
}


/******************************************************************************/
/**
 * sui_button_get_state
 */
static inline
int sui_button_get_state(sui_widget_t *widget, int *state)
{
  if (!widget) return -1;
  if (!sui_is_flag(widget, SUBF_BTNTYPEMASK, SUBF_PUSH_BTN)) {
    long hlp = 0;
    if(sui_is_flag(widget, SUBF_FOLLOWVALUE, SUBF_FOLLOWVALUE) &&
       sui_btnwdg(widget)->di) {
      if (sui_rd_long(sui_btnwdg(widget)->di, 0, &hlp)!=SUI_RET_OK)
        hlp = sui_is_flag(widget, SUBF_CHECKED, SUBF_CHECKED);
    } else {
      hlp = sui_is_flag(widget, SUBF_CHECKED, SUBF_CHECKED);
    }
    *state = hlp;
  } else {
    *state = sui_is_flag(widget, SUBF_PUSHED, SUBF_PUSHED);
  }
  return 0;
}


/**
 * sui_button_set_state - Set button state and put event if widget state was other than new state
 * The function returns zero if state of the button wasn't changed.
 */
inline
int sui_button_set_state(sui_widget_t *widget, int state)
{
  int was;

  if (sui_button_get_state(widget, &was)) return -1;
  if (state) {
    sui_set_flag(widget, SUBF_CHECKED);
  } else {
    sui_clear_flag(widget, SUBF_CHECKED);
  }
  if (state != was) {
    if (sui_btnwdg(widget)->di) { /* set connected dinfo to new state - only for change state */
      long hlp = (state) ? 1 : 0;
      long val = hlp;
      if (sui_test_flag(widget, SUBF_USEVALUE) && hlp)
        val = sui_btnwdg(widget)->value;
      if (hlp || !sui_test_flag(widget, SUBF_GROUPED))
        sui_wr_long(sui_btnwdg(widget)->di, 0, &val);
    }
    sui_obj_emit((sui_obj_t *)widget, SUI_SIG_STATECHANGED, state);
    sui_put_command_event(SUCM_CHANGED, widget, state);
    if(state) {
      sui_obj_emit((sui_obj_t *)widget, SUI_SIG_ACTIVATED);
    } else {
      sui_obj_emit((sui_obj_t *)widget, SUI_SIG_DEACTIVATED);
    }
    sui_damage( widget);
  }
  return 0;
}


/******************************************************************************
 * Button basic functions
 ******************************************************************************/
/**
 * sui_wbutton_get_size - Return button content size
 * @dc: Pointer to device context.
 * @widget: Pointer to widget.
 * @point: pointer to point structure
 */
static inline
int sui_wbutton_get_size( sui_dc_t *dc, sui_widget_t *widget, sui_point_t *point)
{
  sui_coordinate_t s = 0;
  if ( !sui_is_flag( widget, SUBF_BTNTYPEMASK, SUBF_PUSH_BTN)) {
    if ( sui_test_flag( widget, SUBF_USEIMAGE) && sui_btnwdg(widget)->image) {
      point->x = sui_btnwdg(widget)->image->width+2; /* add frame */
      point->y = sui_btnwdg(widget)->image->height+2; /* add frame */
    } else {
      s = sui_btnwdg( widget)->size;
      switch( widget->flags & SUBF_BTNTYPEMASK) {
        case SUBF_LIGHT_BTN: case SUBF_CHECK_BTN:
          s += 2;
          break;
        case SUBF_RADIO_BTN:
          if ( !(s & 0x0001)) s++;
          break;
      }
      point->x = s;
      point->y = s;
    }
  } else {
    point->x = 0;
    point->y = 0;
  }
  return 0;
}

/**
 * sui_wbutton_init - Reaction to INIT button event
 * @widget: Pointer to button widget.
 * @event: Pointer to INIT event.
 *
 * The function initializes the button widget.
 *
 * Return Value: The function does not return a value.
 * File: sui_button.c
 */
static inline
void sui_wbutton_init(sui_dc_t *dc, sui_widget_t *widget)
{
  if (widget->place.w < 0 || widget->place.h < 0) {
    sui_point_t size;
    sui_widget_compute_size(dc, widget, &size);
    if (widget->place.w < 0) widget->place.w = size.x;
    if (widget->place.h < 0) widget->place.h = size.y;
  }
  if (!sui_is_flag(widget, SUBF_BTNTYPEMASK, SUBF_PUSH_BTN) &&
      sui_btnwdg(widget)->di &&
      (!sui_test_flag(widget, SUBF_GROUPED) ||
        sui_is_flag(widget, SUBF_GROUPED | SUBF_USEVALUE, SUBF_GROUPED | SUBF_USEVALUE))) { /* set button status according to dinfo if it is not a PUSH button and it is not in a group */
    long hlp = 0;
    if (sui_rd_long(sui_btnwdg(widget)->di, 0, &hlp)==SUI_RET_OK) {
      if (sui_test_flag(widget, SUBF_USEVALUE)) /* if a button value should be used - check if dinfo is equal to the value */
        hlp = (hlp==sui_btnwdg(widget)->value) ? 1 : 0;
      if (hlp) /* set/reset state of button */
        sui_set_flag(widget, SUBF_CHECKED);
      else
        sui_clear_flag(widget, SUBF_CHECKED);
    }
  }
}

/**
 * sui_wbutton_draw - Reaction to DRAW button event
 * @dc: Pointer to device context.
 * @widget: Pointer to the button widget.
 *
 * The function draws the button widget.
 *
 * Return Value: The function does not return a value.
 * File: sui_button.c
 */
static inline
int sui_wbutton_draw( sui_dc_t *dc, sui_widget_t *widget)
{
  sui_point_t box = { .x=widget->place.w, .y=widget->place.h};
  sui_point_t size, origin;

  unsigned long btntype = sui_test_flag( widget, SUBF_BTNTYPEMASK);
  sui_align_t al;
  int check = 0;

  al = sui_widget_get_item_align( widget);

  if ( btntype == SUBF_PUSH_BTN) return 0;

  sui_button_get_state( widget, &check);

  if ( sui_hevent_command( widget, SUCM_GETSIZE, dc, (long) &size)) {
    size.x = size.y = 0;
  }

  sui_align_object( &box, &origin, &size, 0, widget->style, al);

  if ( sui_test_flag( widget, SUBF_USEIMAGE) && sui_btnwdg(widget)->image) {
    sui_set_widget_fgcolor( dc, widget, SUC_ITEM); //clradd +
    sui_draw_rect( dc, origin.x, origin.y, size.x, size.y, 1);
    if ( check) {
      origin.x++;
      origin.y++;
      sui_gdi_draw_image( dc, &origin, sui_btnwdg(widget)->image, NULL, 0,
      sui_test_flag( widget, SUFL_HIGHLIGHTED)?SUIO_INVERSE_IMAGE:0);//SUFL_FOCUSED
    }
  } else {
    sui_rect_t rect = {.x=origin.x, .y=origin.y, .w=size.x, .h=size.y};
    switch( btntype) {
    case SUBF_LIGHT_BTN:
      if (check)
        sui_gdi_set_fgcolor(dc, sui_btnwdg(widget)->color);
      else
        sui_set_widget_fgcolor(dc, widget, SUC_BACKGROUND);
      rect.x++; rect.y++; rect.w-=2; rect.y-=2;
      sui_draw_rect( dc, rect.x, rect.y, rect.w, rect.h, 0);
      sui_draw_frame( dc, widget, &rect, 1);
      break;
    case SUBF_RADIO_BTN: /* jestli je x a y stred ... */
      if ( !(rect.w & 0x0001)) rect.w++;
      if ( !(rect.h & 0x0001)) rect.h++;
      rect.w = rect.w/2;
      rect.h = rect.h/2;
      sui_set_widget_fgcolor(dc, widget, SUC_FRAME2);
      sui_draw_ellipse(dc, rect.x+rect.w,
                      rect.y+rect.h, rect.w, rect.h, 0);
      sui_set_widget_fgcolor(dc, widget, SUC_FRAME);
      sui_draw_ellipse(dc, rect.x+rect.w,
                      rect.y+rect.h, rect.w+1, rect.h+1, 0);
      if (check)
        sui_set_fgcolor(dc, widget, SUC_ITEM);
      else
        sui_set_fgcolor(dc, widget, SUC_BACKGROUND);
      sui_draw_ellipse(dc, rect.x+rect.w,
                      rect.y+rect.h, rect.w-1, rect.h-1, 1);
      break;
    case SUBF_CHECK_BTN:
      rect.x++; rect.y++; rect.w-=2; rect.h-=2;

      sui_set_fgcolor(dc, widget, SUC_BACKGROUND);
      sui_draw_rect( dc, rect.x, rect.y, rect.w, rect.h, 0);

      if ( check) {
        sui_set_fgcolor(dc, widget, SUC_ITEM);
        sui_draw_line( dc, rect.x, rect.y, rect.x+rect.w-1,
                      rect.y+rect.h-1);
        sui_draw_line( dc, rect.x, rect.y+rect.h-1,
                      rect.x+rect.w-1, rect.y);
      }
      sui_draw_frame( dc, widget, &rect, 1);
      break;
    }
  }
  return 0;
}

/**
 * sui_button_release - Reaction to release the button 
 */
static inline
int sui_button_release(sui_widget_t *widget)
{
  if (sui_test_flag(widget, SUBF_INT_PRESSED)) {
//ul_logdeb("button released (flgs=0x%08X)\n",widget->flags);
    sui_clear_flag(widget, SUBF_PUSHED | SUFL_WIDGETISDOWN | SUBF_INT_PRESSED);
    if (sui_is_flag(widget, SUBF_BTNTYPEMASK, SUBF_PUSH_BTN)) {

  /* emit signal and event */
      sui_obj_emit((sui_obj_t *)widget, SUI_SIG_RELEASED, 0);

      /* template button id - emit event */
      if (sui_btnwdg(widget)->wid >= 0 && widget->parent) {
        sui_event_t ev;
        ev.what = SUEV_COMMAND;
        ev.message.command = SUCM_NOTIFY;
        ev.message.info = sui_btnwdg(widget)->wid;
        ev.message.ptr = widget;
        sui_hevent(widget->parent, &ev);
      } else {
        sui_put_command_event(SUCM_PUSHED, widget, 0);
      }
      return 1;
    }
  }
  return 0;
}


/**
 * sui_button_grouped_distribute_fnc - clear grouped radio button
 */
static inline
int sui_button_grouped_distribute_fnc(sui_widget_t *group, sui_widget_t *widget, sui_event_t *event)
{
  if (sui_test_flag(widget, SUFL_DISABLED) ||
      (widget->type != SUWT_BUTTON) ||
      !sui_is_flag(widget, SUBF_BTNTYPEMASK, SUBF_RADIO_BTN) ||
      !sui_test_flag(widget, SUBF_GROUPED))
    return 1;
  if(event->what == SUEV_NOTHING) return 1;
  sui_hevent(widget, event);
  return 1;
}

/**
 * sui_wbutton_hkey - Reaction to KEY_DOWN, KEY_UP button event
 * @widget: Pointer to the button widget.
 * @event: Pointer to the event.
 *
 * The function response to keyboard events for the button widget.
 * Return Value: The function returns 0 if it takes to event.
 * File: sui_button.c
 */
static inline
int sui_wbutton_hkey(sui_widget_t *widget, sui_event_t *event)
{
  unsigned short key = event->keydown.keycode;
  if ((key == ' ') || (key == MWKEY_ENTER)) {
  /*ul_logdeb( " Button key = 0x%X (dwn=%d), flags = 0x%X\n", key, event->what == SUEV_KDOWN,widget->objflags); */

/* PUSH button widget */
    if (sui_is_flag(widget, SUBF_BTNTYPEMASK, SUBF_PUSH_BTN)) {
      if (!sui_test_flag(widget, SUBF_INT_PRESSED) || sui_test_flag(widget, SUBF_REPEAT)) {    /* button was pressed */
        if (sui_btnwdg(widget)->di) {
          long hlp = 1;
          if (sui_test_flag(widget, SUBF_USEVALUE))
            hlp = sui_btnwdg(widget)->value;
          sui_wr_long(sui_btnwdg(widget)->di, 0, &hlp);
        }
        sui_set_flag(widget, SUBF_PUSHED | SUFL_WIDGETISDOWN | SUBF_INT_PRESSED);
//ul_logdeb("button pressed\n");
        sui_obj_emit((sui_obj_t *)widget, SUI_SIG_PRESSED, 0);

        return 0;
      } else
        return -1;
    } else {

/* All other types */
      int state = 0;
      if (sui_button_get_state(widget, &state)) return -1;
      if (sui_is_flag(widget, SUBF_BTNTYPEMASK, SUBF_RADIO_BTN)) {    /* radio button */
        if (!state) {
          sui_put_command_event(SUCM_CHANGED, widget, 1);
          if (widget->parent && sui_test_flag(widget, SUBF_GROUPED)) {
            sui_event_t e, be;
            e.what = SUEV_COMMAND; e.message.command = SUCM_DISTRIBUTE;
            e.message.ptr = sui_button_grouped_distribute_fnc;    /* send only to all grouped radio buttons */
            e.message.info = ( long) &be;
            be.what = SUEV_COMMAND; be.message.command = SUCM_SETSTATE;
            be.message.info = 0;
            sui_hevent(widget->parent, &e);
          }
          sui_button_set_state(widget, 1);
        } else
          return -1;
      } else { /* checkbox , light */
        if (!sui_test_flag(widget, SUBF_INT_PRESSED) || sui_test_flag(widget, SUBF_REPEAT)) {    /* button was pressed */
          sui_set_flag(widget, SUBF_PUSHED | SUBF_INT_PRESSED);
          sui_button_set_state(widget, !state);
        } else
          return -1;
      }
    }
    sui_redraw_request(widget);
    return 0;
  }
  return -1;
}


/******************************************************************************
 * Button event handler
 ******************************************************************************/
/**
 * sui_button_hevent - Widget event handler for button widget
 * @widget: Pointer to the button widget.
 * @event: Pointer to the event.
 *
 * The function responses to event for button widget or calls
 * basic event handler common for all widget. 
 * Return Value: The function does not return a value.
 * File: sui_button.c
 */
void sui_wbutton_hevent(sui_widget_t *widget, sui_event_t *event)
{
  switch(event->what) {
    case SUEV_DRAW:
    case SUEV_REDRAW:
      sui_widget_draw(event->draw.dc, widget, sui_wbutton_draw);
      return;
    case SUEV_KDOWN:
      if (sui_wbutton_hkey(widget, event)>0)
        sui_clear_event(widget, event);
      break;
    case SUEV_KUP:
      if (sui_button_release(widget)>0)
        sui_clear_event(widget, event);
      break;
    case SUEV_COMMAND:
      switch(event->message.command) {
        case SUCM_FOCUSREL:
          sui_button_release(widget);
          break;
        case SUCM_INIT:
          sui_widget_hevent(widget, event);
          sui_wbutton_init((sui_dc_t *)event->message.ptr, widget);
          sui_clear_event(widget, event);
          return;
        case SUCM_GETSIZE:
          if (!sui_wbutton_get_size((sui_dc_t *)event->message.ptr,
              widget, (sui_point_t *)event->message.info)) {
            sui_clear_event( widget, event);
            return;
          }
          break;
        case SUCM_GETSTATE:
          {
            int state = 0;
            if (!sui_button_get_state(widget, &state)) {
              //event->message.info = state;
              sui_clear_event(widget, event);
              return;
            }
          }
          break;
        case SUCM_SETSTATE:
          if (!sui_button_set_state(widget, event->message.info)) {
            sui_clear_event(widget, event);
            return;
          }
          break;
      }
  }
  if (event->what != SUEV_NOTHING)
    sui_widget_hevent(widget, event);
}


/******************************************************************************
 * Button widget vmt & signal-slot mechanism
 ******************************************************************************/
int sui_wbutton_vmt_init( sui_widget_t *pw, void *params)
{
	pw->data = sui_malloc( sizeof( suiw_button_t));
	if(!pw->data)
		return -1;

	((suiw_button_t *)pw->data)->wid = -1;

	pw->type = SUWT_BUTTON;
	pw->hevent = sui_wbutton_hevent;

	return 0;
}

void sui_wbutton_vmt_done( sui_widget_t *pw)
{
	if(pw->data) {
		suiw_button_t *btn = pw->data;

		if ( btn->image) sui_image_dec_refcnt( btn->image);
		btn->image = NULL;
		if ( btn->di) sui_dinfo_dec_refcnt( btn->di);
		btn->di = NULL;

		free( btn);
		pw->data = NULL;
	}
	pw->type = SUWT_GENERIC;
	pw->hevent = sui_widget_hevent; // NULL;
}


SUI_CANBE_CONST
sui_obj_signal_tinfo_t sui_wbutton_signal_tinfo[] = {
  { "statechanged", SUI_SIG_STATECHANGED, &sui_args_tinfo_int},
  { "pressed", SUI_SIG_PRESSED, &sui_args_tinfo_void},
  { "released", SUI_SIG_RELEASED, &sui_args_tinfo_void},
  { "activated", SUI_SIG_ACTIVATED, &sui_args_tinfo_void},
  { "deactivated", SUI_SIG_DEACTIVATED, &sui_args_tinfo_void},
};

SUI_CANBE_CONST
sui_obj_slot_tinfo_t sui_wbutton_slot_tinfo[] = { 
  { "toggle", SUI_SLOT_TOGGLE, &sui_args_tinfo_void,
          .target_kind = SUI_STGT_METHOD_OFFSET,
          .target.method_offset=UL_OFFSETOF( sui_wbutton_vmt_t, toggle)},
};

SUI_CANBE_CONST
sui_obj_tinfo_t sui_wbutton_vmt_obj_tinfo = {
  .name = "sui_wbutton",
  .obj_size = sizeof(sui_widget_t),
  .obj_align = UL_ALIGNOF(sui_widget_t),
  .signal_count = MY_ARRAY_SIZE(sui_wbutton_signal_tinfo),
  .signal_tinfo = UL_CAST_UNQ1(sui_obj_signal_tinfo_t *, sui_wbutton_signal_tinfo),
  .slot_count = MY_ARRAY_SIZE(sui_wbutton_slot_tinfo),
  .slot_tinfo = UL_CAST_UNQ1(sui_obj_slot_tinfo_t *, sui_wbutton_slot_tinfo)
};

sui_wbutton_vmt_t sui_wbutton_vmt_data = {
  .vmt_size = sizeof(sui_wbutton_vmt_t),
  .vmt_refcnt = SUI_STATIC,
  .vmt_parent = &sui_widget_vmt_data,
  .vmt_init = sui_wbutton_vmt_init,
  .vmt_done = sui_wbutton_vmt_done,
  .vmt_obj_tinfo =UL_CAST_UNQ1(sui_obj_tinfo_t *,  &sui_wbutton_vmt_obj_tinfo),

  .toggle = sui_wbutton_toggle,

};

/******************************************************************************/
/**
 * sui_wbutton - Create button widget
 * @aplace: Button position and size.
 * @alabel: Button label in utf8 string.
 * @astyle: Button widget style.
 * @aflags: Widget common and button special flags.
 * @asize: Size(in pixel) of button item (check,radio,light,...).
 * @aialign: Align the button item to the button label.
 * @abicolor: Color of the button item.
 *
 * The function creates the button widget.
 *
 * Return Value: The function returns pointer to created button widget.
 * File: sui_button.c
 */
sui_widget_t *sui_wbutton( sui_rect_t *aplace, utf8 *alabel, sui_style_t *astyle,
               sui_flags_t aflags, sui_coordinate_t asize, sui_color_t abicolor)
{
	sui_widget_t *pw;

	pw = sui_widget_create_new( &sui_wbutton_vmt_data, aplace, alabel, astyle, aflags);
	if ( !pw) return NULL;
	
	{
		suiw_button_t *wbtn = sui_btnwdg(pw);

		wbtn->size = asize;
		wbtn->color = abicolor;
		wbtn->wid = -1;
	}
	return pw;
}

sui_widget_t *sui_wbutton_with_dinfo( sui_rect_t *aplace, utf8 *alabel, sui_style_t *astyle,
               sui_flags_t aflags, sui_coordinate_t asize, sui_color_t abicolor, sui_dinfo_t *adinfo)
{
	sui_widget_t *pw;
	pw = sui_wbutton(aplace, alabel, astyle, aflags, asize, abicolor);
	if ( !pw) return NULL;
	
	if( adinfo)
	{
		suiw_button_t *wbtn = sui_btnwdg(pw);
		sui_dinfo_inc_refcnt( adinfo);
		wbtn->di = adinfo;
	}
	return pw;
}
