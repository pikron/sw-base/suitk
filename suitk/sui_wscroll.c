/* sui_wscroll.c
 *
 * SUITK scroll (and progess) widget.
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#include "sui_wscroll.h"

#include <ul_log.h>

UL_LOG_CUST(ulogd_suitk)

/******************************************************************************
 * Scroll widget slot functions
 ******************************************************************************/
int sui_wscroll_setbounds(sui_widget_t *widget, long low, long high)
{
  suiw_scroll_t *wsco = sui_scrwdg(widget);
  ul_logdeb("wscroll-set bounds %ld,%ld\n", low, high);
  if (wsco->datai) {
    wsco->datai->minval = low; // dinfo_dinfopar_proxy_wrval
    wsco->datai->maxval = high; // dinfo_dinfopar_proxy_wrval
    // correct value of the wsco->datai ???
    return 1;
  }
  return 0;
}

int sui_wscroll_setvalue(sui_widget_t *widget, long value)
{
  suiw_scroll_t *wsco = sui_scrwdg(widget);
  ul_logdeb("wscroll-set value %ld\n", value);
  if (wsco->datai) {
    if (sui_wr_long(wsco->datai, 0,&value)==SUI_RET_OK) return 1;
  }
  return 0;
}

/******************************************************************************
 * Scroll internal functions
 ******************************************************************************/
#define SUI_EDITEDVALUE    1
#define SUI_RIGHTVALUE     0

static inline
int sui_scroll_read_value( sui_widget_t *widget, long *buf, int editing)
{
  long tmp;
  int ret = SUI_RET_OK;
  suiw_scroll_t *wscr = sui_scrwdg(widget);
  if (!editing)
    tmp = wscr->intvalue;
  else
    ret = sui_rd_long(wscr->datai, 0, &tmp);

  if (wscr->datai) {
    if (tmp > wscr->datai->maxval) tmp = wscr->datai->maxval;
    else if (tmp < wscr->datai->minval) tmp = wscr->datai->minval;
  }
  if (ret==SUI_RET_OK)
    *buf = tmp;
  return ret;
}

/* internal function */
static inline
int sui_scroll_write_value( sui_widget_t *widget, long *buf, int editing)
{
  long tmp = *buf;
  int ret = SUI_RET_OK;
  suiw_scroll_t *wscr = sui_scrwdg(widget);

  if (wscr->datai) {
    if (tmp > wscr->datai->maxval) tmp = wscr->datai->maxval;
    else if (tmp < wscr->datai->minval) tmp = wscr->datai->minval;
  }
  if (!editing)
    wscr->intvalue = tmp;
  else                    /* save value directly to dinfo*/
    ret = sui_wr_long( wscr->datai, 0, &tmp);
  return ret;
}

/**
 * sui_scroll_start_edit - start editing
 */
int sui_scroll_start_edit( sui_widget_t *widget, int center)
{
    suiw_scroll_t *wscr = sui_scrwdg(widget);
    if ( !sui_test_flag( widget, SUFL_EDITEN)) return 0;
    
    if ( !sui_test_flag( widget, SUFL_EDITED)) {
        /* save current value from dinfo to intvalue - it's the same operation for both cases DIRECTLY/INTERNAL EDIT */
        if ( sui_rd_long( wscr->datai, 0, &wscr->intvalue) != SUI_RET_OK) return 0;
        if ( center) {
            long centerval = (wscr->datai->maxval + wscr->datai->minval) / 2;
            //if ( sui_wr_long( wnum->data, 0, &zerovalue) != SUI_RET_OK) return 0;
            if ( sui_scroll_write_value( widget, &centerval, SUI_EDITEDVALUE) 
                    != SUI_RET_OK) return 0;
        }
        sui_set_flag( widget, SUFL_EDITED);
    }
    return 1;
}

/* internal function */
int sui_scroll_stop_edit( sui_widget_t *widget, int confirmed)
{
	suiw_scroll_t *wscr = sui_scrwdg( widget);
	sui_event_t e;
	int ret = 1;
	if ( !sui_test_flag( widget, SUFL_EDITED)) return 0;
	e.what = SUEV_COMMAND;
	e.message.ptr = widget;
	if ( confirmed) {
/*
        if ( !sui_test_flag( widget, SUNF_ONLINEEDIT)) {
            if ( sui_wr_long( wscr->data, 0, &wscr->intvalue) != SUI_RET_OK)
                ret = 0;
        }
*/
		if ( ret) {
			long nowval;
			if (( sui_rd_long( wscr->datai, 0, &nowval) == SUI_RET_OK) && nowval != wscr->intvalue) {
				e.message.command = SUCM_CHANGED;
				sui_put_event( &e); /* SUCM_CHANGED */
//			sui_call_callback( widget, SUCF_CHANGEVALUE, &(wscr->intvalue));
			}
		}
	} else {
//        if ( sui_test_flag( widget, SUNF_ONLINEEDIT)) {
		if ( sui_wr_long( wscr->datai, 0, &wscr->intvalue) != SUI_RET_OK)
			ret = 0;
//        }
		if ( ret) {
			e.message.command = SUCM_CHANGE;
			sui_put_event( &e); /* SUCM_CHANGE */
		}
	}
	if ( !ret) {
		e.message.command = SUCM_ERR_WRVAL;
		sui_put_event( &e); /* SUCM_ERROR_WRVAL */
	}
	sui_clear_flag( widget, SUFL_EDITED);
	return ret;
}




/******************************************************************************
 * Button basic functions
 ******************************************************************************/
/**
 * sui_wscroll_get_size - Return scroll content size
 * @dc: Pointer to device context.
 * @widget: Pointer to widget.
 * @point: pointer to point structure
 */
static inline
int sui_wscroll_get_size( sui_dc_t *dc, sui_widget_t *widget, sui_point_t *point)
{
    sui_coordinate_t *wd, *hg;
    sui_coordinate_t ww = 0, hh = 0;
    unsigned char hdir = sui_is_flag( widget, SUSF_DIRMASK, SUSF_HORIZONTAL);
    if ( !widget) return -1;
    if ( hdir) {
        wd = &widget->place.h; hg = &widget->place.w;
    } else {
        wd = &widget->place.w; hg = &widget->place.h;
    }
    if ( *wd < 0) {
        *wd = SUI_SCROLL_DEFAULT_WIDTH;
        if ( *wd < sui_scrwdg(widget)->wline) *wd = sui_scrwdg(widget)->wline;
    }
    if ( sui_test_flag( widget, SUSF_PARENT_SB) && widget->parent) { // get size from parent
        if (hdir) {
            hh = widget->parent->place.w - 
                 2*(widget->style->frame + widget->style->gap);
            ww = *wd;
        } else {
            ww = widget->parent->place.h - 
                 2*(widget->parent->style->frame + widget->parent->style->gap);
            hh = *wd;
        }
    } else { // count size
        ww = *wd;
        if ( *hg < 0) {
            int s;
            s = sui_scrwdg(widget)->datai->maxval - 
                sui_scrwdg(widget)->datai->minval;
            *hg = 2*SUI_SCROLL_DEFAULT_WIDTH+sui_scrwdg(widget)->wpuller+s;
        }
        hh = *hg;
    }
    if ( hdir) { // return size
        point->x = hh; point->y = ww;
    } else {
        point->x = ww; point->y = hh;
    }
    return 1;
}


/**
 * sui_wscroll_init - Reaction to INIT event in scroll widget
 * @widget: Pointer to scroll widget.
 * @event: Pointer to INIT event.
 *
 * The function initializes the scroll widget.
 *
 * Return Value: The function does not return a value.
 * File: sui_wscroll.c
 */
//inline
void sui_wscroll_init( sui_widget_t *widget, sui_dc_t *dc)
{
/* set default values into empty fields */

	if ( widget->place.w < 0 || widget->place.h < 0) {
		sui_point_t size;
		sui_widget_compute_size( dc, widget, &size);
		if ( widget->place.w < 0) widget->place.w = size.x;
		if ( widget->place.h < 0) widget->place.h = size.y;
	}
	if ( sui_test_flag( widget, SUSF_PARENT_SB) && widget->parent) {
		if ( widget->place.x < 0) {
			widget->place.x = widget->parent->place.w - 
						widget->place.w - 
						widget->style->frame;
		}
		if ( widget->place.y < 0) {
			widget->place.y = widget->parent->place.h - 
						widget->place.h - 
						widget->style->frame;
		}
	}
}


#define SUI_SCROLL_NO_MARK     0x00
#define SUI_SCROLL_ARROW_MARK  0x01
#define SUI_SCROLL_GROOVE_MARK 0x02

/* internal scroll widget draw function - draw button with arrow */
void sui_wscroll_button_draw( sui_dc_t *dc, sui_widget_t *widget, sui_rect_t *rect, 
			unsigned char revflg, unsigned char mark, int color) 
{
	sui_rect_t box;
	unsigned char hdir;
	sui_point_t from, to;
	sui_coordinate_t s;
	
	if ( !dc || !widget || !rect) return;
    
	hdir = sui_is_flag( widget, SUSF_DIRMASK, SUSF_HORIZONTAL);
	box.x = rect->x;// + 1;
	box.y = rect->y;// + 1;
	box.w = rect->w;// - 2;
	box.h = rect->h;// - 2;
/* draw background and frame */
	sui_set_widget_fgcolor( dc, widget, color);
	sui_draw_rect_box( dc, &box, 0);
	sui_draw_frame( dc, widget, &box, 1);
/* get item size */
	if ( box.w < box.h) s = (box.w-3)/2; //w=13->s=5,w=14->s=5
	else s = (box.h-3)/2;

	if ( s > 0) { // draw arrow
		switch( mark) {
		case SUI_SCROLL_ARROW_MARK:
			sui_set_widget_fgcolor( dc, widget, SUC_FRAME);
			if ( hdir) {
				to.x = from.x = box.x + (box.w - s/2)/2 + (revflg ? s/2 : 0);
				to.y = from.y = box.y + box.h/2;
				if ( !(box.h & 0x01)) from.y--;
			} else {
				to.x = from.x = box.x + box.w/2;
				if ( !(box.w & 0x01)) from.x--;
				to.y = from.y = box.y + (revflg ? (box.h - 1 - s) : s);
			}
//			ul_logdeb("Arrow - s=%d, box=[%d,%d,%d,%d], from=[%d,%d], to=[%d,%d]\n", s, box.x, box.y, box.w, box.h, from.x, from.y, to.x, to.y);

			while(s>0) {
				sui_draw_line_points( dc, &from, &to);
				if ( hdir) {
					from.y--; to.y++;
					if ( revflg) { 
						from.x--; to.x--;
					} else {
						from.x++; to.x++;
					}
				} else {
					from.x--; to.x++;
					if ( revflg) { 
						from.y--; to.y--;
					} else {
						from.y++; to.y++;
					}
				}
				s--;
			}
			break;
		case SUI_SCROLL_GROOVE_MARK:        
			if (hdir) {
				box.x += box.w/2; box.y += 2; 
				box.w = 0; box.h -= 4;
				if ( box.h > 4) { box.y += 2; box.h -= 4;}
			} else {
				box.x += 2; box.y += box.h/2; 
				box.w -= 4; box.h = 0;
				if ( box.w > 4) { box.x += 2; box.w -= 4;}
			}
			sui_draw_frame( dc, widget, &box, -1);
			break;
		}
	}
}

/**
 * sui_wscroll_draw - Reaction to DRAW event in scroll widget
 * @dc: Pointer to device context.
 * @widget: Pointer to scroll widget.
 *
 * The function draws the scroll widget.
 *
 * Return Value: The function does not return a value.
 * File: sui_scroll.c
 */
//inline
int sui_wscroll_draw( sui_dc_t *dc, sui_widget_t *widget) {
	sui_dinfo_t *di = sui_scrwdg(widget)->datai;
	sui_coordinate_t *pw, *ph;
	sui_coordinate_t *prx, *pry, *prw, *prh;
	long act = 0;
	sui_coordinate_t rx, rw, ry, rh;
    
	sui_rect_t box;
	sui_coordinate_t ofs = 0, size;
	unsigned char hdir = sui_is_flag( widget, SUSF_DIRMASK, SUSF_HORIZONTAL);

	if ( hdir) {
		pw = &widget->place.h; ph = &widget->place.w;
		prx = &ry; pry = &rx; prw = &rh; prh = &rw;
	} else {
		pw = &widget->place.w; ph = &widget->place.h;
		prx = &rx; pry = &ry; prw = &rw; prh = &rh;
	}

	ofs = 1; size = *ph-2;
/* show buttons */
	if ( !sui_test_flag( widget, SUSF_HIDEBUTTONS)) {
		rw = *pw; 
		rh = rw;
		if ( rh > *ph/2) rh = *ph/2;
		if ( rh > 2*SUI_SCROLL_DEFAULT_WIDTH) rh = 2*SUI_SCROLL_DEFAULT_WIDTH;
		size -= 2*(rh+1); ofs += rh+1;
	
		box.x = 0; box.y = 0;
		box.w = *prw; box.h = *prh;
		sui_wscroll_button_draw( dc, widget, &box, 0, 
					SUI_SCROLL_ARROW_MARK, SUC_BACKGROUND);

		box.x = widget->place.w-*prw; box.y = widget->place.h-*prh;
		box.w = *prw; box.h = *prh;
		sui_wscroll_button_draw( dc, widget, &box, 1, 
					SUI_SCROLL_ARROW_MARK, SUC_BACKGROUND);
	}
/* show ticks */
	if ( sui_test_flag( widget, SUSF_BOTHTICKS) && 
	   (*pw-sui_scrwdg(widget)->wline)>6 && size > 20) {
		sui_coordinate_t i;
		sui_set_widget_fgcolor( dc, widget, SUC_ITEM2);
		for (i=0;i<=10;i++) {
			rx = (*pw - sui_scrwdg(widget)->wline-2)/2;
			rh = ry = ofs + (i * (size-1))/10;
			rw = (i % 5) ? 3 : 1;
			if ( sui_test_flag( widget, SUSF_TOPTICK)) {
				sui_draw_line( dc, *prx, *pry, *prw, *prh);
			}
			if ( sui_test_flag( widget, SUSF_BOTTOMTICK)) {
				rw = *pw - rw - 1; rx = *pw - rx - 1;
				sui_draw_line( dc, *prx, *pry, *prw, *prh);
			}
		}            
	}
/* show line */
	if ( sui_test_flag( widget, SUSF_SHOWLINE) || 
	     sui_is_flag( widget, SUSF_MODEMASK, SUSF_MODE_PROGRESS)) {
		rw = sui_scrwdg(widget)->wline; rh = size;
		if ( rw > (*pw-2) || rw <= 0) {
			rw = *pw-2;
		} else {
			if ( !(*pw & 0x0001) && rw > 0) rw--;
		}
		rx = (*pw - rw)/2; ry = ofs;
		
		box.x = *prx; box.y = *pry; box.w = *prw; box.h = *prh;
		sui_draw_frame( dc, widget, &box, 1);
		if ( sui_is_flag( widget, SUSF_MODEMASK, SUSF_MODE_PROGRESS)) { // count length of progress bar

			sui_scroll_read_value( widget, &act, 1);
			//di->rdval( di, 0, &act);
			rh = (sui_coordinate_t)(((long)size*(act-(long)di->minval))/(di->maxval-di->minval));
			if ( sui_is_flag( widget, SUSF_REVERSE, SUSF_REVERSE) == hdir) {
				ry = size - rh + 1;
			}
		}
		if ( !sui_test_flag( widget, SUSF_NOFILLLINE)) {
			sui_set_widget_fgcolor( dc, widget, SUC_ITEM2);
			sui_draw_rect( dc, *prx, *pry, *prw, *prh, 0);
		}
	}
/* show puller */
	if (size > 0) {
		unsigned long mode = widget->flags & SUSF_MODEMASK;
		rx = 0; rw = *pw;
		if ( mode != SUSF_MODE_PROGRESS) {
			if ( mode == SUSF_MODE_AUTOSIZE && size > 4) {
				rh = (sui_coordinate_t)(size / (di->maxval-di->minval+1));
				if ( rh < 4) rh = 4;
			} else { // SUSF_MODE_FIXEDSIZE:
				rh = sui_scrwdg(widget)->wpuller;
				if ( rh > size) rh = size;
			}
			size = size - rh+2;
			sui_scroll_read_value( widget, &act, 1);
			//di->rdval( di, 0, &act);
			if (( di->maxval-di->minval) == 0) {
				ry=0;
			} else {
				ry = (sui_coordinate_t)(((long)size*(act-di->minval))/(di->maxval-di->minval))-1;
			}
			if ( sui_is_flag( widget, SUSF_REVERSE, SUSF_REVERSE) == hdir) {
				ry = size - ry - 2;
			}
			ry += ofs;
			if ( !sui_style_test_flag( widget->style, SUSFL_FRAME)) {
				rx++; rw-=2;
				ry++; rh-=2;
			}
			box.x = *prx; box.y = *pry; box.w = *prw; box.h = *prh;
			sui_wscroll_button_draw( dc, widget, &box, 0, 
				(unsigned char)(sui_test_flag( widget, SUSF_NOTHUMBLINE) ? 0 : SUI_SCROLL_GROOVE_MARK), 
				SUC_ITEM);
		}
	}
	return 1;
}

/**
 * sui_wscroll_hkey - Reaction to KEY_DOWN, KEY_UP scroll event
 * @widget: Pointer to scroll widget.
 * @event: Pointer to KEY__ event.
 *
 * The function is reaction to keyboard event for the scroll widget.
 *
 * Return Value: The function return 1 if function takes the event.
 * File: sui_scroll.c
 */
//inline
int sui_wscroll_hkey( sui_widget_t *widget, sui_event_t *event) {
	unsigned short key = event->keydown.keycode;
	long new, last;
	sui_dinfo_t *di = sui_scrwdg(widget)->datai;
	sui_finfo_t *fi = sui_scrwdg(widget)->format;
	sui_rd_long( di, 0, &last);
	if ( sui_test_flag( widget, SUSF_REVERSE)) {
		switch( key) {
			case MWKEY_PAGEUP:   key = MWKEY_PAGEDOWN; break;
			case MWKEY_PAGEDOWN: key = MWKEY_PAGEUP; break;
			case MWKEY_UP:       key = MWKEY_DOWN; break;
			case MWKEY_DOWN:     key = MWKEY_UP; break;
			case MWKEY_RIGHT:    key = MWKEY_LEFT; break;
			case MWKEY_LEFT:     key = MWKEY_RIGHT; break;
			case MWKEY_HOME:     key = MWKEY_END; break;
			case MWKEY_END:      key = MWKEY_HOME; break;
		}
	}
    
	switch( key) {
		case MWKEY_UP:
			if ( sui_test_flag( widget, SUFL_EDITEN))
				new = last + fi->litstep;
			else
				return 0;
			break;
		case MWKEY_RIGHT:
			if ( sui_test_flag( widget, SUFL_EDITEN))
				new = last + fi->litstep;
			else
				return 0;
			break;
		case MWKEY_DOWN:
			if ( sui_test_flag( widget, SUFL_EDITEN))
				new = last - fi->litstep;
			else
				return 0;
			break;
		case MWKEY_LEFT:
			if ( sui_test_flag( widget, SUFL_EDITEN))
				new = last - fi->litstep;
			else
				return 0;
			break;
		case MWKEY_PAGEUP:
			if ( sui_test_flag( widget, SUFL_EDITEN))
				new = last + fi->bigstep;
			else
				return 0;
			break;
		case MWKEY_PAGEDOWN:
			if ( sui_test_flag( widget, SUFL_EDITEN))
				new = last - fi->bigstep;
			else
				return 0;
			break;
		case MWKEY_HOME:
			if ( sui_test_flag( widget, SUFL_EDITEN))
				new = di->maxval;
			else
				return 0;
			break;
		case MWKEY_END:
			if ( sui_test_flag( widget, SUFL_EDITEN))
				new = di->minval;
			else
				return 0;
			break;
		case MWKEY_ENTER:
			if ( sui_test_flag( widget, SUFL_EDITED)) {
				sui_hevent_command( widget, SUCM_CONFIRM, widget, 0);
				return 1;
			}
			return 0;
		case MWKEY_ESCAPE:
			if ( sui_test_flag( widget, SUFL_EDITED)) {
				sui_hevent_command( widget, SUCM_DISCARD, widget, 0);
				return 1;
			}
			return 0;
		default:
			return 0; /* no action */
	}
	if ( di->maxval != di->minval) {
		if ( new > di->maxval) new = di->maxval;
		if ( new < di->minval) new = di->minval;
	}
	if ( sui_test_flag( widget, SUFL_EDITEN)) {
		sui_hevent_command( widget, SUCM_STARTEDIT, widget, 1);
		sui_scroll_write_value( widget, &new, 1);
	}
	sui_clear_event( widget, event);
	if ( last != new) {
		sui_redraw_request( widget);
	}
	return 1;
}


/******************************************************************************
 * Scroll event handler
 ******************************************************************************/
/**
 * sui_wscroll_hevent - Widget event handler for scroll widget
 * @widget: Pointer to the scroll widget.
 * @event: Pointer to the event.
 *
 * The function responses to the event for scroll widget or calls
 * basic event handler common for all widget.
 * Return Value: The function does not return a value.
 * File: sui_scroll.c
 */
void sui_wscroll_hevent( sui_widget_t *widget, sui_event_t *event) {
	switch( event->what) {
		case SUEV_DRAW: case SUEV_REDRAW:
			sui_widget_draw( event->draw.dc, widget, sui_wscroll_draw);
			return;
		case SUEV_KDOWN:
			if ( sui_wscroll_hkey( widget, event)>0) {
				sui_clear_event( widget, event);
			}
			break;
		case SUEV_COMMAND:
			switch(event->message.command) {
				case SUCM_INIT:
					sui_widget_hevent(widget, event);
					sui_wscroll_init( widget, (sui_dc_t *)event->message.ptr);
					sui_clear_event( widget, event);
					return;
				case SUCM_GETSIZE:
					if ( sui_wscroll_get_size( (sui_dc_t *)event->message.ptr,
								widget, (sui_point_t *)event->message.info)>0) {
						sui_clear_event( widget, event);
					}
					break;

				case SUCM_STARTEDIT:
					if ( sui_scroll_start_edit( widget, event->message.info)>0) {
						sui_redraw_request( widget);
						sui_clear_event( widget, event);
					} 
					break;
				case SUCM_CONFIRM:
					if ( sui_scroll_stop_edit( widget, 1)>0) {
						sui_redraw_request( widget);
						sui_clear_event( widget, event);
					}
					break;
				case SUCM_DISCARD:
					if ( sui_scroll_stop_edit( widget, 0)>0) {
						sui_redraw_request( widget);
						sui_clear_event( widget, event);
					}
					break;
				case SUCM_FOCUSREL:
					if ( sui_test_flag( widget, SUFL_EDITED)) {
						sui_hevent_command( widget, SUCM_DISCARD, widget, 0);
					}
					break;
			}
	}
	if ( event->what != SUEV_NOTHING)
		sui_widget_hevent( widget,event);
}


/******************************************************************************
 * Scroll widget vmt & signal-slot mechanism
 ******************************************************************************/
int sui_wscroll_vmt_init( sui_widget_t *pw, void *params)
{
	pw->data = sui_malloc( sizeof( suiw_scroll_t));
	if(!pw->data)
		return -1;

	pw->type = SUWT_SCROLL;
	pw->hevent = sui_wscroll_hevent;

	return 0;
}

void sui_wscroll_vmt_done( sui_widget_t *pw)
{
	if(pw->data) {
		suiw_scroll_t *wscl = pw->data;

		if ( wscl->datai) sui_dinfo_dec_refcnt( wscl->datai);
		wscl->datai = NULL;
		if ( wscl->format) sui_finfo_dec_refcnt( wscl->format);
		wscl->format = NULL;

		free( wscl);
		pw->data = NULL;
	}
	pw->type = SUWT_GENERIC;
	pw->hevent = sui_widget_hevent; // NULL;
}


SUI_CANBE_CONST
sui_obj_signal_tinfo_t sui_wscroll_signal_tinfo[] = {
//	{ "statechanged", SUI_SIG_STATECHANGED, &sui_args_tinfo_int},
};

SUI_CANBE_CONST
sui_obj_slot_tinfo_t sui_wscroll_slot_tinfo[] = { 
  { "setbounds", SUI_SLOT_SET_BOUNDS, &sui_args_tinfo_long2,
    .target_kind = SUI_STGT_METHOD_OFFSET,
    .target.method_offset=UL_OFFSETOF( sui_wscroll_vmt_t, setbounds)},
  { "setvalue", SUI_SLOT_SET_VALUE, &sui_args_tinfo_long,
    .target_kind = SUI_STGT_METHOD_OFFSET,
    .target.method_offset=UL_OFFSETOF( sui_wscroll_vmt_t, setvalue)},
};

SUI_CANBE_CONST
sui_obj_tinfo_t sui_wscroll_vmt_obj_tinfo = {
  .name = "sui_wscroll",
  .obj_size = sizeof(sui_widget_t),
  .obj_align = UL_ALIGNOF(sui_widget_t),
  .signal_count = MY_ARRAY_SIZE(sui_wscroll_signal_tinfo),
  .signal_tinfo = UL_CAST_UNQ1(sui_obj_signal_tinfo_t *, sui_wscroll_signal_tinfo),
  .slot_count = MY_ARRAY_SIZE(sui_wscroll_slot_tinfo),
  .slot_tinfo = UL_CAST_UNQ1(sui_obj_slot_tinfo_t *, sui_wscroll_slot_tinfo)
};

sui_wscroll_vmt_t sui_wscroll_vmt_data = {
  .vmt_size = sizeof(sui_wscroll_vmt_t),
  .vmt_refcnt = SUI_STATIC,
  .vmt_parent = &sui_widget_vmt_data,
  .vmt_init = sui_wscroll_vmt_init,
  .vmt_done = sui_wscroll_vmt_done,
  .vmt_obj_tinfo = UL_CAST_UNQ1(sui_obj_tinfo_t *, &sui_wscroll_vmt_obj_tinfo),

  .setbounds = sui_wscroll_setbounds,
  .setvalue = sui_wscroll_setvalue,

};

/******************************************************************************/
/**
 * sui_wscroll - Create scroll widget
 * @aplace: Scroll position and size.
 * @atext: Scroll label in utf8 string.
 * @astyle: Scroll widget style.
 * @aflags: Widget common and scroll special flags.
 * @apullersize: Scroll puller width (in pixels).
 * @adata: Pointer to dinfo connected to scroll widget.
 * @aformat: Pointer to finfo connected to scroll widget.
 *
 * The function creates the scroll widget.
 *
 * Return Value: The function returns pointer to created scroll widget.
 * File: sui_scroll.c
 */
sui_widget_t *sui_wscroll( sui_rect_t *aplace, utf8 *alabel, sui_style_t *astyle, 
              sui_flags_t aflags, sui_dinfo_t *adata, sui_finfo_t *aformat,
              sui_coordinate_t apullersize, sui_coordinate_t alinesize)
{
	sui_widget_t *pw;

	pw = sui_widget_create_new( &sui_wscroll_vmt_data, aplace, alabel, astyle, aflags);

	if ( !pw) return NULL;

	{
		suiw_scroll_t *wscl = sui_scrwdg(pw);

		wscl->wpuller = apullersize;
		wscl->wline = alinesize;
		if ( adata) {
			sui_dinfo_inc_refcnt( adata);
			wscl->datai = adata;
		}
		if ( aformat) {
			sui_finfo_inc_refcnt( aformat);
			wscl->format = aformat;
		}
	}
	return pw;
}
