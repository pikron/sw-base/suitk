/* sui_wdyntext.c
 *
 * SUITK Dynamic text widget.
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#include "sui_wdyntext.h"
#include <suiut/namespace.h>

#include <ul_log.h>

extern unsigned long get_current_time( void);


UL_LOG_CUST(ulogd_suitk)


int sui_wdyntext_rtf_clear(sui_dyntext_rtfpart_t **head);
int sui_wdyntext_prepare_text(sui_widget_t *widget);
long sui_wdyntext_get_index(sui_widget_t *widget);

/******************************************************************************
 * Dynamic text widget slot functions
 ******************************************************************************/
/**
 * sui_wdyntext_update - updates drawn dynamic text
 */
int sui_wdyntext_update(sui_widget_t *widget)
{
  suiw_dyntext_t *wdyn = sui_dytwdg(widget);
  sui_wdyntext_rtf_clear(&wdyn->rtfhead);
  return sui_wdyntext_prepare_text(widget);
}

/**
 * sui_wdyntext_settext - set text into source of dynamic text
 */
int sui_wdyntext_settext(sui_widget_t *widget, utf8 *newtext, int index)
{
  suiw_dyntext_t *wdyn = sui_dytwdg(widget);
  long idx = sui_wdyntext_get_index(widget);
  if (!newtext) return -1;
  switch (widget->flags & SUYF_SRC_MASK) {
    case SUYF_SRC_UTF8:
      sui_utf8_inc_refcnt(newtext);
      sui_utf8_dec_refcnt(wdyn->data.text);
      wdyn->data.text = newtext;
      sui_wdyntext_update(widget);
      return 0;
    
    case SUYF_SRC_DINFO:
      if (sui_wr_utf8(wdyn->data.dinfo, index, &newtext)!= SUI_RET_OK)
        return -1;
      break;

    case SUYF_SRC_LIST:
      {
        sui_listcoord_t lidx = index;
        sui_list_item_t *li = sui_list_gavl_find(wdyn->data.list, &lidx);
        if (li) {
          sui_list_item_t *ni = sui_list_item_create(newtext, NULL, NULL, NULL);
          sui_list_item_change(wdyn->data.list, lidx, ni, SULICH_CONTENT);
          sui_list_item_destroy(ni);
        } else {
          if (sui_list_item_add( wdyn->data.list,
                sui_list_item_create(newtext, NULL, NULL, NULL), lidx)<0)
            return -1;
        }
      }
      break;
    default:
      return -1;
  }
  if (index==idx) {
    sui_wdyntext_update(widget);
  }
  return 0;
}

/******************************************************************************
 * Dynamic text auxiliary functions
 ******************************************************************************/
/**
 * sui_wdyntext_rtf_add - create and add new RTF part
 */
sui_dyntext_rtfpart_t *sui_wdyntext_rtf_add(sui_dyntext_rtfpart_t **head, utf8 *text,
                                      sui_list_t *list, sui_dinfo_t *divar, sui_image_t *image,
                                      sui_font_info_t *font, sui_textdims_t *tsize,
                                      unsigned short flags, sui_coordinate_t gap)
{
  sui_dyntext_rtfpart_t *newone, *last;
  newone = sui_malloc(sizeof(sui_dyntext_rtfpart_t));
  if (!newone) return NULL;
  newone->flags = (flags & ~SUDTX_RTF_CON_MASK);
  if (text) {
    sui_utf8_inc_refcnt(text);
    newone->content.str = text;
    newone->flags |= SUDTX_RTF_CON_TEXT;
  } else if (list) {
    sui_list_inc_refcnt(list);
    newone->content.dilist.list = list;
    sui_dinfo_inc_refcnt(divar);
    newone->content.dilist.didx = divar;
    newone->flags |= SUDTX_RTF_CON_LIST;
  } else if (divar) {
    sui_dinfo_inc_refcnt(divar);
    newone->content.dinfo = divar;
    newone->flags |= SUDTX_RTF_CON_DINFO;
  } else if (image) {
    sui_image_inc_refcnt(image);
    newone->content.image = image;
    newone->flags |= SUDTX_RTF_CON_IMAGE;
  } else {
    newone->flags |= SUDTX_RTF_CON_NONE;
  }
  if (font) {
    sui_fonti_inc_refcnt(font);
    newone->font = font;
  }
  if (tsize) {
    newone->size = *tsize;
    newone->flags |= SUDTX_RTF_MEASURED; /* FIXME: ? really ? */
  }
  newone->gap = gap;
  newone->next = NULL;

  if (*head) {
    last = *head;
    while(last->next)
      last=last->next;
    last->next = newone;
  } else
    *head = newone;

  return newone;
}

/**
 * sui_wdyntext_rtf_delete - clear and delete RTF part
 */
void sui_wdyntext_rtf_delete(sui_dyntext_rtfpart_t *rtf)
{
  if (!rtf) return;
  switch(rtf->flags & SUDTX_RTF_CON_MASK) {
    case SUDTX_RTF_CON_TEXT:
      if (rtf->content.str) sui_utf8_dec_refcnt(rtf->content.str);
      break;
    case SUDTX_RTF_CON_DINFO:
      if (rtf->content.dinfo) sui_dinfo_dec_refcnt(rtf->content.dinfo);
      break;
    case SUDTX_RTF_CON_IMAGE:
      if (rtf->content.image) sui_image_dec_refcnt(rtf->content.image);
      break;
    case SUDTX_RTF_CON_LIST:
      if (rtf->content.dilist.list) sui_list_dec_refcnt(rtf->content.dilist.list);
      if (rtf->content.dilist.didx) sui_dinfo_dec_refcnt(rtf->content.dilist.didx);
      break;
  }
  if (rtf->font) sui_fonti_dec_refcnt(rtf->font);

  free(rtf);
}

/**
 * sui_wdyntext_rtf_clear - clear all RTF parts from chain
 */
int sui_wdyntext_rtf_clear(sui_dyntext_rtfpart_t **head)
{
  sui_dyntext_rtfpart_t *cur, *next;
  cur = *head;
  while(cur) {
    next = cur->next;
    sui_wdyntext_rtf_delete(cur);
    cur = next;
  }
  *head = NULL;
  return SUI_RET_OK;
}

/******************************************************************************/

long sui_wdyntext_get_index(sui_widget_t *widget)
{
  long index;
  suiw_dyntext_t *wdyn = sui_dytwdg(widget);
  if ((widget->flags & SUYF_INDEXMASK) == SUYF_EXTINDEX) {
    if (sui_rd_long(wdyn->index.external, 0, &index)!=SUI_RET_OK)
      index = 0;
  } else {
    index = wdyn->index.internal;
  }
  return index;
}

int sui_wdyntext_change_index(sui_widget_t *widget)
{
  suiw_dyntext_t *wdyn = sui_dytwdg(widget);
  long index = 0;
  if ((widget->flags & SUYF_INDEXMASK) == SUYF_EXTINDEX) {
    if (sui_rd_long(wdyn->index.external, 0, &index)!=SUI_RET_OK)
      index = 0;
  } else {
    index = wdyn->index.internal;
  }

  switch (widget->flags & SUYF_SRC_MASK) {
    case SUYF_SRC_DINFO:
      if (index<(wdyn->data.dinfo->idxsize-1))
        index++;
      else
        index = 0;
      break;
    case SUYF_SRC_LIST:
      {
        sui_listcoord_t lidx = index;
        sui_list_item_t *li = sui_list_gavl_find(wdyn->data.list, &lidx);
        if (li) li = sui_list_gavl_next(wdyn->data.list,li);
        if (li) {
          index = li->idx;
        } else {
          li = sui_list_gavl_first(wdyn->data.list);
          if (li) index = li->idx;
        }
      }
      break;
  }

  if ((widget->flags & SUYF_INDEXMASK) == SUYF_EXTINDEX) {
    sui_wr_long(wdyn->index.external, 0, &index);
  } else {
    wdyn->index.internal = index;
  }
  return SUI_RET_OK;
}

/**
 * sui_wdyntext_parse_text2rtf - creates list of parts of RTF text
 * @text: pointer to utf8 text in TEX format
 * @eol_flag: if it is set, last RTF part will have set flag SUDTX_RTF_EOL
 * The function creates chain of RTF parts which contain text,image,font,...
 * Known marks : \\ - draw '\' in text
 *               \i{name} - insert image with 'name' in namespace
 *               \f{name} - set font to font with 'name' in namespace
 *               \f{} - switch font to normal (end of part with special font)
 *               \d{name} - insert content of dinfo 'name'
 *               \l{listname,dinfoname} - insert content of a list item with index in dinfo
 *               \inv{} - switch inversion flag
 *               \bl{} - switch blink flag
 *               \bli{} - switch inverse blink flag
 * Return Value:The function returns last RTF
 */
sui_dyntext_rtfpart_t *sui_wdyntext_parse_text2rtf(sui_dyntext_rtfpart_t **head, utf8 *text, int eol_flag)
{
  utf8char *str, *from, *to;
  sui_dyntext_rtfpart_t *lastrtf = NULL;
  sui_font_info_t *fnt = NULL;
  int len, pos, lastpos;
  int sepoffs[64]; /* in characters */
  char objname[64]; /* FIXME: namespace cannot work with name,length - it needs name ending with zero */
  int nsep = 0, add;
  int inv = 0, blink = 0;
  ns_object_t *nsobj;
  unsigned short set_flags;
  

  if (!text) return NULL;
  sui_utf8_inc_refcnt(text);
  str = sui_utf8_get_text(text);
  len = sui_utf8_length(text);
  from = str;
  lastpos = 0; add = 0;
/* find all separators */
  while (1) {
    if (lastpos>=len) break;
    pos = sui_utf8_nsearch(U8(from), U8"\\", 1);
    if (pos==-1) break;
    lastpos += pos + 1;

    pos = sui_utf8_count_bytes(from, pos);
    from += pos+1;
    if (nsep && pos==0) { /* '\\' - the last '\' is not a separator */
      nsep--; add = 1;
    } else {
      if (add) {
        sepoffs[nsep] = sepoffs[nsep] + pos + 2; 
        nsep++;
      } else {
        sepoffs[nsep++]=pos;
      }
      add = 0;
    }
    if (nsep==64) break; /* only 64 separators - ! buffer size */
  }

/* process parts ... */
//ul_logdeb("Number of parts = %d\n",nsep);
  pos = 0;
  from = str;
  do {
    if (pos<nsep) {
      lastpos = sepoffs[pos];
      if (pos) lastpos++;
    } else {
      lastpos = -1;
    }
    if (lastpos) {
      if (*from=='\\') {
        int ilen;
        to = strchr(from+3,'}');
        ilen = (to-(from+3));
        if (!strncmp(from+1,"i{",2)) {
//          ul_logdeb("Part - image name='%s'(l=%d)\n",from+3, ilen);
          if (ilen && ilen<63) {
            // font from namespace
            memcpy(objname,from+3,ilen);
            objname[ilen] = 0;
            nsobj = ns_find_any_object_by_type_and_name( SUI_TYPE_IMAGE, (const char *) objname);
            if (nsobj) {
              set_flags = (inv)?SUDTX_RTF_INVERTED:0;
              set_flags |= (blink==0)?0:((blink==1)?SUDTX_RTF_BLINKING:SUDTX_RTF_INVBLINK);
              lastrtf = sui_wdyntext_rtf_add(head, NULL, NULL, NULL, nsobj->ptr, NULL,
                                             NULL, set_flags, 0);
            }
          }
          lastpos -= to-from+1;
          from = to+1;
        } else if (!strncmp(from+1,"f{",2)) {
//          ul_logdeb("Part - font name='%s'(l=%d)\n",from+3,ilen);
          if (ilen && ilen<63) {
            // font from namespace
            memcpy(objname,from+3,ilen);
            objname[ilen] = 0;
            nsobj = ns_find_any_object_by_type_and_name( SUI_TYPE_FONTINFO, (const char *) objname);
            if (nsobj) {
              if (fnt) sui_fonti_dec_refcnt(fnt);
              sui_fonti_inc_refcnt(nsobj->ptr);
              fnt = nsobj->ptr;
            }
          } else {
            if (fnt) sui_fonti_dec_refcnt(fnt);
            fnt = NULL;
          }
          lastpos -= to-from+1;
          from = to+1;
        } else if (!strncmp(from+1,"d{",2)) {
//          ul_logdeb("Part - dinfo name='%s'(l=%d)\n",from+3, ilen);
          if (ilen && ilen<63) {
            // dinfo from namespace
            memcpy(objname,from+3,ilen);
            objname[ilen] = 0;
            nsobj = ns_find_any_object_by_type_and_name( SUI_TYPE_DINFO, (const char *) objname);
            if (nsobj) {
              set_flags = (inv)?SUDTX_RTF_INVERTED:0;
              set_flags |= (blink==0)?0:((blink==1)?SUDTX_RTF_BLINKING:SUDTX_RTF_INVBLINK);
              lastrtf = sui_wdyntext_rtf_add(head, NULL, NULL, nsobj->ptr, NULL, fnt,
                                             NULL, set_flags, 0);
            }
          }
          lastpos -= to-from+1;
          from = to+1;
        } else if (!strncmp(from+1,"l{",2)) {
//          ul_logtrash("Part - list,dinfo name='%s'(l=%d)\n",from+3, ilen);
          if (ilen && ilen<63) {
            char *pcom = strchr(from+3,',');
            int icom = (pcom) ? (pcom-from-3) : 0;
            if (icom && icom<ilen && icom<63) { /* it contains two part - list and dinfo */
            // list from namespace
              memcpy(objname,from+3,icom);
              objname[icom] = 0;
              nsobj = ns_find_any_object_by_type_and_name(SUI_TYPE_LIST, (const char *) objname);
              if (nsobj) {
                sui_list_t *plist = nsobj->ptr;
                ilen -= icom+1;
                if (ilen && ilen<63) {
                  memcpy(objname,from+3+icom+1,ilen);
                  objname[ilen] = 0;
                  nsobj = ns_find_any_object_by_type_and_name(SUI_TYPE_DINFO, (const char *) objname);
                  if (nsobj) {
                    set_flags = (inv)?SUDTX_RTF_INVERTED:0;
                    set_flags |= (blink==0)?0:((blink==1)?SUDTX_RTF_BLINKING:SUDTX_RTF_INVBLINK);
                    lastrtf = sui_wdyntext_rtf_add(head, NULL, plist, nsobj->ptr, NULL, fnt,
                                                   NULL, set_flags, 0);
                  }
                }
              }
            }
          }
          lastpos -= to-from+1;
          from = to+1;
        } else if (!strncmp(from+1,"inv{",4)) {
          inv = !inv;
          lastpos -= to-from+1;
          from = to+1;
        } else if (!strncmp(from+1,"bli{",4)) { /* must be before \bl{} */
          if (blink!=2) blink = 2;
          else blink = 0;
          lastpos -= to-from+1;
          from = to+1;
        } else if (!strncmp(from+1,"bl{",3)) {
          if (blink!=1) blink = 1;
          else blink = 0;
          lastpos -= to-from+1;
          from = to+1;
        } else if (!strncmp(from+1,"g{",2)) {
          ul_logdeb("Part - gap... TODO\n");
        }
      }
  // create and add new part
      if (lastpos) {
        utf8 *parttext;
        if (lastpos<0) {
          lastpos = -1; // FIXME: zjistit spravnou hodnotu poctu bytu na konci retezce ...
        }
//        ul_logdeb("Part #%d from '%s' len=%d\n",pos,from,lastpos);
        parttext = sui_utf8_dynamic(from, -1, lastpos);
        if (parttext) {
          set_flags = (inv)?SUDTX_RTF_INVERTED:0;
          set_flags |= (blink==0)?0:((blink==1)?SUDTX_RTF_BLINKING:SUDTX_RTF_INVBLINK);
          lastrtf = sui_wdyntext_rtf_add(head, parttext, NULL, NULL, NULL, fnt,
                                         NULL, set_flags, 0);
          sui_utf8_dec_refcnt(parttext);
        }
      }
    }
    from += lastpos;
  } while ((pos++)<nsep);

  if (eol_flag && lastrtf) lastrtf->flags |= SUDTX_RTF_EOL;
  if (fnt) sui_fonti_dec_refcnt(fnt);
  sui_utf8_dec_refcnt(text);
  return lastrtf;
}

/**
 * sui_wdyntext_prepare_text - prepares text for measurement and drawing
 * The function clear and create new RTF chain
 */
int sui_wdyntext_prepare_text(sui_widget_t *widget)
{
  suiw_dyntext_t *wdyn;
  utf8 *text = NULL;
  sui_dyntext_rtfpart_t *rtf;

  if (!widget) return SUI_RET_ERR;
  wdyn = sui_dytwdg(widget);

  if (wdyn->rtfhead) {
    sui_wdyntext_rtf_clear(&wdyn->rtfhead);
  }

  switch (widget->flags & SUYF_FNC_MASK) {
    case SUYF_FNC_HIDDEN:
      return SUI_RET_OK;

    case SUYF_FNC_DYNTEXT:
    case SUYF_FNC_STATIC:
      switch(widget->flags & SUYF_SRC_MASK) {
        case SUYF_SRC_UTF8:
          text = wdyn->data.text;
          sui_utf8_inc_refcnt(text);
          break;
        case SUYF_SRC_DINFO:
          {
            long index = sui_wdyntext_get_index(widget);
            if ((text = sui_dinfo_2_utf8(wdyn->data.dinfo, index, NULL, 0))==NULL)
              return SUI_RET_ERR;
          }
          break;
        case SUYF_SRC_LIST:
          {
            sui_listcoord_t lidx = sui_wdyntext_get_index(widget);
            sui_list_item_t *li = sui_list_gavl_find(wdyn->data.list, &lidx);
            if (sui_list_item_get_text(li, &text))
              return SUI_RET_ERR;
            break;
          }
      }
      if (text) {
        if (sui_wdyntext_parse_text2rtf(&wdyn->rtfhead, text, 0)==NULL)
          return SUI_RET_ERR;
        sui_utf8_dec_refcnt(text);
      }
      break;

    case SUYF_FNC_MULTILN:
      switch(widget->flags & SUYF_SRC_MASK) {
        case SUYF_SRC_UTF8:
          if (wdyn->data.text) {
            if (sui_wdyntext_parse_text2rtf(&wdyn->rtfhead, wdyn->data.text, 0)==NULL) return SUI_RET_ERR;
          }
          break;
        case SUYF_SRC_DINFO:
          if (wdyn->data.dinfo) {
            int idx = 0;
            while (idx < wdyn->data.dinfo->idxsize) {
              if ((text = sui_dinfo_2_utf8(wdyn->data.dinfo, idx, NULL, 0))==NULL)
                return SUI_RET_ERR;
              rtf = sui_wdyntext_parse_text2rtf(&wdyn->rtfhead, text, 1);
              sui_utf8_dec_refcnt(text);
              text = NULL;
              if (!rtf) return SUI_RET_ERR;
              idx++;
            }
          }
          break;
        case SUYF_SRC_LIST:
          if (wdyn->data.list) {
            sui_list_item_t *li = sui_list_gavl_first(wdyn->data.list);
            while (li) {
              if (sui_list_item_get_text(li, &text))
                return SUI_RET_ERR;
              rtf = sui_wdyntext_parse_text2rtf(&wdyn->rtfhead, text, 1);
              sui_utf8_dec_refcnt(text);
              text = NULL;
              if (!rtf) return SUI_RET_ERR;
              li = sui_list_gavl_next(wdyn->data.list,li);
            }
          }
          break;
      }
      break;
  }

  return SUI_RET_OK;
}

int sui_wdyntext_rtf_get_size(sui_dc_t *dc, sui_widget_t *widget, sui_dyntext_rtfpart_t *rtf, sui_textdims_t *dims)
{
  sui_dcfont_t *pdcfont;
  sui_textdims_t tdim;

  if (!rtf) return -1;
  if (!(rtf->flags & SUDTX_RTF_MEASURED) && dc) {
    if (rtf->font) {
      pdcfont = sui_font_prepare( dc, rtf->font->font,
                                  rtf->font->size);
    } else {
      pdcfont = sui_font_prepare( dc, widget->style->fonti->font,
                                  widget->style->fonti->size);
    }

    switch(rtf->flags & SUDTX_RTF_CON_MASK) {
      case SUDTX_RTF_CON_NONE:
        rtf->size.w = rtf->size.h = rtf->size.b = 0;
        break;
      case SUDTX_RTF_CON_TEXT:
        if (rtf->content.str) {
          sui_text_get_size( dc, pdcfont, &tdim, -1, rtf->content.str, sui_dytwdg(widget)->align);
          rtf->size = tdim;
        }
        rtf->flags |= SUDTX_RTF_MEASURED;
        break;
      case SUDTX_RTF_CON_DINFO:
        if (rtf->content.dinfo) {
          utf8 *text = NULL;
          if ((text = sui_dinfo_2_utf8(rtf->content.dinfo, 0, NULL, 0))!=NULL) {
            sui_text_get_size(dc, pdcfont, &tdim, -1, text, sui_dytwdg(widget)->align);
            rtf->size = tdim;
            sui_utf8_dec_refcnt(text);
            //rtf->flags |= SUDTX_RTF_MEASURED;
          }
        }
        break;
      case SUDTX_RTF_CON_IMAGE:
        if (rtf->content.image) {
          rtf->size.w = rtf->content.image->width;
          rtf->size.h = rtf->content.image->height;
          rtf->size.b = rtf->size.h;
        }
        rtf->flags |= SUDTX_RTF_MEASURED;
        break;
      case SUDTX_RTF_CON_LIST:
        if (rtf->content.dilist.list) {
          utf8 *text = NULL;
          sui_listcoord_t lidx = 0;
          sui_list_item_t *li = NULL;
          if (rtf->content.dilist.didx) {
            long idx;
            if (sui_rd_long(rtf->content.dilist.didx, 0, &idx) != SUI_RET_OK)
              break; /* error */
            lidx = idx;
          }
          li = sui_list_gavl_find(rtf->content.dilist.list, &lidx);
          if (li && !sui_list_item_get_text(li, &text) && text) {
            sui_text_get_size(dc, pdcfont, &tdim, -1, text, sui_dytwdg(widget)->align);
            rtf->size = tdim;
            sui_utf8_dec_refcnt(text);
            //rtf->flags |= SUDTX_RTF_MEASURED;
          }
        }
        break;
    }
  }
  dims->w = rtf->size.w;
  dims->h = rtf->size.h;
  dims->b = rtf->size.b;
  return 0;
}

/**
 * sui_wdyntext_rtf_get_line_size - get size of line
 */
int sui_wdyntext_rtf_get_line_size(sui_dc_t *dc, sui_widget_t *widget, sui_dyntext_rtfpart_t *rtf, sui_point_t *size)
{
  sui_textdims_t tdims;
  size->x = 0; size->y = 0;
  while (rtf) {
    if (sui_wdyntext_rtf_get_size(dc, widget, rtf, &tdims)<0) {
      tdims.w = tdims.h = tdims.b = 0;
    }
    if (size->y < tdims.h) size->y = tdims.h;
    size->x += tdims.w;
  
    size->x += rtf->gap;
    if (rtf->flags & SUDTX_RTF_EOL) break;
    rtf = rtf->next;
  }
  return SUI_RET_OK;
}


/**
 * sui_wdyntext_set_datasrc
 */
int sui_wdyntext_set_datasrc(sui_widget_t *wdg, void *ptr, unsigned long type)
{
  suiw_dyntext_t *wdyn;
  if (!wdg) return SUI_RET_ERR;
  wdyn = sui_dytwdg(wdg);
  if (!wdyn) return SUI_RET_ERR;

  switch(wdg->flags & SUYF_SRC_MASK) {
    case SUYF_SRC_UTF8:
      sui_utf8_dec_refcnt(wdyn->data.text);
      wdyn->data.text = NULL;
      break;
    case SUYF_SRC_DINFO:
      sui_dinfo_dec_refcnt(wdyn->data.dinfo);
      wdyn->data.dinfo = NULL;
      break;
    case SUYF_SRC_LIST:
      sui_list_dec_refcnt(wdyn->data.list);
      wdyn->data.list = NULL;
      break;
  }
  wdg->flags &= ~SUYF_SRC_MASK;

  switch(type & SUYF_SRC_MASK) {
    case SUYF_SRC_UTF8:
      sui_utf8_inc_refcnt(ptr);
      wdyn->data.text = ptr;
      wdg->flags |= SUYF_SRC_UTF8;
      break;
    case SUYF_SRC_DINFO:
      sui_dinfo_inc_refcnt(ptr);
      wdyn->data.dinfo = ptr;
      wdg->flags |= SUYF_SRC_DINFO;
      break;
    case SUYF_SRC_LIST:
      sui_list_inc_refcnt(ptr);
      wdyn->data.list = ptr;
      wdg->flags |= SUYF_SRC_LIST;
      break;
  }
  return SUI_RET_OK;
}

/**
 * sui_wdyntext_set_timesrc
 */
int sui_wdyntext_set_timesrc(sui_widget_t *wdg, sui_dinfo_t *dtime, long ctime)
{
  suiw_dyntext_t *wdyn;
  if (!wdg) return SUI_RET_ERR;
  wdyn = sui_dytwdg(wdg);
  if (!wdyn) return SUI_RET_ERR;
  if ((wdg->flags & SUYF_TIMEMASK) == SUYF_DYNTIME) {
    sui_dinfo_dec_refcnt(wdyn->time.dynamic);
    wdyn->time.dynamic = NULL;
  }
  wdg->flags &= ~SUYF_TIMEMASK;
  if (dtime) {
    sui_dinfo_inc_refcnt(dtime);
    wdyn->time.dynamic = dtime;
    wdg->flags |= SUYF_DYNTIME;
  } else {
    wdyn->time.constant = ctime;
    wdg->flags |= SUYF_CONSTIME;
  }
  return SUI_RET_OK;
}

/******************************************************************************
 * dynamic text basic functions
 ******************************************************************************/
/**
 * sui_wdyntext_get_size - return dynamic text content size
 * @dc: pointer to device context
 * @widget: pointer to text widget
 * @point: pointer to point structure
 */
int sui_wdyntext_get_size(sui_dc_t *dc, sui_widget_t *widget, sui_point_t *point)
{
  suiw_dyntext_t *wtxt = (suiw_dyntext_t *) widget->data;
  sui_point_t size;
  sui_coordinate_t sx, sy;
  sui_dyntext_rtfpart_t *rtf;
  sui_textdims_t tdim;

  if ( !point) return -1;
  point->x = 0; point->y = 0;
  if ( !widget || !widget->style || !widget->style->fonti) return -1;

  if (wtxt->rtfhead==NULL) {
    if (sui_wdyntext_prepare_text(widget) != SUI_RET_OK)
      return -1;
  }

/* compute size of content */
  size.x = 0; size.y = 0;
  sx = 0; sy = 0;
  rtf = wtxt->rtfhead;
  while (rtf) {
    if (sui_wdyntext_rtf_get_size(dc, widget, rtf, &tdim)<0) {
      tdim.w = tdim.h = tdim.b = 0;
    }

    sx += tdim.w;
    if (sy < tdim.h) sy = tdim.h;

    if ((rtf->flags & SUDTX_RTF_EOL) || !rtf->next) {
      if (sx > size.x) size.x = sx;
      size.y += sy;
      sy = 0; sx = 0;
    }
    rtf = rtf->next;
  }
  if (sy) size.y += sy; /* add last line height to content height */
  
  point->x = size.x; point->y = size.y;
  return 1;
}

/**
 * sui_wdyntext_auto_size
 */
int sui_wdyntext_auto_size(sui_dc_t *dc, sui_widget_t *widget)
{
  sui_point_t size;
  sui_widget_compute_size(dc, widget, &size);
  widget->place.w = size.x; widget->place.h = size.y;
  return 1;
}

/**
 * sui_wdyntext_init - Reaction to INIT dynamic text event
 * @widget: Pointer to dynamic text widget.
 * @event: Pointer to INIT event.
 *
 * The function initializes a dynamic text widget.
 *
 * Return Value: The function does not return a value.
 * File: sui_wdyntext.c
 */
void sui_wdyntext_init(sui_widget_t *widget, sui_dc_t *dc)
{
  suiw_dyntext_t *wtxt = sui_dytwdg(widget);
  long addtime = 1000;

  /* set default values */
  wtxt->align |= SUAL_INSIDE;
  if ((widget->flags & SUYF_TIMEMASK) == SUYF_DYNTIME)
    sui_rd_long(wtxt->time.dynamic,0,&addtime);
  else
    addtime = wtxt->time.constant;
  wtxt->nexttime = get_current_time()+addtime;

  if ((widget->flags & SUYF_INDEXMASK) == SUYF_INTINDEX)
    wtxt->index.internal = 0;

  if (widget->place.w<0 || widget->place.h<0) {
    sui_point_t size;
    sui_widget_compute_size(dc, widget, &size);
    if (widget->place.w < 0) widget->place.w = size.x;
    if (widget->place.h < 0) widget->place.h = size.y;
  }
}

/**
 * sui_wdyntext_draw - Reaction to DRAW text event
 * @dc: Pointer to device context.
 * @widget: Pointer to dynamic text widget.
 *
 * The function draws dynamic text widget.
 *
 * Return Value: The function returnsss -1/0/1 .
 * File: sui_text.c
 */
int sui_wdyntext_draw(sui_dc_t *dc, sui_widget_t *widget)
{
  suiw_dyntext_t *wtxt = sui_dytwdg(widget);
  sui_point_t            origin;
  sui_point_t            tsize;
  sui_point_t            box;
  sui_point_t            curpos;
  sui_dyntext_rtfpart_t *rtf;
  sui_coordinate_t       ycorr;

  box.x = widget->place.w; box.y = widget->place.h;
  sui_wdyntext_get_size(dc, widget, &tsize);
  sui_align_object(&box, &origin, &tsize, tsize.y, widget->style, wtxt->align);

/* draw content */
  rtf = wtxt->rtfhead;
  curpos.x = origin.x; curpos.y = origin.y;
/* is clipping box set ? */
  sui_wdyntext_rtf_get_line_size(dc, widget, rtf, &box);
  if (sui_test_flag(widget, SUYF_ROTATE)) {
    curpos.x += wtxt->offset;
  } else {
    switch (wtxt->align & SUAL_HORMASK) {
      case SUAL_LEFT: break;
      case SUAL_CENTER: curpos.x += (tsize.x - box.x)/2; break;
      case SUAL_RIGHT: curpos.x += (tsize.x-box.x); break;
    }
  }
  while(rtf) {
    unsigned short  rtc = rtf->flags & SUDTX_RTF_CON_MASK;
    utf8           *text = NULL;
    sui_dcfont_t   *pdcfont;
    sui_textdims_t  tdim;

    if (!(rtf->flags & SUDTX_RTF_GAP_AFTER)) {
      curpos.x += rtf->gap;
    }
    switch(rtc) {
      case SUDTX_RTF_CON_TEXT:
      case SUDTX_RTF_CON_DINFO:
      case SUDTX_RTF_CON_LIST:

    /* FIXME: set color according to rtf->flags */
        if (rtf->flags & SUDTX_RTF_INVERTED) {
          sui_set_widget_fgcolor(dc, widget, SUC_BACKGROUND); /* set colors */
          sui_set_widget_bgcolor(dc, widget, SUC_ITEM);
        } else {
          sui_set_widget_fgcolor(dc, widget, SUC_ITEM); /* set colors */
          sui_set_widget_bgcolor(dc, widget, SUC_BACKGROUND);
        }
        if (rtf->font) {
          sui_font_set(dc, pdcfont = sui_font_prepare(dc, rtf->font->font,
                                            rtf->font->size));
        } else {
          sui_font_set(dc, pdcfont = sui_font_prepare(dc, widget->style->fonti->font,
                                            widget->style->fonti->size));
        }

        /* get text */
        if (rtc==SUDTX_RTF_CON_TEXT) {
          text = rtf->content.str;
          sui_utf8_inc_refcnt(text);
          tdim.w = rtf->size.w;
        } else if (rtc==SUDTX_RTF_CON_DINFO) {
          text = sui_dinfo_2_utf8(rtf->content.dinfo, 0, NULL, 0);
          if (!text) break;
          sui_text_get_size(dc, pdcfont, &tdim, -1, text, wtxt->align);
        } else if (rtc==SUDTX_RTF_CON_LIST) {
          sui_listcoord_t lidx = 0;
          sui_list_item_t *li = NULL;
          if (rtf->content.dilist.didx) {
            long idx;
            if (sui_rd_long(rtf->content.dilist.didx, 0, &idx) != SUI_RET_OK)
              break; /* error */
            lidx = idx;
          }
          if ((li = sui_list_gavl_find(rtf->content.dilist.list, &lidx))==NULL)
            break;
          if (sui_list_item_get_text(li, &text) && !text)
            break;
          sui_text_get_size(dc, pdcfont, &tdim, -1, text, sui_dytwdg(widget)->align);
        } else
          break;

        ycorr = (box.y - rtf->size.h)/2;

        if (!((dc->dc_flags & SUDCF_BLINK_NOW) && (rtf->flags & SUDTX_RTF_BLINKING)) &&
            !(!(dc->dc_flags & SUDCF_BLINK_NOW) && (rtf->flags & SUDTX_RTF_INVBLINK))) {
          sui_draw_text(dc, curpos.x, curpos.y + ycorr, -1, text, wtxt->align);
          curpos.x += tdim.w;
          if (rtf->next && (rtf->flags & SUDTX_RTF_INVERTED) && (rtf->next->flags & SUDTX_RTF_INVERTED)) {
            sui_set_widget_fgcolor(dc, widget, SUC_ITEM);
            sui_draw_line(dc, curpos.x-1, curpos.y, curpos.x-1, curpos.y+rtf->size.h-1);
          }
        } else {
          curpos.x += tdim.w;
        }

        /* release text */
        sui_utf8_dec_refcnt(text); text = NULL;
        break;

      case SUDTX_RTF_CON_IMAGE:
        if (rtf->content.image) {
          ycorr = (box.y - rtf->size.h)/2;
          /* draw image */
          if (!((dc->dc_flags & SUDCF_BLINK_NOW) && (rtf->flags & SUDTX_RTF_BLINKING)) &&
              !(!(dc->dc_flags & SUDCF_BLINK_NOW) && (rtf->flags & SUDTX_RTF_INVBLINK))) {
            curpos.y += ycorr;
            sui_gdi_draw_image(dc, &curpos, rtf->content.image, NULL, 0,
                              (rtf->flags & SUDTX_RTF_INVERTED)?SUIO_INVERSE_IMAGE:0);
            curpos.y -= ycorr;
          }
          curpos.x += rtf->size.w;
        }
        break;
    }
    if (rtf->flags & SUDTX_RTF_GAP_AFTER) {
      curpos.x += rtf->gap;
    }

    /* correct offsets - if (rtf & EOL) go to next line */
    if (rtf->flags & SUDTX_RTF_EOL) {
      rtf = rtf->next;
      curpos.x = origin.x; curpos.y += box.y;
      sui_wdyntext_rtf_get_line_size(dc, widget, rtf, &box);
      /* align line ... */
      if (!sui_test_flag(widget, SUYF_ROTATE)) {
        switch (wtxt->align & SUAL_HORMASK) {
          case SUAL_LEFT: break;
          case SUAL_CENTER: curpos.x += (tsize.x - box.x)/2; break;
          case SUAL_RIGHT: curpos.x += (tsize.x-box.x); break;
        }
      }
    } else {
      rtf = rtf->next;
    }

  }

  return 1;
}

/******************************************************************************
 * Dynamic text event handler
 ******************************************************************************/
/**
 * sui_wdyntext_hevent - Widget event handler for dynamic text widget
 * @widget: Pointer to the dynamic text widget.
 * @event: Pointer to the event.
 *
 * The function responses to event for dynamic text widget or calls
 * basic event handler common for all widget.
 * Return Value: The function does not return a value.
 * File: sui_text.c
 */
void sui_wdyntext_hevent(sui_widget_t *widget, sui_event_t *event)
{
  suiw_dyntext_t *wdyn = sui_dytwdg(widget);
  unsigned long curtime = get_current_time();

/* FIXME: here and by this way ? - switch to time event ... */
  if (((widget->flags & SUYF_FNC_MASK)==SUYF_FNC_DYNTEXT) &&
       !(widget->flags & SUYF_NOSWITCH)) {
    if (wdyn->nexttime<curtime) {
      unsigned long newtime = 1000;
      // select new index
      sui_wdyntext_change_index(widget);
      sui_wdyntext_rtf_clear(&wdyn->rtfhead);

      if ((widget->flags & SUYF_TIMEMASK) == SUYF_DYNTIME) {
        sui_rd_ulong(wdyn->time.dynamic,0,&newtime);
      } else {
        newtime = wdyn->time.constant;
      }
      wdyn->nexttime = curtime + newtime;
      wdyn->offsettime = curtime + SUI_WDYNTEXT_ROTATION_FIRSTTIME;
      wdyn->offset = 0;
    }
  }
  if ((widget->flags & SUYF_ROTATE) && (wdyn->offsettime < curtime)) {
    switch(wdyn->align & SUAL_HORMASK) {
      case SUAL_RIGHT: wdyn->offset += SUI_WDYNTEXT_ROTATION_ADDOFFSET; break;
      case SUAL_LEFT: wdyn->offset -= SUI_WDYNTEXT_ROTATION_ADDOFFSET; break;
      case SUAL_CENTER:
      case SUAL_BLOCK:
        if (wdyn->offset < 0) wdyn->offset -= SUI_WDYNTEXT_ROTATION_ADDOFFSET;
        else wdyn->offset += SUI_WDYNTEXT_ROTATION_ADDOFFSET;
        break;
    }
  }

  switch(event->what) {
    case SUEV_DRAW:
    case SUEV_REDRAW:
      sui_widget_draw(event->draw.dc, widget, sui_wdyntext_draw);
      return;

    case SUEV_COMMAND:
      switch(event->message.command) {
        case SUCM_INIT:
          sui_widget_hevent(widget, event);
          sui_wdyntext_init(widget, (sui_dc_t *)event->message.ptr);
          sui_clear_event(widget, event);
          break;
        case SUCM_AUTORESIZE:
          sui_wdyntext_auto_size((sui_dc_t *)event->message.ptr, widget);
          sui_clear_event(widget, event);
          break;
        case SUCM_GETSIZE:
          if (sui_wdyntext_get_size((sui_dc_t *)event->message.ptr,widget,
                                    (sui_point_t *)event->message.info) > 0) {
            sui_clear_event( widget, event);
          }
          break;
        case SUCM_SETTEXT:
          if (sui_wdyntext_settext(widget, event->message.ptr, 0)>=0) {
            sui_clear_event( widget, event);
          }
          break;
      }
  }
  if (event->what != SUEV_NOTHING)
    sui_widget_hevent(widget, event);
}


/******************************************************************************
 * Dynamic text widget vmt & signal-slot mechanism
 ******************************************************************************/
int sui_wdyntext_vmt_init(sui_widget_t *pw, void *params)
{
  suiw_dyntext_t *wtxt;
  pw->data = sui_malloc(sizeof(suiw_dyntext_t));
  if(!pw->data) return -1;

  wtxt = pw->data;
  pw->type = SUWT_DYNTEXT;
  pw->hevent = sui_wdyntext_hevent;

  return 0;
}

void sui_wdyntext_vmt_done(sui_widget_t *pw)
{
  if (pw->data) {
    suiw_dyntext_t *wdyn = pw->data;

    switch(pw->flags & SUYF_SRC_MASK) {
      case SUYF_SRC_UTF8:
        sui_utf8_dec_refcnt(wdyn->data.text);
        wdyn->data.text = NULL;
        break;
      case SUYF_SRC_DINFO:
        sui_dinfo_dec_refcnt(wdyn->data.dinfo);
        wdyn->data.dinfo = NULL;
        break;
      case SUYF_SRC_LIST:
        sui_list_dec_refcnt(wdyn->data.list);
        wdyn->data.list = NULL;
        break;
    }
    if (sui_is_flag(pw,SUYF_INDEXMASK,SUYF_EXTINDEX)) {
      sui_dinfo_dec_refcnt(wdyn->index.external);
    }
    if (sui_is_flag(pw,SUYF_TIMEMASK,SUYF_DYNTIME)) {
      sui_dinfo_dec_refcnt(wdyn->time.dynamic);
    }

    if (wdyn->rtfhead) {
      sui_wdyntext_rtf_clear(&wdyn->rtfhead);
    }
    free( wdyn);
    pw->data = NULL;
  }
  pw->type = SUWT_GENERIC;
  pw->hevent = sui_widget_hevent;
}


SUI_CANBE_CONST
sui_obj_signal_tinfo_t sui_wdyntext_signal_tinfo[] = {
//      { "changed", SUI_SIG_CHANGED, &sui_args_tinfo_utf8},
};

SUI_CANBE_CONST
sui_obj_slot_tinfo_t sui_wdyntext_slot_tinfo[] = {
      { "update", SUI_SLOT_UPDATE, &sui_args_tinfo_void,
        .target_kind = SUI_STGT_METHOD_OFFSET,
        .target.method_offset=UL_OFFSETOF( sui_wdyntext_vmt_t, update)},
//       { "clear", SUI_SLOT_CLEAR, &sui_args_tinfo_void,
//         .target_kind = SUI_STGT_METHOD_OFFSET,
//         .target.method_offset=UL_OFFSETOF( sui_wtext_vmt_t, clear)},
      { "settext", SUI_SLOT_SETTEXT, &sui_args_tinfo_utf8_int,
        .target_kind = SUI_STGT_METHOD_OFFSET,
        .target.method_offset=UL_OFFSETOF( sui_wdyntext_vmt_t, settext)},
};

SUI_CANBE_CONST
sui_obj_tinfo_t sui_wdyntext_vmt_obj_tinfo = {
  .name = "sui_wdyntext",
  .obj_size = sizeof(sui_widget_t),
  .obj_align = UL_ALIGNOF(sui_widget_t),
  .signal_count = MY_ARRAY_SIZE(sui_wdyntext_signal_tinfo),
  .signal_tinfo = UL_CAST_UNQ1(sui_obj_signal_tinfo_t *, sui_wdyntext_signal_tinfo),
  .slot_count = MY_ARRAY_SIZE(sui_wdyntext_slot_tinfo),
  .slot_tinfo = UL_CAST_UNQ1(sui_obj_slot_tinfo_t *, sui_wdyntext_slot_tinfo)
};

sui_wdyntext_vmt_t sui_wdyntext_vmt_data = {
  .vmt_size = sizeof(sui_wdyntext_vmt_t),
  .vmt_refcnt = SUI_STATIC,
  .vmt_parent = &sui_widget_vmt_data,
  .vmt_init = sui_wdyntext_vmt_init,
  .vmt_done = sui_wdyntext_vmt_done,
  .vmt_obj_tinfo = UL_CAST_UNQ1(sui_obj_tinfo_t *, &sui_wdyntext_vmt_obj_tinfo),

  .update = sui_wdyntext_update,
//   .clear = sui_wtext_clear,
  .settext = sui_wdyntext_settext,
};


/******************************************************************************/
/**
 * sui_wdyntext - Create dynamic text widget
 * @aplace: Text position and size.
 * @aname: Text label in utf8 string.
 * @astyle: Text widget style.
 * @aflags: Widget common and text special flags.
 * @atext: Text string in utf8.
 * @amax_len: Maximal length in characters if the atext is NULL(create new string).
 * @ashowed: Count of text character which are showed or -1.
 * @aalign: Align text string to widget.
 *
 * The function creates the text widget.
 *
 * Return Value: The function returns pointer to created text widget.
 * File: sui_text.c
 */
sui_widget_t *sui_wdyntext( sui_rect_t *aplace, utf8 *alabel, sui_style_t *astyle,
                         sui_flags_t aflags, sui_dinfo_t *adi, sui_finfo_t *aformat, unsigned char aalign)
{
  sui_widget_t *pw = sui_widget_create_new( &sui_wtext_vmt_data, aplace,
                      alabel, astyle, aflags);
  if ( !pw) return NULL;

  {
    suiw_dyntext_t *wtxt = sui_dytwdg(pw);

    wtxt->align = aalign;
/*    wtxt->begsel = -1;
    wtxt->cursor = 0;
    if ( adi)
    {
      sui_dinfo_inc_refcnt( adi);
      wtxt->di = adi;
    }
    if ( aformat)
    {
      sui_finfo_inc_refcnt( aformat);
      wtxt->format = aformat;
    }

    wtxt->mk_mode = SUKM_CAPITALS;
*/
  }
  return pw;
}
