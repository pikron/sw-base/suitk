/* sui_wtable.h
 *
 * SUITK table widget.
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#ifndef _SUI_TABLE_WIDGET_
#define _SUI_TABLE_WIDGET_

#include <suitk/suitk.h>

/**
 * struct sui_table_column - table column structure
 * @title: pointer to column title
 * @format: pointer to finfo structure - it describes how will be data showed
 * @data: pointer to dinfo structure
 * @span: pointer to dinfo structure - if it is set dinfo says for each index how many columns will be spanned for this column and row.
 * @font: pointer to column font
 * @align: field in column will be align as $align describes
 * @width: width of the column
 * @height: height of fields in the column (no title)
 */
typedef struct sui_table_column {
  utf8            *title;
  sui_finfo_t     *format;
  sui_dinfo_t     *data;
  sui_dinfo_t     *span;
  sui_list_t      *list;
  sui_font_info_t *font;
  sui_align_t      align;
  sui_coordinate_t width;
  sui_coordinate_t height;
/*  sui_flags_t flags; // visible, sort, ...  */
// utf8 *emptytext;

//  struct sui_table_column *prev;
  struct sui_table_column *next;
} sui_table_column_t;

/* FIXME: change table column in table to ul_list */
/**
 * struct sui_table - table structure
 * @refcnt: table reference counter
 * @maxidx: maximal index in table (from all columns)
 * @colnum: number of columns
 */
typedef struct sui_table {
  sui_refcnt_t        refcnt;
  sui_listcoord_t     maxidx;
  sui_listcoord_t     colnum;

  sui_table_column_t *firstcol;
} sui_table_t;

sui_table_column_t *sui_table_column_create(utf8 *atitle, sui_finfo_t *aformat,
                                    sui_dinfo_t *aspan, sui_dinfo_t *adata, sui_list_t *alist,
                                    sui_font_info_t *afont, sui_align_t aalign,
                                    sui_coordinate_t awidth, sui_coordinate_t aheight);
sui_table_column_t *sui_table_column_create_copy(sui_table_column_t *fromcol);
void sui_table_column_done(sui_table_column_t *tcol);
int sui_table_column_destroy(sui_table_column_t *tcol);
sui_refcnt_t sui_table_inc_refcnt(sui_table_t *tab);
sui_refcnt_t sui_table_dec_refcnt(sui_table_t *tab);
sui_table_t *sui_table_create(void);
void sui_table_clear(sui_table_t *tab);
int sui_table_add_column(sui_table_t *tab, sui_table_column_t *column, sui_listcoord_t idx);
int sui_table_del_column(sui_table_t *tab, sui_listcoord_t idx);
int sui_table_column_item_get_text(sui_table_column_t *column,
                                   sui_listcoord_t row, utf8 **buf);
int sui_table_item_get_text(sui_table_t *tab, sui_listcoord_t colidx,
                                 sui_listcoord_t rowidx, utf8 **buf);


/**
 * struct suiw_table - table widget data structure
 * @rows: number of visible rows in table
 * @cols: number of visible columns in table
 * @tdata: pointer to table structure describing individual columns data sources
 * @currow: pointer to dinfo with current (selected) position row
 * @curcol: pointer to dinfo with current (selected) column
 * @frow: index of first visible table row
 * @fcol: index of first visible table column
 * @htitle: height of title row
 * @hrow: height of all rows - get as maximum of heights form all columns
 *
 */
typedef struct suiw_table {
  sui_listcoord_t   rows;
  sui_listcoord_t   cols;

  sui_table_t      *tdata;

  sui_dinfo_t      *currow;
  sui_dinfo_t      *curcol;
  sui_listcoord_t   frow;
  sui_listcoord_t   fcol;
  sui_coordinate_t  htitle;
  sui_coordinate_t  hrow;
} suiw_table_t;

#define sui_tabwdg( wdg)  ((suiw_table_t *)wdg->data)

/**
 * enum table_flags - Table widget special flags
 * @SUAF_SELECT_NONE: Type of selection - items in the table cannot be selected
 * @SUAF_SELECT_COL: Type of selection - entire column is always selected
 * @SUAF_SELECT_ROW: Type of selection - entire row is always selected
 * @SUAF_SELECT_ONE: Type of selection - each item can be selected
 * @SUAF_SELECT_ALWAYS: Selection is shown always - with/without focus
 * @SUAF_DIV_ROWS: Show delimiting line between all rows
 * @SUAF_DIV_COLS: Show delimiting line between all columns
 * @SUAF_DIV_GRID: Show delimiting line between all columns and rows (grid is shown)
 * @SUAF_TITLES: There are column's titles in the first row in the table.
 * @SUAF_ROTATED: Rows are columns and columns are rows.
 * @SUAF_AUTOINDEX: In the first column in the table will be shown row index.
 * @SUAF_HIDEEMPTY: Hide empty rows, where all columns contain zeros.
 */
enum table_flags {
  SUAF_SELECT_NONE= 0x00000000,
  SUAF_SELECT_COL = 0x00010000,
  SUAF_SELECT_ROW = 0x00020000,
  SUAF_SELECT_ONE = 0x00030000,
  SUAF_SELECT_MODE= 0x00070000,
  SUAF_VIEW_SELECTED= 0x00080000,

  SUAF_DIV_ROWS   = 0x00100000,
  SUAF_DIV_COLS   = 0x00200000,
  SUAF_DIV_GRID   = 0x00300000,

  SUAF_TITLES     = 0x01000000,
  SUAF_ROTATED    = 0x02000000,
  SUAF_AUTOINDEX  = 0x04000000,
  SUAF_HIDEEMPTY  = 0x08000000,

 SUAF_CHANGE_FOCUS= 0x10000000, /* for non-looping moves out of bounds in a table it invokes focus changing */
 SUAF_AUTO_DRANGE = 0x20000000, /* automatic number of lines according to data range (idxsize of the first dinfo) */
};


/******************************************************************************
 * Table slot functions
 ******************************************************************************/
int sui_wtable_update_maxidx(sui_widget_t *widget);

/******************************************************************************
 * Table widget vmt & signal-slot mechanism
 ******************************************************************************/
typedef sui_widget_t sui_wtable_t;

#define sui_wtable_vmt_fields(new_type, parent_type) \
        sui_widget_vmt_fields( new_type, parent_type)

typedef struct sui_wtable_vmt {
  sui_wtable_vmt_fields(sui_wtable, sui_widget)
} sui_wtable_vmt_t;

static inline sui_wtable_vmt_t *
sui_wtable_vmt(sui_widget_t *self)
{
  return (sui_wtable_vmt_t*)(self->base.vmt);
}

extern sui_wtable_vmt_t sui_wtable_vmt_data;

/******************************************************************************
 * Table widget basic functions
 ******************************************************************************/
sui_widget_t *sui_wtable(sui_rect_t *aplace, utf8 *alabel, sui_style_t *astyle,
              sui_flags_t aflags, sui_table_t *atab, sui_dinfo_t *apos,
              sui_listcoord_t arows);

#endif /* _SUI_TABLE_WIDGET_ */
