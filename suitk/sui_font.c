/* sui_font.c
 *          
 * SUITK builtin fonts. 
 * This replaces functions from MicroWindows gen_font 
 * and works only with UTF8 strings.
 *
 * Compound fonts are compounded from several parts. In
 * followed functions many fields of font structure are
 * get from first piece of font.
 * ??? Maybe fixed width will be check in all pieces. It
 * means that some pieces can have fixed width and some can
 * have variable width. ???
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#include "sui_font.h"
#include <suiut/support.h>

/******************************************************************************
 * BuildIn system font X5x7 - this font is main system font
 ******************************************************************************/
//extern MWCFONT font_X5x7;
//MWCOREFONT systemfont_X5x7 = {&sui_fontprocs, 0, 0, 0, "X5x7", &font_X5x7};


/**
 * sui_fonti_inc_refcnt - Increment fonti reference counter
 * @fonti: Pointer to font information structure.
 *
 * The function increments font information structure 
 * reference counter.
 * Return Value: The function does not return a value.
 * File: sui_base.c
 */
sui_refcnt_t sui_fonti_inc_refcnt( sui_font_info_t *fonti)
{
  if (!fonti) return SUI_REFCNTERR;
  if (fonti->refcnt>=0) fonti->refcnt++;
  return fonti->refcnt;
}

/**
 * sui_fonti_dec_refcnt - Decrement fonti reference counter
 * @fonti: Pointer to font information structure.
 *
 * The function decrements font information structure reference 
 * counter. If the counter is decremented to zero
 * fonti structure will be deleted.
 * Return Value: The function does not return a value.
 * File: sui_base.c
 */
sui_refcnt_t sui_fonti_dec_refcnt( sui_font_info_t *fonti)
{
  sui_refcnt_t ref;
  if (!fonti) return SUI_REFCNTERR;
  if(fonti->refcnt>0) fonti->refcnt--;
  if(!(ref=fonti->refcnt)) {
    sui_utf8_dec_refcnt(fonti->font);
    fonti->font = NULL;
    sui_obj_done_vmt((sui_obj_t *)fonti, UL_CAST_UNQ1(sui_obj_vmt_t *, &sui_fontinfo_vmt_data));
    free(fonti);
  }
  return ref;
}


/**
 * sui_fonti_create - Create dynamic font information structure
 * @afont: Font name.
 * @asize: Font size (in pixels).
 * @aflag: Font flags (rotate,mirror,...).
 *
 * The function creates dynamic reference counting
 * font information structure, fill it and increment reference 
 * counter.
 * !!!  In this version field flag isn't used.
 * Return Value: The function returns pointer to created font
 * information structure or NULL.
 * File: sui_base.c
 */
sui_font_info_t *sui_fonti_create( sui_font_t afont, int asize, unsigned short aflag)
{
	sui_font_info_t *pfi = sui_malloc( sizeof(sui_font_info_t));
	if ( pfi) {
		sui_fonti_inc_refcnt( pfi);
		pfi->font = afont;
		pfi->size = asize;
		pfi->flag = aflag;
	}
	return pfi;
}

/******************************************************************************
 * Compound fonts
 ******************************************************************************/
/* fontproc struct for compound fonts */
  MWFONTPROCS sui_comp_fontprocs = {
   #if (MICROWINDOWS_VER_CODE>=VER_CODE(0,93,0))
    0,
   #endif /*MICROWINDOWS_VER_CODE>=VER_CODE(0,93,0)*/
    MWTF_UTF8,
   #if (MICROWINDOWS_VER_CODE>=VER_CODE(0,93,0))
    NULL,
    NULL,
   #endif /*MICROWINDOWS_VER_CODE>=VER_CODE(0,93,0)*/
    sui_font_comp_getfontinfo,
    sui_font_comp_gettextsize,
    sui_font_comp_gettextbits,
    sui_font_unloadfont,
    sui_font_drawtext,
    NULL,
    NULL,
    NULL,
  };
  
/* fontproc struct for one piece font */
  MWFONTPROCS sui_fontprocs = {
   #if (MICROWINDOWS_VER_CODE>=VER_CODE(0,93,0))
    0,
   #endif /*MICROWINDOWS_VER_CODE>=VER_CODE(0,93,0)*/
    MWTF_UTF8,
   #if (MICROWINDOWS_VER_CODE>=VER_CODE(0,93,0))
    NULL,
    NULL,
   #endif /*MICROWINDOWS_VER_CODE>=VER_CODE(0,93,0)*/
    sui_font_getfontinfo,
    sui_font_gettextsize,
    sui_font_gettextbits,
    sui_font_unloadfont,
    sui_font_drawtext,
    NULL,
    NULL,
    NULL,
  };


#if ((MICROWINDOWS_VER_CODE>=VER_CODE(0,93,0)) || (MICROWINDOWS_VER_CODE<VER_CODE(0,90,0)))

/* !!! REPLACE MICROWINDOWS GdSetFont, GdText b/c They can not work with len == -1 */
static PMWFONT sui_gr_pfont;            /* current font*/

/**
 * suiGdSetFont - Replace function GdSetFont from MICROWINDOWS
 * @pfont: Pointer to MicroWindows font structure.
 *
 * For more information see into MicroWindows help.
 * Return Value: The function returns pointer to old MW font structure.
 * File: sui_font.c
 */
  PMWFONT suiGdSetFont(PMWFONT pfont) {
    PMWFONT oldfont = sui_gr_pfont;
    sui_gr_pfont = pfont;
    return oldfont;
  }
#endif /*MICROWINDOWS_VER_CODE>=VER_CODE(0,93,0)*/

#if (MICROWINDOWS_VER_CODE>=VER_CODE(0,93,0))
  void suiGdText( PSD psd, MWCOORD x, MWCOORD y, const void *str, int cc, MWTEXTFLAGS flags) {
    if ( cc == -1)
      cc = sui_utf8_length( str);
    GdText( psd, sui_gr_pfont, x, y, sui_utf8_get_text(str), cc, flags);
  }
#endif /*MICROWINDOWS_VER_CODE>=VER_CODE(0,93,0)*/
  
#if (MICROWINDOWS_VER_CODE<VER_CODE(0,90,0))
/**
 * suiGdText - Replace function GdText from MICROWINDOWS
 * @psd: Pointer to MicroWindows Screen Device structure.
 * @x: Horizontal coordinate of text.
 * @y: Vertical coordinate of text.
 * @str: Pointer to text.
 * @cc: Length of text or -1.
 * @flags: Text flags with 'MWTF_' prefix.
 *
 * For more information see into MicroWindows help.
 * Return Value: The function does not return a value.
 * File: sui_font.c
 */
  void suiGdText( PSD psd, MWCOORD x, MWCOORD y, const void *str, int cc, MWTEXTFLAGS flags) {
    if((flags & MWTF_PACKMASK) != sui_gr_pfont->fontprocs->encoding)
      return;  
    if( cc == -1 && (flags & MWTF_PACKMASK) == MWTF_ASCII)
      cc = strlen((char *)str);
    if( cc == 0 || !sui_gr_pfont->fontprocs->DrawText)
      return;
    sui_gr_pfont->fontprocs->DrawText( sui_gr_pfont, psd, x, y, str, cc, flags);
  }
  
/**
 * suiGdGetTextSize - Replace function GdGetTextSize from MICROWINDOWS
 * @pfont: Pointer to MicroWindows font structure.
 * @str: Pointer to text.
 * @cc: Length of text or -1.
 * @pwidth: Pointer for output of text width.
 * @pheight: Pointer for output of text height.
 * @pbase: Pointer for output of text baseline.
 * @flags: Text flags with 'MWTF_' prefix.
 *
 * In SUITK the MWTF_COLUMN_TEXT flag is added for drawing text in column, but
 * flags aren't given to lower function, so this function set this flag into
 * pbase argument.
 * For more information see into MicroWindows help.
 * Return Value: The function does not return a value.
 * File: sui_font.c
 */
  void suiGdGetTextSize( PMWFONT pfont, const void *str, int cc, MWCOORD *pwidth,
                         MWCOORD *pheight, MWCOORD *pbase, MWTEXTFLAGS flags) {
    if((flags & MWTF_PACKMASK) != pfont->fontprocs->encoding)
      return;  
    if( cc == -1 && (flags & MWTF_PACKMASK) == MWTF_ASCII)
      cc = strlen((char *)str);  
    if( cc == 0 || !pfont->fontprocs->GetTextSize) {
      *pwidth = *pheight = *pbase = 0;
      return;
    }
    *pbase = (flags & MWTF_COLUMN_TEXT) ? SUI_COLUMN_TEXT : 0;
    pfont->fontprocs->GetTextSize( pfont, str, cc, pwidth, pheight, pbase);
  }
  
/* !!! end of REPLACE MICROWINDOWS ... */
#endif /*(MICROWINDOWS_VER_CODE<VER_CODE(0,90,0))*/

/**
 * sui_font_comp_getfontinfo - Get FONTINFO of compound fonts
 * @pfont: pointer to compound font
 * @pfontinfo: pointer to output font info structure
 *
 * The function fills MWFONTINFO by information about coumpound font.
 * The function replaces MicroWindows function 'gen_getfontinfo' for
 * compound font.
 * Return Value: The function returns TRUE for success.
 * File: sui_font.c
 */
  MWBOOL sui_font_comp_getfontinfo( PMWFONT pfont, PMWFONTINFO pfontinfo) {
    MWCFONT **ppf = (MWCFONT **) ((PMWCOREFONT)pfont)->cfont;
    MWCFONT *pfnt;
//    int i;

    if ( *ppf) { // many of fields are filled from first part of font
      pfnt = *ppf;
      pfontinfo->height = pfnt->height; 
      pfontinfo->baseline = pfnt->ascent;
      pfontinfo->fixed = (pfnt->width == NULL) ? TRUE : FALSE;
      pfontinfo->maxwidth = pfnt->maxwidth; 
      pfontinfo->firstchar = pfnt->firstchar; 
      pfontinfo->lastchar = pfnt->firstchar+pfnt->size-1;
      ppf++;
      while ((pfnt=*ppf)) {
        if ( pfnt->maxwidth > pfontinfo->maxwidth) 
          pfontinfo->maxwidth = pfnt->maxwidth;
        if ( pfnt->firstchar < pfontinfo->firstchar) 
          pfontinfo->firstchar = pfnt->firstchar;
        if ( pfnt->firstchar + pfnt->size - 1 > pfontinfo->lastchar) 
          pfontinfo->lastchar = pfnt->firstchar + pfnt->size - 1;
        ppf++;
      }
      return TRUE;
    }
    return FALSE;
  }

/**
 * sui_font_getfontinfo - Get FONTINFO of one piece fonts
 * @pfont: pointer to one piece font
 * @pfontinfo: pointer to output font info structure
 *
 * The function fills MWFONTINFO by information about font.
 * The function replaces MicroWindows function 'gen_getfontinfo' for
 * normal font.
 * Return Value: The function returns TRUE for success.
 * File: sui_font.c
 */
  MWBOOL sui_font_getfontinfo( PMWFONT pfont, PMWFONTINFO pfontinfo) {
    MWCFONT *pfnt = (MWCFONT *) ((PMWCOREFONT)pfont)->cfont;

    if ( pfnt) { // many of fields are filled from first part of font
      pfontinfo->height = pfnt->height; 
      pfontinfo->baseline = pfnt->ascent;
      pfontinfo->fixed = (pfnt->width == NULL) ? TRUE : FALSE;
      pfontinfo->maxwidth = pfnt->maxwidth; 
      pfontinfo->firstchar = pfnt->firstchar; 
      pfontinfo->lastchar = pfnt->firstchar+pfnt->size-1;
      return TRUE;
    }
    return FALSE;
  }

    
    
#if (MICROWINDOWS_VER_CODE>=VER_CODE(0,90,0))
/**
 * sui_font_comp_gettextsize - Calculate bounding box of text(utf8) for compound fonts
 * @pfont: pointer to compound font
 * @text: pointer to ascii text or utf8 string (text/structure)
 * @chars: count of characters to write from text
 * @pwidth: pointer to output width result
 * @pheight: pointer to output height result
 * @pbase: pointer to output base result
 *
 * The function calculates bounding box around text writed with 'pfont'.
 * Results are returned in 'pwidth','pheight' and 'pbase'.
 * The 'pbase' has SUI_COLUMN_TEXT value for column strings on entry.
 *
 * Return Value: The function does not return a value.
 * File: sui_font.c
 */
  void sui_font_comp_gettextsize( PMWFONT pfont, const void *text, int chars, MWTEXTFLAGS flags,
                                  MWCOORD *pwidth, MWCOORD *pheight, MWCOORD *pbase) 
#else /* MICROWINDOWS_VER_CODE<VER_CODE(0,90,0) */
  void sui_font_comp_gettextsize( PMWFONT pfont, const void *text, int chars,
                                  MWCOORD *pwidth, MWCOORD *pheight, MWCOORD *pbase) 
  #define flags *pbase
#endif /* MICROWINDOWS_VER_CODE<VER_CODE(0,90,0) */
  {    
    MWCFONT **pcf, **ppf = (MWCFONT **) ((PMWCOREFONT)pfont)->cfont;
    MWCFONT *pfnt = *ppf; // current piece of font
    utf8 *utf = (utf8 *)text;
    utf8char *utxt;
    int width;
    int cwidth;
  #if UTF8_MAX_BYTES_IN_CHAR == 6 // unicode32
    unsigned long ucs;
  #else                           // unicode16
    unsigned short ucs;
  #endif
  
    if ( chars < 0) chars = sui_utf8_length( utf);
    
    // HERE will be change for combined fixed and variable pieces of font.
    if ( flags & MWTF_COLUMN_TEXT) {
      *pheight = pfnt->height * chars;
      *pbase = 0;
      if ( pfnt->width == NULL) {
        width = pfnt->maxwidth;
      } else {
        width = 0;
        utxt = sui_utf8_get_text(utf);
        while( chars-- > 0) {
          utxt += sui_utf8_to_ucs( utxt, &ucs);
        // search piece of font
          pcf = ppf;
          while((pfnt=*pcf)) {
            if( ucs >= pfnt->firstchar && ucs < pfnt->firstchar+pfnt->size) {
	      if(pfnt->width != NULL)
		cwidth = pfnt->width[ucs - pfnt->firstchar];
	      else
		cwidth = pfnt->maxwidth;
              if ( width < cwidth)
                width = cwidth;
              break;
            }
            pcf++;
          }
        // end of search piece          
        }
      }
      *pwidth = width;      
    } else {
      *pheight = pfnt->height;
      *pbase = pfnt->ascent;
      if( pfnt->width == NULL) { // fixed width
          width = chars * pfnt->maxwidth;
      } else {
        width = 0;
        utxt = sui_utf8_get_text(utf);
        while( chars-- > 0) {        
          utxt += sui_utf8_to_ucs( utxt, &ucs);                
        // search piece of font
          pcf = ppf;
          while((pfnt=*pcf)) {
            if( ucs >= pfnt->firstchar && ucs < pfnt->firstchar+pfnt->size) {
	      if(pfnt->width != NULL)
		cwidth = pfnt->width[ucs - pfnt->firstchar];
	      else
		cwidth = pfnt->maxwidth;
              width += cwidth;
              break;
            }
            pcf++;
          }
        // end of search piece          
        }
      }
      *pwidth = width;
    }
  }

#if (MICROWINDOWS_VER_CODE>=VER_CODE(0,90,0))
/**
 * sui_font_gettextsize - Calculate bounding box of text(utf8) for one piece fonts
 * @pfont: pointer to one piece font
 * @text: pointer to ascii text or utf8 string (text/structure)
 * @chars: count of characters to write from text
 * @pwidth: pointer to output width result
 * @pheight: pointer to output height result
 * @pbase: pointer to output base result
 *
 * The function calculates bounding box around text writed with 'pfont'.
 * Results are returned in 'pwidth','pheight' and 'pbase'.
 * The 'pbase' has SUI_COLUMN_TEXT value for column strings on entry.
 *
 * Return Value: The function does not return a value.
 * File: sui_font.c
 */
  void sui_font_gettextsize( PMWFONT pfont, const void *text, int chars, MWTEXTFLAGS flags,
                             MWCOORD *pwidth, MWCOORD *pheight, MWCOORD *pbase)
#else /* MICROWINDOWS_VER_CODE<VER_CODE(0,90,0) */
  void sui_font_gettextsize( PMWFONT pfont, const void *text, int chars,
                             MWCOORD *pwidth, MWCOORD *pheight, MWCOORD *pbase)
  #define flags *pbase
#endif /* MICROWINDOWS_VER_CODE<VER_CODE(0,90,0) */
   {
    MWCFONT *pfnt = (MWCFONT *) ((PMWCOREFONT)pfont)->cfont;
    utf8 *utf = (utf8 *)text;
    utf8char *utxt;
    int width;
  #if UTF8_MAX_BYTES_IN_CHAR == 6 // unicode32
    unsigned long ucs;
  #else                           // unicode16
    unsigned short ucs;
  #endif  

    if ( chars < 0) chars = sui_utf8_length( utf);
    
    // HERE will be change for combined fixed and variable pieces of font.
    
    if ( flags & MWTF_COLUMN_TEXT) {
      *pheight = pfnt->height * chars;
      *pbase = 0;
      if ( pfnt->width == NULL) { // fixed width
        width = pfnt->maxwidth;
      } else {
        width = 0;
        utxt = sui_utf8_get_text(utf);
        while( chars-- > 0) {        
          utxt += sui_utf8_to_ucs( utxt, &ucs);
          if ( width < pfnt->width[ucs - pfnt->firstchar])
            width = pfnt->width[ucs - pfnt->firstchar];
        }
      }      
      *pwidth = width;
    } else {    
      *pheight = pfnt->height;
      *pbase = pfnt->ascent;
      if ( pfnt->width == NULL) { // fixed width
        width = chars * pfnt->maxwidth;
      } else {
        width = 0;
        utxt = sui_utf8_get_text(utf);
        while( chars-- > 0) {        
          utxt += sui_utf8_to_ucs( utxt, &ucs);                
          width += pfnt->width[ucs - pfnt->firstchar];
        }
      }
      *pwidth = width;
    }
  }
  
  
/**
 * sui_font_unloadfont - Unload compound/one piece font
 * @pfont: pointer to compound or one piece font
 *
 * The function do nothing because all fonts are only builtin.
 * This function is here for compatability with MW.
 * In feature if some fonts will be dynamicly loaded, here 
 * they will be unloaded.
 *
 * Return Value: The function does not return a value.
 * File: sui_font.c
 */  
  void sui_font_unloadfont(PMWFONT pfont) {
    /* builtins can't be unloaded*/
  }
  
  
  
  
#if (MICROWINDOWS_VER_CODE>=VER_CODE(0,90,0))
/**
 * sui_font_comp_gettextbits - Get bitmap which represents character (for compound font)
 * @pfont: pointer to compound font
 * @chr: character in unicode16
 * @retmap: pointer to output pointer to bitmap of character
 * @pwidth: pointer to output width of character
 * @pheight: pointer to output height of character
 * @pbase: pointer to output base of character
 *
 * The function returns character bitmap, width, height and base.
 * For compatability with base MicroWindows, retmap isn't MWIMAGEBITS **retmap,
 * but MWIMAGEBITS *retmap, so explicit typecasting is needed.
 *
 * Return Value: The function does not return a value.
 * File: sui_font.c
 */  
  void sui_font_comp_gettextbits( PMWFONT pfont, int chr, const MWIMAGEBITS **retmap,
                                  MWCOORD *pwidth, MWCOORD *pheight, MWCOORD *pbase) 
#else /* MICROWINDOWS_VER_CODE<VER_CODE(0,90,0) */
  void sui_font_comp_gettextbits( PMWFONT pfont, int chr, MWIMAGEBITS *retmap,
                                  MWCOORD *pwidth, MWCOORD *pheight, MWCOORD *pbase) 
#endif /* MICROWINDOWS_VER_CODE<VER_CODE(0,90,0) */
   {    
    MWCFONT **ppf = (MWCFONT **)((PMWCOREFONT)pfont)->cfont;
    MWCFONT *pfnt;
    //int n, count, width;
    //MWIMAGEBITS *bits;
    
    while((pfnt=*ppf)) {
      if ( chr >= pfnt->firstchar && chr < pfnt->firstchar + pfnt->size)
        break;
      ppf++;
    }
    if ( !pfnt) {     
      *pwidth = 0; *pheight = 0; *pbase = 0;
      *(MWIMAGEBITS **)retmap = NULL;
    } else {
      chr -= pfnt->firstchar;
      /* get font bitmap depending on fixed pitch or not*/
      *retmap = pfnt->bits + (pfnt->offset ? pfnt->offset[chr] : pfnt->height * chr);
      *pheight = pfnt->height;
      *pbase = pfnt->ascent; 
      *pwidth = pfnt->width ? pfnt->width[chr] : pfnt->maxwidth;
    }
  }

#if (MICROWINDOWS_VER_CODE>=VER_CODE(0,90,0))
/**
 * sui_font_gettextbits - Get bitmap which represents character (for one piece font)
 * @pfont: pointer to one piece font
 * @chr: character in unicode16
 * @retmap: pointer to output pointer to bitmap of character
 * @pwidth: pointer to output width of character
 * @pheight: pointer to output height of character
 * @pbase: pointer to output base of character
 *
 * The function returns character bitmap, width, height and base.
 * For compatability with base MicroWindows, retmap isn't MWIMAGEBITS **retmap,
 * but MWIMAGEBITS *retmap, so explicit typecasting is needed.
 *
 * Return Value: The function does not return a value.
 * File: sui_font.c
 */  
  void sui_font_gettextbits( PMWFONT pfont, int chr, const MWIMAGEBITS **retmap,
                             MWCOORD *pwidth, MWCOORD *pheight, MWCOORD *pbase)
#else /* MICROWINDOWS_VER_CODE<VER_CODE(0,90,0) */
  void sui_font_gettextbits( PMWFONT pfont, int chr, MWIMAGEBITS *retmap,
                             MWCOORD *pwidth, MWCOORD *pheight, MWCOORD *pbase)
#endif /* MICROWINDOWS_VER_CODE<VER_CODE(0,90,0) */
   {    
    MWCFONT *pfnt = (MWCFONT *)((PMWCOREFONT)pfont)->cfont;
    //int n, count, width;
    //MWIMAGEBITS *bits;
    
    if ( chr >= pfnt->firstchar && chr < pfnt->firstchar + pfnt->size) {
      chr -= pfnt->firstchar;
      /* get font bitmap depending on fixed pitch or not*/
      *retmap = pfnt->bits + (pfnt->offset ? pfnt->offset[chr] : pfnt->height * chr);
      *pheight = pfnt->height;
      *pbase = pfnt->ascent; 
      *pwidth = pfnt->width ? pfnt->width[chr] : pfnt->maxwidth;
    } else {
      *pwidth = 0; *pheight = 0; *pbase = 0;
      *(MWIMAGEBITS **)retmap = NULL;
    }
  }


/**
 * sui_font_drawtext - Draw text 
 * @pfont: pointer to compound or one piece font
 *
 * The function draws text depended on flags. The text can be in row (default)
 * or in column (MWTF_COLUMN_TEXT). For the text in row flags can be
 * MWTF_TOP(default), MWTF_BASELINE, MWTF_BOTTOM and MWTF_CENTER
 * (combination of MWTF_TOP and MWTF_BOTTOM). It means that the 'y' is 
 * positioned to select coordinate. For the text in column flags can be
 * MWTF_LEFT, MWTF_RIGHT and their combination MWTF_CENTER. It is base position
 * for the 'x' coordinate.
 *
 * Return Value: The function does not return a value.
 * File: sui_font.c
 */  
  void sui_font_drawtext( PMWFONT pfont, PSD psd, MWCOORD x, MWCOORD y,
                          const void *text, int chars, MWTEXTFLAGS flags)
  {
    utf8 *utf = (utf8 *) text;
    utf8char *utxt;
    MWCOORD width;  /* width of text area */
    MWCOORD height; /* height of text area */
    MWCOORD base;   /* baseline of text*/
    MWCOORD startx, starty;
#if (MICROWINDOWS_VER_CODE>=VER_CODE(0,90,0))
    const 
#endif /* MICROWINDOWS_VER_CODE>=VER_CODE(0,90,0) */
    MWIMAGEBITS *bitmap;
  #if UTF8_MAX_BYTES_IN_CHAR == 6 // unicode32
    unsigned long ucs;
  #else                           // unicode16
    unsigned short ucs;
  #endif  
  
    if ( !utf) return;
    if ( chars < 0) chars = sui_utf8_length( utf);
#if (MICROWINDOWS_VER_CODE<VER_CODE(0,90,0))
    base = flags & MWTF_COLUMN_TEXT ? SUI_COLUMN_TEXT : 0;
    pfont->fontprocs->GetTextSize( pfont, utf, chars, &width, &height, &base);
#else /* MICROWINDOWS_VER_CODE>=VER_CODE(0,90,0) */
    pfont->fontprocs->GetTextSize( pfont, utf, chars, flags, &width, &height, &base);
#endif /* MICROWINDOWS_VER_CODE>=VER_CODE(0,90,0) */
    utxt = sui_utf8_get_text( utf);
    // both texts in row and column
    if( flags & MWTF_BASELINE) { // baseline
      y -= base;
    } else {
      if( flags & MWTF_BOTTOM) {
        if ( flags & MWTF_TOP) { // center
          y -= height / 2;
        } else {                 // bottom
          y -= (height - 1);          
        }
      }
    }
    if ( flags & MWTF_RIGHT) {
      if ( flags & MWTF_LEFT) { // CENTER
        startx = x - width / 2;
      } else { // RIGHT
        startx = x - (width - 1);
      }
    } else { // LEFT (default)
      startx = x;
    }
    
    
    if ( flags & MWTF_COLUMN_TEXT) { // column text
      if ( GdClipArea( psd, startx, y, startx + width - 1, y + height - 1) == CLIP_INVISIBLE) return; // from switch
      while ( chars-- > 0 && y < psd->yvirtres) {
        if ( flags & MWTF_UTF8) {
          utxt += sui_utf8_to_ucs( utxt, &ucs);
        } else { // MWTF_ASCII
          ucs = *utxt++; // BE CAREFUL - Is utf8char unsigned char ?
        }
 #if (MICROWINDOWS_VER_CODE>=VER_CODE(0,90,0))
        pfont->fontprocs->GetTextBits( pfont, ucs, (const MWIMAGEBITS **) &bitmap, &width, &height, &base);
 #else /* MICROWINDOWS_VER_CODE<VER_CODE(0,90,0) */
        pfont->fontprocs->GetTextBits( pfont, ucs, (MWIMAGEBITS *) &bitmap, &width, &height, &base);
 #endif /* MICROWINDOWS_VER_CODE<VER_CODE(0,90,0) */
        /* note: change to bitmap*/
        if ( flags & MWTF_RIGHT) {
          if ( flags & MWTF_LEFT) { // CENTER
            startx = x - width/2;
          } else { // RIGHT
            startx = x - (width-1);
          }
        }
        GdBitmap( psd, startx, y, width, height, bitmap);
        y += height;
      }
      // draw underline
    } else {                         // row text
      x = startx; // for underline
      starty = y + base;
    
      if ( GdClipArea( psd, x, y, x + width - 1, y + height - 1) == CLIP_INVISIBLE) return; // from switch
      /* Get the bitmap for each character individually, and then display
       * them using clipping for each one.
       */
      while ( chars-- > 0 && x < psd->xvirtres) {
        if ( flags & MWTF_UTF8) {
          utxt += sui_utf8_to_ucs( utxt, &ucs);
        } else { // MWTF_ASCII
          ucs = *utxt++; // BE CAREFUL - Is utf8char unsigned char ?
        }
 #if (MICROWINDOWS_VER_CODE>=VER_CODE(0,90,0))
        pfont->fontprocs->GetTextBits( pfont, ucs, (const MWIMAGEBITS **) &bitmap, &width, &height, &base);       
 #else /* MICROWINDOWS_VER_CODE<VER_CODE(0,90,0) */
        pfont->fontprocs->GetTextBits( pfont, ucs, (MWIMAGEBITS *) &bitmap, &width, &height, &base);
 #endif /* MICROWINDOWS_VER_CODE<VER_CODE(0,90,0) */
        /* note: change to bitmap*/
        GdBitmap( psd, x, y, width, height, bitmap);
        x += width;
      }    
      if (pfont->fontattr & MWTF_UNDERLINE)
        GdLine(psd, startx, starty, x, starty, FALSE);
    }  
    GdFixCursor(psd);
  }

