/* sui_environment.h
 *
 * SUITK environment structures and settings
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#ifndef _SUITK_ENVIRONMENT_SYSTEM_HEADER_
  #define _SUITK_ENVIRONMENT_SYSTEM_HEADER_

#include <suiut/sui_types.h>
#include <suitk/sui_gdi.h>
#include <suitk/sui_event.h>
#include <suitk/sui_term.h>

#include <ulut/ul_gavl.h>

#ifdef __cplusplus
extern "C" {
#endif

extern sui_event_global_var_t sui_globev;
extern sui_event_buffers_t *curevbufs;
extern struct sui_dc *sui_globdc;

extern int sui_change_local_namespace_from_sxml(utf8 *name, void *par);

#define SUI_GLOBAL_BLINK_TIME 500 // half of blinking period

/* exported return code of processing of the last action */
extern int sui_environment_last_action_retcode;

/******************************************************************************
 * Built-in fonts
 ******************************************************************************/

/* MWCFONT */
extern MWCFONT font_winVerdana20;
extern MWCFONT font_wTrebuchetMS15x40;
extern MWCFONT font_wTrebuchetMS8x23;
extern MWCFONT font_wTrebuchetMS5x16;
extern MWCFONT font_wVerdana18_Bold;
extern MWCFONT font_wVerdana_16;
extern MWCFONT font_wTahoma_88;
extern MWCFONT font_wTahoma_44;
extern MWCFONT font_wTahoma_18;
extern MWCFONT font_wTahoma_16;
extern MWCFONT font_wTahoma40;
extern MWCFONT font_Arial90;
extern MWCFONT font_Arial100;
extern MWCFONT font_wArial105;

/* PMWCFONT */
extern PMWCFONT font_tahoma[];
extern PMWCFONT wT16comp[];
extern PMWCFONT font_wTahoma_28[];
extern PMWCFONT font_wArialMS_18[];

extern MWCOREFONT sui_fonts[];

typedef void gsi_fnc_t(PSD psd, PMWSCREENINFO psi);

gsi_fnc_t *sui_env_set_new_fonts(MWCOREFONT *new_fonts, PSD psd);

/******************************************************************************/
/* suitk structures */

struct sui_widget;
struct sui_application;
struct sui_state;
struct sui_substate;
struct sui_transition;
struct sui_condition;
struct sui_action;
struct sui_list;

/*
 * sui_name_cmp - GAVL comparing function (utf8 name)
 */
static inline int
sui_name_cmp(utf8 * const *a, utf8 * const *b)
{
  return sui_utf8_cmp(*a,*b);
}

/******************************************************************************
 * Condition
 ******************************************************************************/

/**
 * struct sui_condition - Structure with possible action conditions
 * @refcnt: Condition instance reference counter.
 * @str_term: UTF8 text with conditional term.
 * @str_focus: Name of focused widget.
 * @conflags: Condition flags [see SUI_COND_FLG_].
 * @negflags: Condition flags for inhibited condition.
 * @state_time: Maximal time from screen entry.
 * @user_time: Maximal user inactivity time.
 * @event: An event identifier.
 * @evkey: An event key (for events from keyboard SUEV_KDOWN, SUEV_KUP).
 * @evcmd: An event command identifier.
 * @evinfo: An event info field for specified events.
 * @term: Pointer to the parsed term.
 * @lastst: Name of the last state.
 * @lastsubst: Name of the last substate.
 * @focus: Pointer to a widget with focus.
 *
 * File: sui_environment.h
 */
typedef struct sui_condition {
  sui_refcnt_t    refcnt;
  utf8           *str_term;   /* conditional term string */
  utf8           *str_focus;  /* which widget must be focused to transition */
/* conditions */
  sui_flags_t     conflags;      /* condition flags */
  sui_flags_t     negflags;      /* negative condition flags - FIXME: now it is only for focused widget */
  long            state_time;    /* maximal time - from screen entry */
  long            user_time;     /* maximal user inactivity time */
  unsigned short  event;         /* event */
  unsigned short  evkey;         /* event key */
  unsigned short  evcmd;         /* event command */
  long            evinfo;        /* event info (for all events) */
  sui_term_t     *term;          /* it is translated on state entry (after state sxml file is loaded ) */
  utf8           *lastst;        /* laststate */
  utf8           *lastsubst;     /* lastsubst */
  struct sui_widget *focus;      /* and released on state exit */
} sui_condition_t;

sui_refcnt_t sui_condition_inc_refcnt(sui_condition_t *cond);
sui_refcnt_t sui_condition_dec_refcnt(sui_condition_t *cond);

extern SUI_CANBE_CONST_VMT sui_obj_vmt_t sui_condition_vmt_data;

/**
 * enum sui_condition_flags - Flags for condition structure
 * @SUI_COND_FLG_UNCOND: Structure describes no condition.
 * @SUI_COND_FLG_TIME: Maximal time from screen entry is set in the condition.
 * @SUI_COND_FLG_USERTIME: Maximal user inactivity time is set in the condition.
 * @SUI_COND_FLG_TERM: Conditional term is set in the condition.
 * @SUI_COND_FLG_FOCUS: Name of the focused widget is set in the condition.
 * @SUI_COND_FLG_EVENT: Event identifier is set in the condition.
 * @SUI_COND_FLG_EVKEY: Keyboard event is set in the condition.
 * @SUI_COND_FLG_EVCMD: Command event is set in the condition.
 * @SUI_COND_FLG_EVINFO: Event info field is set in the condition.
 * @SUI_COND_FLG_LASTST: Name of the last state is set in the condition.
 * @SUI_COND_FLG_LASTSST: Name of the last substate is set in the condition.
 * @SUI_COND_FLG_SKIP: In the condition was an error - skip it in the future.
 *
 * File: sui_environment.h
 */
enum sui_condition_flags {
  SUI_COND_FLG_UNCOND   = 0x0000,
  SUI_COND_FLG_TIME     = 0x0001,
  SUI_COND_FLG_USERTIME = 0x0002,
  SUI_COND_FLG_TERM     = 0x0004,
  SUI_COND_FLG_FOCUS    = 0x0008,

  SUI_COND_FLG_EVENT    = 0x0010,
  SUI_COND_FLG_EVKEY    = 0x0020,
  SUI_COND_FLG_EVCMD    = 0x0040,
  SUI_COND_FLG_EVINFO   = 0x0080,

  SUI_COND_FLG_LASTST   = 0x0100,
  SUI_COND_FLG_LASTSST  = 0x0200,

  SUI_COND_FLG_SKIP     = 0x8000, /* if there is ERROR - skip this transition for future -> ERROR -> END of Application */
};

int sui_condition_init(sui_condition_t *cond);


/******************************************************************************
 * Action function
 ******************************************************************************/
typedef int sui_action_fcn_t(struct sui_application *appl,
                              struct sui_action *action);

extern SUI_CANBE_CONST_VMT sui_obj_vmt_t sui_action_fcn_vmt_data;
/******************************************************************************
 * Action
 ******************************************************************************/
/**
 * struct sui_action - Structure which describes one action (function)
 * @refcnt: Action structure reference counter.
 * @condition: Pointer to the action conditions.
 * @function: Pointer to the action function.
 * @wdg_src: Optional action argument - source widget.
 * @data_src: Optional action argument - data source DINFO.
 * @wdg_dst: Optional action argument - destination widget.
 * @data_dst: Optional action argument - data destination DINFO.
 * @text: Optional action argument - utf-8 text.
 * @list: Optional action argument - suitk list.
 * @value: Optional action argument - numeric value.
 * @flags: Optional action argument - flags.
 * @cmd: Optional action argument - command value.
 * @next_action: Pointer to the next action in the chain.
 *
 * File: sui_environment.h
 */
typedef struct sui_action {
  sui_refcnt_t       refcnt;
  sui_condition_t   *condition;
  sui_action_fcn_t  *function;
  struct sui_widget *wdg_src;
  sui_dinfo_t       *data_src;
  struct sui_widget *wdg_dst;
  sui_dinfo_t       *data_dst;
  utf8              *text;
  struct sui_list   *list;
  long               value;
  unsigned long      flags;
  short              cmd;

  struct sui_action *next_action;
} sui_action_t;

sui_refcnt_t sui_action_inc_refcnt(sui_action_t *action);
sui_refcnt_t sui_action_dec_refcnt(sui_action_t *action);

extern SUI_CANBE_CONST_VMT sui_obj_vmt_t sui_action_vmt_data;

int sui_action_add_next(sui_action_t **head, sui_action_t *add);

int sui_action_process(struct sui_application *appl, sui_action_t *action);
int sui_action_process_all(struct sui_application *appl, sui_action_t *action_head);


/******************************************************************************
 * Transition
 ******************************************************************************/
/**
 * struct sui_transition - State transition structure
 * @refcnt: Transition instance reference counter.
 * @cond: Pointer to a condition which fires transition.
 * @scenario: An UTF8 name of destination scenario.
 * @tostate: An UTF88 name of destination state.
 * @tosubst: An UTF88 name of destination substate (other than state initial substate).
 * @towidget: An UTF88 name of widget which will contain focus on state entry.
 * @abefore_head: Root of transition actions list (before transition).
 * @aafter_head: Root of transition action list (after transition).
 * @next_trans: Pointer to the linked next description of the next transition.
 *
 * File: sui_environment.h
 */
typedef struct sui_transition {
  sui_refcnt_t           refcnt;
  sui_condition_t       *cond;
  utf8                  *scenario;
  utf8                  *tostate;
  utf8                  *tosubst;
  utf8                  *towidget;
  sui_action_t          *abefore_head;
  sui_action_t          *aafter_head;
  struct sui_transition *next_trans;
} sui_transition_t;

sui_refcnt_t sui_transition_inc_refcnt(sui_transition_t *transition);
sui_refcnt_t sui_transition_dec_refcnt(sui_transition_t *transition);

extern SUI_CANBE_CONST_VMT sui_obj_vmt_t sui_transition_vmt_data;

int sui_transition_add_next(sui_transition_t **head, sui_transition_t *trans);

int sui_transition_init(sui_transition_t *trans);
int sui_transition_init_all(sui_transition_t *thead);


/******************************************************************************
 * SubState Dialog
 ******************************************************************************/
/**
 * struct sui_ssdialog - Substate dialog structuredu
 * @refcnt: SSDialog instance reference counter.
 * @cond: Pointer to a dialog opening condition.
 * @exitcond: Pointer to a dialog closing condition.
 * @dialog: Pointer to a dialog widget.
 * @abefore_head: Pointer to chain of transition actions (before transition).
 * @aafter_head: Pointer to chain of transition action (after transition).
 * @next_dlg: Internal pointer to the next substate dialog structure.
 *
 * File: sui_environment.h
 */
typedef struct sui_ssdialog {
  sui_refcnt_t         refcnt;
  sui_condition_t     *cond;
  sui_condition_t     *exitcond;
  struct sui_widget   *dialog;
  sui_action_t        *abefore_head;
  sui_action_t        *aafter_head;
  struct sui_ssdialog *next_dlg;
} sui_ssdialog_t;

sui_refcnt_t sui_ssdialog_inc_refcnt(sui_ssdialog_t *ssd);
sui_refcnt_t sui_ssdialog_dec_refcnt(sui_ssdialog_t *ssd);

extern SUI_CANBE_CONST_VMT sui_obj_vmt_t sui_ssdialog_vmt_data;

int sui_ssdialog_add_next(sui_ssdialog_t **head, sui_ssdialog_t *trans);

int sui_ssdialog_init(sui_ssdialog_t *trans);
int sui_ssdialog_init_all(sui_ssdialog_t *thead);


/******************************************************************************
 * State function
 ******************************************************************************/
typedef int sui_state_fcn_t(struct sui_application *appl, void *info, int function);

extern SUI_CANBE_CONST_VMT sui_obj_vmt_t sui_state_fcn_vmt_data;

/**
 * enum state_function_fcn - Types of reason why the state function is invoking.
 * @SUI_STATE_FCN_ENTRY: The state function is invoked when state entering is occured.
 * @SUI_STATE_FCN_FIRST_PROC: The state function is invoked in the first cycle of the main processing loop.
 * @SUI_STATE_FCN_PROC: The state function is invoked in cycle of the main processing loop.
 * @SUI_STATE_FCN_EVENT: The state function is invoked when some event occures.
 * @SUI_STATE_FCN_EXIT: The state function is invoked when state leaving is occured.
 *
 * File: sui_environment.h
 */
enum state_function_fcn {
  SUI_STATE_FCN_ENTRY      = 0x01, // info is pointer to current state
  SUI_STATE_FCN_FIRST_PROC = 0x02, // info is pointer to current state
  SUI_STATE_FCN_PROC       = 0x03, // info is pointer to current state
  SUI_STATE_FCN_EVENT      = 0x04, // info is pointer to current event
  SUI_STATE_FCN_EXIT       = 0x05, // info is pointer to current state
};

/******************************************************************************
 * Sub-state
 ******************************************************************************/
/**
 * struct sui_substate - Substate structure
 * @refcnt: Substate instance reference counter.
 * @name: Pointer to a substate utf8 name.
 * @init_focus: Pointer to a utf8 name of the widget which will have got focus after enter to the substate.o'.
 * @trans_root: Pointer to a chain of the substate local transitions.
 * @dialog_head: Pointer to a chain of the substate defined dialogs.
 * @aentry_root: Pointer to a chain of the substate entry actions.
 * @aexit_root: Pointer to a chain of the substate exit actions.
 * @substate_node: Field for the connection with state GAVL tree.
 *
 * File: sui_environment.h
 */
typedef struct sui_substate {
  sui_refcnt_t      refcnt;
  utf8             *name;
  utf8             *init_focus; /* substate initial widget */
  sui_transition_t *trans_head; /* substate transitions */
  sui_ssdialog_t   *dialog_head; /* dialogs */
  sui_action_t     *aentry_head; /* entry actions */
  sui_action_t     *aatfirst_head; /* at_first actions */
//  sui_action_t     *aloop_head;  /* loop actions */
  sui_action_t     *aexit_head;  /* exit actions */

  gavl_node_t       substate_node;
} sui_substate_t;

sui_refcnt_t sui_substate_inc_refcnt(sui_substate_t *substate);
sui_refcnt_t sui_substate_dec_refcnt(sui_substate_t *substate);

extern SUI_CANBE_CONST_VMT sui_obj_vmt_t sui_substate_vmt_data;

int sui_substate_add_action(sui_substate_t *substate,
                            sui_action_t *add, int type);
int sui_substate_process_actions(struct sui_application *appl,
                                 sui_substate_t *substate, int type);

/**
 * enum sui_substate_actions - Action identifiers for action adding to substate
 * @SUI_SSACT_ENTRY: An action will be added as the next substate entry action.
 * @SUI_SSACT_ATFIRST: An action will be added as the next substate "at first" action which will be called after enter to substate.
 * @SUI_SSACT_LOOP: An action will be added as the next substate loop action which will be called every cycle in the main process loop.
 * @SUI_SSACT_EXIT: An action will be added as the next substate exit action.
 */
enum sui_substate_actions {
  SUI_SSACT_ENTRY,
  SUI_SSACT_ATFIRST,
  SUI_SSACT_LOOP,
  SUI_SSACT_EXIT,
};

/******************************************************************************
 * State Extension
 ******************************************************************************/
/**
 * struct sui_stateext - State extension structure
 * @refcnt: Extension instance reference counter.
 * @widget_root: UTF8 name of the main state widget.
 * @init_substate: UTF8 name of the initial substate.
 * @procs: UTF8 name of the state process function.
 * @trans_head: pointer to chain of state transitions.
 * @substate_root: GAVL root of substates.
 *
 * The state extension structure describes dynamical part of the state and contains
 * chain of the state substates, transitions from state and name of the main widget.
 * 
 * File: sui_environment.h
 */
typedef struct sui_stateext {
  sui_refcnt_t            refcnt;

  utf8                   *widget_root;
  utf8                   *init_substate;
  utf8                   *procs;

  sui_transition_t       *trans_head;
  gavl_cust_root_field_t  substate_root;
} sui_stateext_t;

GAVL_CUST_NODE_INT_DEC(sui_ste, struct sui_stateext, sui_substate_t, utf8 *,
                        substate_root, substate_node, name, sui_name_cmp)

sui_refcnt_t sui_stateext_inc_refcnt(sui_stateext_t *stext);
sui_refcnt_t sui_stateext_dec_refcnt(sui_stateext_t *stext);

extern SUI_CANBE_CONST_VMT sui_obj_vmt_t sui_stateext_vmt_data;

sui_stateext_t *sui_stateext_find(utf8 *state);
sui_substate_t *sui_stateext_find_substate(sui_stateext_t *ste, utf8 *subst);


/******************************************************************************
 * State
 ******************************************************************************/
/**
 * struct sui_state - State structure
 * @refcnt: State instance reference counter.
 * @name: State UTF8 name.
 * @nsname: UTF8 name of the state namespace (or sxml filename (relative to application base path) with state extension).
 * @state_ext: Pointer to the current state extension.
 * @state_node: GAVL node field of state.
 *
 * File: sui_environment.h
 */
typedef struct sui_state {
  sui_refcnt_t    refcnt;

  utf8           *name;
  utf8           *nsname;
  sui_stateext_t *state_ext;
  gavl_node_t     state_node;
} sui_state_t;

sui_refcnt_t sui_state_inc_refcnt(sui_state_t *state);
sui_refcnt_t sui_state_dec_refcnt(sui_state_t *state);

extern SUI_CANBE_CONST_VMT sui_obj_vmt_t sui_state_vmt_data;

int sui_state_set_extension(sui_state_t *state, sui_stateext_t *steext);

/******************************************************************************
 * Scenario - screen state machine
 ******************************************************************************/
/**
 * struct sui_scenario - Scenario structure which contains all accessible states
 * @refcnt: Scenario instance reference counter.
 * @init: UTF8 Name of initial state. (after change application scenario).
 * @trans_head: Pointer to the chain of global, (from each state in the scenario).
 * @state_root: GAVL root of states.
 *
 * File: sui_environment.h
 */
typedef struct sui_scenario {
  sui_refcnt_t            refcnt;
  utf8                   *init;       /* init state */
  sui_transition_t       *trans_head; /* state transitions */
  gavl_cust_root_field_t  state_root;
} sui_scenario_t;

GAVL_CUST_NODE_INT_DEC(sui_sco_state, sui_scenario_t, sui_state_t, utf8 *,
                        state_root, state_node, name, sui_name_cmp)


sui_refcnt_t sui_scenario_inc_refcnt(sui_scenario_t *sco);
sui_refcnt_t sui_scenario_dec_refcnt(sui_scenario_t *sco);

sui_state_t *sui_scenario_find_state(sui_scenario_t *sco, utf8 *state);

/******************************************************************************
 * Application
 ******************************************************************************/
/**
 * struct sui_application - Application common structure
 * @base: Reference to base SUITK virtual object.
 * @refcnt: Application instance reference counter.
 * @focus_mask: Pointer to environment flags.
 * @blink_mask: Pointer to environment flags for blink state.
 * @blink_time: Time stored to control blinking.
 * @cur_time: Current time in [ms] updated each cycle in the main loop.
 * @appl_time: Application time in [ms] since the application start.
 * @state_time: State time in [ms] since the application reached current state (it's updated when state is changed).
 * @activity_time: Time of last user activity (when some key was pressed).
 * @base_path: UTF8 string with base path to sxml tree (directory where sxml files are stored).
 * @events: Storage buffers and variables for events.
 * @cscenario: Pointer to structure which contains information about the current scenario (see struct sui_scenario).
 * @cstflg: Current state flags, cleared on new state entry.
 * @cstate: Pointer to structure which contains information about the current state (see struct sui_state).
 * @laststate: UTF8 string with name of the last state (it can be used in conditions).
 * @csubstate: Pointer to structure which contains information about the current substate (see struct sui_substate).
 * @lastsubstate: UTF8 string with name of the last substate (it can be used in conditions).
 * @proc_fcn: State init, loop and exit "C" function  callback (all in one). It is called when a state starts or ends or each cycle in the main loop.
 * @wdgroot: Pointer to the current main screen widget.
 * @focused: Pointer to the currently focused widget or NULL.
 * @mdialog: Pointer to the currently opened modal dialog or NULL.
 *
 * File: sui_environment.h
 */
typedef struct sui_application {
  struct sui_obj       base; /* for signal-slot support */
  sui_refcnt_t         refcnt;
  
  /* environment flags */
  sui_flags_t         *focus_mask;
  sui_flags_t         *blink_mask;
  unsigned long        blink_time; /* environment times - blinking, ... */

  /* times */
  unsigned long        cur_time;   /* current time - updated in each cycle of the main loop (FIXME:change time if time is somewhere set) */
  unsigned long        appl_time;  /* application start time */
  unsigned long        state_time; /* state entry time */
  unsigned long        activity_time; /* time of last user interaction */
  //unsigned long        key_time; /* how long was last key pressed */

  /* scenario */
  utf8 *base_path; /* base path to state filenames */

  /* event queues */
  sui_event_buffers_t  events; /* event buffers and variables */
  
  /* current state */
  sui_scenario_t    *cscenario;
  sui_flags_t        cstflg; /* current state flags - they are cleared on new state entry */
  sui_state_t       *cstate;
  sui_substate_t    *csubstate;
  utf8              *laststate;
  utf8              *lastsubstate;
  
  sui_state_fcn_t   *proc_fcn; /* init, loop and exit function - all in one */
  
  struct sui_widget *wdgroot; /* root widget screen */
  struct sui_widget *focused; /* current focused widget */
  
  struct sui_ssdialog *mdialog; /* current substate modal dialog */
  
} sui_application_t;

/**
 * enum appl_cstflags - Flags which disable using state function in the loop
 * @SUI_APPL_FLAGS_NONE: Any one is disabled.
 * @SUI_APPL_FLAGS_FCN_NOPROC: Calling process function in PROC mode is disabled.
 * @SUI_APPL_FLAGS_FCN_NOEVENT: calling process function in EVENT mode is disabled.
// * @SUI_APPL_FLAGS_INMODAL: One or more modal widgets are opening.
 *
 * File: sui_environment.h
 */
enum appl_cstflags {
  SUI_APPL_FLAGS_NONE       = 0x0000,
  SUI_APPL_FLAGS_FCN_NOPROC = 0x0001,
  SUI_APPL_FLAGS_FCN_NOEVENT= 0x0002,

//  SUI_APPL_FLAGS_INMODAL    = 0x0010,
};

//extern sui_application_t *sui_curapp;

sui_refcnt_t sui_appl_inc_refcnt(sui_application_t *appl);
sui_refcnt_t sui_appl_dec_refcnt(sui_application_t *appl);

/* for sxml loading */
typedef int sui_appl_reload_namespace_fcn(utf8 *name, void *par);

int sui_appl_change_state(sui_application_t *appl, sui_transition_t *trans,
                          sui_appl_reload_namespace_fcn *chns_fcn, void *par);
int sui_appl_replace_scenario(sui_application_t *appl, utf8 *sco);
int sui_appl_change_scenario(sui_application_t *appl, const utf8 *name, void *par);

int sui_appl_check_condition(sui_application_t *appl, sui_condition_t *cond, sui_event_t *event);
sui_transition_t *sui_appl_check_transitions(sui_application_t *appl, sui_event_t *event);
sui_ssdialog_t *sui_appl_check_ssdialog(sui_application_t *appl, sui_event_t *event, int type);

/**
 * enum sui_appl_ssd_event_types - Types of substate dialog conditions
 * @SUI_SSD_COND_ENTRY: The function 'sui_appl_check_ssdialog' will check entry events.
 * @SUI_SSD_COND_EXIT: The function 'sui_appl_check_ssdialog' will check exit events.
 *
 * File: sui_environment.h
 */
enum sui_appl_ssd_event_types {
  SUI_SSD_COND_ENTRY = 0,
  SUI_SSD_COND_EXIT  = 1,
};


struct sui_widget *sui_appl_init_focus(struct sui_widget *root);
int sui_appl_set_focus(sui_application_t *appl, struct sui_widget *wdg);
int sui_appl_clear_focus(sui_application_t *appl);
int sui_appl_change_focus(sui_application_t *appl, int dir);


int sui_appl_open_dialog(sui_application_t *appl, sui_ssdialog_t *ssd);
int sui_appl_close_dialog(sui_application_t *appl, long retcode);

/******************************************************************************
 * Application slot functions
 ******************************************************************************/
//int sui_application_change_screen(sui_application_t *appl, char *name);
int sui_application_show_tip(sui_application_t *appl, struct sui_widget *wdg);
int sui_application_hide_tip(sui_application_t *appl, struct sui_widget *wdg);

/******************************************************************************
 * Application vmt
 ******************************************************************************/
#define sui_application_vmt_fields(new_type, parent_type) \
  SUI_INS_INHERITED_VMT(new_type, parent_type, sui_obj) \
  int (*show_tip) (new_type##_t *self, struct sui_widget *wdg); \
  int (*hide_tip) (new_type##_t *self, struct sui_widget *wdg);
  
typedef struct sui_application_vmt {
  sui_application_vmt_fields(sui_application, sui_obj)
} sui_application_vmt_t;

static inline sui_application_vmt_t *
sui_application_vmt(sui_application_t *self)
{
  return (sui_application_vmt_t*)(self->base.vmt);
}

extern sui_application_vmt_t sui_application_vmt_data;


/******************************************************************************
 * Environment
 ******************************************************************************/
/**
 * struct sui_environment - Environment (global) settings and functions
 * @sui_global_dc: Device context pointing to the actual monitor/panel output device.
 * @sui_global_evar: Global environment variables.
 * @built_in_fonts: Pointer to the list of build-in fonts.
 * @curapp: Pointer to the actual running application specific object.
 *
 * File: sui_environment.h
 */
typedef struct sui_environment {
  sui_dc_t                sui_global_dc; /* output and input device contexts */
  sui_event_global_var_t *sui_global_evar;
  MWCOREFONT             *built_in_fonts; /* built-in fonts */
  sui_application_t *curapp; /* current application */
/*  sui_application_t *apps[]; */ /* array of application */
} sui_environment_t;

/**
 * struct sui_environment_open_params - Structure with opening configuration
 * @flags: opening flags
 * @new_fonts: Pointer to a list of environment's fonts
 */
typedef struct sui_environment_open_params {
  unsigned int flags;
  MWCOREFONT *new_fonts;
} sui_environment_open_params_t;

/**
 * enum sui_environment_opening_flags - environment opening flags
 * @SUI_ENVOPEN_FL_NOMOUSE: don't enable mouse
 */
enum sui_environment_opening_flags {
  SUI_ENVOPEN_FL_MOUSE_MANDATORY = 0x0000,
  SUI_ENVOPEN_FL_MOUSE_OPTIONAL  = 0x0001,
  SUI_ENVOPEN_FL_MOUSE_OFF       = 0x0002,
  SUI_ENVOPEN_FL_MOUSEMASK       = 0x0003,

  SUI_ENVOPEN_FL_KEYB_MANDATORY  = 0x0000,
  SUI_ENVOPEN_FL_KEYB_OPTIONAL   = 0x0004,
  SUI_ENVOPEN_FL_KEYB_OFF        = 0x0008,
  SUI_ENVOPEN_FL_KEYBMASK        = 0x000C,
};

sui_environment_t *sui_environment_open(sui_environment_open_params_t *params);
int sui_environment_close(sui_environment_t *env);
int sui_environment_create_application(sui_environment_t *env, char *nsname, utf8 *apath);
//int sui_environment_switch_app(sui_environment_t *env, int app_id);

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /* _SUITK_ENVIRONMENT_SYSTEM_HEADER_ */
