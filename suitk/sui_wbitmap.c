/* sui_wbitmap.c
 *
 * SUITK bitmap widget.
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#include "sui_wbitmap.h"

#include <ul_log.h>
UL_LOG_CUST(ulogd_suitk)


/******************************************************************************
 * Basic widget assignment function
 ******************************************************************************/
/**
 * sui_wbitmap_assign_image - set image to bitmap widget
 */
int sui_wbitmap_assign_image( sui_widget_t *widget, sui_image_t *newimage)
{
	suiw_bitmap_t *wbmp;
	if ( !widget || !(wbmp = sui_bmpwdg( widget))) return -1;
	if ( wbmp->image) sui_image_dec_refcnt( wbmp->image);
	sui_image_inc_refcnt( newimage);
	wbmp->image = newimage;
	return 0;
}

/**
 * sui_wbitmap_assign_mask - set mask image to bitmap widget
 */
int sui_wbitmap_assign_mask( sui_widget_t *widget, sui_image_t *newmask)
{
	suiw_bitmap_t *wbmp;
	if ( !widget || !(wbmp = sui_bmpwdg( widget))) return -1;
	if ( wbmp->mask) sui_image_dec_refcnt( wbmp->mask);
	sui_image_inc_refcnt( newmask);
	wbmp->mask = newmask;
	return 0;
}


/******************************************************************************
 * Bitmap widget slot functions
 ******************************************************************************/


/******************************************************************************
 * Widget basic functions
 ******************************************************************************/
/**
 * sui_wbitmap_get_size - Return bitmap content size
 * @dc: Pointer to device context.
 * @widget: Pointer to widget.
 * @point: pointer to point structure
 */
static inline
int sui_wbitmap_get_size( sui_dc_t *dc, sui_widget_t *widget, sui_point_t *point)
{
  suiw_bitmap_t *wbmp;
  if ( !widget || !( wbmp = sui_bmpwdg( widget)) || !point) return -1;

  unsigned char rot = sui_is_flag( widget, SUIF_ROTATE_MASK, SUIF_ROTATE_90) ||
                      sui_is_flag( widget, SUIF_ROTATE_MASK, SUIF_ROTATE_90);
  if ( wbmp->image) {
    point->x = (rot) ? wbmp->image->height : wbmp->image->width;
    point->y = (rot) ? wbmp->image->width : wbmp->image->height;
  } else {
    point->x = 0;
    point->y = 0;
  }
  return 0;
}

/**
 * sui_wbitmap_init - initiate bitmap widget
 * @dc: Pointer to device context.
 * @widget: Pointer to the bitmap widget.
 *
 * The function initiates bitmap widget.
 * Return Value: The function does not return a value
 * File: sui_bitmap.c
 */
//inline
void sui_wbitmap_init( sui_dc_t *dc, sui_widget_t *widget)
{
	if ( widget->place.w < 0 || widget->place.h < 0) {
		sui_point_t size;
		sui_widget_compute_size( dc, widget, &size);
		if ( widget->place.w < 0) widget->place.w = size.x;
		if ( widget->place.h < 0) widget->place.h = size.y;
	}
}

/**
 * sui_wbitmap_draw - Draw icon widget
 * @dc: Pointer to device context.
 * @widget: Pointer to the icon widget.
 *
 * The function draws the icon with its flags and
 * its label,if it's inside widget. The function
 * is reaction to event SUEV_DRAW and SUEV_REDRAW
 * Return Value: The function returns 1 if the icon
 * was drawn otherwise returns 0.
 * File: sui_bitmap.c
 */
static inline
int sui_wbitmap_draw( sui_dc_t *dc, sui_widget_t *widget)
{
  suiw_bitmap_t *wbmp;
  sui_point_t box, size, origin = { .x=0, .y=0};
  sui_align_t al;
  unsigned short opflags = SUIO_ROTATE_0;
  long idx = 0;

  if ( !widget || !widget->style || !(wbmp = sui_bmpwdg( widget))) return -1;

  box.x=widget->place.w; box.y=widget->place.h;
  al = sui_widget_get_item_align( widget);

  if ( sui_hevent_command( widget, SUCM_GETSIZE, dc, (long) &size))
  {
    size.x = 0;
    size.y = 0;
  }

  sui_align_object( &box, &origin, &size, 0, widget->style, al);

  if ( sui_test_flag( widget, SUIF_INV_IMAGE))
    opflags |= SUIO_INVERSE_IMAGE;
  if ( sui_test_flag( widget, SUIF_INV_MASK))
    opflags |= SUIO_INVERSE_MASK;
  if ( sui_test_flag( widget, SUIF_MASKED) && wbmp->mask)
    opflags |= SUIO_MASK_IMAGE;
  if ( sui_test_flag( widget, SUIF_MIRROR))
    opflags |= SUIO_MIRROR;
  if ( sui_test_flag( widget, SUIF_TRANSPARENT))
    opflags |= SUIO_TRANSPARENT;
  if ( wbmp->imidx && sui_test_flag( widget, SUIF_MULTIIMAGE)) {
    opflags |= SUIO_MULTIIMAGE;
    sui_rd_long( wbmp->imidx, 0, &idx);
  }

  switch( sui_test_flag( widget, SUIF_ROTATE_MASK)) {
    case SUIF_ROTATE_0:   opflags |= SUIO_ROTATE_0; break;
    case SUIF_ROTATE_90:  opflags |= SUIO_ROTATE_90; break;
    case SUIF_ROTATE_180: opflags |= SUIO_ROTATE_180; break;
    case SUIF_ROTATE_270: opflags |= SUIO_ROTATE_270; break;
  }
  sui_gdi_draw_image( dc, &origin, wbmp->image, wbmp->mask, idx, opflags);
  return 0;
}


/******************************************************************************
 * Bitmap event handler
 ******************************************************************************/
/**
 * sui_bitmap_hevent - Bitmap event handler function
 * @widget: Pointer to the bitmap widget.
 * @event: Pointer to received event.
 *
 * The function is reaction to all events received by the bitmap widget.
 * Return Value: The function does not returns a value.
 * File: sui_bitmap.c
 */
void sui_wbitmap_hevent( sui_widget_t *widget, sui_event_t *event) 
{
	switch( event->what) {
		case SUEV_DRAW:
		case SUEV_REDRAW:
			sui_widget_draw( event->draw.dc, widget, sui_wbitmap_draw);
			return;
		case SUEV_COMMAND:
			switch(event->message.command) {
				case SUCM_INIT:
					sui_widget_hevent(widget, event);
					sui_wbitmap_init((sui_dc_t *)event->message.ptr, widget);
					sui_clear_event( widget, event);
					break;

				case SUCM_GETSIZE:
					if ( !sui_wbitmap_get_size( (sui_dc_t *)event->message.ptr, 
							widget, (sui_point_t *)event->message.info)) {
						sui_clear_event( widget, event);
						return;
					}
					break; /* continue in basic widget hevent */

			}
	}
	if ( event->what != SUEV_NOTHING)
		sui_widget_hevent( widget,event);
}


/******************************************************************************
 * Basic bitmap vmt & signal-slot mechanism
 ******************************************************************************/
int sui_wbitmap_vmt_init( sui_widget_t *pw, void *params)
{
  pw->data = sui_malloc( sizeof( suiw_bitmap_t));
  if(!pw->data)
    return -1;

  pw->type = SUWT_BITMAP;
  pw->hevent = sui_wbitmap_hevent;

  return 0;
}

void sui_wbitmap_vmt_done( sui_widget_t *pw)
{
  if (pw->data) {
    suiw_bitmap_t *wbmp = pw->data;
    if ( wbmp->imidx) sui_dinfo_dec_refcnt( wbmp->imidx);
    wbmp->imidx = NULL;
    if ( wbmp->image) sui_image_dec_refcnt( wbmp->image);
    wbmp->image = NULL;
    if ( wbmp->mask) sui_image_dec_refcnt( wbmp->mask);
    wbmp->mask = NULL;

    free( wbmp);
    pw->data = NULL;
  }
  pw->type = SUWT_GENERIC;
  pw->hevent = sui_widget_hevent; // NULL;
}



SUI_CANBE_CONST
sui_obj_signal_tinfo_t sui_wbitmap_signal_tinfo[] = {
//	{ "statechanged", SUI_SIG_STATECHANGED, &sui_args_tinfo_int},
};

SUI_CANBE_CONST
sui_obj_slot_tinfo_t sui_wbitmap_slot_tinfo[] = { 
/*	{ "toggle", SUI_SLOT_TOGGLE, &sui_args_tinfo_void,
		.target_kind = SUI_STGT_METHOD_OFFSET,
		.target.method_offset=UL_OFFSETOF( sui_wbitmap_vmt_t, toggle)},
*/
};

SUI_CANBE_CONST
sui_obj_tinfo_t sui_wbitmap_vmt_obj_tinfo = {
  .name = "sui_wbitmap",
  .obj_size = sizeof(sui_widget_t),
  .obj_align = UL_ALIGNOF(sui_widget_t),
  .signal_count = MY_ARRAY_SIZE(sui_wbitmap_signal_tinfo),
  .signal_tinfo = UL_CAST_UNQ1(sui_obj_signal_tinfo_t *, sui_wbitmap_signal_tinfo),
  .slot_count = MY_ARRAY_SIZE(sui_wbitmap_slot_tinfo),
  .slot_tinfo = UL_CAST_UNQ1(sui_obj_slot_tinfo_t *, sui_wbitmap_slot_tinfo)
};

sui_wbitmap_vmt_t sui_wbitmap_vmt_data = {
  .vmt_size = sizeof(sui_wbitmap_vmt_t),
  .vmt_refcnt = SUI_STATIC,
  .vmt_parent = &sui_widget_vmt_data,
  .vmt_obj_tinfo = UL_CAST_UNQ1(sui_obj_tinfo_t *, &sui_wbitmap_vmt_obj_tinfo),
  .vmt_init = sui_wbitmap_vmt_init,
  .vmt_done = sui_wbitmap_vmt_done,

//  .toggle = sui_wbutton_toggle,

};

/******************************************************************************/
/**
 * sui_wbutton - Create button widget
 * @aplace: Button position and size.
 * @alabel: Button label in utf8 string.
 * @astyle: Button widget style.
 * @aflags: Widget common and button special flags.
 * @aimage: Pointer to an image used in widget.
 * @amask: Pointer to an image used as mask in widget.
 *
 * The fuction creates bitmap widget, increment reference
 * counters of used components and sets its fields.
 * aname, aimage and amask can be NULL.
 * Return Value: The function returns pointer to
 * created widget or NULL.
 *
 * Return Value: The function returns pointer to created button widget.
 * File: sui_wbitmap.c
 */
sui_widget_t *sui_wbitmap( sui_rect_t *aplace, utf8 *alabel, sui_style_t *astyle,
              unsigned long aflags, sui_image_t *aimage, sui_image_t *amask)
{
	sui_widget_t *pw;

	pw = sui_widget_create_new( &sui_wbitmap_vmt_data, aplace, alabel, astyle, aflags);

	if ( !pw) return NULL;
	
	if ( aimage) {
		sui_image_inc_refcnt( aimage);
		sui_bmpwdg( pw)->image = aimage;
	}
	if ( amask) {
		sui_image_inc_refcnt( amask);
		sui_bmpwdg( pw)->mask = amask;
	}
	return pw;
}

