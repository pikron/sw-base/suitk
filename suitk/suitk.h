/* suitk.h
 *
 * Main header file for SUITK (Simple User Interface TK).
 * 
 * Version 0.1
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

/* Simple flat UI TK, names of some widget fields taken from FLTK */
#ifndef _SUITK_H_
#define _SUITK_H_

	#include <suiut/mymalloc.h>

	#include <device.h>
	#include <mwtypes.h>

	#include <suiut/sui_types.h>
	#include <suiut/support.h>
	#include <suiut/namespace.h>
	#include <suiut/sui_files.h>
	
	#include <suiut/sui_objbase.h>
	#include <suiut/sui_time.h>
	#include <suiut/sui_nls.h>
	#include <suiut/sui_utf8.h>

	#include <suiut/sui_dinfo.h>
	#include <suiut/sui_dtrans.h>
	#include <suiut/sui_di_text.h>
	#include <suiut/sui_finfo.h>

	#include <suitk/sui_comdefs.h>
	#include <suitk/sui_font.h>
	#include <suitk/sui_gdi.h>

	#include <suitk/sui_structs.h>
	#include <suitk/sui_term.h>
	
	#include <suitk/sui_args.h>
	#include <suitk/sui_signalslot.h>
	#include <suitk/sui_event.h>

	#include <suitk/sui_environment.h>

	#include <suitk/sui_widget.h>
	#include <suitk/sui_wgroup.h>
	#include <suitk/sui_wbutton.h>
	#include <suitk/sui_wbitmap.h>
	#include <suitk/sui_wlistbox.h>
	#include <suitk/sui_wscroll.h>
	#include <suitk/sui_wnumber.h>
	#include <suitk/sui_wtext.h>
	#include <suitk/sui_wtime.h>
	#include <suitk/sui_wtable.h>
	#include <suitk/sui_wdyntext.h>
	#include <suitk/sui_wgraph.h>

  #include <suitk/sui_mobilkbd.h>

  #include <suitk/sui_print.h> /* export to printers */

	#include <ul_log.h>

	ul_log_domain_t ulogd_suitk;

/*
	#include <suitk/sui_screen.h>

*/                            
#endif /* _SUITK_H_ */
