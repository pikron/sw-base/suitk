/* sui_widget.c
 *
 * SUITK basic widget
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#include "sui_widget.h"
#include "sui_wgroup.h"
#include <suiut/support.h>
#include <suitk/sui_gdi.h>
#include <suitk/sui_args.h>

#include <ul_log.h>

UL_LOG_CUST(ulogd_suitk)

/**
 * sui_widget_current_focus_flags - The current global focus flags
 *
 * The global mask for setting/clearing focus. When the widget focus is set or cleared
 * this mask is add/removed to/from the focused/unfocused widget flags.
 *
 * File: sui_widget.c
 */
sui_flags_t sui_widget_current_focus_flags = SUFL_FOCUSED | SUFL_ACTIVE;

/**
 * sui_widget_current_blink_flags - The current global blink flags
 *
 * The global mask for blinking. When the widget is highlighted/lighted off
 * this mask is add/removed to/from the widget flags.
 *
 * File: sui_widget.c
 */
sui_flags_t sui_widget_current_blink_flags = SUFL_HIGHLIGHTED;

/******************************************************************************
 * Widget drawing
 ******************************************************************************/
/**
 * sui_draw_init - Common drawing initiate function
 * @dc: Pointer to a device context.
 * @widget: Pointer to a widget.
 *
 * The function initializes (synchonizes) drawing of the widget.
 *
 * Return Value: The function does not return a value.
 *
 * File: sui_widget.c
 */ 
inline
void sui_draw_init(sui_dc_t *dc, struct sui_widget *widget)
{
}

/**
 * sui_draw_done - Common drawing finishing function
 * @dc: Pointer to a device context.
 * @widget: Pointer to a widget.
 *
 * The function finishes (synchonizes) drawing of the widget.
 *
 * Return Value: The function does not return a value.
 *
 * File: sui_widget.c
 */ 
inline
void sui_draw_done(sui_dc_t *dc, struct sui_widget *widget)
{
}

/**
 * sui_set_fgcolor - Set device foreground color directly from a widget colortable
 * @dc: Pointer to a device context.
 * @wdg: Pointer to a widget.
 * @color: Index of required color in the widget colortable
 *
 * The function sets device foreground color from the widget colortable and a required color index.
 *
 * Return Value: The function does not return a value.
 *
 * File: sui_widget.c
 */
inline
void sui_set_fgcolor(sui_dc_t *dc, sui_widget_t *wdg, int color)
{
  if (wdg && (color < wdg->style->coltab->count))
    sui_gdi_set_fgcolor(dc, wdg->style->coltab->ctab[color]);
}

/**
 * sui_set_bgcolor - Set device background color directly from a widget colortable
 * @dc: Pointer to a device context.
 * @wdg: Pointer to a widget.
 * @color: Index of required color in the widget colortable
 *
 * The function sets device background color from the widget colortable and a required color index.
 *
 * Return Value: The function does not return a value.
 *
 * File: sui_widget.c
 */
inline
void sui_set_bgcolor(sui_dc_t *dc, sui_widget_t *wdg, int color)
{
  if (wdg && (color < wdg->style->coltab->count)) {
    sui_color_t bgc = wdg->style->coltab->ctab[color];
    sui_gdi_set_bgcolor(dc, bgc);
    sui_gdi_set_usebgrd(dc, !sui_color_is_transparent(bgc));
  }
}

/**
 * sui_set_widget_fgcolor - Set device foreground color from a widget colortable and the current widget mode
 * @dc: Pointer to a device context.
 * @wdg: Pointer to a widget.
 * @color: Index of required color in the widget colortable
 *
 * The function sets device foreground color from the widget colortable and a required color index.
 * The required color index is only selection of function of color (widget frame, widget background,...)
 * The entered index is updated according to the current widget mode (normal, highlighted, selected, HG+SEL).
 *
 * Return Value: The function does not return a value.
 *
 * File: sui_widget.c
 */
void sui_set_widget_fgcolor(sui_dc_t *dc, sui_widget_t *wdg, int color)
{
  sui_color_t fgcolor;
  sui_flags_t wflg = sui_wdgclr_state(wdg);

  if ((dc->dc_flags & SUDCF_BLINK_NOW) &&
      ((sui_test_flag(wdg, SUFL_BLINKFULL) ||
        (sui_style_test_flag(wdg->style, SUSFL_BLINKEDITED) &&
        sui_is_flag(wdg, SUFL_FOCUSED | SUFL_EDITED, SUFL_FOCUSED | SUFL_EDITED)))))
    wflg ^= sui_widget_current_blink_flags;

  fgcolor = color + (wflg * SUI_COLOR_SUBBLOCK);
  if (wdg && (fgcolor < wdg->style->coltab->count))
    sui_gdi_set_fgcolor(dc, wdg->style->coltab->ctab[fgcolor]);
}

/**
 * sui_set_widget_bgcolor - Set device background color from a widget colortable and the current widget mode
 * @dc: Pointer to a device context.
 * @wdg: Pointer to a widget.
 * @color: Index of required color in the widget colortable
 *
 * The function sets device background color from the widget colortable and a required color index.
 * The required color index is only selection of function of color (widget frame, widget background,...)
 * The entered index is updated according to the current widget mode (normal, highlighted, selected, HG+SEL).
 *
 * Return Value: The function does not return a value.
 *
 * File: sui_widget.c
 */
void sui_set_widget_bgcolor(sui_dc_t *dc, sui_widget_t *wdg, int color)
{
  sui_color_t bgcolor;
  sui_flags_t wflg = sui_wdgclr_state(wdg);

  if ((dc->dc_flags & SUDCF_BLINK_NOW) &&
      ((sui_test_flag(wdg, SUFL_BLINKFULL) ||
        (sui_style_test_flag(wdg->style, SUSFL_BLINKEDITED) &&
        sui_is_flag(wdg, SUFL_FOCUSED | SUFL_EDITED, SUFL_FOCUSED | SUFL_EDITED)))))
    wflg ^= sui_widget_current_blink_flags;

  color = color + (wflg * SUI_COLOR_SUBBLOCK);
  if (wdg && (color < wdg->style->coltab->count)) {
    bgcolor = wdg->style->coltab->ctab[color];
    sui_gdi_set_bgcolor(dc, bgcolor);
    sui_gdi_set_usebgrd(dc, (unsigned char)(bgcolor != SUI_NOBG_COLOR));
  }
}

/**
 * sui_draw_frame - Draw a frame according to the widget settings
 * @dc: Pointer to a device context.
 * @wdg: Pointer to a widget.
 * @box: Pointer to a rectange structure.(Frame is out of the rectangle !)
 * @size: Rectangle width.
 *
 * The function draws the widget's frame according to its settings (widget mode, widget 3D mode, ...).
 *
 * Return Value: The function does not return a value.
 *
 * File: sui_widget.c
 */
void sui_draw_frame(sui_dc_t *dc, sui_widget_t *wdg, sui_rect_t *box,
                    sui_coordinate_t size)
{
  sui_coordinate_t i, j;
  int reverse = 0;

  if (size < 0) {
    reverse = 1;
    size = -size;
  }

  if (sui_style_test_flag(wdg->style, SUSFL_ROUNDEDFRAME))
    j = (size == 1) ? 0 : 2; /* j=1 for size==1, j=2 for size > 1 */
  else
    j = -1;
/* set light color */
  if (sui_style_test_flag(wdg->style, SUSFL_FOCUSFRAME)) /* select color according to widget state */
    sui_set_widget_fgcolor(dc, wdg, reverse ? SUC_DKFRAME : SUC_LTFRAME);
  else /* use always only the main (normal) color */
    sui_set_fgcolor(dc, wdg, reverse ? SUC_DKFRAME : SUC_LTFRAME);
  /* draw left and top lines */
  for (i = 0; i < size; i++) {
    if ((i == size-1) && (j>=0)) {
      sui_draw_line(dc, box->x-i+j, box->y-i-1, box->x+box->w+i-j-1, box->y-i-1);
      sui_draw_line(dc, box->x-i-1, box->y-i+j, box->x-i-1, box->y+box->h+i-j-1);
    } else {
      sui_draw_line(dc, box->x-i-1, box->y-i-1, box->x+box->w+i, box->y-i-1);
      sui_draw_line(dc, box->x-i-1, box->y-i, box->x-i-1, box->y+box->h+i-1);
    }
  }
/* set second color */
  if (sui_style_test_flag(wdg->style, SUSFL_SEEMS3D)) {
    if (sui_style_test_flag(wdg->style, SUSFL_FOCUSFRAME)) /* select color according to widget state */
      sui_set_widget_fgcolor(dc, wdg, reverse ? SUC_LTFRAME : SUC_DKFRAME);
    else /* use always only the main (normal) color */
      sui_set_fgcolor(dc, wdg, reverse ? SUC_LTFRAME : SUC_DKFRAME);
  }
  /* draw right and bottom lines */
  for (i = 0; i < size; i++) {
    if ((i == size-1) && (j>=0)) {
      sui_draw_line(dc, box->x-i+j, box->y+box->h+i, box->x+box->w+i-j-1, box->y+box->h+i);
      sui_draw_line(dc, box->x+box->w+i, box->y-i+j, box->x+box->w+i, box->y+box->h+i-j-1);
    } else {
      sui_draw_line(dc, box->x-i-1, box->y+box->h+i,
                    box->x+box->w+i, box->y+box->h+i);
      sui_draw_line(dc, box->x+box->w+i, box->y-i,
                    box->x+box->w+i, box->y+box->h+i-1);
    }
  }
}

/**
 * sui_draw_widget_background - Draw widget background according its settings
 * @dc : Pointer to a device context
 * @widget: Pointer to a widget.
 *
 * The function draws widget background, if it's present
 *
 * Return Value: The function does not return a value.
 *
 * File: sui_widget.c
 */
void sui_draw_widget_background(sui_dc_t *dc, sui_widget_t *widget)
{
  if (!sui_style_test_flag(widget->style, SUSFL_TRANSPARENT)) {
    int clr = SUC_BACKGROUND;
    if (!sui_style_test_flag(widget->style, SUSFL_FOCUSFRAME)) {
      sui_flags_t wflg = sui_wdgclr_state(widget);
      if ((dc->dc_flags & SUDCF_BLINK_NOW) &&
          (sui_test_flag(widget, SUFL_BLINKFULL) ||
            (sui_style_test_flag(widget->style, SUSFL_BLINKEDITED) &&
            sui_is_flag(widget, SUFL_FOCUSED | SUFL_EDITED, SUFL_FOCUSED | SUFL_EDITED))))
        wflg ^= sui_widget_current_blink_flags;
      clr += wflg * SUI_COLOR_SUBBLOCK;
    }
    sui_set_fgcolor(dc, widget, clr);
    sui_draw_rect(dc, 0, 0, widget->place.w, widget->place.h, 0);
  }
}

/**
 * sui_draw_widget_frame - Draw a widget frame according to the widget placement and setting
 * @dc: Pointer to a device context.
 * @widget: Pointer to a widget.
 *
 * The function draws widget frame and potentially its shadow.
 *
 * Return Value: The function does not return a value.
 *
 * File: sui_widget.c
 */
void sui_draw_widget_frame(sui_dc_t *dc, sui_widget_t *widget)
{
  sui_rect_t box = {.x = 0, .y = 0, .w = widget->place.w, .h = widget->place.h};
  int down = sui_test_flag(widget, SUFL_WIDGETISDOWN);

  if (!sui_test_flag(widget, SUFL_INVISIBLE | SUFL_DISABLED)) {
    if (sui_style_test_flag(widget->style, SUSFL_FRAME)) {
      sui_draw_frame(dc, widget, &box, widget->style->frame);
      box.w += widget->style->frame;
      box.h += widget->style->frame;
    }
    if (sui_style_test_flag(widget->style, SUSFL_SHADOW) && !down) {
      sui_set_fgcolor(dc, widget, SUC_SHADOW);
      if (sui_style_test_flag(widget->style, SUSFL_ROUNDEDFRAME)) {
        if (widget->style->frame==1) {
          box.x = 1;
          box.y = 1;
          sui_draw_line(dc, box.w-1, box.h-1, box.w-1, box.h-1);
        } else {
          box.x = 2;
          box.y = 3;
          sui_draw_line(dc, box.w-3, box.h-1, box.w-1, box.h-1);
          sui_draw_line(dc, box.w-1, box.h-3, box.w-1, box.h-2);
        }
      } else {
        box.x = 0;
      }
      sui_draw_line(dc, box.w, box.x, box.w, box.h-box.y);
      sui_draw_line(dc, box.x, box.h, box.w-box.y, box.h);
    }
  }
}

/**
 * sui_draw_widget_label - Draw widget label according to widget settings and alignment
 * @dc: Pointer to a device context.
 * @widget: Pointer to a widget.
 *
 * The function draws widget label, label frame and background according to the widget settings.
 *
 * Return Value: The function does not return a value.
 *
 * File: sui_widget.c
 */
void sui_draw_widget_label(sui_dc_t *dc, sui_widget_t *widget)
{
  sui_point_t origin;
  sui_textdims_t txtsize;
  sui_point_t box = {.x=widget->place.w, .y=widget->place.h};
  sui_align_t al = 0;
  
  if (!widget->label) return;
  sui_widget_get_label_size(dc, widget, &txtsize);
  
  origin.x = txtsize.w;
  origin.y = txtsize.h;
  al = sui_get_widget_labalign(widget);
  if (sui_style_test_flag(widget->style, SUSFL_TITLE)) al |= SUAL_INSIDE;
  
  sui_align_object(&box, &origin, &origin, txtsize.b, widget->style, al | SUAL_INT_LABEL);

  /* set color */
  if (sui_style_test_flag(widget->style, SUSFL_FOCUSLABEL)) {
    sui_set_widget_fgcolor(dc, widget, SUC_LABEL);
    sui_set_widget_bgcolor(dc, widget, SUC_LABELBACK);
  } else {
    sui_set_fgcolor(dc, widget, SUC_LABEL);
    sui_set_bgcolor(dc, widget, SUC_LABELBACK);
  }

  if (sui_style_test_flag(widget->style, SUSFL_TITLE)) { /* draw label as widget title */
    origin.y = txtsize.h + 2;
    sui_draw_line(dc, 0, origin.y, widget->place.w, origin.y);
    origin.y = 1;

  } else { /* label frame */
    if (sui_style_test_flag(widget->style, SUSFL_LABELHASFRAME)) {
      if ((al & SUAL_INSIDE) &&
        ((al & SUAL_HORMASK) == SUAL_BLOCK)) {
        sui_coordinate_t s = widget->style->gap;
        if (sui_style_test_flag(widget->style, SUSFL_LABELHASBACK)) { /* draw only label background up to frame */
          if (sui_style_test_flag(widget->style, SUSFL_FOCUSLABEL))
            sui_set_widget_fgcolor(dc, widget, SUC_LABELBACK);
          else
            sui_set_fgcolor(dc, widget, SUC_LABELBACK);
          sui_draw_rect(dc, s, s, widget->place.w - 2*s, txtsize.h+2, 0);
          if (sui_style_test_flag(widget->style, SUSFL_FOCUSLABEL))
            sui_set_widget_fgcolor(dc, widget, SUC_LABEL);
          else
            sui_set_fgcolor(dc, widget, SUC_LABEL);
        } /* draw label frame */
        sui_draw_rect(dc, s, s, widget->place.w - 2*s, txtsize.h+2, 1);

        origin.x += s;
      } else {
        sui_draw_rect(dc, origin.x-1, origin.y-1, txtsize.w+2, txtsize.h+2, 1);
      }
      origin.x++; origin.y++; // move text to right by one pixel (label frame)
    }
  }
/* label - label font was set in sui_get_label_size */
  sui_font_set(dc, sui_font_prepare(dc, widget->style->labfonti->font, widget->style->labfonti->size));
  sui_draw_text(dc, origin.x, origin.y, -1, widget->label, al);
}  

/**
 * sui_widget_draw - Basic widget drawing function
 * @dc: Pointer to a device context.
 * @widget: Pointer to a widget.
 * @fnc: Pointer to widget content drawing function.
 *
 * The function draws widget - background, frame and inside label. And then it calls
 * the widget specific function which draws the widget content.
 *
 * Return Value: The function does not return a value.
 *
 * File: sui_widget.c
 */
void sui_widget_draw(sui_dc_t *dc, sui_widget_t *widget, sui_draw_content_fnc_t *fnc)
{
  sui_rect_t box = { .x=0, .y=0, .w=widget->place.w, .h=widget->place.h};
  if (dc && widget && widget->style && widget->style->coltab && !sui_test_flag(widget, SUFL_INVISIBLE)) {
    sui_draw_init(dc, widget);
    sui_draw_widget_background(dc, widget);
    sui_draw_widget_frame(dc, widget);

    if (!(dc->dc_flags & SUDCF_BLINK_NOW && sui_test_flag(widget, SUFL_BLINKCONTENT))) {
      sui_gdi_set_carea(dc, &box);
      if (fnc) {
        fnc(dc, widget); // draw content
      }
      if (sui_get_widget_labalign(widget) & SUAL_INSIDE) {
        sui_gdi_set_carea(dc, &box);
        sui_draw_widget_label(dc, widget); // draw inside label
      }
      sui_gdi_clear_carea(dc);
    }
    sui_draw_done(dc, widget);
  }
}

/******************************************************************************
 * Widget support functions
 ******************************************************************************/
/**
 * sui_widget_get_label_size - Get text size of a widget label
 * @dc: Pointer to a device context.
 * @widget: Pointer to a widget.
 * @size: Pointer to an output label dimension buffer.
 *
 * The function gets size of the widget label and sets it to the output buffer.
 *
 * Return Value: The function returns zero as success and a negative value if any error occured.
 *
 * File: sui_widget.c
 */
inline
int sui_widget_get_label_size(sui_dc_t *dc, sui_widget_t *widget,
                              sui_textdims_t *size)
{
  sui_dcfont_t *pfnt;
  if (!widget || !size) return -1;
  if (!widget->label) {
    size->w = size->h = size->b = 0;
    return 0;
  }
  if (!widget->style || !widget->style->labfonti) return -1;
  pfnt = sui_font_prepare(dc, widget->style->labfonti->font,
                          widget->style->labfonti->size);
  return sui_text_get_size(dc, pfnt, size, -1, widget->label, sui_get_widget_labalign(widget));
}

/**
 * sui_widget_get_placement - Get widget position and full size (with frame)
 * @widget: Pointer to a widget.
 * @box: Pointer to an output buffer for widget placement and dimensions.
 *
 * The function gets and sets @box with the widget position and the widget size with frame and shadow.
 *
 * Return Value: The function returns zero as success and a negative value if any error occured.
 *
 * File: sui_widget.c
 */
int sui_widget_get_placement(sui_widget_t *widget, sui_rect_t *box)
{
  if (!widget || !widget->style || !box) return -1;
  box->x = widget->place.x;
  box->y = widget->place.y;
  box->h = widget->place.h;
  box->w = widget->place.w;

  if (!widget->style && sui_style_test_flag(widget->style, SUSFL_FRAME)) {
    sui_coordinate_t fr = widget->style->frame;
    box->x -= fr;
    box->y -= fr;
    box->w += 2*fr;
    box->h += 2*fr;
    if (sui_style_test_flag(widget->style, SUSFL_SHADOW)) {
      box->w += widget->style->gap;
      box->h += widget->style->gap;
    }
  }
  return 0;
}

/** sui_widget_compute_size - Count dimensions of a widget's content
 * @dc: Pointer to a device context.
 * @widget: Pointer to a widget.
 * @size: Pointer to an output buffer for widget content dimensions.
 *
 * The function counts the widget content dimensions and sets them to the $size buffer.
 *
 * Return Value: The function returns zero as success and a negative value if any error occured.
 *
 * File: sui_widget.c
 */
int sui_widget_compute_size(sui_dc_t *dc, sui_widget_t *widget, sui_point_t *size)
{
  sui_textdims_t tsize;
  sui_coordinate_t gap = widget->style->gap;
  sui_align_t al = sui_get_widget_labalign(widget);
  sui_point_t wcontent;

  sui_hevent_command(widget, SUCM_GETSIZE, dc, (long) &wcontent);

  if (widget->style->flags & SUSFL_FRAME) {
    wcontent.x += 2*gap;
    wcontent.y += 2*gap;
  }
  if (al & SUAL_INSIDE) {
    sui_widget_get_label_size(dc, widget, &tsize);
    switch(al & SUAL_HORMASK) {
      case SUAL_LEFT: case SUAL_RIGHT:
        wcontent.x += tsize.w + gap;
        break;
      default:
        if (wcontent.x < tsize.w + 2*gap)
                wcontent.x = tsize.w + 2*gap;
        break;
    }
    switch(al & SUAL_VERMASK) {
      case SUAL_TOP: case SUAL_BOTTOM:
        wcontent.y += tsize.h + gap;
        break;
      default:
        if (wcontent.y < tsize.h + gap)
                wcontent.y = tsize.h + 2*gap;
        break;
    }
  }
  if (size) {
    size->x = wcontent.x;
    size->y = wcontent.y;
  }
  return 0;
}

/**
 * sui_widget_get_item_align - Get a widget content aligned to the widget box
 * @widget: Pointer to a widget
 *
 * The function gets and returns aligment flags of the widget content according to widget alignment settings.
 *
 * Return Value: The function returns aligment flags.
 *
 * File: sui_widget.c
 */
sui_align_t sui_widget_get_item_align(sui_widget_t *widget)
{
  sui_align_t al = SUAL_INSIDE | SUAL_CENTER;
  if (widget && (sui_get_widget_labalign(widget) & SUAL_INSIDE))
    al = sui_get_complementary_align(sui_get_widget_labalign(widget));
  return al;
}

/******************************************************************************
 * Widget key-table structures and functions
 ******************************************************************************/
/**
 * sui_key_table_inc_refcnt - Increment key-table reference counter
 * @key_table: Pointer to a key-table.
 *
 * The function increments key-table reference counter.
 *
 * Return Value: The function returns value of the key-table's reference counter after incrementing.
 *
 * File: sui_widget.c
 */
sui_refcnt_t sui_key_table_inc_refcnt(sui_key_table_t *key_table)
{
  if (!key_table) return SUI_REFCNTERR;
  if (key_table->refcnt>=0) key_table->refcnt++;
  return key_table->refcnt;
}

/**
 * sui_key_table_dec_refcnt - Decrement key-table reference counter
 * @key_table: Pointer to key-table.
 *
 * The function decrements key-table reference counter.
 * If the counter is decremented to zero key-table will be deleted.
 *
 * Return Value: The function returns value of the key-table's reference counter after decrementing.
 *
 * File: sui_widget.c
 */
sui_refcnt_t sui_key_table_dec_refcnt(sui_key_table_t *key_table)
{
  sui_refcnt_t ref;
  if (!key_table) return SUI_REFCNTERR;
  if (key_table->refcnt>0) key_table->refcnt--;
  if (!(ref=key_table->refcnt)) {
    if (key_table->actions) {
      if (key_table->maxcnt) free(key_table->actions); /* free dynamically created array only */
      key_table->actions = NULL;
    }
    if (key_table->next) {
      sui_key_table_dec_refcnt(key_table->next);
      key_table->next = NULL;
    }
    sui_obj_done_vmt((sui_obj_t *)key_table, UL_CAST_UNQ1(sui_obj_vmt_t *,
                     UL_CAST_UNQ1(sui_obj_vmt_t *, &sui_keytable_vmt_data)));
    free(key_table);
  }
  return ref;
}

/**
 * sui_key_table_create - Create dynamic key-table structure
 * @act: Pointer to a key-actions array.
 *
 * The function creates dynamic array of key-actions description
 * from list of key-actions.
 *
 * Return Value: The function returns a pointer to the key-table structure.
 *
 * File: sui_widget.c
 */
sui_key_table_t *sui_key_table_create(sui_key_action_t *actions)
{
  sui_key_table_t *pkf = sui_malloc(sizeof(sui_key_table_t));
  if (pkf) {
    sui_key_table_inc_refcnt(pkf);
    pkf->actions = actions;
    while (actions && actions->hkey) { /* set number of action in the array */
      pkf->actcnt++;
      actions++;
    }
  }
  return pkf;
}

/**
 * sui_hkey_trans_key - Key-action function for translating a key-action to a keyboard event
 * @widget: Pointer to a widget.
 * @info: A keyboard event keycode.
 *
 * The function creates keyboard press event and then pushes it into event fifo.
 *
 * Return Value: The function does not return a value.
 *
 * File: sui_widget.c
 */
void sui_hkey_trans_key(sui_widget_t *widget, long info)
{
  sui_event_t event;
  event.what=SUEV_KDOWN;
  event.keydown.keycode=(unsigned short)(info & 0xffff);
  event.keydown.modifiers=0;
  event.keydown.scancode=0;
  sui_put_event(&event);
}

/**
 * sui_hkey_trans_cmd - Key-action function for translating a key-action to a command event
 * @widget: Pointer to a widget.
 * @info: A command event type.
 *
 * The function creates command event and then pushes it into event fifo.
 *
 * Return Value: The function does not return a value.
 *
 * File: sui_widget.c
 */
void sui_hkey_trans_cmd(sui_widget_t *widget, long info)
{
  sui_event_t event;
  event.what=SUEV_COMMAND;
  event.message.command=(unsigned short)(info & 0xffff);
  event.message.ptr=NULL;
  event.message.info=0;
  sui_put_event(&event);
}

/**
 * sui_hkey_trans_global - Key-action function for translating a key-action to a global event
 * @widget: Pointer to a widget.
 * @info: High word is a global type and low word is an event context.
 *
 * The function creates global command event and then pushes it onto event FIFO.
 *
 * Return Value: The function does not return a value.
 *
 * File: sui_widget.c
 */
void sui_hkey_trans_global(sui_widget_t *widget, long info)
{
  sui_event_t event;
  event.what=SUEV_GLOBAL;
  event.message.command=(info>>16);
  event.message.ptr=widget;
  event.message.info=info&0xffff;
  sui_put_global_event(&event);
}


/******************************************************************************
 * Widget event structures and functions
 ******************************************************************************/
/**
 * sui_draw_request - Set global widget draw request flag
 * @widget: Pointer to a widget.
 *
 * The function sets global widget draw request flag to SEUV_DRAW if
 * the draw flag was set to SEUV_REDRAW otherwise it is set to SEUV_REDRAW.
 *
 * Return Value: The function does not return a value.
 *
 * File: sui_widget.c
 */
void sui_draw_request(sui_widget_t *widget)
{
  if(widget) {
    widget->damage|=SUI_DAMAGE_ALL;
    if(!curevbufs->draw_request)
      curevbufs->draw_request=SUEV_REDRAW;
  } else
    curevbufs->draw_request=SUEV_DRAW;
}
  
/**
 * sui_redraw_request - Set global widget draw request flag
 * @widget: Pointer to a widget.
 *
 * The function sets global widget draw request flag to SUEV_REDRAW.
 *
 * Return Value: The function does not return a value.
 *
 * File: sui_widget.c
 */
void sui_redraw_request(sui_widget_t *widget)
{
  if(widget) {
    widget->damage|=SUI_DAMAGE_REDRAW;
    if(!curevbufs->draw_request)
      curevbufs->draw_request=SUEV_REDRAW;
  }
}

/* TODO: FIXME: check all about damaging effects to widgets */
/**
 * sui_damage - Set a widget as damaged
 * @widget: Pointer to a widget.
 *
 * The function sets widget damage flags to SUI_DAMAGE_ALL.
 *
 * Return Value: The function does not return a value.
 *
 * File: sui_widget.c
 */
inline
void sui_damage(sui_widget_t *widget)
{
  if (widget) {
    widget->damage |= SUI_DAMAGE_ALL;
  }
}
/**
 * sui_damage_parent - Set a widget's parent as damaged
 * @widget: Pointer to a widget.
 *
 * The function sets widget's parent damage flags to SUI_DAMAGE_ALL.
 *
 * Return Value: The function does not return a value.
 *
 * File: sui_widget.c
 */
inline
void sui_damage_parent(sui_widget_t *widget)
{
  if (widget && widget->parent) {
    widget->parent->damage |= SUI_DAMAGE_ALL;
  }
}



/**
 * sui_widget_is_focusable - Test if a widget can have focus
 * @widget: Pinter to a widget.
 *
 * The function tests the widget focusability by sending command event FOCUSASK
 * to the widget and then returns answer.
 *
 * Return Value: The function returns 1 if the widget is focusable otherwise it returns 0.
 *
 * File: sui_widget.c
 */
inline
int sui_widget_is_focusable(sui_widget_t *widget)
{
  sui_event_t ev;
  ev.what = SUEV_COMMAND;
  ev.message.command = SUCM_FOCUSASK;
  if (!widget) return 0;
  sui_hevent(widget, &ev);
  return (ev.what == SUEV_NOTHING) ? 1 : 0;
}

/******************************************************************************
 * Basic widget functions
 ******************************************************************************/
/**
 * sui_inc_refcnt - Increment widget reference counter
 * @widget: Pointer to a widget.
 *
 * The function increments widget's reference counter.
 *
 * Return Value: The function returns value of the widget's reference counter after incrementing.
 *
 * File: sui_widget.c
 */
sui_refcnt_t sui_inc_refcnt(sui_widget_t *widget)
{
  if (!widget) return SUI_REFCNTERR;
  if (widget->refcnt>=0) widget->refcnt++;
  return widget->refcnt;
}

/**
 * sui_dec_refcnt - Decrement widget reference counter
 * @widget: Pointer to a widget.
 *
 * The function decrements widget's reference counter and if reference
 * counter is decremented to zero, SUEV_FREE event is sended to the
 * widget hevent function. If the widget haven't defined the hevent
 * function, the widget is deleted.
 *
 * Return Value: The function returns value of the widget's reference counter after decrementing.
 *
 * File: sui_widget.c
 */
sui_refcnt_t sui_dec_refcnt(sui_widget_t *widget)
{
  sui_refcnt_t ref;
  if (!widget) return SUI_REFCNTERR;
  if (widget->refcnt>0) widget->refcnt--;
  if (!(ref=widget->refcnt)) {
//ul_logdeb("widget done\n");
    sui_obj_done((sui_obj_t *) widget);
    free(widget);
  }
  return ref;
}

/******************************************************************************
 * Basic widget assignment function
 ******************************************************************************/
/**
 * sui_widget_assign_key_table - Assign a key-table to a widget
 * @widget: Pointer to a widget.
 * @newkeys: Pointer to a key-table.
 *
 * The function assigns the entered key-table to the widget (old key table is removed).
 *
 * Return Value: The function returns zero as success and -1 as an error.
 *
 * File: sui_widget.c
 */
inline
int sui_widget_assign_key_table(sui_widget_t *widget, sui_key_table_t *newkeys)
{
  if (!widget) return -1;
  if (widget->key_table) sui_key_table_dec_refcnt(widget->key_table);
  sui_key_table_inc_refcnt(newkeys);
  widget->key_table = newkeys;
  return 0;
}


/******************************************************************************
 * widget slot functions
 ******************************************************************************/
/**
 * sui_widget_show - Set a widget as visible
 * @widget: Pointer to a widget.
 *
 * The slot function shows the widget and emits signal ENABLED if
 * the widget has been hidden.
 *
 * Return Value: The slot function returns zero as success and -1 as an error.
 *
 * File: sui_widget.c
 */
int sui_widget_show(sui_widget_t *widget)
{
  if (widget && sui_test_flag(widget, SUFL_INVISIBLE)) {
    sui_clear_flag(widget, SUFL_INVISIBLE);
    sui_damage_parent(widget);
    sui_obj_emit((sui_obj_t *)widget, SUI_SIG_SHOWED);
  }
  return 0;
}

/**
 * sui_widget_hide - Set a widget as hidden
 * @widget: Pointer to a widget.
 *
 * The slot function hides the widget and emits signal HIDDEN if
 * the widget has been visibled.
 *
 * Return Value: The slot function returns zero as success and -1 as an error.
 *
 * File: sui_widget.c
 */
int sui_widget_hide(sui_widget_t *widget)
{
  if (widget && !sui_test_flag(widget, SUFL_INVISIBLE)) {
    sui_set_flag(widget, SUFL_INVISIBLE);
    sui_damage_parent(widget);
    sui_obj_emit((sui_obj_t *)widget, SUI_SIG_HIDDEN);
  }
  return 0;
}

/**
 * sui_widget_enable - Set a widget as enabled
 * @widget: Pointer to a widget.
 *
 * The slot function removes the widget's disable mark and emits signal ENABLED if
 * the widget has been disabled.
 *
 * Return Value: The slot function returns zero as success and -1 as an error.
 *
 * File: sui_widget.c
 */
int sui_widget_enable(sui_widget_t *widget)
{
  if (widget && sui_test_flag(widget, SUFL_DISABLED)) {
    sui_clear_flag(widget, SUFL_DISABLED);
    sui_obj_emit((sui_obj_t *)widget, SUI_SIG_ENABLED);
  }
  return 0;
}

/**
 * sui_widget_disable - Set a widget as disabled
 * @widget: Pointer to a widget.
 *
 * The slot function marks the widget as disabled and emits signal DISABLED if
 * the widget has been enabled.
 *
 * Return Value: The slot function returns zero as success and -1 as an error.
 *
 * File: sui_widget.c
 */
int sui_widget_disable(sui_widget_t *widget)
{
  if (widget && !sui_test_flag(widget, SUFL_DISABLED)) {
    sui_set_flag(widget, SUFL_DISABLED);
    sui_obj_emit((sui_obj_t *)widget, SUI_SIG_DISABLED);
  }
  return 0;
}

/**
 * sui_widget_setfocus - Set a widget as focused
 * @widget: Pointer to a widget.
 *
 * The slot function marks the widget as focused and emits signal FOCUSED if
 * the widget can be focused and it haven't been focused.
 *
 * Return Value: The slot function returns zero as success and -1 as an error.
 *
 * File: sui_widget.c
 */
int sui_widget_setfocus(sui_widget_t *widget)
{
  if (sui_test_flag(widget, SUFL_FOCUSEN) &&
      !sui_test_flag(widget, SUFL_FOCUSED)) {
//    ul_logdeb("setFocus-%p (flg=0x%lX)\n", widget, sui_widget_current_focus_flags);
    sui_set_flag(widget, sui_widget_current_focus_flags);
    sui_obj_emit((sui_obj_t *)widget, SUI_SIG_FOCUSED, (void *)widget);
    return 0;
  }
  return -1;
}

/**
 * sui_widget_clearfocus - Remove focus from a widget
 * @widget: Pointer to a widget.
 *
 * The slot function removes widget's focus mark and emits signal UNFOCUSED if
 * the widget has focused.
 *
 * Return Value: The slot function returns zero as success and -1 as an error.
 *
 * File: sui_widget.c
 */
int sui_widget_clearfocus(sui_widget_t *widget)
{
  if (sui_test_flag(widget, SUFL_FOCUSED)) {
//    ul_logdeb("clearFocus-%p\n", widget);
    sui_clear_flag(widget, sui_widget_current_focus_flags);
    sui_obj_emit((sui_obj_t *)widget, SUI_SIG_UNFOCUSED, (void *)widget);
    return 0;
  }
  return -1;
}

/**
 * sui_widget_move - Change position of a widget
 * @widget: Pointer to a widget.
 * @x: A new horizontal widget's position.
 * @y: A new vertical widget's position.
 *
 * The slot function changes position of the widget to x,y and emits signal MOVED.
 *
 * Return Value: The slot function returns zero as success and -1 as an error.
 *
 * File: sui_widget.c
 */
int sui_widget_move(sui_widget_t *widget, sui_coordinate_t x, sui_coordinate_t y)
{
  if (widget && ((widget->place.x != x) || (widget->place.y != y))) {
    widget->place.x = x;
    widget->place.y = y;
    sui_damage_parent(widget);
    sui_obj_emit((sui_obj_t *)widget, SUI_SIG_MOVED, x, y);
  }
  return 0;
}

/**
 * sui_widget_resize - Change dimensions of a widget
 * @widget: Pointer to a widget.
 * @w: A new widget's width.
 * @h: A new widget's height.
 *
 * The slot function changes size of the widget to w,h and emits signal RESIZED.
 *
 * Return Value: The slot function returns zero as success and -1 as an error.
 *
 * File: sui_widget.c
 */
int sui_widget_resize(sui_widget_t *widget, sui_coordinate_t w, sui_coordinate_t h)
{
  if (widget && ((widget->place.w != w) || (widget->place.h != h))) {
    widget->place.w = w;
    widget->place.h = h;
    sui_damage_parent(widget);
    widget->damage |= SUI_DAMAGE_REDRAW;
    sui_obj_emit((sui_obj_t *)widget, SUI_SIG_RESIZED, w, h);
  }
  return 0;
}

/**
 * sui_widget_setfocusen - Set/unset widget's focusability
 * @widget: Pointer to a widget.
 * @state: New widget's focusability - 0=widget won't be focusable, 1=it'll be focusable.
 *
 * The slot function sets/unsets widget's focusability and if the widget has focus, it
 * is cleared.
 *
 * Return Value: The slot function returns zero as success and -1 as an error.
 *
 * File: sui_widget.c
 */
int sui_widget_setfocusen(sui_widget_t *widget, int state)
{
  if (state) {
    if (!sui_test_flag(widget, SUFL_FOCUSEN)) {
      sui_set_flag(widget, SUFL_FOCUSEN);
      /* emit signal FOCUSEN */
      //sui_obj_emit((sui_obj_t *)widget, SUI_SIG_FOCUSED, (void *)widget);
    }
  } else {
    if (sui_test_flag(widget, SUFL_FOCUSEN)) {
      if (sui_test_flag(widget, SUFL_FOCUSED))
        sui_widget_clearfocus(widget);
      sui_clear_flag(widget, SUFL_FOCUSEN);
      /* emit signal UNFOCUSEN */
      //sui_obj_emit((sui_obj_t *)widget, SUI_SIG_FOCUSED, (void *)widget);
    }
  }
  return 0;
}


/**
 * sui_widget_blink_on - Switch on widget blinking with the SUFL_BLINKFULL flag
 * @widget: Pointer to a widget.
 *
 * The slot function sets SUFL_BLINKFULL flag to the widget.
 *
 * Return Value: The slot function returns zero as success and -1 as an error.
 *
 * File: sui_widget.c
 */
int sui_widget_blink_on(sui_widget_t *widget)
{
  if (widget) {
    widget->flags |= SUFL_BLINKFULL;
    sui_damage_parent(widget);
    widget->damage |= SUI_DAMAGE_REDRAW;
  }
  return 0;
}

/**
 * sui_widget_blink_off - Switch off widget blinking with the SUFL_BLINKFULL flag
 * @widget: Pointer to a widget.
 *
 * The slot function clears the  SUFL_BLINKFULL flag from the widget.
 *
 * Return Value: The slot function returns zero as success and -1 as an error.
 *
 * File: sui_widget.c
 */
int sui_widget_blink_off(sui_widget_t *widget)
{
  if (widget) {
    widget->flags &= ~SUFL_BLINKFULL;
    sui_damage_parent(widget);
    widget->damage |= SUI_DAMAGE_REDRAW;
  }
  return 0;
}

/**
 * sui_widget_kdown_slot - Invoke event handler for SUEV_KDOWN
 * @widget: Pointer to a widget
 * @key: pressed key
 *
 * The slot function invokes widget event handler for event SUEV_KDOWN.
 *
 * Return Value: The slot function returns zero as success and -1 as an error.
 *
 * File: sui_widget.c
 */
int sui_widget_kdown_slot(sui_widget_t *widget, int key)
{
  sui_event_t event;
  event.what=SUEV_KDOWN;
  event.keydown.keycode=(unsigned short)(key & 0xffff);
  event.keydown.modifiers=0;
  event.keydown.scancode=0;
  sui_hevent(widget, &event);
  return 0;
}

/**
 * sui_widget_blinkcontent_on - Switch on widget blinking with the SUFL_BLINKCONTENT flag
 * @widget: Pointer to a widget.
 *
 * The slot function sets SUFL_BLINKCONTENT flag to the widget.
 *
 * Return Value: The slot function returns zero as success and -1 as an error.
 *
 * File: sui_widget.c
 */
int sui_widget_blinkcontent_on(sui_widget_t *widget)
{
  if (widget) {
    widget->flags |= SUFL_BLINKCONTENT;
    sui_damage_parent(widget);
    widget->damage |= SUI_DAMAGE_REDRAW;
  }
  return 0;
}

/**
 * sui_widget_blinkcontent_off - Switch off widget blinking with the SUFL_BLINKCONTENT flag
 * @widget: Pointer to a widget.
 *
 * The slot function clears the SUFL_BLINKCONTENT flag from the widget.
 *
 * Return Value: The slot function returns zero as success and -1 as an error.
 *
 * File: sui_widget.c
 */
int sui_widget_blinkcontent_off(sui_widget_t *widget)
{
  if (widget) {
    widget->flags &= ~SUFL_BLINKCONTENT;
    sui_damage_parent(widget);
    widget->damage |= SUI_DAMAGE_REDRAW;
  }
  return 0;
}


/******************************************************************************
 * Widget basic functions
 ******************************************************************************/
/**
 * sui_widget_init - Initiate a widget
 * @widget: Pointer to a widget.
 * @dc: Pointer to a device context.
 *
 * The function sends INIT event to the widget and sets request for widget redrawing.
 *
 * Return Value: The function does not return any value.
 *
 * File: sui_widget.c
 */
inline
void sui_widget_init(sui_widget_t *widget, sui_dc_t *dc)
{
  sui_hevent_command(widget, SUCM_INIT, dc, 0);
  sui_draw_request(widget);
}

/**
 * sui_widget_hevent - Main event processing function for generic widgets.
 * @widget: Pointer to a widget.
 * @event: Pointer to an event.
 *
 * The function processes basic events for all types of widgets.
 *
 * Return Value: The function does not return a value.
 *
 * File: sui_widget.c
 */
void sui_widget_hevent(sui_widget_t *widget, sui_event_t *event)
{
//if (event->what != SUEV_DRAW && event->what != SUEV_REDRAW)
//	ul_logdeb("widget(%p)-event(%d)\n",widget,event->what);

  switch(event->what) {
    case SUEV_REDRAW:
    case SUEV_DRAW:
      sui_widget_draw(event->draw.dc, widget, NULL);
      return;

    case SUEV_KDOWN:
      {
        sui_key_action_t *kaction;
        sui_key_table_t *keytable;
        unsigned short kcode,kmod; /* added 7/9/2003 */
        sui_widget_t *parent = widget->parent;
        keytable = widget->key_table;
        while (!keytable && parent) { /* find parent keytable */
          keytable = parent->key_table;
          parent = parent->parent;
        }
        if(!keytable) return;
        kcode=event->keydown.keycode;
        kmod=event->keydown.modifiers;
        if (!(kaction = keytable->actions)) return;
        do {
          if (!kaction->keycode) {
            if (kaction->hkey == SUI_HKEY_CHAIN) {
              kaction = (sui_key_action_t*)kaction->info;
              continue;
            }
            if (keytable->next) {
              keytable = keytable->next;
              if (!(kaction = keytable->actions)) return;
              continue;
            }
            return;
          }
          if (kcode == kaction->keycode && (kmod & kaction->keymask) == kaction->keymod)
            break; /*& added 7/9/2003 */
          kaction++;
        } while(1);

        sui_clear_event(widget,event);
        if (kaction->hkey)    /* check function */
        kaction->hkey(widget,kaction->info);

        sui_obj_emit((sui_obj_t *)widget, SUI_SIG_KEYPRESSED, kcode);

        return;
      }

    case SUEV_COMMAND:
      switch (event->message.command) {
        case SUCM_FOCUSASK:
          if (sui_test_flag(widget, SUFL_FOCUSEN)) {
//            ul_logdeb("focusask (base) %s : %p\n", widget ? sui_utf8_get_text(widget->label) : "-", widget);
            sui_clear_event(widget, event);
          }
          return;

        case SUCM_FOCUSSET:
          if (!sui_widget_setfocus(widget)) {
            sui_redraw_request(widget); // ??? //
            sui_clear_event(widget, event);
          }
          return;

        case SUCM_FOCUSREL:
          if (!sui_widget_clearfocus(widget)) {
            sui_redraw_request(widget);
            sui_clear_event(widget, event);
          }
          return;

        case SUCM_INIT:
          if (widget && widget->style && widget->hevent &&
              widget->style->coltab && widget->style->coltab->ctab) { /* 'sui_clear_event' must not be used because INIT event continues in the widget's specific handle_event function */
            widget->evmask |= SUEV_DEFMASK;
          } else {
            ul_logerr("Widget hasn't set all needed pointers -> ERROR!!!\n");
          }
          return;

        case SUCM_GETSIZE:
          {
            sui_point_t *size = (sui_point_t *)event->message.info;
            if (size) {
              size->x = 0;
              size->y = 0;
              sui_clear_event(widget,event);
            }
          }
          return;
      }
      break;

#ifdef CONFIG_OC_SUITK_WITH_MOUSE
    case SUEV_MDOWN: {
      sui_obj_emit((sui_obj_t *)widget, SUI_SIG_MOUSEDOWN);

      if (sui_test_flag(widget, SUFL_FOCUSEN) &&
          !sui_test_flag(widget, SUFL_FOCUSED)) {
        sui_group_change_focus(NULL, NULL, widget);
        sui_clear_event(widget,event);
      } else {
        sui_event_t synev;
        memset(&synev, 0, sizeof(synev));
        synev.what = SUEV_KDOWN;
        synev.keydown.keycode = MWKEY_ENTER;
        sui_hevent(widget, &synev);
        if (synev.what == SUEV_NOTHING)
          sui_clear_event(widget,event);
      }
    }
    break;
#endif /*CONFIG_OC_SUITK_WITH_MOUSE*/
/*
    case SUEV_FREE:
      free(widget);
      break;
*/
  }
}


/******************************************************************************
 * Basic widget vmt & signal-slot mechanism
 ******************************************************************************/
SUI_CANBE_CONST
sui_obj_signal_tinfo_t sui_widget_signal_tinfo[] = { 
  {"showed", SUI_SIG_SHOWED, &sui_args_tinfo_void},
  {"hidden", SUI_SIG_HIDDEN, &sui_args_tinfo_void},
  {"enabled", SUI_SIG_ENABLED, &sui_args_tinfo_void},
  {"disabled", SUI_SIG_DISABLED, &sui_args_tinfo_void},
  {"focused", SUI_SIG_FOCUSED, &sui_args_tinfo_widget},
  {"unfocused", SUI_SIG_UNFOCUSED, &sui_args_tinfo_widget},
  {"moved", SUI_SIG_MOVED, &sui_args_tinfo_coord2},
  {"resized", SUI_SIG_RESIZED, &sui_args_tinfo_coord2},
  {"keypressed", SUI_SIG_KEYPRESSED, &sui_args_tinfo_int},
  {"mousedown", SUI_SIG_MOUSEDOWN, &sui_args_tinfo_void},
};

SUI_CANBE_CONST
sui_obj_slot_tinfo_t sui_widget_slot_tinfo[] = { 
  {"show", SUI_SLOT_SHOW, &sui_args_tinfo_void,
    .target_kind=SUI_STGT_METHOD_OFFSET,
    .target.method_offset=UL_OFFSETOF(sui_widget_vmt_t,show)},
  {"hide", SUI_SLOT_HIDE, &sui_args_tinfo_void,
    .target_kind=SUI_STGT_METHOD_OFFSET,
    .target.method_offset=UL_OFFSETOF(sui_widget_vmt_t,hide)},
  {"enable", SUI_SLOT_ENABLE, &sui_args_tinfo_void,
    .target_kind=SUI_STGT_METHOD_OFFSET,
    .target.method_offset=UL_OFFSETOF(sui_widget_vmt_t,enable)},
  {"disable", SUI_SLOT_DISABLE, &sui_args_tinfo_void,
    .target_kind=SUI_STGT_METHOD_OFFSET,
    .target.method_offset=UL_OFFSETOF(sui_widget_vmt_t,disable)},
  {"setfocus", SUI_SLOT_SETFOCUS, &sui_args_tinfo_void,
    .target_kind=SUI_STGT_METHOD_OFFSET,
    .target.method_offset=UL_OFFSETOF(sui_widget_vmt_t,setfocus)},
  {"clearfocus", SUI_SLOT_CLEARFOCUS, &sui_args_tinfo_void,
    .target_kind=SUI_STGT_METHOD_OFFSET,
    .target.method_offset=UL_OFFSETOF(sui_widget_vmt_t,clearfocus)},
  {"move", SUI_SLOT_MOVE, &sui_args_tinfo_coord2,
    .target_kind=SUI_STGT_METHOD_OFFSET,
    .target.method_offset=UL_OFFSETOF(sui_widget_vmt_t,move)},
  {"resize", SUI_SLOT_RESIZE, &sui_args_tinfo_coord2,
    .target_kind=SUI_STGT_METHOD_OFFSET,
    .target.method_offset=UL_OFFSETOF(sui_widget_vmt_t,resize)},
  {"setfocusen", SUI_SLOT_SETFOCUSEN, &sui_args_tinfo_int,
    .target_kind=SUI_STGT_METHOD_OFFSET,
    .target.method_offset=UL_OFFSETOF(sui_widget_vmt_t,setfocusen)},
  {"blinkon", SUI_SLOT_BLINKON, &sui_args_tinfo_void,
    .target_kind=SUI_STGT_METHOD_OFFSET,
    .target.method_offset=UL_OFFSETOF(sui_widget_vmt_t, blinkon)},
  {"blinkoff", SUI_SLOT_BLINKOFF, &sui_args_tinfo_void,
    .target_kind=SUI_STGT_METHOD_OFFSET,
    .target.method_offset=UL_OFFSETOF(sui_widget_vmt_t, blinkoff)},
  {"keydown", SUI_SLOT_KDOWN, &sui_args_tinfo_int,
    .target_kind=SUI_STGT_METHOD_OFFSET,
    .target.method_offset=UL_OFFSETOF(sui_widget_vmt_t, kdownslot)},
  {"blconton", SUI_SLOT_BLINKCONTENTON, &sui_args_tinfo_void,
    .target_kind=SUI_STGT_METHOD_OFFSET,
    .target.method_offset=UL_OFFSETOF(sui_widget_vmt_t, blinkconton)},
  {"blcontoff", SUI_SLOT_BLINKCONTENTOFF, &sui_args_tinfo_void,
    .target_kind=SUI_STGT_METHOD_OFFSET,
    .target.method_offset=UL_OFFSETOF(sui_widget_vmt_t, blinkcontoff)},
};

SUI_CANBE_CONST
sui_obj_tinfo_t sui_widget_vmt_obj_tinfo = {
  .name = "sui_widget",
  .obj_size = sizeof(sui_widget_t),
  .obj_align = UL_ALIGNOF(sui_widget_t),
  .signal_count = MY_ARRAY_SIZE(sui_widget_signal_tinfo),
  .signal_tinfo = UL_CAST_UNQ1(sui_obj_signal_tinfo_t *, sui_widget_signal_tinfo),
  .slot_count = MY_ARRAY_SIZE(sui_widget_slot_tinfo),
  .slot_tinfo = UL_CAST_UNQ1(sui_obj_slot_tinfo_t *, sui_widget_slot_tinfo)
};

/*
 * sui_widget_vmt_init - Init VMT widget object
 */
int sui_widget_vmt_init(sui_widget_t *pw, void *params)
{
  pw->type = SUWT_GENERIC;
  pw->hevent = sui_widget_hevent;
  pw->refcnt = 1;
  /*sui_widget_vmt(pw)->inc_refcnt(pw);*/
  return 0;
}

/*
 * sui_widget_vmt_done - Destroy VMT widget object
 */
void sui_widget_vmt_done(sui_widget_t *pw)
{
  sui_widget_t *parentwdg;

  if (pw->style) sui_style_dec_refcnt(pw->style);
  pw->style = NULL;
  if (pw->label) sui_utf8_dec_refcnt(pw->label);
  pw->label = NULL;
  if (pw->tooltip) sui_utf8_dec_refcnt(pw->tooltip);
  pw->tooltip = NULL;
  if (pw->key_table) sui_key_table_dec_refcnt(pw->key_table);
  pw->key_table = NULL;

  parentwdg = pw->parent;
  if(parentwdg != NULL)
    sui_wgroup_vmt(parentwdg)->reclaim_children(parentwdg, pw);

  pw->parent = NULL;
  pw->prev = NULL;
  pw->next = NULL;
}

/*
 * sui_widget_vmt_data - Generic widget VMT description
 */
sui_widget_vmt_t sui_widget_vmt_data = {
  .vmt_size = sizeof(sui_widget_vmt_t),
  .vmt_refcnt = SUI_STATIC,
  .vmt_parent = &sui_obj_vmt_data,
  .vmt_init   = sui_widget_vmt_init,
  .vmt_done   = sui_widget_vmt_done,
  .vmt_obj_tinfo = UL_CAST_UNQ1(sui_obj_tinfo_t *, &sui_widget_vmt_obj_tinfo),

  .inc_refcnt = sui_inc_refcnt,
  .dec_refcnt = sui_dec_refcnt,

  .show = sui_widget_show,
  .hide = sui_widget_hide,
  .enable = sui_widget_enable,
  .disable = sui_widget_disable,
  .setfocus = sui_widget_setfocus,
  .clearfocus = sui_widget_clearfocus,
  .move = sui_widget_move,
  .resize = sui_widget_resize,
  .setfocusen = sui_widget_setfocusen,
  .blinkon = sui_widget_blink_on,
  .blinkoff = sui_widget_blink_off,
  .kdownslot = sui_widget_kdown_slot,
  .blinkconton = sui_widget_blinkcontent_on,
  .blinkcontoff = sui_widget_blinkcontent_off,
};


/******************************************************************************/

/**
 * sui_widget_create_new - Create a new generic widget from VMT data.
 * @vmt_data: Pointer to a VMT data.
 * @aplace: Widget's position and size.
 * @alabel: Widget's label as utf8 string.
 * @astyle: Widget's style.
 * @aflags: Widget's common flags('SUFL_' prefix) ORed with widget type specific flags('SUxF_' prefix).
 *
 * The function creates a new generic widget from the entered VMT data.
 *
 * Return Value: The function returns a pointer to the created widget.
 *
 * File: sui_widget.c
 */
sui_widget_t *sui_widget_create_new(void *vmt_data, const sui_rect_t *aplace,
                      utf8 *alabel, sui_style_t *astyle, unsigned long aflags)
{
  sui_widget_t *pw;

  pw = (sui_widget_t *) sui_obj_new_vmt((sui_obj_vmt_t *) vmt_data);
  if (!(pw))return NULL;

  if (aplace) pw->place = *aplace;
  if (alabel) {
    sui_utf8_inc_refcnt(alabel);
    pw->label = alabel;
    pw->labalign = SUAL_INSIDE | SUAL_TOP | SUAL_LEFT;
  }
  if (astyle) {
    sui_style_inc_refcnt(astyle);
    pw->style = astyle;
    if (astyle->labalign)
      pw->labalign = 0;
  }
  pw->flags = aflags;
  pw->evmask = SUEV_COMMAND; //|SUEV_FREE;
  return pw;
}


/**
 * sui_widget - Create a generic widget.
 * @aplace: Widget's position and size.
 * @alabel: Widget's label as utf8 string.
 * @astyle: Widget's style.
 * @aflags: Widget's common flags('SUFL_' prefix) ORed with widget type specific flags('SUxF_' prefix).
 *
 * The function creates a new widget. It is common part of all widgets.
 *
 * Return Value: The function returns a pointer to the created widget.
 *
 * File: sui_widget.c
 */
inline
sui_widget_t *sui_widget(const sui_rect_t *aplace, utf8 *alabel,
                          sui_style_t *astyle, unsigned long aflags)
{
  return sui_widget_create_new(&sui_widget_vmt_data, aplace, alabel, astyle, aflags);
}
