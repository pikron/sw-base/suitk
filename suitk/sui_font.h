/* sui_font.h
 *
 * SUITK Microwindows and compound builtin fonts
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#ifndef _SUITK_MW_AND_COMPOUND_FONT_
#define _SUITK_MW_AND_COMPOUND_FONT_

#include <device.h>
#include <mwtypes.h>
#include <suiut/sui_types.h>
#include <suiut/sui_utf8.h>

#ifndef VER_CODE
#define VER_CODE(_major, _minor, _patch) \
  (_major * 0x10000 + _minor * 0x100 + _patch)
#endif /*VER_CODE*/

#ifndef MICROWINDOWS_VER_CODE
  #if defined(MICROWINDOWS_VER_MAJOR) || defined(MICROWINDOWS_VER_MINOR)
    /* At least major or minor defined */
    #ifndef MICROWINDOWS_VER_MAJOR
       #define MICROWINDOWS_VER_MAJOR 0
    #endif
    #ifndef MICROWINDOWS_VER_MINOR
       #define MICROWINDOWS_VER_MINOR 0
    #endif
  #elif defined(MICROWINDOWS_093)
    #define MICROWINDOWS_VER_MAJOR 0
    #define MICROWINDOWS_VER_MINOR 93
  #elif defined(MICROWINDOWS_090)
    #define MICROWINDOWS_VER_MAJOR 0
    #define MICROWINDOWS_VER_MINOR 90
  #elif defined(MICROWINDOWS_089)
    #define MICROWINDOWS_VER_MAJOR 0
    #define MICROWINDOWS_VER_MINOR 89
  #endif
  #ifndef MICROWINDOWS_VER_PATCH
    #define MICROWINDOWS_VER_PATCH 0
  #endif
  #if defined(MICROWINDOWS_VER_MAJOR) && defined(MICROWINDOWS_VER_MINOR)
    #define MICROWINDOWS_VER_CODE  VER_CODE(MICROWINDOWS_VER_MAJOR, \
                          MICROWINDOWS_VER_MINOR, MICROWINDOWS_VER_PATCH)
  #endif
#endif /*MICROWINDOWS_VER_CODE*/

#ifndef MICROWINDOWS_VER_CODE /* undefined version of MICROWINDOWS */
  #error Unknown MicroWindows version - MICROWINDOWS_VER_CODE or MICROWINDOWS_VER_MINOR must be defined
#endif /*MICROWINDOWS_VER_CODE*/

/******************************************************************************
 * BuildIn system font X5x7 - this font is main system font
 ******************************************************************************/
//extern MWCOREFONT systemfont_X5x7;


/******************************************************************************
 * Font basic types and structures
 ******************************************************************************/
/* font info type */
typedef MWFONTINFO sui_fontinfo_t;
/* screen font type */
typedef struct _mwfont sui_dcfont_t;
/* font name type */
typedef utf8 *sui_font_t;

/**
 * struct sui_font_info - Font info common structure 
 * @refcnt: Reference counter of the font info structure.
 * @font: Name of the font.
 * @size: Size of the font. Now it isn't used, because all fonts are only fixed size.
 * @flag: Font flags. Now it isn't used. For features - rotate, column, mirror, ...
 *
 * Reference counting font structure. All fonts in SUITK are accessed throught this structure.
 * If refcnt is negative, structure is static and cannot be deallocated.
 * File: sui_base.h
 */
  typedef struct sui_font_info {
	sui_refcnt_t     refcnt; /* reference counter */
	sui_font_t       font;   /* font */
	int              size;   /* font size */
	unsigned short   flag;   /* font flags - rotate, column, .... in the feature */
	/* here can be next fields - space between chars, between words,.... */
  } sui_font_info_t;

sui_refcnt_t sui_fonti_dec_refcnt( sui_font_info_t *fonti);
sui_refcnt_t sui_fonti_inc_refcnt( sui_font_info_t *fonti);

sui_font_info_t *sui_fonti_create( sui_font_t afont, int asize, unsigned short aflag);



#if !defined(MWTEXTFLAGS) && (MICROWINDOWS_VER_CODE<VER_CODE(0,90,0))
	#define MWTEXTFLAGS unsigned long
#endif /* !defined(MWTEXTFLAGS) && MICROWINDOWS_VER_CODE<VER_CODE(0,90,0) */

/* for drawtext function - column text */
#define MWTF_COLUMN_TEXT 0x10000000L
#define MWTF_LEFT        0x20000000L
#define MWTF_RIGHT       0x40000000L
#define MWTF_HCENTER     (MWTF_LEFT | MWTF_RIGHT)
#define MWTF_VCENTER     (MWTF_TOP | MWTF_BOTTOM)

#if (MICROWINDOWS_VER_CODE>=VER_CODE(0,93,0))

	PMWFONT suiGdSetFont(PMWFONT pfont);
	void suiGdText( PSD psd, MWCOORD x, MWCOORD y, const void *str, int cc, MWTEXTFLAGS flags);
	#define suiGdGetTextSize( p, u, l, w, h, b, f) \
		do { if (l == -1) l = sui_utf8_length( u); \
			GdGetTextSize( p, u, l, w, h, b, f); \
		} while(0)
		  
#elif (MICROWINDOWS_VER_CODE>=VER_CODE(0,90,0))

	#define suiGdSetFont   GdSetFont /* use original MW function */
	#define suiGdGetTextSize( p, u, l, w, h, b, f) \
		do { if (l == -1) l = sui_utf8_length( u); \
			GdGetTextSize( p, u, l, w, h, b, f); \
		} while(0)
	#define suiGdText( d, x, y, u, l, f) \
		do { if ( l == -1) l = sui_utf8_length( u); \
			GdText( d, x, y, sui_utf8_get_text(u), l, f); \
		} while(0)
 
#else /* MICROWINDOWS_VER_CODE<VER_CODE(0,90,0) */
  /* for gettextsize function
     this flag is add to text length (int 16/32b) for compatability to MW
     !!! but the GdTextSize has problems with this and len = -1 !!! */
	#define SUI_COLUMN_TEXT 0x4000
	
	/* REPLACE Microwindows functions */
	PMWFONT suiGdSetFont(PMWFONT pfont);
	void suiGdText( PSD psd, MWCOORD x, MWCOORD y, const void *str, int cc, MWTEXTFLAGS flags);
	void suiGdGetTextSize( PMWFONT pfont, const void *str, int cc, MWCOORD *pwidth,
				MWCOORD *pheight, MWCOORD *pbase, MWTEXTFLAGS flags);
	/* end of REPLACE Microwindows functions */
#endif /* MICROWINDOWS_VER_CODE<VER_CODE(0,90,0) */

extern MWFONTPROCS sui_fontprocs, sui_comp_fontprocs;

#if (MICROWINDOWS_VER_CODE>=VER_CODE(0,93,0))
/* fontprocs, fontsize, fontwidth, fontrotation, fontattr, name, cfont */

#define SUI_FONT_ENTRY(m_fontprocs, m_fontsize, m_fontwidth, m_fontrotation, m_fontattr, m_name, m_cfont) \
{ \
  .fontprocs=m_fontprocs, \
  .fontsize=m_fontsize, \
  .fontwidth=m_fontwidth, \
  .fontrotation=m_fontrotation, \
  .fontattr=m_fontattr, \
  .name=m_name, \
  .cfont=m_cfont, \
}

#else /* MICROWINDOWS_VER_CODE<VER_CODE(0,93,0) */

/* fontprocs, fontsize, fontrotation, fontattr, name, cfont */

#define SUI_FONT_ENTRY(m_fontprocs, m_fontsize, m_fontwidth, m_fontrotation, m_fontattr, m_name, m_cfont) \
{ \
  .fontprocs=m_fontprocs, \
  .fontsize=m_fontsize, \
  /* m_fontwidth not used */ \
  .fontrotation=m_fontrotation, \
  .fontattr=m_fontattr, \
  .name=m_name, \
  .cfont=m_cfont, \
}

#endif /* MICROWINDOWS_VER_CODE<VER_CODE(0,93,0) */

#define SUI_BASE_FONT_ENTRY(m_fontsize, m_fontwidth, m_fontrotation, m_fontattr, m_name, m_cfont) \
  SUI_FONT_ENTRY(&sui_fontprocs, m_fontsize, m_fontwidth, m_fontrotation, m_fontattr, m_name, m_cfont)

#define SUI_COMP_FONT_ENTRY(m_fontsize, m_fontwidth, m_fontrotation, m_fontattr, m_name, m_cfont) \
  SUI_FONT_ENTRY(&sui_comp_fontprocs, m_fontsize, m_fontwidth, m_fontrotation, m_fontattr, m_name, (PMWCFONT)m_cfont)

MWBOOL sui_font_comp_getfontinfo(PMWFONT pfont, PMWFONTINFO pfontinfo);
MWBOOL sui_font_getfontinfo(PMWFONT pfont, PMWFONTINFO pfontinfo);


#if (MICROWINDOWS_VER_CODE>=VER_CODE(0,90,0))
void sui_font_comp_gettextsize( PMWFONT pfont, const void *text, int chars, MWTEXTFLAGS flags,
			MWCOORD *pwidth, MWCOORD *pheight, MWCOORD *pbase);
void sui_font_gettextsize( PMWFONT pfont, const void *text, int chars, MWTEXTFLAGS flags,
			MWCOORD *pwidth, MWCOORD *pheight, MWCOORD *pbase);
void sui_font_comp_gettextbits( PMWFONT pfont, int chr, const MWIMAGEBITS **retmap,
			MWCOORD *pwidth, MWCOORD *pheight, MWCOORD *pbase);
void sui_font_gettextbits( PMWFONT pfont, int chr, const MWIMAGEBITS **retmap,
			MWCOORD *pwidth, MWCOORD *pheight, MWCOORD *pbase);
#else /* MICROWINDOWS_VER_CODE<VER_CODE(0,90,0) */

void sui_font_comp_gettextsize( PMWFONT pfont, const void *text, int chars, 
			MWCOORD *pwidth, MWCOORD *pheight, MWCOORD *pbase);
void sui_font_gettextsize( PMWFONT pfont, const void *text, int chars, 
			MWCOORD *pwidth, MWCOORD *pheight, MWCOORD *pbase);
void sui_font_comp_gettextbits( PMWFONT pfont, int chr, MWIMAGEBITS *retmap,
			MWCOORD *pwidth, MWCOORD *pheight, MWCOORD *pbase);
void sui_font_gettextbits( PMWFONT pfont, int chr, MWIMAGEBITS *retmap,
			MWCOORD *pwidth, MWCOORD *pheight, MWCOORD *pbase);
#endif /* MICROWINDOWS_VER_CODE<VER_CODE(0,90,0) */
  
void sui_font_unloadfont( PMWFONT pfont);
void sui_font_drawtext( PMWFONT pfont, PSD psd, MWCOORD x, MWCOORD y,
                        const void *text, int chars, MWTEXTFLAGS flags);

#endif /* _SUITK_MW_AND_COMPOUND_FONT_ */
