/* sui_structs.h
 *
 * SUITK additional structures - list, suffix structures.
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#ifndef _SUITK_STRUCTURES_MODULE_
#define _SUITK_STRUCTURES_MODULE_

#include <suiut/sui_types.h>
#include <suiut/sui_dinfo.h>
#include <suiut/sui_finfo.h>
#include <suitk/sui_gdi.h>

#include <ulut/ul_gavl.h>

#ifdef __cplusplus
extern "C" {
#endif

/******************************************************************************
 * List
 ******************************************************************************/
struct sui_list;

/**
 * struct sui_varf - Formatted variable
 * @di: Pointer to DINFO specifying value source.
 * @fi: Pointer to the generated text format information.
 *
 * File: sui_structs.h
 */
typedef struct sui_varf {
  sui_dinfo_t *di;
  sui_finfo_t *fi;
} sui_varf_t;

typedef struct sui_varl {
  sui_dinfo_t *di;
  struct sui_list *li;
} sui_varl_t;

/**
 * struct sui_list_item - Suitk list item structure
 * @text: List item text in utf8.
 * @idx: Index to list select array.
 *
 * File: sui_structs.h
 */
typedef struct sui_list_item {
  union {
    utf8          *text;        /* text */
    sui_varf_t     dinfo;
    sui_varl_t     dilist;
    struct sui_list *liprint;   /* list for print style text */
//		struct sui_widget *widget;     /* widget */
  } data;
  sui_listcoord_t  idx; /* gavl index */
  long             index; /* user index */
  int              imgidx; /* user image index */
  unsigned long    flags;
  gavl_node_t      list_node;
} sui_list_item_t;
  
/**
 * enum sui_list_item_flags
 */
enum sui_list_item_flags {
  SULI_SELECTED   = 0x01, /* item is selected */
  SULI_HIDDEN     = 0x02, /* item isn't visible */
  SULI_INDEXED    = 0x04, /* item contains indexed dinfo (or text) */
  SULI_USER_FLAG  = 0x08, /* user specific flag */
  SULI_FLAG_MASK  = 0x0F, /* mask for item flags */

  SULI_TYPE_NONE  = 0x00,    /* list item is empty */
  SULI_TYPE_TEXT  = 0x10,    /* list item contains utf8 text */
  SULI_TYPE_DINFO = 0x20,    /* list item contains dinfo and finfo */
  SULI_TYPE_LIST  = 0x30,    /* list item contains dinfo and list - lists in lists */
  SULI_TYPE_PRINT = 0x40,    /* list item contains list - lists in list which describe text like as printf */
//	SULI_TYPE_WIDGET= 0xC0,    /* list item contains widget */
  SULI_TYPE_MASK  = 0xF0,
};

/**
 * enum sui_li_change_flags - flags for list item changing
 * @SULICH_CONTENT: item content will be cahnged (olditem.data.text or olditem.data.dinfo will be set according to newitem.flags & SULI_TYPE_MASK
 * @SULICH_INDEX: item index will be changed - it removes olditem from list and then it adds olditem with new index
 * @SULICH_USER_INDEX: user index will be changed
 * @SULICH_IMAGE_INDEX: image index will be changed
 * @SULICH_FLAGS: item flags will be changed (except SULI_TYPE_MASK)
 * @SULICH_SET_FLAGS: item->flags |= newitem->flags (except SULI_TYPE_MASK)
 * @SULICH_CLEAR_FLAGS: item->flags &= ~newitem->flags (except SULI_TYPE_MASK)
 */
enum sui_li_change_flags {
  SULICH_CONTENT     = 0x01,
  SULICH_INDEX       = 0x02,
  SULICH_USER_INDEX  = 0x04,
  SULICH_IMAGE_INDEX = 0x08,
  SULICH_FLAGS       = 0x10, /* item->flags = newitem->flags */
  SULICH_SET_FLAGS   = 0x20, /* item->flags |= newitem->flags */
  SULICH_CLEAR_FLAGS = 0x40, /* item->flags &= ~newitem->flags */
};

#define sui_list_item_type( li)    (li->flags & SULI_TYPE_MASK)

/**
 * struct sui_list - Suitk list structure
 * @refcnt: Reference counter of list structure.
 * File: sui_structs.h
 */  
typedef struct sui_list {
/* for signal-slot support */
  struct sui_obj       base;
  sui_refcnt_t refcnt;
  sui_listcoord_t itemcnt;
  sui_listcoord_t maxidx;

  gavl_cust_root_field_t list_root;
} sui_list_t;


static inline int
sui_list_item_index_cmp(const sui_listcoord_t *a, const sui_listcoord_t *b)
{
  if (*a>*b) return 1;
  if (*a<*b) return -1;
  return 0;
}

GAVL_CUST_NODE_INT_DEC( sui_list_gavl, sui_list_t, sui_list_item_t, 
                        sui_listcoord_t, list_root, list_node, idx, 
                        sui_list_item_index_cmp)

/* list item fncs */
sui_list_item_t *sui_list_item_create(utf8 *text, sui_dinfo_t *di,
                                      sui_finfo_t *fi, sui_list_t *li);
int sui_list_item_destroy( sui_list_item_t *item);
/* list fncs */
sui_refcnt_t sui_list_inc_refcnt( sui_list_t *list);
sui_refcnt_t sui_list_dec_refcnt( sui_list_t *list);
//sui_list_t *sui_list_create( void);
void sui_list_clear( sui_list_t *list);
int sui_list_item_add( sui_list_t *list, sui_list_item_t *item, sui_listcoord_t idx);
int sui_list_item_del( sui_list_t *list, sui_listcoord_t idx);
int sui_list_item_change( sui_list_t *list, sui_listcoord_t idx, const sui_list_item_t *newitem, unsigned char chflags);
int sui_list_item_get_text( sui_list_item_t *item, utf8 **buf);
int sui_list_item_get_text_with_index(sui_list_item_t *item, int idx, utf8 **buf);
int sui_list_get_print(sui_list_t *list, int mainidx, utf8 **buf);

/******************************************************************************
 * List slot functions
 ******************************************************************************/
//int sui_application_change_screen( sui_application_t *appl, char *name);

/******************************************************************************
 * List vmt
 ******************************************************************************/
#define sui_list_vmt_fields(new_type, parent_type) \
  SUI_INS_INHERITED_VMT(new_type, parent_type, sui_obj)
//  int (*show_tip) (new_type##_t *self, struct sui_widget *wdg);
//  int (*hide_tip) (new_type##_t *self, struct sui_widget *wdg);
  
typedef struct sui_list_vmt {
  sui_list_vmt_fields(sui_list, sui_obj)
} sui_list_vmt_t;

static inline sui_list_vmt_t *sui_list_vmt( sui_list_t *self)
{
  return (sui_list_vmt_t*)(self->base.vmt);
}

extern sui_list_vmt_t sui_list_vmt_data;


/******************************************************************************
 * Suffix
 ******************************************************************************/
/**
 * struct sui_suffix - Number suffix and decimal digits structure
 * @refcnt: Reference counter of structure.
 * @decalign: Alignment(vertical) of decimal digits of number. It will be used, when it isn't NULL
 * @sufalign: Alignment(vertical) of postfix string.
 * @suffix: Pointer to postfix string in utf8. It will be drawn only if number has set flag SUNF_POSTFIX.
 * @decfonti: Pointer to font info structure for decimal digits font.
 * @suffonti: Pointer to font info structure for suffix string.
 *
 * Structure contains information about decimal alignment and font for different draw decimal digits
 * and integer part of number. Next fields are for suffix string, its alignment and font. Structure
 * has reference counter.
 * File: sui_structs.h
 */  
typedef struct sui_suffix {
    sui_refcnt_t    refcnt;
    sui_align_t     decalign;  /* decimal digit alignment */
    sui_align_t     sufalign; /* suffix aligment flags */
    utf8            *suffix;   /* suffix string */  
    sui_font_info_t *decfonti; /* font info for decimal digits */
    sui_font_info_t *suffonti; /* font info for postfix string */        
} sui_suffix_t;  

void sui_suffix_inc_refcnt( sui_suffix_t *nsuf);
void sui_suffix_dec_refcnt( sui_suffix_t *nsuf);
sui_suffix_t *sui_create_suffix( utf8 *asuffix, sui_font_info_t *adecfi, 
                                 sui_font_info_t *asuffi, sui_align_t adecalign, 
                                 sui_align_t asufalign);

#ifdef WIN32
/**
 * sui_suffix_static - Initialize static decimal and postfix structure
 * @name: Name of sui_numpost structure.
 * @post: Postfix utf8 string.
 * @def: Pointer to font used for decimal digits.
 * @pof: Pointer to font used for postfix string.
 * @dal: Vertical alignment for decimal digits.
 * @pal: Vertical alignment for postfix.
 * 
 * The macro initializes static sui_numpost structure.
 * File: sui_structs.h
 */     
  #define sui_suffix_static( name, suf, def, sof, dal, sal) \
  suiwn_post_t name = { SUI_STATIC, dal, sal, suf, def, sof}
#else
  #define sui_suffix_static( name, suf, def, sof, dal, sal) \
    suiwn_post_t name = { \
      refcnt: SUI_STATIC, \
      decalign: dal, \
      sufalign: sal, \
      suffix: suffix, \
      decfonti: def, \
      suffonti: sof \
    }
#endif

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /* _SUITK_STRUCTURES_MODULE_ */
