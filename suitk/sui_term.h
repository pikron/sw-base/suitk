/* sui_term.h
 *
 * SUITK system of terms (based on dinfo)
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#ifndef _SUITK_TERM_SYSTEM_
#define _SUITK_TERM_SYSTEM_

#include <suiut/sui_types.h>
#include <suiut/sui_dinfo.h>
#include <suiut/sui_di_text.h>
#include <suiut/sui_utf8.h>

struct sui_term;
struct sui_term_atomic;

/******************************************************************************
 * Term value 
 ******************************************************************************/
/* it must be defined as bit mask for some reasons */
enum sui_term_val_types {
  SUI_TERMVAL_NONE    = 0x0000, /* priority order is from LSB to MSB for implicit translation */
  SUI_TERMVAL_LONG    = 0x0001,
  SUI_TERMVAL_ULONG   = 0x0002,
  SUI_TERMVAL_UTF8    = 0x0004,
  SUI_TERMVAL_DINFO   = 0x0008,
  SUI_TERMVAL_SUBTERM = 0x0010,

  SUI_TERMVAL_ALL     = 0x001F,
};

/**
 * struct sui_term_value - Suitk term subtree value node
 * @type:	Value type specified by one of %SUI_TERMVAL_xxx constant.
 * @value:	Union holding one of the possible value representation
 *		selected by @type field.
 *
 * File: sui_term.h
 */
typedef struct sui_term_value {
  unsigned short type;
  union {
    long slong;
    unsigned long ulong;
    utf8 *u8;
    sui_dinfo_t *dinfo;
    struct sui_term *subterm;
  } value;
} sui_term_value_t;

static inline
int sui_term_value_init( sui_term_value_t *val)
{
  if (!val) return -1;
  val->type = SUI_TERMVAL_NONE;
  val->value.slong = 0;
  return 0;
}
int sui_term_value_clear( sui_term_value_t *val);

static inline
int sui_term_value_destroy( sui_term_value_t *val)
{
  int ret;
  if (!(ret=sui_term_value_clear(val)))
    free(val);
  return ret;
}

int sui_term_value_inc_content_refcnt( sui_term_value_t *val);
/******************************************************************************
 * Term 
 ******************************************************************************/
//typedef int sui_term_operation_t( struct sui_term_atomic *cnode, long *result);
typedef int sui_term_operation_t( struct sui_term_atomic *cnode, sui_term_value_t *result);
typedef int sui_term_op_1arg_t(sui_term_value_t *result, sui_term_value_t *first);
typedef int sui_term_op_2arg_t(sui_term_value_t *result, sui_term_value_t *first, sui_term_value_t *second);
typedef int sui_term_op_3arg_t(sui_term_value_t *result, sui_term_value_t *first, sui_term_value_t *second, sui_term_value_t *third);


/*
 * struct sui_screen_eqop_table - assign opfnc to sign
 */
typedef struct sui_term_op_table {
  const char *op_sign;
  int nargin;
  sui_term_operation_t *opfcn;
  unsigned short first_type;
  unsigned short second_type;
  unsigned short third_type;
} sui_term_op_table_t;

/**
 * struct sui_term_atomic - Structure of atomic grain of SuiTk term
 * @op_info: Pointer to operation, which return result of this subterm
 * @data: Pointer to SuiTk term subtree value node.
 * @first: First operand - term subtree
 * @second: Second operand - term subtree
 * @third: Third operand - term subtree
 */
typedef struct sui_term_atomic {
  sui_term_op_table_t *op_info;
  struct sui_term_value data;
  struct sui_term_atomic *first;
  struct sui_term_atomic *second;
  struct sui_term_atomic *third;
} sui_term_atomic_t;

/**
 * struct sui_term - Structure of suitk term
 * @refcnt:	Reference counter
 * @subterm:	Pointer to the top (outermost) operation of the evaluation tree.
 * @result:	Place to store result of evaluated expression.
 * @flags:	Combination of %SUI_TERM_xxx flags.
 */
typedef struct sui_term {
  sui_refcnt_t refcnt;

  sui_term_atomic_t *subterm;
  sui_term_value_t result;

  sui_flags_t flags;
/* TODO: some fields for connection with nested dinfos and subterms */

} sui_term_t;

enum sui_term_flags {
  SUI_TERM_NONE     = 0x0000,
  SUI_TERM_INVALID  = 0x0000,
  SUI_TERM_VALID    = 0x0001,
  SUI_TERM_CHANGING = 0x0002,
  SUI_TERM_CHANGED  = 0x0004,
  SUI_TERM_ERROR    = 0x0008,
};

/******************************************************************************
 * operations
 ******************************************************************************/

sui_refcnt_t sui_term_inc_refcnt( sui_term_t *term);
sui_refcnt_t sui_term_dec_refcnt( sui_term_t *term);

sui_term_atomic_t *sui_term_create_aterm( void);

sui_term_t *sui_term_create_from_prefix_string( char *prefixstr);

//inline 
int sui_term_compute_atomicterm( sui_term_atomic_t *aterm, sui_term_value_t *result);
//inline 
int sui_term_compute_term( sui_term_t *term, sui_term_value_t *result);


/******************************************************************************
 * external function to resolve IDs in input string
 ******************************************************************************/
 /* The function resolves identifier with $id name and it fills $value.
    The function returns 0 if $value is valid and all other values means error. */
extern int sui_term_resolve_id( char *id, sui_term_value_t *value); // , sui_term_t *subterm
 
#endif /* _SUITK_TERM_SYSTEM_ */
