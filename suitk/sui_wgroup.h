/* sui_wgroup.h
 *
 * Header file for SUITK group widget.
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#ifndef _SUI_GROUP_WIDGET_
  #define _SUI_GROUP_WIDGET_

#include "suitk.h"

/******************************************************************************
 * Group(Window) widget - structure
 ******************************************************************************/
/**
 * struct suiw_group - Additional group widget structure
 * @children: Pointer to the first child of all descendent widgets.
 * @focus: Pointer to the focused child/modal widget.
// * @tempfocus: Pointer to the previously focused widget before modal widget adding
 * @status: Pointer to the widget which is registered as group status widget.
 *
 * File: sui_wgroup.h
 */
typedef struct suiw_group {
//  sui_rect_t    clientarea;

  sui_widget_t *children;   /* all widgets have incremented refcnt */

  sui_widget_t *focus;    /* internal pointer - no refcnt increment */
//  sui_widget_t *tempfocus;/* internal pointer - no refcnt increment */
  sui_widget_t *status;   /* internal pointer - no refcnt increment */
} suiw_group_t;

typedef sui_widget_t sui_wgroup_t;

/**
 * enum group_flags - Special group widget flags ['SUGF_' prefix]
 * @SUGF_LAYOUT_NONE: Widget doesn't use any layout for a new child widgets.
 * @SUGF_LAYOUT_BORDER: Widget uses border layout for a new child widgets.
 * @SUGF_LAYOUT_CARD: Widget uses card layout for a new  child widgets.
 * @SUGF_NOTIFY: Widget informed parent widget.
 * @SUGF_CANBEFOCUSED: A group widget can be focused. With this flag, a group can have got focus in both cases - either it contains children widgets or it is empty.
// * @SUGF_EMPTYISFOCUSEN: An empty group can be focused. A focusable group with widgets, which cannot be focusen, can be focused, as well.
 * @SUGF_FOCUSONLYINSIDE: Focus won't be switched to the parent widget, when it will be switched to the next/previous widget.
// * @SUGF_MODALWIDGET: The widget is a modal widget.
 * @SUGF_LAYOUT_MASK: Mask for the widget layout flags.
 *
 * File: sui_wgroup.h
 */
enum group_flags {
  SUGF_LAYOUT_NONE         = 0x00000000,
  SUGF_LAYOUT_BORDER       = 0x00010000,
  SUGF_LAYOUT_CARD         = 0x00020000,

  SUGF_LAYOUT_MASK         = 0x00030000,
  
  SUGF_NOTIFY              = 0x00100000,
  SUGF_CANBEFOCUSED        = 0x00200000,
//  SUGF_EMPTYISFOCUSEN      = 0x00200000,
  SUGF_FOCUSONLYINSIDE     = 0x00400000,

//  SUGF_MODALWIDGET         = 0x01000000,
  SUGF_FIRSTCHILDISMODAL   = 0x02000000, // pro rodice modalniho okna, aby se dalo jednoduse jit po spravnem fokusu - modalni okno je vzdy pristupny jako parent->children
};

#define sui_grpwdg(wdg)  ((suiw_group_t *)wdg->data)

#define sui_grouphasmodal(wdg) (wdg && sui_test_flag(wdg, SUGF_FIRSTCHILDISMODAL))

/**
 * enum group_window_order - Flags for widget ordering inside group
 * @SUGO_FIRST: Set/move widget to background.
 * @SUGO_BACKGROUND: Set/move widget to background.
 * @SUGO_LAST: Set/move widget to foreground.
 * @SUGO_FOREGROUND: Set/move widget to foreground.
 * @SUGO_ONE_BACK: Move widget to one back.
 * @SUGO_ONE_FRONT: Move widget to one front.
 * @SUGO_RELATIVE_ORDER: Desired order is relative.
 * @SUGO_MODAL_WIDGET: A widget will be added to the as the modal widget and no between normal widgets.
 * @SUGO_ORDER_MASK: Mask for absolute order (0-widget is in background, ... ).
 *
 * File: sui_wgroup.h
 */
enum group_window_order {
  SUGO_LAST           = 0x8000,
  SUGO_BACKGROUND     = 0x8000,
  SUGO_FIRST          = 0x8001,
  SUGO_FOREGROUND     = 0x8001,
  SUGO_ONE_BACK       = 0x8002,
  SUGO_ONE_FRONT      = 0x8003,
  SUGO_RELATIVE_ORDER = 0x8000,
  SUGO_MODAL_WIDGET   = 0x4000,
  SUGO_ORDER_MASK     = 0x0FFF,
};

/******************************************************************************
 * function for group handling
 ******************************************************************************/
int sui_group_add_widget(sui_widget_t *group, sui_widget_t *widget, int order);
int sui_group_add_widget_dec_refcnt(sui_widget_t *group, sui_widget_t *widget, int order);
int sui_group_remove_widget(sui_widget_t *widget);
int sui_group_add_modal_widget(sui_widget_t *group,
                                      sui_widget_t *widget, int order);
int sui_group_remove_modal_widget(sui_widget_t *group);
int sui_group_change_widget_order(sui_widget_t *widget, sui_widget_t *from, int order);

/* Functions for Focusing */
sui_widget_t *sui_group_find_focused_widget(sui_widget_t *root, int *level);
int sui_group_count_widget_level(sui_widget_t *wdg, int root2leaf);
int sui_group_only_set_focus(sui_widget_t *wdg, int level);
int sui_group_only_clear_focus(sui_widget_t *wdg, int level);
int sui_group_change_focus(sui_widget_t *root, sui_widget_t *from, sui_widget_t *wdg);
sui_widget_t *sui_group_find_next(sui_widget_t *root, sui_widget_t *from, int backward, int focused);


int sui_group_set_status(sui_widget_t *group, sui_widget_t *widget);

typedef int sui_group_foreach_fnc_t(sui_widget_t *group, sui_widget_t *widget,
                                    sui_event_t *event);
int sui_group_foreach(sui_widget_t *group, sui_widget_t *from, int dir,
                      sui_group_foreach_fnc_t *fnc, sui_event_t *event);

/******************************************************************************
 * Group slot functions
 ******************************************************************************/
int sui_group_status_message(sui_widget_t *group, utf8 *message);
int sui_group_status_clear(sui_widget_t *group);

int sui_group_modal_open(sui_widget_t *group, sui_widget_t *modal);
int sui_group_modal_close(sui_widget_t *group, int retcode);

//signals
// status_changed
// modal_opened
// modal_closed

/******************************************************************************
 * Group widget vmt & signal-slot mechanism
 ******************************************************************************/
#define sui_wgroup_vmt_fields(new_type, parent_type) \
  sui_widget_vmt_fields(new_type, parent_type) \
  int (*status_message)(new_type##_t *self, utf8 *text); \
  int (*reclaim_children)(new_type##_t *self, sui_widget_t *widget);

typedef struct sui_wgroup_vmt {
  sui_wgroup_vmt_fields(sui_wgroup, sui_widget)
} sui_wgroup_vmt_t;

static inline sui_wgroup_vmt_t *
sui_wgroup_vmt(sui_widget_t *self)
{
  return (sui_wgroup_vmt_t*)(self->base.vmt);
}

extern sui_wgroup_vmt_t sui_wgroup_vmt_data;

/******************************************************************************
 * Group widget basic functions
 ******************************************************************************/
/* group widget fcn */

sui_widget_t *sui_wgroup(sui_rect_t *aplace, utf8 *alabel,
                          sui_style_t *astyle, unsigned long aflags);

#endif /* _SUI_GROUP_WIDGET_ */
