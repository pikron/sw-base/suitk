/* sui_gdi.h
 *
 * SUITK graphics device interface.
 * low-level drawing functions
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#ifndef _SUITK_GRAPHICS_DEVICE_INTERFACE_H_
  #define _SUITK_GRAPHICS_DEVICE_INTERFACE_H_

#include <device.h>
#include <mwtypes.h>
#include <suitk/sui_comdefs.h>
#include <suiut/sui_types.h>
#include <suiut/sui_utf8.h>
#include <suitk/sui_font.h>
#include <suitk/sui_geom.h>

#ifdef __cplusplus
extern "C" {
#endif

struct sui_style;

/******************************************************************************
 * Basic GDI types and structures
 ******************************************************************************/
/* color type */
typedef long sui_color_t;	/* color type (32 bit int : 0x00RRGGBB) + -1 for transparent */

/******************************************************************************
 * Compound GDI structures
 ******************************************************************************/

/**
 * struct sui_dc - SUITK device context structure
 * @psd: Original MicroWindows screen device structure.
 * @offset: Origin of drawing canvas.
 * @cliprect: Rectangle with current clipping area.
 * @global_flags: Various global settings (blink,...).
 *
 * File: sui_gdi.h
 */
typedef struct sui_dc {
  struct _mwscreendevice *psd;
  sui_point_t offset;
  sui_rect_t cliprect;
  sui_flags_t dc_flags;
} sui_dc_t;

/**
 * global_dc_flags - Global device context settings
 * @SUDCF_BLINK_NOW: Switch SUFL_HIGHLIGHT flag before drawing.
 *
 * File: sui_gdi.h
 */
enum global_dc_flags {
  SUDCF_BLINK_NOW = 0x0001,
};

/******************************************************************************
 * Alignment
 ******************************************************************************/
/* alignment type */
typedef unsigned short sui_align_t;

/**
 * enum alignment - Object alignment. ['SUAL_' prefix]
 * @SUAL_CENTER: Horizontal and vertical alignment to center of object. (default)
 * @SUAL_TOP: Vertical alignment to top of object.
 * @SUAL_BOTTOM: Vertical alignment to bottom of object.
 * @SUAL_BASELINE: Vertical alignment to baseline. (text only)
 * @SUAL_LEFT: Horizontal alignment to left of object.
 * @SUAL_RIGHT: Horizontal alignment to right of object.
 * @SUAL_BLOCK: Horizontal alignment to block (only some widgets)
 * @SUAL_INSIDE: Object is aligned inside to box.
 * @SUAL_COLUMN: Text object is in column. (text only)
 * @SUAL_CLIP: !!! NOT IMPLEMENTED !!! - inside widget is content clipping always
 * @SUAL_WRAP: Text object is wrapped. (text only) !!! NOT IMPLEMENTED !!!
 * @SUAL_MASK: Mask for all of alignment flags.
 * @SUAL_DIRMASK: Mask for horizontal and vertical alignment flags.
 * @SUAL_VERMASK: Mask for vertical alignment flags.
 * @SUAL_HORMASK: Mask for horizontal alignment flags.
 *
 * File: sui_gdi.h
 */  
enum alignment {
  SUAL_CENTER       = 0x0000,
  SUAL_TOP          = 0x0001,
  SUAL_BOTTOM       = 0x0002,
  SUAL_BASELINE     = 0x0003,
  SUAL_LEFT         = 0x0004,
  SUAL_RIGHT        = 0x0008,
  SUAL_BLOCK        = 0x000C,
  SUAL_INSIDE       = 0x0010,
  SUAL_COLUMN       = 0x0020, /* SUAL_TILED */
  SUAL_CLIP         = 0x0040,
  SUAL_WRAP         = 0x0080,
  SUAL_MASK         = 0x00FF,
  
  SUAL_DIRMASK      = 0x000F,
  SUAL_VERMASK      = 0x0003,
  SUAL_HORMASK      = 0x000C,

  SUAL_INT_LABEL    = 0x8000, /* special internal flag for the 'sui_align_object' function */
};

int sui_align_object(sui_point_t *box, sui_point_t *origin, sui_point_t *obj,
                    sui_coordinate_t objbase, struct sui_style *style, sui_align_t align);
int sui_align_object_to_point(sui_point_t *origin, sui_point_t *obj, sui_coordinate_t objbase,
                              sui_point_t *point, sui_align_t align);
sui_align_t sui_get_complementary_align(sui_align_t align);

/******************************************************************************
 * Common GDI functions
 ******************************************************************************/
#ifndef MWMODE_COPY
  #define MWMODE_COPY MWMODE_SET
#endif

void sui_gdi_set_drmode(sui_dc_t *dc, int mode);
void sui_gdi_set_usebgrd(sui_dc_t *dc, unsigned char usebgrd);

void sui_gdi_clear_screen(sui_dc_t *dc);


/******************************************************************************
 * Clipping
 ******************************************************************************/
void sui_gdi_set_carea(sui_dc_t *dc, sui_rect_t *box);
void sui_gdi_clear_carea(sui_dc_t *dc);


/******************************************************************************
 * Colors
 ******************************************************************************/
#define SUI_COLOR_SUBBLOCK 7  /* cnt colors in subblock (backgr,text,frame,labelbg,label) */
#define SUI_SYS_COLORS   0x20 /* count of system colors */
#define SUI_USER_COLORS  0    /* count of user colors */

/**
 * enum colors - System color indexes ['SUC_' prefix]
 * @SUC_BACKGROUND: Color of background for normal widget.
 * @SUC_FRAME: Color of frame for normal widget.
 * @SUC_FRAME2: Second color of frame for normal widget.
 * @SUC_ITEM: First item color for normal widget.
 * @SUC_ITEM2: Second item color for normal widget.
 * @SUC_LABELBACK: Color of label background for normal widget.
 * @SUC_LABEL: Color of label for normal widget.
 * @SUC_HGBACK: Color of background for highlighted widget.
 * @SUC_HGFRAME: Color of frame for highlighted widget.
 * @SUC_HGFRAME: Second color of frame for highlighted widget.
 * @SUC_HGITEM: First item color for highlighted widget.
 * @SUC_HGITEM2: Second item color for highlighted widget.
 * @SUC_HGLABELBACK: Color of label background for highlighted widget.
 * @SUC_HGLABEL: Color of label for highlighted widget.
 * @SUC_SELBACK: Color of background for selected widget.
 * @SUC_SELFRAME: Color of frame for selected widget.
 * @SUC_SELFRAME2: Second color of frame for selected widget.
 * @SUC_SELITEM: First item color for selected widget.
 * @SUC_SELITEM2: Second item color for selected widget.
 * @SUC_SELLABELBACK: Color of label background for selected widget.
 * @SUC_SELLABEL: Color of label for selected widget.
 * @SUC_HGSELBACK: Color of background for select highlighted widget.
 * @SUC_HGSELFRAME: Color of frame for select highlighted widget.
 * @SUC_HGSELFRAME2: Second color of frame for select highlighted widget.
 * @SUC_HGSELITEM: First item color for select highlighted widget.
 * @SUC_HGSELITEM2: Second item color for select highlighted widget.
 * @SUC_HGSELLABELBACK: Color of label background for select highlighted widget.
 * @SUC_HGSELLABEL: Color of label for select highlighted widget.
 * @SUC_TOOLTIPBACK: Color of tooltip background for all widgets.
 * @SUC_TOOLTIP: Color of tooltip for all widgets.
 * @SUC_SHADOW: Color of widget shadows.
 *
 * Indexes of system colors in color table. Widget can be in four mode - normal, highlighted, selected 
 * and selected+highlighted and for every mode each widget has seven colors (background,frame,frame2,text,text2,label
 * background and label). Except these colors each widget has two tooltip colors (tooltip background and tooltip)
 * and one shadow color.
 *
 * File: sui_gdi.h
 */
enum colors {
  SUC_BACKGROUND     = 0x00,
  SUC_FRAME          = 0x01,
  SUC_FRAME2         = 0x02,
  SUC_ITEM           = 0x03,
  SUC_ITEM2          = 0x04,
  SUC_LABELBACK      = 0x05,
  SUC_LABEL          = 0x06,
  SUC_HGBACK         = 0x07,
  SUC_HGFRAME        = 0x08,
  SUC_HGFRAME2       = 0x09,
  SUC_HGITEM         = 0x0A,
  SUC_HGITEM2        = 0x0B,
  SUC_HGLABELBACK    = 0x0C,
  SUC_HGLABEL        = 0x0D,
  SUC_SELBACK        = 0x0E,
  SUC_SELFRAME       = 0x0F,
  SUC_SELFRAME2      = 0x10,
  SUC_SELITEM        = 0x11,
  SUC_SELITEM2       = 0x12,
  SUC_SELLABELBACK   = 0x13,
  SUC_SELLABEL       = 0x14,
  SUC_HGSELBACK      = 0x15,
  SUC_HGSELFRAME     = 0x16,
  SUC_HGSELFRAME2    = 0x17,
  SUC_HGSELITEM      = 0x18,
  SUC_HGSELITEM2     = 0x19,
  SUC_HGSELLABELBACK = 0x1A,
  SUC_HGSELLABEL     = 0x1B,
  SUC_TOOLTIPBACK    = 0x1C,
  SUC_TOOLTIP        = 0x1D,
  SUC_SHADOW         = 0x1E,

  SUC_LTFRAME        = 0x01,
  SUC_DKFRAME        = 0x02,

  SUC_HIGHLIGHTED    = SUI_COLOR_SUBBLOCK,

  SUC_INACTIVED       = 0,
  SUC_ACTIVED         = SUI_COLOR_SUBBLOCK,
  SUC_SELECTED        = 2*SUI_COLOR_SUBBLOCK,
  SUC_DISABLED        = 3*SUI_COLOR_SUBBLOCK,
};

/**
 * sui_color_from_rgb - macro for converting from rgb to sui_color
 * sui_color (or MWCOLOR) has the following format 0x00RRGGBB
 */
#define sui_color_from_rgb(R,G,B) ((((unsigned long)(R & 0xff))<<16) | (((unsigned long)(G & 0xff))<<8) | ((unsigned long)(B & 0xff)))
#define sui_color_reverse(RGB) ((((unsigned long)RGB>>16)&0xff)|((unsigned long)(RGB & 0xff00))|((unsigned long)(RGB << 16)&0xff0000))

//#ifndef WITH_RTEMS_UID
//#else
//  #define sui_color_reverse(RGB) (RGB)
//#endif

#define SUI_NOBG_COLOR (-1)
#define sui_color_is_transparent(COLOR) ((COLOR & 0xff000000)!=0) /* alpha channel can be used in the future */

/**
 * struct sui_colortable - Color table structure
 * @refcnt: Reference counter.
 * @calloc: Size of allocated space for colors as maximal number of colors or SUI_STATIC for static table.
 * @count: Count of colors in the table.
 * @ctab: Pointer to array of colors.
 * 
 * Reference counting structure. If refcnt is negative, structure is static and
 * cannot be deallocated from memory.
 *
 * File: sui_gdi.h
 */
typedef struct sui_colortable {
  sui_refcnt_t  refcnt;
  int           calloc; /* allocated space for colors in count of colors or SUI_STATIC */
  int           count;  /* color array length - for more than system colors */
  sui_color_t  *ctab;   /* color table */
} sui_colortable_t;

sui_refcnt_t sui_colortable_dec_refcnt(sui_colortable_t *ctab);
sui_refcnt_t sui_colortable_inc_refcnt(sui_colortable_t *ctab);

void sui_color_add_to_colortable(sui_colortable_t *ctab, sui_color_t c);
sui_colortable_t *sui_colortable_create(int acnt, sui_color_t ac[]);

void sui_gdi_set_fgcolor(sui_dc_t *dc, sui_color_t color);
void sui_gdi_set_bgcolor(sui_dc_t *dc, sui_color_t color);
void sui_gdi_set_color(sui_dc_t *dc, sui_color_t fgcolor, sui_color_t bgcolor);


/******************************************************************************
 * FONTs
 ******************************************************************************/
sui_dcfont_t *sui_font_prepare(sui_dc_t *dc, sui_font_t font, sui_coordinate_t height);
void sui_font_set(sui_dc_t *dc, sui_dcfont_t *pdcfont);
int sui_font_get_info(sui_dc_t *dc, sui_dcfont_t *pdcfont, sui_fontinfo_t *fontinfo);


/******************************************************************************
 * Text
 ******************************************************************************/
int sui_text_get_size(sui_dc_t *dc, sui_dcfont_t *pdcfont, sui_textdims_t *tdims,
                             int len, utf8 *utf, sui_align_t align);
void sui_draw_text(sui_dc_t *dc, sui_coordinate_t x, sui_coordinate_t y,
                   int len, utf8 *utf, sui_align_t align);


/******************************************************************************
 * Image
 ******************************************************************************/
/**
 * struct sui_image_t - Common image structure
 * @refcnt: Structure reference counter.
 * @flags:  Image flags - color depth, transparency,...
 * @width:  Image width.
 * @height: Image height.
 * @count:  Count of frames (usable for animation).
 * @bpr: MWIMAGEBITS per line for bitmap images.
 * @colormap: Pointer to color table (count of colors must be equal or great than image needs (2^SUIM_BITMAP_xBIT).
 * @data:  Pointer to image data.
 *
 * File: sui_gdi.h
 */
typedef struct sui_image {
  sui_refcnt_t           refcnt;
  unsigned short         flags;
  sui_coordinate_t       width;
  sui_coordinate_t       height;
  int                    count;        // count of images in image data - for animation
  int                    bpr;          // MWIMAGEBITS per row for bitmaps or don't care (quick search)
  struct sui_colortable *cmap;         // for color images
  MWIMAGEBITS           *data;
} sui_image_t;

/**
 * enum image_flags - Common image flags ['SUIM_' prefix]
 * @SUIM_BITMAP_1BIT:  Image is 1bit per pixel bitmap (2colors).
 * @SUIM_BITMAP_2BIT:  Image is 2bit per pixel bitmap (4colors).
 * @SUIM_BITMAP_4BIT:  Image is 4bit per pixel bitmap (16colors).
 * @SUIM_BITMAP_8BIT:  Image is 8bit per pixel bitmap (256colors).
 * @SUIM_VECTOR:       Vector image.
 * @SUIM_TRANSPARENT:  Image hasn't background (The pixels with color 0 will not be drawn.).
 * @SUIM_DYNAMIC_DATA: Data is in a dynamic array.
 *
 * File: sui_gdi.h
 */
enum image_flags {
  SUIM_BITMAP_1BIT   = 0x0000,
  SUIM_BITMAP_2BIT   = 0x0001,
  SUIM_BITMAP_4BIT   = 0x0002,
  SUIM_BITMAP_8BIT   = 0x0003,
  //SUIM_VECTOR        = 0x0008,

  SUIM_TRANSPARENT   = 0x0010,
  SUIM_DYNAMIC_DATA  = 0x4000,

  SUIM_TYPEMASK      = 0x000f,
};

sui_refcnt_t sui_image_inc_refcnt(sui_image_t *image);
sui_refcnt_t sui_image_dec_refcnt(sui_image_t *image);

sui_image_t *sui_image_create(sui_coordinate_t awidth, sui_coordinate_t aheight,
                              short acount, unsigned short aflags,
                              MWIMAGEBITS *adata, sui_colortable_t *acmap);

/**
 * enum image_operation_flags - Operation flags for sui_draw_image function
 * @SUIO_ROTATE_0:  No rotation.
 * @SUIO_ROTATE_90: Rotate image and mask about 90 deg.
 * @SUIO_ROTATE_180: Rotate image and mask about 180 deg.
 * @SUIO_ROTATE_270: Rotate image and mask about 270 deg.
 * @SUIO_MIRROR: Mirror image and mask horizontaly.
 * @SUIO_INVERSE_IMAGE: Inverse image index in color table.
 * @SUIO_INVERSE_MASK: Inverse mask index in color table.
 * @SUIO_MASK_IMAGE: Mask image - draw image pixels with non-zero pixels in mask.
 * @SUIO_TRANSPARENT: Don't draw color 0 (transparent background).
 * @SUIO_ROTATION_MASK: Mask for rotation flags.
 * @SUIO_MULTIIMAGE: Use index to select another image than the first image in mutliimage.
 *
 * File: sui_gdi.h
 */
enum image_operation_flags {
  SUIO_ROTATE_0      = 0x0000,
  SUIO_ROTATE_90     = 0x0001,
  SUIO_ROTATE_180    = 0x0002,
  SUIO_ROTATE_270    = 0x0003,
  SUIO_MIRROR        = 0x0004,
  SUIO_INVERSE_IMAGE = 0x0010,
  SUIO_INVERSE_MASK  = 0x0020,
  SUIO_MASK_IMAGE    = 0x0040,
  SUIO_TRANSPARENT   = 0x0080,
  SUIO_MULTIIMAGE    = 0x0100,

  SUIO_ROTATION_MASK = 0x0003,
};

/******************************************************************************
 * Style
 ******************************************************************************/
/**
 * struct sui_style - Common widget style structure
 * @refcnt: Reference counter of the style structure.
 * @flags: Style flags (visual style).
 * @coltab: Pointer to the color table structure (See 'sui_coltab' structure).
 * @bkcolor: background color index to first group of colortable (NORMAL).
 * @labfonti: Pointer to the font structure used for label (See 'sui_font_info' structure).
 * @fonti: Pointer to the font structure used for texts and items in widget (See 'sui_font_info' structure).
 * @frame: Width of widget frame in pixels. Frame is drawn only if widget has set flag SUSFL_FRAME.
 * @gap: Width of space (in pixels) between frame and items or between two items in widget.
 *
 * File: sui_gdi.h
 */
typedef struct sui_style {
  sui_refcnt_t      refcnt;       /* reference counter */
  sui_flags_t       flags;
  sui_colortable_t *coltab;
//  int               bkcolor;
  sui_font_info_t  *labfonti;
  sui_font_info_t  *fonti;
  sui_align_t    labalign;
  sui_coordinate_t  frame;        /* frame width */
  sui_coordinate_t  gap;
} sui_style_t;

/* Style flags */
/**
 * enum style_flags - Flags for widget style settings
 * @SUSFL_FRAME: A widget frame will be drawn.
 * @SUSFL_ROUNDEDFRAME: A widget has got rounded frame.
 * @SUSFL_SHADOW: A widget has got its shadow.
 * @SUSFL_SEEMS3D: A widget has got alternative frame for 3D effect.
 * @SUSFL_TRANSPARENT: A widget is transparent (the widget isn't filled by background color).
 * @SUSFL_LABELHASFRAME: A Widget label has its frame.
 * @SUSFL_FOCUSLABEL: A widget label is highlighted.
 * @SUSFL_FOCUSFRAME: A widget has got highlighted border otherwise to highlight whole object.
 * @SUSFL_TITLE: A widget has title (can be with label).
 * @SUSFL_LABELHASBACK: A widget label has got its background.
 * @SUSFL_BLINKEDITED: A widget content will blink if it is focused and edited
 *
 * File: sui_gdi.h
 */
enum style_flags {
  SUSFL_FRAME              = 0x00000001, /* frame is drawn */
  SUSFL_ROUNDEDFRAME       = 0x00000002, /* widget has rounded frame */
  SUSFL_SHADOW             = 0x00000004, /* widget has a shadow */
  SUSFL_SEEMS3D            = 0x00000008, /* widget has alternative frame with 3d effect */
  SUSFL_TRANSPARENT        = 0x00000010, /* widget is transparent (wdg isn't filled by background color) */
  SUSFL_LABELHASFRAME      = 0x00000020, /* label itself has frame */
  SUSFL_FOCUSLABEL         = 0x00000040, /* highlight label */
  SUSFL_FOCUSFRAME         = 0x00000080, /* highlight border otherwise highlight whole object */
  SUSFL_TITLE              = 0x00000100, /* widget has title (can be with label) */
  SUSFL_LABELHASBACK       = 0x00000200, /* label has itself background */
  SUSFL_BLINKEDITED        = 0x00000400, /* content of widget blinks when it has focus and is edited */
};

/* style flags macros */
#define sui_style_is_flag(style, flgs, isflgs)  (style && ((style->flags & (flgs)) == (isflgs)))
#define sui_style_test_flag(style, flgs) ((style) ? (style->flags & (flgs)) : 0)
#define sui_style_set_flag(style, flgs) style->flags |= (flgs)
#define sui_style_clear_flag(style, flgs) style->flags &= ~(flgs)
#define sui_style_switch_flag(style, flgs) style->flags ^= (flgs)

sui_refcnt_t sui_style_inc_refcnt(sui_style_t *style);
sui_refcnt_t sui_style_dec_refcnt(sui_style_t *style);

sui_style_t *sui_style_create(sui_flags_t aflags, sui_colortable_t *actab, //int abkclr,
                              sui_font_info_t *alabfi, sui_font_info_t *afi,
                              sui_coordinate_t aframe, sui_coordinate_t agap);


/******************************************************************************
 * Drawing functions - Lines and curves
 ******************************************************************************/
void sui_draw_line(sui_dc_t *dc, sui_coordinate_t x1, sui_coordinate_t y1,
                          sui_coordinate_t x2, sui_coordinate_t y2);
void sui_draw_line_points(sui_dc_t *dc, sui_point_t *from, sui_point_t *to);
void sui_draw_line_rect(sui_dc_t *dc, sui_rect_t *rect);

void sui_draw_ellipse(sui_dc_t *dc, sui_coordinate_t x, sui_coordinate_t y,
                             sui_coordinate_t rx, sui_coordinate_t ry, unsigned char fill);


/******************************************************************************
 * Drawing functions - Compound objects
 ******************************************************************************/
void sui_draw_rect(sui_dc_t *dc, sui_coordinate_t x, sui_coordinate_t y,
                   sui_coordinate_t w, sui_coordinate_t h, sui_coordinate_t frm);
void sui_draw_rect_points(sui_dc_t *dc, sui_point_t *origin,
                          sui_point_t *size, sui_coordinate_t frm);
void sui_draw_rect_box(sui_dc_t *dc, sui_rect_t *rect, sui_coordinate_t frm);
void sui_gdi_draw_image(sui_dc_t *dc, sui_point_t *origin,
                        sui_image_t *image, sui_image_t *mask,
                        int index, sui_flags_t operation);

/******************************************************************************
 * Function for screen-shoting
 ******************************************************************************/
sui_image_t *sui_gdi_image_from_screen(sui_dc_t *dc, sui_rect_t *rect, int imgbpp);

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /* _SUITK_GRAPHICS_DEVICE_INTERFACE_H_ */
