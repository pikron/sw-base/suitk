/* sui_args.c
 *
 * SUITK function arguments propagation.
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#include <stdarg.h>
#include "sui_args.h"

#include "sui_gdi.h"

typedef int va_bare_int2_fnc(int arg1, int arg2);

static int va_bare_int2(int (*fnc)(void), va_list args)
{
  va_bare_int2_fnc *f=(va_bare_int2_fnc*)fnc;
  int a1 = va_arg(args, int);
  int a2 = va_arg( args, int);
  return f( a1, a2); // sui_coordinate_t
}

typedef int va_context_int2_fnc(void *context, int arg1, int arg2);

static int va_context_int2(int (*fnc)(void), void *context, va_list args)
{
  va_context_int2_fnc *f=(va_context_int2_fnc*)fnc;
  int a1 = va_arg(args, int);
  int a2 = va_arg(args, int);
  return f(context, a1, a2); // sui_coordinate_t
}

sui_args_tinfo_t sui_args_tinfo_int2 = {
	.count = 2,
	.call_va_bare = va_bare_int2,
	.call_va_context = va_context_int2,
	.args = { SUI_TYPE_INT, SUI_TYPE_INT}
};

sui_args_tinfo_t sui_args_tinfo_coord2 = {
	.count = 2,
	.call_va_bare = va_bare_int2,
	.call_va_context = va_context_int2,
	.args = { SUI_TYPE_COORD, SUI_TYPE_COORD}
};


/******************************************************************************/
typedef int va_bare_long2_fnc(long arg1, long arg2);

static int va_bare_long2(int (*fnc)(void), va_list args)
{
  va_bare_long2_fnc *f=(va_bare_long2_fnc*)fnc;
  long a1 = va_arg(args, long);
  long a2 = va_arg( args, long);
  return f( a1, a2); // sui_coordinate_t
}

typedef int va_context_long2_fnc(void *context, long arg1, long arg2);

static int va_context_long2(int (*fnc)(void), void *context, va_list args)
{
  va_context_long2_fnc *f=(va_context_long2_fnc*)fnc;
  long a1 = va_arg(args, long);
  long a2 = va_arg(args, long);
  return f(context, a1, a2); // sui_coordinate_t
}

sui_args_tinfo_t sui_args_tinfo_long2 = {
  .count = 2,
  .call_va_bare = va_bare_long2,
  .call_va_context = va_context_long2,
  .args = { SUI_TYPE_LONG, SUI_TYPE_LONG}
};

/******************************************************************************/
typedef int va_bare_utf8_fnc( utf8 *arg1);
static int va_bare_utf8( int (*fnc)(void), va_list args)
{
	va_bare_utf8_fnc *f=(va_bare_utf8_fnc*)fnc;
	utf8 *a1 = va_arg( args, utf8*);
	return f(a1);
}

typedef int va_context_utf8_fnc( void *context, utf8 *arg1);
static int va_context_utf8( int (*fnc)(void), void *context, va_list args)
{
	va_context_utf8_fnc *f=(va_context_utf8_fnc *)fnc;
	utf8 *a1 = va_arg( args, utf8*);
	return f(context, a1);
}

sui_args_tinfo_t sui_args_tinfo_utf8 = {
	.count = 1,
	.call_va_bare = va_bare_utf8,
	.call_va_context = va_context_utf8,
	.args = { SUI_TYPE_UTF8}
};

/******************************************************************************/
typedef int va_bare_widget_fnc( struct sui_widget *arg1);
static int va_bare_widget( int (*fnc)(void), va_list args)
{
	va_bare_widget_fnc *f=(va_bare_widget_fnc*)fnc;
	struct sui_widget *a1 = va_arg( args, struct sui_widget *);
	return f(a1);
}

typedef int va_context_widget_fnc( void *context, struct sui_widget *arg1);
static int va_context_widget( int (*fnc)(void), void *context, va_list args)
{
	va_context_widget_fnc *f=(va_context_widget_fnc *)fnc;
	struct sui_widget *a1 = va_arg( args, struct sui_widget *);
	return f(context, a1);
}

sui_args_tinfo_t sui_args_tinfo_widget = {
	.count = 1,
	.call_va_bare = va_bare_widget,
	.call_va_context = va_context_widget,
	.args = { SUI_TYPE_WIDGET}
};

/******************************************************************************/
typedef int va_bare_utf8_int_fnc(utf8 *arg1, int arg2);
static int va_bare_utf8_int(int (*fnc)(void), va_list args)
{
  va_bare_utf8_int_fnc *f=(va_bare_utf8_int_fnc*)fnc;
  utf8 *a1 = va_arg(args, utf8 *);
  int a2 = va_arg(args, int);
  return f(a1, a2);
}

typedef int va_context_utf8_int_fnc( void *context, utf8 *arg1, int arg2);
static int va_context_utf8_int( int (*fnc)(void), void *context, va_list args)
{
  va_context_utf8_int_fnc *f=(va_context_utf8_int_fnc *)fnc;
  utf8 *a1 = va_arg(args, utf8 *);
  int a2 = va_arg(args, int);
  return f(context, a1, a2);
}

sui_args_tinfo_t sui_args_tinfo_utf8_int = {
  .count = 2,
  .call_va_bare = va_bare_utf8_int,
  .call_va_context = va_context_utf8_int,
  .args = {SUI_TYPE_UTF8, SUI_TYPE_INT}
};
