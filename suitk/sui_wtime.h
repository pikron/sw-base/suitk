/* sui_wtime.h
 *
 * SUITK time widget. (Time and date)
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#ifndef _SUI_TIME_WIDGET_
#define _SUI_TIME_WIDGET_
  
#include <suitk/suitk.h>

/******************************************************************************
 * Time widget extension
 ******************************************************************************/
/**
 * struct suiw_time - Additional time and date widget structure
 * @data: DINFO with time in seconds
 * File: sui_time.h
 */
typedef struct suiw_time {
  sui_dinfo_t   *data;
  utf8          *format;
  sui_align_t    align;

  sui_dinfo_t   *day_name; /* names of days of week (index from 0-6) */
  sui_dinfo_t   *month_name; /* names of months (index from 0 to 11) */
  sui_dinfo_t   *ampm_name; /* names of AM/PM indicator */
  
  sui_time_elements_t edtime; /* time structure for editing */
  
//  unsigned long intvalue; /* original value for ONLINE editing */

  sui_flags_t saved_flags; /* saved widget flag for editing */

  unsigned short item_type; /* type and flags of selected time item */
  short item_idx; /* current selected time item in format text */
  short item_cnt;  /* position in item of selected char */
  short item_size; /* number of character of item text */
  short maxidx; /* number of time items in format text */
} suiw_time_t;

#define sui_timwdg( wdg)  ((suiw_time_t *)wdg->data)

/**
 * enum time_flags - Time and date widget special flags ['SUDF_' prefix]
 * @SUDF_99HOURS: Time is realtive and can have 99 hours
 * File: sui_time.h
 */
enum time_flags {
  SUDF_99HOURS    = 0x00010000,
  SUDF_ONLINEEDIT = 0x00020000, /* value will be editen only inside widget and dinfo will be updated after SUCM_CONFIRM */
  SUDF_CHECKLIMITS= 0x00040000, /* check limits (only for 99hours) - widget shows '---' if time is bigger then 99:59:59 */
  SUDF_MUSTBEVALID= 0x00080000, /* edited value must be valid */
  SUDF_LOCALTIME  = 0x00100000, /* time according to local timezone */

  SUDF_INTBVMODE  = 0x80000000, /* internal flag - BadValue mode - when flag SUDF_MUSTBEVALID is used and time is out of the range */
};

#define SUDF_EDIT_CHANGE_MASK (SUFL_HIGHLIGHTED) /* | SUFL_BLINKFULL) */ /* SUFL_TRANSPARENT |  */

enum sui_time_element_type {
  SUTE_NONE    = 0x0000,
  SUTE_SECOND  = 0x0001, /* second */
  SUTE_MINUTE  = 0x0002, /* minute */
  SUTE_HOUR    = 0x0003, /* hour */
  SUTE_DAY     = 0x0004, /* day of month */
  SUTE_MONTH   = 0x0005, /* month in year */
  SUTE_YEAR    = 0x0006, /* year */
  SUTE_DOW     = 0x0007, /* day of week */
  SUTE_DOY     = 0x0008, /* day of year */
  SUTE_TIME    = 0x000F, /* time in seconds from ??? */
  SUTE_EMASK   = 0x000F, /* mask for element */

  SUTE_HR_12   = 0x0100, /* hour in 12 hours cycle */
  SUTE_HR_AMPM = 0x0200, /* hour AM/PM indicator */
  SUTE_YR_CENT = 0x0100, /* year as century */
  SUTE_YR_100  = 0x0200, /* year of century */
  SUTE_DY_WOY  = 0x0100, /* day of year and day of week as week of year */
  SUTE_SPCMASK = 0x0F00, /* mask for special representation of element */

  SUTE_TEXT    = 0x1000, /* element is in text representation */
  SUTE_99HOURS = 0x2000, /* elements is hours with max=99 */
};

/* Base year in RTEMS is 1988 */
#define SUI_TIME_BASE_YEAR 1988

/******************************************************************************
 * Time slot functions
 ******************************************************************************/
int sui_wtime_set_format( sui_widget_t *widget, utf8 *text);

/******************************************************************************
 * Time widget vmt & signal-slot mechanism
 ******************************************************************************/
typedef sui_widget_t sui_wtime_t;

#define sui_wtime_vmt_fields(new_type, parent_type) \
  sui_widget_vmt_fields( new_type, parent_type) \
  int (*settext)(parent_type##_t *self, utf8 *str);

typedef struct sui_wtime_vmt {
  sui_wtime_vmt_fields(sui_wtime, sui_widget)
} sui_wtime_vmt_t;

static inline sui_wtime_vmt_t *
sui_wtime_vmt(sui_widget_t *self)
{
  return (sui_wtime_vmt_t*)(self->base.vmt);
}

extern sui_wtime_vmt_t sui_wtime_vmt_data;

/******************************************************************************
 * Time widget basic functions
 ******************************************************************************/
sui_widget_t *sui_wtime( sui_rect_t *aplace, utf8 *alabel, sui_style_t *astyle, 
    sui_flags_t aflags, sui_dinfo_t *adi, utf8 *aformat, sui_align_t al);

  
#endif /* _SUI_TIME_WIDGET_ */
