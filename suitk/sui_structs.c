/* sui_struct.c
 *
 * SUITK additional structures - list, suffix and table structures.
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#include <string.h>

#include "sui_structs.h"
#include <suiut/support.h>
#include "sui_signalslot.h"

#include <ulut/ul_gavl.h>
#include <ulut/ul_gavlcust.h>

#include <suiut/sui_di_text.h>

#include <ul_log.h>

UL_LOG_CUST(ulogd_suitk)

/******************************************************************************
 * List
 ******************************************************************************/
/* List GAVL implementation */
GAVL_CUST_NODE_INT_IMP( sui_list_gavl, sui_list_t, sui_list_item_t, 
                        sui_listcoord_t, list_root, list_node, idx, 
                        sui_list_item_index_cmp)


/******************************************************************************/
/* List Item functions */

/**
 * sui_list_item_destroy - destroy list item
 * @item: pointer to list item
 */
int sui_list_item_destroy( sui_list_item_t *item)
{
  if ( !item) return -1;
  switch( sui_list_item_type( item)) {
      case SULI_TYPE_TEXT:
          sui_utf8_dec_refcnt( item->data.text);
          break;
      case SULI_TYPE_DINFO:
          sui_dinfo_dec_refcnt( item->data.dinfo.di);
          sui_finfo_dec_refcnt( item->data.dinfo.fi);
          break;
      case SULI_TYPE_LIST:
          sui_dinfo_dec_refcnt(item->data.dilist.di);
          sui_list_dec_refcnt(item->data.dilist.li);
      case SULI_TYPE_PRINT:
          sui_list_dec_refcnt(item->data.liprint);
  }
  free( item);
  return 0;
}

/**
 * sui_list_del_item - Delete item from list
 * @list: Pointer to list structure.
 * @idx: Index of item,which will be deleted.
 *
 * The function deletes item with index 'idx' from list.
 * Return Value: The function return zero as success and negative value as error.
 * File: sui_list.c
 */
int sui_list_item_del( sui_list_t *list, sui_listcoord_t idx)
{
  sui_list_item_t *item;
    
  item = sui_list_gavl_find( list, &idx);
  if ( !item) return -1;
  if ( sui_list_gavl_delete( list, item) > 0) {
    list->itemcnt--;
    if ( sui_list_item_destroy( item) >= 0) {
      sui_obj_emit((sui_obj_t *)list, SUI_SIG_REMOVED, idx);
      return 0;
    }
  }
  return -1;
}

/**
 * sui_list_add_item - Add item into list
 * @list: Pointer to list structure.
 * @item: Pointer to added item.
 * @idx: Position in list for added item or -1 for add item to end of list.
 *
 * The function adds item into list.
 * Return Value: The function returns a negative value as an error or it returns index of the added item (zero or positive number).
 * File: sui_list.c
 */
int sui_list_item_add( sui_list_t *list, sui_list_item_t *item, sui_listcoord_t idx)
{
  sui_listcoord_t newmax = -1;
  if (!list || !item) return -1;
  if (idx < 0) {
    idx = list->maxidx;
    newmax = idx+1;
  } else if (idx >= list->maxidx) {
    newmax = idx+1;
  } else {
    sui_list_item_t *olditem = sui_list_gavl_find(list, &idx);
    if (olditem) return 1;    /* item with index idx is in list */
  }
  item->idx = idx;
  if (sui_list_gavl_insert(list, item) > 0) { /* FIXME: only greater or '>=' */
    list->itemcnt++;
    if (newmax >=0) list->maxidx = newmax;
    sui_obj_emit( (sui_obj_t *)list, SUI_SIG_ADDED, idx);
    return idx;
  } else
    return -1;
}

int sui_list_item_change(sui_list_t *list, sui_listcoord_t idx,
                         const sui_list_item_t *newitem, unsigned char chflags)
{
  sui_list_item_t *item = sui_list_gavl_find(list, &idx);
  if (!item) return -1;
  if (chflags & SULICH_CONTENT) {
    switch(item->flags & SULI_TYPE_MASK) {
      case SULI_TYPE_TEXT:
        sui_utf8_dec_refcnt(item->data.text);
        item->data.text = NULL;
        break;
      case SULI_TYPE_DINFO:
        sui_dinfo_dec_refcnt(item->data.dinfo.di);
        item->data.dinfo.di=NULL;
        sui_finfo_dec_refcnt(item->data.dinfo.fi);
        item->data.dinfo.fi=NULL;
        break;
      case SULI_TYPE_LIST:
        sui_dinfo_dec_refcnt(item->data.dilist.di);
        item->data.dilist.di=NULL;
        sui_list_dec_refcnt(item->data.dilist.li);
        item->data.dilist.li=NULL;
        break;
      case SULI_TYPE_PRINT:
        sui_list_dec_refcnt(item->data.liprint);
        item->data.liprint=NULL;
        break;
      default: return -1;
    }
    item->flags &= ~SULI_TYPE_MASK;

    item->flags |= newitem->flags & SULI_TYPE_MASK;
    switch(newitem->flags & SULI_TYPE_MASK) {
      case SULI_TYPE_TEXT:
        sui_utf8_inc_refcnt(newitem->data.text);
        item->data.text = newitem->data.text;
        break;
      case SULI_TYPE_DINFO:
        sui_dinfo_inc_refcnt(newitem->data.dinfo.di);
        item->data.dinfo.di = newitem->data.dinfo.di;
        sui_finfo_inc_refcnt(newitem->data.dinfo.fi);
        item->data.dinfo.fi = newitem->data.dinfo.fi;
        break;
      case SULI_TYPE_LIST:
        sui_dinfo_inc_refcnt(newitem->data.dilist.di);
        item->data.dilist.di = newitem->data.dilist.di;
        sui_list_inc_refcnt(newitem->data.dilist.li);
        item->data.dilist.li = newitem->data.dilist.li;
        break;
      case SULI_TYPE_PRINT:
        sui_list_inc_refcnt(newitem->data.liprint);
        item->data.liprint = newitem->data.liprint;
      default: return -1;
    }
  }
  if (chflags & SULICH_INDEX) {
/* FIXME: emit two signals SUI_SIG_CHANGED with old and new indexes ? */
    if (sui_list_gavl_delete( list, item)<=0) return -1;
    item->idx = newitem->idx;
    if(sui_list_gavl_insert(list, item)<=0) return -1;
  }
  if (chflags & SULICH_USER_INDEX)
    item->index = newitem->index;
  if (chflags & SULICH_IMAGE_INDEX)
    item->imgidx = newitem->imgidx;
  if (chflags & SULICH_FLAGS)
    item->flags = (item->flags & SULI_TYPE_MASK) | (newitem->flags & ~SULI_TYPE_MASK);
  if (chflags & SULICH_SET_FLAGS)
    item->flags = item->flags | (newitem->flags & ~SULI_TYPE_MASK);
  if (chflags & SULICH_CLEAR_FLAGS) {
    unsigned char tmp = ~newitem->flags & ~SULI_TYPE_MASK;
    item->flags = (item->flags & SULI_TYPE_MASK) | (item->flags & tmp);
  }

  sui_obj_emit( (sui_obj_t *)list, SUI_SIG_CHANGED, idx);
  return 0;
}

/**
 * sui_list_item_create - creates one list item according to arguments
 * @text: pointer to utf8 text
 * @di: pointer to dinfo
 * @fi: pointer to format
 * @li: pointer to list
 */
sui_list_item_t *sui_list_item_create(utf8 *text, sui_dinfo_t *di,
                                       sui_finfo_t *fi, sui_list_t *li)
{
    sui_list_item_t *ni = sui_malloc(sizeof(sui_list_item_t));
    if (ni) {
        ni->idx = -1;
        if (text) {
            sui_utf8_inc_refcnt(text);
            ni->data.text = text;
            ni->flags = SULI_TYPE_TEXT;
        } else if (di) {
          if (li) {
            sui_dinfo_inc_refcnt(di);
            ni->data.dilist.di = di;
            sui_list_inc_refcnt(li);
            ni->data.dilist.li = li;
            ni->flags = SULI_TYPE_LIST;
          } else {
            sui_dinfo_inc_refcnt(di);
            ni->data.dinfo.di = di;
            sui_finfo_inc_refcnt(fi);
            ni->data.dinfo.fi = fi;
            ni->flags = SULI_TYPE_DINFO;
          }
/*
        } else if ( wdg) {
            sui_inc_refcnt( wdg);
            ni->data.widget = wdg;
            ni->flags = SULI_TYPE_WIDGET;
*/
        } else if (li) { /* only list and no dinfo -> print style list */
          sui_list_inc_refcnt(li);
          ni->data.liprint = li;
          ni->flags = SULI_TYPE_PRINT;
        }
    }
    return ni;
}
 
/**
 * sui_list_clear - Clear list from items
 * @list: Pointer to list structure.
 *
 * The function clear all list items.
 * Return Value: The function does not return a value.
 * File: sui_list.c
 */
void sui_list_clear( sui_list_t *list)
{
    sui_list_item_t *item;
    while((item=sui_list_gavl_cut_first(list))){
        sui_list_item_destroy( item);
        list->itemcnt--;
    }
    list->maxidx = 0;
}

/**
 * sui_list_get_print - returns text from list composed in a print style system
 * @list: pointer to list
 * @buf: pointer to output text buffer
 * The list must contain item with index 0, which contains format string. All
 * other necessary values must be in the same order ass in format text.
 */
int sui_list_get_print(sui_list_t *list, int mainidx, utf8 **buf)
{
  utf8 *format = NULL, *tmpbuf;
  utf8char *formtext = NULL, *prtext;
  sui_list_item_t *item;
  sui_listcoord_t idx = 0;
  int lastpos, pos, len, prlen;
  utf8char outstr[512]; /* FIXME: dynamic */
  int outlen = 0;

  if (!list || !buf) return -1;
  item = sui_list_gavl_find(list, &idx);
  if (!item) return -1;
  if (item->flags & SULI_INDEXED) prlen = mainidx;
  else prlen = 0;
  if (sui_list_item_get_text_with_index(item, prlen, &format) < 0) return -1;

  lastpos = 0;
  idx++;
  formtext = sui_utf8_get_text(format);
  len = sui_utf8_bytesize(format);
//ul_logdeb("PRINT-Fmtstr='%s'\n",formtext);
  while (item) {
    pos = lastpos;
    while(pos<len) {
      if (*(formtext+pos)=='%') break;
      pos++;
    }
    if (pos-lastpos) {
      memcpy(outstr+outlen,formtext+lastpos, pos-lastpos);
      outlen += pos-lastpos;
    }
    if (pos==len) break; /* end of compose */
    if ( *(formtext+pos+1)!='%') {
//ul_logdeb("PRINT-Position=%d\n",pos);
      item = sui_list_gavl_find(list, &idx);
      if (item->flags & SULI_INDEXED) prlen = mainidx;
      else prlen = 0;
      if (sui_list_item_get_text_with_index(item, prlen, &tmpbuf)<0) tmpbuf = U8"?";
      prtext = sui_utf8_get_text(tmpbuf);
      prlen = sui_utf8_bytesize(tmpbuf);
      if (*(prtext+prlen-1)==0) prlen--;
      memcpy(outstr+outlen, prtext, prlen);
      outlen += prlen;
//ul_logdeb("PRINT-added='%s'\n",prtext);

      sui_utf8_dec_refcnt(tmpbuf);
      tmpbuf = NULL;
  
      lastpos = pos+1;
      idx++;
    } else {
      *(outstr+outlen)='%';
      outlen++;
      lastpos =pos+2;
      pos += 2;
    }
  }
  if (outlen) {
    *(outstr+outlen)=0;
//ul_logdeb("PRINT:'%s'\n",outstr);
    *buf = sui_utf8_dynamic(outstr, -1, -1);
  } else {
    *buf = NULL;
  }
//list_get_print_end:
  sui_utf8_dec_refcnt(format);
  return 0;
}

/**
 * sui_list_item_get_text - return text of list item
 * @item: pointer to list item
 * @buf: pointer to output utf8 buffer
 * The function get text (or transform dinfo to text) and
 * put it to the output utf8 buffer.
 */
int sui_list_item_get_text(sui_list_item_t *item, utf8 **buf)
{
  if (item == NULL || buf==NULL) return -1;
  switch(item->flags & SULI_TYPE_MASK) {
    case SULI_TYPE_TEXT:
      sui_utf8_inc_refcnt(item->data.text);
      *buf = item->data.text;
      break;
    case SULI_TYPE_DINFO:
      *buf = sui_dinfo_2_utf8(item->data.dinfo.di, 0, item->data.dinfo.fi, 0);
      if (*buf==NULL) return -1;
      break;
    case SULI_TYPE_LIST:
      {
        long inidx;
        sui_listcoord_t liinidx;
        sui_list_item_t *initem;
        if (sui_rd_long(item->data.dilist.di, 0, &inidx)!= SUI_RET_OK)
          return -1;
        liinidx = inidx;
        initem = sui_list_gavl_find(item->data.dilist.li, &liinidx);
        if (!initem) return -1;
        return sui_list_item_get_text(initem, buf);
      }
      break;
    case SULI_TYPE_PRINT:
      return sui_list_get_print(item->data.liprint, 0, buf);
      break;
    default:
      return -1;
      break;
  }
  return 0;
}

/**
 * sui_list_item_get_text_with_index - return content of list item (with index to array dinfo)
 */
int sui_list_item_get_text_with_index(sui_list_item_t *item, int idx, utf8 **buf)
{
  if (!item || !buf) return -1;
//  if (!(item->flags & SULI_INDEXED)) {
//    return sui_list_item_get_text(item, buf);
//  } else {
    switch(item->flags & SULI_TYPE_MASK) {
      case SULI_TYPE_TEXT:
        sui_utf8_inc_refcnt(item->data.text); /* indexed text ? e.g. #index:text */
        *buf = item->data.text;
        break;
      case SULI_TYPE_DINFO:
        if (!(item->flags & SULI_INDEXED)) idx = 0;
        *buf = sui_dinfo_2_utf8(item->data.dinfo.di, idx, item->data.dinfo.fi, 0);
        if (*buf==NULL) return -1;
        break;
      case SULI_TYPE_LIST:
        {
          sui_listcoord_t liinidx;
          sui_list_item_t *initem;
          {
            long inidx;
            if (!(item->flags & SULI_INDEXED)) idx = 0;
            if (sui_rd_long(item->data.dilist.di, idx, &inidx)!= SUI_RET_OK)
              return -1;
            liinidx = inidx;
          }
          initem = sui_list_gavl_find(item->data.dilist.li, &liinidx);
          if (!initem) return -1;
          return sui_list_item_get_text(initem, buf);
        }
        break;
      case SULI_TYPE_PRINT:
        if (!(item->flags & SULI_INDEXED)) idx = 0;
        return sui_list_get_print(item->data.liprint, idx, buf);
        break;
      default:
        return -1;
        break;
    }
//  }
  return 0;
}

/******************************************************************************/
SUI_CANBE_CONST
sui_obj_signal_tinfo_t sui_list_signal_tinfo[] = {
  { "item_added", SUI_SIG_ADDED, &sui_args_tinfo_int},
  { "item_removed", SUI_SIG_REMOVED, &sui_args_tinfo_int},
  { "item_changed", SUI_SIG_CHANGED, &sui_args_tinfo_int},
};

SUI_CANBE_CONST
sui_obj_slot_tinfo_t sui_list_slot_tinfo[] = {
/*  { "show_tip", SUI_SLOT_APP_SHOW_TIP, &sui_args_tinfo_widget,
    .target_kind=SUI_STGT_METHOD_OFFSET,
    .target.method_offset=UL_OFFSETOF(sui_application_vmt_t, show_tip)},
  { "hide_tip", SUI_SLOT_APP_HIDE_TIP, &sui_args_tinfo_widget,
    .target_kind=SUI_STGT_METHOD_OFFSET,
    .target.method_offset=UL_OFFSETOF(sui_application_vmt_t, hide_tip)},*/
};

SUI_CANBE_CONST
sui_obj_tinfo_t sui_list_vmt_obj_tinfo = {
  .name = "sui_list",
  .obj_size = sizeof(sui_list_t),
  .obj_align = UL_ALIGNOF(sui_list_t),
  .signal_count = MY_ARRAY_SIZE(sui_list_signal_tinfo),
  .signal_tinfo = UL_CAST_UNQ1(sui_obj_signal_tinfo_t *, sui_list_signal_tinfo),
  .slot_count = MY_ARRAY_SIZE(sui_list_slot_tinfo),
  .slot_tinfo = UL_CAST_UNQ1(sui_obj_slot_tinfo_t *, sui_list_slot_tinfo)
};


int sui_list_vmt_init( sui_list_t *list, void *params)
{
  list->refcnt = 1;
  sui_list_gavl_init_root_field(list);
  return 0;
}

void sui_list_vmt_done( sui_list_t *list)
{
  sui_list_clear( list);
}

/**
 * sui_list_inc_refcnt - Increment list structure reference counter
 * @list: Pointer to list structure.
 *
 * The function increments reference counter of list described
 * by sui_list_t structure.
 * Return Value: The function does not return a value.
 * File: sui_list.c
 */
sui_refcnt_t sui_list_inc_refcnt( sui_list_t *list)
{
  if (!list) return SUI_REFCNTERR;
  if(list->refcnt>=0) list->refcnt++;
  return list->refcnt;
}

/**
 * sui_list_dec_refcnt - Decrement list structure reference counter
 * @style: Pointer to list structure.
 *
 * The function decrements reference counter of list described
 * by sui_list_t structure. If the counter is decremented to 
 * zero list structure will be destroyed.
 * Return Value: The function does not return a value.
 * File: sui_list.c
 */
sui_refcnt_t sui_list_dec_refcnt( sui_list_t *list)
{
  sui_refcnt_t ref;
  if(!list) return SUI_REFCNTERR;
  if(list->refcnt>0) list->refcnt--;
  if(!(ref=list->refcnt)) {
    sui_obj_done((sui_obj_t *)list);
    free( list);
  }
  return ref;
}

sui_list_vmt_t sui_list_vmt_data = {
  .vmt_size = sizeof(sui_list_vmt_t),
  .vmt_refcnt = SUI_STATIC,
  .vmt_parent = &sui_obj_vmt_data,
  .vmt_init   = sui_list_vmt_init,
  .vmt_done   = sui_list_vmt_done,
  .vmt_obj_tinfo = UL_CAST_UNQ1(sui_obj_tinfo_t *, &sui_list_vmt_obj_tinfo),

  .inc_refcnt = sui_list_inc_refcnt,
  .dec_refcnt = sui_list_dec_refcnt,

//  .show_tip = sui_application_show_tip,
//  .hide_tip = sui_application_hide_tip,
};


/******************************************************************************
 * Suffix
 ******************************************************************************/
/**
 * sui_suffix_inc_refcnt - Increment number decimal digits and suffix structure reference counter.
 * @ctab: Pointer to decimal digits and suffix structure.
 *
 * The function increments decimal and suffix structure reference counter.
 * Return Value: The function does not return a value.
 * File: sui_number.c
 */
void sui_suffix_inc_refcnt(sui_suffix_t *nsuf) {
  if (!nsuf) return;
  if (nsuf->refcnt>=0) nsuf->refcnt++;
}

/**
 * sui_suffix_dec_refcnt - Decrement decimal and suffix structure reference counter.
 * @ctab: Pointer to decimal digits and suffix structure.
 *
 * The function decrements decimal digits and suffix structure reference counter.
 * If the counter is decremented to zero color table structure will be deleted.
 * Return Value: The function does not return a value.
 * File: sui_number.c
 */
void sui_suffix_dec_refcnt(sui_suffix_t *nsuf) {
  if (!nsuf) return;
  if (nsuf->refcnt>0) nsuf->refcnt--;
  if (!nsuf->refcnt) {
    sui_utf8_dec_refcnt(nsuf->suffix);
    nsuf->suffix = NULL;
    sui_fonti_dec_refcnt(nsuf->decfonti);
    nsuf->decfonti = NULL;
    sui_fonti_dec_refcnt(nsuf->suffonti);
    nsuf->suffonti = NULL;
    sui_obj_done_vmt((sui_obj_t *)nsuf, UL_CAST_UNQ1(sui_obj_vmt_t *, &sui_suffix_vmt_data));
    free(nsuf);
  }
}

/**
 * sui_create_suffix - Create dynamic decimal and suffix structure
 * @asuffix: Postfix string in utf8 (normally units)
 * @adecfi: Pointer to font info used for drawing decimal digits.
 * @asuffi: Pointer to font info used for drawing suffix string.
 * @adecalign: Alignment of decimal digits to integer digits of number.
 * @asufalign: Alignment of suffix string to all number.
 *
 * The function creates sui_suffix structure.
 * Return Value: The function returns pointer to created sui_suffix structure.
 * File: sui_number.c
 */
sui_suffix_t *sui_create_suffix( utf8 *asuffix, sui_font_info_t *adecfi, 
                                 sui_font_info_t *asuffi, sui_align_t adecalign, 
                                 sui_align_t asufalign)
{
  sui_suffix_t *wnp = sui_malloc( sizeof(sui_suffix_t));
  if ( wnp) {
    sui_suffix_inc_refcnt( wnp);

    sui_fonti_inc_refcnt( asuffi);
    wnp->suffonti = asuffi;
    sui_fonti_inc_refcnt( adecfi);
    wnp->decfonti = adecfi;
    sui_utf8_inc_refcnt( asuffix);
    wnp->suffix = asuffix;
    wnp->decalign = adecalign;
    wnp->sufalign = asufalign;
  }
  return wnp;
}

