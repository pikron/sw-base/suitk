/* sui_event.c
 *
 * SUITK basic event functions
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#include <string.h>

#ifdef WITH_SELECT
  #include <unistd.h>
  #include <stdlib.h>
  #include <errno.h>
#endif

#ifdef WITH_RTEMS_UID
  #include <errno.h>
  #include <rtems/mw_uid.h>
#else /*WITH_RTEMS_UID*/
  #include <suitk/sui_gdi.h>
#endif /*WITH_RTEMS_UID*/

#include "sui_event.h"

#include <mwtypes.h>

#include <ul_log.h>

UL_LOG_CUST(ulogd_suitk)

sui_event_global_var_t sui_globev = { 0, 0, 0, 0};
sui_event_buffers_t *curevbufs = NULL;

//  static sui_event_t sui_event_buf;
//  static sui_event_t sui_global_event_buf;
  
//  extern int sui_check_kbd;
//  extern int sui_check_mouse;
//  short draw_request;
  /*extern unsigned short sui_last_mobkey;*/


/**
 * sui_put_event - Put event to event queue
 * @event: Pointer to event structure.
 *
 * If the event is something else than nothing, the function will put the event 
 * to the queue.
 * Return Value: The function returns -1 if event queue is full and 0 if
 * the event is put into queue
 * File: sui_base.c
 */
int sui_put_event( sui_event_t *event)
{
	if( curevbufs->sui_event_buf.what != SUEV_NOTHING) return -1;
	curevbufs->sui_event_buf = *event;
	return 0;
}

/**
 * sui_flush_event - Flush event buffer
 *
 * If the event is something else than nothing, the function will put the event 
 * to the queue.
 * Return Value: The function returns -1 if error occurs
 * File: sui_base.c
 */
int sui_flush_event( void)
{
	curevbufs->sui_event_buf.what = SUEV_NOTHING;
	return 0;
}

int sui_flush_global_event( void)
{
	curevbufs->sui_global_event_buf.what = SUEV_NOTHING;
	return 0;
}

/**
* sui_put_global_event - put event to global event queue
*/
int sui_put_global_event( sui_event_t *event)
{
	if ( curevbufs->sui_global_event_buf.what != SUEV_NOTHING) return -1;
	curevbufs->sui_global_event_buf = *event;
	return 0;
}

/**
* sui_event_draw_deffer_inc - stop next (re)draw events till unblocked
*/
int sui_event_draw_deffer_inc(void)
{
  return (curevbufs->draw_request_deffer++);
}

/**
* sui_event_draw_deffer_dec - lower level of (re)draw events blocking
*/
int sui_event_draw_deffer_dec(void)
{
  if(curevbufs->draw_request_deffer>0)
    curevbufs->draw_request_deffer--;
  return curevbufs->draw_request_deffer;
}

/**
* sui_event_draw_deffer_dec - lower level of (re)draw events blocking
*/
int sui_event_draw_deffer_clear(void)
{
  int ret=curevbufs->draw_request_deffer;
  curevbufs->draw_request_deffer=0;
  return ret;
}

/**
 * sui_count_repeated_key - key hold counter
 */
int sui_count_repeated_key( unsigned short keycode, int up)
{
	static unsigned short lastkey = 0;
	static unsigned short keycounter = 0;

	if ( up || (keycode!=lastkey)) {
		keycounter = 0;
		if ( up) keycode = 0;
	} else {
		if ( keycounter < 0xffff)
			keycounter++;
	}
	lastkey = keycode;
//	ul_loginf("Key(0x%04X [%d]<0x%04X>)=%d\n", keycode, up, lastkey, keycounter);
	return keycounter;
}

#ifdef WITH_SELECT
  int sui_event_select( long timeout)
  {
    int ret;
    struct timeval tv;
    fd_set rfset;
  
    if( sui_globdc->psd->PreSelect)
     sui_globdc->psd->PreSelect( sui_globdc->psd);
    FD_ZERO (&rfset);
    if(sui_globev.sui_mouse_fd >= 0)
        FD_SET ( sui_globev.sui_mouse_fd, &rfset);
    if(sui_globev.sui_kbd_fd >= 0)
        FD_SET ( sui_globev.sui_kbd_fd, &rfset);
    tv.tv_sec = 0;
    tv.tv_usec = timeout*1000;
    do{
      ret=select(FD_SETSIZE,&rfset, NULL, NULL,
                 (timeout>=0)?&tv:NULL);
    } while (ret==-1 && errno==-EINTR);

    if(sui_globev.sui_mouse_fd >= 0)
        if(FD_ISSET( sui_globev.sui_mouse_fd,&rfset))
            sui_globev.sui_check_mouse=1;
    if(sui_globev.sui_kbd_fd >= 0)
        if(FD_ISSET( sui_globev.sui_kbd_fd,&rfset))
            sui_globev.sui_check_kbd=1;

    return ret;
  }
#endif /* WITH_SELECT */

#ifdef WITH_RTEMS_UID
  int sui_get_uid_event( sui_event_t *event, long timeout)
  {
    struct MW_UID_MESSAGE m;
    int rc;

    rc = uid_read_message(&m, timeout);
    if( rc < 0 )
    {
      if( errno != ETIMEDOUT )
        /* Some error occurred */
        return 0;
      else{
	 /* timeout handling */
        return 0;
      }
    }
    
    switch( m.type )
    {
      /* Mouse or Touch Screen event */
      case MV_UID_REL_POS:
      case MV_UID_ABS_POS: {
	  MWCOORD x = m.m.pos.x;
	  MWCOORD y = m.m.pos.y; 
//	  MWCOORD z = m.m.pos.z;
	  int btns = m.m.pos.btns;
	  static int last_buttons=0;

          event->what=SUEV_MMOVE;
          event->mouse.x=x;
          event->mouse.y=y;
          /*MWBUTTON_L;MWBUTTON_M;MWBUTTON_R;*/
          event->mouse.buttons=btns;
          if(btns!=last_buttons){
            if(btns&~last_buttons) event->what=SUEV_MDOWN;
            else event->what=SUEV_MUP;
          }
          last_buttons=btns;

          return 1;
      }

      /* KBD event */
      case MV_UID_KBD: {
	  MWKEY keycode = m.m.kbd.code;
	  MWKEYMOD modifiers = m.m.kbd.modifiers;
	  MWSCANCODE scancode = 0;

	  /*ul_logdeb("Key %02X mod %02X scan %02X\n",keycode, modifiers, scancode);*/

          event->what=(modifiers&0x80)?SUEV_KUP:SUEV_KDOWN;
          event->keydown.keycode=keycode;
//          if ( keycode != sui_last_mobkey) sui_last_mobkey = 0;
          event->keydown.modifiers = modifiers&~0x80;
          event->keydown.scancode = scancode;
          event->keydown.repeats = sui_count_repeated_key( keycode, event->what==SUEV_KUP);
          return 1;
      }

      case MV_UID_INVALID:
//    	  ul_logdeb("No event");
          break;

      case MV_UID_TIMER:
          break;
    }  
    return 0;
  } 
#endif /*WITH_RTEMS_UID*/


/**
 * sui_get_event - Get event from event queue
 * @event: Pointer to output event structure.
 * @timeout: Timeout for waiting to receive event.
 *
 * The function gets event from queue and preprocesses it.
 * Return Value: The function returns 1 if event is get from queue
 * and 0 if event isn't get.
 * File: sui_base.c
 */
int sui_get_event( sui_event_t *event, long timeout)
  {
    int ret;
  
    if( curevbufs->sui_event_buf.what!=SUEV_NOTHING) {
      *event = curevbufs->sui_event_buf;
      curevbufs->sui_event_buf.what = SUEV_NOTHING;
      return 1;
    }
	if( curevbufs->sui_global_event_buf.what != SUEV_NOTHING) {
		*event = curevbufs->sui_global_event_buf;
		curevbufs->sui_global_event_buf.what = SUEV_NOTHING;
		return 1;
	}
	
    memset(event,0,sizeof(sui_event_t));
  
   #ifndef WITH_RTEMS_UID
    if(kbddev.Poll) {
      if(kbddev.Poll()) sui_globev.sui_check_kbd=1;
    } else
      if(sui_globev.sui_kbd_fd==0) sui_globev.sui_check_kbd=1;
  
    if(mousedev.Poll) {
      if(mousedev.Poll()) sui_globev.sui_check_kbd=1;
    } else
      if(sui_globev.sui_mouse_fd==0) sui_globev.sui_check_mouse=1;
  
    if(curevbufs->draw_request) timeout=0;
   #endif /*WITH_RTEMS_UID*/
  
   #ifdef WITH_SELECT
    sui_event_select( timeout);
   #endif /* WITH_SELECT */
   #ifdef WITH_RTEMS_UID
    if(sui_get_uid_event(event,timeout)>0)
      return 1;
   #else /*WITH_RTEMS_UID*/
    /*if((ret=kbddev.Read(&key, &modifiers, &scancode))>0) */
    if(sui_globev.sui_check_kbd){
      MWKEY key;
      MWKEYMOD modifiers;
      MWSCANCODE scancode;
      if((ret=GdReadKeyboard(&key, &modifiers, &scancode))>0){
        if(key!=MWKEY_REDRAW){
          event->what=(ret==1)?SUEV_KDOWN:SUEV_KUP;
          event->keydown.keycode=key;
//          if ( key != sui_last_mobkey) sui_last_mobkey = 0;
          event->keydown.modifiers=modifiers;
          event->keydown.scancode=scancode;
          event->keydown.repeats=sui_count_repeated_key( key, event->what==SUEV_KUP);
          return 1;
        }
        curevbufs->draw_request=SUEV_DRAW;
      } else sui_globev.sui_check_kbd=0;
    }
   #endif /*WITH_RTEMS_UID*/
   #ifndef WITH_RTEMS_UID
    if(mousedev.Read&&sui_globev.sui_check_mouse){
      MWCOORD x, y;
      int bp;
      static int last_buttons=0;
      /* if((ret=mousedev.Read(&x,&y,&z,&bp))>0) */
      if((ret=GdReadMouse(&x,&y,&bp))>0) {
        event->what=SUEV_MMOVE;
        event->mouse.x=x;
        event->mouse.y=y;
        /*MWBUTTON_L;MWBUTTON_M;MWBUTTON_R;*/
        event->mouse.buttons=bp;
        if(bp!=last_buttons){
          if(bp&~last_buttons) event->what=SUEV_MDOWN;
          else event->what=SUEV_MUP;
        }
        last_buttons=bp;
        return 1;
      } else sui_globev.sui_check_mouse=0;
    }
   #endif /*WITH_RTEMS_UID*/
    if((curevbufs->draw_request_deffer<=0)&&
       (curevbufs->draw_request)){
      event->what=curevbufs->draw_request;
      curevbufs->draw_request=0;
      event->draw.dc = sui_globdc;
      return 1;
    }
    return 0;
  }
