/* sui_mobilkbd.h
 *
 * SUITK mobile keyboard support
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#ifndef _SUI_MOBILKBD_
#define _SUI_MOBILKBD_

#include <suitk/suitk.h>
#include <ulut/ul_gavl.h>


/* mobil keys character table */
/**
 * enum sui_mk_mode - Mobile Keyboard mode flags [SUKM_ prefix]
 * @SUKM_NONE: Any flag is used.
 * @SUKM_CAPITALS: Capital characters keyboard mode.
 * @SUKM_SMALL: Small characters keyboard mode.
 * @SUKM_NUMBER: Number keyboard mode.
 * @SUKM_SELECT: Special select mode - character is selected from a table.
 * @SUKM_MODECHNG: Internal indication of mode changing.
 * @SUKM_CNTMASK: Mask for counting of already entered characters.
 * @SUKM_MODEMASK: Mask for testing the current keyboard mode.
 * @SUKM_SPCMASK: Mask
 * @SUKM_MODESHIFT: Number of bits for mode flags shifting
 *
 * File: sui_mobilkey.h
 */
enum sui_mk_mode {
  SUKM_NONE     = 0x0000, /* without mobil keyboard */
  SUKM_CAPITALS = 0x0100, /* ABC */
  SUKM_SMALL    = 0x0200, /* abc */
  SUKM_NUMBER   = 0x0400, /* 123 */
  SUKM_SELECT   = 0x0800, /* ?!. */
  SUKM_MODECHNG = 0x1000, /* last action was mode changing */
  SUKM_CNTMASK  = 0x00FF,
  SUKM_MODEMASK = 0x0F00,
  SUKM_SPCMASK  = 0xF000,
  
  SUKM_MODESHIFT = 8,
};

/******************************************************************************/
/**
 * struct sui_mk_list_key - Mobil keyboard list item
 * @key: Key identifierr
 */
typedef struct sui_mk_list_key {
  unsigned short  key;
  unsigned short  mode;
} sui_mk_list_key_t;

/**
 * struct sui_mk_key_map - Mobil keyboard information
 * @key_mode: Mode in which map operates
 */
typedef struct sui_mk_key_map {
  sui_mk_list_key_t key_mode;
  utf8           *chars;
  gavl_node_t     map_node;
} sui_mk_key_map_t;

/**
 * struct sui_mk_keyboard_map - Structure with all defined transitions mobil-keyboard
 * @map_root: GAVL tree holding map for key translations
 */
typedef struct sui_mk_keyboard_map {
  gavl_cust_root_field_t map_root;
} sui_mk_keyboard_map_t;

/**
 * sui_mk_key_map_cmp -Function for mobil-keyboard list item comparing
 * @a: Pointer to ano list item.
 * @b: Pointer to another ano list_item.
 *
 * The function compares two mobile-keyboard list items and returns
 * one of the following -1/0/1 values.
 *
 * Return Value: The function returns zero for the the same lists.
 * In all other cases it returns -1 or 1.
 *
 * File: sui_mobilkbd.h
 */
static inline int
sui_mk_key_map_cmp(const sui_mk_list_key_t *a, const sui_mk_list_key_t *b)
{
  if (a->key>b->key) return 1;
  if (a->key<b->key) return -1;
/* keys are the same */
  if ((a->mode & SUKM_MODEMASK)>(b->mode & SUKM_MODEMASK)) return 1;
  if ((a->mode & SUKM_MODEMASK)<(b->mode & SUKM_MODEMASK)) return -1;
  return 0;
}

GAVL_CUST_NODE_INT_DEC(sui_mk_gavl, sui_mk_keyboard_map_t, sui_mk_key_map_t,
                       sui_mk_list_key_t, map_root, map_node, key_mode,
                       sui_mk_key_map_cmp)

/******************************************************************************/
/*
 * return codes for the sui_mk_translate_key2char function
 */
enum sui_mk_ret_codes {
  SUI_MK_RET_NOKEY  = 0x00, /* key wasn't processed */
  SUI_MK_RET_OK     = 0x01, /* key was processed */
  SUI_MK_RET_REMOVE = 0x02, /* previous character must be removed */
  SUI_MK_RET_CHAR   = 0x04, /* returned character is valid */
};

int sui_mk_init(void);
int sui_mk_clear(void);
int sui_mk_done(void);

int sui_mk_translate_key2char(unsigned short key, unsigned short *mode, utf8char **retchar);
int sui_mk_clear_state(void);
int sui_mk_in_repeat_mode(void);

int sui_mk_set_repeat_time(unsigned long time_ms);
int sui_mk_set_mapping(sui_list_t *srclist);

int sui_mk_drawing(sui_dc_t *dc, sui_widget_t *widget);
int sui_mk_set_drawing(sui_widget_t *widget, sui_align_t align);
int sui_mk_remove_drawing(sui_widget_t *widget);

int sui_mk_mode_rdval(sui_dinfo_t *datai, long idx, void *buf);

#endif /* _SUI_MOBILKBD_ */
