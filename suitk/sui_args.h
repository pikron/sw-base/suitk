/* sui_args.h
 *
 * SUITK function arguments propagation.
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#ifndef _SUI_ARGS_H_
#define _SUI_ARGS_H_

#include <stdarg.h>
/*FIXME: sui_tinfo_t has to be declared somewhere else */
//#include "sui_dinfo.h"
#include <suiut/sui_types.h>
#include <suiut/sui_argsbase.h>

extern sui_args_tinfo_t sui_args_tinfo_int2;
extern sui_args_tinfo_t sui_args_tinfo_coord2;

extern sui_args_tinfo_t sui_args_tinfo_long2;

extern sui_args_tinfo_t sui_args_tinfo_utf8;

extern sui_args_tinfo_t sui_args_tinfo_widget;

extern sui_args_tinfo_t sui_args_tinfo_utf8_int;

#endif /*_SUI_ARGS_H_*/
