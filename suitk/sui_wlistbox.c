/* sui_wlistbox.c
 *
 * SUITK listbox widget
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#include "sui_wlistbox.h"

#include <ul_log.h>

UL_LOG_CUST(ulogd_suitk)

/******************************************************************************/
/* special string for all listbox widget - empty list */
utf8 *sui_wlistbox_common_empty_text = U8"Empty list";

utf8 *sui_wlistbox_ptr_empty_text = U8"X";

/******************************************************************************/
/* temporary special case insensitivy fcn */
/* FIXME: function must be updated with localization */
int wlist_nicmp( utf8 *one, utf8 *two, int chars)
{
  utf8char *otxt, *stxt;
  _UTF_UCS_TYPE ucs1, ucs2;
  
  if ( !one || !two) return -2; /* WHAT ELSE ??? */
  if ( chars < 0) chars = sui_utf8_length( two);
  if ( chars > sui_utf8_length( one)) return -1;
  otxt = sui_utf8_get_text( one); stxt = sui_utf8_get_text( two);
  while( chars--) {
    otxt += sui_utf8_to_ucs( otxt, &ucs1); stxt += sui_utf8_to_ucs( stxt, &ucs2);
    if ( ucs1 >= 'a' && ucs1 <='z') ucs1 -= 'a'-'A';
    if ( ucs2 >= 'a' && ucs2 <='z') ucs2 -= 'a'-'A';
    if ( ucs1 < ucs2) return -1;
    if ( ucs1 > ucs2) return 1;
  }
  return 0;
}

/**
 * sui_wlistbox_get_first - return the first item in the list
 */
static inline
sui_list_item_t *sui_wlistbox_get_first(sui_widget_t *widget)
{
  suiw_listbox_t *wlist = sui_lstwdg(widget);

  if (wlist->list == NULL)
    return NULL;

  if (wlist->filter.items && wlist->filter.cnt) { /* list is filtered/sorted */
//ul_logdeb("-GetFirst:i=%d\n",*wlist->filter.items);
    return sui_list_gavl_find(wlist->list, wlist->filter.items);
  } else if (wlist->filter.pattern.flags & SULF_PAT_FILTER_IS_USED) { // wlist->filter.pattern.data.text) {
    return NULL;
  } else {
    return sui_list_gavl_first(wlist->list);
  }
}
/**
 * sui_wlistbox_get_last - return the last item in the list
 */
static inline
sui_list_item_t *sui_wlistbox_get_last(sui_widget_t *widget)
{
  suiw_listbox_t *wlist = sui_lstwdg(widget);

  if (wlist->list == NULL)
    return NULL;

  if (wlist->filter.items && wlist->filter.cnt) { /* list is filtered/sorted */
//ul_logdeb("-GetLast:i=%d\n",*(wlist->filter.items+(wlist->filter.cnt-1)));
    return sui_list_gavl_find(wlist->list, wlist->filter.items+(wlist->filter.cnt-1));
  } else if (wlist->filter.pattern.flags & SULF_PAT_FILTER_IS_USED) { // wlist->filter.pattern.data.text) {
    return NULL;
  } else {
    return sui_list_gavl_last(wlist->list);
  }
}

/**
 * sui_wlist_get_next - return the next item
 */
static inline
sui_list_item_t *sui_wlistbox_get_next(sui_widget_t *widget, sui_list_item_t *item)
{
  suiw_listbox_t *wlist = sui_lstwdg(widget);

  if (wlist->list == NULL)
    return NULL;

  if (wlist->filter.items && wlist->filter.cnt) { /* list is filtered/sorted */
    sui_listcoord_t i=0;
  /* find index in the sorted array of the item*/
    while(i<wlist->filter.cnt) {
      if (*(wlist->filter.items+i)==item->idx) {
        if (i==wlist->filter.cnt-1) break;
//ul_logdeb("-GetNext:i=%d\n",*(wlist->filter.items+(i+1)));
        return sui_list_gavl_find(wlist->list, wlist->filter.items+(i+1));
      }
      i++;
    }
//ul_logdeb("-GetNext:END\n");
    return NULL;
  } else if (wlist->filter.pattern.flags & SULF_PAT_FILTER_IS_USED) { // wlist->filter.pattern.data.text) {
    return NULL;
  } else {
    return sui_list_gavl_next(wlist->list,item);
  }
}

/**
 * sui_wlist_get_prev - return the previous item
 */
static inline
sui_list_item_t *sui_wlistbox_get_prev(sui_widget_t *widget, sui_list_item_t *item)
{
  suiw_listbox_t *wlist = sui_lstwdg(widget);

  if (wlist->list == NULL)
    return NULL;

  if (wlist->filter.items && wlist->filter.cnt) { /* list is filtered/sorted */
    sui_listcoord_t i=0;
  /* find index in the sorted array of the item*/
    while(i<wlist->filter.cnt) {
      if (*(wlist->filter.items+i)==item->idx) {
        if (i==0) break;
//ul_logdeb("-GetNext:i=%d\n",*(wlist->filter.items+(i-1)));
        return sui_list_gavl_find(wlist->list, wlist->filter.items+(i-1));
      }
      i++;
    }
//ul_logdeb("-GetNext:BEGINNING\n");
    return NULL;
  } else if (wlist->filter.pattern.flags & SULF_PAT_FILTER_IS_USED) { // wlist->filter.pattern.data.text) {
    return NULL;
  } else {
    return sui_list_gavl_prev(wlist->list,item);
  }
}

/**
 * sui_wlistbox_get_remains - return number of items after engaged item
 */
inline
sui_listcoord_t sui_wlistbox_get_remains(sui_widget_t *widget, sui_list_item_t *item)
{
  suiw_listbox_t *wlist = sui_lstwdg(widget);
  sui_listcoord_t i=0;
  if (wlist->filter.items && wlist->filter.cnt) { /* list is filtered/sorted */
  /* find index in the sorted array of the item*/
    while(i<wlist->filter.cnt) {
      if (*(wlist->filter.items+i)==item->idx) {
//ul_logdeb("-GetRemains:cnt=%d\n",wlist->filter.cnt-i-1);
        return (wlist->filter.cnt-i-1);
      }
      i++;
    }
  } else if (wlist->filter.pattern.flags & SULF_PAT_FILTER_IS_USED) { // wlist->filter.pattern.data.text) {
    return 0;
  } else {
    if (item && (wlist->list != NULL)) {
      while((item=sui_list_gavl_next(wlist->list,item))!=NULL)
        i++;
      return i;
    }
  }
  return -1;
}

/**
 * sui_wlistbox_get_position - return number of items before engaged item
 */
static inline
sui_listcoord_t sui_wlistbox_get_position(sui_widget_t *widget, sui_list_item_t *item)
{
  suiw_listbox_t *wlist = sui_lstwdg(widget);
  sui_listcoord_t i=0;
  if (wlist->filter.items && wlist->filter.cnt) { /* list is filtered/sorted */
  /* find index in the sorted array of the item*/
    while(i<wlist->filter.cnt) {
      if (*(wlist->filter.items+i)==item->idx) {
//ul_logdeb("-GetPosition:pos=%d\n",i);
        return i;
      }
      i++;
    }
  } else if (wlist->filter.pattern.flags & SULF_PAT_FILTER_IS_USED) { // wlist->filter.pattern.data.text) {
    return 0;
  } else {
    if (item && (wlist->list != NULL)) {
      while((item=sui_list_gavl_prev(wlist->list,item))!=NULL)
        i++;
      return i;
    }
  }
  return -1;
}

/**
 * sui_wlistbox_find - return item with engaged index or NULL (if item isn't in the current list (visible))
 */
static inline
sui_list_item_t *sui_wlistbox_find(sui_widget_t *widget, sui_listcoord_t index)
{
  suiw_listbox_t *wlist = sui_lstwdg(widget);
  sui_listcoord_t i=0;

  if (wlist->list == NULL)
    return NULL;

  if (wlist->filter.items && wlist->filter.cnt) { /* list is filtered/sorted */
  /* find index in the sorted array of the item*/
    while(i<wlist->filter.cnt) {
      if (*(wlist->filter.items+i)==index) {
//ul_logdeb("-Found:idx=%d\n",index);
        return sui_list_gavl_find(wlist->list, &index);
      }
      i++;
    }
  } else if (wlist->filter.pattern.flags & SULF_PAT_FILTER_IS_USED) { // wlist->filter.pattern.data.text) {
    return NULL;
  } else {
    return sui_list_gavl_find(wlist->list, &index);
  }
  return NULL;
}

/**
 * sui_wlistbox_repair_position - function corrects current position
 */
int sui_wlistbox_repair_position(sui_widget_t *widget)
{
  sui_list_item_t *item;
/* repair current position */
  if (!sui_wlistbox_get_current_item(widget)) {
    long hlp = 0;
    item = sui_wlistbox_get_first(widget);
    if (item) {
ul_logdeb("Position moved to %d\n",item->idx);
      hlp = item->idx;
      return sui_wr_long(sui_lstwdg(widget)->pos, 0, &hlp);
    }
  }
  return SUI_RET_OK;
}

/* FIXME: use better filtering and sorting algorithms */
/**
 * sui_wlistbox_filter_and_sort - create array of indexes of sorted and filtered items
 */
int sui_wlistbox_filter_and_sort(sui_widget_t *widget)
{
  sui_sorting_item_t *sortarray=NULL;
  suiw_listbox_t *wlist = sui_lstwdg(widget);
  sui_list_item_t *item=NULL;
  utf8 *text=NULL;
  int i;
  int patlen;
  unsigned char patflg = wlist->filter.pattern.flags;

  if (wlist->filter.items) {
    sui_free( wlist->filter.items);
    wlist->filter.items = NULL;
    wlist->filter.cnt = 0;
//    wlist->filter.max = 0;
    wlist->filter.pattern.flags &= ~SULF_PAT_FILTER_IS_USED;
  }
/* empty list or no filtering and sorting */
  if (!wlist->list ||
     (!wlist->filter.pattern.data.text && !wlist->filter.pattern.flags &&
      !sui_test_flag(widget,SULF_SORTED))) {
    long lowb = 0, highb = 0;
    sui_list_item_t *fi = NULL;
    if (wlist->list != NULL)
      fi = sui_list_gavl_last(wlist->list);
    if (fi) highb = fi->idx - 1; /* it isn't correct !!! it needn't be a number of items in the list */
    sui_obj_emit((sui_obj_t *)widget, SUI_SIG_BOUNDS_SET, lowb, highb);
    return 0;
  }

  sortarray = sui_malloc(sizeof(sui_sorting_item_t)*wlist->list->itemcnt);
  if (!sortarray) return -1;

/* filter list */
  if (wlist->filter.pattern.data.text) {
    ul_logdeb("List filtering...\n");
    patlen = sui_utf8_length(wlist->filter.pattern.data.text);
  } else {
    patlen = 0;
  }
  i = 0;
  gavl_cust_for_each( sui_list_gavl, wlist->list, item) {
    if (sui_list_item_get_text(item, &text)>=0) {
      if ((!patlen ||
          (patlen && !wlist_nicmp(text, wlist->filter.pattern.data.text, patlen))) &&
          (!(patflg & SULF_PAT_IMGIDX) || (item->imgidx == wlist->filter.pattern.imgidx)) &&
          (!(patflg & SULF_PAT_USERIDX) || (item->index == wlist->filter.pattern.index)) &&
          (!(patflg & SULF_PAT_FLAGS) || ((item->flags & SULI_FLAG_MASK) & patflg))
         ) {
//ul_logdeb("-Filtered:(i=%d)'%s'(l=%d)\n",item->idx,sui_utf8_get_text(text),sui_utf8_length(text));
        (sortarray+i)->textlen = sui_utf8_length(text);
        (sortarray+i)->text = text;
        text = NULL;
        (sortarray+i)->index = item->idx;
        i++;
      } else
        sui_utf8_dec_refcnt(text);
    }
  }

  if ( wlist->filter.pattern.flags || patlen)
    wlist->filter.pattern.flags |= SULF_PAT_FILTER_IS_USED;

  wlist->filter.cnt = i;
  wlist->filter.items = sui_malloc(sizeof(sui_listcoord_t)*i);
  if (wlist->filter.items) {

   if ( !sui_test_flag(widget,SULF_SORTED)) {
      for(i=0;i<wlist->filter.cnt;i++) {
        *(wlist->filter.items+i)=(sortarray+i)->index;
//ul_logdeb("-NoSorted:(i=%d)\n",(sortarray+i)->index);
      }

    } else {

      int j, k;
    /* sort filtered list */
      ul_logdeb("List sorting...\n");
/* FIXME: some better algorithm than insert sort */
      for(i=0;i<wlist->filter.cnt;i++) {
        j = 0;
        while ((sortarray+j)->text==NULL) j++; /* find first unused item */
        k = j; j++;
        while(j < wlist->filter.cnt) { /* find the */
          if ((sortarray+j)->text && wlist_nicmp((sortarray+k)->text,
                       (sortarray+j)->text, (sortarray+k)->textlen)>0) {
            k = j;
          }
          j++;
        }
        /* move the best item to the array */
        *(wlist->filter.items+i)=(sortarray+k)->index;
//ul_logdeb("-Sorted:(i=%d)\n",(sortarray+k)->index);
        sui_utf8_dec_refcnt((sortarray+k)->text);
        (sortarray+k)->text = NULL;
        (sortarray+k)->textlen = 0;
      }
    }
  }

/* free temporary array */
  for (i=0;i<wlist->filter.cnt;i++) {
    if ((sortarray+i)->text) sui_utf8_dec_refcnt((sortarray+i)->text);
  }
  sui_free(sortarray);

/* repair current position */
  sui_wlistbox_repair_position(widget);

  if (wlist->filter.items) {
    long lowb = 0, highb = 0;
    highb = wlist->filter.cnt-1;
    sui_obj_emit((sui_obj_t *)widget, SUI_SIG_BOUNDS_SET, lowb, highb);
    return 0;
  }
  return -1;
}

/**
 * sui_wlistbox_find_current - find item beggining with pattern after current item
 */
int sui_wlistbox_find_current(sui_widget_t *widget, utf8 *pattern)
{
  sui_list_item_t *olditem, *item;
  utf8 *text;
  int patlen = sui_utf8_length(pattern);
  olditem = item = sui_wlistbox_get_current_item(widget);
  while (item) {
    item = sui_wlistbox_get_next(widget, item);
    if ( !item) item = sui_wlistbox_get_first(widget);
    if (item==olditem) break; /* break searching on the initial item */

    if (sui_list_item_get_text(item, &text)>=0) {
      if (!wlist_nicmp(pattern, text, patlen)) {
        sui_utf8_dec_refcnt(text);
        break;
      }
      sui_utf8_dec_refcnt(text);
    }
  }
  if ( item && item!=olditem) {
    sui_wlistbox_set_current_item(widget, item);
    return 0;
  }
  return -1;
}

/******************************************************************************
 * Listbox slot functions
 ******************************************************************************/
/**
 * sui_wlistbox_set_filter - set new filter string
 */
int sui_wlistbox_set_filter(sui_widget_t *widget, utf8 *filter)
{
  suiw_listbox_t *wlist = sui_lstwdg(widget);
  if (sui_utf8_cmp(wlist->filter.pattern.data.text, filter)) {
    sui_utf8_dec_refcnt(wlist->filter.pattern.data.text);
    if (filter)
      wlist->filter.pattern.data.text = sui_utf8_dynamic(sui_utf8_get_text(filter), sui_utf8_length(filter), -1);
    else
      wlist->filter.pattern.data.text = NULL;
    wlist->first = -1;
ul_logdeb("Set new listbox(%p) filter '%s'(l=%d)\n",widget, sui_utf8_get_text(filter),sui_utf8_length(filter));

    sui_obj_emit((sui_obj_t *)widget, SUI_SIG_FILTERCHANGED, wlist->filter.pattern.data.text);
    sui_wlistbox_update(widget, -1);

  }
  return 0;
}

/**
 * sui_wlistbox_update - update listbox (with filtering and sorting)
 */
int sui_wlistbox_update(sui_widget_t *widget, int index)
{
  ul_logdeb("Update listbox(%p) - item idx=%d\n",widget,index);
  if (sui_wlistbox_filter_and_sort(widget) < 0) return -1;
  sui_redraw_request( widget);
  return 0;
}

int sui_wlistbox_get_text( sui_widget_t *widget, sui_dinfo_t *diout)
{
  utf8 *curtext = NULL;
  sui_list_item_t *item = 0;
  int ret = -1;

  if (!widget || !diout) return 0;

/* get current position */
  item = sui_wlistbox_get_current_item(widget);
  if (item) {
    if ( sui_list_item_get_text(item, &curtext)<0) return 0;
    ret = sui_wr_utf8(diout, 0, &curtext);
    sui_utf8_dec_refcnt(curtext);
  }
  if (ret == SUI_RET_OK) return 1;
  return 0;
}

/******************************************************************************
 * Listbox basic functions
 ******************************************************************************/
/**
 * sui_wlistbox_get_current_item - return the current item (point by pos dinfo)
 */
sui_list_item_t *sui_wlistbox_get_current_item(sui_widget_t *widget)
{
  long hlp = 0;
  sui_listcoord_t cursel;
  suiw_listbox_t *wlist = sui_lstwdg(widget);

  if (sui_rd_long(wlist->pos, 0, &hlp) != SUI_RET_OK)
    return NULL;
  cursel = (sui_listcoord_t)hlp;
//ul_logdeb("-Get current item %ld\n",hlp);
//  return sui_list_gavl_find(wlist->list, &cursel);
  return sui_wlistbox_find(widget, cursel);
}

/**
 * sui_wlistbox_set_current_item - set item index as the current item (point by pos dinfo)
 */
int sui_wlistbox_set_current_item(sui_widget_t *widget, sui_list_item_t *item)
{
  long curidx;
//ul_logdeb("-Set current item %ld\n",curidx);

  curidx = sui_wlistbox_get_position(widget, item);
  sui_obj_emit((sui_obj_t *)widget, SUI_SIG_VALUE_SET, curidx);

  if (item) curidx = item->idx;
  else curidx = -1;
  return sui_wr_long(sui_lstwdg(widget)->pos, 0, &curidx);
}

/**
 * sui_wlistbox_get_item_size - get size of list item 
 */
int sui_wlistbox_get_item_size( sui_dc_t *dc, sui_list_item_t *item, sui_dcfont_t *font, 
                                sui_align_t align, sui_textdims_t *size)
{
  utf8 *txt = NULL;
  if ( !item || !font || !size) return -1;
  size->h = size->w = size->b = 0;
  if (sui_list_item_get_text(item, &txt)<0) return -1;
  sui_text_get_size(dc, font, size, -1, txt, align);
  sui_utf8_dec_refcnt( txt);
  return 0;
}

/******************************************************************************/
/**
 * sui_wlistbox_get_size - return list content size
 * @dc: pointer to device context
 * @widget: pointer to list widget
 * @point: pointer to point structure
 */
static inline
int sui_wlistbox_get_size( sui_dc_t *dc, sui_widget_t *widget, sui_point_t *size)
{
  suiw_listbox_t *wlist;
  if ( !size) return -1;
  wlist = sui_lstwdg(widget);
  size->x = wlist->cols * wlist->maxwidth;
  size->y = wlist->lines * wlist->maxheight;
  return 1;
}
  
/**
 * sui_wlistbox_init - Reaction to INIT event in list widget
 * @widget: Pointer to list widget.
 * @event: Pointer to INIT event.
 *
 * The function initializes the list widget.
 *
 * Return Value: The function does not return a value.
 * File: sui_wlistbox.c
 */
//inline
int sui_wlistbox_init( sui_dc_t *dc, sui_widget_t *widget) 
{
  suiw_listbox_t  *wlist = sui_lstwdg(widget);
  sui_coordinate_t wl, hl;
  sui_textdims_t labsize = { .w=0, .h=0, .b=0};
  int islabelinside = (widget->label && (sui_get_widget_labalign(widget) & SUAL_INSIDE)) ? 1 : 0;
  sui_list_item_t *item, *curitem;

  if (!wlist->list) {
    ul_logmsg("!!! wl_init list=NULL!\n");
    return -1;
  }
/* set default values */
  wlist->align |= SUAL_INSIDE; // Items must be inside list.
  wlist->mk_mode = SUKM_CAPITALS;

/* if SORTED flag or filter is set */
  sui_wlistbox_filter_and_sort( widget);

/* get idx of first item */
  if ( wlist->first < 0) {
    sui_listcoord_t first, pos;

    item = sui_wlistbox_get_first(widget);
    if ( item) wlist->first = item->idx;

    curitem = sui_wlistbox_get_current_item(widget);
    if ( curitem) {
      first = sui_wlistbox_get_position(widget, item);
      pos = sui_wlistbox_get_position(widget, curitem);
      if ( pos>=0) {
        sui_listcoord_t cnt = wlist->lines * wlist->cols;
        if ( pos < first) {
          wlist->first = curitem->idx;
        } else if ( pos >= first+cnt) {
          cnt = pos - (first+cnt-1);
          while(cnt--) {
            item = sui_wlistbox_get_next(widget,item);
            if (!item) break; /* ERROR !!! current item is somewhere in algorithm - impossible ... */
          }
          wlist->first = item->idx;
        }
      } else
        sui_wlistbox_set_current_item(widget, item);
    } else // current item isn't in list */
      sui_wlistbox_set_current_item(widget, item);
  }
/* computing of width and height of widget content */
  /* label width and height */
  hl = widget->style->gap;
  if ( islabelinside) {
    sui_widget_get_label_size( dc, widget, &labsize);
    labsize.w += hl;
    labsize.h += hl;

    if ( sui_get_widget_labalign(widget) & (SUAL_RIGHT | SUAL_LEFT))
      labsize.h = 0;
    else
      labsize.w = 0;
  }
  /* add space and frame size */
  labsize.w += 2*hl;
  labsize.h += 2*hl;
  if ( sui_test_flag( widget, SULF_LISTFRAME)) {
    labsize.w += 2;
    labsize.h += 2; /* add listframe */
  }
  wl = hl = 0;
/* item width and height */
  {
    sui_textdims_t itemsize;
    int withicon = (sui_test_flag( widget, SULF_WITH_ICON) && (wlist->imglib != NULL));
  
    sui_dcfont_t *pdcfont = sui_font_prepare( dc, widget->style->fonti->font,
                                              widget->style->fonti->size);
  
    gavl_cust_for_each( sui_list_gavl, wlist->list, item) {
      if ( sui_wlistbox_get_item_size( dc, item, pdcfont, wlist->align, &itemsize))
        break;
      if (withicon) {
        itemsize.w += wlist->imglib->width+2;
        if (itemsize.h < wlist->imglib->height) itemsize.h = wlist->imglib->height;
      }
      if ( itemsize.w > wl) wl = itemsize.w;
      if ( itemsize.h > hl) hl = itemsize.h;
    }
  }
/* horizontal dimesions */
  if ( widget->place.w < 0) {
    if ( wlist->cols <= 0)
      wlist->cols = 1;
    widget->place.w = (wl*wlist->cols)+labsize.w;
  } else {
    if ( wlist->cols <= 0)
      wlist->cols = (widget->place.w-labsize.w)/wl;
    else
      wl = (widget->place.w-labsize.w)/wlist->cols;
  }
  wlist->maxwidth = wl;
/* vertical dimensions */
  if ( widget->place.h < 0) {
    if ( wlist->lines <= 0)
      wlist->lines = 1;
    widget->place.h = (hl*wlist->lines)+labsize.h;
  } else {
    if ( wlist->lines <= 0)
      wlist->lines = (widget->place.h-labsize.h)/hl;
    else
      hl = (widget->place.h-labsize.h)/wlist->lines;
  }
  wlist->maxheight = hl;
  return 1;
}

/**
 * sui_wlistbox_draw - draw listbox widget content
 * @dc: Pointer to device context.
 * @widget: Pointer to list widget.
 * @event: Pointer to the event.
 *
 * The function draws list widget.
 *
 * Return Value: The function does not return a value.
 * File: sui_list.c
 */
//inline
int sui_wlistbox_draw( sui_dc_t *dc, sui_widget_t *widget)
{
  sui_dcfont_t *pdcfont;
  suiw_listbox_t  *wlist = sui_lstwdg(widget);
  sui_list_item_t *item, *curitem;

  sui_dc_t wdg_dc;
  sui_event_t wdg_event;

  sui_point_t box = { .x=widget->place.w, .y=widget->place.h};
  sui_point_t origin;
  sui_point_t size;
  sui_point_t itemmax = { .x = wlist->maxwidth, .y = wlist->maxheight};
  sui_point_t newmax = { .x = 0, .y = 0};
  sui_textdims_t textsize;
  sui_coordinate_t xp, yp;
  sui_point_t itemorigin;
  sui_coordinate_t hlpx, hlpy;
  utf8 *txt;

  unsigned char selected, framed;
  int withicon = (sui_test_flag( widget, SULF_WITH_ICON) && (wlist->imglib != NULL));

/* widget content aligning */
  sui_align_t al = sui_widget_get_item_align(widget);
    
/* dc and event for widget items */
  wdg_dc.psd = dc->psd;
  wdg_event.what = SUEV_DRAW;
  wdg_event.draw.dc = &wdg_dc;

/* get size of visible list */
  sui_wlistbox_get_size(dc, widget, &size);
  sui_align_object(&box, &origin, &size, 0, widget->style, al);
  pdcfont=sui_font_prepare(dc, widget->style->fonti->font, widget->style->fonti->size);

/* select colors */
  if (sui_test_flag( widget, SULF_LISTFRAME)) {
    sui_set_widget_fgcolor(dc, widget, SUC_FRAME); // SUC_ITEM
    sui_set_widget_bgcolor(dc, widget, SUC_BACKGROUND);
    sui_draw_rect(dc, origin.x-1, origin.y-1, size.x+2, size.y+2, 1);
  }

/* get first item */
  item = sui_wlistbox_get_first(widget);
/* get current position */
  curitem = sui_wlistbox_get_current_item(widget);
  if (!curitem) curitem = item;

/* if there isn't any items - end of drawing */
  if (!item) {
    if (sui_wlistbox_ptr_empty_text) {
      sui_text_get_size(dc, pdcfont, &textsize, -1, sui_wlistbox_ptr_empty_text,
                        SUAL_CENTER | SUAL_INSIDE);
      size.x=textsize.w;
      size.y=textsize.h;
      sui_align_object(&box, &itemorigin, &size, textsize.b, NULL,
                      SUAL_CENTER | SUAL_INSIDE);
  
      sui_set_fgcolor(dc, widget, SUC_ITEM);
      sui_set_bgcolor(dc, widget, SUC_BACKGROUND);
  
      sui_draw_text(dc,itemorigin.x,itemorigin.y,-1,sui_wlistbox_ptr_empty_text,
                    SUAL_CENTER | SUAL_INSIDE);
    }
    goto wlistbox_end_of_drawing;
  }

/* get first visible item from list */
  item = sui_wlistbox_find(widget, wlist->first); // sui_list_gavl_find(wlist->list, &wlist->first);
  if (!item) {    /* item was deleted ??? - get item with idx greater than wlist->first */
    item = sui_wlistbox_get_first(widget);
    wlist->first = item->idx;
  }
  
/* first item correction */
  if ( curitem) {
    sui_listcoord_t first, pos;
    first = sui_wlistbox_get_position(widget, item);
    pos = sui_wlistbox_get_position(widget, curitem);
    if ( pos>=0) {
      sui_listcoord_t cnt = wlist->lines * wlist->cols;
      if ( pos < first) {
        wlist->first = curitem->idx;
        item = curitem;
      } else if ( pos >= first+cnt) {
        cnt = pos - (first+cnt-1);
        while(cnt--) {
          item = sui_wlistbox_get_next(widget,item);
          if (!item) break; /* ERROR !!! current item is somewhere in algorithm - impossible ... */
        }
        wlist->first = item->idx;
      }
    }
  }
/* now: item is pointer to the first visible list item, curitem is pointer to the current list item */
    
/* initialization of item drawings */   
  xp = origin.x; hlpx = wlist->cols;
  yp = origin.y; hlpy = wlist->lines;
  size.x += itemmax.x;
  size.y += itemmax.y;
    
  sui_font_set( dc, pdcfont);
/* for each visible item (in 'item' must be first visible item) */
  while (item) {
  /* get item size */
    if (sui_wlistbox_get_item_size(dc, item, pdcfont, wlist->align, &textsize))
      break;

    if (withicon) {
      textsize.w += wlist->imglib->width+2;
      if (textsize.h < wlist->imglib->height) textsize.h = wlist->imglib->height;
    }

  /* correct dimensions in autoresize mode */
    if (textsize.w > newmax.x) newmax.x = textsize.w;
    if (textsize.h > newmax.y) newmax.y = textsize.h;
    
  /* set clipping area */
    {
      sui_rect_t itembox = {.x=xp, .y=yp, .w=itemmax.x, .h=itemmax.y};
      sui_gdi_set_carea(dc, &itembox);
    }
    {
      sui_point_t isize = {.x=textsize.w, .y=textsize.h};
      sui_align_object(&itemmax, &itemorigin, &isize, textsize.b, NULL, wlist->align);
    }

  /* determine state of item selection and framing */
    selected = 0; framed = 0;
    switch(widget->flags & SULF_TYPE_MASK) {
      case SULF_NOSELECT:
        if ((item->idx == curitem->idx) && sui_test_flag(widget, SUFL_FOCUSED))
          framed = 1;
        break;
      case SULF_SELECT:
        if ((item->idx == curitem->idx) && sui_test_flag(widget, SUFL_FOCUSED))
          selected = 1;
        break;
      case SULF_SINGLE:
        if (item->idx == curitem->idx) {
          selected = 1;
          if (sui_test_flag(widget, SUFL_FOCUSED))
            framed = 1;
        }
        break;
      case SULF_SINGLE_FRAME:
        if (item->idx == curitem->idx) {
          if (sui_test_flag(widget, SUFL_FOCUSED))
            selected = 1;
          else
            framed = 1;
        }
        break;
      case SULF_MULTI:
        if (item->flags & SULI_SELECTED)
          selected = 1;
        if ((item->idx == curitem->idx) && sui_test_flag(widget, SUFL_FOCUSED))
          framed = 1;
        break;
    }

  /* !!! change to reading from select dinfo for multiselect */
    if (((item->idx == curitem->idx) &&
        (sui_is_flag(widget, SULF_TYPE_MASK, SULF_SINGLE) ||
        (sui_test_flag(widget, SUFL_FOCUSED) &&
        (sui_is_flag(widget, SULF_TYPE_MASK, SULF_SELECT) ||
          sui_is_flag(widget, SULF_TYPE_MASK, SULF_SINGLE_FRAME))))) ||
        ((item->flags & SULI_SELECTED) &&
          sui_is_flag(widget, SULF_TYPE_MASK, SULF_MULTI))) {
      selected = 1;
    } else {
      selected = 0;
    }
  /* draw background */
    sui_set_fgcolor(dc, widget, SUC_BACKGROUND+((selected)?SUC_SELECTED:0));    /* bg color of line */
    sui_draw_rect(dc, xp, yp, itemmax.x, itemmax.y, 0);    /* draw item background */

    sui_set_fgcolor(dc, widget, SUC_ITEM+((selected)?SUC_SELECTED:0));
    sui_set_bgcolor(dc, widget, SUC_BACKGROUND+((selected)?SUC_SELECTED:0));

  /* leave empty space */
    if ((wlist->align & SUAL_HORMASK) == SUAL_LEFT) {
      itemorigin.x += wlist->gap;
    } else if ((wlist->align & SUAL_HORMASK) == SUAL_RIGHT) {
      itemorigin.x -= wlist->gap;
    }
  /* draw icon if we need */
    itemorigin.x += xp;
    itemorigin.y += yp;
    if (withicon) {
      int imgidx = item->imgidx;
      int save_y = itemorigin.y;
      if (imgidx >= wlist->imglib->count) {
        imgidx = 0;
      }
      itemorigin.y += (textsize.h - wlist->imglib->height)/2;
      sui_gdi_draw_image(dc, &itemorigin, wlist->imglib, NULL, imgidx, SUIO_MULTIIMAGE);
      itemorigin.y = save_y;
      itemorigin.x += wlist->imglib->width+2;
    }

  /* draw item */
    if (sui_list_item_get_text(item, &txt)>=0) {
      sui_draw_text(dc, (sui_coordinate_t)itemorigin.x,
                    (sui_coordinate_t)itemorigin.y, -1, txt, wlist->align);
      sui_utf8_dec_refcnt( txt);
    }
  /* mark current position - current focuset item in single,multi,noselect mode */
    if (framed) {
      sui_draw_rect(dc, xp+1, yp+1, itemmax.x-2, itemmax.y-2, 1);
    }

  /* update position for next item and check space for next item */
    if (hlpy) {
      yp += itemmax.y;
      hlpy--;
    }
    if (!hlpy) {
      yp = origin.y;
      hlpy = wlist->lines;
      xp += itemmax.x;
      hlpx--;
    }
    if ((xp >= size.x) || (yp >= size.y) || !hlpx || !hlpy)
      break;
  /* get next visible item */
    item = sui_wlistbox_get_next(widget,item);
  }

/* save size for autoresized widget */
  if (sui_test_flag(widget, SUFL_AUTORESIZE) && newmax.x && newmax.y) {
    wlist->maxwidth = newmax.x;
    wlist->maxheight = newmax.y;
  }

wlistbox_end_of_drawing:
  return 1;
}

/**
 * sui_wlistbox_hkey - Reaction to KEY_DOWN, KEY_UP event in list widget
 * @widget: Pointer to list widget.
 * @event: Pointer to the event.
 *
 * The function is response to keyboard event for list widget.
 *
 * Return Value: The function returns 0 if it takes to event.
 * File: sui_list.c
 */
//inline
int sui_wlistbox_hkey( sui_widget_t *widget, sui_event_t *event)
{
  int key = event->keydown.keycode;
  suiw_listbox_t *wlist = sui_lstwdg(widget);
  int add = 0, ret, pg = wlist->lines * wlist->cols;
  sui_listcoord_t now = 0;
  sui_list_item_t *item, *newitem;

/* repair current position */
  if (sui_test_flag(widget, SUFL_EDITEN))
    sui_wlistbox_repair_position(widget);

  if ( sui_test_flag( widget, SULF_REVERSEKEYS)) {
    switch( key) {
      case MWKEY_PAGEUP:   key = MWKEY_PAGEDOWN; break;
      case MWKEY_PAGEDOWN: key = MWKEY_PAGEUP; break;
      case MWKEY_UP:       key = MWKEY_DOWN; break;
      case MWKEY_DOWN:     key = MWKEY_UP; break;
      case MWKEY_LEFT:     key = MWKEY_RIGHT; break;
      case MWKEY_RIGHT:    key = MWKEY_LEFT; break;
      case MWKEY_HOME:     key = MWKEY_END; break;
      case MWKEY_END:      key = MWKEY_HOME; break;
    }
  }

/* move in list with arrows */
  ret = 1;
  switch( key) {
    case MWKEY_PAGEUP:
      add = -pg; break;
    case MWKEY_LEFT:
      add = -wlist->lines; break;
    case MWKEY_UP:
      add = -1; break;
    case MWKEY_PAGEDOWN:
      add = pg; break;
    case MWKEY_RIGHT:
      add = wlist->lines; break;
    case MWKEY_DOWN:
      add = 1; break;
  /*
  case MWKEY_HOME:
          now = 0; break;
  case MWKEY_END:
          now = wlist->list->rows-1; break;
  */
    default:
      ret = 0; break;
  }
/* one of relative move keys */
  if ( ret) {
    if ( sui_test_flag( widget, SUFL_EDITEN) ||
         sui_is_flag( widget, SULF_TYPE_MASK, SULF_NOSELECT)) {
      long newpos;
      sui_list_item_t *last;
/* get current item */
      newitem = item = sui_wlistbox_get_current_item(widget);
      if (!item) {
        if ( add < 0)
          newitem = item = sui_wlistbox_get_last(widget);
        else
          newitem = item = sui_wlistbox_get_first(widget);
      }
      if (!item) {
        if ( sui_test_flag(widget, SULF_CHANGE_FOCUS)) return -1; /* no key processing */
        else return 1;    /* key processed */
      }

    /* find new item */
      last = newitem;
      while (add && newitem) {
        if (add>0) {
          newitem = sui_wlistbox_get_next(widget, newitem);
          if (!newitem) {
            if (sui_test_flag( widget, SULF_LOOP))
              newitem = sui_wlistbox_get_first(widget);
            else {
              ret = 2;
              newitem = last;
              break;
            }
          }
          add--;
        } else {
          newitem = sui_wlistbox_get_prev(widget, newitem);
          if (!newitem) {
            if (sui_test_flag( widget, SULF_LOOP))
              newitem = sui_wlistbox_get_last(widget);
            else {
              ret = 1;
              newitem = last;
              break;
            }
          }
          add++;
        }
        last = newitem;
      }

      if ( !newitem) return -1;

      newpos = newitem->idx;
      if ( item != newitem) {
        if (sui_wlistbox_set_current_item(widget, newitem)!=SUI_RET_OK) return -1;
        if ( sui_is_flag( widget, SULF_TYPE_MASK, SULF_SINGLE) ||
             sui_is_flag( widget, SULF_TYPE_MASK, SULF_SINGLE_FRAME)) {
          item->flags &= ~SULI_SELECTED;
          newitem->flags |= SULI_SELECTED;
          sui_put_command_event( SUCM_CHANGED, widget, newpos);
        } else {
          sui_put_command_event( SUCM_CHANGE, widget, newpos);
        }
      } else {    /* bounaries */
        if ( ret == 1) { /* lower boundary */
          if ( sui_test_flag( widget, SULF_CHANGE_FOCUS)) {
            sui_put_command_event( SUCM_PREV, NULL, 0);
          } else {
            sui_put_command_event( SUCM_MOVEMIN, widget, newpos);
          }
        } else if ( ret == 2) { /* upper boundary */
          if ( sui_test_flag( widget, SULF_CHANGE_FOCUS)) {
            sui_put_command_event( SUCM_NEXT, NULL, 0);
          } else {
            sui_put_command_event( SUCM_MOVEMAX, widget, newpos);
          }
        }
        return 0;
      }
      now = newpos; /* for common checking */
/* check visibility of the current item */
//      sui_wlistbox_check_visible_item( widget, now, newitem);
      return 1;
    }
  } else if ((key == ' ') || (key == MWKEY_ENTER)) { /* select item - space, enter */
    if (sui_test_flag(widget, SUFL_EDITEN)) {
/* get current item */
      newitem = item = sui_wlistbox_get_current_item(widget);
      if (!item) {
        if ( sui_test_flag(widget, SULF_CHANGE_FOCUS)) return -1; /* no key processing */
        else return 1;    /* key processed */
      }

      if (sui_is_flag(widget, SULF_TYPE_MASK, SULF_MULTI)) {    /* change select */
        item->flags ^= SULI_SELECTED;
        sui_put_command_event(SUCM_CHANGED, widget, now);  /* select in the dinfo array */
      } else {    /* on noselect and singleselect list invoke SUCM_CONFIRM event */
        sui_put_command_event(SUCM_CONFIRM, widget, now);
/* emit confirm signal */
        {
          utf8 *text = NULL;
          if (sui_list_item_get_text(item, &text) >= 0) {
            sui_obj_emit((sui_obj_t *)widget, SUI_SIG_CONFIRMED, text);
            sui_utf8_dec_refcnt(text);
          }
        }
      }
      return 1;
    }
    /* quick search - first letter */
  } else if (key==MWKEY_BACKSPACE || key==MWKEY_DELETE) {
    if (sui_test_flag(widget, SULF_FILTERING)) {
      if (wlist->filter.pattern.data.text) {
        if ( sui_utf8_length(wlist->filter.pattern.data.text)-1) {
          utf8 *patnew = sui_utf8_dynamic( NULL, sui_utf8_length(wlist->filter.pattern.data.text)-1, -1);
          sui_utf8_ncpy(patnew, wlist->filter.pattern.data.text, sui_utf8_length(wlist->filter.pattern.data.text)-1);
          sui_wlistbox_set_filter(widget, patnew); /* clear only last character from the filter pattern */
          sui_utf8_dec_refcnt(patnew);
        } else
          sui_wlistbox_set_filter(widget, NULL); /* clear only last character from the filter pattern */
        return 1;
      }
    } else {
      sui_list_item_t *item = sui_wlistbox_get_first(widget);
      if (item) {
        sui_wlistbox_set_current_item(widget, item);
        return 1;
      }
    }
  } else if ((key>=' ')&&(key<0xd800)) { /* the first visible character */
    char keytext[] = {0,0,0};
    int mobkeyret = 0;
/* FIXME: repair the following code for simultaneously receiving character from normal and mobil keyboard ... */
    if (sui_test_flag(widget, SULF_MOBILKBD)) {
      utf8char *tab;
      mobkeyret = sui_mk_translate_key2char(event->keydown.keycode, &wlist->mk_mode, &tab);
ul_logdeb("WLIST-K2MK: rc=%d,ch=%p mode=0x%04X\n", mobkeyret, tab, wlist->mk_mode);
      if (mobkeyret & SUI_MK_RET_CHAR) {
        memcpy( keytext, tab, sui_utf8_count_bytes(tab, 1));
      }
    } else {
      keytext[0] = key;
      mobkeyret |= SUI_MK_RET_CHAR;
    }
    
    if (sui_test_flag(widget, SULF_FILTERING)) {
      if ( wlist->filter.pattern.data.text) {
        utf8 *patnew = sui_utf8_dynamic( NULL, sui_utf8_length(wlist->filter.pattern.data.text)+1, -1);
        int len = sui_utf8_length(wlist->filter.pattern.data.text);
        if (mobkeyret & SUI_MK_RET_REMOVE) len--;
        if ( len) sui_utf8_ncpy(patnew, wlist->filter.pattern.data.text, len);
        if (mobkeyret & SUI_MK_RET_CHAR) sui_utf8_add_char(patnew,keytext);
        sui_wlistbox_set_filter(widget, patnew); /* clear only last character from the filter pattern */
        sui_utf8_dec_refcnt(patnew);
      } else {
        if (mobkeyret & SUI_MK_RET_CHAR)
          sui_wlistbox_set_filter(widget, U8(keytext)); /* clear only last character from the filter pattern */
      }
      return 1;
    } else {
      if (mobkeyret & SUI_MK_RET_CHAR) {
        if (!sui_wlistbox_find_current(widget, U8(keytext))) {
  //        sui_obj_emit( (sui_obj_t *)widget, SUI_SIG_KEYPRESSED, keytext);
          return 1;
        }
      }
    }
  }
  return 0;
}


/******************************************************************************
 * Listbox event handler
 ******************************************************************************/
/**
 * sui_wlistbox_hevent - Widget event handler for list widget
 * @widget: Pointer to the list widget.
 * @event: Pointer to the event.
 *
 * The function is response to all events for the list widget.
 * Return Value: The function does not return a value.
 * File: sui_list.c
 */
void sui_wlistbox_hevent( sui_widget_t *widget, sui_event_t *event)
{
  switch(event->what) {
    case SUEV_DRAW:
    case SUEV_REDRAW:
      sui_widget_draw(event->draw.dc, widget, sui_wlistbox_draw);
      return;
    case SUEV_KDOWN:
      if (sui_wlistbox_hkey(widget, event)>0) {
        sui_clear_event(widget, event);
        sui_redraw_request(widget);
      }
      break;
    case SUEV_COMMAND:
      switch(event->message.command) {
        case SUCM_FOCUSSET:
          if (sui_test_flag(widget, SULF_CHANGE_FOCUS)) {
//        ul_logmsg("wlb-FOCUS:event.msg.info=0x%X\n",event->message.info);
            sui_list_item_t *item = NULL;

            if ( event->message.info == 1) {
              item = sui_wlistbox_get_first(widget);
              sui_wlistbox_set_current_item(widget, item);
            } else if ( event->message.info == -1) {
              item = sui_wlistbox_get_last(widget);
              sui_wlistbox_set_current_item(widget, item);
            }
          }
          if (sui_test_flag( widget, SULF_MOBILKBD))
            sui_mk_clear_state();
          break;
        case SUCM_INIT:
          sui_widget_hevent(widget, event);
          sui_wlistbox_init((sui_dc_t *)event->message.ptr, widget);
          sui_clear_event(widget, event);
          break;
        case SUCM_GETSIZE:
          if (sui_wlistbox_get_size((sui_dc_t *)event->message.ptr,
               widget, (sui_point_t *)event->message.info)>0) {
            sui_clear_event(widget, event);
          }
          break;
      }
  }
  if (event->what != SUEV_NOTHING)
    sui_widget_hevent(widget,event);
}


/******************************************************************************
 * Listbox widget vmt & signal-slot mechanism
 ******************************************************************************/
int sui_wlistbox_vmt_init( sui_widget_t *pw, void *params)
{
  pw->data = sui_malloc( sizeof( suiw_listbox_t));
  if(!pw->data)
          return -1;

  ((suiw_listbox_t *)pw->data)->first = -1;

  pw->type = SUWT_LISTBOX;
  pw->hevent = sui_wlistbox_hevent;

  return 0;
}

void sui_wlistbox_vmt_done( sui_widget_t *pw)
{
  if ( pw->data) {
    suiw_listbox_t *wlist = pw->data;
    if ( wlist->list) sui_list_dec_refcnt( wlist->list);
    wlist->list = NULL;

    if (wlist->filter.pattern.data.text) sui_utf8_dec_refcnt( wlist->filter.pattern.data.text);
    wlist->filter.pattern.data.text = NULL;
    if (wlist->filter.items) free(wlist->filter.items);
    wlist->filter.items = NULL;
    wlist->filter.cnt = 0;
//    wlist->filter.max = 0;

    if ( wlist->pos) sui_dinfo_dec_refcnt( wlist->pos);
    wlist->pos = NULL;
    if ( wlist->selected) sui_dinfo_dec_refcnt( wlist->selected);
    wlist->selected = NULL;
    if ( wlist->imglib) sui_image_dec_refcnt( wlist->imglib);
    wlist->imglib = NULL;
    sui_free( wlist);
    pw->data = NULL;
  }
  pw->type = SUWT_GENERIC;
  pw->hevent = sui_widget_hevent; // NULL;
}

 
SUI_CANBE_CONST
sui_obj_signal_tinfo_t sui_wlistbox_signal_tinfo[] = {
  { "confirmed", SUI_SIG_CONFIRMED, &sui_args_tinfo_utf8},
//  { "keypressed", SUI_SIG_KEYPRESSED, &sui_args_tinfo_utf8},
  { "filterchanged", SUI_SIG_FILTERCHANGED, &sui_args_tinfo_utf8},
  { "boundchanged", SUI_SIG_BOUNDS_SET, &sui_args_tinfo_long2},
  { "indexchanged", SUI_SIG_VALUE_SET, &sui_args_tinfo_long},
};

SUI_CANBE_CONST
sui_obj_slot_tinfo_t sui_wlistbox_slot_tinfo[] = { 
  { "update", SUI_SLOT_UPDATE, &sui_args_tinfo_int,
    .target_kind = SUI_STGT_METHOD_OFFSET,
    .target.method_offset=UL_OFFSETOF( sui_wlistbox_vmt_t, update)},
  { "setfilter", SUI_SLOT_SETFILTER, &sui_args_tinfo_utf8,
    .target_kind = SUI_STGT_METHOD_OFFSET,
    .target.method_offset=UL_OFFSETOF( sui_wlistbox_vmt_t, setfilter)},
};

SUI_CANBE_CONST
sui_obj_tinfo_t sui_wlistbox_vmt_obj_tinfo = {
  .name = "sui_wlistbox",
  .obj_size = sizeof(sui_widget_t),
  .obj_align = UL_ALIGNOF(sui_widget_t),
  .signal_count = MY_ARRAY_SIZE(sui_wlistbox_signal_tinfo),
  .signal_tinfo = UL_CAST_UNQ1(sui_obj_signal_tinfo_t *, sui_wlistbox_signal_tinfo),
  .slot_count = MY_ARRAY_SIZE(sui_wlistbox_slot_tinfo),
  .slot_tinfo = UL_CAST_UNQ1(sui_obj_slot_tinfo_t *, sui_wlistbox_slot_tinfo)
};

sui_wlistbox_vmt_t sui_wlistbox_vmt_data = {
  .vmt_size = sizeof(sui_wlistbox_vmt_t),
  .vmt_refcnt = SUI_STATIC,
  .vmt_parent = &sui_widget_vmt_data,
  .vmt_init = sui_wlistbox_vmt_init,
  .vmt_done = sui_wlistbox_vmt_done,
  .vmt_obj_tinfo = UL_CAST_UNQ1(sui_obj_tinfo_t *, &sui_wlistbox_vmt_obj_tinfo),

  .update = sui_wlistbox_update,
  .setfilter = sui_wlistbox_set_filter,

};

/******************************************************************************/

/**
 * sui_wlistbox - Create listbox widget
 * @aplace: List widget position and size.
 * @aname: List label in utf8 string.
 * @astyle: List widget style.
 * @astb: Status bar number. !!! MUST BE CHANGE !!!
 * @aflags: Widget common and list special flags.
 * @alist: Pointer to list structure.
 * @apos: Pointer to dinfo with current position in list
 * @asarr: Pointer to dinfo with selection array of list items.
 * @alines: Count of visible lines of listbox.
 * @acols: Count of visible columns of listbox.
 * @aalign: Alignment of all items.
 * @agap: Width of space between frame and items.
 *
 * The function creates the list widget.
 *
 * Return Value: The function returns pointer to created list widget.
 * File: sui_list.c
 */
sui_widget_t *sui_wlistbox( sui_rect_t *aplace, utf8 *alabel, sui_style_t *astyle, 
              sui_flags_t aflags, struct sui_list *alist, sui_dinfo_t *apos, 
              sui_dinfo_t *asarr, sui_listcoord_t alines, sui_listcoord_t acols,
              sui_align_t aalign, sui_coordinate_t axgap)
{
  sui_widget_t *pw;

  pw = sui_widget_create_new( &sui_wlistbox_vmt_data, aplace, alabel, astyle, aflags);

  if ( !pw) return NULL;

  {
    suiw_listbox_t *wlist = sui_lstwdg(pw);

    wlist->align = aalign;
    wlist->gap = axgap;
    wlist->first = -1;
    wlist->lines = alines;
    wlist->cols = acols;
    wlist->mk_mode = SUKM_CAPITALS;
    if ( alist) {
      sui_list_inc_refcnt( alist);
      wlist->list = alist;
    }
    if ( apos) {
      sui_dinfo_inc_refcnt( apos);
      wlist->pos = apos;
    }
    if ( asarr) {
      sui_dinfo_inc_refcnt( asarr);
      wlist->selected = asarr;
    }
  }
  return pw;
}


/**
 * sui_wlistbox_set_empty_list_string - set a new common empty_list string
 * @newstring: pointer to new string
 */
void sui_wlistbox_set_empty_list_string( utf8 *newstring)
{
  if ( sui_wlistbox_ptr_empty_text) {
    sui_utf8_dec_refcnt( sui_wlistbox_ptr_empty_text);
    sui_wlistbox_ptr_empty_text = NULL;
  }
  if ( newstring) {
    sui_utf8_inc_refcnt( newstring);
    sui_wlistbox_ptr_empty_text = newstring;
  } else {
    sui_wlistbox_ptr_empty_text = sui_wlistbox_common_empty_text;
  }
}
