/* sui_wgraph.c
 *
 * SUITK graph widgets.
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2008 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#include "sui_wgraph.h"

#include <ul_log.h>
UL_LOG_CUST(ulogd_suitk)


/******************************************************************************
 * Common functions for working with graph data sets
 ******************************************************************************/

/**
 * sui_graph_dset_create - create dynamic graph data-set
 * @aflags: graph data set flags
 * @adata: pointer to DINFO with graph data
 * @alabel: pointer to data set label
 * @aalign: data set label alignment
 */
sui_graph_dataset_t *sui_graph_dset_create(sui_flags_t aflags, sui_dinfo_t *adata,
                                          sui_finfo_t *aformat, utf8 *alabel,
                                          sui_font_info_t *afont, sui_align_t aalign)
{
  sui_graph_dataset_t *ret = sui_malloc(sizeof(sui_graph_dataset_t));
  if (ret) {
    ret->flags = aflags;
    sui_dinfo_inc_refcnt(adata);
    ret->data = adata;
    sui_utf8_inc_refcnt(alabel);
    ret->label = alabel;
    sui_finfo_inc_refcnt(aformat);
    ret->format = aformat;
    sui_fonti_inc_refcnt(afont);
    ret->font = afont;
    ret->align = aalign;
  }
  return ret;
}

/**
 * sui_graph_dset_copy - copy values from one data-set to another one
 * @to: pointer to destination data-set structure
 * @from: pointer to source data-set structure
 * Return Value: The function returns zero if success.
 */
int sui_graph_dset_copy(sui_graph_dataset_t *to, sui_graph_dataset_t *from)
{
  if (!to || !from) return -1;
  memcpy(to, from, sizeof(sui_graph_dataset_t));
  sui_utf8_inc_refcnt(to->label);
  sui_dinfo_inc_refcnt(to->data);
  sui_finfo_inc_refcnt(to->format);
  sui_fonti_inc_refcnt(to->font);
  return 0;
}

/**
 * sui_graph_dset_done
 * @dset: pointer to graph data-set
 */
void sui_graph_dset_done(sui_graph_dataset_t *dset)
{
  if (!dset) return;
  if (dset->data)
    sui_dinfo_dec_refcnt(dset->data);
  if (dset->label)
    sui_utf8_dec_refcnt(dset->label);
  if (dset->format)
    sui_finfo_dec_refcnt(dset->format);
  if (dset->font)
    sui_fonti_dec_refcnt(dset->font);
  memset(dset, 0, sizeof(sui_graph_dataset_t));
  return;
}

/**
 * sui_graph_dset_destroy
 * @dset: pointer to graph data-set
 */
int sui_graph_dset_destroy(sui_graph_dataset_t *dset)
{
  if (!dset) return -1;
  sui_graph_dset_done(dset);
  free(dset);
  return 0;
}

/******************************************************************************/
/**
 * sui_graph_data_inc_refcnt - increments reference counter of a graph data structure
 * @gd: pointer to a graph data structure
 */
sui_refcnt_t sui_graph_data_inc_refcnt(sui_graph_data_t *gd)
{
  if (!gd) return SUI_REFCNTERR;
  if(gd->refcnt>=0) gd->refcnt++;
  return gd->refcnt;
}

/**
 * sui_graph_data_dec_refcnt - decrements reference counter of a graph data structure
 * @gd: pointer to a graph data structure
 */
sui_refcnt_t sui_graph_data_dec_refcnt(sui_graph_data_t *gd)
{
  sui_refcnt_t ref;
  if (!gd) return SUI_REFCNTERR;
  if(gd->refcnt>0) gd->refcnt--;
  if(!(ref=gd->refcnt)) {
    sui_graph_data_clear(gd);
    sui_obj_done_vmt((sui_obj_t *)gd, UL_CAST_UNQ1(sui_obj_vmt_t *, &sui_gdata_vmt_data));
    free(gd);
  }
  return ref;
}

/**
 * sui_graph_data_create - allocates dynamical graph data structure
 */
sui_graph_data_t *sui_graph_data_create(void)
{
  sui_graph_data_t *gd = sui_malloc(sizeof(sui_graph_data_t));
  if (gd) {
    sui_graph_data_inc_refcnt(gd);
    // init fields
  }
  return gd;
}

/**
 * sui_graph_data_clear
 */
void sui_graph_data_clear(sui_graph_data_t *gd)
{
  sui_graph_dataset_t *gs;
  int cnt;
  if (!gd) return;
  if (gd->dset) {
    gs = gd->dset;
    cnt = gd->numsets;
    while (cnt) {
      sui_graph_dset_done(gs);
      gs++;
      cnt--;
    }
    gd->capsets = 0;
    free(gd->dset);
    gd->dset = NULL;
  }
  gd->numsets = 0;
}

/**
 * sui_graph_data_add_dset - add one data-set into array of data-sets
 * @gd: poitner to graph data where a new data-set will be added to
 * @dset: pointer to a source of added data-set structure
 * Return Value: The function returns zero if success.
 */
int sui_graph_data_add_dset(sui_graph_data_t *gd, sui_graph_dataset_t *dset)
{
  sui_graph_dataset_t *ngs;
  if (!gd || !dset) return -1;
  ngs = sui_add_item_to_array((void **)((void *)&(gd->dset)), sizeof(sui_graph_dataset_t), &gd->numsets, &gd->capsets);
  if (!ngs) return -1;
  return sui_graph_dset_copy(ngs, dset);
}

/**
 * sui_graphxy_data_del_line - remove last data-set from graph data's array
 * @gd: poitner to graph data
 * Return Value: The function returns zero if success.
 */
int sui_graph_data_del_dset(sui_graph_data_t *gd)
{
  sui_graph_dataset_t *gs;
  if (!gd || !gd->dset) return -1;
  if (gd->numsets<=0) return 1;
  gs = gd->dset + (gd->numsets-1);
  sui_graph_dset_done(gs);
  gd->numsets--;
  return 0;
}

/******************************************************************************/
/**
 * sui_graph_get_value
 */
static inline
int sui_graph_get_value(sui_graph_dataset_t *ds, int idx, long *cval, long *tval)
{
  long v;
  if (sui_rd_long(ds->data, idx, &v)!= SUI_RET_OK) {
    ul_logerr("GR-get_val: read data failed\n");
    return -1;
  }
  switch (ds->flags & SUGDS_TYPE_MASK) {
    case SUGDS_TYPE_ABS:
      *cval = v; /* tval is not used */
      break;
    case SUGDS_TYPE_INC:
      *cval = *tval;  /* tval is next value from the previous step */
      *tval = *tval + v;
      break;
    case SUGDS_TYPE_DIF:
      *cval = v - *tval; /* tval is the previous value */
      *tval = v;
      break;
    default:
      return -1;
  }
  return 0;
}

/**
 * sui_graph_get_minmax - find minimum and maximum in data-set and return them
 * @ds: pointer to a data-set
 * @cntdss: number of ds to end of data-set array
 * @fromidx: the first index of window where min/max will be searched or -1 for searching from beginning
 * @toidx: the last index of window where min/max will be searched or -1 searching up to end
 * @min: pointer to an output buffer for minimum
 * @max: pointer to an output buffer for maximum
 */
int sui_graph_get_minmax(sui_graph_dataset_t *ds, int cntdss, int fromidx, int toidx, long *min, long *max)
{
  long minval = 0, maxval = 0, val = 0, curval;
  int idx = 0;
  sui_graph_dataset_t *dsn = NULL;

  if (!ds || !ds->data) return -1;

  if (fromidx<0) fromidx = 0;
  if (toidx==-1) toidx = ds->data->idxsize-1;
  if (toidx<idx) toidx = idx;

  switch (ds->flags & SUGDS_TYPE_MASK) {
    case SUGDS_TYPE_ABS:
      for (idx=fromidx; idx<=toidx; idx++) {
        if (sui_rd_long(ds->data, idx, &curval)!=SUI_RET_OK) return -1;
        if (cntdss) {
          int i;
          long curnval;
          dsn = ds+1;
          for (i = cntdss; i>0;i--) {
            if (((dsn->flags & SUGDS_AXIS_MASK)!=(ds->flags & SUGDS_AXIS_MASK)) ||
                !(dsn->flags & SUGDS_CONTINUE) || !dsn->data)
              break;
            if (sui_rd_long(dsn->data, idx, &curnval)!=SUI_RET_OK)
              break;
            curval += curnval;
            dsn++;
          }
        }
        if ((curval<minval) || (idx==fromidx)) minval = curval;
        if ((curval>maxval) || (idx==fromidx)) maxval = curval;
      }
      break;

    case SUGDS_TYPE_INC:
      minval = 0; /* minimum is automatically 0 */
      val = 0;
      for (idx=fromidx; idx<=toidx; idx++) {
        if (sui_rd_long(ds->data, idx, &curval)!=SUI_RET_OK) return -1;
        val += curval;
      }
      maxval = val;
      break;

    case SUGDS_TYPE_DIF:
      curval = 0;
      for (idx=fromidx; idx<=toidx; idx++) {
        long diff;
        val = curval;
        if (sui_rd_long(ds->data, idx, &curval)!=SUI_RET_OK) return -1;
        if (idx==fromidx) val = curval; /* the first diff is equal zero */
        diff = curval - val;
        if ((diff<minval) || (idx==fromidx)) minval = diff;
        if ((diff>maxval) || (idx==fromidx)) maxval = diff;
      }
      break;

    default:
      return -1;
  }
  if (min) *min = minval;
  if (max) *max = maxval;
  return 0;
}

/******************************************************************************/
void sui_graph_coord_point(suiw_graphxy_t *wg, long x, long y, sui_point_t *po)
{
  po->x = wg->gr_orig.x + ((x-wg->datawin.l)*(long)wg->gr_size.x)/(wg->datawin.r-wg->datawin.l+1);
  po->y = ((wg->datawin.t-y)*(long)wg->gr_size.y)/(wg->datawin.t-wg->datawin.b+1);
}

/*
 * sui_graph_draw_point - draw point as a star
 */
void sui_graph_draw_point(sui_dc_t *dc, suiw_graphxy_t *wg, sui_point_t *po)
{
  sui_coordinate_t cfx, ctx;
  sui_coordinate_t cfy, cty;

  ctx=cfx=po->x;
  cty=cfy=po->y;
  if (ctx<wg->gr_size.x) ctx++;
  if (cty<wg->gr_size.y) cty++;
  if (cfx>0) cfx--;
  if (cfy>0) cfy--;

  sui_draw_line(dc, cfx, cfy, cfx, cty);
  sui_draw_line(dc, cfx, cfy, ctx, cfy);
  sui_draw_line(dc, ctx, cfy, ctx, cty);
  sui_draw_line(dc, cfx, cty, ctx, cty);
}

/*
 * sui_graph_draw_line - draw line between two points
 */
void sui_graph_draw_line(sui_dc_t *dc, suiw_graphxy_t *wg, sui_point_t *pf, sui_point_t *pt)
{
  sui_draw_line(dc, pf->x, pf->y, pt->x, pt->y);
}

/*
 * sui_graph_draw_step - draw one step
 */
void sui_graph_draw_step(sui_dc_t *dc, suiw_graphxy_t *wg, sui_point_t *pc, sui_point_t *pl)
{
  if (pl) {
    sui_draw_line(dc, pl->x, pl->y, pc->x, pl->y);
    sui_draw_line(dc, pc->x, pl->y, pc->x, pc->y);
  } else
    sui_draw_line(dc, pc->x, wg->gr_zero.y, pc->x, pc->y);
}

/*
 * sui_graph_draw_outline - draw outline draw
 */
void sui_graph_draw_outline(sui_dc_t *dc, suiw_graphxy_t *wg, sui_point_t *pf, sui_point_t *pt)
{
  if (pf) {
    sui_draw_line(dc, pf->x, wg->gr_zero.y, pf->x, pf->y);
    sui_draw_line(dc, pf->x, pf->y, pt->x, pf->y);
    sui_draw_line(dc, pt->x, wg->gr_zero.y, pt->x, pf->y);
  }
  sui_draw_line(dc, pt->x, wg->gr_zero.y, pt->x, pt->y);
}

/*
 * sui_graph_draw_box - draw box between borders
 */
void sui_graph_draw_box(sui_dc_t *dc, suiw_graphxy_t *wg, sui_point_t *pf, sui_point_t *pt)
{
  if (pf->x<pt->x) {
    if (pf->y<wg->gr_zero.y) {
      sui_draw_rect(dc, pf->x, pf->y, pt->x-pf->x+1, wg->gr_zero.y-pf->y+1, 0);
    } else {
      sui_draw_rect(dc, pf->x, wg->gr_zero.y, pt->x-pf->x+1, pf->y-wg->gr_zero.y+1, 0);
    }
  } else {
    if (pf->y<wg->gr_zero.y) {
      sui_draw_rect(dc, pt->x, pf->y, pf->x-pt->x+1, wg->gr_zero.y-pf->y+1, 0);
    } else {
      sui_draw_rect(dc, pt->x, wg->gr_zero.y, pf->x-pt->x+1, pf->y-wg->gr_zero.y+1, 0);
    }
  }
}

/*
 * sui_graph_draw_olfx
 */
void sui_graph_draw_olfx(sui_dc_t *dc, suiw_graphxy_t *wg, sui_point_t *pf, sui_point_t *pt, sui_point_t *pcur)
{
  if (pf) {
    if (pcur->x > pf->x) {
      if (pcur->x < pt->x) { /* both */
        if (pf->y<wg->gr_zero.y)
          sui_draw_rect(dc, pf->x, pf->y, pcur->x-pf->x+1, wg->gr_zero.y-pf->y+1, 0);
        else
          sui_draw_rect(dc, pf->x, wg->gr_zero.y, pcur->x-pf->x+1, pf->y-wg->gr_zero.y+1, 0);
        sui_draw_line(dc, pcur->x, pf->y, pt->x, pf->y);
      } else { /* box only */
        if (pf->y<wg->gr_zero.y)
          sui_draw_rect(dc, pf->x, pf->y, pt->x-pf->x+1, wg->gr_zero.y-pf->y+1, 0);
        else
          sui_draw_rect(dc, pf->x, wg->gr_zero.y, pt->x-pf->x+1, pf->y-wg->gr_zero.y+1, 0);
        return;
      }
    } else { /* outlines only */
      sui_draw_line(dc, pf->x, pf->y, pt->x, pf->y);
    }
    sui_draw_line(dc, pt->x, wg->gr_zero.y, pt->x, pf->y);
  }
  sui_draw_line(dc, pt->x, wg->gr_zero.y, pt->x, pt->y);
}

/*
 * sui_graph_draw_olfy
 */
void sui_graph_draw_olfy(sui_dc_t *dc, suiw_graphxy_t *wg, sui_point_t *pf, sui_point_t *pt, sui_point_t *pcur)
{
  if (pf) {
    if (pf->y < wg->gr_zero.y) { /* pf->y is above zero (positive) */
      if (pcur->y < pf->y) { /* full box */
        sui_draw_rect(dc, pf->x, pf->y, pt->x-pf->x+1, wg->gr_zero.y-pf->y+1, 0);
      } else if (pcur->y > wg->gr_zero.y) {
        sui_draw_line(dc, pf->x, pf->y, pt->x, pf->y);
      } else {
        sui_draw_rect(dc, pf->x, pcur->y, pt->x-pf->x+1, wg->gr_zero.y-pcur->y+1, 0);
        sui_draw_line(dc, pf->x, pf->y, pt->x, pf->y);
      }
    } else { /* pf->y is below zero (negative) */
      if (pcur->y > pf->y) { /* full box */
        sui_draw_rect(dc, pf->x, wg->gr_zero.y, pt->x-pf->x+1, pf->y-wg->gr_zero.y+1, 0);
      } else if (pcur->y < wg->gr_zero.y) {
        sui_draw_line(dc, pf->x, pf->y, pt->x, pf->y);
      } else {
        sui_draw_rect(dc, pf->x, wg->gr_zero.y, pt->x-pf->x+1, pcur->y-wg->gr_zero.y+1, 0);
        sui_draw_line(dc, pf->x, pf->y, pt->x, pf->y);
      }
    }
    sui_draw_line(dc, pt->x, wg->gr_zero.y, pt->x, pf->y);
  }
  sui_draw_line(dc, pt->x, wg->gr_zero.y, pt->x, pt->y);
}


/*
 * sui_graph_draw_text
 */
void sui_graph_draw_text(sui_dc_t *dc, suiw_graphxy_t *wg, sui_dcfont_t *pdcfont,
                         utf8 *str, int len, sui_point_t *poi, sui_align_t align)
{
  sui_textdims_t tsize;
  sui_point_t ts;

  sui_text_get_size(dc, pdcfont, &tsize, len, str, align);
  ts.x = tsize.w;
  ts.y = tsize.h;

  if (sui_align_object_to_point(&ts, &ts, tsize.b, poi, align))
    return;
  sui_font_set(dc, pdcfont);
  sui_draw_text(dc, ts.x, ts.y, len, str, align);
}

/******************************************************************************
 * Widget basic functions
 ******************************************************************************/
/**
 * sui_wgraphxy_get_size - Return graphXY content size
 * @dc: Pointer to device context
 * @widget: Pointer to widget
 * @point: pointer to point structure
 * File: sui_wgraphxy.c
 */
//inline
int sui_wgraphxy_get_size(sui_dc_t *dc, sui_widget_t *widget, sui_point_t *point)
{
  suiw_graphxy_t *wgxy = (suiw_graphxy_t *)widget->data;
  sui_graph_dataset_t *ds = NULL;
  int i, n, o;
  sui_dcfont_t *pdcfont;
  sui_fontinfo_t fontinfo;

  pdcfont=sui_font_prepare(dc, widget->style->fonti->font, widget->style->fonti->size);
  sui_font_get_info(dc, pdcfont, &fontinfo);

  if (wgxy->gr_size.x<0) { /* compute graph X size */
    wgxy->gr_orig.x = 0;
    if (wgxy->data) {
      ds = wgxy->data->dset;
      i = 0; n = 0; o = 0;
      while(ds && i<wgxy->data->numsets) {
        if ((ds->flags & SUGDS_AXIS_MASK)==SUGDS_AXIS_Y) { /* check Y axis only */
          if ((ds->flags & SUGDS_SHOW_AXIS) && !(ds->flags & SUGDS_CONTINUE)) {
            n++; /* show 'n' X axes */
            if (ds->flags & SUGDS_SHOW_AXVALS) {
              if (ds->format)
                o += (ds->format->digits*fontinfo.maxwidth)-2; /* show axes labels - max. format->digits characters */
              else
                o += fontinfo.maxwidth-2;
            }
          }
        }
        ds++; i++;
      }
      wgxy->gr_orig.x += o+n*3;
      if (wgxy->gap>0)
        wgxy->gr_orig.x += wgxy->gap;
    }
    if (widget->place.w<0)
      wgxy->gr_size.x = wgxy->gr_orig.x + 100; /* random graph width if it isn't specified */
    else
      wgxy->gr_size.x = widget->place.w - wgxy->gr_orig.x; /* TODO: widget can have frame and gap !!! */
  }
  point->x = wgxy->gr_size.x+wgxy->gr_orig.x;

  if (wgxy->gr_size.y<0) { /* compute graph Y size */
    wgxy->gr_orig.y = 0;
    if (wgxy->data) {
      ds = wgxy->data->dset;
      i = 0; n = 0; o = 0;
      while(ds && i<wgxy->data->numsets) {
        if ((ds->flags & SUGDS_AXIS_MASK)==SUGDS_AXIS_X) { /* check X axis only */
          if ((ds->flags & SUGDS_SHOW_AXIS) && !(ds->flags & SUGDS_CONTINUE)) {
            n++; /* show 'n' Y axes */
            if (ds->flags & SUGDS_SHOW_AXVALS)
              o++; /* show 'o' axes labels */
          }
        }
        ds++; i++;
      }
      wgxy->gr_orig.y += o*(fontinfo.height)+n*3;
      if (wgxy->gap>0)
        wgxy->gr_orig.y += wgxy->gap;
    }
    if (widget->place.h<0)
      wgxy->gr_size.y = wgxy->gr_orig.y + 100; /* random graph height if it isn't specified */
    else
      wgxy->gr_size.y = widget->place.h - wgxy->gr_orig.y; /* TODO: widget can have frame and gap !!! */
  }
  point->y = wgxy->gr_size.y;

  return 0;
}

/**
 * sui_wgraphxy_init - initiate graphXY widget
 * @dc: Pointer to device context.
 * @widget: Pointer to a graphXY widget.
 *
 * The function initiates graphXY widget.
 * Return Value: The function does not return a value
 * File: sui_wgraphxy.c
 */
void sui_wgraphxy_init(sui_dc_t *dc, sui_widget_t *widget)
{
  suiw_graphxy_t *wgxy;

  wgxy = (suiw_graphxy_t *)widget->data;
  wgxy->gr_size.x = -1;
  wgxy->gr_size.y = -1;
  wgxy->gr_orig.x = 0;
  wgxy->gr_orig.y = 0;
  wgxy->datawin.l = -1;
  wgxy->datawin.r = -1;
  wgxy->datawin.b = 0;
  wgxy->datawin.t = 0;
  wgxy->offsx = 0;
  wgxy->offsy = 0;

  if (widget->place.w < 0 || widget->place.h < 0) {
    sui_point_t size;
    sui_widget_compute_size(dc, widget, &size);
    if (widget->place.w < 0) widget->place.w = size.x;
    if (widget->place.h < 0) widget->place.h = size.y;
  }
}

/**
 * sui_wgraphxy_draw - Draw graphXY widget
 * @dc: Pointer to device context.
 * @widget: Pointer to a graphXY widget.
 *
 * The function draws two dimensional graph according to its settings. The function
 * is reaction to event SUEV_DRAW and SUEV_REDRAW.
 * Return Value: The function returns 0 if graph has been successfully drawn.
 * File: sui_wgraphxy.c
 */
int sui_wgraphxy_draw(sui_dc_t *dc, sui_widget_t *widget)
{
  sui_dcfont_t *pdcfont;
  sui_fontinfo_t fontinfo;
  suiw_graphxy_t *wgxy;
  sui_point_t box, size, origin = {.x=0, .y=0};
  sui_align_t al;
  int i, j;
  sui_graph_dataset_t *ds = NULL, *dsx = NULL;
/* graph window */
  sui_rect_t wdgwin;
/* data window */
  int xidx_min, xidx_max;
  long posx = 0, posy = 0;
  int ax = 0, ay = 0;
/* temporary variables */
  long tmpaxval;
  sui_dinfo_t di_axisvalue = {.refcnt=SUI_STATIC, .tinfo=0, .ptr = NULL, .rdval=NULL, .wrval=NULL, .info=0, .fdigits=0};

  if (!widget || !widget->style || !(wgxy = sui_gxywdg(widget))) return -1;
  if (!wgxy->data) return 0;

  box.x=widget->place.w; box.y=widget->place.h;
  al = sui_widget_get_item_align(widget);

  if (wgxy->gr_size.x==-1 && wgxy->gr_size.y==-1) {
    if (sui_hevent_command(widget, SUCM_GETSIZE, dc, (long) &size))
    {
      size.x = 0;
      size.y = 0;
    }
  }
  sui_align_object(&box, &origin, &size, 0, widget->style, al);
  pdcfont=sui_font_prepare(dc, widget->style->fonti->font, widget->style->fonti->size);
  sui_font_set(dc, pdcfont);

/* get current position if it is specified */
  if (wgxy->curx) {
    if (sui_rd_long(wgxy->curx, 0, &posx)!=SUI_RET_OK)
      ul_logerr("GR-DRAW: reading curX failed\n");
  }
  if (wgxy->cury) {
    if (sui_rd_long(wgxy->cury, 0, &posy)!=SUI_RET_OK)
      ul_logerr("GR-DRAW: reading curY failed\n");
  }
/* prepare temporary variables */
  tmpaxval = 0;
  di_axisvalue.ptr = &tmpaxval;
  di_axisvalue.tinfo = SUI_TYPE_LONG;
  di_axisvalue.rdval = sui_long_rdval;

/* check data, get minimal,maximal values ... */
  for (i=0;i<wgxy->data->numsets;i++) { /* find data-set describes X axis */
    ds = (wgxy->data->dset+i);
    if ((ds->flags & SUGDS_AXIS_MASK)==SUGDS_AXIS_X) { /* remember the first data-set for X-axis */
      dsx = ds; break;
    }
  }
  switch (sui_test_flag(widget, SUXF_RX_MASK)) {
    case SUXF_RX_AUTO:
      {
        long minx, maxx;
        sui_graph_get_minmax(dsx, wgxy->data->numsets-i-1, -1, -1, &minx, &maxx);
        if (wgxy->datawin.l==-1 && wgxy->datawin.r==-1) {
          wgxy->datawin.l = minx;
          wgxy->datawin.r = maxx;
        } else {
          if (wgxy->datawin.l>minx)
            wgxy->datawin.l = minx;
          if (wgxy->datawin.r<maxx)
            wgxy->datawin.r = maxx;
        }
      }
      break;
    case SUXF_RX_FIXABS:
      if (!wgxy->originx || (sui_rd_long(wgxy->originx, 0, &wgxy->datawin.l)!=SUI_RET_OK)) wgxy->datawin.l = 0;
      if (!wgxy->rangex || (sui_rd_long(wgxy->rangex, 0, &wgxy->datawin.r)!=SUI_RET_OK)) wgxy->datawin.r = 0; /* absolute value */
      if (wgxy->datawin.r>0) wgxy->datawin.r--;
      else wgxy->datawin.r = 0;
      wgxy->datawin.r += wgxy->datawin.l;
      break;
    case SUXF_RX_FIXRELO:
      {
        long tmprng;
        if (!wgxy->originx || (sui_rd_long(wgxy->originx, 0, &wgxy->datawin.l)!=SUI_RET_OK)) wgxy->datawin.l = 0;
        if (!wgxy->rangex || (sui_rd_long(wgxy->rangex, 0, &wgxy->datawin.r)!=SUI_RET_OK)) wgxy->datawin.r = 0; /* range from origin */
        tmprng = wgxy->datawin.r;
        wgxy->datawin.r = wgxy->datawin.l + tmprng/2;
        wgxy->datawin.l = wgxy->datawin.l - tmprng/2;
      }
      break;
    case SUXF_RX_FIXRELC:
      if (!wgxy->rangex || (sui_rd_long(wgxy->rangex, 0, &wgxy->datawin.r)!=SUI_RET_OK)) wgxy->datawin.r = 0; /* range around current position */
      wgxy->datawin.l = posx - wgxy->datawin.r/2;
      wgxy->datawin.r = posx + wgxy->datawin.r/2;
      break;
  }
  wgxy->datawin.l += wgxy->offsx;
  wgxy->datawin.r += wgxy->offsx;

  ds = NULL; i = 0;
  while(i<wgxy->data->numsets) {
    ds = (wgxy->data->dset+i);
    if ((ds->flags & SUGDS_AXIS_MASK)==SUGDS_AXIS_Y) /* data-set describes Y axis */
      break;
    i++;
  }
  if (i<wgxy->data->numsets && ds) {
    switch (sui_test_flag(widget, SUXF_RY_MASK)) {
      case SUXF_RY_AUTO:
        {
          long miy, may, rngy;
          if (!wgxy->rangey || (sui_rd_long(wgxy->rangey, 0, &rngy)!=SUI_RET_OK))
            rngy = 0;
          sui_graph_get_minmax(ds, wgxy->data->numsets-i-1, -1, -1, &miy, &may);
          if (wgxy->datawin.b>miy)
            wgxy->datawin.b = miy;
          if (wgxy->datawin.t<may)
            wgxy->datawin.t = may;
          if ((wgxy->datawin.t-wgxy->datawin.b+1)<rngy) /* use rangey if it is defined as a minimal range */
            wgxy->datawin.t = wgxy->datawin.b + rngy - 1;
          //ul_logtrash("WGRAPH-DRAW:minmaxY = miY=%ld, maY=%ld, b=%ld, t=%ld\n", miy, may, wgxy->datawin.b, wgxy->datawin.t);
        }
        break;
      case SUXF_RY_FIXABS:
        if (!wgxy->originy || (sui_rd_long(wgxy->originy, 0, &wgxy->datawin.b)!=SUI_RET_OK)) wgxy->datawin.b = 0;
        if (!wgxy->rangey || (sui_rd_long(wgxy->rangey, 0, &wgxy->datawin.t)!=SUI_RET_OK)) wgxy->datawin.t = 0; /* absolute value */
        if (wgxy->datawin.t>0) wgxy->datawin.t--;
        else wgxy->datawin.t = 0;
        wgxy->datawin.t += wgxy->datawin.b;
        break;
      case SUXF_RY_FIXRELO:
        {
          long tmprng;
          if (!wgxy->originy || (sui_rd_long(wgxy->originy, 0, &wgxy->datawin.b)!=SUI_RET_OK)) wgxy->datawin.b = 0;
          if (!wgxy->rangey || (sui_rd_long(wgxy->rangey, 0, &wgxy->datawin.t)!=SUI_RET_OK)) wgxy->datawin.t = 0; /* range from origin */
          tmprng = wgxy->datawin.t;
          wgxy->datawin.t = wgxy->datawin.b + tmprng/2;
          wgxy->datawin.b = wgxy->datawin.b - tmprng/2;
        }
        break;
      case SUXF_RY_FIXRELC:
        if (!wgxy->rangey || (sui_rd_long(wgxy->rangey, 0, &wgxy->datawin.t)!=SUI_RET_OK)) wgxy->datawin.t = 0; /* range around current position */
        wgxy->datawin.b = posy - wgxy->datawin.t/2;
        wgxy->datawin.t = posy + wgxy->datawin.t/2;
        break;
    }
  }
  wgxy->datawin.b += wgxy->offsy;
  wgxy->datawin.t += wgxy->offsy;

/* draw frame and others ... */
//  sui_set_widget_fgcolor(dc, widget, SUC_ITEM);
//  sui_set_widget_bgcolor(dc, widget, SUC_BACKGROUND);

/* draw all data-sets */

  dsx = NULL;
  xidx_min = -1; xidx_max = -1;
  for (i=0;i<wgxy->data->numsets;i++) {
    ds = wgxy->data->dset+i;

    /* prepare all for drawing one data-set */
    sui_set_bgcolor(dc, widget, SUC_BACKGROUND);
    if (ds->flags & SUGDS_USE_COLOR) {
      sui_gdi_set_fgcolor(dc, ds->color);
    } else {
      sui_set_fgcolor(dc, widget, SUC_ITEM);
    }

    if ((ds->flags & SUGDS_AXIS_MASK)==SUGDS_AXIS_X) { /* X data isn't drawn directly */
      dsx = ds;

      /* draw axis */
      if (ds->flags & SUGDS_SHOW_AXIS) {
        int st=0;
        sui_coordinate_t lx = -1;
        /* set clip area to all X axes */
        wdgwin.x = 0;
        wdgwin.y = wgxy->gr_size.y;
        wdgwin.w = wgxy->gr_orig.x+wgxy->gr_size.x;
        wdgwin.h = wgxy->gr_orig.y;
        sui_gdi_set_carea(dc, &wdgwin);

        pdcfont=sui_font_prepare(dc, widget->style->fonti->font, widget->style->fonti->size);
        sui_font_get_info(dc, pdcfont, &fontinfo);
        /* draw axis */
        wdgwin.y = wgxy->gr_size.y+ax;
        if (ds->flags & SUGDS_SHOW_AXVALS)
          ax += fontinfo.height;
        else
          ax += 3;
        sui_draw_line(dc, wgxy->gr_orig.x, wdgwin.y+wgxy->gap, wgxy->gr_orig.x+wgxy->gr_size.x, wdgwin.y+wgxy->gap);
        /* draw tick */
        if (ds->step>0)
          st = ds->step;
        else if (ds->step==0)
          st = -1;
        else
          st = (ds->format && ds->format->bigstep) ? ds->format->bigstep : 10;
          /* find first tick before datawin.l */
        tmpaxval = 0;
        if (st>0) {
          switch (sui_test_flag(widget, SUXF_RX_MASK)) {
            case SUXF_RX_AUTO:
            case SUXF_RX_FIXABS:
              tmpaxval = wgxy->datawin.l;
              break;
            case SUXF_RX_FIXRELO:
              if (wgxy->originx) sui_rd_long(wgxy->originx, 0, &tmpaxval);
              if (st>0)
                tmpaxval = tmpaxval - ((tmpaxval - wgxy->datawin.l)/st)*st;
              break;
            case SUXF_RX_FIXRELC:
              if (wgxy->curx) sui_rd_long(wgxy->curx, 0, &tmpaxval);
              if (st>0)
                tmpaxval = tmpaxval - ((tmpaxval - wgxy->datawin.l)/st)*st;
              break;
          }
        } else {
          tmpaxval = wgxy->datawin.l;
        }
        /* draw all ticks up to datawin.r */
        if (ds->data) di_axisvalue.fdigits = ds->data->fdigits;
        while (tmpaxval <= wgxy->datawin.r) {
          sui_point_t cpt;
          cpt.x = wgxy->gr_orig.x + ((tmpaxval-wgxy->datawin.l)*(long)wgxy->gr_size.x)/(wgxy->datawin.r-wgxy->datawin.l+1);
          cpt.y = wdgwin.y+wgxy->gap;
          sui_draw_line(dc, cpt.x, cpt.y, cpt.x, cpt.y+3);
          if (ds->flags & SUGDS_SHOW_AXVALS) {
            sui_point_t ts;
            sui_textdims_t tsize;
            sui_utf8_t *vtxt = NULL;
            if (ds->data) {
              if (tmpaxval>=0 && tmpaxval<ds->data->idxsize)
                vtxt = sui_dinfo_2_utf8(ds->data, tmpaxval, ds->format, SUTR_RELATIVETIME);
              else
                vtxt = NULL;
            } else {
              vtxt = sui_dinfo_2_utf8(&di_axisvalue, 0, ds->format, SUTR_RELATIVETIME);
            }
            sui_text_get_size(dc, pdcfont, &tsize, -1, vtxt, SUAL_CENTER);
            ts.x = tsize.w; ts.y = tsize.h;
            sui_align_object_to_point(&ts, &ts, tsize.b, &cpt, ds->align | SUAL_BOTTOM);
            if ((lx<0 || ts.x>lx) && (ts.x>=0)) {
              sui_font_set(dc, pdcfont);
              sui_draw_text(dc, ts.x, ts.y+3, -1, vtxt, SUAL_CENTER);
              lx = ts.x+tsize.w;
            }
            sui_utf8_dec_refcnt(vtxt); vtxt = NULL;
          }
          if (st>0)
            tmpaxval += st;
          else {
            if (tmpaxval==wgxy->datawin.r)
              break;
            else
              tmpaxval=wgxy->datawin.r;
          }
        }
      }
      /* continue with any Y axis */
      continue;
    }

    /* set clip area to graph canvas */
    wdgwin.x = wgxy->gr_orig.x;
    wdgwin.y = 0;
    wdgwin.w = wgxy->gr_size.x;
    wdgwin.h = wgxy->gr_size.y+1;
    sui_gdi_set_carea(dc, &wdgwin);

    /* for each data-set : DRAW IT (or its windowed part) */
    if ((ds->flags & SUGDS_XYDATA) && dsx) { // && xidx_min>=0) { /* XY data and any "visible" X data has been found */
      int max, over = 0;
      long lastx = 0, lasty = 0;
      long curx = 0, cury = 0;
      long tmpx = 0, tmpy = 0;

      sui_point_t plast, pcur, ppos;

      max = (dsx->data->idxsize>=ds->data->idxsize) ? ds->data->idxsize : dsx->data->idxsize;
      if ((ds->flags & SUGDS_DRAW_MASK)==SUGDS_DRAW_NONE) /* don't draw data-set */
        continue;

      sui_graph_coord_point(wgxy, lastx, lasty, &pcur); /* zero in graph */
      wgxy->gr_zero = pcur;

      sui_graph_coord_point(wgxy, posx, posy, &ppos); /* current position */

      for (j=0;j<max;j++) { /* process data from beginning - ABS/INC/DIFF data */
        lastx = curx; lasty = cury;
        plast = pcur;

        if (sui_graph_get_value(dsx, j, &curx, &tmpx) ||
            sui_graph_get_value(ds, j, &cury, &tmpy)) {
          ul_logerr("GR-DRAW: reading X,Y axis data failed\n");
          return -1;
        }
        sui_graph_coord_point(wgxy, curx, cury, &pcur);
        if (lastx>wgxy->datawin.r && curx>wgxy->datawin.r) /* don't draw graph outside visible window */
          over=1;

        switch (ds->flags & SUGDS_DRAW_MASK) {
          case SUGDS_DRAW_POINT:
            if (!over)
              sui_graph_draw_point(dc, wgxy, &pcur);
            break;

          case SUGDS_DRAW_LINE:
            if (j>0 || ((curx>0) && ((ds->flags & SUGDS_TYPE_MASK)==SUGDS_TYPE_INC))) {
              sui_graph_draw_line(dc, wgxy, &plast, &pcur);
            }
            break;

          case SUGDS_DRAW_DOTLINE:
            if (j>0 || ((curx>0) && ((ds->flags & SUGDS_TYPE_MASK)==SUGDS_TYPE_INC))) {
              sui_graph_draw_line(dc, wgxy, &plast, &pcur);
            }
            if (!over)
              sui_graph_draw_point(dc, wgxy, &pcur);
            break;

          case SUGDS_DRAW_STEP:
            if (j>0)
              sui_graph_draw_step(dc, wgxy, &pcur, &plast);
            else
              sui_graph_draw_step(dc, wgxy, &pcur, NULL);
            break;

          case SUGDS_DRAW_BOX:
            if (j>0) {
              sui_graph_draw_box(dc, wgxy, &plast, &pcur); /* draw previous box */
              if ((over && (j+1)<max) || ((j+1)==max && ((dsx->flags &SUGDS_TYPE_MASK)==SUGDS_TYPE_INC))) {
                if ((j+1)<max) {
                  if (sui_graph_get_value(dsx, j+1, &lastx, &tmpx)) {
                    ul_logerr("GR-DRAW: reading X,Y axis data (2) failed\n");
                    return -1;
                  }
                } else
                  lastx = tmpx;
                sui_graph_coord_point(wgxy, lastx, lasty, &plast);
                sui_graph_draw_box(dc, wgxy, &pcur, &plast); /* draw the last box (partially invisible) */
              }
            }
            break;

          case SUGDS_DRAW_OUTLINE:
            if (j>0) {
              sui_graph_draw_outline(dc, wgxy, &plast, &pcur); /* draw previous box */
              if ((over && (j+1)<max) || ((j+1)==max && ((dsx->flags &SUGDS_TYPE_MASK)==SUGDS_TYPE_INC))) {
                if ((j+1)<max) {
                  if (sui_graph_get_value(dsx, j+1, &lastx, &tmpx)) {
                    ul_logerr("GR-DRAW: reading X,Y axis data (3) failed\n");
                    return -1;
                  }
                } else
                  lastx = tmpx;
                lasty = cury;
                sui_graph_coord_point(wgxy, lastx, lasty, &plast);
                sui_graph_draw_outline(dc, wgxy, &pcur, &plast); /* draw the last box (partially invisible) */
              }
            } else
              sui_graph_draw_outline(dc, wgxy, NULL, &pcur); /* draw the first point */
            break;

          case SUGDS_DRAW_OLFX:
            if (j>0) {
              sui_graph_draw_olfx(dc, wgxy, &plast, &pcur, &ppos); /* draw previous box */
            } else
              sui_graph_draw_outline(dc, wgxy, NULL, &pcur); /* draw the first point */
            break;

          case SUGDS_DRAW_OLFY:
            if (j>0) {
              sui_graph_draw_olfy(dc, wgxy, &plast, &pcur, &ppos); /* draw previous box */
            } else
              sui_graph_draw_outline(dc, wgxy, NULL, &pcur); /* draw the first point */
            break;

          default:
            break;
        }

        /* draw values */
        if (ds->flags & SUGDS_SHOW_VALUES) {
          utf8 *buf = sui_dinfo_2_utf8(ds->data, j, ds->format, 0);
          if (buf) { /* draw aligned text to a point in graph */
            if (ds->font)
              pdcfont=sui_font_prepare(dc, ds->font->font, ds->font->size);
            else
              pdcfont=sui_font_prepare(dc, widget->style->fonti->font, widget->style->fonti->size);
            sui_graph_draw_text(dc, wgxy, pdcfont, buf, -1, &pcur, ds->align);
            sui_utf8_dec_refcnt(buf);
          }
        }

        if (over) /* the current point is outside of viewed range */
          break;
      }

    } else { /* only Y data */
      /* Y data at periodical X position (offset,step) */
      sui_graph_dataset_t *dsf = NULL;
      long cury = 0, tmpy = 0, zeroy, curofsy;
      int xsize, ysize;
      sui_point_t cpos = {0,0};
      sui_point_t tpos;
      int pposy;
      int k = i;

      if (ds->flags & SUGDS_CONTINUE) /* data set is a part of one graph with the previous data set */
        continue;

      xsize = (wgxy->datawin.r-wgxy->datawin.l+1); /* number of items */
      xsize = (wgxy->gr_size.x - (xsize-1)*wgxy->gap) / xsize;
      if (xsize==0)
        xsize = 1; /* rangeX should be recounted */

      ysize = wgxy->datawin.t-wgxy->datawin.b;
      if (ysize<=0)
        ysize = 1; /* rangeY */
      tpos.y = ((wgxy->datawin.t-0)*(long)wgxy->gr_size.y)/ysize; /* temporary position - Y as zero */
      pposy = ((wgxy->datawin.t-posy)*(long)wgxy->gr_size.y)/ysize; /* current position Y */

      dsf = ds; /* save first dataset */
      zeroy = tpos.y; /* save first zero */
      curofsy = 0;
      cpos.x = wgxy->gr_orig.x;
      j = wgxy->datawin.l;

      while (ds && j<=wgxy->datawin.r) { /* process data from beginning - ABS/INC/DIFF data */
        tpos.x = cpos.x + xsize-1;
        if (j>=0 && j<ds->data->idxsize) {
          if (sui_graph_get_value(ds, j, &cury, &tmpy)) {
            ul_logerr("GR-DRAW: reading Y axis data failed\n");
            return -1;
          }
          cury += curofsy;
          cpos.y = ((wgxy->datawin.t-cury)*(long)wgxy->gr_size.y)/ysize;

          switch(ds->flags & SUGDS_DRAW_MASK) {
            case SUGDS_DRAW_POINT:
              sui_draw_line(dc, cpos.x, cpos.y, tpos.x, cpos.y);
              break;
            case SUGDS_DRAW_LINE:
              sui_draw_line(dc, cpos.x+xsize/2,cpos.y,cpos.x+xsize/2,tpos.y);
              break;
            case SUGDS_DRAW_DOTLINE:
              sui_draw_line(dc, cpos.x+xsize/2,cpos.y,cpos.x+xsize/2,tpos.y);
              sui_draw_line(dc, cpos.x, cpos.y, tpos.x, cpos.y);
              break;
            case SUGDS_DRAW_BOX:
              if (cpos.y<tpos.y) {
                sui_draw_rect(dc, cpos.x, cpos.y, xsize, tpos.y-cpos.y+1, 0);
              } else {
                sui_draw_rect(dc, cpos.x, tpos.y, xsize, cpos.y-tpos.y+1, 0);
              }
              break;
            case SUGDS_DRAW_OUTLINE:
              sui_draw_line(dc, cpos.x,cpos.y,tpos.x,cpos.y);
              sui_draw_line(dc, cpos.x,cpos.y,cpos.x,tpos.y);
              sui_draw_line(dc, tpos.x,cpos.y,tpos.x,tpos.y);
              sui_draw_line(dc, cpos.x,tpos.y,tpos.x,tpos.y);
              break;
            case SUGDS_DRAW_OLFX:
              if (posx>=j) {
                if (cpos.y<tpos.y) {
                  sui_draw_rect(dc, cpos.x, cpos.y, xsize, tpos.y-cpos.y+1, 0);
                } else {
                  sui_draw_rect(dc, cpos.x, tpos.y, xsize, cpos.y-tpos.y+1, 0);
                }
              } else {
                sui_draw_line(dc, cpos.x,cpos.y,tpos.x,cpos.y);
                sui_draw_line(dc, cpos.x,cpos.y,cpos.x,tpos.y);
                sui_draw_line(dc, tpos.x,cpos.y,tpos.x,tpos.y);
                sui_draw_line(dc, cpos.x,tpos.y,tpos.x,tpos.y);
              }
              break;
            case SUGDS_DRAW_OLFY:
              if (cpos.y<tpos.y) { /* positive value */
                int border = (pposy>cpos.y) ? ((pposy<tpos.y) ? pposy : tpos.y) : cpos.y;
                sui_draw_rect(dc, cpos.x, border, xsize, tpos.y-border+1, 0);
                sui_draw_line(dc, cpos.x,cpos.y,tpos.x,cpos.y);
                sui_draw_line(dc, cpos.x,cpos.y,cpos.x,border);
                sui_draw_line(dc, tpos.x,cpos.y,tpos.x,border);
              } else { /* negative value */
                int border = (pposy<cpos.y) ? ((pposy>tpos.y) ? pposy : tpos.y) : cpos.y;
                sui_draw_rect(dc, cpos.x, tpos.y, xsize, border-tpos.y+1, 0);
                sui_draw_line(dc, cpos.x,cpos.y,tpos.x,cpos.y);
                sui_draw_line(dc, cpos.x,border,cpos.x,cpos.y);
                sui_draw_line(dc, tpos.x,border,tpos.x,cpos.y);
              }
              break;
          }
        }
        ds++; /* get next dataset */
        k++;
        if (k<wgxy->data->numsets && ds &&
            ((dsf->flags & SUGDS_AXIS_MASK)==(ds->flags & SUGDS_AXIS_MASK)) &&
            (ds->flags & SUGDS_CONTINUE) &&
            ((ds->flags & SUGDS_TYPE_MASK) == SUGDS_TYPE_ABS)) {
          tpos.y = cpos.y;
          curofsy = cury;
        } else {
          k = i;
          ds = dsf;
          tpos.y = zeroy;
          curofsy = 0;
          cpos.x += xsize+wgxy->gap;
          j++;
        }
      }
      ds = dsf; /* continue from the original dataset */
    }

    /* draw Y axis */
    if ((ds->flags & SUGDS_SHOW_AXIS) && !(ds->flags & SUGDS_CONTINUE)) {
      int st = 0;
      sui_coordinate_t ly = -1;
      long ysize = 0;
      /* set clip area to all Y axes */
      wdgwin.x = 0;
      wdgwin.y = 0;
      wdgwin.w = wgxy->gr_orig.x;
      wdgwin.h = wgxy->gr_orig.y+wgxy->gr_size.y;
      sui_gdi_set_carea(dc, &wdgwin);

      pdcfont=sui_font_prepare(dc, widget->style->fonti->font, widget->style->fonti->size);
      sui_font_get_info(dc, pdcfont, &fontinfo);
      wdgwin.x = wgxy->gr_orig.x-ay-wgxy->gap;
      if (ds->flags & SUGDS_SHOW_AXVALS)
        ay += fontinfo.maxwidth;
      else
        ay += 3;
      sui_draw_line(dc, wdgwin.x, 0, wdgwin.x, wgxy->gr_size.y);
      /* draw ticks and labels */

      if (ds->step>0)
        st = ds->step;
      else if (ds->step==0)
        st = -1;
      else
        st = (ds->format && ds->format->bigstep) ? ds->format->bigstep : 10;

      tmpaxval = wgxy->datawin.b;
      di_axisvalue.fdigits = (ds->data) ? ds->data->fdigits : 0;
      ysize = wgxy->datawin.t-wgxy->datawin.b;
      if (ysize<=0)
        ysize = 1; /* rangeY */

      while (tmpaxval <= wgxy->datawin.t) {
        sui_textdims_t tsize;
        sui_point_t ts, cpt;
        sui_utf8_t *vtxt = NULL;

        cpt.x = wdgwin.x;
        cpt.y = wgxy->gr_size.y - ((tmpaxval-wgxy->datawin.b)*(long)wgxy->gr_size.y)/ysize;
        sui_draw_line(dc, cpt.x-3, cpt.y, cpt.x, cpt.y);
        if (ds->flags & SUGDS_SHOW_AXVALS) {
          vtxt = sui_dinfo_2_utf8(&di_axisvalue, 0, ds->format, SUTR_RELATIVETIME);
          sui_text_get_size(dc, pdcfont, &tsize, -1, vtxt, SUAL_CENTER);
          ts.x = tsize.w; ts.y = tsize.h;
          sui_align_object_to_point(&ts, &ts, tsize.b, &cpt, ds->align | SUAL_LEFT);
          if (ly<0) ly = cpt.y;
          if ((ts.y+tsize.h)<ly && ts.y<wgxy->gr_size.y) {
            sui_font_set(dc, pdcfont);
            sui_draw_text(dc, ts.x-3, ts.y, -1, vtxt, SUAL_CENTER);
            ly = ts.y+tsize.h;
          }
          sui_utf8_dec_refcnt(vtxt); vtxt = NULL;
        }
        if (st>0)
          tmpaxval += st;
        else {
          if (tmpaxval==wgxy->datawin.t)
            break;
          else
            tmpaxval = wgxy->datawin.t;
        }
      }
    }

  }

/* draw cross-line at current position */
  sui_set_bgcolor(dc, widget, SUC_BACKGROUND);
  sui_set_fgcolor(dc, widget, SUC_FRAME);
  if (sui_test_flag(widget, SUXF_DRAW_CURPOS)) {
    sui_point_t pcur, ptmp;
    sui_graph_coord_point(wgxy, posx, posy, &pcur);
    if (wgxy->curx) {
      sui_coordinate_t hlp = pcur.y;
      ptmp.x = pcur.x;
      pcur.y = 0;
      ptmp.y = wgxy->gr_size.y;
      sui_graph_draw_line(dc, wgxy, &pcur, &ptmp);
      pcur.y = hlp;
    }
    if (wgxy->cury) {
      ptmp.y = pcur.y;
      pcur.x = 0;
      ptmp.x = wgxy->gr_size.x;
      sui_graph_draw_line(dc, wgxy, &pcur, &ptmp);
    }
  }

/* draw axes */

  return 0;
}

/**
 * sui_wgraphxy_hkey - Reaction to KEY_DOWN, KEY_UP event in GRAPHXY widget
 * @widget: Pointer to table widget.
 * @event: Pointer to the event.
 *
 * The function is response to keyboard event in graphxy widget.
 *
 * Return Value: The function returns 1 if it takes to event.
 * File: sui_wgraphxy.c
 */
//inline
int sui_wgraphxy_hkey(sui_widget_t *widget, sui_event_t *event)
{
  int key = event->keydown.keycode;
  suiw_graphxy_t *wgxy = sui_gxywdg(widget);
  sui_graph_dataset_t *ds, *dsy = NULL, *dsx = NULL;
  long addx = 0, addy = 0, val = 0;
  int used = 0, i=0;

  if (!wgxy->data) return 0;

  /* find first X and first Y data-set */
  ds = wgxy->data->dset;
  while(ds && i<wgxy->data->numsets) {
    if (!dsx &&((ds->flags & SUGDS_AXIS_MASK)==SUGDS_AXIS_X))
      dsx = ds;
    if (!dsy &&((ds->flags & SUGDS_AXIS_MASK)==SUGDS_AXIS_Y))
      dsy = ds;
    if (dsx && dsy) break;
    ds++;
  }

  if (key==MWKEY_UP || key==MWKEY_DOWN) {
    if (dsy) {
      if ((dsy->flags & SUGDS_XYDATA) && dsy->format)
        addy = dsy->format->litstep;
      else
        addy = dsy->step;
    } else
      addy = 10; /* random value ... */
    if (key==MWKEY_DOWN) addy = -addy;

  } else if (key==MWKEY_LEFT || key==MWKEY_RIGHT) {
    if (dsx) {
      if ((dsx->flags & SUGDS_XYDATA) && dsx->format)
        addx = dsx->format->litstep;
      else
        addx = dsx->step;
    } else
      addx = 10; /* random value ... */
    if (key==MWKEY_LEFT) addx = -addx;

  } else if (key==MWKEY_PAGEUP) {
    if (wgxy->rangex) {
      if (sui_rd_long(wgxy->rangex, 0, &val)!=SUI_RET_OK) return -1;
      val = val << 1;
      if (sui_wr_long(wgxy->rangex, 0, &val)!=SUI_RET_OK) return -1;
    }
    if (wgxy->rangey) {
      if (sui_rd_long(wgxy->rangey, 0, &val)!=SUI_RET_OK) return -1;
      val = val << 1;
      if (sui_wr_long(wgxy->rangey, 0, &val)!=SUI_RET_OK) return -1;
    }
    used = 1;

  } else if (key==MWKEY_PAGEDOWN) {
    if (wgxy->rangex) {
      if (sui_rd_long(wgxy->rangex, 0, &val)!=SUI_RET_OK) return -1;
      if (val>2) val = val >> 1;
      if (sui_wr_long(wgxy->rangex, 0, &val)!=SUI_RET_OK) return -1;
    }
    if (wgxy->rangey) {
      if (sui_rd_long(wgxy->rangey, 0, &val)!=SUI_RET_OK) return -1;
      if (val>2) val = val >> 1;
      if (sui_wr_long(wgxy->rangey, 0, &val)!=SUI_RET_OK) return -1;
    }
    used = 1;

  } else if (key==MWKEY_ENTER || key==' ') {
    if (sui_test_flag(widget, SUXF_KEYS_CHOFS)) {
      wgxy->offsx = 0;
      wgxy->offsy = 0;
      used = 1;
    }
  }

  if (addx) {
    if (sui_test_flag(widget, SUXF_KEYS_CHOFS)) {
      wgxy->offsx += addx;
      used = 1;
    } else if (wgxy->originx) {
      if(sui_rd_long(wgxy->originx, 0, &val)!=SUI_RET_OK) return -1;
      val += addx;
      if (wgxy->originx->minval || wgxy->originx->maxval) {
        if (val<wgxy->originx->minval) val = wgxy->originx->minval;
        if (val>wgxy->originx->maxval) val = wgxy->originx->maxval;
      }
      if (sui_wr_long(wgxy->originx, 0, &val)!=SUI_RET_OK) return -1;
      used = 1;
    }
  }
  if (addy) {
    if (sui_test_flag(widget, SUXF_KEYS_CHOFS)) {
      wgxy->offsy += addy;
      used = 1;
    } else if (wgxy->originy) {
      if (sui_rd_long(wgxy->originy, 0, &val)!=SUI_RET_OK) return -1;
      val += addy;
      if (wgxy->originy->minval || wgxy->originy->maxval) {
        if (val<wgxy->originy->minval) val = wgxy->originy->minval;
        if (val>wgxy->originy->maxval) val = wgxy->originy->maxval;
      }
      if (sui_wr_long(wgxy->originy, 0, &val)!=SUI_RET_OK) return -1;
      used = 1;
    }
  }

  return used;
}

/******************************************************************************
 * GraphXY event handler
 ******************************************************************************/
/**
 * sui_wgraphxy_hevent - GraphXY event handler function
 * @widget: Pointer to the graphXY widget.
 * @event: Pointer to received event.
 *
 * The function is reaction to all events received by the bitmap widget.
 * Return Value: The function does not returns a value.
 * File: sui_wgraphxy.c
 */
void sui_wgraphxy_hevent(sui_widget_t *widget, sui_event_t *event)
{
  switch(event->what) {
    case SUEV_DRAW:
    case SUEV_REDRAW:
      sui_widget_draw(event->draw.dc, widget, sui_wgraphxy_draw);
      return;
    case SUEV_KDOWN:
      if (sui_wgraphxy_hkey(widget, event)>0) {
        sui_clear_event(widget, event);
        sui_redraw_request(widget);
      }
      break;
    case SUEV_COMMAND:
      switch(event->message.command) {
        case SUCM_INIT:
          sui_widget_hevent(widget, event);
          sui_wgraphxy_init((sui_dc_t *)event->message.ptr, widget);
          sui_clear_event(widget, event);
          break;

        case SUCM_GETSIZE:
          if (!sui_wgraphxy_get_size((sui_dc_t *)event->message.ptr,
                                  widget, (sui_point_t *)event->message.info)) {
            sui_clear_event(widget, event);
            return;
          }
          break; /* continue in basic widget hevent */
      }
  }
  if (event->what != SUEV_NOTHING)
    sui_widget_hevent(widget,event);
}


/******************************************************************************
 * Basic graphXY vmt & signal-slot mechanism
 ******************************************************************************/
int sui_wgraphxy_vmt_init(sui_widget_t *pw, void *params)
{
  pw->data = sui_malloc(sizeof(suiw_graphxy_t));
  if(!pw->data)
    return -1;

  pw->type = SUWT_GRAPHXY;
  pw->hevent = sui_wgraphxy_hevent;

  return 0;
}

void sui_wgraphxy_vmt_done(sui_widget_t *pw)
{
  if (pw->data) {
    suiw_graphxy_t *wgxy = pw->data;
    /* add decrementing reference counters */
    sui_graph_data_dec_refcnt(wgxy->data);
    sui_dinfo_dec_refcnt(wgxy->curx);
    sui_dinfo_dec_refcnt(wgxy->cury);
    sui_dinfo_dec_refcnt(wgxy->originx);
    sui_dinfo_dec_refcnt(wgxy->originy);
    sui_dinfo_dec_refcnt(wgxy->rangex);
    sui_dinfo_dec_refcnt(wgxy->rangey);

    free(wgxy);
    pw->data = NULL;
  }
  pw->type = SUWT_GENERIC;
  pw->hevent = sui_widget_hevent; // NULL;
}



SUI_CANBE_CONST
sui_obj_signal_tinfo_t sui_wgraphxy_signal_tinfo[] = {
//  { "statechanged", SUI_SIG_STATECHANGED, &sui_args_tinfo_int},
};

SUI_CANBE_CONST
sui_obj_slot_tinfo_t sui_wgraphxy_slot_tinfo[] = {
/*  { "toggle", SUI_SLOT_TOGGLE, &sui_args_tinfo_void,
    .target_kind = SUI_STGT_METHOD_OFFSET,
    .target.method_offset=UL_OFFSETOF( sui_wbitmap_vmt_t, toggle)},
*/
};

SUI_CANBE_CONST
sui_obj_tinfo_t sui_wgraphxy_vmt_obj_tinfo = {
  .name = "sui_wgraphxy",
  .obj_size = sizeof(sui_widget_t),
  .obj_align = UL_ALIGNOF(sui_widget_t),
  .signal_count = MY_ARRAY_SIZE(sui_wgraphxy_signal_tinfo),
  .signal_tinfo = UL_CAST_UNQ1(sui_obj_signal_tinfo_t *, sui_wgraphxy_signal_tinfo),
  .slot_count = MY_ARRAY_SIZE(sui_wgraphxy_slot_tinfo),
  .slot_tinfo = UL_CAST_UNQ1(sui_obj_slot_tinfo_t *, sui_wgraphxy_slot_tinfo)
};

sui_wgraphxy_vmt_t sui_wgraphxy_vmt_data = {
  .vmt_size = sizeof(sui_wgraphxy_vmt_t),
  .vmt_refcnt = SUI_STATIC,
  .vmt_parent = &sui_widget_vmt_data,
  .vmt_obj_tinfo = UL_CAST_UNQ1(sui_obj_tinfo_t *, &sui_wgraphxy_vmt_obj_tinfo),
  .vmt_init = sui_wgraphxy_vmt_init,
  .vmt_done = sui_wgraphxy_vmt_done,

//  .toggle = sui_wbutton_toggle,

};

/******************************************************************************/
/**
 * sui_wgraphxy - Create graphXY widget
 * @aplace: Button position and size.
 * @alabel: Button label in utf8 string.
 * @astyle: Button widget style.
 * @aflags: Widget common and button special flags.
 * @aimage: Pointer to an image used in widget.
 * @amask: Pointer to an image used as mask in widget.
 *
 * The function creates bitmap widget, increment reference
 * counters of used components and sets its fields.
 * aname, aimage and amask can be NULL.
 * Return Value: The function returns pointer to
 * created widget or NULL.
 *
 * Return Value: The function returns pointer to created button widget.
 * File: sui_wbitmap.c
 */
sui_widget_t *sui_wgraphxy(sui_rect_t *aplace, utf8 *alabel, sui_style_t *astyle,
              unsigned long aflags /* TODO: all necessary arguments for graphXY */)
{
  sui_widget_t *pw;
  pw = sui_widget_create_new(&sui_wgraphxy_vmt_data, aplace, alabel, astyle, aflags);
  if (!pw) return NULL;

  /* TODO: set all necessary atributes */

  return pw;
}
