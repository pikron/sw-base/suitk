/* sui_geom.h
 *
 * SUITK geometry - header file
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#ifndef _SUITK_GEOM_H_
  #define _SUITK_GEOM_H_

#include <suitk/sui_coordtype.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * struct sui_point - Point position structure
 * @x: Horizontal coordinate of a point.
 * @y: Vertical coordinate of a point.
 *
 * File: sui_gdi.h
 */
typedef struct sui_point {
  sui_coordinate_t x; /* horizontal position */
  sui_coordinate_t y; /* vertical position */
} sui_point_t;

/**
 * struct sui_rect - Rectange position and dimensions structure
 * @x: Horizontal position of a rectangle top left corner.
 * @y: Vertical position of a rectangle top left corner.
 * @w: Width of a rectangle.
 * @h: Height of a rectangle.
 *
 * File: sui_gdi.h
 */
typedef struct sui_rect {
  sui_coordinate_t x;  /* left side (x) */
  sui_coordinate_t y;  /* top of rect (y) */
  sui_coordinate_t w;  /* width of rect */
  sui_coordinate_t h;  /* height of rect */
} sui_rect_t;

/**
 * struct sui_textdims - Text dimensions structure
 * @w: Text width.
 * @h: Text height.
 * @b: Text base line from top of text.
 *
 * File: sui_gdi.h
 */
typedef struct sui_textdims {
  sui_coordinate_t w; /* width */
  sui_coordinate_t h; /* height */
  sui_coordinate_t b; /* base line */
} sui_textdims_t;

sui_coordinate_t sui_rect_to_point_n1dist(const sui_rect_t *r, const sui_point_t *p);

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /*_SUITK_GEOM_H_*/
