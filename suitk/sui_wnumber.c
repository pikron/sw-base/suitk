/* sui_wnumber.c
 *
 * SUITK number widget.
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#include "sui_wnumber.h"

#include <ul_log.h>

UL_LOG_CUST(ulogd_suitk)

/******************************************************************************
 * Number widget slot functions
 ******************************************************************************/


/******************************************************************************/
/* internal function */
/**
 * sui_number_read_value - reads valid/edited number linked with widget
 * @widget:  Pointer to a number widget.
 * @buf:     Output buffer for the read number.
 * @editing: Read current validated or edited number.
 *
 * The function reads the number connected to the number widget. According to
 * $editing argument it reads validated number from dinfo or number from internal buffer.
 * If the $editing argument is set to non-zero value the function reads currently edited number
 * otherwise it reads validated (original) number.
 * Return: The function returns SUI_RET_OK or error if it occurs.
 * File: sui_wnumber.c
 */
int sui_number_read_value(sui_widget_t *widget, long *buf, int editing)
{
  suiw_number_t *wnum = sui_numwdg(widget);
  if ((editing && !sui_test_flag(widget, SUNF_ONLINEEDIT)) ||
      (!editing && sui_test_flag(widget, SUNF_ONLINEEDIT))) { /* return value from intvalue */
    *buf = wnum->intvalue;
  } else {                                                      /* return value directly from dinfo */
    return sui_rd_long(wnum->data, 0, buf);
  }
  return SUI_RET_OK;
}

/**
 * sui_number_write_value - writes number to the valid/edited number linked with widget
 * @widget:  Pointer to a number widget.
 * @buf:     Input buffer with number to writing.
 * @editing: Write number as validated or edited number
 *
 * The function writes the number connected to the number widget. According to
 * $editing argument it writes a number to number in dinfo or number in internal buffer.
 * If the $editing argument is set to non-zero value the function writes currently edited number
 * otherwise it writes validated (original) number.
 * Return: The function returns SUI_RET_OK or error if it occurs.
 * File: sui_wnumber.c
 */
int sui_number_write_value(sui_widget_t *widget, long *buf, int editing)
{
  suiw_number_t *wnum = sui_numwdg(widget);
  if ((editing && !sui_test_flag(widget, SUNF_ONLINEEDIT)) ||
      (!editing && sui_test_flag(widget, SUNF_ONLINEEDIT))) { /* save value to intvalue */
    wnum->intvalue = *buf;
  } else {                                                      /* save value directly to dinfo*/
    return sui_wr_long(wnum->data, 0, buf);
  }
  return SUI_RET_OK;
}

/**
 * sui_number_prepare_number - transforms number to string and sets sizes of its parts to wnumber structure 
 * @widget: pointer to widget
 * If n_int and o_dec don't equal between integer and decimal parts is decimal point (character or string)
 * Return: The function returns SUI_RET_OK if number was transformed successfully
 * File: sui_wnumber.c
 */
int sui_number_prepare_number(sui_widget_t *widget)
{
  suiw_number_t *wnum = sui_numwdg( widget);
  long value = 0, digit;
  char buffer[100];
  int minus, i, j, ddig, ret;
  int base = 10;
  sui_dinfo_t *di = wnum->data; /* for some information type,min,max,fdigits */

  /* clear previous transformation */
  if (wnum->str_number) {
    sui_free(wnum->str_number);
    wnum->str_number = NULL;
    wnum->nint = 0;
    wnum->ndec = 0;
  }

/* read and transform number to string */
  if (sui_test_flag(widget, SUFL_EDITED)) {
    ret = sui_number_read_value(widget, &value, sui_test_flag(widget, SUFL_EDITED || (wnum->eflg & SUNEF_INIT)));
  } else {
    ret = sui_rd_long(wnum->data, 0, &value);
  }
  if (ret!=SUI_RET_OK) {
    /* something is wrong ... show '!!!' */
    wnum->str_number = strdup("!!!");
    wnum->nint = 3; wnum->ndec = 0;
//    goto wnumber_transform_end;
  } else {
//ul_logdeb("transformed number = %ld\n", value);
    if (sui_test_flag(widget, SUNF_SHOWLIMITS) && (value<di->minval || value>di->maxval)) {
      if (value<di->minval) {
        wnum->str_number = strdup("LOW");
        wnum->nint = 3; wnum->ndec = 0;
        goto wnumber_transform_end;
      } else {
        wnum->str_number = strdup("HIGH");
        wnum->nint = 4; wnum->ndec = 0;
        goto wnumber_transform_end;
      }
    } else {
      if (!sui_test_flag(widget, SUFL_EDITED) && !(wnum->eflg &SUNEF_INIT)) {
        if (sui_test_flag(widget, SUNF_CHECKLIMITS) && (value<di->minval || value>di->maxval)) {
          wnum->str_number = strdup("---");
          wnum->nint = 3; wnum->ndec = 0;
          goto wnumber_transform_end;
        }
      }
    }
/* transform number */
/* ?FIXME: now we suppose the tranfsormed number contains only ASCII characters */

    if (wnum->format && wnum->format->base) base = wnum->format->base;

    if (value<0) { /* minus */
      minus = 1;
      value = -value;
    } else
      minus = 0;
    i = 0;
    /* 1. transform number to digits and save them to buffer from index=50 */
    while (value) {
      digit = value % base; value = value / base;
      if ( digit <= 9) buffer[i+50] = digit + '0';
      else buffer[i+50] = digit + 'a' - 10;
      i++;
    }
    /* Test if value can be showed - not perfect */
    if (wnum->format->digits) {
      int k = i;
      if (wnum->data->fdigits) {
        if (k<wnum->data->fdigits)
          k=wnum->data->fdigits+1; /* 0.xxx */
        k++;
      }
      if (minus) k++;
      if (k>wnum->format->digits) {
        if (minus) {
          wnum->str_number = strdup("-Over");
        } else {
          wnum->str_number = strdup("+Over");
        }
        wnum->nint = 5; wnum->ndec = 0;
        goto wnumber_transform_end;
      }
    }
    /* 2. fill and revert digits to right order */
    j = 0;
    if (sui_test_flag(widget, SUFL_EDITED) && !(wnum->eflg & SUNEF_INIT)) {
      /* what we know: i = number of digits converted as zeroed value, ndig = number of entered digits */
      /* ddig = number of entered decimal digits, MINUS = minus sign is needed, POINT = decimal point sign is need */
      /* digits = number of all showable digits - display capacity, fdigits = number of number decimal digits */

      if (wnum->eflg & SUNEF_MINUS) { /* show minus if it is needed */
        buffer[j++] = '-';
        wnum->nint++;
      }
      if (i<=wnum->data->fdigits) { /* fill zero if it is needed (i <= fdigits) */
        buffer[j++] = '0';
        wnum->nint++;
      } else {                      /* show integer digits (i > fdigits) */
        while(i>wnum->data->fdigits) {
          buffer[j++] = buffer[--i+50];
          wnum->nint++;
        }
      }
      if (wnum->eflg & SUNEF_POINT) { /* show decimal point (and digits) */
        int k;
        buffer[j++] = SUI_NLS_USE_NLS ? *((char *)SUI_NLS_POINT) : '.';
        wnum->nint++;

        ddig = (wnum->eflg & SUNEF_DMASK) >> SUNEF_DSHIFT;
        k = wnum->data->fdigits - i;
        while (ddig && (k-- > 0)) {
          buffer[j++] = '0';          /* fill zeros after decimal point */
          wnum->ndec++;
          ddig--;
        }
        while (ddig) {                /* show decimal digits */
          buffer[j++] = buffer[--i+50];
          wnum->ndec++;
          ddig--;
        }
      }
      buffer[j] = 0; /* ending zero */

      /* save translated number to widget internal buffer */
      wnum->str_number = strdup(buffer);

    } else {
      if (minus) {                    /* show minus mark */
        buffer[j++] = '-';
        wnum->nint++;
      }
      ddig = i - wnum->data->fdigits; /* number of integer digits (to zero value) */
      if (ddig<0) ddig = 0;
      if ((j < wnum->format->digits) && (sui_test_flag(widget,SUNF_FILLUPBYZERO) ||
          (wnum->eflg & SUNEF_INIT))) { /* fill-up number by zeros before valid digits */
        int maxint = wnum->format->digits;
        if (minus) maxint--; /* one digit is minus */
        if (wnum->data->fdigits)
          maxint -= wnum->data->fdigits+1;
        while(ddig<maxint) {
          buffer[j++] = '0';
          wnum->nint++;
          maxint--;
        }
      } else {
        if (!ddig) {                    /* show zero if number is only decimal */
          buffer[j++] = '0';
          wnum->nint++;
        }
      }
      while (ddig) {                    /* show integer part of number */
        buffer[j++] = buffer[--i+50];
        wnum->nint++;
        ddig--;
      }

/* show decimal point if flag HIDEPOINT isn't used or widget is in INIT phase and */
/*   DECIMALZEROS or one of buffer[50-50-fdigits] isn't zeros */
      ddig = 0; minus = 0;
      while((ddig<wnum->data->fdigits) && (ddig<i)) {
        if (buffer[ddig+50]!='0') { minus=1; break;}
        ddig++;
      }
//ul_logdeb("i=%d,ddig=%d,minus=%d,fdig=%d,\n",i,ddig,minus,wnum->data->fdigits);
      if ((!sui_test_flag(widget, SUNF_HIDEPOINT) &&
          ((sui_test_flag(widget, SUNF_DECIMALZEROS) && wnum->data->fdigits)|| (minus)))
          || ((wnum->eflg & SUNEF_INIT) && (wnum->data->fdigits>0))) {       /* show decimal point */
//ul_logdeb("wnumber - decimalzeros (i=%d)\n",i);
        buffer[j++] = SUI_NLS_USE_NLS ? *((char *)SUI_NLS_POINT) : '.';
        wnum->nint++;
      }
      if((i || (!i && sui_test_flag(widget, SUNF_DECIMALZEROS)) || (wnum->eflg & SUNEF_INIT)) && (i<wnum->data->fdigits)) { /* decimal part of number must filled-up by zeros after decimal point and before digits */
        int k = wnum->data->fdigits;
        while(k>i) {
          buffer[j++] = '0';
          wnum->ndec++;
          k--;
        }
      }
      while(i>ddig) {                  /* show decimal part of number */
        buffer[j++] = buffer[--i+50];
        wnum->ndec++;
      }
      if (sui_test_flag(widget, SUNF_DECIMALZEROS) && wnum->data->fdigits) { /* show all decimal zeros after digits */
        while(i) {
          buffer[j++] = buffer[--i+50];
          wnum->ndec++;
        }
      }

      buffer[j] = 0; /* ending zero */
      wnum->str_number = strdup(buffer);
    }
  }

wnumber_transform_end:
  return SUI_RET_OK;
}

/**
 * sui_wnumber_get_dims - collects dimensions of all number parts
 * @dc: pointer to device context
 * @widget: pointer to wnumber widget
 * @pdims: pointer to structure with dimensions
 * The function collects all needed dimensions of number (and suffix) and fills $pdims structure.
 * Return Value: The function doesn't return any value.
 * File: sui_wnumber.c
 */
void sui_wnumber_get_dims(sui_dc_t *dc, sui_widget_t *widget, sui_wnumber_dims_t *pdims)
{
  sui_coordinate_t bint = 0, bdec = 0, bsuf = 0, tmp;
  sui_textdims_t tsize;
  sui_dcfont_t *pdcf;
  suiw_number_t *wnum = sui_numwdg(widget);
  sui_align_t al;
  
  /* clear output buffer */
  memset(pdims, 0, sizeof(sui_wnumber_dims_t));
  /* obtain dimensions of integer part of number */
  al = wnum->align;
  pdcf = sui_font_prepare(dc, widget->style->fonti->font, widget->style->fonti->size);
  if (wnum->str_number && wnum->nint) {
    sui_text_get_size(dc, pdcf, &tsize, wnum->nint, (utf8 *)(wnum->str_number), al);
    pdims->integer.x = tsize.w;
    pdims->integer.y = tsize.h;
    bint = tsize.b;
  }
  /* obtain dimensions of decimal part of number */
  if (wnum->str_number && wnum->ndec) {
    if (wnum->suffix) {
      if (wnum->suffix->decfonti)
        pdcf = sui_font_prepare(dc, wnum->suffix->decfonti->font, wnum->suffix->decfonti->size);
      al = (sui_align_t)(wnum->align ^ (wnum->suffix->decalign & SUAL_COLUMN));
    }
    sui_text_get_size(dc, pdcf, &tsize, wnum->ndec, (utf8 *)(wnum->str_number + wnum->nint), al);
    pdims->decimal.x = tsize.w;
    pdims->decimal.y = tsize.h;
    bdec = tsize.b;
  }
  /* obtain dimensions of suffix part of number */
  if (wnum->suffix && wnum->suffix->suffix && sui_test_flag(widget, SUNF_SUFFIX)) {
    if (wnum->suffix->suffonti)
      pdcf = sui_font_prepare(dc, wnum->suffix->suffonti->font, wnum->suffix->suffonti->size);
    al = (sui_align_t)(wnum->align ^ (wnum->suffix->sufalign & SUAL_COLUMN));
    sui_text_get_size(dc, pdcf, &tsize, -1, wnum->suffix->suffix, al);
    pdims->suffix.x = tsize.w;
    pdims->suffix.y = tsize.h;
    bsuf = tsize.b;
  }
  /* compute wnumber full size */
  /* compute positions of wnumber parts */
  pdims->number.b = (bint > bdec) ? ((bint > bsuf) ? bint : bsuf) : ((bdec > bsuf) ? bdec : bsuf);
  if (wnum->align & SUAL_COLUMN) {
    pdims->number.w = (pdims->integer.x > pdims->decimal.x) ?
      ((pdims->integer.x > pdims->suffix.x) ? pdims->integer.x : pdims->suffix.x) :
      ((pdims->decimal.x > pdims->suffix.x) ? pdims->decimal.x : pdims->suffix.x);
    pdims->number.h = pdims->integer.y + pdims->decimal.y + pdims->suffix.y;
    switch(wnum->align & SUAL_HORMASK) {
      case SUAL_CENTER:
        pdims->integer.x = (pdims->number.w-pdims->integer.x)/2;
        pdims->decimal.x = (pdims->number.w-pdims->decimal.x)/2;
        pdims->suffix.x = (pdims->number.w-pdims->suffix.x)/2;
        break;
      case SUAL_LEFT:
        pdims->integer.x = 0;
        pdims->decimal.x = 0;
        pdims->suffix.x = 0;
        break;
      case SUAL_RIGHT:
        pdims->integer.x = pdims->number.w-pdims->integer.x;
        pdims->decimal.x = pdims->number.w-pdims->decimal.x;
        pdims->suffix.x = pdims->number.w-pdims->suffix.x;
        break;
      case SUAL_BLOCK:
        pdims->integer.x = (pdims->number.w-pdims->integer.x)/2;
        pdims->decimal.x = 0;
        pdims->suffix.x = 0;
        break;
    }
    pdims->suffix.y = pdims->decimal.y + pdims->integer.y;
    pdims->decimal.y = pdims->integer.y;
    pdims->integer.y = 0;
  } else {
    pdims->number.w = pdims->integer.x + pdims->decimal.x + pdims->suffix.x;
    pdims->number.h = (pdims->integer.y > pdims->decimal.y) ?
      ((pdims->integer.y > pdims->suffix.y) ? pdims->integer.y : pdims->suffix.y) :
      ((pdims->decimal.y > pdims->suffix.y) ? pdims->decimal.y : pdims->suffix.y);
    switch(wnum->align & SUAL_VERMASK) {
      case SUAL_CENTER:
        pdims->integer.y = (pdims->number.h-pdims->integer.y)/2;
        break;
      case SUAL_TOP:
        pdims->integer.y = 0;
        break;
      case SUAL_BOTTOM:
        pdims->integer.y = pdims->number.h-pdims->integer.y;
        break;
      case SUAL_BASELINE:
        tmp = bint;
        if (!(wnum->suffix->decalign & SUAL_COLUMN) && (bdec > tmp))
          tmp = bdec;
        if (!(wnum->suffix->sufalign & SUAL_COLUMN) && (bsuf > tmp))
          tmp = bsuf;
        
        pdims->integer.y = tmp - bint;
        break;
    }
    if (wnum->suffix) {
      switch(wnum->suffix->decalign & SUAL_VERMASK) {
        case SUAL_CENTER:
          pdims->decimal.y = (pdims->number.h-pdims->decimal.y)/2;
          break;
        case SUAL_TOP:
          pdims->decimal.y = 0;
          break;
        case SUAL_BOTTOM:
          pdims->decimal.y = pdims->number.h-pdims->decimal.y;
          break;
        case SUAL_BASELINE:
          pdims->decimal.y = pdims->integer.y + bint - bdec;
          break;
      }
      switch(wnum->suffix->decalign & SUAL_VERMASK) {
        case SUAL_CENTER:
          pdims->suffix.y = (pdims->number.h-pdims->suffix.y)/2;
          break;
        case SUAL_TOP:
          pdims->suffix.y = 0;
          break;
        case SUAL_BOTTOM:
          pdims->suffix.y = pdims->number.h-pdims->suffix.y;
          break;
        case SUAL_BASELINE:
          pdims->suffix.y = pdims->integer.y + bint - bsuf;
          break;
      }
    } else {
      pdims->decimal.y = pdims->integer.y;
    }
    pdims->suffix.x = pdims->decimal.x + pdims->integer.x + 1; /* add one pixel gap */
    pdims->decimal.x = pdims->integer.x;
    pdims->integer.x = 0;
  }
}


/******************************************************************************
 * Number basic functions
 ******************************************************************************/
/**
 * sui_wnumber_get_size - gets size of the number widget content
 * @dc:     Pointer to device context
 * @widget: Pointer to number widget
 * @point:  Pointer to output buffer of point structure
 * The function determines size of number widget content and it is reaction to
 * the SUCM_GETSIZE command event.
 * Return: The function returns 1 if it ends successfully or -1 if an error occurs.
 * File: sui_wnumber.c
 */
//inline
int sui_wnumber_get_size(sui_dc_t *dc, sui_widget_t *widget, sui_point_t *point)
{
  sui_wnumber_dims_t dims;

  if (!point || !widget || !widget->style || !widget->style->fonti) return -1;

  if (sui_number_prepare_number(widget)!=SUI_RET_OK)
    return -1;
/* FIXME: add correction of number for edited widget - umazat cislice do editovane pozice */
  
  sui_wnumber_get_dims(dc, widget, &dims);
  
  point->x = dims.number.w;
  point->y = dims.number.h;
  return 1;
}

/**
 * sui_wnumber_auto_size - sets widget size according to its content
 * @widget: pointer to widget structure
 * @dc: pointer to device context
 *
 * The function sets widgeet size according to dimmensions of its content.
 *
 * Return Value: The function returns -1 if an error occured, 0 if function processed correctly but
 * it was unsuccessful and 1 if function was successful.
 * File: sui_wnumber.c
 */
static inline
int sui_wnumber_auto_size(sui_widget_t *widget, sui_dc_t *dc)
{
  sui_point_t size;
  sui_widget_compute_size(dc, widget, &size);
  widget->place.w = size.x; widget->place.h = size.y;
  return 1;
}

/**
 * sui_wnumber_init - Number widget reaction to SUCM_INIT event
 * @widget: Pointer to a number widget.
 * @dc:     Pointer to device context.
 *
 * The function initializes the number widget.
 *
 * Return: The function does not return value.
 * File: sui_wnumber.c
 */
static inline
void sui_wnumber_init(sui_widget_t *widget, sui_dc_t *dc)
{
  /* set default values */
  sui_numwdg(widget)->align |= SUAL_INSIDE;
  /* initiate step size */
  if (sui_numwdg(widget)->format && (sui_numwdg(widget)->format->litstep)) /* if litstep is negative arrowkeys will be switched */
    sui_numwdg(widget)->step = sui_numwdg(widget)->format->litstep;
  else
    sui_numwdg(widget)->step = 1;
  
  if (widget->place.w<0 || widget->place.h<0) {
    sui_point_t size;
    if (!sui_test_flag(widget, SUFL_AUTORESIZE)) { /* not autoresize */
      unsigned long wflags = widget->flags;
      sui_numwdg(widget)->eflg = SUNEF_INIT;
//      widget->flags |= SUNF_DECIMALZEROS | SUNF_FILLUPBYZERO; // SUFL_EDITEN | SUFL_EDITED | 
      sui_widget_compute_size( dc, widget, &size);
      sui_numwdg(widget)->eflg = 0;
      widget->flags = wflags;
    } else {
      sui_widget_compute_size(dc, widget, &size);
    }
    if (widget->place.w < 0) widget->place.w = size.x;
    if (widget->place.h < 0) widget->place.h = size.y;
  }
}


/**
 * sui_wnumber_draw - Number widget reaction to SUEV_DRAW event
 * @dc:     Pointer to device context.
 * @widget: Pointer to number widget.
 *
 * The function draws content of a number widget.
 *
 * Return: The function returns a negative value if an error occurs,
 * zero if event wasn't processed and one if event was processed successfully.
 * File: sui_wnumber.c
 */
static inline
int sui_wnumber_draw(sui_dc_t *dc, sui_widget_t *widget)
{
  sui_dcfont_t *pdcf;
  sui_point_t origin;
  sui_wnumber_dims_t dims;
  suiw_number_t *wnum;
  sui_align_t al;

  if (!widget || !widget->style || !widget->style->fonti) return -1;
  wnum = sui_numwdg(widget);

  if (sui_number_prepare_number(widget)!=SUI_RET_OK)
    return -1;
/* FIXME: add correction of number for edited widget - umazat cislice do editovane pozice */
  
  sui_wnumber_get_dims(dc, widget, &dims);

  {
    sui_point_t box;
    sui_point_t obj;
    box.x = widget->place.w;
    box.y = widget->place.h;
    obj.x = dims.number.w;
    obj.y = dims.number.h;
    sui_align_object(&box, &origin, &obj, dims.number.b,
                      widget->style, wnum->align);
  }

  sui_set_widget_fgcolor(dc, widget, SUC_ITEM);
  sui_set_widget_bgcolor(dc, widget, SUC_BACKGROUND);

  /* set defualt font for every part */
  pdcf = sui_font_prepare(dc, widget->style->fonti->font,
                          widget->style->fonti->size);
  sui_font_set(dc, pdcf);
  al = wnum->align;

  sui_draw_text(dc, origin.x + dims.integer.x, origin.y + dims.integer.y,
                wnum->nint, (utf8 *)(wnum->str_number), al);

  if (wnum->str_number && wnum->ndec) {
    if (wnum->suffix) {
      if (wnum->suffix->decfonti) {
        pdcf = sui_font_prepare(dc, wnum->suffix->decfonti->font,
                                wnum->suffix->decfonti->size);
        sui_font_set(dc, pdcf);
      }
      al = (sui_align_t)(wnum->align ^ (wnum->suffix->decalign & SUAL_COLUMN));
    }
    sui_draw_text(dc, origin.x + dims.decimal.x, origin.y + dims.decimal.y,
                  wnum->ndec, (utf8 *)(wnum->str_number + wnum->nint), al);
  }

  if (wnum->suffix && wnum->suffix->suffix && sui_test_flag(widget, SUNF_SUFFIX)) {
    if (wnum->suffix->suffonti) {
      pdcf = sui_font_prepare(dc, wnum->suffix->suffonti->font,
                              wnum->suffix->suffonti->size);
      sui_font_set(dc, pdcf);
    }
    al = (sui_align_t)(wnum->align ^ (wnum->suffix->sufalign & SUAL_COLUMN));
    sui_draw_text(dc, origin.x + dims.suffix.x, origin.y + dims.suffix.y,
                  -1, wnum->suffix->suffix, al);
  }

//sui_draw_rect(dc, origin.x, origin.y, dims.number.w, dims.number.h, 1);
/* draw frame as indicator of BadValue mode */
  if (wnum->eflg & SUNEF_BVMODE) { /* indicate Bad Value */
    sui_draw_rect(dc, 1, 1, widget->place.w-2, widget->place.h-2, 1);
  }

  return 1;
}


/*
 * sui_number_start_edit - prepares widget number to editing
 * @widget: pointer to widget
 * @zero:   clear edited number to zero
 * Return: The function returns zero if there isn't any error.
 */
int sui_wnumber_start_edit(sui_widget_t *widget, int zero)
{
  suiw_number_t *wnum = sui_numwdg(widget);
  if (!sui_test_flag(widget, SUFL_EDITEN)) return 1;
    
  if (!sui_test_flag(widget, SUFL_EDITED)) { /* number isn't edited yet */
/* save current value from dinfo to intvalue - it's the same operation for both cases DIRECTLY/INTERNAL EDIT */
    if (sui_rd_long(wnum->data, 0, &wnum->intvalue) != SUI_RET_OK)
      wnum->intvalue = 0; //return -1;
    if (zero) {
      long zerovalue = 0;
      if (sui_number_write_value(widget, &zerovalue, 1) != SUI_RET_OK)
        return -1;
      wnum->eflg = 0;
    }
    sui_set_flag(widget, SUFL_EDITED);
    sui_obj_emit((sui_obj_t *)widget, SUI_SIG_EDIT_STARTED);
  }
  return 0;
}

/*
 * sui_number_stop_edit - close editing on widget number
 * @widget:    pointer to number widget
 * @confirmed: edited number is validated and vil be saved otherwise it will be throw away
 * Return: The function returns zero if there isn't any error.
 */
int sui_wnumber_stop_edit(sui_widget_t *widget, int confirmed)
{
  suiw_number_t *wnum = sui_numwdg(widget);
  sui_event_t e;
  int ret = 0;

//ul_logdeb("%c(%d):val=%ld, intval=%ld\n",sui_test_flag(widget, SUNF_ONLINEEDIT)?'O':' ', confirmed,*(long*)wnum->data->ptr,wnum->intvalue);
  if (!sui_test_flag(widget, SUFL_EDITED)) return 1;
  e.what = SUEV_COMMAND;
  e.message.ptr = widget;
  if (confirmed) {
    if (!sui_test_flag(widget, SUNF_ONLINEEDIT)) {
      if (sui_wr_long(wnum->data, 0, &wnum->intvalue) != SUI_RET_OK) {
        e.message.command = SUCM_ERR_WRVAL;
        ret = -1;
      }
    }
    if (ret>=0) {
      e.message.command = SUCM_CHANGED;
      //  sui_call_callback( widget, SUCF_CHANGEVALUE, &(wnum->intvalue));
    }
  } else {
    if (sui_test_flag( widget, SUNF_ONLINEEDIT)) {
      if (sui_wr_long( wnum->data, 0, &wnum->intvalue) != SUI_RET_OK) {
        e.message.command = SUCM_ERR_WRVAL;
        ret = -1;
      }
    }
    if (ret>=0) {
      e.message.command = SUCM_CHANGE;
    }
  }
  sui_put_event(&e); /* SUCM_ERROR_WRVAL */
  wnum->eflg = 0;
  sui_clear_flag(widget, SUFL_EDITED);
  sui_obj_emit((sui_obj_t *)widget, SUI_SIG_EDIT_STOPPED);

  return ret;
}

/*
 * sui_wnumber_clear_value - set value to 
 */
int sui_wnumber_clear_value(sui_widget_t *widget)
{
  long zero = 0;
  suiw_number_t *wnum = sui_numwdg(widget);
  if (!wnum->data) return SUI_RET_ERR;
  if (sui_test_flag(widget,SUNF_MUSTBEVALID)) { /* correction if value must be checked */
    if (zero < wnum->data->minval)
      zero = wnum->data->minval;
    else if (zero > wnum->data->maxval)
      zero = wnum->data->maxval;
  }
  return sui_number_write_value(widget, &zero, 0);
}

long sui_wnumber_unedit_value(suiw_number_t *wnum, long value, long base)
{
  int i = (wnum->eflg & SUNEF_DMASK) >> SUNEF_DSHIFT;
  i = wnum->data->fdigits - i;
  while(i--) value *= base;
  if (wnum->eflg & SUNEF_MINUS) value = -value;
  return value;
}

/*
 * sui_wnumber_check_limits - checks if value is inside dinfo range
 */
int sui_wnumber_check_limits(sui_widget_t *widget, long value, int edit)
{
  suiw_number_t *wnum = sui_numwdg(widget);
  if ((value < wnum->data->minval) || (value > wnum->data->maxval)) {
    if ((edit && sui_test_flag(widget,SUNF_MUSTBEVALID)) ||
        (!edit && sui_test_flag(widget,SUNF_CHECKLIMITS))) {
      if (value < wnum->data->minval) {
ul_logdeb("check - EOORN\n");
        return SUI_RET_EOORN;
      } else {
ul_logdeb("check - EOORP\n");
        return SUI_RET_EOORP;
      }
    }
  }
ul_logdeb("check - OK\n");
  return SUI_RET_OK;
}

int sui_wnumber_edit_value(suiw_number_t *wnum, long value, int base)
{
  int ncnt=0;
  /* correct MINUS edit flag */
  if (value<0) {
    wnum->eflg |= SUNEF_MINUS;
    value = -value;
  } else
    wnum->eflg &= ~SUNEF_MINUS;
  /* correct POINT edit flag */
  if (wnum->data->fdigits>0)
    wnum->eflg |= SUNEF_POINT;
  else
    wnum->eflg &= ~SUNEF_POINT;
  
  while (value) {
    value = value / base;
    ncnt++;
  }
  /* correct number of decimal digits */
  wnum->eflg = (wnum->eflg & ~SUNEF_DMASK) | (wnum->data->fdigits * SUNEF_DPLUS);
  /* correct number of integer digits */
  if (ncnt < wnum->data->fdigits)
    ncnt = wnum->data->fdigits + 1;
  wnum->eflg = (wnum->eflg & ~SUNEF_NMASK) | (ncnt * SUNEF_NPLUS);
  return 0;
}

/**
 * sui_wnumber_hkey - Reaction to KEY_DOWN, KEY_UP events incoming to a number widget
 * @widget: Pointer to number widget.
 * @event: Pointer to the event.
 *
 * The function is response to keyboard events in a number widget.
 *
 * Return Value: The function returns 0 if it takes to event.
 * File: sui_wnumber.c
 */
//inline
int sui_wnumber_hkey(sui_widget_t *widget, sui_event_t *event)
{
  unsigned short key = event->keydown.keycode;
//  unsigned short kmod = event->keydown.modifiers;
  suiw_number_t *wnum = sui_numwdg(widget);

  int base = 10;
  int somekey = 0;
//  int limits;
  long value, newval = -1; //, addval = 0;
// // 	sui_event_t e;
//  limits = (wnum->data->minval!=0 || wnum->data->maxval!=0);

//ul_logdeb("Wnumber KEY : %s\n",(event->what==SUEV_KDOWN)?"KDOWN":((event->what==SUEV_KUP)?"KUP":"???"));

  if (wnum->format && wnum->format->base) base = wnum->format->base;
  if (!wnum->data) return 0;

/* after this part - newval will contain value <0,base) or -1 if bad key was pressed */
  if (sui_test_flag(widget, SUFL_EDITEN)) {
    if (( key >= '0' && key <= '9') || ( key >= 'a' && key <= 'z') ||
        ( key >= 'A' && key <= 'Z')) {
      if ( key > '9') {
        if ( key >= 'a') newval = key - 'a' + 10;
        else newval = key -'A' + 10;
      } else
        newval = key - '0';
      if ( newval >= base)
        newval = -1;
      else
        somekey = 1;
    } else if (key=='.' || key=='-' || key==MWKEY_ENTER ||
        key==MWKEY_DELETE || key==MWKEY_BACKSPACE) { /* editing by number or ?ENTER? or DELETE/BACKSPACE(clear entire number) */
      somekey = 1;
    } else {
      if (sui_test_flag(widget, SUNF_ARROWKEY)) {
        if ((key==MWKEY_RIGHT)||(key==MWKEY_LEFT)||(key==MWKEY_UP)||(key==MWKEY_DOWN)||
            (key==MWKEY_PAGEUP)||(key==MWKEY_PAGEDOWN)||(key==MWKEY_HOME)||
            (key==MWKEY_END)) { /* editing with arrows */
          somekey = 2;
        }
      }
    }
  }


/* branches by widget state */
  if (!sui_test_flag(widget, SUFL_EDITED)) { /* NORMAL state */
    if (!sui_test_flag(widget, SUFL_EDITEN)) { /* widget where editing isn't allowed */
      if ((key==MWKEY_DELETE || key==MWKEY_BACKSPACE) &&
          sui_test_flag(widget, SUNF_CLEARNOEDITEN)) { /* clear number (zero or minimum/maximum)*/
        return ((sui_wnumber_clear_value(widget)==SUI_RET_OK) ? 1 : -1);
      }
    } else { /* EDITEN widget bud not EDITED */
      if (somekey) {
        if (key!=MWKEY_ENTER && somekey!=2)
          sui_wnumber_start_edit(widget, 1);
        else
          sui_wnumber_start_edit(widget, 0);
      }
    }
  }


  if (sui_test_flag(widget, SUFL_EDITED)) { /* number is in EDIT or BADVAL mode */
    /* read currently edited value */
    if (sui_number_read_value(widget, &value, 1)!= SUI_RET_OK) {
      return -1;
    }
ul_logdeb("Readed Value : %ld\n",value);
//    value = sui_wnumber_edit_value(value);
{
  int i;
  i = (wnum->eflg & SUNEF_DMASK) >> SUNEF_DSHIFT;
  i = wnum->data->fdigits - i;
  if (value<0) {
    wnum->eflg |= SUNEF_MINUS;
    value = -value;
  }
  while(i--)
    value /= base;
//  return value;
}

    if (wnum->eflg & SUNEF_BVMODE) {      /* number is in the BADVAL mode */
      if (key==MWKEY_ENTER) { /* confirm value which was changed to limit */
        sui_hevent_command( widget, SUCM_CONFIRM, widget, 0);
        return 1;
      } else if (key==MWKEY_ESCAPE) { /* discard automatically changed value */
        sui_hevent_command( widget, SUCM_DISCARD, widget, 0);
        return 1;
      } else {
        
        //        sui_wnumber_continue_edit(widget);
        {
          wnum->eflg &= ~(SUNEF_BVMODE | SUNEF_POINT | SUNEF_MINUS | SUNEF_NMASK | SUNEF_DMASK);
        //  sui_number_write_value(widget, &val, 1);
          value = 0; /* there will be problems perhaps - with MINUS,POINT,DMASK and NMASK */
        }

      }
    }

    //} else {                               /* number is in the EDIT mode */
    /* we can come from BVMODE - continue editing ... */
    if (!(wnum->eflg & SUNEF_BVMODE)) {

/* FIXME: variable step for repeating arrow keys */
      if (somekey==2) {
//ul_logmsg("arrow repeat = %d\n",event->keydown.repeats);
        int update = 0;
        long newvalue = 0;
        switch(key) {
          case MWKEY_RIGHT:
          case MWKEY_UP:   /* inc one */
            newvalue = wnum->step;
            update = 1;
            break;
          case MWKEY_LEFT:
          case MWKEY_DOWN: /* dec one */
            newvalue = -wnum->step;
            update = 1;
            break;
          case MWKEY_PAGEUP:
            newvalue = wnum->format->bigstep;
            break;
          case MWKEY_PAGEDOWN:
            newvalue = -wnum->format->bigstep;
            break;
        }
        if (update) {
          newvalue += sui_wnumber_unedit_value(wnum, value, base);
/* FIXME: long overflow isn't checked */
          if (sui_test_flag(widget, SUNF_MUSTBEVALID)) {
            if ((newvalue<wnum->data->minval)) { // (wnum->data->minval<=0) && 
              newvalue = wnum->data->minval;
              sui_hevent_command( widget, SUCM_EDITMIN, widget, 0);
            } else if ((newvalue>wnum->data->maxval)) { // (wnum->data->maxval>=0) && 
              newvalue = wnum->data->maxval;
              sui_hevent_command( widget, SUCM_EDITMAX, widget, 0);
            }
          }
          value = newvalue;
          sui_wnumber_edit_value(wnum, value, base);
          wnum->eflg |= SUNEF_ARROW;
          goto hkeywasprocessed;
        }
      }

      if (key==MWKEY_ESCAPE) {
        sui_hevent_command( widget, SUCM_DISCARD, widget, 0);
        return 1;
      } else if (key==MWKEY_ENTER) {
        int ret;
        value = sui_wnumber_unedit_value(wnum, value, base);
        ret = sui_wnumber_check_limits(widget, value, 1);
        if (ret == SUI_RET_OK) { /* value is inside */
          sui_hevent_command( widget, SUCM_CONFIRM, widget, 0);
          return 1;
        } else { /* it is outside - correct it and set BVMODE flag -> change edit mode to BADVALue mode */
          if (ret == SUI_RET_EOORN) { /* val < min */
            value = wnum->data->minval;
          } else { /* val > max */
            value = wnum->data->maxval;
          }
          sui_wnumber_edit_value(wnum, value, base);

          if (sui_number_write_value(widget, &value, 1)!= SUI_RET_OK) /* change value to */
            return -1;
          if (ret == SUI_RET_EOORN) {
            sui_hevent_command( widget, SUCM_EDITMIN, widget, 0);
          } else {
            sui_hevent_command( widget, SUCM_EDITMAX, widget, 0);
          }
          wnum->eflg |= SUNEF_BVMODE;
        }
        return 0;
      } else if (key=='.') {
        if (wnum->eflg & SUNEF_ARROW) {
          value = 0;
          wnum->eflg = 0;
        }
        if (wnum->data->fdigits>0 && !(wnum->eflg & SUNEF_POINT)) { /* add point */
          int filled = (wnum->eflg & SUNEF_NMASK) + ((wnum->eflg & SUNEF_MINUS)?1:0);
          if (filled<wnum->format->digits) {
            wnum->eflg |= SUNEF_POINT;
            goto hkeywasprocessed;
          }
        }
      } else if (key=='-') {
        if (sui_check_typecompat( wnum->data->tinfo, SUI_TYPE_LONG) == SUI_RET_OK) {
          if (wnum->eflg & SUNEF_ARROW) {
            value = 0;
            wnum->eflg = 0;
          }
          if (wnum->eflg & SUNEF_MINUS) { /* remove minus */
//ul_logdeb("remove minus\n");
            wnum->eflg &= ~SUNEF_MINUS;
            goto hkeywasprocessed;
          } else { /* add minus only if there is free space for it (finfo->digits) */
            int filled = (wnum->eflg & SUNEF_NMASK) + ((wnum->eflg & SUNEF_POINT)?1:0);
            if (filled<wnum->format->digits) {
//ul_logdeb("add minus\n");
              wnum->eflg |= SUNEF_MINUS;
              goto hkeywasprocessed;
            }
          }
        }
      } else if (newval>=0) { /* some new digit */
        int filled = (wnum->eflg & SUNEF_NMASK) + ((wnum->eflg & SUNEF_POINT)?1:0) + ((wnum->eflg & SUNEF_MINUS)?1:0);
        unsigned long lasteflg = wnum->eflg;
        long lastvalue = value;
        int updated = 0;
        if (wnum->eflg & SUNEF_ARROW) {
          value = 0;
          wnum->eflg = 0;
          filled = 0;
        }
        if (wnum->eflg & SUNEF_POINT) { /* added digit is added to decimal part */
          if (filled<wnum->format->digits) { /* new digits can be included into number */
            int dfill = (wnum->eflg & SUNEF_DMASK) >> SUNEF_DSHIFT;
            if (dfill<wnum->data->fdigits) { /* there is a free space for another decimal digits */
//ul_logdeb("add decimal digit %d (dfilled=%d,fdigits=%d)\n",newval,dfill,wnum->data->fdigits);
              value = value * base + newval;
              wnum->eflg += SUNEF_NPLUS + SUNEF_DPLUS;
              updated = 1;
            }
          }
        } else { /* digit is added to integer part */
          int dfill = wnum->format->digits;
          if (wnum->data->fdigits)
            dfill -= wnum->data->fdigits+1;
          if (filled<dfill) {
//ul_logdeb("add integer digit %d (filled=%d,digits=%d)\n",newval,filled,(wnum->format->digits-1-wnum->data->fdigits));
            value = value * base + newval;
            if (value) /* count digit only if value isn't zero (all previous digits were zeros) */
              wnum->eflg += SUNEF_NPLUS;
            updated = 1;
          }
        }

        if (updated) { /* number was updated */
          int bad = 0;
          if (sui_is_flag(widget,SUNF_ONLINEEDIT | SUNF_MUSTBEVALID,
              SUNF_ONLINEEDIT | SUNF_MUSTBEVALID)) {
            long newvalue = sui_wnumber_unedit_value(wnum, value, base);
            if ((wnum->data->minval<=0) && (newvalue<wnum->data->minval)) {
              bad = 1;
              value = lastvalue;
              wnum->eflg = lasteflg;
            } else if ((wnum->data->maxval>=0) && (newvalue>wnum->data->maxval)) {
              bad = 1;
              value = lastvalue;
              wnum->eflg = lasteflg;
            }
          }
          if (bad) {
            sui_hevent_command( widget, SUCM_EDITMAX, widget, 0);
            return 0;
          } else
            goto hkeywasprocessed;
        }
      } else if (key==MWKEY_DELETE || key==MWKEY_BACKSPACE) {
        if ((wnum->eflg & SUNEF_NMASK) || (wnum->eflg & SUNEF_POINT)) {
          if (wnum->eflg & SUNEF_DMASK) {
            value = value / base;
            wnum->eflg -= SUNEF_NPLUS + SUNEF_DPLUS;
          } else if (wnum->eflg & SUNEF_POINT) {
            wnum->eflg &= ~SUNEF_POINT;
          } else {
            value = value / base;
            wnum->eflg -= SUNEF_NPLUS;
          }
//ul_logdeb("digit deleted : %ld, (%d,%d,%c)\n", value,wnum->eflg & SUNEF_NMASK,(wnum->eflg & SUNEF_DMASK)>>SUNEF_DSHIFT,(wnum->eflg & SUNEF_POINT)?'P':' ');
          goto hkeywasprocessed;
        }
      }
    }
  }

  return 0;
hkeywasprocessed:
  value = sui_wnumber_unedit_value(wnum, value, base);
  if (sui_number_write_value(widget, &value, 1)!= SUI_RET_OK) /* change value to */
    return -1;
  return 1;
}


/******************************************************************************
 * Number event handler
 ******************************************************************************/
/**
 * sui_wnumber_hevent - Widget event handler for number widget
 * @widget: Pointer to the number widget.
 * @event: Pointer to the event.
 *
 * The function responses to event for number widget or calls
 * basic event handler common for all widget.
 * Return Value: The function does not return a value.
 * File: sui_wnumber.c
 */
void sui_wnumber_hevent( sui_widget_t *widget, sui_event_t *event)
{

  switch( event->what) {
    case SUEV_DRAW:
    case SUEV_REDRAW:
      sui_widget_draw( event->draw.dc, widget, sui_wnumber_draw);
      return;
    case SUEV_KDOWN:
      if ( sui_wnumber_hkey( widget, event)>0) {
        sui_clear_event( widget, event);
        sui_redraw_request( widget);
      }
    break;
    case SUEV_COMMAND:
      switch(event->message.command) {
        case SUCM_GETSIZE:
          if ( sui_wnumber_get_size( (sui_dc_t *)event->message.ptr,
              widget, (sui_point_t *)event->message.info)>0) {
            sui_clear_event( widget, event);
          }
          break;
        case SUCM_STARTEDIT:
          if (!sui_wnumber_start_edit(widget, event->message.info)) {
            sui_redraw_request(widget);
            sui_clear_event(widget, event);
          }
          break;
        case SUCM_CONFIRM:
          if (!sui_wnumber_stop_edit( widget, 1)) {
            sui_redraw_request( widget);
            sui_clear_event( widget, event);
          }
          break;
        case SUCM_DISCARD:
          if (!sui_wnumber_stop_edit( widget, 0)) {
            sui_redraw_request( widget);
            sui_clear_event( widget, event);
          }
          break;
        case SUCM_EDITBACKSPACE:
          {
            sui_event_t e;
            e.what = SUEV_KDOWN;
            e.keydown.keycode = MWKEY_BACKSPACE;
            if ( sui_wnumber_hkey( widget, &e)>0)
              sui_clear_event( widget, event);   /* event was changed to SUEV_KDOWN(MWKEY_BACKSPACE) !!! */
          }
          break;

        case SUCM_INIT:
          sui_widget_hevent(widget, event);
          sui_wnumber_init( widget, (sui_dc_t *)event->message.ptr);
          sui_clear_event( widget, event);
          return;
        case SUCM_FOCUSSET:
          if ( !sui_test_flag( widget, SUFL_EDITED)) { // && sui_test_flag( widget, SUNF_CONFIRMEDIT)
            //sui_numwdg(widget)->intflags |= SUNIF_CAMEFROMOUTSIDE;
            sui_numwdg(widget)->eflg = 0;
          } else {
//				long val;
// //				if (sui_number_read_value( widget, &val, 1/*SUI_EDITEDNUMBER*/)== SUI_RET_OK)
// //					sui_number_edit_position( widget, val);
          }
          break;
                    
        case SUCM_FOCUSREL:
          if ( sui_test_flag( widget, SUFL_EDITED) &&
               !sui_test_flag( widget, SUNF_CONFIRMEDIT)) {
            sui_hevent_command( widget, SUCM_DISCARD, widget, 0);
          }
          break;
        case SUCM_AUTORESIZE:
          sui_wnumber_auto_size( widget, (sui_dc_t *)event->message.ptr);
          return;
        default:
          break;
      }
      break;

    default:
      break;
  }
  if ( event->what != SUEV_NOTHING) sui_widget_hevent( widget, event);
}


/******************************************************************************
 * Number widget vmt & signal-slot mechanism
 ******************************************************************************/
/**
 * sui_wnumber_vmt_init - allocates and initializes number widget substructure
 * @pw:     pointer to widget structure
 * @params: pointer to optional parameters
 * The function allocates memory for number widget substructure and
 * sets widget structure as number widget.
 * Return Value: The function returns zero for successful processing
 * File: sui-wnumber.c
 */
int sui_wnumber_vmt_init( sui_widget_t *pw, void *params)
{
  pw->data = sui_malloc( sizeof( suiw_number_t));
  if(!pw->data)
    return -1;

  pw->type = SUWT_NUMBER;
  pw->hevent = sui_wnumber_hevent;

  return 0;
}

/**
 * sui_wnumber_vmt_done - releases and destroyes number widget substructure VMT
 * @pw: pointer to common widget structure
 * The function releases and destroyes number widget substructure.
 * Return Value: The function doesn't return any value
 * File: sui_wnumber.c
 */
void sui_wnumber_vmt_done( sui_widget_t *pw)
{
  if(pw->data) {
    suiw_number_t *wnum = pw->data;
  
    if ( wnum->format) sui_finfo_dec_refcnt( wnum->format);
    wnum->format = NULL;
    if ( wnum->data) sui_dinfo_dec_refcnt( wnum->data);
    wnum->data = NULL;
    if ( wnum->suffix) sui_suffix_dec_refcnt( wnum->suffix);
    wnum->suffix = NULL;
    if ( wnum->str_number) sui_free( wnum->str_number);
    wnum->str_number = NULL;
    wnum->nint = 0;
    wnum->ndec = 0;
  
    free( wnum);
    pw->data = NULL;
  }
  pw->type = SUWT_GENERIC;
  pw->hevent = sui_widget_hevent; // NULL;
}



SUI_CANBE_CONST
sui_obj_signal_tinfo_t sui_wnumber_signal_tinfo[] = {
  { "editstarted", SUI_SIG_EDIT_STARTED, &sui_args_tinfo_void},
  { "editstopped", SUI_SIG_EDIT_STOPPED, &sui_args_tinfo_void},
};

SUI_CANBE_CONST
sui_obj_slot_tinfo_t sui_wnumber_slot_tinfo[] = { 
//	{ "toggle", SUI_SLOT_TOGGLE, &sui_args_tinfo_void,
//		.target_kind = SUI_STGT_METHOD_OFFSET,
//		.target.method_offset=UL_OFFSETOF( sui_wbutton_vmt_t, toggle)},
};

SUI_CANBE_CONST
sui_obj_tinfo_t sui_wnumber_vmt_obj_tinfo = {
  .name = "sui_wnumber",
  .obj_size = sizeof(sui_widget_t),
  .obj_align = UL_ALIGNOF(sui_widget_t),
  .signal_count = MY_ARRAY_SIZE(sui_wnumber_signal_tinfo),
  .signal_tinfo = UL_CAST_UNQ1(sui_obj_signal_tinfo_t *, sui_wnumber_signal_tinfo),
  .slot_count = MY_ARRAY_SIZE(sui_wnumber_slot_tinfo),
  .slot_tinfo = UL_CAST_UNQ1(sui_obj_slot_tinfo_t *, sui_wnumber_slot_tinfo)
};

sui_wnumber_vmt_t sui_wnumber_vmt_data = {
  .vmt_size = sizeof(sui_wnumber_vmt_t),
  .vmt_refcnt = SUI_STATIC,
  .vmt_parent = &sui_widget_vmt_data,
  .vmt_init = sui_wnumber_vmt_init,
  .vmt_done = sui_wnumber_vmt_done,
  .vmt_obj_tinfo = UL_CAST_UNQ1(sui_obj_tinfo_t *, &sui_wnumber_vmt_obj_tinfo),
 
//  .toggle = sui_wbutton_toggle,
};

/******************************************************************************/
/**
 * sui_wnumber - Creates number widget
 * @aplace:  Pointer to widget place and size structure. (w,h can be set to -1 for automatic size on init)
 * @alabel:  Widget label string in utf8.
 * @astyle:  Pointer to widget style. (See to 'struct sui_style')
 * @aflags:  Widget flags. Common flags('SUFL_'prefix) ORed with number flags('SUNF_' prefix)
 * @adata:   Pointer to dinfo connected to the widget
 * @aformat: Pointer to number format of the dinfo.
 * @aalign:  Alignment of number inside the widget area.
 * @asuffix: Pointer sui_suffix structure which describes decimal digits and suffix string. (See to 'struct sui_suffig')
 *
 * The function creates number widget.
 *
 * Return Value: The function returns pointer to created number widget or NULL.
 * File: sui_wnumber.c
 */
sui_widget_t *sui_wnumber( sui_rect_t *aplace, utf8 *alabel, sui_style_t *astyle, 
              unsigned long aflags, sui_dinfo_t *adata, sui_finfo_t *aformat, 
              sui_align_t aalign, sui_suffix_t *asuffix)
{
  sui_widget_t *pw;

  pw = sui_widget_create_new( &sui_wnumber_vmt_data, aplace, alabel, astyle, aflags);
  if ( !pw) return NULL;

  {
    suiw_number_t *wnum = sui_numwdg(pw);

    if ( adata) {
      sui_dinfo_inc_refcnt( adata);
      wnum->data = adata;
    }
    if ( aformat) {
      sui_finfo_inc_refcnt( aformat);
      wnum->format = aformat;
      wnum->step = wnum->format->litstep;
    }
    wnum->align = aalign;
    if ( asuffix) {
      sui_suffix_inc_refcnt( asuffix);
      wnum->suffix = asuffix;
    }
  }
  return pw;
}
