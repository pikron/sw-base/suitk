/* sui_gdi.c
 *
 * SUITK graphics device interface.
 *
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#include "sui_gdi.h"
#include <stdio.h>
#include <suiut/support.h>

#include <ul_log.h>

UL_LOG_CUST(ulogd_suitk)

#ifndef MICROWINDOWS_VER_CODE /* undefined version of MICROWINDOWS */
    #error Version of MICROWINDOWS_VER_CODE must be defined
#endif

/* Provide backward compatibility even with ancient Microwindows versions */
#if (MICROWINDOWS_VER_CODE<VER_CODE(0,90,0))
  #define GdSetForegroundColor(psd, c) GdSetForeground(c)
  #define GdSetBackgroundColor(psd, c) GdSetBackground(c)
#endif /* MICROWINDOWS_VER_CODE<VER_CODE(0,90,0) */

/******************************************************************************
 * Common GDI functions
 ******************************************************************************/
/**
 * sui_gdi_set_drmode - Set drawing mode
 * @dc: Pointer to a device context.
 * @mode: New drawing mode.
 *
 * The function sets drawing mode to the device context.
 *
 * Return Value: The function does not return any value.
 *
 * File: sui_gdi.c
 */
void sui_gdi_set_drmode(sui_dc_t *dc, int mode)
{
  GdSetMode(mode);
}

/**
 * sui_gdi_set_usebgrd - Set using background
 * @dc: Pointer to a device context.
 * @usebgrd: Use background flag. (0=don't use background, 1=use background)
 *
 * The function sets using background for drawing objects in the device context.
 *
 * Return Value: The function does not return any value.
 *
 * File: sui_gdi.c
 */
void sui_gdi_set_usebgrd(sui_dc_t *dc, unsigned char usebgrd)
{
  GdSetUseBackground(usebgrd);
}


/**
 * sui_gdi_clear_screen - Clear entire screen
 * @dc: Pointer to a device context.
 *
 * The function clears entire screen in the device context.(Black rectangle will be drawn over the entire screen.)
 *
 * Return Value: The function does not return any value.
 *
 * File: sui_gdi.c
 */
void sui_gdi_clear_screen(sui_dc_t *dc)
{
 #if (MICROWINDOWS_VER_CODE<VER_CODE(0,93,0))
  sui_gdi_set_drmode(dc, MWMODE_COPY);
 #else /*MICROWINDOWS_VER_CODE>=VER_CODE(0,93,0)*/
  sui_gdi_set_drmode(dc, MWROP_COPY);
 #endif /*MICROWINDOWS_VER_CODE>=VER_CODE(0,93,0)*/
  sui_gdi_set_usebgrd(dc, FALSE);

  sui_gdi_set_fgcolor(dc, 0);
  GdFillRect(dc->psd, 0, 0, dc->psd->xvirtres, dc->psd->yvirtres);
  sui_gdi_set_fgcolor(dc, dc->psd->ncolors-1);
}


/******************************************************************************
 * Alignment
 ******************************************************************************/
/**
 * sui_align_object - Compute an object's alignment to a specified box according to align flags (relative alignment)
 * @box: A box dimensions (x=width, y=height).
 * @origin: Pointer to an output buffer for the object origin (x, y).
 * @obj: A object dimensions (x=width, y=height).
 * @objbase: A value of base line of the object (for text) for alignment according to flag SUAL_BASELINE.
 * @style: Pointer to a used style (gap and frame is needs) (can be NULL).
 * @align: An alignment flags.
 *
 * The function computes object's origin relatively to entered box dimensions, object dimensions and alignment flags.
 *
 * Return Value: The function returns zero as success or a negative value if any error occurs.
 *
 * File: sui_gdi.c
 */
int sui_align_object(sui_point_t *box, sui_point_t *origin, sui_point_t *obj,
                    sui_coordinate_t objbase, sui_style_t *style, sui_align_t align)
{
  sui_coordinate_t x=0, y=0;
  sui_coordinate_t fr = 0, sp = 0;
  if (!box || !origin || !obj) return -1;
  if (style && ((style->flags & SUSFL_FRAME) ||
               ((align & SUAL_INT_LABEL) &&(style->flags & SUSFL_LABELHASFRAME)))) {
    fr = style->frame;
    sp = style->gap;
  }
  if (align & SUAL_INSIDE) {
    switch(align & SUAL_HORMASK) {
      case SUAL_CENTER: x = (box->x - obj->x)/2; break;
      case SUAL_LEFT:   x = sp; break;
      case SUAL_RIGHT:  x = box->x - obj->x - sp; break;
      case SUAL_BLOCK:  x = sp; break; // !!! change -> changed 17.10.2004
    }
    switch(align & SUAL_VERMASK) {
      case SUAL_CENTER:   y = (box->y - obj->y)/2; break;
      case SUAL_TOP:      y = sp; break;
      case SUAL_BOTTOM:   y = box->y - obj->y - sp; break;
      case SUAL_BASELINE: y = (box->y - objbase)/2; break; // !!! change
    }
  } else {
    switch(align & SUAL_HORMASK) {
      case SUAL_CENTER:x = (box->x - obj->x)/2; break;
      case SUAL_BLOCK: x = 0; break;
      case SUAL_LEFT:  x = - (fr+sp) - obj->x; break;
      case SUAL_RIGHT: x = box->x + (fr+sp); break;
    }
    switch(align & SUAL_VERMASK) {
      case SUAL_CENTER:   y = (box->y - obj->y)/2; break;
      case SUAL_TOP:      y = - (fr+sp) - obj->y; break;
      case SUAL_BOTTOM:   y = box->y + (fr+sp); break;
      case SUAL_BASELINE: y = -objbase; break;
    }
  }
  origin->x = x;
  origin->y = y;
  return 0;
}

/**
 * sui_align_object_to_point - align object to point according to align flags
 * @origin: Pointer to an output buffer for the object origin (x, y).
 * @obj: A object dimensions (x=width, y=height).
 * @objbase: A value of base line of the object (for text) for alignment according to flag SUAL_BASELINE.
 * @box: A point (x, y).
 * @align: Alignment flags.
 *
 * The function computes object's origin relatively to entered point, object dimensions and alignment flags.
 * Returned origin is the object's left upper corner.
 *
 * Return Value: The function returns zero as success or a negative value if any error occurs.
 *
 * File: sui_gdi.c
 */
int sui_align_object_to_point(sui_point_t *origin, sui_point_t *obj, sui_coordinate_t objbase,
                              sui_point_t *point, sui_align_t align)
{
  sui_coordinate_t x=0, y=0;
  if (!point || !origin || !obj) return -1;
  switch(align & SUAL_HORMASK) {
    case SUAL_BLOCK:
    case SUAL_CENTER:x = point->x - obj->x/2; break;
    case SUAL_LEFT:  x = point->x - obj->x; break;
    case SUAL_RIGHT: x = point->x; break;
  }
  switch(align & SUAL_VERMASK) {
    case SUAL_CENTER:   y = point->y - obj->y/2; break;
    case SUAL_TOP:      y = point->y - obj->y; break;
    case SUAL_BOTTOM:   y = point->y; break;
    case SUAL_BASELINE: y = point->y + objbase; break;
  }
  origin->x = x;
  origin->y = y;
  return 0;
}

/**
 * sui_get_complementary_align - Compute complementary alignment to entered one (other side to the entered alignment)
 * @align: Alignment flags.
 *
 * The function returns complementary alignment flags to entered one.
 *
 * Return Value: The function returns computed alignment flags.
 *
 * File: sui_gdi.c
 */
sui_align_t sui_get_complementary_align(sui_align_t align)
{
  switch(align & SUAL_HORMASK) {
    case SUAL_LEFT:  align = (align & ~SUAL_HORMASK) | SUAL_RIGHT; break;
    case SUAL_RIGHT: align = (align & ~SUAL_HORMASK) | SUAL_LEFT; break;
  }
  switch(align & SUAL_VERMASK) {
    case SUAL_TOP:    align = (align & ~SUAL_VERMASK) | SUAL_BOTTOM; break;
    case SUAL_BOTTOM: align = (align & ~SUAL_VERMASK) | SUAL_TOP; break;
  }
  return align;
}


/******************************************************************************
 * Clipping
 ******************************************************************************/
/**
 * sui_gdi_set_carea - Set current device clipping area
 * @dc: Pointer to a device context.
 * @box: Pointer to a clip area (position and dimensions).
 *
 * The function sets entered rectangle area as the current device clipping area.
 *
 * Return Value: The function does not return any value.
 *
 * File: sui_gdi.c
 */
void sui_gdi_set_carea(sui_dc_t *dc, sui_rect_t *box)
{
  sui_coordinate_t x, y;
  x = box->x + dc->offset.x;
  y = box->y + dc->offset.y;
  dc->cliprect.x = box->x + dc->offset.x;
  dc->cliprect.y = box->y + dc->offset.y;
  dc->cliprect.w = box->w;
  dc->cliprect.h = box->h;
  GdSetClipRegion(dc->psd, GdAllocRectRegion(x, y, x + box->w, y + box->h));
}

/**
 * sui_gdi_clear_carea - Clear device clipping area
 * @dc: Pointer to a device context.
 *
 * The function clears current device clipping area.
 *
 * Return Value: The function does not return any value.
 *
 * File: sui_gdi.c
 */
void sui_gdi_clear_carea(sui_dc_t *dc)
{
  dc->cliprect.x = 0;
  dc->cliprect.y = 0;
  dc->cliprect.w = 0;
  dc->cliprect.h = 0;
  GdSetClipRegion(dc->psd, GdAllocRectRegion(0, 0, dc->psd->xvirtres, dc->psd->yvirtres));
}


/******************************************************************************
 * Colors
 ******************************************************************************/
/**
 * sui_colortable_inc_refcnt - Increment color-table reference counter
 * @ctab: Pointer to a color-table structure.
 *
 * The function increments color-table structure reference counter.
 *
 * Return Value: The function returns value of the color-table's reference counter after incrementing.
 *
 * File: sui_gdi.c
 */
sui_refcnt_t sui_colortable_inc_refcnt(sui_colortable_t *ctab)
{
  if (!ctab) return SUI_REFCNTERR;
  if(ctab->refcnt>=0) ctab->refcnt++;
  return ctab->refcnt;
}

/**
 * sui_colortable_dec_refcnt - Decrement color-table reference counter
 * @ctab: Pointer to a color-table structure.
 *
 * The function decrements the color-table structure reference counter.
 * If the counter is decremented to zero color-table structure is deleted.
 *
 * Return Value: The function returns value of the color-table's reference counter after decrementing.
 *
 * File: sui_gdi.c
 */
sui_refcnt_t sui_colortable_dec_refcnt(sui_colortable_t *ctab)
{
  sui_refcnt_t ref;
  if (!ctab) return SUI_REFCNTERR;
  if(ctab->refcnt>0) ctab->refcnt--;
  if(!(ref=ctab->refcnt)) {
    if (ctab->ctab && ctab->calloc != SUI_STATIC) {
      free(ctab->ctab);
      ctab->ctab = NULL;
    }
    sui_obj_done_vmt((sui_obj_t *)ctab, UL_CAST_UNQ1(sui_obj_vmt_t *, &sui_colortable_vmt_data));
    free(ctab);
  }
  return ref;
}

/**
 * sui_colortable_create - Create color-table structure
 * @acnt: Number of colors added from an array of colors for dynamic array or SUI_STATIC for static table. ? Negative value is count of colors.?
 * @ac: An array of colors.
 *
 * The function creates dynamic reference counting color-table from
 * an array of colors. And then it increments reference counter.
 *
 * Return Value: The function returns pointer to created color table or NULL if color-table isn't allocated.
 *
 * File: sui_gdi.c
 */
sui_colortable_t *sui_colortable_create(int acnt, sui_color_t ac[])
{
  sui_colortable_t *ct = sui_malloc(sizeof(sui_colortable_t));
  if (!ct) return NULL;
  sui_colortable_inc_refcnt(ct);
  if (acnt < 0) { /* static array of colors */
    ct->calloc = SUI_STATIC;
    ct->ctab = ac;
    ct->count = -acnt;
  } else {
    ct->ctab = malloc(sizeof(sui_color_t)*acnt);
    ct->calloc = acnt;
    if (ct->ctab) {
      sui_color_t *c = ct->ctab;
      int i;
      if (ac) {
        for(i=0; i<acnt; i++) {
          *c = ac[i];
          c++;
        }
        ct->count = i;
      }
    }
  }
  return ct;
}

/**
 * sui_color_add_to_colortable - Add color into color-table
 * @ctab: Pointer to a color-table.
 * @c: Value of an added color.
 *
 * The function adds new color value into color table if the table isn't static.
 * If there isn't allocate space for new color, the function allocates new space.
 *
 * Return Value: The function does not return any value.
 *
 * File: sui_gdi.c
 */
void sui_color_add_to_colortable(sui_colortable_t *ctab, sui_color_t c)
{
  sui_color_t *color;
  if (!ctab || ctab->calloc == SUI_STATIC) return;
  color = sui_add_item_to_array((void **)((void *)&ctab->ctab), sizeof(sui_color_t),
                                &ctab->count, &ctab->calloc);
  if (!color) return;
  *color = c;
}

/**
 * sui_gdi_set_fgcolor - Set device foreground color as a color value
 * @dc: Pointer to a device context.
 * @color: Value of a new foreground color.
 *
 * The function sets foreground color to the device context.
 *
 * Return Value: The function does not return any value.
 *
 * File: sui_gdi.c
 */
inline
void sui_gdi_set_fgcolor(sui_dc_t *dc, sui_color_t color)
{
  color = sui_color_reverse(color); /* MW has color as 0x00bbggrr */
  GdSetForegroundColor(dc->psd, color); /* GdFindColor is inside */
}

/**
 * sui_gdi_set_bgcolor - Set device background color as a color value
 * @dc: Pointer to a device context.
 * @color: Value of a new background color.
 *
 * The function sets background color to the device context.
 *
 * Return Value: The function does not return any value.
 *
 * File: sui_gdi.c
 */
inline
void sui_gdi_set_bgcolor(sui_dc_t *dc, sui_color_t color)
{
  color = sui_color_reverse(color); /* MW has color as 0x00bbggrr */
  GdSetBackgroundColor(dc->psd, color); /* GdFindColor is inside */
}

/**
 * sui_gdi_set_color - Set device foreground and background color as color values
 * @dc: Pointer to a device context.
 * @fgcolor: Value of a new foreground color.
 * @bgcolor: Value of a new background color.
 *
 * The function sets foreground and background colors to the device context.
 *
 * Return Value: The function does not return any value.
 *
 * File: sui_gdi.c
 */
inline
void sui_gdi_set_color(sui_dc_t *dc, sui_color_t fgcolor, sui_color_t bgcolor)
{
  sui_gdi_set_fgcolor(dc, fgcolor);
  sui_gdi_set_bgcolor(dc, bgcolor);
}


/******************************************************************************
 * FONTs
 ******************************************************************************/
/**
 * sui_font_prepare - Prepare a new font for using under device
 * @dc: Pointer to a device context.
 * @font: Pointer to a SUITK font-info structure.(See 'sui_font_info' structure).
 * @height: Value of font height.
 *
 * The function prepares font for using in the device context.
 *
 * Return Value: The function returns pointer to prepared device font.
 *
 * File: sui_gdi.c
 */
sui_dcfont_t *sui_font_prepare(sui_dc_t *dc, sui_font_t font, sui_coordinate_t height)
{
  sui_dcfont_t *dcfnt;
  if (!font) return NULL;
 #if (MICROWINDOWS_VER_CODE<VER_CODE(0,93,0))
  dcfnt = GdCreateFont(dc->psd, sui_utf8_get_text(font), height, NULL);
 #else /*MICROWINDOWS_VER_CODE>=VER_CODE(0,93,0)*/
  dcfnt = GdCreateFont(dc->psd, sui_utf8_get_text(font), height, height, NULL);
 #endif /*MICROWINDOWS_VER_CODE>=VER_CODE(0,93,0)*/
  if (!dcfnt) {
    ul_logerr("There wasn't found font %s !\n", sui_utf8_get_text(font));
   #if (MICROWINDOWS_VER_CODE<VER_CODE(0,93,0))
    dcfnt = GdCreateFont(dc->psd, "X5x7", height, NULL);
   #else /*MICROWINDOWS_VER_CODE>=VER_CODE(0,93,0)*/
    dcfnt = GdCreateFont(dc->psd, "X5x7", height, height, NULL);
   #endif /*MICROWINDOWS_VER_CODE>=VER_CODE(0,93,0)*/
  }
  return dcfnt;
}

/**
 * sui_font_set - Select a device font for using
 * @dc: Pointer to a device context.
 * @pdcfont: Pointer to a prepared device font.
 *
 * All fonts must be selected by this function before using with sui_gettextsize,
 * sui_draw_text and sui_draw_align_text functions.
 *
 * Return Value: The function does not return any value.
 *
 * File: sui_gdi.c
 */
inline
void sui_font_set(sui_dc_t *dc, sui_dcfont_t *pdcfont)
{
  suiGdSetFont(pdcfont);
}

/**
 * sui_font_get_info - Get font-info structure from device font
 * @dc: Pointer to a device context.
 * @pdcfont: Pointer to a prepared device font.
 * @fontinfo: Pointer to an output buffer for sui_fontinfo structure.
 *
 * Function fills fontinfo structure from prepared device context font.
 *
 * Return Value: The function returns zero as success and a negative value if any error occurs.
 *
 * File: sui_gdi.c
 */
inline
int sui_font_get_info(sui_dc_t *dc, sui_dcfont_t *pdcfont, sui_fontinfo_t *fontinfo)
{
  return (GdGetFontInfo(pdcfont, fontinfo)) ? 0 : -1;
}


/******************************************************************************
 * Text
 ******************************************************************************/
/**
 * sui_text_get_size - Get UTF8 text bounding box
 * @dc: Pointer to a device context.
 * @pdcfont: Pointer to a prepared device font.
 * @tdims: Pointer to an output buffer for text dimensions.
 * @len: Length of used text (in characters) or -1 for entire text.
 * @utf: An UTF8 text.
 * @align: Text's alignment flags (only SUAL_COLUMN flag is tested).
 *
 * The function gets width, height and baseline of the UTF8 text (or its part) and returns them into the tdims structure.
 *
 * Retrn Value: The function returns a negative value if any error occurs otherwise it returns zero.
 *
 * File: sui_gdi.c
 */
inline
int sui_text_get_size(sui_dc_t *dc, sui_dcfont_t *pdcfont, sui_textdims_t *tdims,
                      int len, utf8 *utf, sui_align_t align)
{
  MWCOORD w, h, b;
  suiGdGetTextSize(pdcfont, utf, len, &w, &h, &b,
                  MWTF_UTF8 | ((align & SUAL_COLUMN) ? MWTF_COLUMN_TEXT : 0));
  if (!tdims) return -1;
  tdims->w = w;// + 1; // add one pixel before text (for better reading)
  tdims->h = h;
  tdims->b = b;
  return 0;
}

/**
 * sui_draw_text - Draw text with coordinates relatively to the current device canvas
 * @dc: Pointer to a device context.
 * @x: The horizontal position of the text's top left corner.
 * @y: The vertical position of the text's top left corner.
 * @len: Length (in characters) of the drawn part of the text or -1 for the entire text.
 * @utf: An UTF8 text.
 * @align: Text's alignment flags (only SUAL_COLUMN flag is tested).
 *
 * The function draws either entire text or its part according to the entered position relatively to the current device canvas.
 *
 * Return Value: The function does not return any value.
 *
 * File: sui_gdi.c
 */
void sui_draw_text(sui_dc_t *dc, sui_coordinate_t x, sui_coordinate_t y,
                  int len, utf8 *utf, sui_align_t align)
{
  MWTEXTFLAGS flg = MWTF_UTF8;
  x += dc->offset.x;
  y += dc->offset.y;
  if (align & SUAL_COLUMN) flg |= MWTF_COLUMN_TEXT;
  suiGdText(dc->psd, x, y, utf, len, flg);
}


/******************************************************************************
 * Image
 ******************************************************************************/
/**
 * sui_image_inc_refcnt - Increment image reference counter
 * @img: Pointer to an image structure.
 *
 * The function increments image structure reference counter.
 *
 * Return Value: The function returns value of the image's reference counter after incrementing.
 *
 * File: sui_gdi.c
 */
sui_refcnt_t sui_image_inc_refcnt(sui_image_t *img)
{
  if (!img) return SUI_REFCNTERR;
  if (img->refcnt >= 0) img->refcnt++;
  return img->refcnt;
}

/**
 * sui_image_dec_refcnt - Decrement image reference counter
 * @img: Pointer to an image structure.
 *
 * The function decrements image structure reference counter.
 * If the counter is decremented to zero image structure is deleted.
 *
 * Return Value: The function returns value of the image's reference counter after decrementing.
 *
 * File: sui_gdi.c
 */
sui_refcnt_t sui_image_dec_refcnt(sui_image_t *image)
{
  sui_refcnt_t ref;
  if (!image) return SUI_REFCNTERR;
  if (image->refcnt > 0) image->refcnt--;
  if (!(ref = image->refcnt)) {
    if ((image->flags & SUIM_DYNAMIC_DATA) && image->data) {
      free(image->data);
      image->data = NULL;
    }
    sui_colortable_dec_refcnt(image->cmap);
    image->cmap = NULL;
    sui_obj_done_vmt((sui_obj_t *)image, UL_CAST_UNQ1(sui_obj_vmt_t *, &sui_image_vmt_data));
    free(image);
  }
  return ref;
}

/**
 * sui_create_image - Create dynamic image structure from image data and palette
 * @awidth: Image width.
 * @aheight: Image height.
 * @acount: Number of image frames (subimages).
 * @aflags: Image flags - there must be specified SUIM_DYNAMIC_DATA for dynamic image data (MWIMAGEBITS).
 * @adata: Pointer to image data.
 * @acolmap: Pointer to image color palette.
 *
 * The function creates a new image structure and fills it with entered values.
 *
 * Return Value: The function returns pointer to the created image or NULL if any error ocurred.
 *
 * File: sui_gdi.c
 */
sui_image_t *sui_image_create(sui_coordinate_t awidth, sui_coordinate_t aheight,
                              short acount, unsigned short aflags,
                              MWIMAGEBITS *adata, sui_colortable_t *acmap)
{
  int bpmw;
  sui_image_t *img = sui_malloc(sizeof(sui_image_t));
  if (img) {
    sui_image_inc_refcnt(img);
    img->flags = aflags;
    img->width  = awidth;
    img->height = aheight;
    img->count  = acount;
    img->data   = adata;
    if (acmap) {
      sui_colortable_inc_refcnt(acmap);
      img->cmap = acmap;
    }
    bpmw = sizeof(MWIMAGEBITS);
    switch(aflags & SUIM_TYPEMASK) {
      case SUIM_BITMAP_1BIT: bpmw *= 8; break;
      case SUIM_BITMAP_2BIT: bpmw *= 4; break;
      case SUIM_BITMAP_4BIT: bpmw *= 2; break;
      case SUIM_BITMAP_8BIT: bpmw *= 1; break;
      default: bpmw /= sizeof(MWIMAGEBITS); break;
    }
    img->bpr = awidth / bpmw;
    if (awidth % bpmw) img->bpr++;
  }
  return img;
}

/**
 * sui_gdi_draw_image - Draw image bitmap optionally with several simple efects(mirroring,rotating,...)
 * @dc: Pointer to a device context.
 * @origin: Pointer to position of image's left top corner.
 * @image: Pointer to an image.
 * @mask: Pointer to a mask image or NULL.
 * @index: Index of images' frame in multi-image.
 * @operation: Operation flags [see flags with 'SUIO_' suffix].
 *
 * The function draws the image in several possible modes selected by the flags.
 *
 * Return Value: The function does not return any value.
 *
 * File: sui_gdi.c
 */
void sui_gdi_draw_image(sui_dc_t *dc, sui_point_t *origin, sui_image_t *image,
                        sui_image_t *mask, int index, sui_flags_t operation)
{
  MWIMAGEBITS *imdata=NULL, *immask=NULL;
  MWIMAGEBITS nowim = 0, nowma = 0;
  MWIMAGEBITS mb, mmb = 0, imidx, maidx;
  MWIMAGEBITS previmidx;
  MWPIXELVAL pixelval;
  unsigned char withmask = (operation & SUIO_MASK_IMAGE) == SUIO_MASK_IMAGE;
  unsigned char invdata = (operation & SUIO_INVERSE_IMAGE) == SUIO_INVERSE_IMAGE;
  unsigned char invmask = (operation & SUIO_INVERSE_MASK) == SUIO_INVERSE_MASK;
  unsigned char rot = ((operation & SUIO_ROTATION_MASK) == SUIO_ROTATE_90) ||
                        ((operation & SUIO_ROTATION_MASK)== SUIO_ROTATE_270);
  unsigned char notrans = (operation & SUIO_TRANSPARENT) != SUIO_TRANSPARENT;
  sui_coordinate_t x, y, w, h, ww, dx, dy;
  int ppb, cnt, mppb, mcnt; // pixels per MWIMAGEBITS
  int bits, mbits = 1, clip;
  sui_color_t *cmap, defcmap[2];

  if (!image || !image->data || (withmask && !mask))
    return;
  imdata = image->data;
  if (image->cmap)
    cmap = image->cmap->ctab;
  else {
    defcmap[0] = 0; defcmap[1] = ~0;
    cmap = &defcmap[0];
  }
  if (withmask) { // mask must have with same size as image
    if(image->height != mask->height || image->width != mask->width)
      return;
    immask = mask->data;
  }

  x = origin->x + dc->offset.x;
  y = origin->y + dc->offset.y;

  if (rot) {
    w = x; x = y; y = w;
  }

  w = image->width; h = image->height;

/* FIXME: add drawing partially clipped image */
  if (rot) {
    if ((clip=GdClipArea(dc->psd, y, x, y+w-1, x+h-1))!=CLIP_VISIBLE) return; 
  } else {
    if ((clip=GdClipArea(dc->psd, x, y, x+w-1, y+h-1))!=CLIP_VISIBLE) return;
  }

  if (((operation & SUIO_MIRROR)== SUIO_MIRROR) ^
      (((operation & SUIO_ROTATION_MASK)== SUIO_ROTATE_90) ||
      ((operation & SUIO_ROTATION_MASK)== SUIO_ROTATE_180))) {
    ww = w;
    x += w-1;
    dx = -1;
  } else {
    ww = -w;
    dx = 1;
  }
  if ((operation & SUIO_ROTATION_MASK) >= SUIO_ROTATE_180) {
    dy = -1;
    y += h-1;
  } else {
    dy = 1;
  }

  switch(image->flags & SUIM_TYPEMASK) {
    case SUIM_BITMAP_1BIT: bits = 1; mb = 0x01; break;
    case SUIM_BITMAP_2BIT: bits = 2; mb = 0x03; break;
    case SUIM_BITMAP_4BIT: bits = 4; mb = 0x0f; break;
    case SUIM_BITMAP_8BIT: bits = 8; mb = 0xff; break;
    default: return;
  }
  mb = mb << (MWIMAGE_BITSPERIMAGE-bits);
  ppb = MWIMAGE_BITSPERIMAGE / bits;
  if (withmask) {
    switch(mask->flags & SUIM_TYPEMASK) {
      case SUIM_BITMAP_1BIT: mbits = 1; mmb = 0x01; break;
      case SUIM_BITMAP_2BIT: mbits = 2; mmb = 0x03; break;
      case SUIM_BITMAP_4BIT: mbits = 4; mmb = 0x0f; break;
      case SUIM_BITMAP_8BIT: mbits = 8; mmb = 0xff; break;
    }
    mmb = mmb << (MWIMAGE_BITSPERIMAGE-mbits);
    mppb = MWIMAGE_BITSPERIMAGE / mbits;
  } else {
    mppb = 0;
  }
  if (operation & SUIO_MULTIIMAGE && index >= 0) {
    if (index < image->count) {
      imdata += index*(image->height*(image->width/ppb)); /* FIXME: check if w/ppb is always right */
    }
    if (mask && immask && index < mask->count)
      immask += index*(mask->height*(mask->width/mppb)); /* FIXME: check if w/mppb is always right */
  }

  maidx = 1;
  previmidx = cmap[0];
  pixelval=GdFindColor(dc->psd, sui_color_reverse(cmap[0]));
  while (h--) {
    cnt = 0; mcnt = 0;
    w = image->width;
    while(w--) {
      if (!cnt) {
        nowim = *imdata++;
        if (invdata) nowim = ~nowim;
        cnt = ppb;
      }
      imidx = (nowim & mb) >> (MWIMAGE_BITSPERIMAGE-bits);
      nowim <<= bits;
      cnt--;

      if (withmask) {
        if (!mcnt) {
          nowma = *immask++;
          if (invmask) nowma = ~nowma;
          mcnt = mppb;
        }
        maidx = (nowma & mmb) >> (MWIMAGE_BITSPERIMAGE-mbits);
        nowma <<= mbits;
        mcnt--;
      }

      if (maidx) {
        if (notrans || imidx) { /* transparency */
          if(previmidx != cmap[imidx]) {
            previmidx = cmap[imidx];
            pixelval=GdFindColor(dc->psd, sui_color_reverse(cmap[imidx]));
            //pixelval = cmap[imidx];
//ul_logdeb("GDI-ICON: 0x%X - 0x%X", pixelval,cmap[imidx]);
          }
          if (rot)
            dc->psd->DrawPixel(dc->psd, y, x, pixelval);
          else
            dc->psd->DrawPixel(dc->psd, x, y, pixelval);
        }
      }
      x += dx;
    }
    x += ww; y += dy;
  }
}

/**
 * sui_gdi_image_from_screen - Creates hardcopy of a screen as an image
 * @dc: Pointer to a device context.
 * @rect: Pointer to dimensions of screen cut-out or NULL for coping of entire screen.
 * @imgbpp: Desired number of image's bits per pixel (-1 for bpp according to screen).
 *
 * The function makes a hardcopy of the screen or its part (if $rect is set) and the function returns it as an image.
 *
 * Return Value: The function returns pointer to created image or NULL.
 *
 * File: sui_gdi.c
 */
sui_image_t *sui_gdi_image_from_screen(sui_dc_t *dc, sui_rect_t *rect, int imgbpp)
{
  sui_image_t *img = NULL;
  sui_coordinate_t x, y, l, t, w, h;
  unsigned long pix;
  int ppmw; // pixels per mwimagebits
  int cmax; // number of colors
  int cidx; // current index to color table
  int clast; // last index of known color
  MWIMAGEBITS *pimg, imgdata; // pointer to image data
  MWIMAGEBITS pixmask, defmask; // masks for image data
  int bpp; // bits per pixel
  int idximg; // index of pixel in MWIMAGEBITS
  sui_color_t *color;

  if (!dc) return NULL;
// create image with dimensions according to $rect or $dc
  img = sui_malloc(sizeof(sui_image_t));
  if (!img) return NULL;

  w = dc->psd->xres;
  h = dc->psd->yres;
  l = 0; t = 0;
  if (rect) {
    if (rect->x>0) l = rect->x;
    if (rect->y>0) t = rect->y;
    w = rect->w;
    h = rect->h;
    if ((l+w)>dc->psd->xres) w = dc->psd->xres-l;
    if ((t+h)>dc->psd->yres) h = dc->psd->yres-t;
  }

  sui_image_inc_refcnt(img);
  img->width = w;
  img->height = h;
  img->count = 1;
  ppmw = sizeof(MWIMAGEBITS);
  img->flags = SUIM_DYNAMIC_DATA;

  if (imgbpp<0) imgbpp = dc->psd->bpp;
  switch(imgbpp) {
    case 1: bpp = 1; img->flags |= SUIM_BITMAP_1BIT; defmask = 0x01 << (ppmw*8-bpp); ppmw *= 8; cmax = 2; break;
    case 2: bpp = 2; img->flags |= SUIM_BITMAP_2BIT; defmask = 0x03 << (ppmw*8-bpp); ppmw *= 4; cmax = 4; break;
    case 4: bpp = 4; img->flags |= SUIM_BITMAP_4BIT; defmask = 0x0F << (ppmw*8-bpp); ppmw *= 2; cmax = 16; break;
    default: bpp= 8; img->flags |= SUIM_BITMAP_8BIT; defmask = 0xFF << (ppmw*8-bpp); ppmw *= 1; cmax = 256; break;
  }

  img->bpr = w / ppmw;
  if (w % ppmw) img->bpr++;

// allocate memory for image data and colormap
  img->data = sui_malloc(sizeof(MWIMAGEBITS)*img->bpr*h);
  img->cmap = sui_colortable_create(cmax, NULL);
  img->cmap->count = cmax; // all colors are in colortable (2,4,16,256)
  clast = 0;
  color = img->cmap->ctab;
// save image data from screen
  y = 0;
  pimg = img->data;
  while (y<h) {
    x = 0;
    idximg = ppmw-1;
    pixmask = defmask;
    while (x<w) {
      pix = dc->psd->ReadPixel(dc->psd, x+l, y+t);
      // cidx <- pix; pixel color (add color to table)
      cidx = 0;
      while(cidx<clast && cidx<cmax) {
        if (*(color+cidx)==pix) break;
        cidx++;
      }
      if (cidx==clast) {
        if (cidx<cmax) {
          *(color+clast) = pix;
          clast++;
        } else {
          cidx = cmax-1;
        }
      }
    // save pixel to pimg
      imgdata = *pimg;
      imgdata |= (cidx << (bpp*idximg)) & pixmask;
      *pimg = imgdata;

      if (idximg == 0) {
        pimg++;
        idximg = ppmw-1;
        pixmask = defmask;
      } else {
        idximg--;
        pixmask >>= bpp;
      }
      x++;
    }
    if (idximg!=ppmw-1) pimg++;   // end of row with no full mwimagebits
    y++;
  }
  return img;
}

/******************************************************************************
 * Style
 ******************************************************************************/
/**
 * sui_style_inc_refcnt - Increment style reference counter
 * @style: Pointer to a style structure.
 *
 * The function increments the style structure reference counter.
 *
 * Return Value: The function returns value of the style's reference counter after incrementing.
 *
 * File: sui_gdi.c
 */
sui_refcnt_t sui_style_inc_refcnt(sui_style_t *style)
{
  if (!style) return SUI_REFCNTERR;
  if(style->refcnt>=0) style->refcnt++;
  return style->refcnt;
}

/**
 * sui_style_dec_refcnt - Decrement style reference counter
 * @style: Pointer to a style structure.
 *
 * The function decrements the style structure reference counter.
 * If the counter is decremented to zero the style structure is deleted.
 *
 * Return Value: The function returns value of the style's reference counter after decrementing.
 *
 * File: sui_gdi.c
 */
sui_refcnt_t sui_style_dec_refcnt(sui_style_t *style)
{
  sui_refcnt_t ref;
  if (!style) return SUI_REFCNTERR;
  if(style->refcnt>0) style->refcnt--;
  if(!(ref=style->refcnt)) {
    sui_colortable_dec_refcnt(style->coltab);
    style->coltab = NULL;
    sui_fonti_dec_refcnt(style->fonti);
    style->fonti = NULL;
    sui_fonti_dec_refcnt(style->labfonti);
    style->labfonti = NULL;
    sui_obj_done_vmt((sui_obj_t *)style, UL_CAST_UNQ1(sui_obj_vmt_t *, &sui_style_vmt_data));
    free(style);
  }
  return ref;
}


/**
 * sui_style_create - Create a dynamic style structure
 * @aflags: A new style initial flags.
 * @actab: Pointer to a color-table.
 * @alabfi: Pointer to a label font-info structure.
 * @afi: Pointer to an object font-info structure.
 * @aframe: Size of an object's frame.
 * @agap: Size of objects' gaps.
 *
 * The function creates dynamically reference counting style structure,
 * fills it and increments its reference counter.
 *
 * Return Value: The function returns pointer to created structure or NULL.
 *
 * File: sui_gdi.c
 */
sui_style_t *sui_style_create(sui_flags_t aflags, sui_colortable_t *actab,
                              sui_font_info_t *alabfi, sui_font_info_t *afi,
                              sui_coordinate_t aframe, sui_coordinate_t agap)
{
  sui_style_t *pst = sui_malloc(sizeof(sui_style_t));
  if (pst) {
    sui_style_inc_refcnt(pst);
    pst->flags = aflags;
    sui_colortable_inc_refcnt(actab);
    pst->coltab = actab;
    sui_fonti_inc_refcnt(alabfi);
    pst->labfonti = alabfi;
    sui_fonti_inc_refcnt(afi);
    pst->fonti = afi;
    pst->frame = aframe;
    pst->gap = agap;
  }
  return pst;
}


/******************************************************************************
 * Lines and curves
 ******************************************************************************/
/**
 * sui_draw_line - Draw line according to its coordinates
 * @dc: Pointer to a device context.
 * @x1: Horizontal coordinate of the line's begin point.
 * @y1: Vertical coordinate of the line's begin point.
 * @x2: Horizontal coordinate of the line's end point.
 * @y2: Vertical coordinate of the line's end point.
 *
 * The function draws line from [x1,y1] to [x2,y2] relatively to the current device canvas.
 *
 * Return Value: The function does not return any value.
 *
 * File: sui_gdi.c
 */
inline
void sui_draw_line(sui_dc_t *dc, sui_coordinate_t x1, sui_coordinate_t y1,
                  sui_coordinate_t x2, sui_coordinate_t y2)
{
  GdLine(dc->psd, x1 + dc->offset.x, y1 + dc->offset.y,
        x2 + dc->offset.x, y2 + dc->offset.y, 1);
}

/**
 * sui_draw_line_points - Draw line according to its coordinates
 * @dc: Pointer to a device context.
 * @from: Pointer to a point structure with line's begin point.
 * @to: Pointer to a point structure with line's end point.
 *
 * The function draws line from [from.x,from.y] to [to.x,to.y] relatively to the current device canvas.
 *
 * Return Value: The function does not return any value.
 *
 * File: sui_gdi.c
 */
inline
void sui_draw_line_points(sui_dc_t *dc, sui_point_t *from, sui_point_t *to)
{
  GdLine(dc->psd, from->x + dc->offset.x, from->y + dc->offset.y,
        to->x + dc->offset.x, to->y + dc->offset.y, 1);
}

/**
 * sui_draw_line_rect - Draw line according to its coordinates
 * @dc: Pointer to a device context.
 * @rect: Pointer to a rectangle structure with position and dimensions of the line.
 *
 * The function draws line from [rect.x,rect.y] to [rect.x+rect.w,rect.y+rect.h] relatively to the current device canvas.
 *
 * Return Value: The function does not return any value.
 *
 * File: sui_gdi.c
 */
inline
void sui_draw_line_rect(sui_dc_t *dc, sui_rect_t *rect)
{
  GdLine(dc->psd, rect->x + dc->offset.x, rect->y + dc->offset.y,
        rect->x + rect->w + dc->offset.x, rect->y + rect->h + dc->offset.y, 1);
}

/**
 * sui_draw_ellipse - Draw ellipse with entered coordinates
 * @dc: Pointer to a device context.
 * @x: Horizontal position of the ellipse center.
 * @y: Vertical position of the ellipse center.
 * @rx: Horizontal radius of the ellipse.
 * @ry: Vertical radius of the ellipse.
 * @fill: If it is set the ellipse will be filled.
 *
 * The function draws ellipse relatively to the current device canvas.
 *
 * Return Value: The function does not return any value.
 *
 * File: sui_gdi.c
 */
inline
void sui_draw_ellipse(sui_dc_t *dc, sui_coordinate_t x, sui_coordinate_t y,
                      sui_coordinate_t rx, sui_coordinate_t ry, unsigned char fill)
{
  GdEllipse(dc->psd, x + dc->offset.x, y + dc->offset.y, rx, ry, fill);
}


/******************************************************************************
 * Compound objects
 ******************************************************************************/
/**
 * sui_draw_rect - Draw rectangle according to its coordinates
 * @dc: Pointer to a device context.
 * @x: Horizontal position of the rectangle's top left corner.
 * @y: Vertical position of the rectangle's top left corner.
 * @w: Width of the rectangle.
 * @h: Height of the rectangle.
 * @frm: Size of the rectangle frame, for 0 for filled rectangle.
 *
 * The function draws rectangle frame or fully filled rectangle relatively to the current device canvas..
 *
 * Return Value: The function does not return any value.
 *
 * File: sui_gdi.c
 */
void sui_draw_rect(sui_dc_t *dc, sui_coordinate_t x, sui_coordinate_t y,
                  sui_coordinate_t w, sui_coordinate_t h, sui_coordinate_t frm)
{
  if (w <= 0 || h <= 0) return; /* draw only positive width and height - in sui_base_draw_widget is subtraction */
  x += dc->offset.x; y += dc->offset.y;
  if (!frm) {
    GdFillRect(dc->psd, x, y, w, h);
  } else {
    if (frm == 1) {
      GdRect(dc->psd, x, y, w, h);
    } else {
      GdFillRect(dc->psd, x, y, w, frm);
      GdFillRect(dc->psd, x, y+h-frm, w, frm);
      GdFillRect(dc->psd, x, y+frm, frm, h-2*frm);
      GdFillRect(dc->psd, x+w-frm, y+frm, frm, h-2*frm);
    }
  }
}

/**
 * sui_draw_rect_points - Draw rectangle according to its coordinates
 * @dc: Pointer to a device context.
 * @origin: Coodinates of the rectangle's top left corner.
 * @size: The rectangle's dimensions.
 * @frm: Size of the rectangle frame, for 0 for filled rectangle.
 *
 * The function draws rectangle frame or fully filled rectangle relatively to the current device canvas..
 *
 * Return Value: The function does not return any value.
 *
 * File: sui_gdi.c
 */
void sui_draw_rect_points(sui_dc_t *dc, sui_point_t *origin,
                          sui_point_t *size, sui_coordinate_t frm)
{
  sui_coordinate_t x, y;
  if (size->x <= 0 || size->y <= 0) return; /* draw only positive width and height - in sui_base_draw_widget is subtraction */
  x = origin->x + dc->offset.x;
  y = origin->y + dc->offset.y;
  if (!frm) {
    GdFillRect(dc->psd, x, y, size->x, size->y);
  } else {
    if (frm == 1) {
      GdRect(dc->psd, x, y, size->x, size->y);
    } else {
      GdFillRect(dc->psd, x, y, size->x, frm);
      GdFillRect(dc->psd, x, y+size->y-frm, size->x, frm);
      GdFillRect(dc->psd, x, y+frm, frm, size->y-2*frm);
      GdFillRect(dc->psd, x+size->x-frm, y+frm, frm, size->y-2*frm);
    }
  }
}

/**
 * sui_draw_rect_box - Draw rectangle according to its coordinates
 * @dc: Pointer to a device context.
 * @rect: Pointer to rectangle's position and dimensions.
 * @frm: Size of the rectangle frame, for 0 for filled rectangle.
 *
 * The function draws rectangle frame or fully filled rectangle relatively to the current device canvas..
 *
 * Return Value: The function does not return any value.
 *
 * File: sui_gdi.c
 */
void sui_draw_rect_box(sui_dc_t *dc, sui_rect_t *rect, sui_coordinate_t frm)
{
  if (rect->w <= 0 || rect->h <= 0) return; /* draw only positive width and height - in sui_base_draw_widget is subtraction */
  sui_coordinate_t x, y;
  x = rect->x + dc->offset.x;
  y = rect->y + dc->offset.y;
  if (!frm) {
    GdFillRect(dc->psd, x, y, rect->w, rect->h);
  } else {
    if (frm == 1) {
      GdRect(dc->psd, x, y, rect->w, rect->h);
    } else {
      GdFillRect(dc->psd, x, y, rect->w, frm);
      GdFillRect(dc->psd, x, y+rect->h-frm, rect->w, frm);
      GdFillRect(dc->psd, x, y+frm, frm, rect->h-2*frm);
      GdFillRect(dc->psd, x+rect->w-frm, y+frm, frm, rect->h-2*frm);
    }
  }
}
