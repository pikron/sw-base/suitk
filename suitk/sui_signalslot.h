/* sui_signalslot.h
 *
 * SUITK signal and slots' identifiers
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#ifndef _SUITK_SIGNAL_SLOT_ID_SYSTEM_
  #define _SUITK_SIGNAL_SLOT_ID_SYSTEM_

/******************************************************************************
 * suitk signal-slot enumeration
 ******************************************************************************/
/**
 * enum suitk_slots - Suitk slots' identifiers
 * @SUI_SLOT_NONE: "Black hole" slot.
 * @SUI_SLOT_SHOW: Make object visible (show widget).
 * @SUI_SLOT_HIDE: Make object invisible (hide widget).
 * @SUI_SLOT_ENABLE: Enable object (widget).
 * @SUI_SLOT_DISABLE: Disable object (widget).
 * @SUI_SLOT_SETFOCUS: Set focus to the widget.
 * @SUI_SLOT_CLEARFOCUS: Remove focus from the widget.
 * @SUI_SLOT_MOVE: Move widget to a new position.
 * @SUI_SLOT_RESIZE: Resize widget with a new dimensions.
 * @SUI_SLOT_SETFOCUSEN: Enable/Disable widget focusability.
 * @SUI_SLOT_UPDATE: Invoke widget (wlistbox, wdyntext) refreshing.
 * @SUI_SLOT_SETLABEL: Set/Change widget label.
 * @SUI_SLOT_TOGGLE: Toggle widget button (checkbox, radio, ...).
 * @SUI_SLOT_SETFILTER: Set wlistbox text filter
 * @SUI_SLOT_APPEND: Append text at the end of a wtext's text.
 * @SUI_SLOT_INSERT: Insert text into a wtext's text.
 * @SUI_SLOT_CLEAR: Remove text from a wtext.
 * @SUI_SLOT_SETTEXT: Set/Change text in wtext.
 * @SUI_SLOT_STATUS_MESSAGE: Set/Change wgroup status message text.
 * @SUI_SLOT_STATUS_CLEAR: Clear wgroup status message text.
 * @SUI_SLOT_APP_SHOW_TIP: Set/Change current application tip's text.
 * @SUI_SLOT_APP_HIDE_TIP: Clear current application tip's text.
 * @SUI_SLOT_SET_BOUNDS: Set widget's value bounds (wscroll).
 * @SUI_SLOT_SET_VALUE: Set widget's value (wscroll).
 *
 * File: sui_signalslot.h
 */
enum suitk_slots {
  SUI_SLOT_NONE = 0,
/* basic widget */
  SUI_SLOT_SHOW,
  SUI_SLOT_HIDE,
  SUI_SLOT_ENABLE,
  SUI_SLOT_DISABLE,
  SUI_SLOT_SETFOCUS,
  SUI_SLOT_CLEARFOCUS,
  SUI_SLOT_MOVE,
  SUI_SLOT_RESIZE,
  SUI_SLOT_SETFOCUSEN,
  SUI_SLOT_BLINKON,
  SUI_SLOT_BLINKOFF,
  SUI_SLOT_KDOWN,  /* call widget event hadler with SUEV_KDOWN */
  SUI_SLOT_BLINKCONTENTON,
  SUI_SLOT_BLINKCONTENTOFF,

  SUI_SLOT_UPDATE,
  SUI_SLOT_SETLABEL,
  SUI_SLOT_TOGGLE,

  SUI_SLOT_SETFILTER,

  SUI_SLOT_APPEND,
  SUI_SLOT_INSERT,
  SUI_SLOT_CLEAR,
  SUI_SLOT_SETTEXT,

  SUI_SLOT_STATUS_MESSAGE,
  SUI_SLOT_STATUS_CLEAR,
//  SUI_SLOT_MODAL_OPEN,
//  SUI_SLOT_MODAL_CLOSE,

  SUI_SLOT_APP_SHOW_TIP,
  SUI_SLOT_APP_HIDE_TIP,

  SUI_SLOT_SET_BOUNDS,
  SUI_SLOT_SET_VALUE,
};

/**
 * enum suitk_signals - Suitk signals' identifiers
 * @SUI_SIG_NONE: Empty signal.
 * @SUI_SIG_SHOWED: Object (widget) was showed.
 * @SUI_SIG_HIDDEN: Object (widget) was hidden.
 * @SUI_SIG_ENABLED: Object (widget) was enabled.
 * @SUI_SIG_DISABLED: Object (widget) was disabled.
 * @SUI_SIG_FOCUSED: Widget obtained focus.
 * @SUI_SIG_UNFOCUSED: Widget lost focus.
 * @SUI_SIG_MOVED: Object (widget) was moved.
 * @SUI_SIG_RESIZED: Object (widget) was resized.
 * @SUI_SIG_UPDATED: Object's (wlistbox, wdyntext) content was updated.
 * @SUI_SIG_LABELCHANGED: Widget's label was changed.
 * @SUI_SIG_PRESSED: Object (wbutton) was pressed.
 * @SUI_SIG_RELEASED: Object (wbutton) was released.
 * @SUI_SIG_CLICKED: It was clicked on an object (widget).
 * @SUI_SIG_STATECHANGED: Object's (wbutton) state was changed.
 * @SUI_SIG_CHANGED: Object (wtext's, wlistbox's content) was changed.
 * @SUI_SIG_CONFIRMED: Object's changes was confirmed.
 * @SUI_SIG_KEYPRESSED: Widget received and didn't process keypress event.
 * @SUI_SIG_ADDED: A new item into a list was added.
 * @SUI_SIG_REMOVED: Some item from list was removed.
 * @SUI_SIG_FILTERCHANGED: Wlistbox's filter was changed.
 * @SUI_SIG_BOUNDS_SET: low and high bound of value have been set (wlistbox number of items for wscroll).
 * @SUI_SIG_VALUE_SET: value has been set (wlistbox position for wscroll).
 * @SUI_SIG_EDIT_STARTED: editing of widget content has been started
 * @SUI_SIG_EDIT_STOPPED: editing of widget content has been stopped
 *
 * File: sui_signalslot.h
 */
enum suitk_signals {
  SUI_SIG_NONE = 0,
  SUI_SIG_SHOWED,
  SUI_SIG_HIDDEN,
  SUI_SIG_ENABLED,
  SUI_SIG_DISABLED,
  SUI_SIG_FOCUSED,
  SUI_SIG_UNFOCUSED,
  SUI_SIG_MOVED,
  SUI_SIG_RESIZED,
  SUI_SIG_UPDATED,
  SUI_SIG_LABELCHANGED,

  SUI_SIG_PRESSED,
  SUI_SIG_RELEASED,
  SUI_SIG_CLICKED,
  SUI_SIG_STATECHANGED,
  SUI_SIG_ACTIVATED,
  SUI_SIG_DEACTIVATED,

  SUI_SIG_CHANGED,
  SUI_SIG_CONFIRMED,

  SUI_SIG_KEYPRESSED,

  SUI_SIG_ADDED,
  SUI_SIG_REMOVED,

  SUI_SIG_FILTERCHANGED,

  SUI_SIG_BOUNDS_SET,
  SUI_SIG_VALUE_SET,

  SUI_SIG_EDIT_STARTED,
  SUI_SIG_EDIT_STOPPED,

  SUI_SIG_MOUSEDOWN,
};

#endif /* _SUITK_SIGNAL_SLOT_ID_SYSTEM_ */
