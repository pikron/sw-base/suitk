/* sui_wscroll.h
 *
 * SUITK scroll (and progress) widget.
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#ifndef _SUI_SCROLL_WIDGET_
#define _SUI_SCROLL_WIDGET_

#include <suitk/suitk.h>

#define SUI_SCROLL_DEFAULT_WIDTH 10


/******************************************************************************
 * Scroll widget extension
 ******************************************************************************/  
/**
 * struct suiw_scroll - Additional scroll widget structure.
 * @datai: Pointer to dinfo connected to scroll widget.
 * @format: Pointer to finfo connected with dinfo to scroll widget (range,step,...).
 * @puller: Puller width (in pixels).
 * File: sui_scroll.h
 */
typedef struct suiw_scroll {
    sui_dinfo_t *datai;   /* set from dinfo (min,max) */
    sui_finfo_t *format;  /* (lstep,bstep) */
    sui_coordinate_t wline;   /* line width */
    sui_coordinate_t wpuller; /* puller width */
    
    long intvalue;
} suiw_scroll_t;

  #define sui_scrwdg( wdg)  ((suiw_scroll_t *)wdg->data)

/**
 * enum scroll_flags - Scroll widget special flags ['SUSF_' prefix]
 * @SUSF_VERTICAL: Widget is vertical scroll. 
 * @SUSF_HORIZONTAL: Widget is horizontal scroll.
 * @SUSF_SHOWBTN: Scroll widget has arrow buttons.
 * @SUSF_SHOWLINE: Scroll widget has central line under puller.
 * @SUSF_SHOWGRID: Scroll widget has grid between arrow buttons.
 * @SUSF_REVERSE: Scroll has reversed axis.
 * @SUSF_REV_ARROW: Scroll has swaped direction of buttons and keys.
 * @SUSF_TYPEMASK: Mask for direction of scroll widget (vertical,horizontal).
 * File: sui_scroll.h
 */
enum scroll_flags {
    SUSF_VERTICAL           = 0x00000000, /* select between VERTICAL & HORIZONTAL direction */
    SUSF_HORIZONTAL         = 0x00010000, 
    
    SUSF_MODE_AUTOSIZE      = 0x00000000,
    SUSF_MODE_FIXEDSIZE     = 0x00020000,
//    SUSF_MODE_FLOATSIZE     = 0x00040000,
    SUSF_MODE_PROGRESS      = 0x00060000,
    
    SUSF_PARENT_SB          = 0x00080000, /* scroll bar get size from parent */

    SUSF_SHOWLINE           = 0x00100000, /* center line (progress bar) is showed */
    SUSF_NOFILLLINE         = 0x00200000, /* center line is transparent */
    SUSF_LEFTTICK           = 0x00400000, /* left ticks are showed */
    SUSF_TOPTICK            = 0x00400000, /* top ticks are showed */
    SUSF_RIGHTTICK          = 0x00800000, /* right ticks are showed */
    SUSF_BOTTOMTICK         = 0x00800000, /* bottom ticks are showed */
    SUSF_BOTHTICKS          = 0x00C00000, /* both ticks are showed */
    SUSF_HIDEBUTTONS        = 0x01000000, /* buttons is hidden */

//    SUSF_ONLINEEDIT

//    SUSF_REV_ARROW          = 0x04000000, /* no supported since v.1.0 */
    SUSF_REVERSE            = 0x08000000, /* for reverse scroll arrow ( <,> ) */ /* changed to reverse axis */
    SUSF_NOTHUMBLINE        = 0x10000000, /* don't draw center line inside thumb */
    
    SUSF_DIRMASK            = 0x00010000,
    SUSF_MODEMASK           = 0x00060000,
};


/******************************************************************************
 * Scroll slot functions
 ******************************************************************************/


/******************************************************************************
 * Scroll widget vmt & signal-slot mechanism
 ******************************************************************************/
typedef sui_widget_t sui_wscroll_t;

#define sui_wscroll_vmt_fields(new_type, parent_type) \
	sui_widget_vmt_fields( new_type, parent_type) \
  int (*setbounds)(parent_type##_t *self, long low, long high); \
  int (*setvalue)(parent_type##_t *self, long value);
// functions have sui_widget_t* as first argument

typedef struct sui_wscroll_vmt {
	sui_wscroll_vmt_fields(sui_wscroll, sui_widget)
} sui_wscroll_vmt_t;

static inline sui_wscroll_vmt_t *
sui_wscroll_vmt(sui_widget_t *self)
{
	return (sui_wscroll_vmt_t*)(self->base.vmt);
}

extern sui_wscroll_vmt_t sui_wscroll_vmt_data;

/******************************************************************************
 * Scroll widget basic functions
 ******************************************************************************/

sui_widget_t *sui_wscroll( sui_rect_t *aplace, utf8 *alabel, sui_style_t *astyle, 
              sui_flags_t aflags, sui_dinfo_t *adata, sui_finfo_t *aformat,
              sui_coordinate_t apullersize, sui_coordinate_t alinesize);

#endif
