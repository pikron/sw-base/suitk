/* sui_wnumber.h
 *
 * Header file for the SUITK number widget.
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#ifndef _SUI_NUMBER_WIDGET_
#define _SUI_NUMBER_WIDGET_

#include <suitk/suitk.h>


/******************************************************************************
 * Number widget extension
 ******************************************************************************/
/**
 * struct suiw_number - Number widget specific sub-structure  
 * @data:     Pointer to a dinfo which contains a number connected with the number widget
 * @format:   Pointer to a number format structure which contains information about number format used in the number widget
 * @suffix:   Pointer to a suffix structure which contains extension of number - alignment and suffix text
 * @align:    Common number alignment inside the widget area
 * @edflg:    flags for editing
 * @intvalue: here is saved original number when editing mode is started
 * @step:     current value of step size for incrementing/decrementing number by arrow keys 
 * @str_number: text representation of the value
 * @nint:     number of integer digits in the number (with minus, decimal point)
 * @ndec:     number of decimal digits in the number
 * @baseline: base line of the integer part of the number
 * File: sui_wnumber.h
 */
typedef struct suiw_number {
  sui_dinfo_t   *data;     /* dinfo */
  sui_finfo_t   *format;   /* finfo */
  sui_suffix_t  *suffix;   /* suffix format */
  sui_align_t    align;    /* number aligned */
/* next fields are for editing */
  unsigned long  eflg;      /* edited flags - minus, ...  */
  long           intvalue;   /* saved previous confirmed value */
  long           step;       /* the current step size for incementing and decrementing by arrow keys */
/* transformed number to string in an instance */
  char          *str_number; /* pointer to buffer with transformed number to text (only ASCII characters) */
  int            nint;       /* number of integer digits in transformed number (with all characters - minus,...) */
//  int            odec;       /* offset of number decimal part in buffer (after decimal point) */
  int            ndec;       /* number of decimal digits in transformed number */
  int            baseline;   /* number integer part base line for aligning */
} suiw_number_t;

#define sui_numwdg( wdg)  ((suiw_number_t *)wdg->data)

/**
 * enum wnumber_flags - Number widget special flags.
 * @SUNF_CLEARNOEDITEN: Number can be cleared (set to zero) by MWKEY_DELETE or MWKEY_BACKSPACE if widget isn't in edit mode (SUFL_EDITEN).
 * @SUNF_ARROWKEY:      Allow using arrow keys for editing Number is edited.
 * @SUNF_SUFFIX:        Show the suffix text behind the number.
 * @SUNF_HIDEPOINT:     Decimal point won't showed.
 * @SUNF_DECIMALZEROS:  Number is fill up by zeros to empty digits after decimal point.
 * @SUNF_FILLUPBYZERO:  Number is fill up by zeros to empty digits before decimal point.
 * @SUNF_CONFIRMARROW:  Changes which are made by arrow keys must be confirmed to valid them.
 * @SUNF_CONFIRMEDIT:   Widget doesn't lose SUFL_EDITED flag if widget is un-focused.
 * @SUNF_ONLINEEDIT:    Widget will be edited on-line opposite off-line editing (editing in widget until it receives SUCM_CONFIRM command).
 * @SUNF_CHECKLIMITS:   Limits are checking in edit mode - lower(upper) boundary of number will be set when editer number is lower or higher then boundaries.
 * @SUNF_MUSTBEVALID:   Only valid number (between number limit boundaries) can be confirmed.
 *
 * File: sui_number.h
 */  
enum wnumber_flags {
//    SUNF_EDITEACHNUMBER=0x00010000, /* each digit of number will be edited separately in order */
    SUNF_CLEARNOEDITEN= 0x00020000, /* number can be cleared by MWKEY_DELETE or MWKEY_BACKSPACE if widget isn't SUFL_EDITEN */
    SUNF_ARROWKEY     = 0x00040000, /* widget accept arrow keys - change value immediately */
    SUNF_SUFFIX       = 0x00080000, /* number has suffix string */
    SUNF_HIDEPOINT    = 0x00100000, /* don't show decimal point( if number is float (fdigit != 0)) */
    SUNF_DECIMALZEROS = 0x00200000, /* zeros in all position after decimal point */
    SUNF_FILLUPBYZERO = 0x00400000, /* fill up number by zeros to all digits(befor decimal point) */
    SUNF_SHOWLIMITS   = 0x20000000, /* if the number is lower than its bound - show 'LOW' and if it is higher than its bound - show 'HIGH' */
    SUNF_CONFIRMARROW = 0x01000000, /* changes made by arrow keys must be confirm */
    SUNF_CONFIRMEDIT  = 0x02000000, /* widget doesn't lose edited flag on SUEV_FOCUSREL with this flag */
    SUNF_ONLINEEDIT   = 0x04000000, /* value will be editen only inside widget and dinfo will be updated after SUCM_CONFIRM */
    SUNF_CHECKLIMITS  = 0x08000000, /* limits are checking in edit mode, with this flag minimum(maximum) will be set when edited number is smaller or larger then limit */
    SUNF_MUSTBEVALID  = 0x10000000, /* only valid (inside limit boundaries) number can be confirmed */
};

/*
 * struct sui_wnumber_dims - internal structure with wnumber's dimensions
 */
typedef struct sui_wnumber_dims {
  sui_point_t integer;
  sui_point_t decimal;
  sui_point_t suffix;
  sui_textdims_t number;
} sui_wnumber_dims_t;

/**
 * enum wnumber_edit - Flags for editing number in number widget. ['SUNEF_' prefix]
 * @SUNEF_MINUS: Edited number is negative. (number has minus)
 * @SUNEF_POINT: Edited number has decimal point.
 * @SUNEF_DIGMASK: Mask for count digits in number.
 *
 * File: sui_wnumber.h
 */
enum wnumber_edit_flags {
  SUNEF_BVMODE= 0x80000000, /* value is out of range - when user tried to confirm invalid value */
  SUNEF_ARROW = 0x40000000, /* value was changed by pressing some arrowkey - when number is pressed value will be cleared */

  SUNEF_POINT = 0x00010000, /* edited number contains decimal point - position is number of decimal digits*/
  SUNEF_MINUS = 0x00020000, /* edited number is negative - minus will be added */
  SUNEF_INEDIT= 0x00040000, /* edited number is in editing process */
  SUNEF_INIT  = 0x00080000, /* special flag for number widget initialization */

  SUNEF_NPLUS = 0x00000001, /* it increases number of all entered digits */
  SUNEF_NMASK = 0x000000FF, /* mask for number of all entered digits in the number */

  SUNEF_DPLUS = 0x00000100, /* it incerases/decreases number of entered decimal digits in the number */
  SUNEF_DMASK = 0x0000FF00, /* mask for number of entered decimal digits in the number */
  SUNEF_DSHIFT= 8, 
};

/******************************************************************************
 * Number slot functions
 ******************************************************************************/
//int sui_wnumber_clear(sui_widget_t *widget);
//int sui_wnumber_set(sui_widget_t *widget, long value);


/******************************************************************************
 * Number widget vmt & signal-slot mechanism
 ******************************************************************************/
typedef sui_widget_t sui_wnumber_t;

#define sui_wnumber_vmt_fields(new_type, parent_type) \
	sui_widget_vmt_fields( new_type, parent_type)
//	int (*toggle)(parent_type##_t *self);
// functions have sui_widget_t* as first argument

/*
 * struct sui_wnumber_vmt - Widget number VMT structure
 */
typedef struct sui_wnumber_vmt {
  sui_wnumber_vmt_fields(sui_wnumber, sui_widget)
} sui_wnumber_vmt_t;

/**
 * sui_wnumber_vmt - returns pointer to widget number vmt table of widget instance
 * @self:
 */
static inline
sui_wnumber_vmt_t *sui_wnumber_vmt(sui_widget_t *self)
{
  return (sui_wnumber_vmt_t*)(self->base.vmt);
}

extern sui_wnumber_vmt_t sui_wnumber_vmt_data;

/******************************************************************************
 * Number widget basic functions
 ******************************************************************************/
sui_widget_t *sui_wnumber( sui_rect_t *aplace, utf8 *alabel, sui_style_t *astyle, 
              unsigned long aflags, sui_dinfo_t *adata, sui_finfo_t *aformat, 
              sui_align_t aalign, sui_suffix_t *asuffix);

#endif
