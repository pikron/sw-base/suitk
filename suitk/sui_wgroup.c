/* sui_wgroup.c
 *
 * SUITK group widget.
 *
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#include "sui_wgroup.h"

#include <ul_log.h>

UL_LOG_CUST(ulogd_suitk)

/******************************************************************************
 * Group widget functions
 ******************************************************************************/
/* TODO: absolute position of the widget - it is placed after N children (order & SUGO_ORDER_MASK) */
/**
 * sui_group_add_widget - Add widget to the group
 * @group: Pointer to a group widget.
 * @widget: Pointer to an added widget.
 * @order: Order of a new widget between widgets (flags with SUGO_ prefix and/or number of widgets before/after the added one).
 *
 * The function adds the widget to the group and increments its reference counter.
 * Only one modal group can be added to each group.
 *
 * Return Value: The function returns a negative value if any error occurs, otherwise it returns zero.
 *
 * File: sui_wgroup.c
 */
int sui_group_add_widget(sui_widget_t *group, sui_widget_t *widget, int order)
{
  suiw_group_t *wpar;
  int ret = 0;

  if(!widget || !group || group->type!=SUWT_GROUP) return -1;
  wpar = sui_grpwdg(group);

  sui_inc_refcnt(widget); /* temporary - increment refcnt */

  if(widget->parent) { /* remove widget from its previous parent */
    if ((ret = sui_group_remove_widget(widget))!=0) {
      sui_dec_refcnt(widget); /* temporary - decrement refcnt */
      return ret;
    }
  }

  if (wpar->children) {
    if (sui_test_flag(group, SUGF_FIRSTCHILDISMODAL)) { /* parent group already contains modal group */
      if (order & SUGO_MODAL_WIDGET) { /* added widget is modal - it is an error, because only one modal widget can be under each group */
        sui_dec_refcnt(widget);
        return -1;
      }
      if ((order & (SUGO_RELATIVE_ORDER | SUGO_ORDER_MASK))==SUGO_FIRST) { /* add a new widget as the first one - after modal widget */
        widget->next = wpar->children;
        widget->prev = wpar->children->prev;
      } else {
        widget->next = wpar->children->next;
        widget->prev = wpar->children;
      }
    } else {
      widget->next = wpar->children->next;
      widget->prev = wpar->children;
      if (order & SUGO_MODAL_WIDGET) { /* add a modal widget to the parent group */
        wpar->children = widget;
        sui_set_flag(group, SUGF_FIRSTCHILDISMODAL);
      } else if ((order & (SUGO_RELATIVE_ORDER | SUGO_ORDER_MASK))==SUGO_FIRST) { /* add a new widget as the first one */
        wpar->children = widget;
      } else { /* add widget to at the end */
        // do nothing special
      }
    }
    widget->next->prev = widget;
    widget->prev->next = widget;
  } else {
    widget->next = widget;
    widget->prev = widget;
    wpar->children = widget;
    if (order & SUGO_MODAL_WIDGET) {
      sui_set_flag(group, SUGF_FIRSTCHILDISMODAL);
    }
  }
  sui_inc_refcnt(widget); /* widget is added to the parent group */
  widget->parent = group;

  sui_dec_refcnt(widget); /* temporary - decrement refcnt */
  return 0;
}

/**
 * sui_group_add_widget_dec_refcnt - Pass widget to the group - release reference
 * @group: Pointer to a group widget.
 * @widget: Pointer to an added widget.
 * @order: Order of a new widget between widgets (flags with SUGO_ prefix and/or number of widgets before/after the added one).
 *
 * The function adds the widget to the group. The original reference
 * is passed to the group and the widget pointer should not be used any more
 * by the function caller.
 *
 * Return Value: The function returns a negative value if any error occurs, otherwise it returns zero.
 *
 * File: sui_wgroup.c
 */
int sui_group_add_widget_dec_refcnt(sui_widget_t *group, sui_widget_t *widget, int order)
{
  int ret;
  ret = sui_group_add_widget(group, widget, order);
  sui_dec_refcnt(widget); /* temporary - decrement refcnt */
  return ret;
}

/**
 * sui_group_reclaim_children - Remove widget from specified group
 * @group: Pointer to a group widget.
 * @widget: Pointer to the widget which will be deleted from the group widget.
 *
 * The function removes the widget from group. The widget reference ownership
 * is transferred to the callee of the function.
 *
 * Return Value: The function returns a negative value as an error or zero as success.
 *
 * File: sui_wgroup.c
 */
int sui_group_reclaim_children(sui_widget_t *group, sui_widget_t *widget)
{
  suiw_group_t *gpar;

  if (widget->parent != group)
    return -1;

  gpar = sui_grpwdg(group);

/* remove modal widget */
  if (gpar->children == widget) {
    if (sui_test_flag(widget->parent, SUGF_FIRSTCHILDISMODAL)) {
      sui_clear_flag(widget->parent, SUGF_FIRSTCHILDISMODAL);
    }
    /* correct gpar->children */
    if (widget == widget->prev) /* only this widget is in the group list*/
      gpar->children = NULL;
    else
      gpar->children = widget->prev;
  }
/* correct pointer to focused widget and update modal widget */
  if (widget == gpar->focus)
    gpar->focus = NULL; /* FIXME: update focus correctly to focusable widget */
/* set the group status widget to NULL if it is the removed widget */
  if (widget == gpar->status)
    gpar->status = NULL;

/* disconnect widget from the list */
  if (widget->prev) widget->prev->next = widget->next;
  if (widget->next) widget->next->prev = widget->prev;
  widget->next = NULL;
  widget->prev = NULL;
  widget->parent = NULL;

  return 0;
}

/**
 * sui_group_remove_widget - Remove widget from parent group
 * @widget: Pointer to the widget which will be deleted from the group widget.
 *
 * The function removes the widget from its parent group. The widget reference counter
 * is decremented.And if it is need, focus will be changed to next widget in the group.
 *
 * Return Value: The function returns a negative value as an error or zero as success.
 *
 * File: sui_wgroup.c
 */
int sui_group_remove_widget(sui_widget_t *widget)
{
  sui_widget_t *parentwdg = widget->parent;

  if(!widget || !parentwdg) return -1;

  sui_inc_refcnt(widget); /* temporary - inc.refcnt */

  if (sui_wgroup_vmt(parentwdg)->reclaim_children(parentwdg, widget) < 0) {
    sui_dec_refcnt(widget); /* temporary - dec.refcnt */
    return -1;
  }

  if(sui_dec_refcnt(widget)>0) /* widget is removed from its parent */
    sui_dec_refcnt(widget); /* temporary - dec.refcnt */

  return 0;
}

/**
 * sui_group_add_modal - Add modal widget to the group widget
 * @group: Pointer to a group widget.
 * @widget: Pointer to a modal widget.
 *
 * The function adds the a widget as the most important modal widget to the group and increments its reference counter.
 *
 * Return Value: The function returns a negative value if any error occurs, otherwise it returns zero.
 *
 * File: sui_wgroup.c
 */
inline
int sui_group_add_modal(sui_widget_t *group, sui_widget_t *modal)
{
  return sui_group_add_widget(group, modal, SUGO_FIRST | SUGO_MODAL_WIDGET);
}

/**
 * sui_group_remove_modal - Remove modal widget from parent group
 * @group: Pointer to parent group of modal widget for removing.
 *
 * The function removes the first modal widget from the parent group.
 *
 * Return Value: The function returns a negative value if any error occurs, otherwise it returns zero.
 *
 * File: sui_wgroup.c
 */
int sui_group_remove_modal(sui_widget_t *group)
{
  if (!group || (group->type != SUWT_GROUP) || 
      !sui_test_flag(group, SUGF_FIRSTCHILDISMODAL)) 
    return -1;
  return sui_group_remove_widget(sui_grpwdg(group)->children);
}


/**
 * sui_group_change_widget_order - Change order of the group children
 * @widget: Pointer to the widget which order will be changed.
 * @from: Pointer to the reference widget for changing according to relative order. It can be NULL.
 * @order: Desired order and flags (see constants with prefix SUGO_ ).
 *
 * The function changes widget order under parent group
 *
 * Return Value: The function returns zero as success and a negative value as an error.
 *
 * File: sui_wgroup.c
 */
int sui_group_change_widget_order(sui_widget_t *widget, sui_widget_t *from, int order)
{
// //   suiw_group_t *wpar;
// //   sui_widget_t **list;
// //   if (!widget || !widget->parent) return -1;
// //   wpar = sui_grpwdg(widget->parent);
// // 
// //   list = &(wpar->children);
// // 
// //   if (order & SUGO_RELATIVE_ORDER) { /* relative order */
// //     if (((order == SUGO_LAST) && (widget != *list)) ||
// //         ((order == SUGO_FIRST) && (widget != (*list)->prev))) { /* widget will be first or last widget in the list after the change */
// //       widget->prev->next = widget->next; /* cut off widget from chain */
// //       widget->next->prev = widget->prev;
// // 
// //       widget->next = *list;       /* paste on between last and first widget in the list */
// //       widget->prev = (*list)->prev;
// //       widget->prev->next = widget;
// //       widget->next->prev = widget;
// //       if (order == SUGO_LAST) *list = widget;
// // 
// //     } else if (order == SUGO_ONE_BACK) {
// //       if (!from) {
// //         if (widget==*list) return -1; /* we are on the last widget */
// //         from = widget->prev;
// //       }
// //       if ((from->parent != widget->parent) || (from==*list)) return -1;
// //       widget->prev->next = widget->next; /* cut off widget from chain */
// //       widget->next->prev = widget->prev;
// //       widget->prev = from->prev;       /* paste on widget to chain */
// //       widget->next = from;
// //       widget->next->prev = widget;
// //       widget->prev->next = widget;
// //       if (from==*list) *list = widget; /* widget is the last one now */
// // 
// //     } else if (order == SUGO_ONE_FRONT) {
// //       if (!from) {
// //         if (widget->next==*list) return -1; /* we are on the first widget */
// //         from = widget->next;
// //       }
// //       if ((from->parent != widget->parent) || (from==(*list)->prev)) return -1;
// //       if (widget==*list) *list=widget->next; /* widget was the last one */
// //       widget->prev->next = widget->next; /* cut off widget from chain */
// //       widget->next->prev = widget->prev;
// //       widget->prev = from;         /* paste on widget to chain */
// //       widget->next = from->next;
// //       widget->next->prev = widget;
// //       widget->prev->next = widget;
// //     } else
// //       return -1;
// //   } else {
// //     /* TODO: absolute position of widget ... */
// //     return -1;
// //   }
  return 0;
}


/******************************************************************************/
/**
 * sui_group_devent - Call widget handler with group event
 * @group: Pointer to a group widget.
 * @widget: Pointer to a widget.
 * @event: Pointer to an event.
 *
 * The function calls widget hevent function for group event.
 *
 * Return Value: The function returns 1 if event was accepted, -1 if some error occurs and zero as success.
 *
 * File: sui_wgroup.c
 */
int sui_group_devent(sui_widget_t *group, sui_widget_t *widget, sui_event_t *event)
{
  if (sui_test_flag(widget, SUFL_DISABLED) && !is_init_event(event)) return -1;
  if (event->what == SUEV_NOTHING) return 0;
  sui_hevent(widget, event);
  if (event->what == SUEV_NOTHING) return 1;
  return 0;
}

/**
 * sui_group_foreach - Distribute event to each group descendant widget
 * @group: Pointer to a group widget.
 * @widget: Pointer to the first widget or NULL.
 * @dir: Direction of a distribution. A positive value or zero is for forward direction. A negative value for backward direction.
 * @fnc: Pointer to distribute function.
 * @event: Pointer to event.
 *
 * The function calls the distribute function for each descendant of the group widget.
 * The function doesn't call the distribute function for modal (group) widgets, they must be
 * called in another way.
 *
 * Return Value: The function returns 1 if all childs accept event.
 *
 * File: sui_wgroup.c
 */
int sui_group_foreach(sui_widget_t *group, sui_widget_t *from, int dir,
                      sui_group_foreach_fnc_t *fnc, sui_event_t *event)
{
  sui_widget_t *widget, *next;
  sui_event_t e;
  int ret = 1, oneret;
  if (!from) {
    from = sui_grpwdg(group)->children;
    if (!from) return -1;
    if (dir >= 0) from = from->next;
  } else {
    if (from->parent!=group) return -1;
  }
  widget = from;
  sui_inc_refcnt(from);
  do {
    sui_inc_refcnt(widget);
    e = *event;
    oneret = fnc(group, widget, &e);
    if (oneret < 0) { ret = oneret; break; }
    ret &= oneret;
    if (dir < 0)
      next = widget->prev;
    else
      next = widget->next;
    sui_dec_refcnt(widget);
    widget = next;
  } while (ret && (widget != from));
  sui_dec_refcnt(from);
  return ret;
}


/******************************************************************************/
/**
 * sui_group_find_focused_widget - Find focused widget from a root
 * @root: Pointer to a root widget.
 * @level: Pointer to output buffer for level or NULL.
 *
 * The function tries find the focused widget from the root widget and
 * returns pointer to it or NULL. If the focused widget is found its level from root
 * can be set into output bufer ($level).
 *
 * Return Value: The function returns pointer to the focused widget or NULL.
 *
 * File: sui_wgroup.c
 */
sui_widget_t *sui_group_find_focused_widget(sui_widget_t *root, int *level)
{
  int lev = 0;
  sui_widget_t *out = NULL;
  while(root) {
    if (!(root->flags & SUFL_FOCUSED)) break;
    lev++;
    out = root;
    if (root->type==SUWT_GROUP) {
      if (sui_test_flag(root, SUGF_FIRSTCHILDISMODAL))
        root = sui_grpwdg(root)->children;
      else if (sui_grpwdg(root)->focus)
        root = sui_grpwdg(root)->focus;
      else
        break; /* no next focused widget*/
    } else
      break;
  }
  if (out && level)
    *level = lev;
  return out;
}

/**
 * sui_group_count_widget_level - Count widget level from the root widget
 * @wdg: Pointer to the tested widget or to the root.
 * @root2leaf: If it is set, $wdg is set to the root widget and we count level of the focused widget.
 *             Otherwise $wdg contains a widget which level we are finding.
 *
 * The function counts either level of the focused widget from the root or level of the entered widget
 * to the root.
 *
 * Return Value: The function returns level of entered or focused widget.
 *
 * File: sui_wgroup.c
 */
int sui_group_count_widget_level(sui_widget_t *wdg, int root2leaf)
{
  int ret = 0;
  if (root2leaf) {
    sui_group_find_focused_widget(wdg, &ret);
  } else {
    while (wdg) {
      ret++;
      wdg = wdg->parent;
    }
  }
  return ret;
}

/**
 * sui_group_only_set_focus - Set focus to widget only
 * @wdg: Pointer to widget which will have got focus.
 * @level: Level of a virtual root or -1 for real root.
 *
 * The function sets focus for a widget from root (real or virtual with $level) to the widget.
 * If the modal group is focused on the way, the focus from the parent group of the modal widget
 * won't be changed - it must be cleared by the 'sui_group_only_clear_focus' function before
 * focus setting.
 *
 * Return Value: The function returns zero as success.
 *
 * File: sui_wgroup.c
 */
int sui_group_only_set_focus(sui_widget_t *wdg, int level)
{
  int i, ret = 0, lev = sui_group_count_widget_level(wdg, 0);
  sui_widget_t **arr = NULL, *tmp;

  if (lev>level && level>= 0) lev = level; // jen omezeny pocet prvku od zadaneho prvku

  if (lev) {
    arr = malloc(lev*sizeof(sui_widget_t *));
    tmp = wdg; i = lev;
    while (i--) {
      if (!tmp) { ret = -1; break;}
      *(arr+i)=tmp;
      tmp = tmp->parent;
    }
    if (ret==0) {
/* set focus from root to wdg */
      i = 0;
      while(i<lev) {
        tmp = *(arr+i++);
        if (sui_hevent_command(tmp, SUCM_FOCUSASK, NULL, 0)) { ret = 1; break;}
        sui_hevent_command(tmp, SUCM_FOCUSSET, NULL, 0);
ul_logdeb("SetFocus '%s'\n", tmp ? sui_utf8_get_text(tmp->label) : "-");

        if (tmp->parent && (tmp->parent->type == SUWT_GROUP)) {
          if (!((sui_grpwdg(tmp->parent)->children==tmp) &&
              sui_test_flag(tmp->parent, SUGF_FIRSTCHILDISMODAL))) {
            sui_grpwdg(tmp->parent)->focus = tmp;
          }
        }
      }
    } else {
      ret = -1; /* not all widgets ?! in the array */
    }
    if (arr) free(arr);
  }
  return ret;
}

/**
 * sui_group_only_clear_focus - Remove focus from widget only
 * @wdg: Pointer to widget which won't have got a focus.
 * @level: Level of root or -1 for true root.
 *
 * The function clears focus for widget from root (real or virtual with $level) to widget.
 *
 * Return Value: The function returns zero as success.
 *
 * File: sui_wgroup.c
 */
int sui_group_only_clear_focus(sui_widget_t *wdg, int level)
{
  int ret = 0;
  while (wdg) {
    if (level==0) break;
    sui_hevent_command(wdg, SUCM_FOCUSREL, NULL, 0);
ul_logdeb("ClearFocus '%s'\n", wdg ? sui_utf8_get_text(wdg->label) : "-");

    if (wdg->parent && (wdg->parent->type == SUWT_GROUP)) {
      if (!((sui_grpwdg(wdg->parent)->children==wdg) &&
             sui_test_flag(wdg->parent, SUGF_FIRSTCHILDISMODAL))) {
        sui_grpwdg(wdg->parent)->focus = NULL;
      }
    }

    if (level>0) level--;
    wdg = wdg->parent;
  }

  return ret;
}

/**
 * sui_group_change_focus - Change focus either from the currently focused widget or from the entered widget to another widget
 * @root: Pointer to the main root widget or NULL for main root widget from $wdg widget.
 * @from: Pointer to a widget with focus or NULL for the currently focused widget.
 * @wdg:Pointer to a widget which will have got focus.
 *
 * The function removes focus from the currently focused widget up to the first common parent
 * widget with newly focused widget and then it sets focus from the first common poarent widget
 * up to desired widget. If some widget cannot be focused, function stops.
 *
 * Return Value: The function returns always zero.
 *
 * File: sui_wgroup.c
 */
int sui_group_change_focus(sui_widget_t *root, sui_widget_t *from, sui_widget_t *wdg)
{
  int lwas = 0, lwill = 0;
  int ltmpwas = 0, ltmpwill = 0;
  sui_widget_t *wwas = NULL, *wwill = NULL;
  sui_widget_t *wtmpwas = NULL, *wtmpwill = NULL;

  if (!root) {
    root = wdg;
    while (root->parent) root = root->parent;
  }
  if (from) {
    wwas = from;
    lwas = sui_group_count_widget_level(from, 0);
  } else {
    wwas = sui_group_find_focused_widget(root, &lwas);
  }
  lwill = sui_group_count_widget_level(wdg, 0);
  wwill = wdg;
  ltmpwas = lwas; ltmpwill = lwill;
  wtmpwas = wwas; wtmpwill = wwill;
  while (wtmpwas && ltmpwas>ltmpwill) { /* adjust levels */
    wtmpwas = wtmpwas->parent;
    ltmpwas--;
  }
  while (wtmpwill && ltmpwill>ltmpwas) { /* adjust levels */
    wtmpwill = wtmpwill->parent;
    ltmpwill--;
  }
  while (wtmpwill && wtmpwas && wtmpwill!=wtmpwas) {
    wtmpwas = wtmpwas->parent;
    wtmpwill = wtmpwill->parent;
    ltmpwas--; ltmpwill--;
  }
  sui_group_only_clear_focus(wwas, lwas-ltmpwas);
  sui_group_only_set_focus(wwill, lwill-ltmpwill);

  return 0;
}

/******************************************************************************/
#define sui_isfocwdg(wdg) (sui_is_flag(wdg, SUFL_DISABLED | SUFL_INVISIBLE | SUFL_FOCUSEN, SUFL_FOCUSEN))
#define sui_ismodwdg(wdg) (wdg->parent && sui_grouphasmodal(wdg->parent) && (wdg==sui_grpwdg(wdg->parent)->children))
#define sui_isclosegrp(wdg) (wdg->type == SUWT_GROUP && sui_is_flag(wdg, SUFL_DISABLED | SUFL_INVISIBLE | SUGF_FOCUSONLYINSIDE, SUGF_FOCUSONLYINSIDE))

#define DIR_DOWN 1
#define DIR_NEXT 2
#define DIR_UP   4
#define DIR_PREV 8
#define DIR_TEST 16

/* searching algorithm */
/**
 * sui_group_find_next - Find the next/previous focusable widget
 * @root: Pointer to the main root widget.
 * @from: Pointer to a widget from which we will search.
 * @backward: If it is set, searching is in backward direction.
 * @focused: If it is set, the function will be search widget with set FOCUSED flag.
 *
 * The function find the first next/previous focusable widget from the entered one.
 *
 * Return Value: The function returns pointer to the next widget or NULL.
 *
 * File: sui_wgroup.c
 */
sui_widget_t *sui_group_find_next(sui_widget_t *root, sui_widget_t *from, int backward, int focused)
{
  sui_widget_t *wdg; /* currently tested widget */
  sui_widget_t *loop = NULL; /* root of the last internal loop (modal widget or group with FOCUSINLYINSIDE flag) */

  int found = 0; /* a widget was selected flag */
  int dir = DIR_DOWN;
  int lastdir = 0;
  int skip = 0;
  
  if (!from) {
    from = root; 
    dir = DIR_TEST;
  }
  /* we will look for next/prev.widget from the root or the entered widget */
  wdg = from;
  /* the enter widget is the root of a widget subtree */
  if (sui_isfocwdg(wdg) && (sui_isclosegrp(wdg) || sui_ismodwdg(wdg))) {
    loop = wdg;
  }

  /* Next group search mode */
  /* FIXME: mode specification  */
  if(focused == 2) {
    while((wdg->parent != NULL) && (wdg->parent->parent != NULL) &&
         !(sui_isfocwdg(wdg) && (sui_isclosegrp(wdg) || sui_ismodwdg(wdg)))) {
      wdg = wdg->parent;
      dir = backward ? DIR_PREV : DIR_NEXT;
    }
    focused = 0;
  }

/*       
  ul_logdeb("Find %s widget from '%s'(%p) [root is '%s' %p] \n", backward ? "previous" : "next",
         wdg ? sui_utf8_get_text(wdg->label) : "-", wdg,
         root ? sui_utf8_get_text(root->label) : "-", root
        );
  if (sui_isfocwdg(wdg))
    ul_logdeb("... WDG is focused ...\n");
  if (sui_isclosegrp(wdg))
    ul_logdeb("... WDG is GRP with FOCUSONLYINSIDE ...\n");
  if (sui_ismodwdg(wdg))
    ul_logdeb("... WDG is modal widget ...\n");
*/

  do {
    switch (dir) {
      case DIR_DOWN:
//        ul_logdeb("* DOWN ('%s' %p) (from='%s' %p)\n", wdg ? sui_utf8_get_text(wdg->label) : "-", wdg, from ? sui_utf8_get_text(from->label) : "-", from);
        if (wdg->type==SUWT_GROUP && sui_grpwdg(wdg)->children) {
          if (!sui_isfocwdg(wdg)) skip++; /* disabled or invisible  group */
          if (!sui_grouphasmodal(wdg) && !backward) { // parent has modal... or 
            wdg = sui_grpwdg(wdg)->children->next;
          } else {
            wdg = sui_grpwdg(wdg)->children;
          }

          dir = DIR_TEST;
        } else {
          dir = backward ? DIR_PREV : DIR_NEXT;
        }
        lastdir = DIR_DOWN;
        break;
      case DIR_NEXT:
//        ul_logdeb("* NEXT ('%s' %p) (from='%s' %p)\n", wdg ? sui_utf8_get_text(wdg->label) : "-", wdg, from ? sui_utf8_get_text(from->label) : "-", from);
        
        if (loop && wdg==loop) {
          found = 1;
          dir = DIR_TEST;
          break;
        }
        
        if (wdg->parent && 
            ((sui_grouphasmodal(wdg->parent) && (wdg->next == sui_grpwdg(wdg->parent)->children)) ||
            (!sui_grouphasmodal(wdg->parent) && (wdg == sui_grpwdg(wdg->parent)->children)))) {
          dir = DIR_UP;
        } else {
          if (!(sui_isfocwdg(wdg) && (sui_isclosegrp(wdg) || sui_ismodwdg(wdg)))) { /* widget isn't root of a closed widget subtree */
            wdg = wdg->next;
          }
          dir = DIR_TEST;
        }
        lastdir = DIR_NEXT;
        break;
      case DIR_PREV:
//        ul_logdeb("* PREV ('%s' %p) (from='%s' %p)\n", wdg ? sui_utf8_get_text(wdg->label) : "-", wdg, from ? sui_utf8_get_text(from->label) : "-", from);
        
        if (loop && wdg==loop) {
          found = 1;
          dir = DIR_TEST;
          break;
        }
        
        if (wdg->parent && (wdg == sui_grpwdg(wdg->parent)->children->next)) { /* at the end of a widget subtree */
          dir = DIR_UP;
        } else {
          if (!(sui_isfocwdg(wdg) && (sui_isclosegrp(wdg) || sui_ismodwdg(wdg)))) { /* widget isn't root of a closed widget subtree */
            wdg = wdg->prev;
          }
          dir = DIR_TEST;
        }
        lastdir = DIR_PREV;
        break;
      case DIR_UP:
//        ul_logdeb("* UP ('%s' %p) (from='%s' %p)\n", wdg ? sui_utf8_get_text(wdg->label) : "-", wdg, from ? sui_utf8_get_text(from->label) : "-", from);

        if (wdg->parent && wdg!=root && !(sui_isclosegrp(wdg) || sui_ismodwdg(wdg))) {
          wdg = wdg->parent;
          if (!sui_isfocwdg(wdg) && (skip>0)) skip--;
        }
        if (wdg==root || wdg->next==wdg || sui_isclosegrp(wdg) || sui_ismodwdg(wdg)) {
          dir = DIR_TEST;
        } else {
          dir = backward ? DIR_PREV : DIR_NEXT;
        }

        if (loop && wdg==loop) { /* we meet a loop root - again the same widget */
          found = 1;
          dir = DIR_TEST;
        }
        lastdir = DIR_UP;
        break;
      
      case DIR_TEST:
        if (!sui_isfocwdg(wdg)) skip++;
//        ul_logdeb("%d -> TEST ('%s' %p)\n", skip, wdg ? sui_utf8_get_text(wdg->label) : "-", wdg);
        if ((wdg == from) && (lastdir!=0)) {
          found = 1;
          break;
        }
        
        if (!skip) { /* the current 'wdg' widget can be tested for focusing */
          if (sui_isclosegrp(wdg) || sui_ismodwdg(wdg)) { /* widget is a new loop root */
            loop = wdg;
          }
/* test widget for FOCUSING - only no group widget or focusable group */
          if ((wdg->type==SUWT_GROUP && sui_test_flag(wdg, SUGF_CANBEFOCUSED)) ||
               (wdg->type!=SUWT_GROUP)) {
            if (focused) { /* we are looking for focused widget */
              if (sui_test_flag(wdg, SUFL_FOCUSED)) {
                found = 1;
                //ul_logdeb("-> FOCUSed ('%s' %p)\n", wdg ? sui_utf8_get_text(wdg->label) : "-", wdg);
              }
            } else { /* we are looking for focusable widget */
              if (!sui_hevent_command(wdg, SUCM_FOCUSASK, NULL, 0)) {
                found = 1;
                //ul_logdeb("-> FOCUSable ('%s' %p)\n", wdg ? sui_utf8_get_text(wdg->label) : "-", wdg);
              }
            }
          }
        }

        if ((wdg->type == SUWT_GROUP) && sui_grpwdg(wdg)->children) { /* next step */
          dir = DIR_DOWN;
        } else {
          dir = backward ? DIR_PREV : DIR_NEXT;
        }
        
        if (!sui_isfocwdg(wdg) && (skip>0)) skip--;
        break;
    }
  } while (!found && wdg);
//  ul_logdeb("-- End on %s\n", (found) ? "found==1" : (wdg ? "wdg==from" : "wdg==NULL"));
  return wdg;
}


/******************************************************************************/
/**
 * sui_group_set_status - Set one of group widget children as status widget
 * @group: Pointer to a group widget.
 * @widget: Pointer to a widget which will be set as the group status widget.
 *
 * The function sets the entered widget as the group status widget.
 *
 * Return Value: The function returns 0 as success and -1 as error
 *
 * File: sui_wgroup.c
 */
int sui_group_set_status(sui_widget_t *group, sui_widget_t *widget)
{
  suiw_group_t *wgrp;
  if (!group) return -1;
  wgrp = sui_grpwdg(group);
  if (widget) {
    if (widget->parent != group) return -1;
    wgrp->status = widget;
  } else {
    wgrp->status = NULL;
  }
  return 0;
}

/******************************************************************************
 * Slot functions
 ******************************************************************************/
/**
 * sui_group_status_message - Slot function sends message to status widget
 * @group: Pointer to a group widget.
 * @message: A UTF8 message.
 *
 * The function sends message to the group status widget if it is set.
 *
 * Return Value: The slot function returns zero as success and -1 as an error.
 *
 * File: sui_wgroup.c
 */
int sui_group_status_message(sui_widget_t *group, utf8 *message)
{
  sui_event_t gev;
  if (!group) return -1;
  if (!sui_grpwdg(group)->status) return 0; /* group without status */
  gev.what = SUEV_COMMAND;
  gev.message.command = SUCM_SETTEXT;
  gev.message.ptr = message;
  gev.message.info = -1; /* no index - for text box only */
  sui_hevent(sui_grpwdg(group)->status, &gev);
  if (gev.what == SUEV_NOTHING) return 0;
  return -1;
}

/**
 * sui_group_status_clear - Slot function clears status widget
 * @group: Pointer to a group widget.
 *
 * The slot function clears content of the group status widget.
 *
 * Return Value: The slot function returns zero as success and -1 as an error.
 *
 * File: sui_wgroup.c
 */
int sui_group_status_clear(sui_widget_t *group)
{
  sui_event_t gev;
  if (!group) return -1;
  if (!sui_grpwdg(group)->status) return 0;
  gev.what = SUEV_COMMAND;
  gev.message.command = SUCM_CLEARTEXT;
  sui_hevent(sui_grpwdg(group)->status, &gev);
  if (gev.what == SUEV_NOTHING) return 0;
  return -1;
}

/**
 * sui_group_modal_open - Slot function adds and initiates modal widget under group
 * @group: Pointer to a group widget.
 * @modal: Pointer to a widget which will become modal widget.
 *
 * The slot function adds a new modal widget to the group widget and then initiates it.
 *
 * Return Value: The slot function returns zero as success and -1 as an error and 1 as an warning.
 *
 * File: sui_wgroup.c
 */
int sui_group_modal_open(sui_widget_t *group, sui_widget_t *modal)
{
  if (!sui_group_add_widget(group, modal, SUGO_FIRST | SUGO_MODAL_WIDGET)) {
    sui_event_t ev;
    ev.what = SUEV_COMMAND;
    ev.message.command = SUCM_INIT;
    ev.message.ptr = sui_globdc;
    sui_hevent(modal, &ev);
    if (ev.what == SUEV_NOTHING) return 0; /* added and initiated OK */
    else return 1; /* problem with INIT event */
  }
  return -1;
}

/**
 * sui_group_modal_close - Slot function removes modal widget from list of all modals
 * @group: Pointer to a modal widget's parent.
 * @retcode: A close return code.
 *
 * The slot function removes modal widget from its parents. And then put SUCM_CLOSE command event
 * with closing return code.
 *
 * Return Value: The slot function returns zero as success and -1 as an error.
 *
 * File: sui_wgroup.c
 */
int sui_group_modal_close(sui_widget_t *group, int retcode)
{
  int ret = sui_group_remove_modal(group);
/*  if (!ret) {
    sui_event_t ev;
    ev.what = SUEV_COMMAND;
    ev.message.command = SUCM_CLOSE;
    ev.message.info = retcode;
    sui_put_event(&ev);
  }*/
  return ret;
}


/******************************************************************************/
 
/**
 * sui_wgroup_get_size - Get content size of the group widget
 * @dc: Pointer to a device context.
 * @widget: Pointer to a widget.
 * @point: Pointer to an output point structure.
 *
 * The function obtain size of widget content and then returns it.
 * Thes function replys to the SUCM_GETSIZE event.
 *
 * Return Value: The function returns 1 as the success.
 *
 * File: sui_wgroup.c
 */
int sui_wgroup_get_size(sui_dc_t *dc, sui_widget_t *widget, sui_point_t *point)
{
  sui_widget_t     *current = sui_grpwdg(widget)->children;
  sui_coordinate_t  wmax = 0, hmax = 0;
  sui_rect_t       *box;

  if (current && sui_test_flag(widget, SUGF_FIRSTCHILDISMODAL)) /* widget contains modal widget -> we skip it. */
    current = current->prev;
  while (current) {
    /* FIXME: no perfect - outside label must be counted too */
    box = &(current->place);
    if ((box->x + box->w) > wmax) wmax = box->x + box->w;
    if ((box->y + box->h) > hmax) hmax = box->y + box->h;
    current = current->prev;
    if (current == sui_grpwdg(widget)->children) break;
  }
  if (point) {
    point->x = wmax;
    point->y = hmax;
  }
  return 1;
}

/**
 * sui_wgroup_init - Initiate the group widget.
 * @widget: Pointer to a widget.
 * @event: Pointer to an INIT event.
 *
 * The function initiates the group widget (i.e. It initiates all its children).
 * And 
 *
 * Return Value: The function returns 1 when the event is processed and -1 as an error.
 *
 * File: sui_wgroup.c
 */
void sui_wgroup_init(sui_widget_t *widget, sui_event_t *event)
{
/* set default values */
  widget->evmask |= SUEV_GRPMASK;
  sui_group_foreach(widget, NULL, 0, sui_group_devent, event); /* send event to all children */

//   if (sui_test_flag(widget, SUFL_FOCUSED) && sui_grpwdg(widget)->children) {
//     sui_widget_t *chld = sui_grpwdg(widget)->children; /* go from FIRST child (i.e. last in children list) */
//     if (sui_test_flag(chld, SUGF_MODALWIDGET)) {
//       chld = chld->prev; /* search focused after modal widget */
//     }
//     do {
//       if (sui_test_flag(chld, SUFL_FOCUSED)) {
//         sui_grpwdg(widget)->focus = chld; /* FIXME: FOCUSASK ? */
//         break;
//       }
//       chld = chld->prev;
//     } while (chld != sui_grpwdg(widget)->children);
//   }

  if (widget->place.w < 0 || widget->place.h < 0) {
    sui_point_t size;
    sui_widget_compute_size(event->message.ptr, widget, &size);
    if (widget->place.w < 0) widget->place.w = size.x;
    if (widget->place.h < 0) widget->place.h = size.y;
  }
}

/**
 * sui_wgroup_draw_child - Draw child widget frame, label and send draw/redraw event to the child
 * @dc: Pointer to a device context.
 * @child: Pointer to a widget.
 *
 * Return Value: The function does not return any value.
 *
 * File: sui_wgroup.c
 */
void sui_wgroup_draw_child(sui_dc_t *dc, sui_widget_t *child)
{
  sui_dc_t down_dc;
  sui_event_t down_event;

  if (!child || !child->style || !child->style->coltab) return;

  if (sui_test_flag(child, SUFL_AUTORESIZE)) { /* count new widget size before drawing if the SUFL_AUTORESIZE flag is set */
    down_event.what = SUEV_COMMAND;
    down_event.message.command = SUCM_AUTORESIZE;
    down_event.message.ptr = dc;
    sui_hevent(child, &down_event);
  }
  down_dc.psd = dc->psd;
  down_dc.offset.x = dc->offset.x + child->place.x;
  down_dc.offset.y = dc->offset.y + child->place.y;
  down_dc.dc_flags = dc->dc_flags;
  down_event.what = SUEV_DRAW;
  down_event.draw.dc = &down_dc;
  // child outside label
  if (child->label && !(sui_get_widget_labalign(child) & SUAL_INSIDE)) {
    sui_draw_widget_label(&down_dc, child);
  }
  sui_hevent(child, &down_event);
}

/**
 * sui_wgroup_draw - Draw content of the group widget
 * @dc: Pointer to a device context.
 * @widget: Pointer to a group widget.
 *
 * The function draws content of the group widget - it sends the DRAW event to
 * all children (the focused widget is drown in normal order as all others) and
 * then all modal widgets are drown.
 *
 * Return Value: The function returns 1 when the event is processed and -1 as an error.
 *
 * File: sui_wgroup.c
 */
int sui_wgroup_draw(sui_dc_t *dc, sui_widget_t *widget)
{
  sui_widget_t *current = sui_grpwdg(widget)->children;

  if (current) { /* draw normal all widgets from the LAST widget to the FIRST one */
    do {
      current = current->next;  /* draw from the last child widget to the first (modal) widget */
      sui_wgroup_draw_child(dc, current);
    } while (current != sui_grpwdg(widget)->children);
  }
  return 1;
}

/**
 * sui_wgroup_hkey - Process events from keyboard
 * @widget: Pointer to a group widget.
 * @event: Pointer to an event structure.
 *
 * The function does not do anything.
 *
 * Return Value: The function returns 0, because it doesn't process keyboard events.
 *
 * File: sui_wgroup.c
 */
int sui_wgroup_hkey(sui_widget_t *widget, sui_event_t *event)
{
  return 0;
}

/**
 * sui_group_notify - Process events from its children
 * @widget: Pointer to a group widget.
 * @event: Pointer to an event structure.
 *
 * The function propagates the event to the widget parent if it exists.
 *
 * Return Value: The function returns 1 when the event is processed and -1 as an error.
 *
 * File: sui_wgroup.c
 */
int sui_group_notify(sui_widget_t *widget, sui_event_t *event)
{
  if (widget->parent && sui_test_flag(widget, SUGF_NOTIFY)) {
//    ul_logdeb("Send notify to parent group ...\n");
    sui_hevent(widget->parent, event);
  } else {
    sui_event_t gev;
//    ul_logdeb("Globalize notification ...\n");
    gev.what = SUEV_GLOBAL;
    gev.message.command = SUGM_NOTIFY;
    gev.message.ptr = event->message.ptr;
    gev.message.info = event->message.info;
    sui_put_global_event(&gev);
  }
  return 1;
}

#ifdef CONFIG_OC_SUITK_WITH_MOUSE

sui_widget_t *sui_wgroup_child_at_pos(sui_widget_t *widget, sui_coordinate_t x,
                                      sui_coordinate_t y, sui_coordinate_t tol)
{
  suiw_group_t *wgrp = sui_grpwdg(widget);
  sui_widget_t *wdgfound = NULL;
  sui_widget_t *w = NULL;
  sui_point_t pos = {x, y};
  sui_coordinate_t dist;
  sui_coordinate_t distmin = 0;

  for (w = wgrp->children; w != NULL; w = (w->next != wgrp->children)? w->next: NULL) {
    if (sui_test_flag(w, SUFL_DISABLED))
      continue;

    dist = sui_rect_to_point_n1dist(&w->place, &pos);
    if (!dist) {
      wdgfound = w;
      break;
    }
    if (dist > tol)
      continue;
    if ((wdgfound != NULL) || (dist < distmin)) {
      wdgfound = w;
      distmin = dist;
    }
  }

  return wdgfound;
}

#endif /*CONFIG_OC_SUITK_WITH_MOUSE*/


#define PROPEV_TO_TOP  (SUEV_MOUSE|SUEV_KEYBOARD)
#define PROPEV_TO_DOWN (SUEV_MESSAGE)

/**
 * sui_wgroup_hevent - Process all group widget events
 * @widget: Pointer to a group widget.
 * @event: Pointer to an event.
 *
 * The function processes all events in the group widget or calls
 * the basic event handler if it cannot process the event.
 * The function (or the called basic handle-event function clears event if the
 * event is processed.
 *
 * Return Value: The function does not return a value.
 *
 * File: sui_wgroup.c
 */
void sui_wgroup_hevent(sui_widget_t *widget, sui_event_t *event)
{
  suiw_group_t *wgrp = sui_grpwdg(widget);

#ifdef CONFIG_OC_SUITK_WITH_MOUSE
  if (event->what & SUEV_MOUSE) {
    typeof(event->mouse.x) in_x = event->mouse.x;
    typeof(event->mouse.y) in_y = event->mouse.y;
    sui_widget_t *wmousein;
    wmousein = sui_wgroup_child_at_pos(widget, in_x, in_y, 0);

    if (wgrp->children && sui_test_flag(widget, SUGF_FIRSTCHILDISMODAL)) {
      if ((wmousein == NULL) || !sui_test_flag(wmousein, SUFL_BOTTOMACTIVE))
        wmousein = wgrp->children; /* grp->children points to the modal widget */
    }

    if (wmousein != NULL) {
      event->mouse.x -= wmousein->place.x;
      event->mouse.y -= wmousein->place.y;

      sui_hevent(wmousein, event);

      event->mouse.x = in_x;
      event->mouse.y = in_y;

      return;
    }
  }
#endif /*CONFIG_OC_SUITK_WITH_MOUSE*/

// first down (if propagate event from bottom to top)
  if ((event->what != SUEV_NOTHING) && (event->what & PROPEV_TO_TOP)) {
    if (wgrp->children && sui_test_flag(widget, SUGF_FIRSTCHILDISMODAL)) {
      sui_hevent(wgrp->children, event); /* grp->children points to the modal widget */
    } else if (wgrp->focus) {
      sui_hevent(wgrp->focus, event);
    }
  }
// both - if event wasn't processed
  if (event->what != SUEV_NOTHING) {

    switch(event->what) {
      case SUEV_DRAW:
      case SUEV_REDRAW:
        sui_widget_draw(event->draw.dc, widget, sui_wgroup_draw);
        sui_clear_event(widget, event);
        break;

      case SUEV_KDOWN:
      case SUEV_KUP:
        if (wgrp->children && sui_test_flag(widget, SUGF_FIRSTCHILDISMODAL)) {
          sui_hevent(wgrp->children, event); /* grp->children points to the modal widget */
        } else {
          if (sui_test_flag(widget, SUFL_FOCUSED) && wgrp->focus) {
            sui_hevent(wgrp->focus, event);
          } else {
            if (sui_wgroup_hkey(widget, event) > 0)
              sui_clear_event(widget, event);
          }
        }
        break;

      case SUEV_COMMAND:
        switch (event->message.command) {
          case SUCM_INIT:
            sui_widget_hevent(widget, event);
            sui_wgroup_init(widget, event);
            sui_clear_event(widget, event);
            break;

          case SUCM_NEXT:
            ul_logdeb("WE_NEXT\n");
            if (widget->parent == NULL) { /* only for root group widget */
              sui_widget_t *from = sui_group_find_focused_widget(widget, NULL);
// ul_logdeb(" -- We are searching focus from '%s' %p\n", from ? sui_utf8_get_text(from->label) : "-", from);
              sui_widget_t *to = sui_group_find_next(widget, from, 0, 0);
              if (!sui_group_change_focus(widget, from, to))
                sui_clear_event(widget, event);
            }
            return;

          case SUCM_PREV:
            ul_logdeb("WE_PREV\n");
            if (widget->parent == NULL) { /* only for root group widget */
              sui_widget_t *from = sui_group_find_focused_widget(widget, NULL);
// ul_logdeb(" -- We are searching focus from '%s' %p\n", from ? sui_utf8_get_text(from->label) : "-", from);
              sui_widget_t *to = sui_group_find_next(widget, from, 1, 0);
              if (!sui_group_change_focus(widget, from, to))
                sui_clear_event(widget, event);
            }
            return;

          case SUCM_NEXT_GROUP:
            ul_logdeb("WE_NEXT_GROUP\n");
            if (widget->parent == NULL) { /* only for root group widget */
              sui_widget_t *from = sui_group_find_focused_widget(widget, NULL);
// ul_logdeb(" -- We are searching focus from '%s' %p\n", from ? sui_utf8_get_text(from->label) : "-", from);
              sui_widget_t *to = sui_group_find_next(widget, from, 0, 2);
              if (!sui_group_change_focus(widget, from, to))
                sui_clear_event(widget, event);
            }
            return;

          case SUCM_PREV_GROUP:
            ul_logdeb("WE_PREV_GROUP\n");
            if (widget->parent == NULL) { /* only for root group widget */
              sui_widget_t *from = sui_group_find_focused_widget(widget, NULL);
// ul_logdeb(" -- We are searching focus from '%s' %p\n", from ? sui_utf8_get_text(from->label) : "-", from);
              sui_widget_t *to = sui_group_find_next(widget, from, 1, 2);
              if (!sui_group_change_focus(widget, from, to))
                sui_clear_event(widget, event);
            }
            return;

          case SUCM_GETSIZE:
            if (sui_wgroup_get_size((sui_dc_t *)event->message.ptr,
                widget, (sui_point_t *)event->message.info) > 0) {
              sui_clear_event(widget, event);
            }
            break;

          case SUCM_FOCUSASK:
// /*            if (sui_group_is_focusable(widget) > 0) {
//               sui_clear_event(widget, event);
//             }
//             break;    // return*/
            /* widget is focusen if itself is focusen and at least one of its children is focusen - Not perfectly correct */
            if (sui_is_flag(widget, SUFL_FOCUSEN | SUFL_DISABLED, SUFL_FOCUSEN)) {
              int isit = 0;
              if (sui_test_flag(widget, SUGF_CANBEFOCUSED)) {
                isit = 1;
              } else {
                sui_widget_t *chld = wgrp->children;
                while (chld) {
                  if (chld && !sui_hevent_command(chld, SUCM_FOCUSASK, NULL, 0)) {
                    isit = 1;
                    break;
                  }
                  chld = chld->prev;
                  if (chld == wgrp->children) break;
                }
              }
              if (isit)
                sui_clear_event(widget, event);
            }
            break;    // return

          case  SUCM_DISTRIBUTE:
            if (event->message.ptr && event->message.info) {
              if (!sui_group_foreach(widget, NULL, 1, event->message.ptr,
                  (sui_event_t *)event->message.info)) {
                sui_clear_event(widget, event);
              }
            }
            break;

          case SUCM_NOTIFY:
            sui_group_notify(widget, event);
            break;
/*
          default:
            needbroadcast = 1;
            break;
*/
        }
        break;
/*
      case SUEV_BROADCAST:
        needbroadcast = 1;
        break;
*/
    }
    if (event->what != SUEV_NOTHING)
      sui_widget_hevent(widget, event);
  }

  if (event->what & PROPEV_TO_DOWN) {
//ul_logdeb("event-maybe go down\n");
    if (event->what == SUEV_BROADCAST) {
      sui_group_foreach(widget, NULL, 1, sui_group_devent, event);
    } else {
      if (wgrp->children && sui_test_flag(widget, SUGF_FIRSTCHILDISMODAL)) {
        sui_hevent(wgrp->children, event);
      } else if (wgrp->focus) {
        sui_hevent(wgrp->focus, event);
      }
    }
  }
}


/******************************************************************************
 * Basic widget vmt & signal-slot mechanism
 ******************************************************************************/

/**
 * sui_wgroup_vmt_init - Initialize vmt group widget
 * @pw: Pointer to a common widget structure.
 * @params: Initial function's input arguments.
 *
 * The function updates the basic widget to the group widget and fills its structure fields.
 *
 * Return Value: The function return 0 as success and -1 as an error.
 *
 * File: sui_wgroup.c
 */
int sui_wgroup_vmt_init(sui_widget_t *pw, void *params)
{
  pw->data = sui_malloc(sizeof(suiw_group_t));
  if (!pw->data) return -1;
  pw->type = SUWT_GROUP;
  pw->hevent = sui_wgroup_hevent;
  return 0;
}

/**
 * sui_wgroup_vmt_done - Remove vmt group widget
 * @pw: Pointer to a group widget.
 *
 * The function removes the group widget extension from the basic widget and
 * releases each allocated memory for the group extension.
 *
 * Return Value: The function does not return any value.
 *
 * File: sui_wgroup.c
 */
void sui_wgroup_vmt_done(sui_widget_t *pw)
{
  if(pw->data) {
    suiw_group_t *wgrp = sui_grpwdg(pw);

    while (wgrp->children) /* remove all children from the group widget */
      sui_group_remove_widget(wgrp->children);
    wgrp->children = NULL;

    free(wgrp);
    pw->data = NULL;
  }
  pw->type = SUWT_GENERIC;
  pw->hevent = sui_widget_hevent; // NULL;
}

/******************************************************************************/
SUI_CANBE_CONST
sui_obj_signal_tinfo_t sui_wgroup_signal_tinfo[] = {
};

SUI_CANBE_CONST
sui_obj_slot_tinfo_t sui_wgroup_slot_tinfo[] = {
  { "status_message", SUI_SLOT_STATUS_MESSAGE, &sui_args_tinfo_utf8,
    .target_kind=SUI_STGT_METHOD_OFFSET,
    .target.method_offset=UL_OFFSETOF(sui_wgroup_vmt_t, status_message)},

};

SUI_CANBE_CONST
sui_obj_tinfo_t sui_wgroup_vmt_obj_tinfo = {
  .name = "sui_wgroup",
  .obj_size = sizeof(sui_widget_t),
  .obj_align = UL_ALIGNOF(sui_widget_t),
  .signal_count = MY_ARRAY_SIZE(sui_wgroup_signal_tinfo),
  .signal_tinfo = UL_CAST_UNQ1(sui_obj_signal_tinfo_t *, sui_wgroup_signal_tinfo),
  .slot_count = MY_ARRAY_SIZE(sui_wgroup_slot_tinfo),
  .slot_tinfo = UL_CAST_UNQ1(sui_obj_slot_tinfo_t *, sui_wgroup_slot_tinfo)
};

sui_wgroup_vmt_t sui_wgroup_vmt_data = {
  .vmt_size = sizeof(sui_wgroup_vmt_t),
  .vmt_refcnt = SUI_STATIC,
  .vmt_parent = &sui_widget_vmt_data,
  .vmt_init = sui_wgroup_vmt_init,
  .vmt_done = sui_wgroup_vmt_done,
  .vmt_obj_tinfo = UL_CAST_UNQ1(sui_obj_tinfo_t *, &sui_wgroup_vmt_obj_tinfo),

  .status_message = sui_group_status_message,
  .reclaim_children = sui_group_reclaim_children,
};



/**
 * sui_wgroup - Create a new group widget
 * @aplace: Pointer to a rectangle structure with the widget position and dimensions.
 * @alabel: A UTF8 string with widget label.
 * @astyle: Pointer to a style structure which widget will use.
 * @aflags: Widget flags.
 *
 * The function creates a new group widget and sets its main properties.
 *
 * Return Value: The function returns a pointer to a new group widget.
 *
 * File: sui_wgroup.c
 */
sui_widget_t *sui_wgroup(sui_rect_t *aplace, utf8 *alabel, sui_style_t *astyle,
                         unsigned long aflags)
{
  sui_widget_t *pw;

  pw = sui_widget_create_new(&sui_wgroup_vmt_data, aplace, alabel, astyle, aflags);

  if (!pw) return NULL;

  return pw;
}
