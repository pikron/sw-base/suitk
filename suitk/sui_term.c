/* sui_term.c
 *
 * SUITK system of terms (based on dinfo)
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */


#include <string.h>
#include "sui_term.h"
#include <suiut/support.h>

#include <ul_log.h>

UL_LOG_CUST(ulogd_suitk)

/******************************************************************************
 * Term value 
 ******************************************************************************/
int sui_term_value_clear( sui_term_value_t *val)
{
  if (!val) return -1;
  if (val->type==SUI_TERMVAL_UTF8) {
    sui_utf8_dec_refcnt(val->value.u8);
  } else if (val->type==SUI_TERMVAL_DINFO) {
    sui_dinfo_dec_refcnt(val->value.dinfo);
  } else if (val->type==SUI_TERMVAL_SUBTERM) {
    sui_term_dec_refcnt(val->value.subterm);
  }
  sui_term_value_init( val);
  return 0;
};

int sui_term_value_inc_content_refcnt( sui_term_value_t *val)
{
  if (!val) return -1;
  if (val->type==SUI_TERMVAL_UTF8) {
    sui_utf8_inc_refcnt(val->value.u8);
  } else if (val->type==SUI_TERMVAL_DINFO) {
    sui_dinfo_inc_refcnt(val->value.dinfo);
  } else if (val->type==SUI_TERMVAL_SUBTERM) {
    sui_term_inc_refcnt(val->value.subterm);
  }
  return 0;
}

/******************************************************************************
 * term value translations
 ******************************************************************************/
static inline
int sui_term_value_long2ulong( sui_term_value_t *val)
{
  if (val->value.slong < 0) /* return ERROR or set ulong to zero ? */
    return SUI_RET_ETYPE;
  val->type = SUI_TERMVAL_ULONG;
  return SUI_RET_OK;
}

static inline
int sui_term_value_ulong2long( sui_term_value_t *val)
{
  if (val->value.ulong >= 0x80000000)
    return SUI_RET_ETYPE;
  val->type = SUI_TERMVAL_LONG;
  return SUI_RET_OK;
}

static inline
int sui_term_value_dinfo2long( sui_term_value_t *val)
{
  long res = 0;
  int ret;
  ret = sui_rd_long( val->value.dinfo, 0, &res);
  if ( ret == SUI_RET_OK) {
    sui_dinfo_dec_refcnt(val->value.dinfo);
    val->value.slong = res;
    val->type = SUI_TERMVAL_LONG;
  }
  return ret;
}

static inline
int sui_term_value_dinfo2ulong( sui_term_value_t *val)
{
  unsigned long res = 0;
  int ret;
  ret = sui_rd_ulong( val->value.dinfo, 0, &res);
  if ( ret == SUI_RET_OK) {
    sui_dinfo_dec_refcnt(val->value.dinfo);
    val->value.ulong = res;
    val->type = SUI_TERMVAL_ULONG;
  }
  return ret;
}

static inline
int sui_term_value_dinfo2utf( sui_term_value_t *val)
{
  utf8 *res=NULL;
  int ret;
  ret = sui_rd_utf8( val->value.dinfo, 0, &res);
  if ( ret == SUI_RET_OK) {
    sui_dinfo_dec_refcnt(val->value.dinfo);
    val->value.u8 = res;
    val->type = SUI_TERMVAL_UTF8;
  }
  return ret;
}

static inline
int sui_term_value_utf2ulong( sui_term_value_t *val)
{
  return SUI_RET_ERR;
}

static inline
int sui_term_value_utf2long( sui_term_value_t *val)
{
  return SUI_RET_ERR;
}

static inline
int sui_term_value_ulong2utf( sui_term_value_t *val)
{
  return SUI_RET_ERR;
}

static inline
int sui_term_value_long2utf( sui_term_value_t *val)
{
  return SUI_RET_ERR;
}

static inline
int sui_term_value_utf2dinfo( sui_term_value_t *val)
{
  return SUI_RET_ERR;
}

static inline
int sui_term_value_long2dinfo( sui_term_value_t *val)
{
  return SUI_RET_ERR;
}

static inline
int sui_term_value_ulong2dinfo( sui_term_value_t *val)
{
  return SUI_RET_ERR;
}

int sui_term_value_convert( sui_term_value_t *val, unsigned short totype)
{
  if ( val->type == totype) return SUI_RET_OK;
  switch ( val->type) {
    case SUI_TERMVAL_LONG:
      switch ( totype) {
        case SUI_TERMVAL_ULONG: return sui_term_value_long2ulong(val);
        case SUI_TERMVAL_UTF8:  return sui_term_value_long2utf(val);
        case SUI_TERMVAL_DINFO: return sui_term_value_long2dinfo(val);
      }
      break;
    case SUI_TERMVAL_ULONG:
      switch ( totype) {
        case SUI_TERMVAL_LONG:  return sui_term_value_ulong2long(val);
        case SUI_TERMVAL_UTF8:  return sui_term_value_ulong2utf(val);
        case SUI_TERMVAL_DINFO: return sui_term_value_ulong2dinfo(val);
      }
      break;
    case SUI_TERMVAL_UTF8:
      switch ( totype) {
        case SUI_TERMVAL_LONG:  return sui_term_value_utf2long(val);
        case SUI_TERMVAL_ULONG: return sui_term_value_utf2ulong(val);
        case SUI_TERMVAL_DINFO: return sui_term_value_utf2dinfo(val);
      }
      break;
    case SUI_TERMVAL_DINFO:
      switch ( totype) {
        case SUI_TERMVAL_LONG:  return sui_term_value_dinfo2long(val);
        case SUI_TERMVAL_ULONG: return sui_term_value_dinfo2ulong(val);
        case SUI_TERMVAL_UTF8:  return sui_term_value_dinfo2utf(val);
      }
      break;
  }
  return SUI_RET_ERR;
}

int sui_term_value_implicit_convert( sui_term_value_t *val, unsigned short tomask)
{
  unsigned short totype;
  int ret = SUI_RET_ERR;
  if (tomask & val->type) return SUI_RET_OK;
  totype = 0x0001;
  while (totype<SUI_TERMVAL_SUBTERM) {
    if ( tomask & totype) {
      ret = sui_term_value_convert(val, totype); /* try first possible */
      if ( ret == SUI_RET_OK) return ret;
    }
    totype <<= 1;
  }
  return ret;
}
/*******************************************************************************
	term computing operations
*******************************************************************************/
//typedef int sui_term_operation_t( struct sui_term_atomic *cnode, sui_term_value_t *result);
//typedef int sui_term_op_1arg_t(sui_term_value_t *result, sui_term_value_t *first);
//typedef int sui_term_op_2arg_t(sui_term_value_t *result, sui_term_value_t *first, sui_term_value_t *second);
//typedef int sui_term_op_3arg_t(sui_term_value_t *result, sui_term_value_t *first, sui_term_value_t *second, sui_term_value_t *third);

/**
 * sui_term_op_get_value - return value from outside
 */
int sui_term_op_get_value(struct sui_term_atomic *cnode, sui_term_value_t *result)
{
//	if (!result) return SUI_RET_NCON;  /* FIXME: ? returned value ? - checkit only for term not for all atomicterm */
  *result = cnode->data;
  sui_term_value_inc_content_refcnt( result);
  return SUI_RET_OK;
}

/**
 * sui_term_op_get_subterm - return value of subterm
 */
int sui_term_op_get_subterm(struct sui_term_atomic *cnode, sui_term_value_t *result)
{
  sui_term_t *subterm;
//	if (!result) return SUI_RET_NCON;
  if (cnode->data.type != SUI_TERMVAL_SUBTERM) return SUI_RET_ETYPE;
  subterm = cnode->data.value.subterm;

  switch(subterm->flags) {
    case SUI_TERM_VALID:
      *result = subterm->result;
      sui_term_value_inc_content_refcnt( result);
      return SUI_RET_OK;
    case SUI_TERM_ERROR:
      return SUI_RET_ERR;
    default: /* recount */
      if (!(subterm->flags & SUI_TERM_CHANGING))
        return sui_term_compute_term(subterm, result);
      /* FIXME: changing/changed ... ??? */
  }
  return SUI_RET_ERR;
}

int sui_term_op_get_size(sui_term_value_t *result, sui_term_value_t *first)
{
  if (first->type == SUI_TERMVAL_UTF8) {
    result->type = SUI_TERMVAL_LONG;
    result->value.slong = sui_utf8_length(first->value.u8);
    return SUI_RET_OK;
  }
  return SUI_RET_ETYPE;
}

/******************************************************************************/
/* unary operations - input from left node */
//typedef int sui_term_op_1arg_t(sui_term_value_t *result, sui_term_value_t *first);

/**
 * sui_term_op_not - unary operation: result= ~left
 */
int sui_term_op_not(sui_term_value_t *result, sui_term_value_t *first)
{
  if ( first->type == SUI_TERMVAL_LONG) {
    result->type = SUI_TERMVAL_LONG;
    result->value.slong = ~first->value.slong;
    return SUI_RET_OK;
  } else if ( first->type == SUI_TERMVAL_ULONG) {
    result->type = SUI_TERMVAL_ULONG;
    result->value.ulong = ~first->value.ulong;
    return SUI_RET_OK;
  }
  return SUI_RET_ETYPE;
}
/**
 * sui_term_op_minus - unary operation: result = -left
 */
int sui_term_op_minus(sui_term_value_t *result, sui_term_value_t *first)
{
  if ( first->type == SUI_TERMVAL_LONG) {
    result->type = SUI_TERMVAL_LONG;
    result->value.slong = -first->value.slong;
    return SUI_RET_OK;
  }
  return SUI_RET_ETYPE;
}
/**
 * sui_term_op_test_dinfo - unary operation: test if dinfo value is in its range (for long values)
 * result = 0 if dinfo.value>=dinfo.minval&&dinfo.value<=dinfo.maxval
 * result = 1 if dinfo.value>dinfo.maxval
 * result = -1 if dinfo.value<dinfo.minval or dinfo reading failed
 */
int sui_term_op_test_dinfo(sui_term_value_t *result, sui_term_value_t *first)
{
  if (first->type == SUI_TERMVAL_DINFO) {
    long val;
    result->type = SUI_TERMVAL_LONG;
    if (sui_rd_long(first->value.dinfo, 0, &val)!=SUI_RET_OK)
      result->value.slong = -1;
    else {
      if (val<first->value.dinfo->minval)
        result->value.slong = -1;
      else if (val>first->value.dinfo->maxval)
        result->value.slong = 1;
      else
        result->value.slong = 0;
    }
    return SUI_RET_OK;
  }
  return SUI_RET_ETYPE;
}

/******************************************************************************/
/* binary operations - inputs are in left and right node */
//typedef int sui_term_op_2arg_t(sui_term_value_t *result, sui_term_value_t *first, sui_term_value_t *second);

/**
 * sui_term_op_add - result = first + second
 */
int sui_term_op_add(sui_term_value_t *result, sui_term_value_t *first, sui_term_value_t *second)
{
  if (first->type==SUI_TERMVAL_ULONG) {
    if (second->type==SUI_TERMVAL_ULONG) {
      result->type = SUI_TERMVAL_ULONG;
      result->value.ulong = first->value.ulong + second->value.ulong;
      return SUI_RET_OK;
    } else if (second->type==SUI_TERMVAL_LONG) {
      result->type = SUI_TERMVAL_LONG;
      result->value.slong = first->value.ulong + second->value.slong;
      return SUI_RET_OK;
    }
  } else if (first->type==SUI_TERMVAL_LONG) {
    if (second->type==SUI_TERMVAL_ULONG) {
      result->type = SUI_TERMVAL_LONG;
      result->value.slong = first->value.slong + second->value.ulong;
      return SUI_RET_OK;
    } else if (second->type==SUI_TERMVAL_LONG) {
      result->type = SUI_TERMVAL_LONG;
      result->value.slong = first->value.slong + second->value.slong;
      return SUI_RET_OK;
    }
  }
  return SUI_RET_ETYPE;
}
int sui_term_op_sub(sui_term_value_t *result, sui_term_value_t *first, sui_term_value_t *second)
{
  if (first->type==SUI_TERMVAL_ULONG) {
    if (second->type==SUI_TERMVAL_ULONG) {
      result->type = SUI_TERMVAL_ULONG;
      result->value.ulong = first->value.ulong - second->value.ulong;
      return SUI_RET_OK;
    } else if (second->type==SUI_TERMVAL_LONG) {
      result->type = SUI_TERMVAL_LONG;
      result->value.slong = first->value.ulong - second->value.slong;
      return SUI_RET_OK;
    }
  } else if (first->type==SUI_TERMVAL_LONG) {
    if (second->type==SUI_TERMVAL_ULONG) {
      result->type = SUI_TERMVAL_LONG;
      result->value.slong = first->value.slong - second->value.ulong;
      return SUI_RET_OK;
    } else if (second->type==SUI_TERMVAL_LONG) {
      result->type = SUI_TERMVAL_LONG;
      result->value.slong = first->value.slong - second->value.slong;
      return SUI_RET_OK;
    }
  }
  return SUI_RET_ETYPE;
}
int sui_term_op_mult(sui_term_value_t *result, sui_term_value_t *first, sui_term_value_t *second)
{
  if (first->type==SUI_TERMVAL_ULONG) {
    if (second->type==SUI_TERMVAL_ULONG) {
      result->type = SUI_TERMVAL_ULONG;
      result->value.ulong = first->value.ulong * second->value.ulong;
      return SUI_RET_OK;
    } else if (second->type==SUI_TERMVAL_LONG) {
      result->type = SUI_TERMVAL_LONG;
      result->value.slong = first->value.ulong * second->value.slong;
      return SUI_RET_OK;
    }
  } else if (first->type==SUI_TERMVAL_LONG) {
    if (second->type==SUI_TERMVAL_ULONG) {
      result->type = SUI_TERMVAL_LONG;
      result->value.slong = first->value.slong * second->value.ulong;
      return SUI_RET_OK;
    } else if (second->type==SUI_TERMVAL_LONG) {
      result->type = SUI_TERMVAL_LONG;
      result->value.slong = first->value.slong * second->value.slong;
      return SUI_RET_OK;
    }
  }
  return SUI_RET_ETYPE;
}

int sui_term_op_div(sui_term_value_t *result, sui_term_value_t *first, sui_term_value_t *second)
{
  if (second->type==SUI_TERMVAL_ULONG) {
    if ( second->value.ulong == 0) return SUI_RET_ZERODIV;
    if (first->type==SUI_TERMVAL_ULONG) {
      result->type = SUI_TERMVAL_ULONG;
      result->value.ulong = first->value.ulong / second->value.ulong;
      return SUI_RET_OK;
    } else if (first->type==SUI_TERMVAL_LONG) {
      result->type = SUI_TERMVAL_LONG;
      result->value.slong = first->value.slong / second->value.ulong;
      return SUI_RET_OK;
    }
  } else if (second->type==SUI_TERMVAL_LONG) {
    if (second->value.slong == 0) return SUI_RET_ZERODIV;
    if (first->type==SUI_TERMVAL_ULONG) {
      result->type = SUI_TERMVAL_LONG;
      result->value.slong = first->value.ulong / second->value.slong;
      return SUI_RET_OK;
    } else if (first->type==SUI_TERMVAL_LONG) {
      result->type = SUI_TERMVAL_LONG;
      result->value.slong = first->value.slong / second->value.slong;
      return SUI_RET_OK;
    }
  }
  return SUI_RET_ETYPE;
}

int sui_term_op_and(sui_term_value_t *result, sui_term_value_t *first, sui_term_value_t *second)
{
  if (first->type==SUI_TERMVAL_ULONG && second->type==SUI_TERMVAL_ULONG) {
    result->type = SUI_TERMVAL_ULONG;
    result->value.ulong = first->value.ulong & second->value.ulong;
    return SUI_RET_OK;
  }
  return SUI_RET_ETYPE;
}

int sui_term_op_or(sui_term_value_t *result, sui_term_value_t *first, sui_term_value_t *second)
{
  if (first->type==SUI_TERMVAL_ULONG && second->type==SUI_TERMVAL_ULONG) {
    result->type = SUI_TERMVAL_ULONG;
    result->value.ulong = first->value.ulong | second->value.ulong;
    return SUI_RET_OK;
  }
  return SUI_RET_ETYPE;
}
int sui_term_op_xor(sui_term_value_t *result, sui_term_value_t *first, sui_term_value_t *second)
{
  if (first->type==SUI_TERMVAL_ULONG && second->type==SUI_TERMVAL_ULONG) {
    result->type = SUI_TERMVAL_ULONG;
    result->value.ulong = first->value.ulong ^ second->value.ulong;
    return SUI_RET_OK;
  }
  return SUI_RET_ETYPE;
}
int sui_term_op_shr(sui_term_value_t *result, sui_term_value_t *first, sui_term_value_t *second)
{
  if (first->type==SUI_TERMVAL_ULONG && second->type==SUI_TERMVAL_ULONG) {
    result->type = SUI_TERMVAL_ULONG;
    result->value.ulong = first->value.ulong >> second->value.ulong;
    return SUI_RET_OK;
  }
  return SUI_RET_ETYPE;
}
int sui_term_op_shl(sui_term_value_t *result, sui_term_value_t *first, sui_term_value_t *second)
{
  if (first->type==SUI_TERMVAL_ULONG && second->type==SUI_TERMVAL_ULONG) {
    result->type = SUI_TERMVAL_ULONG;
    result->value.ulong = first->value.ulong << second->value.ulong;
    return SUI_RET_OK;
  }
  return SUI_RET_ETYPE;
}

int sui_term_op_equal(sui_term_value_t *result, sui_term_value_t *first, sui_term_value_t *second)
{
  if (first->type==SUI_TERMVAL_ULONG) {
    if (second->type==SUI_TERMVAL_ULONG) {
      result->value.slong = (first->value.ulong == second->value.ulong);
    } else if (second->type==SUI_TERMVAL_LONG) {
      if (second->value.slong < 0)
        result->value.slong = 0; /* they must be different */
      else
        result->value.slong = (first->value.ulong == (unsigned long)second->value.ulong);
    } else {
      return SUI_RET_ETYPE;
    }
    result->type = SUI_TERMVAL_LONG;
    return SUI_RET_OK;
  } else if (first->type==SUI_TERMVAL_LONG) {
    if ( second->type==SUI_TERMVAL_LONG) {
      result->value.slong = (first->value.slong == second->value.slong);
    } else if ( second->type==SUI_TERMVAL_ULONG) {
      if ( second->value.ulong > 0x7fffffff)
        result->value.slong = 0; /* they must be different */
      else
        result->value.slong = (first->value.ulong == (long)second->value.ulong);
    } else {
      return SUI_RET_ETYPE;
    }
    result->type = SUI_TERMVAL_LONG;
    return SUI_RET_OK;
  } else if (first->type==SUI_TERMVAL_UTF8 && second->type==SUI_TERMVAL_UTF8) {
    result->type = SUI_TERMVAL_LONG;
    result->value.ulong = (sui_utf8_cmp(first->value.u8, second->value.u8)==0);
    return SUI_RET_OK;
  }
  return SUI_RET_ETYPE;
}

int sui_term_op_less(sui_term_value_t *result, sui_term_value_t *first, sui_term_value_t *second)
{
  if (first->type==SUI_TERMVAL_ULONG) {
    if (second->type==SUI_TERMVAL_ULONG) {
      result->value.slong = (first->value.ulong < second->value.ulong);
    } else if (second->type==SUI_TERMVAL_LONG) {
      if (second->value.slong < 0)
        result->value.slong = 0; /* the second cannot be great */
      else
        result->value.slong = (first->value.ulong < (unsigned long)second->value.ulong);
    } else {
      return SUI_RET_ETYPE;
    }
    result->type = SUI_TERMVAL_LONG;
    return SUI_RET_OK;
  } else if (first->type==SUI_TERMVAL_LONG) {
    if ( second->type==SUI_TERMVAL_LONG) {
      result->value.slong = (first->value.slong < second->value.slong);
    } else if ( second->type==SUI_TERMVAL_ULONG) {
      if ( second->value.ulong > 0x7fffffff)
        result->value.slong = 1; /* the second must be great */
      else
        result->value.slong = (first->value.ulong < (long)second->value.ulong);
    } else {
      return SUI_RET_ETYPE;
    }
    result->type = SUI_TERMVAL_LONG;
    return SUI_RET_OK;
  } else if (first->type==SUI_TERMVAL_UTF8 && second->type==SUI_TERMVAL_UTF8) {
    result->type = SUI_TERMVAL_LONG;
    result->value.ulong = (sui_utf8_cmp(first->value.u8, second->value.u8)<0);
    return SUI_RET_OK;
  }
  return SUI_RET_ETYPE;
}

int sui_term_op_great(sui_term_value_t *result, sui_term_value_t *first, sui_term_value_t *second)
{
  if (first->type==SUI_TERMVAL_ULONG) {
    if (second->type==SUI_TERMVAL_ULONG) {
      result->value.slong = (first->value.ulong > second->value.ulong);
    } else if (second->type==SUI_TERMVAL_LONG) {
      if (second->value.slong < 0)
        result->value.slong = 1; /* the second must be less */
      else
        result->value.slong = (first->value.ulong > (unsigned long)second->value.ulong);
    } else {
      return SUI_RET_ETYPE;
    }
    result->type = SUI_TERMVAL_LONG;
    return SUI_RET_OK;
  } else if (first->type==SUI_TERMVAL_LONG) {
    if ( second->type==SUI_TERMVAL_LONG) {
      result->value.slong = (first->value.slong > second->value.slong);
    } else if ( second->type==SUI_TERMVAL_ULONG) {
      if ( second->value.ulong > 0x7fffffff)
        result->value.slong = 0; /* the second cannot be less */
      else
        result->value.slong = (first->value.ulong > (long)second->value.ulong);
    } else {
      return SUI_RET_ETYPE;
    }
    result->type = SUI_TERMVAL_LONG;
    return SUI_RET_OK;
  } else if (first->type==SUI_TERMVAL_UTF8 && second->type==SUI_TERMVAL_UTF8) {
    result->type = SUI_TERMVAL_LONG;
    result->value.ulong = (sui_utf8_cmp(first->value.u8, second->value.u8)>0);
    return SUI_RET_OK;
  }
  return SUI_RET_ETYPE;
}

int sui_term_op_lessorequal(sui_term_value_t *result, sui_term_value_t *first, sui_term_value_t *second)
{
  if (first->type==SUI_TERMVAL_ULONG) {
    if (second->type==SUI_TERMVAL_ULONG) {
      result->value.slong = (first->value.ulong <= second->value.ulong);
    } else if (second->type==SUI_TERMVAL_LONG) {
      if (second->value.slong < 0)
        result->value.slong = 0; /* the second cannot be great or equal */
      else
        result->value.slong = (first->value.ulong <= (unsigned long)second->value.ulong);
    } else {
      return SUI_RET_ETYPE;
    }
    result->type = SUI_TERMVAL_LONG;
    return SUI_RET_OK;
  } else if (first->type==SUI_TERMVAL_LONG) {
    if ( second->type==SUI_TERMVAL_LONG) {
      result->value.slong = (first->value.slong <= second->value.slong);
    } else if ( second->type==SUI_TERMVAL_ULONG) {
      if ( second->value.ulong > 0x7fffffff)
        result->value.slong = 1; /* the second must be great */
      else
        result->value.slong = (first->value.ulong <= (long)second->value.ulong);
    } else {
      return SUI_RET_ETYPE;
    }
    result->type = SUI_TERMVAL_LONG;
    return SUI_RET_OK;
  } else if (first->type==SUI_TERMVAL_UTF8 && second->type==SUI_TERMVAL_UTF8) {
    result->type = SUI_TERMVAL_LONG;
    result->value.ulong = (sui_utf8_cmp(first->value.u8, second->value.u8)<=0);
    return SUI_RET_OK;
  }
  return SUI_RET_ETYPE;
}

int sui_term_op_greatorequal(sui_term_value_t *result, sui_term_value_t *first, sui_term_value_t *second)
{
  if (first->type==SUI_TERMVAL_ULONG) {
    if (second->type==SUI_TERMVAL_ULONG) {
      result->value.slong = (first->value.ulong >= second->value.ulong);
    } else if (second->type==SUI_TERMVAL_LONG) {
      if (second->value.slong < 0)
        result->value.slong = 1; /* the second must be less */
      else
        result->value.slong = (first->value.ulong >= (unsigned long)second->value.ulong);
    } else {
      return SUI_RET_ETYPE;
    }
    result->type = SUI_TERMVAL_LONG;
    return SUI_RET_OK;
  } else if (first->type==SUI_TERMVAL_LONG) {
    if ( second->type==SUI_TERMVAL_LONG) {
      result->value.slong = (first->value.slong >= second->value.slong);
    } else if ( second->type==SUI_TERMVAL_ULONG) {
      if ( second->value.ulong > 0x7fffffff)
        result->value.slong = 0; /* the second cannot be less */
      else
        result->value.slong = (first->value.ulong >= (long)second->value.ulong);
    } else {
      return SUI_RET_ETYPE;
    }
    result->type = SUI_TERMVAL_LONG;
    return SUI_RET_OK;
  } else if (first->type==SUI_TERMVAL_UTF8 && second->type==SUI_TERMVAL_UTF8) {
    result->type = SUI_TERMVAL_LONG;
    result->value.ulong = (sui_utf8_cmp(first->value.u8, second->value.u8)>=0);
    return SUI_RET_OK;
  }
  return SUI_RET_ETYPE;
}

int sui_term_op_nonequal(sui_term_value_t *result, sui_term_value_t *first, sui_term_value_t *second)
{
  if (first->type==SUI_TERMVAL_ULONG) {
    if (second->type==SUI_TERMVAL_ULONG) {
      result->value.slong = (first->value.ulong != second->value.ulong);
    } else if (second->type==SUI_TERMVAL_LONG) {
      if (second->value.slong < 0)
        result->value.slong = 1; /* they must be different */
      else
        result->value.slong = (first->value.ulong != (unsigned long)second->value.ulong);
    } else {
      return SUI_RET_ETYPE;
    }
    result->type = SUI_TERMVAL_LONG;
    return SUI_RET_OK;
  } else if (first->type==SUI_TERMVAL_LONG) {
    if ( second->type==SUI_TERMVAL_LONG) {
      result->value.slong = (first->value.slong != second->value.slong);
    } else if ( second->type==SUI_TERMVAL_ULONG) {
      if ( second->value.ulong > 0x7fffffff)
        result->value.slong = 1; /* they must be different */
      else
        result->value.slong = (first->value.ulong != (long)second->value.ulong);
    } else {
      return SUI_RET_ETYPE;
    }
    result->type = SUI_TERMVAL_LONG;
    return SUI_RET_OK;
  } else if (first->type==SUI_TERMVAL_UTF8 && second->type==SUI_TERMVAL_UTF8) {
    result->type = SUI_TERMVAL_LONG;
    result->value.ulong = (sui_utf8_cmp(first->value.u8, second->value.u8)!=0);
    return SUI_RET_OK;
  }
  return SUI_RET_ETYPE;
}

/* explicit conversion from dinfo to long/ulong with specific size of decimal places */
int sui_term_op_get_dinfo_value(sui_term_value_t *result, sui_term_value_t *first, sui_term_value_t *second)
{
  int ret = SUI_RET_ETYPE;
  if (first->type != SUI_TERMVAL_DINFO || second->type != SUI_TERMVAL_LONG) return SUI_RET_ETYPE;
  if (sui_typeid_buffer_type(first->value.dinfo->tinfo) == SUI_TYPE_LONG) {
    long res;
    if ((ret = sui_rd_long(first->value.dinfo, 0, &res)) != SUI_RET_OK) return ret;
    /* change result */
    if (second->value.slong >= 0) {
      int diff = second->value.slong - first->value.dinfo->fdigits;
      while (diff) {
        if (diff>0) {
          if ((unsigned long)res >= (0x80000000/10)) return SUI_RET_EOORP;
          res *= 10;
          diff--;
        } else {
          int rem = res % 10;
          res /= 10;
          if (rem>=5) res++; /* rounding */
          diff++;
        }
      }
    }
    result->value.ulong = res;
    result->type = SUI_TERMVAL_LONG;
    ret = SUI_RET_OK;
  } else if (sui_typeid_buffer_type(first->value.dinfo->tinfo) == SUI_TYPE_ULONG) {
    unsigned long res;
    if ((ret = sui_rd_ulong(first->value.dinfo, 0, &res)) != SUI_RET_OK) return ret;
    /* change result */
    if (second->value.slong >= 0) {
      int diff = second->value.slong - first->value.dinfo->fdigits;
      while (diff) {
        if (diff>0) {
          if (res >= (0xFFFFFFFF/10)) return SUI_RET_EOORP;
          res *= 10;
          diff--;
        } else {
          int rem = res % 10;
          res /= 10;
          if (rem>=5) res++; /* rounding */
          diff++;
        }
      }
    }
    result->value.ulong = res;
    result->type = SUI_TERMVAL_ULONG;
    ret = SUI_RET_OK;
  }
  return ret;
}
/******************************************************************************/
/* ternary operations - inputs are in left, middle and right node */
//typedef int sui_term_op_3arg_t(sui_term_value_t *result, sui_term_value_t *first, sui_term_value_t *second, sui_term_value_t *third);
int sui_term_op_select(sui_term_value_t *result, sui_term_value_t *first,
                       sui_term_value_t *second, sui_term_value_t *third)
{
  int getone = 0;
  if (first->type==SUI_TERMVAL_LONG)
    getone = (first->value.slong!=0);
  else if (first->type==SUI_TERMVAL_ULONG)
    getone = (first->value.ulong!=0);
  else
    return SUI_RET_ETYPE;
  if (getone)
    *result = *second;
  else
    *result = *third;
  sui_term_value_inc_content_refcnt(result);
  return SUI_RET_OK;
}

/*******************************************************************************
	term functions
*******************************************************************************/
inline
sui_term_atomic_t *sui_term_create_aterm(void)
//sui_screen_equation_t *sui_screen_create_equation_node( void)
{
  return sui_malloc( sizeof( sui_term_atomic_t));
}

//sui_term_operation_t *sui_term_find_op_in_table( sui_term_op_table_t *table, int sign, int unary);

SUI_CANBE_CONST
sui_term_op_table_t sui_term_implemented_operation_table[] =
{{"+", 2, (sui_term_operation_t*)sui_term_op_add,
    SUI_TERMVAL_LONG | SUI_TERMVAL_ULONG, SUI_TERMVAL_LONG | SUI_TERMVAL_ULONG, 0},
 {"-", 2, (sui_term_operation_t*)sui_term_op_sub,
    SUI_TERMVAL_LONG | SUI_TERMVAL_ULONG, SUI_TERMVAL_LONG | SUI_TERMVAL_ULONG, 0},
 {"*", 2, (sui_term_operation_t*)sui_term_op_mult,
    SUI_TERMVAL_LONG | SUI_TERMVAL_ULONG, SUI_TERMVAL_LONG | SUI_TERMVAL_ULONG, 0},
 {"/", 2, (sui_term_operation_t*)sui_term_op_div,
    SUI_TERMVAL_LONG | SUI_TERMVAL_ULONG, SUI_TERMVAL_LONG | SUI_TERMVAL_ULONG, 0},
 {"&", 2, (sui_term_operation_t*)sui_term_op_and,
    SUI_TERMVAL_ULONG, SUI_TERMVAL_ULONG, 0},
 {"|", 2, (sui_term_operation_t*)sui_term_op_or,
    SUI_TERMVAL_ULONG, SUI_TERMVAL_ULONG, 0},
 {"xor", 2, (sui_term_operation_t*)sui_term_op_xor,
    SUI_TERMVAL_ULONG, SUI_TERMVAL_ULONG, 0}, /* op sign was '^' */
 {"shr", 2, (sui_term_operation_t*)sui_term_op_shr,
    SUI_TERMVAL_ULONG, SUI_TERMVAL_ULONG, 0}, /* op sign was 'R' */
 {"shl", 2, (sui_term_operation_t*)sui_term_op_shl,
    SUI_TERMVAL_ULONG, SUI_TERMVAL_ULONG, 0}, /* op sign was 'L' */
 {"=", 2, (sui_term_operation_t*)sui_term_op_equal,
    SUI_TERMVAL_LONG | SUI_TERMVAL_ULONG | SUI_TERMVAL_UTF8,
    SUI_TERMVAL_LONG | SUI_TERMVAL_ULONG | SUI_TERMVAL_UTF8, 0},
 {"!", 2, (sui_term_operation_t*)sui_term_op_nonequal,
    SUI_TERMVAL_LONG | SUI_TERMVAL_ULONG | SUI_TERMVAL_UTF8,
    SUI_TERMVAL_LONG | SUI_TERMVAL_ULONG | SUI_TERMVAL_UTF8, 0}, /* op sign was '!' */
 {"<", 2, (sui_term_operation_t*)sui_term_op_less,
    SUI_TERMVAL_LONG | SUI_TERMVAL_ULONG | SUI_TERMVAL_UTF8,
    SUI_TERMVAL_LONG | SUI_TERMVAL_ULONG | SUI_TERMVAL_UTF8, 0},
 {">", 2, (sui_term_operation_t*)sui_term_op_great,
    SUI_TERMVAL_LONG | SUI_TERMVAL_ULONG | SUI_TERMVAL_UTF8,
    SUI_TERMVAL_LONG | SUI_TERMVAL_ULONG | SUI_TERMVAL_UTF8, 0},
 {"<=", 2, (sui_term_operation_t*)sui_term_op_lessorequal,
    SUI_TERMVAL_LONG | SUI_TERMVAL_ULONG | SUI_TERMVAL_UTF8,
    SUI_TERMVAL_LONG | SUI_TERMVAL_ULONG | SUI_TERMVAL_UTF8, 0}, /* op sign was '{' */
 {">=", 2, (sui_term_operation_t*)sui_term_op_greatorequal,
    SUI_TERMVAL_LONG | SUI_TERMVAL_ULONG | SUI_TERMVAL_UTF8,
    SUI_TERMVAL_LONG | SUI_TERMVAL_ULONG | SUI_TERMVAL_UTF8, 0}, /* op sign was '}' */
 {"?", 3, (sui_term_operation_t*)sui_term_op_select, SUI_TERMVAL_LONG | SUI_TERMVAL_ULONG, SUI_TERMVAL_ALL, SUI_TERMVAL_ALL},
 {"-", 1, (sui_term_operation_t*)sui_term_op_minus, SUI_TERMVAL_LONG, 0, 0},
 {"~", 1, (sui_term_operation_t*)sui_term_op_not, SUI_TERMVAL_LONG | SUI_TERMVAL_ULONG, 0, 0},

 {"V", 0, (sui_term_operation_t*)sui_term_op_get_value, SUI_TERMVAL_ALL, 0, 0}, /* op sign isn't directly used */
 {"S", 0, (sui_term_operation_t*)sui_term_op_get_subterm, SUI_TERMVAL_ALL, 0, 0}, /* op sign isn't directly used */

 {"size", 1, (sui_term_operation_t*)sui_term_op_get_size, SUI_TERMVAL_UTF8, 0, 0},
 {"getp", 2, (sui_term_operation_t*)sui_term_op_get_dinfo_value, SUI_TERMVAL_DINFO, SUI_TERMVAL_LONG, 0},

 {"dit", 1, (sui_term_operation_t*)sui_term_op_test_dinfo, SUI_TERMVAL_DINFO, 0, 0},

 { NULL, 0, NULL, 0, 0, 0}
};

/**
 * sui_term_find_op_in_table - function return pointer to operation function according to sign
 * @table: pointer to term operation table
 * @sign: desired function sign
 * @unary: return unary function, otherwise return binary function 
 */
sui_term_op_table_t *sui_term_find_op_in_table(sui_term_op_table_t *table, char *sign, int argc)
{
  while(table && table->opfcn) {
    if (argc < 0) {
      if (!strcmp(sign, table->op_sign)) return table;
    } else {
      if ((table->nargin == argc) && !strcmp(sign, table->op_sign)) return table;
    }
    table++;
  }
ul_logdeb("!!! Unknown term operator '%s' with %d arguments !!!\n",sign,argc);
  return NULL;
}

//inline
int sui_term_compute_atomicterm(sui_term_atomic_t *aterm, sui_term_value_t *result)
{
  sui_term_value_t first;
  sui_term_value_t second;
  sui_term_value_t third;
  int ret = SUI_RET_NCON;

  if ( result && aterm && aterm->op_info) {
    if ( !aterm->op_info->opfcn) return SUI_RET_NCON;
    switch (aterm->op_info->nargin) {
      case 0:
        return ((sui_term_operation_t *)aterm->op_info->opfcn)(aterm, result);
      case 1:
        sui_term_value_init( &first);
        if ((ret = sui_term_compute_atomicterm( aterm->first, &first))==SUI_RET_OK) {
          if ((ret = sui_term_value_implicit_convert( &first, aterm->op_info->first_type))==SUI_RET_OK) {
            ret = ((sui_term_op_1arg_t *)aterm->op_info->opfcn)(result, &first);
          }
        }
        sui_term_value_clear( &first);
        break;
      case 2:
        sui_term_value_init( &first);
        sui_term_value_init( &second);
        if ((ret = sui_term_compute_atomicterm( aterm->first, &first))==SUI_RET_OK) {
          if ((ret = sui_term_compute_atomicterm( aterm->second, &second))==SUI_RET_OK) {
            if ((ret = sui_term_value_implicit_convert( &first, aterm->op_info->first_type))==SUI_RET_OK) {
              if ((ret = sui_term_value_implicit_convert( &second, aterm->op_info->second_type))==SUI_RET_OK) {
                ret = ((sui_term_op_2arg_t *)aterm->op_info->opfcn)(result, &first, &second);
              }
            }
          }
        }
        sui_term_value_clear( &first);
        sui_term_value_clear( &second);
        break;
      case 3:
        sui_term_value_init( &first);
        sui_term_value_init( &second);
        sui_term_value_init( &third);
        if ((ret = sui_term_compute_atomicterm( aterm->first, &first))==SUI_RET_OK) {
          if ((ret = sui_term_compute_atomicterm( aterm->second, &second))==SUI_RET_OK) {
            if ((ret = sui_term_compute_atomicterm( aterm->third, &third))==SUI_RET_OK) {
              if ((ret = sui_term_value_implicit_convert( &first, aterm->op_info->first_type))==SUI_RET_OK) {
                if ((ret = sui_term_value_implicit_convert( &second, aterm->op_info->second_type))==SUI_RET_OK) {
                  if ((ret = sui_term_value_implicit_convert( &third, aterm->op_info->third_type))==SUI_RET_OK) {
                    ret = ((sui_term_op_3arg_t *)aterm->op_info->opfcn)(result, &first, &second, &third);
                  }
                }
              }
            }
          }
        }
        sui_term_value_clear( &first);
        sui_term_value_clear( &second);
        sui_term_value_clear( &third);
        break;
    }
  }
  return ret;
}

//inline
int sui_term_compute_term(sui_term_t *term, sui_term_value_t *result)
{
  int ret;
  if ( term && term->subterm && result) {
//		if ( term->flags & SUI_TERM_VALID)
//			return SUI_RET_OK;
//		else {
    ret = sui_term_compute_atomicterm( term->subterm, &term->result);
    *result = term->result;
//			term->flags |= SUI_TERM_VALID;
    return ret;
//		}
  }
  return SUI_RET_NCON;
}



/**
 * sui_term_aterm_destroy - recursive destroy term binary tree
 */
void sui_term_aterm_destroy(sui_term_atomic_t *atm)
{
  if (!atm) return;
  sui_term_value_clear(&atm->data);
  if (atm->first)
    sui_term_aterm_destroy(atm->first);
  if (atm->second)
    sui_term_aterm_destroy(atm->second);
  if (atm->third)
    sui_term_aterm_destroy(atm->third);
  sui_free(atm);
}

sui_refcnt_t sui_term_inc_refcnt(sui_term_t *term)
{
  if (!term) return SUI_REFCNTERR;
  if(term->refcnt>=0) term->refcnt++;
  return term->refcnt;
}

sui_refcnt_t sui_term_dec_refcnt(sui_term_t *term)
{
  sui_refcnt_t ref;
  if (!term) return SUI_REFCNTERR;
  if(term->refcnt>0) term->refcnt--;
  if(!(ref=term->refcnt)) {
    sui_term_aterm_destroy(term->subterm);
    sui_free(term);
  }
  return ref;
}

/******************************************************************************/
/* translate string to term */

enum sui_term_parser_states {
  SUI_TSTATE_OPER = 0x0000,
  SUI_TSTATE_ARG1 = 0x0001,
  SUI_TSTATE_ARG2 = 0x0002,
  SUI_TSTATE_ARG3 = 0x0003,
  SUI_TSTATE_END  = 0x0004,
  SUI_TSTATE_WHAT = 0x000F,

  SUI_TSTATE_BEFORE = 0x0000,
  SUI_TSTATE_INSIDE = 0x0010,
  SUI_TSTATE_BEHIND = 0x0020,
  SUI_TSTATE_WHERE  = 0x0030,

  SUI_TSTATE_ARG_OP  = 0x0000,
  SUI_TSTATE_ARG_ID  = 0x0100,
  SUI_TSTATE_ARG_NUM = 0x0200,
  SUI_TSTATE_ARG_STR = 0x0300,
  SUI_TSTATE_ARG_MASK= 0x0300,
};

#define sui_term_set_state_what(state, what) state = (state & ~SUI_TSTATE_WHAT) | what
#define sui_term_set_state_where(state, where) state = (state & ~SUI_TSTATE_WHERE) | where
#define sui_term_set_state_arg(state, what) state = (state & ~SUI_TSTATE_ARG_MASK) | what

/**
 * sui_term_parse_token - read one part
 */
char sui_term_tokenize( char **term, char *buffer, int buflen, char *delimiters)
{
  char *ptr, *del;
  int ret = 0, inside = 0;

  if (!term) return -1;
  ptr = *term;
  /* remove white space */
  while ((*ptr<=' ') && *ptr) ptr++;
  inside = 2; /* outside string */

  while (!ret && *ptr && (buflen-1)) {
    if (inside>1) {
      del = delimiters;
      while (*del) { /* try find delimiter */
        if (*del == *ptr) ret = *del;
        del++;
      }
    }
    if (inside==2 && *ptr=='\'') inside = 1;
    if (inside==1 && *ptr=='\'') {
      if (*(ptr+1)!='\'') inside = 2;
    }
    if (inside==2 && *ptr<=' ') inside=3;
    if (inside<3) {
      if (!ret) {
        *buffer++ = *ptr;
        buflen--;
      }
    }
    if (!ret) ptr++;
  }
  *buffer = 0;
  *term = ptr;
  return ret;
}

#define SUI_TERM_BUFSIZE 64

/**
 * sui_term_str2arg
 */
sui_term_atomic_t *sui_term_str2arg(char *str)
{
  sui_term_atomic_t *arg = sui_term_create_aterm();
  char *tmpstr = str;

  if (!arg) return arg;
  sui_term_value_init(&arg->data);
  arg->op_info = sui_term_find_op_in_table(sui_term_implemented_operation_table, "V", 0);

  if (*str == '\'') { /* string */
    int len = strlen(str);
    //*(str+len-1)=0;
    arg->data.type = SUI_TERMVAL_UTF8;
    arg->data.value.u8 = sui_utf8_dynamic(str+1, -1, len-2);

  } else if ((*str>= '0' && *str<='9')||(*str=='-')) { /* number */
    unsigned long ulong;
    int minus = 0;
    if (*str=='-') {
      minus=1; str++;
    }
    if (sui_str_to_ulong(&str, &ulong) == SUI_RET_OK) {
      if (ulong<0x80000000) {
        arg->data.type = SUI_TERMVAL_LONG;
        arg->data.value.slong = (long)ulong;
        if (minus) arg->data.value.slong = -arg->data.value.slong;
      } else {
        if ( !minus) {
          arg->data.type = SUI_TERMVAL_ULONG;
          arg->data.value.ulong = ulong;
        }
      }
    }
  } else if ((*str>='A' && *str<='Z')||(*str>='a'&& *str<='z')) { /* identifier */
    if (sui_term_resolve_id(str,&arg->data)!=SUI_RET_OK) {
      sui_term_value_clear(&arg->data);
    }
  }

  if (arg->data.type == SUI_TERMVAL_NONE) {
    sui_term_aterm_destroy(arg);
    arg = NULL;
    ul_logdeb("Bad term argument '%s'!\n",tmpstr);
  }
  return arg;
}


/**
 * sui_term_from_prefixstring_rec - create condition binary tree from equation in prefix format
 * @equ: string with equation in prefix format
 * @fnc_get_var: pointer to function, which translate name of variable to pointer to dinfo
 */
sui_term_atomic_t *sui_term_from_prefixstring( char **term)
{
  char name[SUI_TERM_BUFSIZE], ch, *ptr;
  int narg = 0;
//  int state = SUI_TSTATE_OPER | SUI_TSTATE_BEFORE;
  sui_term_atomic_t *node = NULL;
  sui_term_atomic_t *arg = NULL;
  sui_term_op_table_t *oper;

  if (!term || !*term) return NULL;
  ptr = *term;

  /* operation */
  ch = sui_term_tokenize( &ptr, name, SUI_TERM_BUFSIZE, "(");
  if (!ch) return NULL; /* error - no operation */
  oper = sui_term_find_op_in_table(sui_term_implemented_operation_table, name, -1);
  if (!oper) return NULL; /* no known operation */
  node = sui_term_create_aterm();
  node->op_info = oper;
  ptr++;

  /* get first argument */
  while (narg<3) {
    char *tmpptr = ptr;
    ch = sui_term_tokenize( &ptr, name, SUI_TERM_BUFSIZE, "(,)");
    if (!ch) break; /* no correct end of term */
    if (ch=='(') { /* argument is a subterm */
/* TODO:hierarchy */
      ptr = tmpptr;
      arg = sui_term_from_prefixstring(&ptr);
      ch = sui_term_tokenize( &ptr, name, SUI_TERM_BUFSIZE, ",)"); /* find the ending ',' or ')' */
      ptr++;
    } else if (ch==')' || ch==',') { /* end of operation arguments */
      ptr++;
      arg = sui_term_str2arg(name);
    }
    if (!arg) goto sui_term_parse_error; /* no correct argument */

    if (narg==0) node->first = arg;
    else if (narg==1) node->second = arg;
    else node->third = arg;
    narg++;

    if (ch==')') { /* end of operation - check op_info number of arguments */
      if ( node->op_info->nargin != narg) { /* try correct op function according to number of arguments */
        oper = sui_term_find_op_in_table(sui_term_implemented_operation_table, (char *)node->op_info->op_sign, narg);
        if (!oper)
          goto sui_term_parse_error;
        else
          node->op_info = oper;
      }
      break; /* correct*/
    }

  }
  *term = ptr;
  return node;

sui_term_parse_error: /* $state holds the last parse phase where is an error */
  if ( node) {
    sui_term_aterm_destroy( node);
    node = NULL;
  }
  return NULL;
}

/**
 * sxml_create_equation_from_string - translate string to equation
 */
//static inline 
sui_term_t *sui_term_create_from_prefix_string( char *prefixstr)
{
  char *pstr = prefixstr;
  sui_term_t *term = sui_malloc(sizeof(sui_term_t));
  if (term) {
    term->refcnt = 1;
    term->subterm = sui_term_from_prefixstring(&pstr);
    term->flags = SUI_TERM_INVALID;
    if (!term->subterm) { /* somewhere in term was an error */
      sui_term_dec_refcnt( term);
      term = NULL;
    }
  }
  return term;
}
