/* sui_print.h
 *
 * SUITK export to printers (text based).
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#ifndef _SUI_EXPORT_TO_PRINTER_HEADER_
#define _SUI_EXPORT_TO_PRINTER_HEADER_

#include <suitk/suitk.h>
#include <ul_dbuff.h>

struct sui_print_filter;
struct sui_print_layout;


/******************************************************************************/
/**
 * struct sui_print_line - description of one line used mainly for filtering
 */
typedef struct sui_print_line {
  ul_dbuff_t content;
  int align;
  int idx_align;

  struct sui_print_line *nextline;
} sui_print_line_t;

sui_print_line_t *sui_print_create_line(void);
sui_print_line_t *sui_print_destroy_line(sui_print_line_t *line);


/******************************************************************************/
/*
 * sui_print_filter_t - defines filter function type
 */
typedef int sui_print_filter_fnc_t(struct sui_print_layout *prlay,
                              struct sui_print_filter *pfilt, ul_dbuff_t *dbuf);

/**
 * struct sui_print_filter - description of a printer filter
 * @ffnc: Pointer to a filter function.
 * @info: Function parameter.
 */
typedef struct sui_print_filter {
  sui_refcnt_t refcnt;
  sui_print_filter_fnc_t *ffnc;
  unsigned long flags;
//  union {
    long value;
    utf8 *text;
//    void *ptr;
//  } args;
} sui_print_filter_t;

/**
 * enum sui_print_filter_flags
 */
enum sui_print_filter_flags {
  SUI_PRNFLT_NONE     = 0x0000,
  SUI_PRNFLT_USEONCE  = 0x0000,
  SUI_PRNFLT_EACHPAGE = 0x0001,
};


/******************************************************************************/
/**
 * struct sui_print_layout - description of a printer layout
 * @devices: names of supported devices
 * @paperwidth: width of paper [number of characters]
 * @paperheight: height of paper [number of characters] or -1 for roll paper
 * @filters: list of filters and theirs arguments
 * @layout: pointer to the print layout
 *
 */
typedef struct sui_print_layout {
  sui_refcnt_t refcnt;
  utf8 *devices;
  int paperwidth;
  int paperheight;
  unsigned long flags;
// newline, newpage and others characters ???
  int numfilters;
  sui_print_filter_t **filters;

  utf8 *layout;
} sui_print_layout_t;

/**
 * enum sui_print_layout_flags - Suitk print layout flags
 * @SUI_PRN_END_NONE: Any character won't be added at the end of each line
 * @SUI_PRN_END_CR: CR sign will be added at the end of each line
 * @SUI_PRN_END_LF: LF sign will be added at the end of each line
 * @SUI_PRN_END_CRLF: CR followed by LF sign will be added at the end of each line
 */
enum sui_print_layout_flags {
  SUI_PRN_END_NONE = 0x00000000,
  SUI_PRN_END_CR   = 0x00000001,
  SUI_PRN_END_LF   = 0x00000002,
  SUI_PRN_END_CRLF = 0x00000003,
  
  SUI_PRN_END_MASK = 0x00000003,

};

sui_refcnt_t sui_pfilter_inc_refcnt(sui_print_filter_t *spf);
sui_refcnt_t sui_pfilter_dec_refcnt(sui_print_filter_t *spf);
sui_print_filter_fnc_t *sui_pfilter_from_name(char *fnc_name);


sui_refcnt_t sui_playout_inc_refcnt(sui_print_layout_t *spl);
sui_refcnt_t sui_playout_dec_refcnt(sui_print_layout_t *spl);
int sui_playout_add_filter(sui_print_layout_t *spl, sui_print_filter_t *spf);
int sui_playout_print(ul_dbuff_t *outbuf, sui_print_layout_t *spl);

/* for testing on linux */
int sui_playout_debug_print(sui_print_layout_t *spl);

#endif /* _SUI_EXPORT_TO_PRINTER_HEADER_ */
