/* sui_wtext.c
 *
 * SUITK text widget. Static and edit single/multi line text with MOBIL keyboard support
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#include "sui_wtext.h"

#include <ul_log.h>

UL_LOG_CUST(ulogd_suitk)

static utf8 *sui_wtext_null_str = U8"";

/******************************************************************************
 * Text widget slot functions
 ******************************************************************************/
/**
 * sui_wtext_append - Slot function appends a text to the widget's text.
 * @widget: Pointer to a text widget.
 * @str: Appended text.
 *
 * The slot function appends a text to a widget's text.
 *
 * Return Value: The slot function returns zero as success and -1 as an error and 1 as an warning.
 *
 * File: sui_wtext.c
 */
int sui_wtext_append(sui_widget_t *widget, utf8 *str)
{
  suiw_text_t *wtxt;
  int ret = -1;
  // ul_logdeb("SS:wtext->insert\n");
  if (!widget) return ret;
  wtxt = sui_txtwdg(widget);
  if (str && wtxt->di && (wtxt->di->tinfo == SUI_TYPE_UTF8)) {
    utf8 *text = NULL;
    sui_rd_utf8(wtxt->di, 0, &text);
    if (sui_utf8_cat(text, str) > 0) {
    //    sui_wr_utf8(wtxt->di, 0, &text);
      sui_obj_emit((sui_obj_t *)widget, SUI_SIG_CHANGED, text);
      ret = 0;
    } else
      ret = 1;
    sui_utf8_dec_refcnt(text);
  } else
    ret = -1;
  return ret;
}

/**
 * sui_wtext_set_text - Slot function set a new text to the widget's text.
 * @widget: Pointer to a text widget.
 * @text: Pointer to a new text which will be connected with the widget
 *
 * The slot function sets a new text to the widget's text.
 *
 * Return Value: The slot function returns zero as success and -1 as an error and 1 as an warning.
 *
 * File: sui_wtext.c
 */
int sui_wtext_set_text(sui_widget_t *widget, utf8 *text)
{
  suiw_text_t *wtxt;
  int ret = -1;
  if (!widget) return ret;
  wtxt = sui_txtwdg(widget);
  if (!text) text = sui_wtext_null_str;

  if (!wtxt->di) { /* create new utf8 dinfo ... */
    ul_logdeb("create new utf8 wtext dinfo \n");
    ret = -1;
  } else { /* replace text in dinfo with new utf8 */
    if ((sui_utf8_get_type(text) == UTF8_DYNAMIC_STRUCT) ||
        (sui_utf8_get_type(text) == UTF8_DYNAMIC_FULL)) {
      sui_wr_utf8(wtxt->di, 0, &text);
      ret = 0;
    } else { /* create copy of text */
      utf8 *copy = sui_utf8_dup(text);
      sui_wr_utf8(wtxt->di, 0, &copy);
      sui_utf8_dec_refcnt(copy);
      ret = 0;
    }
  }
  return ret;
}

/**
 * sui_wtext_clear - Slot function clears a widget's text.
 * @widget: Pointer to a text widget.
 *
 * The slot function clears a widget's text.
 *
 * Return Value: The slot function returns zero as success and -1 as an error and 1 as an warning.
 *
 * File: sui_wtext.c
 */
int sui_wtext_clear(sui_widget_t *widget)
{
  return sui_wtext_set_text(widget, NULL);
}

/******************************************************************************/
#define SUI_EDITEDTEXT    1
#define SUI_RIGHTTEXT     0

/*
 * sui_text_read_value - Internal function for reading text from normal or editing text widget
 * @widget: Pointer to a text widget.
 * @buf: Pointer to an output buffer for obtained text.
 * @editing: Flag if the function will read normal or edited widget text.
 *
 * The function reads widget's text or widget's editing text into an output buffer.
 *
 * Return Value: The function returns dinfo reading error codes (SUI_RET_...).
 *
 * File: sui_wtext.c
 */
int sui_text_read_value(sui_widget_t *widget, utf8 **buf, int editing)
{
  suiw_text_t *wt = sui_txtwdg(widget);
  if (sui_test_flag(widget, SUFL_EDITED)) {
    if ((editing && !sui_test_flag(widget, SUTF_ONLINEEDIT)) ||
        (!editing && sui_test_flag(widget, SUTF_ONLINEEDIT))) { /* return value from intvalue */
      sui_utf8_inc_refcnt(wt->edtext);
      *buf = wt->edtext;
    } else {
      return sui_rd_utf8(wt->di, 0, buf);
    }
  } else {                                                      /* return value directly from dinfo */
    *buf = sui_dinfo_2_utf8(wt->di, 0, wt->format, 0);
    if (*buf==NULL) {
      *buf = sui_wtext_null_str;
    }
    return ((*buf!=NULL) ? SUI_RET_OK:SUI_RET_NRDY); //sui_rd_utf8(wt->di, 0, buf);
  }
  return SUI_RET_OK;
}

/*
 * sui_text_write_value - Internal function for writing normal or editing text to text widget
 * @widget: Pointer to a text widget.
 * @buf: Pointer to an input text buffer.
 * @editing: Flag if the function will write normal or edited widget text.
 *
 * The function writes a new text to the widget's text.
 *
 * Return Value: The function returns dinfo writing error codes (SUI_RET_...).
 *
 * File: sui_wtext.c
 */
int sui_text_write_value(sui_widget_t *widget, utf8 **buf, int editing)
{
  suiw_text_t *wt = sui_txtwdg(widget);
  if ((editing && !sui_test_flag(widget, SUTF_ONLINEEDIT)) ||
      (!editing && sui_test_flag(widget, SUTF_ONLINEEDIT))) { /* save value to intvalue */
    sui_utf8_dec_refcnt(wt->edtext);
    sui_utf8_inc_refcnt(*buf);
    wt->edtext = *buf;
  } else {                                                    /* save value directly to dinfo*/
    return sui_wr_utf8(wt->di, 0, buf);
  }
  return SUI_RET_OK;
}

/*
 * sui_text_start_edit - Internal function starts and initializes text editing
 * @widget: Pointer to a text widget.
 * @clear: Flag if editing starts from clear text.
 *
 * The function initializes editing of the widget's text.
 *
 * Return Value: The function returns zero if editing wasn't started and it returns 1 if the text editing was started.
 *
 * File: sui_wtext.c
 */
int sui_text_start_edit(sui_widget_t *widget, int clear)
{
  suiw_text_t *wtxt = sui_txtwdg(widget);
  if (!sui_test_flag(widget, SUFL_EDITEN) || !wtxt->di) return 0;

  if (!sui_test_flag(widget, SUFL_EDITED)) {
    utf8 *tmpstr;
    /* save current value from dinfo to intvalue - it's the same operation for both cases DIRECTLY/INTERNAL EDIT */
    if (sui_rd_utf8(wtxt->di, 0, &tmpstr) != SUI_RET_OK) return -1;
    if (wtxt->edtext) sui_utf8_dec_refcnt(wtxt->edtext);
    if (sui_utf8_length(tmpstr) == 0) /* protect to uninitialized utf8 */
      wtxt->edtext = sui_utf8(NULL, 1, -1);
    else
      wtxt->edtext = sui_utf8_dup(tmpstr);
    if (wtxt->edtext == NULL) return -1;
    sui_utf8_dec_refcnt(tmpstr);
    /* before editing edit - start with empty clear text */
    if (clear) {
      tmpstr = sui_utf8(NULL,1,-1);
      if (sui_text_write_value(widget, &tmpstr, SUI_EDITEDTEXT) != SUI_RET_OK)
        return -1;
      sui_utf8_dec_refcnt(tmpstr);
    }
    wtxt->cursor = sui_utf8_length(wtxt->edtext);
    wtxt->begsel = -1;
    sui_set_flag(widget, SUFL_EDITED);
    sui_obj_emit((sui_obj_t *)widget, SUI_SIG_EDIT_STARTED);
  }
  return 1;
}

/*
 * sui_text_stop_edit - Internal function stops text editing
 * @widget: Pointer to a text widget.
 * @confirmed: Flag if text editing is confirmed and the changed text will be saved into widget's text.
 *
 * The function stops editing and saves the edited text if the confirmed flag is set.
 *
 * Return Value: The function returns zero if editing wasn't started and it returns 1 if the text editing was stopped.
 *
 * File: sui_wtext.c
 */
int sui_text_stop_edit(sui_widget_t *widget, int confirmed)
{
  suiw_text_t *wtxt = sui_txtwdg(widget);
  sui_event_t e;

  if (!sui_test_flag(widget, SUFL_EDITED) || !wtxt->di) return 0;
  e.what = SUEV_COMMAND;
  e.message.ptr = widget;
  if (confirmed) {
    if (!sui_test_flag(widget, SUTF_ONLINEEDIT)) {
      if (sui_wr_utf8(wtxt->di, 0, &wtxt->edtext) != SUI_RET_OK) {
        e.message.command = SUCM_ERR_WRVAL;
        sui_put_event(&e); /* SUCM_ERROR_WRVAL */
        return -1;
      }
    }
    e.message.command = SUCM_CHANGED;
    sui_put_event(&e); /* SUCM_CHANGED */
  } else {
    if (sui_test_flag(widget, SUTF_ONLINEEDIT)) {
      if (sui_wr_utf8(wtxt->di, 0, &wtxt->edtext) != SUI_RET_OK) {
        e.message.command = SUCM_ERR_WRVAL;
        sui_put_event(&e); /* SUCM_ERROR_WRVAL */
        return -1;
      }
      e.message.command = SUCM_CHANGE;
      sui_put_event(&e); /* SUCM_CHANGE */
    }
  }
  if (wtxt->edtext) {
    sui_utf8_dec_refcnt(wtxt->edtext);
    wtxt->edtext = NULL;
  }
  wtxt->cursor = 0;
  wtxt->begsel = -1;
  sui_clear_flag(widget, SUFL_EDITED);
  sui_obj_emit((sui_obj_t *)widget, SUI_SIG_EDIT_STOPPED);
  return 1;
}


/******************************************************************************
 * Multi-line text auxiliary functions
 ******************************************************************************/
/**
 * sui_wtext_get_line - Get pointer to the required description of a text line
 * @headblock: Pointer to a block of text lines.
 * @line: Index of the required line (from zero).
 *
 * The function obtains a pointer to the required text line.
 *
 * Return Value: The function returns a pointer to the required text line or NULL.
 *
 * File: sui_wtext.c
 */
sui_wtext_line_t *sui_wtext_get_line(sui_wtext_block_t *head, int line)
{
  while (head) {
    if (line < head->cnt) {	/* line is in this block */
      if (!head->lines) return NULL;
      return (head->lines + line);
    }
    line -= head->cnt;
    head = head->next;
  }
  return NULL;
}

/**
 * sui_wtext_clear_lines - Clears all text line descriptions.
 * @head: Pointer to the first text line block.
 *
 * The function clears all descriptions of all lines.
 *
 * Return Value: The function returns zero if it was successfully processed and -1 as an error.
 *
 * File: sui_wtext.c
 */
int sui_wtext_clear_lines(sui_wtext_block_t **head)
{
  sui_wtext_block_t *block = *head;
  if (!head) return -1;
  while (block) {
    if (block->lines && block->cnt)
      memset(block->lines, 0, block->cnt * sizeof(sui_wtext_line_t));
    block = block->next;
  }
  return 0;
}

#define WTEXT_MIN_BLOCK_SIZE 8

/**
 * sui_wtext_set_line - Set description of one line (and create new line's block if it needs)
 * @head: Pointer to the first text line block.
 * @line: Index of the required line.
 * @data: Pointer to a line description structure, which contains line's data.
 *
 * The function sets description of the required line. If the line hasn't exist yet,
 * it creates new block with the required line.
 *
 * Return Value: The function returns zero if it was successfully processed and -1 as an error.
 *
 * File: sui_wtext.c
 */
int sui_wtext_set_line(sui_wtext_block_t **head, int line, sui_wtext_line_t *data)
{
  sui_wtext_line_t *lnset = NULL;
  sui_wtext_block_t *bl, *last; /* last block */
  if (!head || !data) return -1;
  bl = *head; last = NULL;
  while(bl) {
    if (line < bl->cnt) break;
    line -= bl->cnt;
    last = bl;
    bl = bl->next;
  }
  if (!bl) { /* block doesn't exist - create and save it */
    bl = malloc(sizeof(sui_wtext_block_t));
    if (!bl) return -1;
    memset(bl, 0, sizeof(sui_wtext_block_t));
    if (last) last->next = bl;
    else *head = bl;
  }
  if (!bl->lines) {
    int szline = line+1;
    if (szline < WTEXT_MIN_BLOCK_SIZE) szline=WTEXT_MIN_BLOCK_SIZE;
    bl->lines = malloc(szline*sizeof(sui_wtext_line_t));
    if (!bl->lines) {
      free(bl); return -1;
    }
    bl->cnt = szline;
    memset(bl->lines, 0, szline*sizeof(sui_wtext_line_t));
  }
  if (bl && bl->lines) {
    lnset = bl->lines + line;
    memcpy(lnset, data, sizeof(sui_wtext_line_t));
    return 0;
  }
  return -1;
}

/**
 * sui_wtext_destroy_lines - Destroy all line's descriptions
 * @head: Pointer to the first text line block.
 *
 * The function removes and releases all line's description structures.
 *
 * Return Value: The function returns zero if it was successfully processed and -1 as an error.
 *
 * File: sui_wtext.c
 */
int sui_wtext_destroy_lines(sui_wtext_block_t **head)
{
  sui_wtext_block_t *block, *tmp;
  if (!head) return -1;
  block = *head;
  while(block) {
    if (block->lines) free(block->lines);
    tmp = block->next;
    free(block);
    block = tmp;
  }
  head = NULL;
  return 0;
}

/******************************************************************************
 * Text basic functions
 ******************************************************************************/
/* FIXME: the MULTILINE and AUTOMULTI must be corrected in get_size, draw, hkey functions */
/**
 * sui_wtext_get_size - Get content size of a text widget
 * @dc: Pointer to a device context.
 * @widget: Pointer to a text widget.
 * @point: Pointer to an output point structure.
 *
 * The function obtain size of a widget content and then returns it.
 * This function replies to the SUCM_GETSIZE event.
 *
 * Return Value: The function returns 1 as the success.
 *
 * File: sui_wtext.c
 */
int sui_wtext_get_size(sui_dc_t *dc, sui_widget_t *widget, sui_point_t *point)
{
  sui_dcfont_t *pdcfont;
  suiw_text_t *wtxt = (suiw_text_t *) widget->data;
  sui_textdims_t tsize;
  utf8 *text = NULL;
  sui_wtext_line_t lastline;

  if (!point) return -1;
  point->x = 0; point->y = 0;
  if (!widget || !widget->style || !widget->style->fonti) return -1;

  {
    utf8 *tmptxt = NULL;
    if (sui_text_read_value(widget, &tmptxt, sui_test_flag(widget, SUFL_EDITED)) != SUI_RET_OK) {
      ul_logmsg("Read text ERROR\n");
    }
    if (sui_test_flag(widget, SUTF_UPPERCASE)) {
      text = sui_utf8_toupper(tmptxt);
    } else if (sui_test_flag(widget, SUTF_LOWERCASE)) {
      text = sui_utf8_tolower(tmptxt);
    } else {
      sui_utf8_inc_refcnt(tmptxt);
      text = tmptxt;
    }
    if (tmptxt) sui_utf8_dec_refcnt(tmptxt);
  }

  pdcfont = sui_font_prepare(dc, widget->style->fonti->font,
                              widget->style->fonti->size);

  switch ((wtxt->txtalign & SUAL_COLUMN) ? SUTF_SINGLELINE :
                                           (widget->flags & SUTF_LINEMASK)) { /* text in column can be only in single-line mode */
    case SUTF_SINGLELINE:
    case SUTF_AUTOSINGLE:
      sui_text_get_size(dc, pdcfont, &tsize, -1, text, wtxt->txtalign);
      wtxt->maxwidth = tsize.w;
      wtxt->lineheight = tsize.h;
      wtxt->cntline = 1;
      if (!tsize.w || !tsize.h)
      {
        sui_fontinfo_t finfo;
        sui_font_get_info(dc, pdcfont, &finfo);
        if (wtxt->txtalign & SUAL_COLUMN) {
          if (!tsize.w) tsize.w = finfo.maxwidth;
        } else {
          if (!tsize.h) tsize.h = finfo.height;
        }
      }
      break;
    case SUTF_MULTILINE:
      {
        utf8char      *tch;  /* pointer to the currently tested character of the text */
        utf8char      *ttmp; /* temporary pointer */
        _UTF_UCS_TYPE  chr;  /* tested character as UINT16 */
        int            chrlen; /* size of the current UTF8 character in bytes */
        int            tlen; /* size of the text in characters */
        int            hlp;
        int            llen; /* length of the last line */
        int            toffs;
  
        wtxt->cntline = 0;
        wtxt->maxwidth = 0;
        wtxt->lineheight = 0;
        tch = sui_utf8_get_text(text);
        tlen = sui_utf8_length(text);
        toffs = 0;
  
        while (tlen>0) {
          ttmp = tch;
          hlp = 0;
          llen = 0;
          do {
            chrlen = sui_utf8_to_ucs(tch, &chr);
            tch += chrlen;
            if (chr!='\t' && chr!='\n') {
              hlp++;
              llen += chrlen;
            }
            tlen--;
            toffs += chrlen;
          } while ((tlen>0) && (chr!='\t' && chr!='\n'));
          sui_text_get_size(dc, pdcfont, &tsize, hlp, (utf8 *) ttmp, wtxt->txtalign);
          lastline.width = tsize.w;
          lastline.chars = hlp;
          lastline.bytes = llen;
          lastline.offset = toffs;
          if (sui_wtext_set_line(&wtxt->head_lines, wtxt->cntline, &lastline) < 0)
            break;
          wtxt->cntline++;
          if (tsize.w > wtxt->maxwidth) wtxt->maxwidth = tsize.w;
          if (tsize.h > wtxt->lineheight) wtxt->lineheight = tsize.h;
        }
        tsize.w = wtxt->maxwidth;
        tsize.h = wtxt->cntline * wtxt->lineheight;
      }
      break;
    case SUTF_AUTOMULTI:
    {
      utf8char *tch;
      int bs = 0;
      sui_coordinate_t lnwidth = 0;
      int ltb = 0; /* length of line text in bytes */
      int ltch = 0;/* length of line text in chars */
      int ltoff = 0; /* offset of line text from beginning */
      int tlen, tbpos = 0;

      memset(&lastline, 0, sizeof(sui_wtext_line_t));
      wtxt->cntline = 0;
      wtxt->maxwidth = 0;
      wtxt->lineheight = 0;
      tch = sui_utf8_get_text(text);
      tlen = sui_utf8_length(text);

      while (ltoff<tlen) {
        sui_text_get_size(dc, pdcfont, &tsize, 1, (utf8 *) tch, wtxt->txtalign);

        if (wtxt->lineheight < tsize.h) wtxt->lineheight = tsize.h;

        bs = sui_utf8_count_bytes(tch, 1);
        if (*tch==' ') {
          lastline.width = lnwidth;
          lastline.chars = ltch;
          lastline.bytes = ltb;
        }
//ul_logdeb("Char: %c\n",*tch);
        if (tsize.w + lnwidth > widget->place.w) { /* line is too long */
          if (!lastline.chars) { /* no space on line */
            lastline.width = lnwidth;
            lastline.chars = ltch;
            lastline.bytes = ltb;
          }

//ul_logdeb("Line #%d is longer (%d) size:[b=%d,ch=%d],offset:%d\n", wtxt->cntline,
//lastline.width, lastline.bytes, lastline.chars, lastline.offset);
          if (sui_wtext_set_line(&wtxt->head_lines, wtxt->cntline, &lastline) < 0)
            break;
          if (wtxt->maxwidth < lastline.width) wtxt->maxwidth = lastline.width;

          tbpos = lastline.offset + lastline.bytes;
          lnwidth -= lastline.width;
          ltch -= lastline.chars;
          ltb -= lastline.bytes;

          if ((*tch==' ') || (ltch)) { /* last character in line is a space - skip it */
            tbpos++;
          } else {
            ltb += bs;
            ltch++;
            lnwidth += tsize.w;
          }
          wtxt->cntline++;
          lastline.width = 0;
          lastline.chars = 0;
          lastline.bytes = 0;
          lastline.offset = tbpos;
//ul_logdeb("-remain (%d) size:[b=%d,ch=%d],offset:%d\n", lnwidth, ltb, ltch, lastline.offset);
        } else {
          ltb += bs;
          ltch++;
          lnwidth += tsize.w;
        }

        ltoff++; /* next char */
        tbpos += bs;
        tch += bs;
      }

      if (ltch>0) {
//ul_logdeb("Line %d -remain (%d) size:[b=%d,ch=%d],offset:%d\n", wtxt->cntline, lnwidth, ltb, ltch, lastline.offset);
        lastline.width = lnwidth;
        lastline.bytes = ltb;
        lastline.chars = ltch;
        sui_wtext_set_line(&wtxt->head_lines, wtxt->cntline, &lastline);
        if (wtxt->maxwidth < lastline.width) wtxt->maxwidth = lastline.width;
        wtxt->cntline++;
      }

      tsize.w = wtxt->maxwidth;
      tsize.h = wtxt->cntline * wtxt->lineheight;
    }
      break;
  }

// correct size with style->gap space
/*
	if (sui_style_test_flag(widget->style, SUSFL_FRAME)) {
		tsize.b = 2*widget->style->gap;
	} else
		tsize.b = 0;
*/
   if(wtxt->txtalign & SUAL_COLUMN)
      tsize.h += widget->style->gap;
   else
      tsize.w += widget->style->gap;

   sui_utf8_dec_refcnt(text);
   point->x = tsize.w; point->y = tsize.h;
  return 1;
}

/**
 * sui_wtext_auto_size - Change text widget size according to its content
 * @dc: Pointer to a device context.
 * @widget: Pointer to a text widget.
 *
 * The function computes widget's content size and sets widget's dimensions to
  * the computed values.
 *
 * Return Value: The function returns 1 as the success.
 *
 * File: sui_wtext.c
 */
int sui_wtext_auto_size(sui_dc_t *dc, sui_widget_t *widget)
{
  sui_point_t size;
  sui_widget_compute_size(dc, widget, &size);
  widget->place.w = size.x; widget->place.h = size.y;
  return 1;
}

/**
 * sui_wtext_init - Initiate a text widget.
 * @widget: Pointer to a widget.
 * @event: Pointer to an INIT event.
 *
 * The function initiates the text widget.
 *
 * Return Value: The function does not return value.
 *
 * File: sui_wtext.c
 */
void sui_wtext_init(sui_widget_t *widget, sui_dc_t *dc)
{
  suiw_text_t *wtxt = sui_txtwdg(widget);
  /* set default values */
  wtxt->txtalign |= SUAL_INSIDE;
//  wtxt->mk_last = 0;
  wtxt->mk_mode = SUKM_CAPITALS;

  if (widget->place.w<0 || widget->place.h<0)
  {
    sui_point_t size;
    sui_widget_compute_size(dc, widget, &size);
    if (widget->place.w < 0) widget->place.w = size.x;// + 2*widget->style->space;
    if (widget->place.h < 0) widget->place.h = size.y;// + 2*widget->style->space;
  }
}

/**
 * sui_wtext_draw - Draw content of a text widget
 * @dc: Pointer to a device context.
 * @widget: Pointer to a text widget.
 *
 * The function draws content of the text widget.
 *
 * Return Value: The function returns 1 when the event is processed and -1 as an error.
 *
 * File: sui_wtext.c
 */
int sui_wtext_draw(sui_dc_t *dc, sui_widget_t *widget)
{
  int show, i;
  suiw_text_t *wtxt = sui_txtwdg(widget);
  sui_dcfont_t *pdcfont;
  utf8 *str = NULL;
  sui_point_t origin;
  sui_point_t ts;
  sui_point_t tsize;
  sui_point_t box;
  sui_textdims_t psize;
  utf8 *text = NULL;
  utf8 *tmptxt = NULL;

//   int focframe = sui_style_is_flag(widget, SUSFL_FOCUSFRAME, SUSFL_FOCUSFRAME) &&
//                 sui_is_flag(widget, SUFL_FOCUSED, SUFL_FOCUSED); /* FIXME: this is only patch for fucusframed widgets - it needs system solution !!! */

  if (sui_test_flag(widget, SUFL_EDITED)) {
    sui_text_read_value(widget, &tmptxt, SUI_EDITEDTEXT);
  } else {
    tmptxt = sui_dinfo_2_utf8(wtxt->di, 0, wtxt->format, 0);
  }
  if (sui_test_flag(widget, SUTF_UPPERCASE)) {
    text = sui_utf8_toupper(tmptxt);
  } else if (sui_test_flag(widget, SUTF_LOWERCASE)) {
    text = sui_utf8_tolower(tmptxt);
  } else {
    sui_utf8_inc_refcnt(tmptxt);
    text = tmptxt;
  }
  if (tmptxt) sui_utf8_dec_refcnt(tmptxt);

  pdcfont = sui_font_prepare(dc, widget->style->fonti->font,
                              widget->style->fonti->size);
  sui_font_set(dc, pdcfont);

  sui_hevent_command(widget, SUCM_GETSIZE, dc, (long) &ts);

  if (sui_test_flag(widget, SUTF_PASSWORD)) {
    show = sui_utf8_length(text);
    str = sui_utf8(NULL, show, -1); /* create utf8 string (allocate buffer) */
    for(i = 0; i < show; i++) {
      sui_utf8_add_char(str, (utf8char *)"X");
    }
  } else {
    sui_utf8_inc_refcnt(text);
    str = text;
  }

  sui_set_widget_bgcolor(dc, widget, SUC_BACKGROUND);
  sui_set_widget_fgcolor(dc, widget, SUC_ITEM);

  if (!sui_is_flag(widget, SUTF_LINEMASK, SUTF_SINGLELINE) &&
      !(wtxt->txtalign & SUAL_COLUMN)) {
    utf8char *tch;
    sui_coordinate_t ox;
    sui_wtext_line_t *line;

    tch = sui_utf8_get_text(text);
    box.x = widget->place.w; box.y = widget->place.h;
    sui_align_object(&box, &origin, &ts, 0, widget->style, wtxt->txtalign);

//sui_draw_rect(dc, origin.x, origin.y, ts.x, ts.y, 1);
    i = 0;
    while(i < wtxt->cntline) {
      line = sui_wtext_get_line(wtxt->head_lines, i);
      if (!line) break; /* next line doesn't exist */
      /* aligning */
      if ((wtxt->txtalign & SUAL_HORMASK) == SUAL_LEFT) {
        ox = 0;
      } else {
        ox = ts.x - line->width;
        if ((wtxt->txtalign & SUAL_HORMASK) != SUAL_RIGHT)
          ox = ox / 2;
      }
      /* drawing */
//ul_logdeb("Draw line #%d: offs=%d, len=%d\n",i,line->offset,line->chars);
      sui_draw_text(dc, origin.x + ox, origin.y, line->chars,
                    (utf8*)(tch + line->offset), wtxt->txtalign);
      origin.y += wtxt->lineheight;
      i++;
    }
  } else {
    sui_text_get_size(dc, pdcfont, &psize, wtxt->cursor, str, wtxt->txtalign);
    tsize.x = psize.w; tsize.y = psize.h;

//ul_logdeb("('%s'),ts.x=%d,ts.y=%d,or.x=%d,or.y=%d\n",sui_utf8_get_text(str), ts.x,ts.y,origin.x,origin.y);
    box.x = widget->place.w; box.y = widget->place.h;
    sui_align_object(&box, &origin, &ts, 0, widget->style, wtxt->txtalign);

/* correct position to current cursor position */
    if (wtxt->txtalign & SUAL_COLUMN) {
      if (origin.y +tsize.y < widget->style->gap)
        origin.y = widget->style->gap - tsize.y;
      else
        if (origin.y + tsize.y > widget->place.h - widget->style->gap)
          origin.y = widget->place.h - widget->style->gap - tsize.y;
    } else {
      if (origin.x + tsize.x < widget->style->gap)
        origin.x = widget->style->gap - tsize.x;
      else
        if (origin.x + tsize.x > widget->place.w - widget->style->gap)
          origin.x = widget->place.w - widget->style->gap - tsize.x;
    }


//		if (focframe) sui_clear_flag(widget, SUFL_HIGHLIGHTED); /* FIXME: this is only patch for fucusframed widgets - it needs system solution !!! */

    if (wtxt->begsel < 0) {
//ul_logdeb("Draw text \n");
      sui_draw_text(dc, origin.x, origin.y, -1, str, wtxt->txtalign);
    } else { // part of text is selected
      int first, last, lstr, old;
      utf8char *chstr;

      if (wtxt->txtalign & SUAL_COLUMN)
        old = origin.y;
      else
        old = origin.x;
      if (wtxt->begsel < wtxt->cursor) {
        first = wtxt->begsel;
        last = wtxt->cursor;
      } else {
        first = wtxt->cursor;
        last = wtxt->begsel;
      }
      chstr = sui_utf8_get_text(str);
      lstr = sui_utf8_length(str) - last;
      last -= first;
      sui_draw_text(dc, origin.x, origin.y, first, (utf8 *) chstr, wtxt->txtalign);
      sui_text_get_size(dc, pdcfont, &psize, first, (utf8 *) chstr, wtxt->txtalign);

      if (wtxt->txtalign & SUAL_COLUMN)
        origin.y += psize.h;
      else
        origin.x += psize.w;
      chstr += sui_utf8_count_bytes(chstr, first);
      sui_set_widget_bgcolor(dc, widget, SUC_BACKGROUND + SUC_SELECTED);
      sui_set_widget_fgcolor(dc, widget, SUC_ITEM + SUC_SELECTED);
      sui_draw_text(dc, origin.x, origin.y, last, (utf8 *) chstr, wtxt->txtalign);
  
      sui_text_get_size(dc, pdcfont, &psize, last, (utf8 *) chstr, wtxt->txtalign);
      if (wtxt->txtalign & SUAL_COLUMN)
        origin.y += psize.h;
      else
        origin.x += psize.w;
      chstr += sui_utf8_count_bytes(chstr, last);
      sui_set_widget_bgcolor(dc, widget, SUC_BACKGROUND);
      sui_set_widget_fgcolor(dc, widget, SUC_ITEM);
      sui_draw_text(dc, origin.x, origin.y, lstr, (utf8 *) chstr, wtxt->txtalign);
    }
  /* draw cursor */
    if (sui_is_flag(widget, SUTF_CURSOR | SUFL_FOCUSED, SUTF_CURSOR | SUFL_FOCUSED) ||
        sui_test_flag(widget, SUFL_EDITED)) {
      if (wtxt->txtalign & SUAL_COLUMN) {
        origin.y += tsize.y-1;
        tsize.y = /*origin.y +*/ (ts.x<10)?1:((ts.x<20)?2:4);
        tsize.x = /*origin.x +*/ ts.x;
      } else {
        origin.x += tsize.x-1;
        tsize.x = /*origin.x +*/ (ts.y<10)?1:((ts.y<20)?8:8);
        tsize.y = /*origin.y +*/ ts.y;
      }
      if (!(dc->dc_flags & SUDCF_BLINK_NOW)) {
//ul_logdeb("DrawCursor [%d,%d]-[%d,%d]\n",origin.x,origin.y,tsize.x, tsize.y);
        /*sui_draw_line_points(dc, &origin, &tsize);*/
        sui_draw_rect_points(dc, &origin, &tsize, 0);
      }
      tsize.x += origin.x;
      tsize.y += origin.y;

  /* draw mobilkey marks */
      if (sui_test_flag(widget, SUFL_EDITED) && sui_test_flag(widget, SUTF_MOBILKBD_MSK)) {
        sui_fontinfo_t finfo;
        sui_font_get_info(dc, pdcfont, &finfo);
  /* draw delete mark */
        if (sui_mk_in_repeat_mode()) {
          if (wtxt->txtalign & SUAL_COLUMN) {
            origin.x = tsize.x;
            origin.y = tsize.y - finfo.height;
          } else {
            origin.x -= finfo.maxwidth / 2;
            origin.y = tsize.y;
          }
          sui_draw_line_points(dc, &tsize, &origin);
        }
      }
    }
  }

  //if (focframe) sui_set_flag(widget, SUFL_HIGHLIGHTED); /* FIXME: this is only patch for fucusframed widgets - it needs system solution !!! */

  sui_utf8_dec_refcnt(str);
  sui_utf8_dec_refcnt(text);
  return 1;
}

/**
 * sui_wtext_hkey - Process events from keyboard
 * @widget: Pointer to a text widget.
 * @event: Pointer to an event structure.
 *
 * The function is response to keyboard event for text widget.
 *
 * Return Value: The function returns 0, because it doesn't process keyboard events.
 *
 * File: sui_wtext.c
 */
int sui_wtext_hkey(sui_widget_t *widget, sui_event_t *event)
{
  suiw_text_t *etxt = sui_txtwdg(widget);
  int newshift = 0, newbound = 0;
  int shift = (event->keydown.modifiers & MWKMOD_SHIFT)==MWKMOD_SHIFT;

  switch(event->keydown.keycode) {
  case MWKEY_DOWN:
    if (sui_test_flag(widget, SUFL_EDITAUTO)) { /* this is not 100% correctly - it is mainly for TECHNIC pump */
      if (!sui_test_flag(widget, SUFL_EDITEN))
        goto hkey_noprocesskey_end;
      if (!sui_is_flag(widget, SUTF_LINEMASK, SUTF_MULTILINE))
        goto hkey_noprocesskey_end;
      /* it can be used in a multiline text */
      break;
    }
  case MWKEY_RIGHT:
    if (sui_test_flag(widget, SUFL_EDITAUTO)) {
      if (sui_text_start_edit(widget, 0)<=0)
        goto hkey_noprocesskey_end;
    }
    if (!sui_test_flag(widget, SUFL_EDITED))
      goto hkey_noprocesskey_end;
    newshift = 1;
    break;
  case MWKEY_UP:
    if (sui_test_flag(widget, SUFL_EDITAUTO)) { /* this is not 100% correctly - it is mainly for TECHNIC pump */
      if (!sui_test_flag(widget, SUFL_EDITEN))
        goto hkey_noprocesskey_end;
      if (!sui_is_flag(widget, SUTF_LINEMASK, SUTF_MULTILINE))
        goto hkey_noprocesskey_end;
      /* it can be used in a multiline text */
      break;
    }
  case MWKEY_LEFT:
    if (sui_test_flag(widget, SUFL_EDITAUTO)) {
      if (sui_text_start_edit(widget, 0)<=0)
        goto hkey_noprocesskey_end;
    }
    if (!sui_test_flag(widget, SUFL_EDITED))
      goto hkey_noprocesskey_end;
    newshift = -1;
    break;
  case MWKEY_HOME:
    if (sui_test_flag(widget, SUFL_EDITAUTO)) {
      if (sui_text_start_edit(widget, 0)<=0)
        goto hkey_noprocesskey_end;
    }
    if (!sui_test_flag(widget, SUFL_EDITED))
      goto hkey_noprocesskey_end;
    newbound = -1;
    break;
  case MWKEY_END:
    if (sui_test_flag(widget, SUFL_EDITAUTO)) {
      if (sui_text_start_edit(widget, 0)<=0)
        goto hkey_noprocesskey_end;
    }
    if (!sui_test_flag(widget, SUFL_EDITED))
      goto hkey_noprocesskey_end;
    newbound = 1;
    break;
  }

  if (newshift || newbound) { /* no editing keys */
    utf8 *edtext = NULL;
    int len;
    if (sui_text_read_value(widget, &edtext, 1) != SUI_RET_OK) { /* widget is certainly in the edit mode */
      ul_logerr("wtext edited text hasn't been read\n");
      goto hkey_noprocesskey_end;
    }
    len=sui_utf8_length(edtext);
    if (newshift) {
      if (shift) {
        if (etxt->begsel<0) /* change etxt->begsel - it isn't set */
          etxt->begsel = etxt->cursor;
      } else
        etxt->begsel = -1;
      etxt->cursor += newshift;
      if (etxt->cursor > len) etxt->cursor = len;
      if (etxt->cursor < 0) etxt->cursor = 0;
    } else { /* HOME/END */
      if (shift) {
        if (etxt->begsel<0) /* change etxt->begsel - it isn't set */
          etxt->begsel = etxt->cursor;
      } else
        etxt->begsel = -1;
      if (newbound<0)
        etxt->cursor = 0;
      else
        etxt->cursor = len;
    }
    if (edtext) {
      sui_utf8_dec_refcnt(edtext);
    }
    goto hkey_keywasprocessed_end;
  }

//  if (!edited && !(event->keydown.keycode == MWKEY_ENTER))
//    goto hkey_noprocesskey_end;

/* mobil keyboard processing */
//  knownkey = 1;

  if (sui_test_flag(widget, SUTF_MOBILKBD_MSK)) {
    utf8char *tab;
    int retcode;

//ul_logdeb("WTEXT-K2MK: rc=%d,ch=%p mode=0x%04X\n", retcode, tab, etxt->mk_mode);
    retcode = sui_mk_translate_key2char(event->keydown.keycode, &etxt->mk_mode, &tab);
    if (retcode & SUI_MK_RET_OK) {
      utf8 *edtext = NULL;
      int emit = 0;
      if (sui_test_flag(widget, SUFL_EDITAUTO)) {
        if (sui_text_start_edit(widget, 1)<=0)
          goto hkey_noprocesskey_end;
      }
      if (!sui_test_flag(widget, SUFL_EDITED))
        goto hkey_noprocesskey_end;
      if (sui_text_read_value(widget, &edtext, 1) != SUI_RET_OK) { /* widget is certainly in the edit mode */
        ul_logerr("wtext edited text hasn't been read\n");
        goto hkey_noprocesskey_end;
      }
      /* remove last character */
      if (retcode & SUI_MK_RET_REMOVE) {
        etxt->cursor--;
        if (sui_utf8_delete(edtext, etxt->cursor)) etxt->cursor++;
        else
          emit = 1;
      }
      if (retcode & SUI_MK_RET_CHAR) {
      /* insert new character */
        if (!sui_utf8_insert_dynamic(edtext, etxt->cursor, tab)) {
          etxt->cursor++;
          emit = 1;
        }
      }
      if (emit) {
        sui_obj_emit((sui_obj_t *)widget, SUI_SIG_CHANGED, edtext);
      }
      if (edtext) {
        sui_utf8_dec_refcnt(edtext);
      }
      goto hkey_keywasprocessed_end;
    }
  }

  switch(event->keydown.keycode) {
    case MWKEY_DELETE:
    case MWKEY_BACKSPACE:
      if (sui_test_flag(widget, SUFL_EDITAUTO)) {
        if (sui_text_start_edit(widget, 1)<=0)
          goto hkey_noprocesskey_end;
      }
      if (!sui_test_flag(widget, SUFL_EDITED))
        goto hkey_noprocesskey_end;
      {
        utf8 *edtext = NULL;
        if (sui_text_read_value(widget, &edtext, 1) != SUI_RET_OK) { /* widget is certainly in the edit mode */
          ul_logerr("wtext edited text hasn't been read\n");
          goto hkey_noprocesskey_end;
        }
        if ((event->keydown.keycode==MWKEY_DELETE) || !etxt->cursor) {
          /* DELETE or if cursor is on beginning of text */
          if (etxt->cursor<sui_utf8_length(edtext)) {
            if (edtext) {
              sui_utf8_delete(edtext, etxt->cursor);
              sui_obj_emit((sui_obj_t *)widget, SUI_SIG_CHANGED, edtext);
            }
          }
        } else {
          etxt->cursor--;
          if (edtext) {
            if (sui_utf8_delete(edtext, etxt->cursor))
              etxt->cursor++;
            sui_obj_emit((sui_obj_t *)widget, SUI_SIG_CHANGED, edtext);
          }
        }
        if (edtext) {
          sui_utf8_dec_refcnt(edtext);
        }
      }
      break;

    case MWKEY_ENTER:
      if (!sui_test_flag(widget, SUFL_EDITED)) {
        if (!sui_test_flag(widget, SUFL_EDITEN))
          goto hkey_noprocesskey_end;
        else {
          if (sui_text_start_edit(widget, 0)<=0)
            goto hkey_noprocesskey_end;
        }
      } else {
        if (sui_text_stop_edit(widget, 1)<=0)
          goto hkey_noprocesskey_end;
      }
      break;

    case MWKEY_ESCAPE:
      if (!sui_test_flag(widget, SUFL_EDITED))
        goto hkey_noprocesskey_end;
      if (sui_text_stop_edit(widget, 0)<=0)
        goto hkey_noprocesskey_end;
      break;

    default:
      if ((event->keydown.keycode < ' ') || (event->keydown.keycode > 0xf000))
        goto hkey_noprocesskey_end;

      if (sui_test_flag(widget, SUFL_EDITAUTO)) {
        if (sui_text_start_edit(widget, 1)<=0)
          goto hkey_noprocesskey_end;
      }
      if (!sui_test_flag(widget, SUFL_EDITED))
        goto hkey_noprocesskey_end;
      {
        utf8 *edtext = NULL;
        if (sui_text_read_value(widget, &edtext, 1) != SUI_RET_OK) { /* widget is certainly in the edit mode */
          ul_logerr("wtext edited text hasn't been read\n");
          goto hkey_noprocesskey_end;
        }
        {
          char buf[6];
          sui_utf8_from_ucs(buf, event->keydown.keycode);
          if (!sui_utf8_insert_dynamic(edtext, etxt->cursor, (utf8char *) buf)) {
            etxt->cursor++;
            sui_obj_emit((sui_obj_t *)widget, SUI_SIG_CHANGED, edtext);
          }
        }
        if (edtext) {
          sui_utf8_dec_refcnt(edtext);
        }
      }
      break;
  }

/* end of function for unprocessed keys*/
hkey_keywasprocessed_end:
  sui_clear_event(widget, event);
  sui_draw_request(widget);
//  if (edtext) sui_utf8_dec_refcnt(edtext);
  return 1;

/* end of function for unprocessed keys*/
hkey_noprocesskey_end:
//  if (edtext) sui_utf8_dec_refcnt(edtext);
  return 0;
}


/******************************************************************************
 * Text event handler
 ******************************************************************************/
/**
 * sui_wtext_hevent - Process all text widget events
 * @widget: Pointer to a text widget.
 * @event: Pointer to an event.
 *
 * The function processes all events in the text widget or calls
 * the basic event handler if it cannot process the event.
 * The function (or the called basic handle-event function) clears event if the
 * event is processed.
 *
 * Return Value: The function does not return a value.
 *
 * File: sui_wtext.c
 */
void sui_wtext_hevent(sui_widget_t *widget, sui_event_t *event)
{
  suiw_text_t *wtxt = sui_txtwdg(widget);

//if (event->what == SUEV_KUP || event->what == SUEV_KDOWN)
//ul_logdeb("wtext(%p)-event(%d)\n",widget,event->what);

  switch(event->what) {
    case SUEV_DRAW:
    case SUEV_REDRAW:
      sui_widget_draw(event->draw.dc, widget, sui_wtext_draw);
      return;

    case SUEV_KDOWN:
      if (sui_wtext_hkey(widget, event)>0) {
        sui_clear_event(widget, event);
      }
      break;
    case SUEV_COMMAND:
      switch(event->message.command) {
        case SUCM_STARTEDIT:
          if (sui_text_start_edit(widget, event->message.info)>0) {
            sui_redraw_request(widget);
            sui_clear_event(widget, event);
          }
          break;
        case SUCM_CONFIRM:
          if (sui_text_stop_edit(widget, 1)>0) {
            sui_redraw_request(widget);
            sui_clear_event(widget, event);
          }
          break;
        case SUCM_DISCARD:
          if (sui_text_stop_edit(widget, 0)>0) {
            sui_redraw_request(widget);
            sui_clear_event(widget, event);
          }
          break;
        case SUCM_EDITBACKSPACE:
          {
            sui_event_t e;
            e.what = SUEV_KDOWN;
            e.keydown.keycode = MWKEY_BACKSPACE;
            if (sui_wtext_hkey(widget, &e)>0)
              sui_clear_event(widget, event);  /* event was changed to SUEV_KDOWN(MWKEY_BACKSPACE) !!! */
          }
          break;
        case SUCM_INIT:
          sui_widget_hevent(widget, event);
          sui_wtext_init(widget, (sui_dc_t *)event->message.ptr);
          sui_clear_event(widget, event);
          break;
        case SUCM_FOCUSSET:
          switch (sui_test_flag(widget, SUTF_MOBILKBD_MSK)) {
            case SUTF_MOBILKBD_C:
              wtxt->mk_mode = (wtxt->mk_mode & ~SUKM_MODEMASK) | SUKM_CAPITALS;
              break;
            case SUTF_MOBILKBD_S:
              wtxt->mk_mode = (wtxt->mk_mode & ~SUKM_MODEMASK) | SUKM_SMALL;
              break;
            case SUTF_MOBILKBD_N:
              wtxt->mk_mode = (wtxt->mk_mode & ~SUKM_MODEMASK) | SUKM_NUMBER;
              break;
          }
          if (sui_test_flag(widget, SUTF_MOBILKBD_MSK))
            sui_mk_clear_state();
          break;
        case SUCM_FOCUSREL:
          if (sui_test_flag(widget, SUFL_EDITED) &&
                !sui_test_flag(widget, SUTF_CONFIRMEDIT)) {
            sui_hevent_command(widget, SUCM_DISCARD, widget, 0);
          }
          break;
        case SUCM_AUTORESIZE:
          sui_wtext_auto_size((sui_dc_t *)event->message.ptr, widget);
          sui_clear_event(widget, event);
          break;
        case SUCM_GETSIZE:
          if (sui_wtext_get_size((sui_dc_t *)event->message.ptr,
                    widget, (sui_point_t *)event->message.info) > 0) {
            sui_clear_event(widget, event);
          }
          break;
        case SUCM_SETTEXT:
          if (sui_wtext_set_text(widget, event->message.ptr)>0) {
            sui_clear_event(widget, event);
          }
          break;
      }
  }
  if (event->what != SUEV_NOTHING)
    sui_widget_hevent(widget, event);
}


/******************************************************************************
 * Text widget vmt & signal-slot mechanism
 ******************************************************************************/
/**
 * sui_wtext_vmt_init - Initialize vmt text widget
 * @pw: Pointer to a common widget structure.
 * @params: Initial function's input arguments.
 *
 * The function updates the basic widget to the text widget and fills its structure fields.
 *
 * Return Value: The function return 0 as success and -1 as an error.
 *
 * File: sui_wtext.c
 */
int sui_wtext_vmt_init(sui_widget_t *pw, void *params)
{
  suiw_text_t *wtxt;
  pw->data = sui_malloc(sizeof(suiw_text_t));
  if(!pw->data) return -1;

  wtxt = pw->data;
  wtxt->begsel = -1;
  wtxt->cursor = 0;
  wtxt->mk_mode = SUKM_CAPITALS;

  pw->type = SUWT_TEXT;
  pw->hevent = sui_wtext_hevent;
  return 0;
}

/**
 * sui_wtext_vmt_done - Remove vmt text widget
 * @pw: Pointer to a text widget.
 *
 * The function removes the text widget extension from the basic widget and
 * releases each allocated memory for the text extension.
 *
 * Return Value: The function does not return any value.
 *
 * File: sui_wtext.c
 */
void sui_wtext_vmt_done(sui_widget_t *pw)
{
  if (pw->data) {
    suiw_text_t *wtxt = pw->data;
  
    if (wtxt->di) sui_dinfo_dec_refcnt(wtxt->di);
    wtxt->di = NULL;
    if (wtxt->format) sui_finfo_dec_refcnt(wtxt->format);
    wtxt->format = NULL;
    if (wtxt->edtext) sui_utf8_dec_refcnt(wtxt->edtext);
    wtxt->edtext = NULL;
    if (wtxt->head_lines) sui_wtext_destroy_lines(&wtxt->head_lines);
    wtxt->head_lines = NULL;

    free(wtxt);
    pw->data = NULL;
  }
  pw->type = SUWT_GENERIC;
  pw->hevent = sui_widget_hevent;
}

/******************************************************************************/
SUI_CANBE_CONST
sui_obj_signal_tinfo_t sui_wtext_signal_tinfo[] = {
  { "changed", SUI_SIG_CHANGED, &sui_args_tinfo_utf8},
  { "editstarted", SUI_SIG_EDIT_STARTED, &sui_args_tinfo_void},
  { "editstopped", SUI_SIG_EDIT_STOPPED, &sui_args_tinfo_void},
};

SUI_CANBE_CONST
sui_obj_slot_tinfo_t sui_wtext_slot_tinfo[] = {
  { "append", SUI_SLOT_APPEND, &sui_args_tinfo_utf8,
    .target_kind = SUI_STGT_METHOD_OFFSET,
    .target.method_offset=UL_OFFSETOF(sui_wtext_vmt_t, append)},
  { "clear", SUI_SLOT_CLEAR, &sui_args_tinfo_void,
    .target_kind = SUI_STGT_METHOD_OFFSET,
    .target.method_offset=UL_OFFSETOF(sui_wtext_vmt_t, clear)},
  { "settext", SUI_SLOT_SETTEXT, &sui_args_tinfo_utf8,
    .target_kind = SUI_STGT_METHOD_OFFSET,
    .target.method_offset=UL_OFFSETOF(sui_wtext_vmt_t, settext)},
};

SUI_CANBE_CONST
sui_obj_tinfo_t sui_wtext_vmt_obj_tinfo = {
  .name = "sui_wtext",
  .obj_size = sizeof(sui_widget_t),
  .obj_align = UL_ALIGNOF(sui_widget_t),
  .signal_count = MY_ARRAY_SIZE(sui_wtext_signal_tinfo),
  .signal_tinfo = UL_CAST_UNQ1(sui_obj_signal_tinfo_t *, sui_wtext_signal_tinfo),
  .slot_count = MY_ARRAY_SIZE(sui_wtext_slot_tinfo),
  .slot_tinfo = UL_CAST_UNQ1(sui_obj_slot_tinfo_t *, sui_wtext_slot_tinfo)
};

sui_wtext_vmt_t sui_wtext_vmt_data = {
  .vmt_size = sizeof(sui_wtext_vmt_t),
  .vmt_refcnt = SUI_STATIC,
  .vmt_parent = &sui_widget_vmt_data,
  .vmt_init = sui_wtext_vmt_init,
  .vmt_done = sui_wtext_vmt_done,
  .vmt_obj_tinfo = UL_CAST_UNQ1(sui_obj_tinfo_t *, &sui_wtext_vmt_obj_tinfo),

  .append = sui_wtext_append,
  .clear = sui_wtext_clear,
  .settext = sui_wtext_set_text,
};


/******************************************************************************/
/**
 * sui_wtext - Create a new text widget
 * @aplace: Pointer to a rectangle structure with the widget position and dimensions.
 * @aname: A UTF8 string with widget label.
 * @astyle: Pointer to a style structure which widget will use.
 * @aflags: Widgets' common and text special flags.
 * @adi: Pointer to a dinfo with a new text widget's content.
 * @aformat: Pointer to a requested format of a new text widget's content.
 * @aalign: Alignment of a new text widget's content.
 *
 * The function creates a new text widget and initializes it.
 *
 * Return Value: The function returns pointer to the created text widget or NULL.
 *
 * File: sui_wtext.c
 */
sui_widget_t *sui_wtext(sui_rect_t *aplace, utf8 *alabel, sui_style_t *astyle,
                        sui_flags_t aflags, sui_dinfo_t *adi,
                        sui_finfo_t *aformat, unsigned char aalign)
{
  sui_widget_t *pw = sui_widget_create_new(&sui_wtext_vmt_data, aplace,
                      alabel, astyle, aflags);
  if (!pw) return NULL;

  {
    suiw_text_t *wtxt = sui_txtwdg(pw);

    wtxt->txtalign = aalign;
    wtxt->begsel = -1;
    wtxt->cursor = 0;
    if (adi) {
      sui_dinfo_inc_refcnt(adi);
      wtxt->di = adi;
    }
    if (aformat) {
      sui_finfo_inc_refcnt(aformat);
      wtxt->format = aformat;
    }
    wtxt->mk_mode = SUKM_CAPITALS;
  }
  return pw;
}
