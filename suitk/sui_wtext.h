/* sui_wtext.h
 *
 * SUITK text widget. (Static text and edit box) with MOBIL keyboard support
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#ifndef _SUI_TEXT_WIDGET_
  #define _SUI_TEXT_WIDGET_

#include <suitk/suitk.h>


/**
 * struct sui_wtext_line - Description of each line for multiline text
 * @width: Width in pixels of the line's text.
 * @chars: Length of the line's text in characters.
 * @bytes: Length of the line's text in bytes.
 * @offset: Offset of the text from beginning (in bytes).
 *
 * File: sui_wtext.h
 */
typedef struct sui_wtext_line {
  int width;
  int chars;
  int bytes;
  int offset;
} sui_wtext_line_t;

/**
 * sui_wtext_block - Description of text block (list of lines)
 * @cnt: Number of lines described in this block.
 * @lines: Pointer to an array of lines in this block.
 * @next: Pointer to the next block description.
 *
 * File: sui_wtext.h
 */
typedef struct sui_wtext_block {
  int cnt;
  sui_wtext_line_t *lines;
  struct sui_wtext_block *next;
} sui_wtext_block_t;

/******************************************************************************
 * Text widget extension
 ******************************************************************************/
/**
 * struct suiw_text - Additional text widget structure
 * @di: Pointer to text DINFO.
 * @format: Pointer to text format.
 * @txtalign: Text's alignment to the widget.
 * @begsel: Index of the first selected character.
 * @cursor: Index of the current cursor's position (and the last selected character).
 * @edtext: Temporary saved text in edit mode.
 * @cntline: Number of lines for multiline text.
 * @maxwidth: Maximal text width (in pixels) from widths of all lines.
 * @lineheight: Height (in pixels) of one line.
 * @head_lines: Pointer to block of text lines descriptions.
 * @mk_mode: Selected keyboard mode - for mobil keyboard.
 *
 * File: sui_wtext.h
 */
typedef struct suiw_text {
  sui_dinfo_t       *di;
  sui_finfo_t       *format;
  sui_align_t        txtalign;  /* align of text */
  int                begsel;    /* idx of first selected char */
  int                cursor;    /* idx of the current position of cursor (and last selected char) */

  utf8              *edtext;    /* edited text */

  int                cntline;   /* multiline - number of lines */
  int                maxwidth;  /* maxwidth of text */
  int                lineheight;  /* height of one line */
  sui_wtext_block_t *head_lines;

  unsigned short     mk_mode; /* keybord mode for editing */
} suiw_text_t;

#define sui_txtwdg(wdg)  ((suiw_text_t *)wdg->data)

/**
 * enum text_flags - Text widget special flags ['SUTF_' prefix]
 * @SUTF_SINGLELINE: Widget's text is only in single-line.
 * @SUTF_MULTILINE: Widget's text is drawn as manual multi-line - new line is signed by TAB or NL character ('\t','\n').
 * @SUTF_AUTOSINGLE: Widget's text is drawn as auto single-line - ??? (Not specified and used yet).
 * @SUTF_AUTOMULTI: Widget's text is drawn as auto multi-line - text is splitted to many lines if it is longer than line on spaces.
 * @SUTF_LINEMASK: Mask for type of text drawing.
 * @SUTF_CURSOR: Cursor is shown in edit mode.
 * @SUTF_PASSWORD: Mask all chars with global password character ('*').
 * @SUTF_UPPERCASE: Draw all character as uppercase.
 * @SUTF_LOWERCASE: Draw all character as lowercase.
 * @SUTF_CONFIRMEDIT: Widget doesn't lose edited flag on SUEV_FOCUSREL with this flag.
 * @SUTF_ONLINEEDIT: Value will be editen only inside widget and dinfo will be updated after SUCM_CONFIRM.
 * @SUTF_MBKBDHELP: Mobil keyboard's help will be shown when the modil keyboard will be used.
 * @SUTF_MOBILKBD_C: Mobil keyboard's mode 'capital letters' is used.
 * @SUTF_MOBILKBD_S: Mobil keyboard's mode 'small letters' is used.
 * @SUTF_MOBILKBD_N: Mobil keyboard's mode 'numbers' is used.
 * @SUTF_MOBILKBD_MSK: Mask for mobil keyboard's mode.
 *
 * File: sui_wtext.h
 */
enum text_flags {
  SUTF_SINGLELINE  = 0x00000000, /* text widget is only single-line */
  SUTF_MULTILINE   = 0x00010000, /* text widget is manual multi-line - new line is signed by TAB character */
  SUTF_AUTOSINGLE  = 0x00020000, /* text widget is auto single-line - ??? */
  SUTF_AUTOMULTI   = 0x00030000, /* text widget is auto multi-line - text is splitted to many lines if it is longer than line on spaces */
  SUTF_LINEMASK    = 0x00030000, /* mask for type of text widget according to number of lines */

  SUTF_CURSOR      = 0x00040000, /* show cursor */
  SUTF_PASSWORD    = 0x00080000, /* mask all chars with password character */
  SUTF_UPPERCASE   = 0x00100000, /* draw all character as uppercase */
  SUTF_LOWERCASE   = 0x00200000, /* draw all character as lowercase */

  SUTF_CONFIRMEDIT = 0x02000000, /* widget doesn't lose edited flag on SUEV_FOCUSREL with this flag */
  SUTF_ONLINEEDIT  = 0x04000000, /* value will be editen only inside widget and dinfo will be updated after SUCM_CONFIRM */

  SUTF_MBKBDHELP   = 0x08000000,
  SUTF_MOBILKBD_C  = 0x10000000,
  SUTF_MOBILKBD_S  = 0x20000000,
  SUTF_MOBILKBD_N  = 0x30000000,
  SUTF_MOBILKBD_MSK= 0x30000000,
};


/******************************************************************************
 * Text slot functions
 ******************************************************************************/
int sui_wtext_append(sui_widget_t *widget, utf8 *str);
int sui_wtext_set_text(sui_widget_t *widget, utf8 *text);
int sui_wtext_clear(sui_widget_t *widget);

/******************************************************************************
 * Text widget vmt & signal-slot mechanism
 ******************************************************************************/
typedef sui_widget_t sui_wtext_t;

#define sui_wtext_vmt_fields(new_type, parent_type) \
   sui_widget_vmt_fields(new_type, parent_type) \
   int (*append)(parent_type##_t *self, utf8 *str); \
   int (*clear)(parent_type##_t *self); \
   int (*settext)(parent_type##_t *self, utf8 *str);

typedef struct sui_wtext_vmt {
   sui_wtext_vmt_fields(sui_wtext, sui_widget)
} sui_wtext_vmt_t;

static inline sui_wtext_vmt_t *
sui_wtext_vmt(sui_widget_t *self)
{
   return (sui_wtext_vmt_t*)(self->base.vmt);
}

extern sui_wtext_vmt_t sui_wtext_vmt_data;

/******************************************************************************
 * Text widget basic functions
 ******************************************************************************/
sui_widget_t *sui_wtext(sui_rect_t *aplace, utf8 *alabel, sui_style_t *astyle,
                        sui_flags_t aflags, sui_dinfo_t *adi,
                        sui_finfo_t *aformat, unsigned char aalign);

#endif
