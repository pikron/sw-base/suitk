/* sui_wbitmap.h
 *
 * SUITK bitmap widget.
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#ifndef _SUI_BITMAP_WIDGET_
#define _SUI_BITMAP_WIDGET_

#include "suitk.h"


/******************************************************************************
 * Bitmap widget extension
 ******************************************************************************/
/**
 * struct suiw_bitmap - Additional bitmap widget structure
 * @imnum: pointer to dinfo with index of clip in multi-image
 * @image: Image data structure.
 * @mask: Mask data structure.
 * File: sui_bitmap.h 
 */
typedef struct suiw_bitmap {
  sui_dinfo_t *imidx;
  sui_image_t *image;
  sui_image_t *mask;
} suiw_bitmap_t;
  
#define sui_bmpwdg( wdg)  ((suiw_bitmap_t *)wdg->data)
  
/**
 * enum bitmap_flags - Bitmap widget special flags ['SUIF_' prefix]
 * @SUIF_TYPE_8BIT: One icon pixel is in byte.
 * @SUIF_TYPE_1BIT: One icon pixel is in bit. Lines are aligned to byte.
 * @SUIF_TYPEMASK: Mask for testing icon type (8bit,1bit,...).
 * @SUIF_MASKED: Icon data are masked by mask data
 * @SUIF_HOR_REVERSE: Icon is drawn with horizontal mirror.
 * @SUIF_VER_REVERSE: Icon is drawn with vertical mirror.
 * @SUIF_INV_MASK: Mask is inverted.
 * @SUIF_INV_DATA: Icon is inverted.
 * @SUIF_HIDE_INVISIBLE: Redraw icon by rectangle with background color, if flag SUFL_VISIBLE is cleared.
 * File: sui_bitmap.h
 */
enum bitmap_flags {   
  SUIF_MASKED      = 0x00010000,  /* using mask(binary).but in same format as data (1/8bit) 1-use data,0-use bg/transp */
  SUIF_INV_MASK    = 0x00020000,
  SUIF_INV_IMAGE   = 0x00040000,
  SUIF_MIRROR      = 0x00080000,
  SUIF_ROTATE_0    = 0x00000000,
  SUIF_ROTATE_90   = 0x00100000,
  SUIF_ROTATE_180  = 0x00200000,
  SUIF_ROTATE_270  = 0x00300000,

  SUIF_ROTATE_MASK = 0x00300000,
  //    SUIF_HIDE_INVISIBLE     = 0x01000000, /* redraw icon by background color, if flag SUFL_VISIBLE is cleared (only one draw) */
  SUIF_TRANSPARENT = 0x00400000, // masked icon has transparent background - is same as !SUFL_BACKGROUND */
  SUIF_MULTIIMAGE  = 0x00800000, // bitmap image (and mask) is multi-clip (images with the same dimensions under itself in one image)
};


/******************************************************************************
 * Bitmap slot functions
 ******************************************************************************/
//int sui_wbutton_toggle( sui_widget_t *widget);


/******************************************************************************
 * Basic widget assignment function
 ******************************************************************************/
int sui_wbitmap_assign_image( sui_widget_t *widget, sui_image_t *newimage);
int sui_wbitmap_assign_mask( sui_widget_t *widget, sui_image_t *newmask);


/******************************************************************************
 * Bitmap widget vmt & signal-slot mechanism
 ******************************************************************************/
typedef sui_widget_t sui_wbitmap_t;

#define sui_wbitmap_vmt_fields(new_type, parent_type) \
	sui_widget_vmt_fields( new_type, parent_type)
//	int (*toggle)(parent_type##_t *self);
// functions have sui_widget_t* as first argument

typedef struct sui_wbitmap_vmt {
	sui_wbitmap_vmt_fields(sui_wbitmap, sui_widget)
} sui_wbitmap_vmt_t;

static inline sui_wbitmap_vmt_t *
sui_wbitmap_vmt(sui_widget_t *self)
{
	return (sui_wbitmap_vmt_t*)(self->base.vmt);
}

extern sui_wbitmap_vmt_t sui_wbitmap_vmt_data;

/******************************************************************************
 * Bitmap widget basic functions
 ******************************************************************************/

sui_widget_t *sui_wbitmap( sui_rect_t *aplace, utf8 *alabel, sui_style_t *astyle,
              unsigned long aflags, sui_image_t *aimage, sui_image_t *amask);

#endif
