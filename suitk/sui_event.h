/* sui_event.h
 *
 * SUITK event system
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#ifndef _SUI_EVENT_H_
#define _SUI_EVENT_H_

#include <suitk/sui_comdefs.h>
#include <suiut/sui_types.h>
#include <suiut/sui_utf8.h>

#ifdef __cplusplus
extern "C" {
#endif

/* event struct */
//  struct sui_event;

//  extern struct sui_event sui_event_buf;
//  extern struct sui_event sui_global_event_buf;
//  extern int sui_check_kbd;
//  extern int sui_check_mouse;
//  extern short draw_request;
//  extern unsigned short sui_last_mobkey;

/*********************************************************/
/* Event types and structs                               */
/*********************************************************/
/**
 * struct sui_event - Common suitk event structure
 * @what: Code of event.(See 'event_code' enum with 'SUEV_' prefix)
 * @buttons: Mouse flags of pressed buttons in mouse events.
 * @dblclk: Mouse button was double clicked.
 * @x: Mouse horizontal position in mouse events. 
 * @y: Mouse vertical position in mouse events.
 * @keycode: Key code in keyboard events.
 * @modifiers: Flags of keyboard modifiers (SHIFT,CTRL,ALT,...) in keyboard events.
 * @scancode: Scan code of key in keybord events. 
 *
 * File: sui_event.h
 */
  typedef struct sui_event {
    unsigned short what;
    unsigned short broad;
    union {
      struct { 
        short buttons; 
        short dblclk; 
        int x; 
        int y; 
      } mouse;
      struct { 
        unsigned short keycode;
        unsigned short modifiers; /* key modifiers mask */
        unsigned short scancode;
        unsigned short repeats;
      } keydown;
      struct { 
        short command;
        void *ptr;
        long info;
      } message;
      struct {
       #if 1
        struct sui_dc *dc;
       #else
        void *dc;
       #endif
      } draw;
    };
  } sui_event_t;


/**
 * struct sui_event_global_var - Information about Suitk mouse and keyboard event sources.
 * @sui_mouse_fd:	File descriptor of mouse driver.
 * @sui_kbd_fd:		File descriptor of keyboard driver.
 * @sui_check_kbd:	Keyboard event source should be checked.
 * @sui_check_mouse:	Mouse event source should be checked.
 *
 * The information to the structure is filled by sui_environment_open()
 * function and it controls event sources in sui_get_event() function.
 *
 * File: sui_event.h
 */
typedef struct sui_event_global_var {
	int             sui_mouse_fd;
	int             sui_kbd_fd;
	int             sui_check_kbd;
	int             sui_check_mouse;
} sui_event_global_var_t;

typedef struct sui_event_buffers {
	struct sui_event sui_event_buf;
	struct sui_event sui_global_event_buf;
	short draw_request;
	short draw_request_deffer;
//	unsigned short sui_last_key;
} sui_event_buffers_t;


extern sui_event_global_var_t sui_globev;
extern sui_event_buffers_t *curevbufs;
extern struct sui_dc *sui_globdc;

  
/* dodefinovani modifikatoru klavesy - opakovane stisknuti - napr. pro mobilovou klavesnici
 */
 #define MWKMOD_REPEATED 0x0004 // klavesa byla stisknuta opakovane
 #define MWKMOD_N_REPEAT 0x0008 // klavesa byla stisknuta n-krat, kde n je urceno symbolem SUI_COUNT_OF_KEY_REPEAT
 
 #define SUI_COUNT_OF_KEY_REPEAT 6 // pouzito pro widget number - zvetseni kroku pro sipky
 
  
/**
 * enum event_code - Code of SUITK events ['SUEV_' prefix]
 * @SUEV_MDOWN: Mouse button is down.
 * @SUEV_MUP: Mouse button is up.
 * @SUEV_MMOVE: Mouse is in move.
 * @SUEV_MAUTO: 
 * @SUEV_KDOWN: Key is down.
 * @SUEV_KUP: Key is up.
 * @SUEV_DRAW: Draw widget.
 * @SUEV_REDRAW: Redraw widget.
 * @SUEV_COMMAND: Command event.
 * @SUEV_BROADCAST: Broadcast event.
 * @SUEV_SIGNAL: ?
 * @SUEV_GLOBAL: ?
 * @SUEV_FREE: ?
 * @SUEV_NOTHING: ?
 * @SUEV_MOUSE: ?
 * @SUEV_KEYBOARD: ?
 * @SUEV_MESSAGE: ?
 * @SUEV_DEFMASK: ?
 * @SUEV_GRPMASK: ?
 * File: sui_base.h
 */
  enum event_code {
    /* Event codes */
    SUEV_MDOWN     = 0x0001, /* mouse */
    SUEV_MUP       = 0x0002, /* mouse */
    SUEV_MMOVE     = 0x0004, /* mouse */
    SUEV_MAUTO     = 0x0008, /* mouse */
    SUEV_KDOWN     = 0x0010, /* keyboard */
    SUEV_KUP       = 0x0020, /* keyboard */
    SUEV_DRAW      = 0x0040,
    SUEV_REDRAW    = 0x0080,
    SUEV_COMMAND   = 0x0100,
    SUEV_BROADCAST = 0x0200,
    SUEV_SIGNAL    = 0x0400,
    SUEV_GLOBAL    = 0x0800,  
    SUEV_FREE      = 0x8000,
    SUEV_NOTHING   = 0x0000,
    /* Event masks */
    SUEV_MOUSE     = 0x000f,
    SUEV_KEYBOARD  = 0x0030,
    SUEV_MESSAGE   = 0x7FC0,
    SUEV_DEFMASK   = SUEV_MDOWN|SUEV_DRAW|SUEV_KDOWN|SUEV_REDRAW
                     |SUEV_COMMAND|SUEV_SIGNAL|SUEV_BROADCAST|SUEV_FREE,
    SUEV_GRPMASK   = SUEV_DEFMASK|SUEV_MESSAGE|SUEV_MOUSE|SUEV_KEYBOARD
  };
  
/**
 * enum command_event - Command codes for command event ['SUCM_' prefix]
 * @SUCM_VALID: VALID command event.
 * @SUCM_QUIT: QUIT command event.
 * @SUCM_ERROR: ERROR command event.
 * @SUCM_MENU: MENU command event. Open, select, close, ... menu.
 * @SUCM_CLOSE: CLOSE command event.
 * @SUCM_ZOOM: ZOOM command event.
 * @SUCM_RESIZE: RESIZE command event.
 * @SUCM_NEXT: NEXT command event. Mainly for change widget focus by pressing TAB key.
 * @SUCM_PREV: PREV command event. Mainly for change widget focus by pressing SHIFT+TAB key.
 * @SUCM_HELP: HELP command event.
 * @SUCM_OK: OK button pressed.
 * @SUCM_CANCEL: CANCEL button pressed.
 * @SUCM_YES: YES button pressed.
 * @SUCM_NO: NO button pressed.
 * @SUCM_DEFAULT: DEFAULT button pressed.
 * @SUCM_FOCUSASK: Which widget has focus ?
 * @SUCM_FOCUSSET: Set focus to the widget.
 * @SUCM_FOCUSREL: Release focus from the widget.
 * @SUCM_INIT: Initialize widget.
 * @SUCM_DONE: Done widget - decrement reference counter, deallocate widget data.
 * @SUCM_NEWDISPLAY: Create new screen from pointer to screen.
 * @SUCM_DISPNUMB: Create new screen from number to screen.
 * !@SUCM_CHANGE_STBAR: Status bar is changed.
 * @SUCM_NEXT_GROUP: Change focus between groups (like as ALT+TAB in Windows).
 * @SUCM_PREV_GROUP: Change focus between groups.
 * @SUCM_NOTIFY: Forward event to parent.
 *
 * File: sui_base.h
 */  
enum command_event {
  /* Standard command codes */
  SUCM_VALID         = 0,
  SUCM_QUIT          = 1,
  SUCM_ERROR         = 2,
  SUCM_MENU          = 3,
  SUCM_CLOSE         = 4,
  SUCM_ZOOM          = 5,
  SUCM_RESIZE        = 6,
  SUCM_NEXT          = 7,
  SUCM_PREV          = 8,
  SUCM_HELP          = 9,
  SUCM_OK            = 10,
  SUCM_CANCEL        = 11,
  SUCM_YES           = 12,
  SUCM_NO            = 13,
  SUCM_DEFAULT       = 14,

  SUCM_FOCUSASK      = 15,
  SUCM_FOCUSSET      = 16,
  SUCM_FOCUSREL      = 17,
  SUCM_INIT          = 18,	/* ptr=dc */
  SUCM_DONE          = 19,

  SUCM_GETSIZE       = 20,    /* ptr=pointer to dc, info=pointer to output point(x,y) structure */
  //    SUCM_NEWDISPLAY    = 20,	/* ptr=new display */
  //    SUCM_DISPNUMB      = 21,	/* info=number of new display */
  //SUCM_CHANGE_STBAR  = 22,

  SUCM_NEXT_GROUP    = 23, /* added for change focus between groups */
  SUCM_PREV_GROUP    = 24,

  SUCM_EVC_LINK_TO   = 25,    /* ptr=evc_link, info=dst */

  SUCM_AUTORESIZE    = 26, /* added for autoresize widget according to content */

  SUCM_CHANGE        = 30, /* widget content is changing */
  SUCM_CHANGED       = 31, /* widget content was changed and confirmed */
  SUCM_CONFIRM       = 32, /* edited content was confirmed */
  SUCM_DISCARD       = 33, /* edited content was discarded */
  SUCM_STARTEDIT     = 34, /* start content editing , info!=0 => clear before edit*/

  SUCM_EDITMAX       = 35,
  SUCM_EDITMIN       = 36,
  SUCM_EDITFULL      = 37,
  SUCM_EDITEMPTY     = 38,

  SUCM_ERR_WRVAL     = 39,    /* value wasn't write into dinfo - error */

  SUCM_GETSTATE      = 40,    /* widget(button) return state(check) in info = 0/1 */
  SUCM_SETSTATE      = 41,    /* widget(button) sets its own state(check) by info */
  SUCM_PUSHED        = 42,    /* widget(button) sends this message whet it's pushed */

  SUCM_DISTRIBUTE    = 43,    /* message for widget(group) to distribute to all chilren, distribute_fnc is in ptr and event is in info */

  SUCM_MOVEMIN       = 44,    /* move in list, scroll, .. wasn't perform because pointer is on lower boundary */
  SUCM_MOVEMAX       = 45,    /* move in list, scroll, .. wasn't perform because pointer is on upper boundary */

  SUCM_EDITBACKSPACE = 50,
  SUCM_TEXTTIME      = 51,

  SUCM_NOTIFY        = 52, /* added for notification parent - ptr: pointer of child widget, info: child widget id */

  SUCM_SETTEXT       = 53,
  SUCM_CLEARTEXT     = 54,

  SUCM_DEADKEY       = 55, /* SUEV_KDOWN event was cleared (key lock is enabled or another reason) - in msg.info is keycode */

  SUCM_EXTINPUT      = 56, /* event from external input - in the event.message.info is coded index of external input (0x0001-0x7fff) and state if the input (mask=0x8000; 0x8000-down, 0x0000-up) */
};

enum global_event {
    SUGM_DEFOCUS       = 0,
    SUGM_CHANGE_SCREEN, // = 2,
    SUGM_BLINKING, //      = 4,
    SUGM_CHANGE_FOCUS, //  = 8,
		SUGM_NOTIFY, //        = 16,
};

enum broad_event_flag {
	SUEB_SPECIFIC = 0,
	SUEB_ALL = 1,
	SUEB_FOCUSED_FROM_ROOT = 2,
	SUEB_FOCUSED_TO_ROOT = 3,
};

enum sui_signal_event {
	SUSIG_DINFO_CHANGING= 1,    /* ptr=dinfo */
	SUSIG_DINFO_CHANGED = 2,    /* ptr=dinfo */
};


#define is_init_event(e) (((e)->what==SUEV_COMMAND)&&((e)->message.command==SUCM_INIT))

/*********************************************************/
/* event functions                                       */
/*********************************************************/
int sui_put_event( sui_event_t *event);
int sui_put_global_event( sui_event_t *event);
int sui_flush_event( void);
int sui_flush_global_event( void);
int sui_event_draw_deffer_inc(void);
int sui_event_draw_deffer_dec(void);
int sui_event_draw_deffer_clear(void);

static inline void sui_put_command_event( short cmd, void *ptr, long info)
{
	sui_event_t ev;
	ev.what = SUEV_COMMAND;
	ev.message.command = cmd;
	ev.message.ptr = ptr;
	ev.message.info = info;
	sui_put_event( &ev);
}

int sui_get_event( sui_event_t *event, long timeout);

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /* _SUI_EVENT_H_ */
