/* sui_wtable.c
 *
 * SUITK table widget.
 *
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#include "sui_wtable.h"

#include <ul_log.h>

UL_LOG_CUST(ulogd_suitk)

/******************************************************************************
 * Functions with table column
 ******************************************************************************/
// sui_table_column_create
// sui_table_column_destroy
// sui_table_inc_refnct
// sui_table_dec_refcnt
// sui_table_create
// sui_table_clear
// sui_table_add_column
// sui_table_del_column

/**
 * sui_table_column_create - creates dynamic table column structure
 * @atitle: utf8 string - column title (can be NULL)
 * @aformat: pointer to format structure
 * @adata: pointer to dinfo
 * @alist: pointer to associated list
 * @afont: font used for writing data in entire column (no title) (can be NULL)
 * @aalign: alignment of data in column
 * @awidth: width of column (in pixels) or -1
 * The function creates a new dynamic column structure in memory and fills up it.
 * Return Value: The function returns pointer to a new column or NULL.
 */
sui_table_column_t *sui_table_column_create(utf8 *atitle, sui_finfo_t *aformat,
                                    sui_dinfo_t *aspan, sui_dinfo_t *adata, sui_list_t *alist,
                                    sui_font_info_t *afont, sui_align_t aalign,
                                    sui_coordinate_t awidth, sui_coordinate_t aheight)
{
  sui_table_column_t *ret = sui_malloc(sizeof(sui_table_column_t));
  if (ret) {
    sui_utf8_inc_refcnt(atitle);
    ret->title = atitle;
    sui_finfo_inc_refcnt(aformat);
    ret->format = aformat;
    sui_dinfo_inc_refcnt(aspan);
    ret->data = aspan;
    sui_dinfo_inc_refcnt(adata);
    ret->data = adata;
    sui_list_inc_refcnt(alist);
    ret->list = alist;
    sui_fonti_inc_refcnt(afont);
    ret->font = afont;
    ret->align = aalign;
    ret->width = awidth;
    ret->height = aheight;
  }
  return ret;
}

/**
 * sui_table_column_create_copy - creates table column from an existing column
 * @fromcol: pointer to source table column
 * The function creates a new dynamic column structure in memory and fills up it.
 * Return Value: The function returns pointer to a new column or NULL.
 */
sui_table_column_t *sui_table_column_create_copy(sui_table_column_t *fromcol)
{
  sui_table_column_t *ret = sui_malloc(sizeof(sui_table_column_t));
  if (ret) {
    if (fromcol) {
      *ret = *fromcol;
      sui_utf8_inc_refcnt(ret->title);
      sui_finfo_inc_refcnt(ret->format);
      sui_dinfo_inc_refcnt(ret->span);
      sui_dinfo_inc_refcnt(ret->data);
      sui_list_inc_refcnt(ret->list);
      sui_fonti_inc_refcnt(ret->font);
    }
  }
  return ret;
}

/**
 * sui_table_column_done - releases all fields of structure, but no structure itself
 * @tcol: pointer to column structure
 */
void sui_table_column_done(sui_table_column_t *tcol)
{
  if (tcol->title) {
    sui_utf8_dec_refcnt(tcol->title);
    tcol->title = NULL;
  }
  if (tcol->format) {
    sui_finfo_dec_refcnt(tcol->format);
    tcol->format = NULL;
  }
  if (tcol->span) {
    sui_dinfo_dec_refcnt(tcol->span);
    tcol->span = NULL;
  }
  if (tcol->data) {
    sui_dinfo_dec_refcnt(tcol->data);
    tcol->data = NULL;
  }
  if (tcol->list) {
    sui_list_dec_refcnt(tcol->list);
    tcol->list = NULL;
  }
  if (tcol->font) {
    sui_fonti_dec_refcnt(tcol->font);
    tcol->font = NULL;
  }
  return;
}

/**
 * sui_table_column_destroy - destroys dynamic column structure
 * @tcol: pointer to column structure
 * Return Value: The function returns zero if the column structure was destroyed
 * and negativ value if any error occurs.
 */
int sui_table_column_destroy(sui_table_column_t *tcol)
{
  if (!tcol) return -1;
  sui_table_column_done( tcol);
  free(tcol);
  return 0;
}

/**
 * sui_table_inc_refcnt - increments table reference counter
 * @tab: pointer to table structure
 * Return Value: new reference counter of the table
 */
sui_refcnt_t sui_table_inc_refcnt(sui_table_t *tab)
{
  if (!tab) return SUI_REFCNTERR;
  if(tab->refcnt>=0) tab->refcnt++;
  return tab->refcnt;
}

/**
 * sui_table_dec_refcnt - decrements table reference counter
 * @tab: pointer to table structure
 * Return Value: new reference counter of the table
 */
sui_refcnt_t sui_table_dec_refcnt(sui_table_t *tab)
{
  sui_refcnt_t ref;
  if (!tab) return SUI_REFCNTERR;
  if(tab->refcnt>0) tab->refcnt--;
  if(!(ref=tab->refcnt)) {
    sui_table_clear(tab);
    sui_obj_done_vmt((sui_obj_t *)tab,
      UL_CAST_UNQ1(sui_obj_vmt_t *, &sui_table_vmt_data));
    free(tab);
  }
  return ref;
}

/**
 * sui_table_create - creates table structure
 * Return Value: The function returns pointer to created table or NULL
 */
sui_table_t *sui_table_create(void)
{
  sui_table_t *tab = sui_malloc(sizeof(sui_table_t));
  if (tab) {
    sui_table_inc_refcnt(tab);
    // init ul_list of columns
  }
  return tab;
}

/**
 * sui_table_clear - clears table structure
 * @tab: pointer to table structure
 */
void sui_table_clear(sui_table_t *tab)
{
  sui_table_column_t *tcol;
  while (tab->firstcol) {
    tcol = tab->firstcol->next;
    sui_table_column_destroy(tab->firstcol);
    tab->firstcol = tcol;
  }
  tab->firstcol = NULL;
  tab->maxidx = 0;
  tab->colnum = 0;
}

/**
 * sui_table_add_column - adds table column to table
 * @tab: pointer to table
 * @column: pointer to new table column
 * @idx: zero based position where the new column will be added (or -1 to add column to end of list)
 * Return Value: return code - 0=success, a negative value=error
 */
int sui_table_add_column(sui_table_t *tab, sui_table_column_t *column, sui_listcoord_t idx)
{
  sui_table_column_t *tcol, *newcol;
  if (!tab || !column) return -1;
  if (idx < 0) idx = tab->colnum;
  newcol = sui_table_column_create_copy(column);
  if (!newcol) return -1;
  if (tab->firstcol) {
    tcol = tab->firstcol;
    while (tcol->next && idx>0) {
      tcol = tcol->next;
      idx--;
    }
    newcol->next = tcol->next;
    tcol->next = newcol;
  } else {
    tab->firstcol = newcol;
  }
  tab->colnum++;
  if (tab->colnum==1)
    tab->maxidx = 0;
  if (newcol->data) {
    if (newcol->data->idxsize > tab->maxidx)
      tab->maxidx = newcol->data->idxsize;
  }
  return 0;
}

/**
 * sui_table_del_column - removes table column from table
 * @tab: pointer to table
 * @idx: zero based position of column in table (-1 remove first column)
 * Return Value: return code - 0=success, a negative value=error
 */
int sui_table_del_column( sui_table_t *tab, sui_listcoord_t idx)
{
  sui_table_column_t *tcol, *rem;
  if ( !tab || !tab->firstcol) return -1;
  if ( idx < 0) idx = 0;
  if ( idx) {
    tcol = tab->firstcol;
    while( idx && tcol->next) {
      tcol = tcol->next;
      idx--;
    }
    rem = tcol->next;
    if ( !rem) return -1;
    tcol->next = rem->next;
    tab->colnum--;
  } else {
    rem = tab->firstcol;
    tab->firstcol = rem->next;
  }
  return sui_table_column_destroy( rem);
}

/**
 * sui_table_column_item_get_text - get content of table column field (by column and row index)
 */
int sui_table_column_item_get_text(sui_table_column_t *column,
                                   sui_listcoord_t row, utf8 **buf)
{
  int ret = -1;
  if (!column) return ret;
  if (row >= 0) {
    if (!column->data && !column->list) return ret;
    if (column->list) {
      sui_listcoord_t listindex;
      sui_list_item_t *litem;
      if (column->data) {
        long rowindex = 0;
        ret = sui_rd_long(column->data, row, &rowindex);
        if (ret!=SUI_RET_OK) return -1;
        listindex = rowindex;
      } else {
        listindex = row;
      }
//ul_logdeb("Read Table-ListIndex=%d",listindex);
      litem = sui_list_gavl_find(column->list, &listindex);
      ret = sui_list_item_get_text_with_index(litem, row, buf);
    } else {
      utf8 *txt = NULL;
      txt = sui_dinfo_2_utf8(column->data, row, column->format, SUTR_CHECKLIMITS);
      if (txt) {
        *buf = txt;
        ret=0;
      }
    }
  } else {
    if (column->title) {
      sui_utf8_inc_refcnt(column->title);
      *buf = column->title;
      ret = 0;
    }
  }
  return ret;
}

/**
 * sui_table_item_get_text - get content of table field (by table, column index and row index)
 */
int sui_table_item_get_text(sui_table_t *tab, sui_listcoord_t colidx,
                                 sui_listcoord_t rowidx, utf8 **buf)
{
  int ret = -1;
  sui_table_column_t *col;
  if (!tab) return ret;
  if (colidx >= 0) {
    col = tab->firstcol;
    while(colidx && col) {
      col = col->next;
      colidx--;
    }
    if (col) {
      ret = sui_table_column_item_get_text(col, rowidx, buf);
    }
  }
  return ret;
}

/******************************************************************************
 * Table widget slot functions
 ******************************************************************************/
int sui_wtable_update_maxidx(sui_widget_t *widget)
{
  suiw_table_t *wtab;
  int i=0;

  if (!widget) return -1;
  wtab = sui_tabwdg(widget);
  if (!wtab || !wtab->tdata) return -1;
  sui_table_column_t *tcol = wtab->tdata->firstcol;
  wtab->tdata->maxidx = 0;
  while (tcol && i<wtab->cols) {
    if (tcol->data && tcol->data->idxsize > wtab->tdata->maxidx)
      wtab->tdata->maxidx = tcol->data->idxsize;
    i++;
    tcol = tcol->next;
  }
//ul_logmsg("wtab upd idx=%d\n",wtab->tdata->maxidx);
  return 0;
}

/******************************************************************************
 * Table basic functions
 ******************************************************************************/
/*
 * sui_wtable_get_hrow - function counts row height from all columns
 */
int sui_wtable_get_hrow(suiw_table_t *wtab)
{
  sui_coordinate_t hrow = 0;
  sui_table_column_t *tcol = wtab->tdata->firstcol;
  int i=0;
  while (tcol && i<wtab->cols) {
    if (tcol->height > hrow) hrow=tcol->height;
    tcol = tcol->next;
    i++;
  }
  if (hrow>0) {
    wtab->hrow = hrow;
    return 0;
  } else
    return -1;
}

/**
 * sui_wtable_get_size - return table content size
 * @dc: pointer to device context
 * @widget: pointer to list widget
 * @point: pointer to point structure
 */
//inline
int sui_wtable_get_size(sui_dc_t *dc, sui_widget_t *widget, sui_point_t *size)
{
  suiw_table_t *wtab;
  sui_table_column_t *tcol;
  int i,j, fullx = 0;

  if (!widget || !size) return -1;
  wtab = sui_tabwdg(widget);
  size->x = 0;
  i = 0;
  if (wtab->tdata) { /* sum width of columns */
    tcol = wtab->tdata->firstcol;
    while (tcol && i<wtab->cols) {
      if (tcol->width<0)
        fullx = 1;
      size->x += tcol->width;
      tcol = tcol->next;
      i++;
    }
  }
/* y */
  j = wtab->rows;
  size->y = j * ((wtab->hrow>0)?wtab->hrow:wtab->htitle);
  if (sui_test_flag(widget,SUAF_TITLES)) {
    size->y += wtab->htitle+1;
    j++;
  }
  if (sui_test_flag(widget, SUAF_DIV_ROWS) && j>0)
    size->y += j-1;
/* x */
  if (sui_test_flag(widget, SUAF_DIV_COLS) && i>0) {
    size->x += i - 1;
  }
  if (fullx) size->x = widget->place.w; /* FIXME: correct computation width of all columns if one has set width to -1 */
  return 1;
}

/**
 * sui_wtable_get_field_size - get size of table field
 * @dc: pointer to device context
 * @wdg: pointer to widget
 * @col: pointer to table column structure
 * @idx: index of table row (-1 is title)
 * @size: pointer to output buffer
 */
int sui_wtable_get_field_size(sui_dc_t *dc, sui_widget_t *wdg, sui_table_column_t *col,
                               sui_listcoord_t idx, sui_textdims_t *size)
{
  utf8 *txt = NULL;
  sui_font_info_t *fnt;
  sui_dcfont_t *pdcf;

  if (!col || !size) return -1;
  if (col->font) fnt = col->font;
  else fnt = wdg->style->fonti;
  pdcf = sui_font_prepare(dc, fnt->font, fnt->size);

  if (!sui_table_column_item_get_text(col, idx, &txt)) {
    sui_text_get_size(dc, pdcf, size, -1, txt, col->align);
    sui_utf8_dec_refcnt(txt);
    return 0;
  }
  return -1;
}

/**
 * sui_wtable_init - Reaction to INIT event in table widget
 * @widget: Pointer to table widget.
 * @event: Pointer to INIT event.
 *
 * The function initializes the table widget.
 *
 * Return Value: The function does not return a value.
 * File: sui_wtable.c
 */
//inline
void sui_wtable_init(sui_dc_t *dc, sui_widget_t *widget)
{
  suiw_table_t *wtab;
//  sui_coordinate_t wl=0, hl=0;
  sui_textdims_t titlesize;
  sui_table_column_t *tcol;
  int i = 0;

  sui_font_info_t *fnt;
  sui_dcfont_t *pdcf;

  wtab = sui_tabwdg(widget);

  wtab->frow = 0; /* index of first row in table */
  wtab->fcol = 0; /* index of first col in table */

/* fill all dimensions if they are set to -1 */
  fnt = widget->style->fonti;
  pdcf = sui_font_prepare(dc, fnt->font, fnt->size);
  if (wtab->tdata) {
    int numofneg = 0;
    sui_coordinate_t sumw = 0;
    tcol = wtab->tdata->firstcol;
    while(i<wtab->cols && tcol) {
      if (tcol->title) {
        sui_text_get_size(dc, pdcf, &titlesize, -1, tcol->title, tcol->align);
        if (widget->place.w<0) {
          if (tcol->width < 0) tcol->width = titlesize.w;
        } else {
          if (tcol->width < 0) numofneg++;
          else sumw += tcol->width;
        }
        if (wtab->htitle < 0) wtab->htitle = titlesize.h;
      }
      i++;
      tcol = tcol->next;
    }
/* added feature - column can have got unknown width, which will be computed from widget.width */
    if (numofneg>0) {
      if ( sui_test_flag(widget, SUAF_DIV_COLS)) sumw += (i-1);
      sui_coordinate_t onew = (widget->place.w-sumw-1)/numofneg;
      tcol = wtab->tdata->firstcol;
      i = 0;
      while(i<wtab->cols && tcol) {
        if (tcol->width<0) {
          numofneg--;
          if (numofneg) {
            tcol->width=onew;
            sumw += onew;
          } else
            tcol->width=widget->place.w-sumw-1;
        }
        i++;
        tcol = tcol->next;
      }
    }
  }
  sui_wtable_get_hrow(wtab);

/* FIXME: dimensions initialization */
  if (widget->place.w < 0 || widget->place.h < 0) {
    sui_point_t size;
    sui_wtable_get_size(dc, widget, &size);
    if (widget->place.w < 0)
      widget->place.w = size.x;
    if (widget->place.h < 0)
      widget->place.h = size.y;
  }
}

/**
 * sui_wtable_draw - draw table widget content
 * @dc: Pointer to device context.
 * @widget: Pointer to table widget.
 * @event: Pointer to the event.
 *
 * The function draws table widget.
 *
 * Return Value: The function does not return a value.
 * File: sui_wtable.c
 */
//inline
int sui_wtable_draw( sui_dc_t *dc, sui_widget_t *widget)
{
  sui_dcfont_t *pdcfont;
  suiw_table_t *wtab = sui_tabwdg(widget);
  sui_table_column_t *tcol = NULL, *fcol = NULL;
  sui_point_t box = {.x=widget->place.w, .y=widget->place.h};
  sui_point_t origin;
  sui_point_t size;
  sui_rect_t dimfield;
  sui_point_t contorigin;
  sui_point_t contsize;
  sui_textdims_t textsize;
  sui_align_t colal;
  utf8 *fieldtext;

  int ly,i, col, row, irow, icol;
  int addverline = sui_test_flag(widget,SUAF_DIV_COLS)?1:0;
  int addhorline = sui_test_flag(widget,SUAF_DIV_ROWS)?1:0;
  long crow = -1, ccol = -1, span = 1;
  int selected, canbeselected, framed = 0;

/* FIXME: this is only temporary solution !!! check and update table maxidx automatically with changing dinfo */
  if (sui_test_flag(widget,SUAF_AUTO_DRANGE)) {
    if (wtab->tdata && wtab->tdata->firstcol && wtab->tdata->firstcol->data) {
      wtab->tdata->maxidx = wtab->tdata->firstcol->data->idxsize;
    }
  }

/* get size of visible table */
  sui_wtable_get_size(dc, widget, &size);
  sui_align_object(&box, &origin, &size, 0, widget->style, sui_widget_get_item_align(widget));

  if (origin.x<0) origin.x=0;
  if (origin.y<0) origin.y=0;

  dimfield.x = origin.x; dimfield.y = origin.y;
  dimfield.w = box.x; dimfield.h = box.y;
  sui_gdi_set_carea(dc, &dimfield);

/* get current row/column and automatic repair first row/column */
  if (wtab->currow) {
    if (sui_rd_long(wtab->currow, 0, &crow)== SUI_RET_OK) {
      if (crow < wtab->frow) {
        wtab->frow = crow;
      } else if (crow >= (wtab->frow+wtab->rows)) {
        wtab->frow = crow-wtab->rows+1;
      }
      if (wtab->frow < 0) wtab->frow = 0;
    }
  } else if (sui_test_flag(widget, SUAF_AUTO_DRANGE) && !sui_test_flag(widget,SUFL_FOCUSED)) {
    if ((wtab->tdata->maxidx > wtab->rows) &&
        (wtab->frow<(wtab->tdata->maxidx-wtab->rows)))
      wtab->frow = wtab->tdata->maxidx-wtab->rows;
  }

  if (wtab->curcol) {
    if (sui_rd_long(wtab->curcol, 0, &ccol)== SUI_RET_OK) {
      if (ccol < wtab->fcol) {
        wtab->fcol = ccol;
      } else if (ccol >= (wtab->fcol+wtab->cols)) {
        wtab->fcol = ccol-wtab->cols+1;
      }
      if (wtab->fcol < 0) wtab->fcol = 0;
    }
  }

/* determine number of drawed rows and cols */
  if (wtab->tdata->maxidx==0)
    irow = 0;
  else  
    irow = wtab->tdata->maxidx-wtab->frow; /* number of rows from first to max */
  if (irow>wtab->rows) irow=wtab->rows;
  if (wtab->tdata->colnum==0)
    icol = 0;
  else
    icol = wtab->tdata->colnum-wtab->fcol; /* number of columns from first to max */
  if (icol>wtab->cols)
    icol=wtab->cols;

/* determine first column */
  if (wtab->tdata) fcol = wtab->tdata->firstcol;
  i = wtab->fcol;
  while ( i && fcol) {
    fcol = fcol->next;
    i--;
  }
  if (!fcol) return 1; /* no columns */

  pdcfont=sui_font_prepare(dc, widget->style->fonti->font, widget->style->fonti->size);
  sui_font_set(dc, pdcfont);

/* draw title */
  if (sui_test_flag(widget,SUAF_TITLES)) {
    dimfield.h = wtab->htitle+addhorline;
    if ((irow>0) && sui_test_flag(widget,SUAF_DIV_ROWS)) {
      tcol = fcol;
      i = icol;
      sui_set_widget_fgcolor(dc, widget, SUC_FRAME); // SUC_ITEM
      sui_draw_line(dc, dimfield.x, dimfield.y+dimfield.h, dimfield.x+size.x, dimfield.y+dimfield.h);
    }
    tcol = fcol;
    i = icol;
    while(tcol && i) {
      colal = tcol->align | SUAL_INSIDE;
      /* FIXME: width for spanned columns ??? */
      dimfield.w = tcol->width + addverline;
      sui_gdi_set_carea(dc, &dimfield);

      if (sui_is_flag(widget,SUAF_SELECT_MODE,SUAF_SELECT_NONE))
        sui_set_widget_fgcolor(dc, widget, SUC_BACKGROUND); //SUC_ITEM2);
      else
        sui_set_fgcolor(dc, widget, SUC_BACKGROUND);
      sui_draw_rect_box(dc, &dimfield, 0);

      if (sui_is_flag(widget,SUAF_SELECT_MODE,SUAF_SELECT_NONE)) {
        sui_set_widget_fgcolor(dc, widget, SUC_ITEM);
        sui_set_widget_bgcolor(dc, widget, SUC_BACKGROUND);
      } else {
        sui_set_fgcolor(dc, widget, SUC_ITEM);
        sui_set_bgcolor(dc, widget, SUC_BACKGROUND);
      }
      sui_text_get_size(dc, pdcfont, &textsize, -1, tcol->title, colal);
      box.x = dimfield.w; box.y = dimfield.h;
      contsize.x = textsize.w; contsize.y = textsize.h;
      sui_align_object(&box, &contorigin, &contsize, textsize.b, widget->style, colal);
      sui_draw_text(dc, dimfield.x+contorigin.x, dimfield.y+contorigin.y,
                    -1, tcol->title, colal);
      /* draw vertical line */
      if (sui_test_flag(widget,SUAF_DIV_COLS) && (i>1)) {
        sui_set_widget_fgcolor(dc, widget, SUC_FRAME); // SUC_ITEM
        sui_draw_line(dc, dimfield.x+dimfield.w-1, dimfield.y, dimfield.x+dimfield.w-1, dimfield.y+dimfield.h);
      }
      dimfield.x += dimfield.w;
      tcol = tcol->next;
      i--;
    }
    dimfield.y += dimfield.h;
    dimfield.x = origin.x;
  }

/* draw all visible rows */
  if (wtab->hrow > 0) /* height of row is set */
    ly = wtab->hrow;
  else if (wtab->htitle > 0) /* otherwise ... height of title is set */
    ly = wtab->htitle;
  else if (wtab->rows>0) /* otherwise ... number of rows is set */
    ly = dimfield.h / wtab->rows;
  else if (irow>0)       /* otherwise ... we know number of rows */
    ly = dimfield.h / irow;
  else               /* otherwise ... we don't know anything */
    ly = 0;

  row = wtab->frow;
  while(irow) {
    if (wtab->tdata->maxidx>0 && (row >= wtab->tdata->maxidx))
      break;
    tcol=fcol;
    i = icol;
    dimfield.h = ly+addhorline;
    dimfield.x = origin.x;
    if (sui_test_flag(widget,SUAF_DIV_ROWS)) { /* draw line at top */
      dimfield.w = size.x;
      sui_gdi_set_carea(dc, &dimfield);
      sui_set_widget_fgcolor(dc, widget, SUC_FRAME); // SUC_ITEM
      sui_draw_line(dc, dimfield.x, dimfield.y, dimfield.x+size.x, dimfield.y);
    }

    if (sui_is_flag(widget,SUAF_SELECT_MODE, SUAF_SELECT_ROW)) {
      if (crow != row) canbeselected = 0; /* cannot be selected */
      else canbeselected = 1; /* must be selected */
    } else canbeselected = -1; /* can be selected by column */

    col = wtab->fcol;
    /* draw all columns */
    while(tcol && i) {
      colal = tcol->align | SUAL_INSIDE;
      if ( tcol->font) {
        pdcfont=sui_font_prepare(dc, tcol->font->font, tcol->font->size);
        sui_font_set(dc, pdcfont);
      } else {
        pdcfont=sui_font_prepare(dc, widget->style->fonti->font, widget->style->fonti->size);
        sui_font_set(dc, pdcfont);
      }

      /* FIXME: width for spanned columns */
    /* determine field width - spanned fields */
      if (tcol->span) {
        sui_table_column_t *scol = tcol;
        int j;
        if (sui_rd_long(scol->span, row, &span)!= SUI_RET_OK)
          span = 1; /* error in span dinfo - no spanning */
        j=span;
        dimfield.w = 0;
        while (j-- && scol) {
          dimfield.w += scol->width+addverline;
          scol = scol->next;
        }
      } else {
        span = 1;
        dimfield.w = tcol->width+addverline;
      }

      sui_gdi_set_carea(dc, &dimfield);

    /* determine selection and set colors */
      if (canbeselected) {
        if (sui_test_flag(widget,SUAF_SELECT_MODE) & SUAF_SELECT_COL) {
          if ((ccol >= col) && (ccol<col+span)) selected = 1;
          else selected = 0;
        } else {
          if (canbeselected > 0) selected = 1;
          else selected = 0;
        }
      } else selected = 0;
      // ul_logdeb("Table - [r=%d,c=%d]=>sel=%d (csel=%d)\n",row,col,selected,canbeselected);

    /* draw background */
      if (sui_test_flag(widget, SUFL_FOCUSED)) {
        selected = (selected) ? SUC_SELECTED : 0;
        framed = 0; /* no frame */
      } else {
        framed = selected; /* if selected - draw a frame */
        selected = 0;
      }
      sui_set_fgcolor(dc, widget, SUC_BACKGROUND+selected);    /* bg color of line */
      sui_draw_rect(dc, dimfield.x, dimfield.y+addhorline, dimfield.w, dimfield.h-addhorline, 0);    /* draw item background */
      sui_set_fgcolor(dc, widget, SUC_ITEM+selected);
      sui_set_bgcolor(dc, widget, SUC_BACKGROUND+selected);

    /* draw field content */
      if (!sui_table_column_item_get_text(tcol, row, &fieldtext) && fieldtext) {
        sui_text_get_size(dc, pdcfont, &textsize, -1, fieldtext, colal);
        box.x = dimfield.w; box.y = dimfield.h-addhorline;
        contsize.x = textsize.w; contsize.y = textsize.h;
        sui_align_object(&box, &contorigin, &contsize, textsize.b, widget->style, colal);
        sui_draw_text(dc, dimfield.x+contorigin.x, dimfield.y+addhorline+contorigin.y,
                      -1, fieldtext, colal);
        sui_utf8_dec_refcnt(fieldtext);
      } // else { something as '---' }

    /* draw frame */
      if (framed && sui_test_flag(widget, SUAF_VIEW_SELECTED)) {
        if (i == icol) {
          sui_draw_line(dc, dimfield.x, dimfield.y+addhorline+1, dimfield.x, dimfield.y+dimfield.h-addhorline-addhorline-1);
        }
        sui_draw_line(dc, dimfield.x, dimfield.y+addhorline, dimfield.x+dimfield.w, dimfield.y+addhorline);
        sui_draw_line(dc, dimfield.x, dimfield.y+dimfield.h-3*addhorline, dimfield.x+dimfield.w, dimfield.y+dimfield.h-3*addhorline);
      }

    /* draw vertical line */
      if (sui_test_flag(widget,SUAF_DIV_COLS) && ((i-span)>0)) {
        sui_set_widget_fgcolor(dc, widget, SUC_FRAME); // SUC_ITEM
        sui_draw_line(dc, dimfield.x+dimfield.w-1, dimfield.y, dimfield.x+dimfield.w-1, dimfield.y+dimfield.h);
      }
      dimfield.x += dimfield.w;
      /* skip all spanned fields */
      while(span-- && tcol) {
        tcol = tcol->next;
        i--;
        col++;
      }
    }
    row++;
    dimfield.y += ly;
    irow--;
  }

  return 1;
}


/**
 * sui_wtable_hkey - Reaction to KEY_DOWN, KEY_UP event in table widget
 * @widget: Pointer to table widget.
 * @event: Pointer to the event.
 *
 * The function is response to keyboard event for table widget.
 *
 * Return Value: The function returns 0 if it takes to event.
 * File: sui_wtable.c
 */
//inline
int sui_wtable_hkey(sui_widget_t *widget, sui_event_t *event)
{
  int key = event->keydown.keycode;
  suiw_table_t *wtab = sui_tabwdg(widget);
  long addrow = 0, addcol = 0, newval = 0;

  if (!wtab->tdata) return -1;
  
  switch(key) {
    case MWKEY_UP:       addrow=-1; break;
    case MWKEY_DOWN:     addrow=+1; break;
    case MWKEY_LEFT:     addcol=-1; break;
    case MWKEY_RIGHT:    addcol=+1; break;
  }

  if (addrow) { /* row position changed */
    if (sui_test_flag(widget,SUAF_SELECT_ROW) && wtab->currow) {
      long crow = 0;
      if (sui_rd_long(wtab->currow,0,&crow)!=SUI_RET_OK) return -1;
      newval = crow + addrow;
      if (newval >= wtab->tdata->maxidx)
        newval = wtab->tdata->maxidx-1;
      if (newval < 0) newval = 0;
//ul_logdeb("Move table selrow- c=%ld,m=%ld,o=%ld\n",newval,wtab->tdata->maxidx,crow);
      if (crow != newval) {
        if (sui_wr_long(wtab->currow,0,&newval)!=SUI_RET_OK) return -1;
        if (newval < wtab->frow)
          wtab->frow = newval;
        else if (newval >= wtab->frow + wtab->rows)
          wtab->frow = newval - wtab->rows+1;
        return 1;
      } else if (!sui_test_flag(widget, SUAF_CHANGE_FOCUS)) { /* crow == newval */
        if (crow==0 && addrow<0) return 1;
        if (crow && addrow>0) return 1;
      }
    } else {
      newval = wtab->frow + addrow;
      if (newval > wtab->tdata->maxidx-wtab->rows)
        newval = wtab->tdata->maxidx-wtab->rows;
      if (newval < 0) newval = 0;
      if (wtab->frow != newval) {
        wtab->frow = newval;
        return 1;
      } else if (!sui_test_flag(widget, SUAF_CHANGE_FOCUS)) { /* frow == newval */
        if (wtab->frow==0 && addrow<0) return 1;
        if (wtab->frow>=(wtab->tdata->maxidx-wtab->rows) && addrow>0) return 1;
      }
    }
  }
  if (addcol) { /* column position changed */
    if (sui_test_flag(widget,SUAF_SELECT_COL) && wtab->curcol) {
      long ccol = 0;
      if (sui_rd_long(wtab->curcol,0,&ccol)!=SUI_RET_OK) return -1;
      newval = ccol + addcol;
      if (newval >= wtab->tdata->colnum)
        newval = wtab->tdata->colnum-1;
      if (newval < 0) newval = 0;
      if (ccol != newval) {
        if (sui_wr_long(wtab->curcol,0,&newval)!=SUI_RET_OK) return -1;
        if (newval < wtab->fcol)
          wtab->fcol = newval;
        else if (newval >= wtab->fcol + wtab->cols)
          wtab->fcol = newval - wtab->cols+1;
        return 1;
      } else if (!sui_test_flag(widget, SUAF_CHANGE_FOCUS)) { /* ccol == newval */
        if (ccol==0 && addcol<0) return 1;
        if (ccol && addcol>0) return 1;
      }
    } else {
      newval = wtab->fcol + addcol;
      if (newval > wtab->tdata->colnum-wtab->cols)
        newval = wtab->tdata->colnum-wtab->cols;
      if (newval < 0) newval = 0;
      if (wtab->fcol != newval) {
        wtab->fcol = newval;
        return 1;
      } else if (!sui_test_flag(widget, SUAF_CHANGE_FOCUS)) { /* fcol == newval */
        if (wtab->fcol==0 && addcol<0) return 1;
        if (wtab->fcol>=(wtab->tdata->colnum-wtab->cols) && addcol>0) return 1;
      }
    }
  }
  if (((key==' ') || (key==MWKEY_ENTER)) && wtab->tdata &&
      sui_test_flag(widget, SUAF_SELECT_MODE)) {
    int ret = SUI_RET_OK;
    
    newval = sui_test_flag(widget, SUAF_SELECT_MODE);

    if (newval & SUAF_SELECT_ROW) {
      ret |= sui_rd_long(wtab->currow,0,&addrow);
      if (addrow<0 || addrow>=wtab->tdata->maxidx) ret = SUI_RET_ERR;
    } else
      addrow=0;

    if (newval & SUAF_SELECT_COL) {
      ret |= sui_rd_long(wtab->curcol,0,&addcol);
      if (addcol<0 || addcol>=wtab->tdata->colnum) ret = SUI_RET_ERR;
    } else
      addcol=0;

    if (ret == SUI_RET_OK) {
      newval = (addrow & 0xffff) | ((addcol<<16)&&0xffff); /* in info there is a value composed from the current column and the current row */
ul_logdeb("Pressed Enter - event(info=0x%08lX)\n",newval);
      sui_put_command_event( SUCM_CONFIRM, widget, newval);
      return 1;
    }
  }
  return 0;
}


/******************************************************************************
 * Table event handler
 ******************************************************************************/
/**
 * sui_wtable_hevent - Widget event handler for table widget
 * @widget: Pointer to the list widget.
 * @event: Pointer to the event.
 *
 * The function is response to all events for the list widget.
 * Return Value: The function does not return a value.
 * File: sui_list.c
 */
void sui_wtable_hevent( sui_widget_t *widget, sui_event_t *event)
{
  switch( event->what) {
    case SUEV_DRAW:
    case SUEV_REDRAW:
      sui_widget_draw( event->draw.dc, widget, sui_wtable_draw);
      return;
    case SUEV_KDOWN:
      if ( sui_wtable_hkey( widget, event)>0) {
        sui_clear_event( widget, event);
        sui_redraw_request( widget);
      }
      break;
    case SUEV_COMMAND:
      switch(event->message.command) {
// 			case SUCM_FOCUSSET:
// 				if ( sui_test_flag( widget, SULF_CHANGE_FOCUS)) {
// //        ul_logmsg("wlb-FOCUS:event.msg.info=0x%X\n",event->message.info);
// 					sui_list_item_t *item = NULL;
// 					struct sui_list *list = sui_lstwdg(widget)->list;
// 					sui_dinfo_t *dp = sui_lstwdg(widget)->pos;
// 					long pos = 0;
//           if ( sui_lstwdg(widget)->filtered) list=sui_lstwdg(widget)->filtered;
//
// 					if ( event->message.info == 1) {
// 						item = sui_list_gavl_first( list);
// 					} else if ( event->message.info == -1) {
// 						item = sui_list_gavl_last( list);
// 					} else {
// 						if ( sui_rd_long( dp, 0, &pos) == SUI_RET_OK) {
// 							sui_listcoord_t tmppos = pos;
// 							item = sui_list_gavl_find( list, &tmppos);
// 						}
// 					}
// 					if ( item) {
// 						pos = item->idx;
// 						sui_wr_long( dp, 0, &pos);
// 						sui_wlistbox_check_visible_item( widget, pos, item);
// 					}
// 				}
// 				break;
        case SUCM_INIT:
          sui_widget_hevent(widget, event);
          sui_wtable_init( (sui_dc_t *)event->message.ptr, widget);
          sui_clear_event( widget, event);
          break;
        case SUCM_GETSIZE:
          if ( sui_wtable_get_size( (sui_dc_t *)event->message.ptr,
                              widget, (sui_point_t *)event->message.info)>0) {
            sui_clear_event( widget, event);
          }
          break;
      }
  }
  if ( event->what != SUEV_NOTHING)
    sui_widget_hevent( widget,event);
}


/******************************************************************************
 * Table widget vmt & signal-slot mechanism
 ******************************************************************************/
int sui_wtable_vmt_init( sui_widget_t *pw, void *params)
{
  suiw_table_t *wtab;
  pw->data = sui_malloc( sizeof( suiw_table_t));
  if(!pw->data)
    return -1;
  wtab = sui_tabwdg(pw);
  wtab->frow = -1;
  wtab->fcol = -1;
  wtab->htitle = -1;
  wtab->hrow = -1;

  pw->type = SUWT_TABLE;
  pw->hevent = sui_wtable_hevent;

  return 0;
}

void sui_wtable_vmt_done( sui_widget_t *pw)
{
  if ( pw->data) {
    suiw_table_t *wtab = pw->data;

//    sui_wtable_column_t *cols;
    if ( wtab->currow) sui_dinfo_dec_refcnt( wtab->currow);
    wtab->currow = NULL;
    if ( wtab->curcol) sui_dinfo_dec_refcnt( wtab->curcol);
    wtab->curcol = NULL;
    if ( wtab->tdata) sui_table_dec_refcnt( wtab->tdata);
    wtab->tdata = NULL;

    sui_free( wtab);
    pw->data = NULL;
  }
  pw->type = SUWT_GENERIC;
  pw->hevent = sui_widget_hevent; // NULL;
}


SUI_CANBE_CONST
sui_obj_signal_tinfo_t sui_wtable_signal_tinfo[] = {
};

SUI_CANBE_CONST
sui_obj_slot_tinfo_t sui_wtable_slot_tinfo[] = {
};

SUI_CANBE_CONST
sui_obj_tinfo_t sui_wtable_vmt_obj_tinfo = {
  .name = "sui_wtable",
  .obj_size = sizeof(sui_widget_t),
  .obj_align = UL_ALIGNOF(sui_widget_t),
  .signal_count = MY_ARRAY_SIZE(sui_wtable_signal_tinfo),
  .signal_tinfo = UL_CAST_UNQ1(sui_obj_signal_tinfo_t *, sui_wtable_signal_tinfo),
  .slot_count = MY_ARRAY_SIZE(sui_wtable_slot_tinfo),
  .slot_tinfo = UL_CAST_UNQ1(sui_obj_slot_tinfo_t *, sui_wtable_slot_tinfo)
};

sui_wtable_vmt_t sui_wtable_vmt_data = {
  .vmt_size = sizeof(sui_wtable_vmt_t),
  .vmt_refcnt = SUI_STATIC,
  .vmt_parent = &sui_widget_vmt_data,
  .vmt_init = sui_wtable_vmt_init,
  .vmt_done = sui_wtable_vmt_done,
  .vmt_obj_tinfo = UL_CAST_UNQ1(sui_obj_tinfo_t *, &sui_wtable_vmt_obj_tinfo),

};

/******************************************************************************/

sui_widget_t *sui_wtable( sui_rect_t *aplace, utf8 *alabel, sui_style_t *astyle,
              sui_flags_t aflags, sui_table_t *atab, sui_dinfo_t *apos,
              sui_listcoord_t arows)
{
  sui_widget_t *pw = sui_widget_create_new(&sui_wtable_vmt_data, aplace,
                      alabel, astyle, aflags);
  if (!pw) return NULL;

  {
    suiw_table_t *wtab = sui_tabwdg(pw);

    wtab->rows = arows;

    if ( atab) {
      sui_table_inc_refcnt( atab);
      wtab->tdata = atab;
      wtab->cols = atab->colnum;
    }

    if ( apos) {
      sui_dinfo_inc_refcnt( apos);
      wtab->currow = apos;
    }

  }
  return pw;
}
