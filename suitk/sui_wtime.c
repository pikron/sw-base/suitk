/* sui_time.c
 *
 * SUITK time widget. (Time and date)
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#include "sui_wtime.h"

#include <ul_log.h>

UL_LOG_CUST(ulogd_suitk)

/* text constants - default names of days, months and AP/PM sign */
const char *sui_time_default_dow[7] = { "Sun", "Mon", "Tue", "Wed","Thu","Fri","Sat"};
const char *sui_time_default_mon[12] = { "Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};
const char *sui_time_default_ampm[2] = { "AM","PM"};


/******************************************************************************
 * Time widget slot functions
 ******************************************************************************/
int sui_wtime_set_format( sui_widget_t *widget, utf8 *text)
{
  if ( widget) {
    suiw_time_t *wtim = sui_timwdg( widget);
    sui_utf8_dec_refcnt(wtim->format);
    if ( !text) {
      wtim->format = NULL;
    } else {
      wtim->format = sui_utf8_dup(text);
    }
  }
  return 0;
}

/******************************************************************************
 * Time widget time zone related funcions
 ******************************************************************************/

int sui_wtime_get_selected_tzdst( sui_widget_t *widget, long *ptzoffs, sui_time_dst_rule_t **pdstr)
{
  if ( sui_test_flag( widget, SUDF_LOCALTIME)) {
    *ptzoffs = sui_time_tz_offset;
    *pdstr = sui_time_dst_rule;
  } else {
    *ptzoffs = 0;
    *pdstr = NULL;
  }
  return 0;
}

/******************************************************************************/
int sui_time_read_value( sui_widget_t *widget, sui_time_elements_t *buf)
{
  int ret;
  if ( sui_is_flag( widget, SUFL_EDITED | SUDF_ONLINEEDIT, SUFL_EDITED)) {
    *buf = sui_timwdg(widget)->edtime;
  } else {
    unsigned long val;
    long tzoffs;
    sui_time_dst_rule_t *dstr;
    if (sui_timwdg(widget)->data->tinfo == SUI_TYPE_LONG)
      ret = sui_rd_long(sui_timwdg(widget)->data, 0, (long *)&val);
    else
      ret = sui_rd_ulong(sui_timwdg(widget)->data, 0, &val);
    if ( ret != SUI_RET_OK) return ret;
    sui_wtime_get_selected_tzdst(widget, &tzoffs, &dstr);
    if ( sui_time_convert_sec2tzdst( val, buf, tzoffs, dstr) < 0) return SUI_RET_ERR;
  }
  return SUI_RET_OK;
}
int sui_time_write_value( sui_widget_t *widget, sui_time_elements_t *buf)
{
  if ( sui_is_flag( widget, SUFL_EDITED | SUDF_ONLINEEDIT, SUFL_EDITED)) {
    sui_timwdg(widget)->edtime = *buf;
  } else {
    long tzoffs;
    sui_time_dst_rule_t *dstr;
    unsigned long val;
    sui_wtime_get_selected_tzdst(widget, &tzoffs, &dstr);
    if ( sui_time_convert_tzdst2sec( buf, &val, tzoffs, dstr) < 0) return SUI_RET_ERR;
    return sui_wr_ulong( sui_timwdg(widget)->data, 0, &val);
  }
  return SUI_RET_OK;
}

/******************************************************************************/
int sui_time_get_part( sui_time_elements_t *tm, unsigned long *value, unsigned long *min, unsigned long *max,
                      unsigned short type, long tzoffs, sui_time_dst_rule_t *dstr)
{
  unsigned long ret, rmin, rmax;
  if ( !value || !tm) return SUI_RET_EPERM;
  switch( type & SUTE_EMASK) {
    case SUTE_SECOND:
      ret = tm->sec;
      rmin = 0; rmax = 59;
      break;
    case SUTE_MINUTE:
      ret = tm->min;
      rmin = 0; rmax = 59;
      break;
    case SUTE_HOUR:
      rmin = 0;
      switch ( type & SUTE_SPCMASK) {
        case SUTE_HR_12:
          ret = tm->hour + 1;
          if ( ret > 12) ret -= 12;
          rmin = 1; rmax = 12;
          break;
        case SUTE_HR_AMPM:
          ret = (tm->hour > 11) ? 1 : 0;
          rmax = 1;
          break;
        default:
          if ( type & SUTE_99HOURS) {
            //ret = (tm->mday-1) * 24 + tm->hour;
            ret = tm->yday * 24 + tm->hour;
            rmax = 99;
          } else {
            ret = tm->hour;
            rmax = 23;
          }
          break;
      }
      break;
    case SUTE_DAY:
      ret = sui_time_year_is_leap(tm->year);
      rmin = 1;
      rmax = sui_time_month_description[ret][tm->mon+1]-sui_time_month_description[ret][tm->mon];
      ret = tm->mday;
      break;
    case SUTE_MONTH:
      ret = tm->mon + 1;
      rmin = 1;
      rmax = 12;
      break;
    case SUTE_YEAR:
      switch ( type & SUTE_SPCMASK) {
        case SUTE_YR_CENT:
          ret = (tm->year / 100) + 1;
          rmin = 20; rmax = 22;
          break;
        case SUTE_YR_100:
          ret = tm->year % 100;
          rmin = 0; rmax = 99; break;
        default:
          ret = tm->year;
          rmin = SUI_TIME_BASE_YEAR;
          rmax = 2100; break;
      }
      break;
    case SUTE_DOW:
      ret = tm->wday;
      rmin = 0;
      rmax = 6;
      break;
    case SUTE_DOY:
      if (( type & SUTE_SPCMASK) ==  SUTE_DY_WOY) {
        return SUI_RET_NRDY;
        //rmin = 1;
        //rmax = 52;
      } else {
        ret = tm->yday + 1;
        rmin = 1;
        rmax = sui_time_year_is_leap(tm->year) ? 366 : 365;
      }
      break;
    case SUTE_TIME:
      sui_time_convert_tzdst2sec( tm, &ret, tzoffs, dstr);
      rmin = 0;
      rmax = 0xffffffff;
      break;
    default:
      return SUI_RET_NRDY;
  }
  *value = ret;
  if ( min) *min = rmin;
  if ( max) *max = rmax;
  return SUI_RET_OK;
}

int sui_time_set_part( sui_time_elements_t *tm, unsigned long value, unsigned short type,
                       long tzoffs, sui_time_dst_rule_t *dstr)
{
  if ( !tm) return SUI_RET_EPERM;
  switch( type & SUTE_EMASK) {
    case SUTE_SECOND:
      tm->sec = value;
      break;
    case SUTE_MINUTE:
      tm->min = value;
      break;
    case SUTE_HOUR:
      switch ( type & SUTE_SPCMASK) {
        case SUTE_HR_12:
          tm->hour = (value-1) % 12; /* am/pm must be setted after this ... */
          break;
        case SUTE_HR_AMPM:
          if ((tm->hour < 12) && ( value == 1))
            tm->hour += 12;
          else if ((tm->hour > 12) && ( value == 0))
            tm->hour -= 12;
          break;
        default:
          if ( type & SUTE_99HOURS) {
            if ( value > 99) value = 99;
            tm->hour = value % 24;
            tm->mday = 1 + value / 24;
            tm->yday = value / 24;
          } else
            tm->hour = value;
          break;
      }
      break;
    case SUTE_DAY: /* it must change yday and wday according to year, month and mday */
      tm->mday = value;
      sui_time_convert_dmy2doy( tm);
      sui_time_convert_dmy2dow( tm);
      break;
    case SUTE_MONTH: /* it must change yday and wday according to year, month and mday */
      tm->mon = value - 1;
      sui_time_convert_dmy2doy( tm);
      sui_time_convert_dmy2dow( tm);
      break;
    case SUTE_YEAR: /* it must change yday and wday according to year, month and mday */
      switch ( type & SUTE_SPCMASK) {
        case SUTE_YR_CENT:
          tm->year = (tm->year % 100) + (value-1);
          break;
        case SUTE_YR_100:
          tm->year = tm->year - (tm->year % 100) + value;
          break;
        default:
          tm->year = value;
          break;
      }
      sui_time_convert_dmy2doy( tm);
      sui_time_convert_dmy2dow( tm);
      break;
    case SUTE_DOW: /* it must change mday, and it must computes yday according to year, month and mday */
      {
        int diff = value - tm->wday; /* difference between old and new day of week */
        if ( diff < 0) diff += 7;
        tm->wday = value;
        if (( tm->mday - ( 7 - diff)) < 1)
          tm->mday += diff;
        else
          tm->mday -= (7-diff);
      }
      sui_time_convert_dmy2doy( tm);
      break;
    case SUTE_DOY: /* it must change mday, month, wday according to year and yday */
      if (( type & SUTE_SPCMASK) ==  SUTE_DY_WOY) {
        // tm->yday = ...
      } else {
        tm->yday = value - 1;
      }
      sui_time_convert_yd2dmw( tm);
      break;
    case SUTE_TIME:
      sui_time_convert_sec2tzdst( value, tm, tzoffs, dstr);
      break;
    default:
      return SUI_RET_NRDY;
  }
  return SUI_RET_OK;
}

/******************************************************************************/

#define SUI_TIME_MAX_STRING_SIZE 128

/**
 * sui_time_transform_time - Create string from time dinfo and its format
 * @widget: pointer to time widget
 * @varidx: index of current selected variable for the first char in string of variable returning in $fchvar
 * @fchvar: pointer to output int for first char in string of current variable
 * The function converts time dinfo according to format to utf8 string. If $varidx is equal or great to
 * zero the function will return position of first character ($fchvar) and size ($schvar) in string of indexed variable.
 * The length of output string can be SUI_TIME_MAX_STRING_SIZE characters maximally.
 * If day_name (month_name) dinfo is set in time widget structure and day (month) name is required  it is used otherwise
 * static included names are used Sun-Sat(Jan-Dec).
 * Return: The function returns pointer to utf8 string with time in required format.
 * The output utf8 string has refcnt equal to one (It must be decremented)
 * File: sui_time.c
 */
 utf8 *sui_time_transform_time( sui_widget_t *widget, int varidx, int *fchvar)
 {
   suiw_time_t *wtim = sui_timwdg(widget);
  utf8 *ret = NULL, *tmp = NULL;
  utf8char *format;

  char defformat[] = "%a %b %d %H:%M:%S ??? %Y"; /* Sat Nov 04 12:02:33 EST 1989 */

  int fsize, chsize, fchv, schv = 0;
  char *nstr = NULL;
  int nadd = 0, nset;
  unsigned long nnum = 0;

  unsigned short idxchv = 0;
  unsigned short etype = 0;

  sui_time_elements_t tm, *ptm;

  long tzoffs;
  sui_time_dst_rule_t *dstr;

  sui_wtime_get_selected_tzdst(widget, &tzoffs, &dstr);

//  if ( !wtim->data) return NULL;

  if ( !wtim->format) { /* no format - return time in ms */
    format = (utf8char*)defformat;
    fsize = sui_utf8_length( U8(defformat));
  } else {
    format = sui_utf8_get_text( wtim->format); /* get format string */
    fsize = sui_utf8_length( wtim->format);
  }

  if ( sui_time_read_value( widget, &tm) != SUI_RET_OK) return NULL;
  ptm = &tm;

  if ((ret = sui_utf8( NULL, SUI_TIME_MAX_STRING_SIZE, -1)) == NULL)
    return sui_dinfo_2_utf8( wtim->data, 0, NULL, 0);

  if (sui_is_flag(widget, SUFL_EDITED | SUDF_99HOURS | SUDF_CHECKLIMITS, SUDF_99HOURS | SUDF_CHECKLIMITS)) {
    if (((ptm->yday * 24 + ptm->hour)>999) || ptm->mon || ptm->year>1970) {
      sui_utf8_cat( ret, U8"---");
      wtim->maxidx = -1;
      return ret;
    }
  }

  while( fsize) {
    chsize = sui_utf8_char_size( format);
    if (( chsize==1) && (*format== '%')) { /* special char */
      fsize--; format++;
      chsize = sui_utf8_char_size( format);
      if ( chsize == 1) {
        nset = 2;
        fchv = sui_utf8_length( ret);
        if ( *format>='0' && *format<='9') {
          nset = *format-'0';
          fsize--; format++;
          chsize = sui_utf8_char_size( format);
        }
        switch( *format) {
        case '%': /* '%' character */
          sui_utf8_add_char( ret, (utf8char*)"%");
          etype = SUTE_NONE;
          break;
        case 'a': case 'A': /* weekday */
        {
          unsigned long nmin, nmax;
          etype = SUTE_DOW | SUTE_TEXT;
          if ( sui_time_get_part( ptm, &nnum, &nmin, &nmax, etype, tzoffs, dstr) == SUI_RET_OK) {
            if (( nnum < nmin) || ( nnum > nmax)) {
              if ( sui_generic_long_to_str( &nnum, &nstr, 0, 10) == SUI_RET_OK) {
                sui_utf8_cat( ret, U8(nstr));
                schv = sui_utf8_length( U8(nstr));
                free( nstr);
              } else
                schv = 0;
            } else {
              if ((wtim->day_name==NULL) ||
                  ( sui_rd_utf8( wtim->day_name, nnum, &tmp) != SUI_RET_OK)) {
                tmp = U8(sui_time_default_dow[nnum]);
              }
              sui_utf8_cat( ret, tmp);
              schv = sui_utf8_length( tmp);
              sui_utf8_dec_refcnt( tmp);
            }
          } else
            schv =  0;
          idxchv++;
        }
          break;
        case 'b': case 'h': case 'B': /* month */
        {
          unsigned long nmin, nmax;
          etype = SUTE_MONTH | SUTE_TEXT;
          if ( sui_time_get_part( ptm, &nnum, &nmin, &nmax, etype, tzoffs, dstr) == SUI_RET_OK) {
            if (( nnum < nmin) || ( nnum > nmax)) {
              if ( sui_generic_long_to_str( &nnum, &nstr, 0, 10) == SUI_RET_OK) {
                sui_utf8_cat( ret, U8(nstr));
                schv = sui_utf8_length( U8(nstr));
                free( nstr);
              } else
                schv = 0;
            } else {
              nnum--; /* month is returned between 1 and 12, but we need 0-11 */
              if ((wtim->month_name==NULL) ||
                ( sui_rd_utf8( wtim->month_name, nnum, &tmp) != SUI_RET_OK)) {
                tmp = U8(sui_time_default_mon[nnum]);
              }
              sui_utf8_cat( ret, tmp);
              schv = sui_utf8_length( tmp);
              sui_utf8_dec_refcnt( tmp);
            }
          } else
            schv = 0;
          idxchv++;
        }
          break;
        case 'p': case 'P': /* AM/PM indicator */
        {
          unsigned long nmin, nmax;
          etype = SUTE_HOUR | SUTE_HR_AMPM | SUTE_TEXT;
          if ( sui_time_get_part( ptm, &nnum, &nmin, &nmax, etype, tzoffs, dstr) == SUI_RET_OK) {
            if (( nnum < nmin) || ( nnum > nmax)) {
              if ( sui_generic_long_to_str( &nnum, &nstr, 0, 10) == SUI_RET_OK) {
                sui_utf8_cat( ret, U8(nstr));
                schv = sui_utf8_length( U8(nstr));
                free( nstr);
              } else
                schv = 0;
            } else {
              if ((wtim->ampm_name==NULL) ||
                ( sui_rd_utf8( wtim->ampm_name, nnum, &tmp) != SUI_RET_OK)) {
                tmp = U8(sui_time_default_ampm[nnum]);
              }
              sui_utf8_cat( ret, tmp);
              schv = sui_utf8_length( tmp);
              sui_utf8_dec_refcnt( tmp);
            }
          } else
            schv = 0;
          idxchv++;
        }
          break;
        case 'C': /* century */
        case 'y': /* year in century 00-99 */
        case 'Y': /* year */
          etype = SUTE_YEAR;
          nadd = nset;
          if ( *format == 'C') etype |= SUTE_YR_CENT;
          else if ( *format == 'y') etype |= SUTE_YR_100;
          else nadd = 4;
          break;
  
        case 'd': case 'e': /* day of month aligned to right with zeros or spaces */
          etype = SUTE_DAY;
          if ( *format == 'd') nadd = nset; /* zeros */
          else nadd = -nset; /* spaces */
          break;
        case 'H': case 'k': /* hour 00-23 (00-99) aligned to right with zeros or spaces */
          etype = SUTE_HOUR;
          if ( sui_test_flag( widget, SUDF_99HOURS))
            etype |= SUTE_99HOURS;
          if ( *format == 'H') nadd = nset; /* zeros */
          else  nadd = -nset; /* spaces */
          break;
        case 'I': case 'l': /* hour 01-12 */
          etype = SUTE_HOUR | SUTE_HR_12;
          if ( *format == 'I') nadd = nset; /* zeros */
          else  nadd = -nset; /* spaces */
          break;
        case 'j': /* day of year 001-366 */
          etype = SUTE_DOY;
          nadd = 3;
          break;
        case 'm': case 'n': /* month 01-12 aligned to right with zeros or spaces */
          etype = SUTE_MONTH;
          if ( *format =='m') nadd = nset; /* zeros */
          else nadd = -nset; /* spaces */
          break;
        case 'M': /* minute 00-59 */
          etype = SUTE_MINUTE;
          nadd = nset;
          break;
        case 'S': /* second 00-59 */
          etype = SUTE_SECOND;
          nadd = nset;
          break;
        case 'u': case 'w': /* day of week 0-6 (Sun) or (1-7) Mon */
          if ( *format == 'w') nnum++;
          etype = SUTE_DOW;
          break;
        case 'U': case 'V': case 'W': /* week of year 0-53 */
          /* compute from wday and yday ... */
          etype = SUTE_DOY | SUTE_DY_WOY;
          nadd = nset;
          break;
        case 's':
          etype = SUTE_TIME;
          nadd = 10;
          break;
        }
        if (( etype & SUTE_TEXT) != SUTE_TEXT) {
          if (( sui_time_get_part( ptm, &nnum, NULL, NULL, etype, tzoffs, dstr) == SUI_RET_OK) &&  /* return one values by type */
              ( sui_generic_long_to_str( &nnum, &nstr, 0, 10) == SUI_RET_OK)) {
            if ( nadd > 0) { /* add zeros before number */
              schv = nadd;
              nadd -= sui_utf8_length( U8(nstr));
              while( nadd-- > 0) sui_utf8_cat( ret, U8("0"));
            } else if ( nadd < 0) { /* add spaces before number */
              schv = -nadd;
              nadd += sui_utf8_length( U8(nstr));
              while( nadd++ < 0) sui_utf8_cat( ret, U8(" "));
  
            } else { /* else draw only number*/
              schv = sui_utf8_length( U8(nstr));
            }
            sui_utf8_cat( ret, U8(nstr));
            free( nstr);
            nstr = NULL;
          }
          nadd = 0;
          idxchv++;
        }
        if ( idxchv == varidx) {
          if (fchvar)
            *fchvar = fchv;
          wtim->item_size = schv;
          wtim->item_type = etype;
        }
      }
    } else {
      sui_utf8_add_char( ret, format);
    }
    fsize--;
    format += chsize;
  }

  wtim->maxidx = idxchv;
  return ret;
 }


/**
 * sui_time_start_edit - Internal function for starting to edit of time widget
 * @widget: pointer to widget
 * @clear: start with zero value
 * The function sets EDITED flag to widget flags and copies value to internal buffer.
 * Return: The function return 1 if starting was successful and 0 otherwise.
 */
int sui_time_start_edit( sui_widget_t *widget, int clear)
{
  suiw_time_t *wtim = sui_timwdg(widget);
  unsigned long time = 0;
  long tzoffs;
  sui_time_dst_rule_t *dstr;

  if ( !sui_test_flag( widget, SUFL_EDITEN)) return 0;
  if ( !sui_test_flag( widget, SUFL_EDITED)) {
    if ( sui_rd_ulong( wtim->data, 0, &time) != SUI_RET_OK)
      time=0;
    if ( clear) {
      if ( sui_test_flag( widget, SUDF_ONLINEEDIT)) {
        unsigned long zero = 0;
        if ( sui_wr_ulong( wtim->data, 0, &zero) != SUI_RET_OK) return -1;
      } else
        time = 0;
    }
    sui_wtime_get_selected_tzdst(widget, &tzoffs, &dstr);
    if ( sui_time_convert_sec2tzdst( time, &wtim->edtime, tzoffs, dstr) < 0)
      return -1;

    /* correct to 99HOURS */
    if ( sui_test_flag( widget, SUDF_99HOURS)) {
      wtim->edtime.hour += (wtim->edtime.mday-1)*24;
      wtim->edtime.mday = 1;
      wtim->edtime.yday = 0;
    }

    sui_set_flag( widget, SUFL_EDITED);
    wtim->item_idx = 1;
    wtim->item_cnt = 0;

/* patch to highlighting only one part of widget */
    wtim->saved_flags = widget->flags;
    sui_clear_flag( widget, SUFL_HIGHLIGHTED);

    sui_obj_emit((sui_obj_t *)widget, SUI_SIG_EDIT_STARTED);
  }
  return 1;
}

/**
 * sui_time_stop_edit - Internal function for stopping editing
 * @widget: pointer to widget
 * @confirmed: new value is confirmed and will be saved to dinfo
 * The function stops editing and clears EDITED flag
 */
int sui_time_stop_edit( sui_widget_t *widget, int confirmed)
{
  suiw_time_t *wtim = sui_timwdg( widget);
  sui_event_t e;

   ul_logdeb("sui_time_stop_edit: initiated\n");

  if ( !sui_test_flag( widget, SUFL_EDITED)) return 0;

  e.what = SUEV_COMMAND;
  e.message.ptr = widget;

  if (( confirmed && !sui_test_flag( widget, SUDF_ONLINEEDIT)) ||
      ( !confirmed && sui_test_flag( widget, SUDF_ONLINEEDIT))) {
    unsigned long time;
    long tzoffs;
    sui_time_dst_rule_t *dstr;
    sui_wtime_get_selected_tzdst(widget, &tzoffs, &dstr);
    if ( sui_time_convert_tzdst2sec( &wtim->edtime, &time, tzoffs, dstr) < 0) {
      ul_logdeb("sui_time_stop_edit: conversion failed\n");
      e.message.command = SUCM_ERR_WRVAL;
      sui_put_event( &e); /* SUCM_ERROR_WRVAL */
      return 0;
    } else  if( sui_wr_ulong( wtim->data, 0, &time) != SUI_RET_OK) {
      ul_logdeb("sui_time_stop_edit: write failed\n");
      e.message.command = SUCM_ERR_WRVAL;
      sui_put_event( &e); /* SUCM_ERROR_WRVAL */
      return 0;
    }
  }
  if ( confirmed)
    e.message.command = SUCM_CHANGED;
  else
    e.message.command = SUCM_CHANGE;
  sui_put_event( &e);
//ul_loginf("Time was set to 0x%08lX.\n",time);

  sui_clear_flag( widget, SUFL_EDITED);
  wtim->item_idx = 1; //0;

/* patch to highlighting only one part of widget */
  widget->flags = (widget->flags & ~SUDF_EDIT_CHANGE_MASK) |
      ( wtim->saved_flags & SUDF_EDIT_CHANGE_MASK);

  sui_obj_emit((sui_obj_t *)widget, SUI_SIG_EDIT_STOPPED);

  return 1;
}

 
/******************************************************************************
 * Time basic functions
 ******************************************************************************/
/**
 * sui_time_get_size - Return time content size
 * @dc: Pointer to device context.
 * @widget: Pointer to widget.
 * @point: pointer to point structure
 * File: sui_time.c
 */
static inline
int sui_wtime_get_size( sui_dc_t *dc, sui_widget_t *widget, sui_point_t *point)
{
  sui_dcfont_t *pdcf;
  suiw_time_t *wtim;
  sui_textdims_t tsize;
  utf8 *strtime;

  if ( !point || !widget || !widget->style || !widget->style->fonti) return -1;

  wtim = sui_timwdg( widget);
  pdcf = sui_font_prepare( dc, widget->style->fonti->font, widget->style->fonti->size);

  if (( strtime = sui_time_transform_time( widget, 0, NULL)) == NULL)
    return -1;

  sui_text_get_size( dc, pdcf, &tsize, -1, strtime, wtim->align);
  point->x = tsize.w;
  point->y = tsize.h;
  sui_utf8_dec_refcnt( strtime);
  return 1;
}

/**
 * sui_time_auto_size - Recount widget size 
 * @dc: pointer to device context
 * @widget: pointer to widget
 * The function can be called when the widget frame and background are drawn and 
 * the widget has set SUFL_AUTORESIZE flag.
 */

static inline
void sui_wtime_auto_size( sui_dc_t *dc, sui_widget_t *widget)
{
    sui_point_t size;
    sui_widget_compute_size( dc, widget, &size);
    widget->place.w = size.x; widget->place.h = size.y;
}


/**
 * sui_time_init - Reaction to INIT time event
 * @widget: Pointer to time widget.
 * @event: Pointer to INIT event.
 *
 * The function initializes the time widget.
 *
 * Return Value: The function does not return a value.
 * File: sui_time.c
 */
static inline
void sui_wtime_init( sui_widget_t *widget, sui_dc_t *dc)
{
  sui_timwdg( widget)->align |= SUAL_INSIDE;
  sui_timwdg( widget)->item_idx = 1; //0;
  sui_timwdg( widget)->maxidx = -1;

  if ( widget->place.w < 0 || widget->place.h<0) {
    sui_point_t size;
    sui_widget_compute_size( dc, widget, &size);
    if ( widget->place.w < 0) widget->place.w = size.x;
    if ( widget->place.h < 0) widget->place.h = size.y;
  }
}


/**
 * sui_time_draw - Reaction to DRAW time event
 * @dc: Pointer to device context.
 * @widget: Pointer to the time widget.
 *
 * The function draws the time widget.
 *
 * Return Value: The function does not return a value.
 * File: sui_time.c
 */
//inline
int sui_wtime_draw( sui_dc_t *dc, sui_widget_t *widget)
{
  sui_dcfont_t *pdcf;
  suiw_time_t *wtim = sui_timwdg( widget);
  utf8 *strtime;
//  sui_coordinate_t tx = 0, ty = 0;
  sui_textdims_t textsize;
  sui_point_t    box;
  sui_point_t    origin;
  sui_point_t    tsize;
  int            fitem, sitem = 0; /* position of text in edit mode */

  if ( !widget->style || !widget->style->fonti)
    return -1;

  if (( strtime = sui_time_transform_time( widget, wtim->item_idx,
      &fitem)) == NULL)
    return -1;

  if ( wtim->item_size) {
    sitem = wtim->item_size;
  }

  pdcf = sui_font_prepare( dc, widget->style->fonti->font, widget->style->fonti->size);

  sui_font_set( dc, pdcf);

  box.x = widget->place.w; box.y = widget->place.h;
  sui_text_get_size( dc, pdcf, &textsize, -1, strtime, wtim->align);
  tsize.x = textsize.w; tsize.y = textsize.h;
  sui_align_object( &box, &origin, &tsize, textsize.b, widget->style, wtim->align);

  if (sui_test_flag( widget, SUFL_EDITED)) { // && (wtim->item_idx > 0)) { /* select item in change */
    int lstr, old;
    //sui_coordinate_t pw, ph;
    utf8char *chstr;
/* draw background, if widget needs it */
    if ( !sui_style_test_flag(widget->style, SUSFL_TRANSPARENT)) {
      sui_gdi_set_fgcolor( dc, widget->style->coltab->ctab[ SUC_BACKGROUND]);
      sui_draw_rect( dc, 0, 0, widget->place.w, widget->place.h, 0);
    }

    if ( wtim->align & SUAL_COLUMN) old = origin.y; else old = origin.x;
    chstr = sui_utf8_get_text( strtime);
    lstr = sui_utf8_length( strtime) - (fitem+sitem);
  /* draw text before selected part */
    sui_set_bgcolor(dc, widget, SUC_BACKGROUND);
    sui_set_fgcolor(dc, widget, SUC_ITEM);
    sui_draw_text( dc, origin.x, origin.y, fitem, (utf8 *) chstr, wtim->align);
    sui_text_get_size( dc, pdcf, &textsize,
          fitem, (utf8 *) chstr, wtim->align);
    if ( wtim->align & SUAL_COLUMN) origin.y += textsize.h;
    else origin.x += textsize.w;
    chstr += sui_utf8_count_bytes( chstr, fitem);
  /* draw selected part */
    sui_set_widget_bgcolor(dc, widget, SUC_BACKGROUND);
    sui_set_widget_fgcolor(dc, widget, SUC_ITEM);

    sui_draw_text( dc, origin.x, origin.y, sitem, (utf8 *) chstr, wtim->align);
    sui_text_get_size( dc, pdcf, &textsize,
          sitem, (utf8 *) chstr, wtim->align);
    if ( wtim->align & SUAL_COLUMN) origin.y += textsize.h;
    else origin.x += textsize.w;
    chstr += sui_utf8_count_bytes( chstr, sitem);
  /* draw text after selected part */
    sui_set_bgcolor(dc, widget, SUC_BACKGROUND);
    sui_set_fgcolor(dc, widget, SUC_ITEM);
    sui_draw_text( dc, origin.x, origin.y, lstr, (utf8 *) chstr, wtim->align);
/* draw frame as a indicator of BadValue mode */
    if (sui_test_flag(widget, SUDF_INTBVMODE)) { /* indicate Bad Value */
      sui_draw_rect(dc, 1, 1, widget->place.w-2, widget->place.h-2, 1);
    }
    
  } else {
    sui_set_widget_bgcolor( dc, widget, SUC_BACKGROUND);
    sui_set_widget_fgcolor( dc, widget, SUC_ITEM);
    sui_draw_text( dc, origin.x, origin.y, -1, strtime, wtim->align);
  }

  sui_utf8_dec_refcnt( strtime);
  return 0;
}

/*
 * enum sui_time_change_flags - internal flags for changing edited position
 */
enum sui_time_change_flags {
  SUI_TIME_CHNG_ONLYSAVE,
  SUI_TIME_CHNG_NEXTDIGIT,
  SUI_TIME_CHNG_NEXTITEM,
  SUI_TIME_CHNG_PREVDIGIT,
  SUI_TIME_CHNG_PREVITEM,
};

/**
 * sui_time_change_item - internal function for changing 
 * The function returns zero as success and a negative value otherwise
 */
int sui_time_change_item( sui_widget_t *widget, int chngflg)
{
  unsigned long val, vmin, vmax;
  int jump = 0;
  suiw_time_t *wtim = sui_timwdg( widget);

  switch (chngflg) {
    case SUI_TIME_CHNG_NEXTDIGIT:
      if ((wtim->item_cnt >= 0) && (wtim->item_cnt < wtim->item_size-1)) {
        wtim->item_cnt++;
        break;
      }
      /* else continue - change to the next item */
    case SUI_TIME_CHNG_NEXTITEM:
      if (wtim->item_idx < wtim->maxidx)
        wtim->item_idx++;
      else
        wtim->item_idx = 1;
      wtim->item_cnt = 0;
      jump = 1;
      break;
    case SUI_TIME_CHNG_PREVDIGIT:
      if (wtim->item_cnt > 0) {
        wtim->item_cnt--;
        break;
      }
    case SUI_TIME_CHNG_PREVITEM:
      if (wtim->item_idx > 1)
        wtim->item_idx--;
      else
        wtim->item_idx = wtim->maxidx;
      wtim->item_cnt = 0;
      jump = 1;
      break;
  }

  if ((chngflg == SUI_TIME_CHNG_ONLYSAVE) || (jump)) { /* only save */
    sui_time_elements_t tm;
    long tzoffs;
    sui_time_dst_rule_t *dstr;

    if ( sui_time_read_value( widget, &tm) != SUI_RET_OK) return -1;

    sui_wtime_get_selected_tzdst(widget, &tzoffs, &dstr);

    if ( sui_time_get_part( &tm, &val, &vmin, &vmax, wtim->item_type, tzoffs, dstr) == SUI_RET_OK) {
      if (( val < vmin) || ( val > vmax)) {
        if ( val < vmin) val = vmin;
        else val = vmax;
        sui_time_set_part( &tm, val, wtim->item_type, tzoffs, dstr);
        if ( sui_time_write_value( widget, &tm) != SUI_RET_OK) return -1;
//        return -1; // ???
      }
    }
  }
  return 0;
}

/*
 * sui_wtime_get_checked_time - read and check time
 * @widget: pointer to widget
 * @time: pointer to
 * @tmbuf: pointer to output buffer with corrected time
 * @edit: function is called for edit mode
 * The function reads time from widget (if time is NULL otherwise the $time is get)
 * and it checks if time is in number range
 */
int sui_wtime_get_checked_time(sui_widget_t *widget, sui_time_elements_t *time, sui_time_elements_t *tmbuf, int edit)
{
  suiw_time_t *wtim = sui_timwdg(widget);
  sui_time_elements_t tm;
  unsigned long value;
  int ret = SUI_RET_OK;
  long tzoffs;
  sui_time_dst_rule_t *dstr;

  if (!time) { /* no explicit time - read */
    if (sui_time_read_value(widget, &tm) != SUI_RET_OK) return SUI_RET_ERR;
    time = &tm;
  }

  sui_wtime_get_selected_tzdst(widget, &tzoffs, &dstr);

/* check currently edited item */
  {
    unsigned long vmin, vmax;
    if (((wtim->item_type & SUTE_EMASK)!=SUTE_NONE) &&
        (sui_time_get_part(time, &value, &vmin, &vmax, wtim->item_type, tzoffs, dstr) == SUI_RET_OK)) {
      if (value<vmin) {
        value = vmin;
        ret = SUI_RET_EOORN;
      } else if (value>vmax) {
        value = vmax;
        ret = SUI_RET_EOORP;
      }
      if (ret!=SUI_RET_OK) {
        sui_time_set_part(time, value, wtim->item_type, tzoffs, dstr);
        *tmbuf = *time;
        return ret;
      }
    }
  }

  if (sui_time_convert_tzdst2sec(time, &value, tzoffs, dstr) != SUI_RET_OK) return SUI_RET_ERR;
  if ((edit && sui_test_flag(widget,SUDF_MUSTBEVALID)) ||
      (!edit && sui_test_flag(widget,SUDF_CHECKLIMITS))) {
    if (value<wtim->data->minval) {
ul_logdeb("check - EOORN\n");
        if (sui_time_convert_sec2tzdst(wtim->data->minval, tmbuf, tzoffs, dstr) != SUI_RET_OK) return SUI_RET_ERR;
        ret = SUI_RET_EOORN;
    } else if (value>wtim->data->maxval) {
ul_logdeb("check - EOORP\n");
        sui_time_convert_sec2tzdst(wtim->data->maxval, tmbuf, tzoffs, dstr);
        ret = SUI_RET_EOORP;
    }
  } else {
ul_logdeb("check - OK\n");
  }
  return ret;
}


/**
 * sui_wtime_hkey - Reaction to KEY_DOWN, KEY_UP time event
 * @widget: Pointer to the time widget.
 * @event: Pointer to the event.
 *
 * The function response to keyboard events for the time widget.
 * Return Value: The function returns 1 if it takes to event.
 * File: sui_time.c
 */
int sui_wtime_hkey( sui_widget_t *widget, sui_event_t *event)
{
  suiw_time_t *wtim = sui_timwdg(widget);
  int key = event->keydown.keycode;
  unsigned long val, vmin, vmax;
  long tzoffs;
  sui_time_dst_rule_t *dstr;

  if ( !sui_test_flag( widget, SUFL_EDITEN) || ( wtim->maxidx < 1))  /* widget isn't editable */
    return 0;
  switch ( key) {
    case MWKEY_ENTER:
      if (sui_test_flag(widget, SUFL_EDITED)) { /* widget is edited - jump to next time item or end of editing */
        sui_time_elements_t tm;
        int testret;
        if (sui_test_flag(widget, SUDF_INTBVMODE))
          sui_clear_flag(widget, SUDF_INTBVMODE);
        testret = sui_wtime_get_checked_time(widget, NULL, &tm, 1);
        if (testret == SUI_RET_OK) { /* value is inside */
          sui_hevent_command( widget, SUCM_CONFIRM, widget, 0);
          return 1;
        } else if (testret==SUI_RET_EOORN || testret==SUI_RET_EOORP) { /* it is outside - correct it and set BVMODE flag -> change edit mode to BADVALue mode */
          if (sui_time_write_value( widget, &tm)!= SUI_RET_OK) /* change value to */
            return -1;
          sui_set_flag(widget, SUDF_INTBVMODE);
          wtim->item_idx = 1;
          wtim->item_cnt = 0;
          return 1;
        } else
          return 0;
      } else { /* start editing */
        sui_hevent_command( widget, SUCM_STARTEDIT, widget, 0);
      }
      return 1;
    case MWKEY_ESCAPE:
      if ( sui_test_flag( widget, SUFL_EDITED)) {
        if (sui_test_flag(widget, SUDF_INTBVMODE)) sui_clear_flag(widget, SUDF_INTBVMODE);
        sui_hevent_command( widget, SUCM_DISCARD, widget, 0);
        return 1;
      }
      break;
    case MWKEY_DOWN:
    case MWKEY_RIGHT:
      if ( sui_test_flag( widget, SUFL_EDITED)) {
        if (sui_test_flag(widget, SUDF_INTBVMODE)) sui_clear_flag(widget, SUDF_INTBVMODE);
        sui_time_change_item(widget, SUI_TIME_CHNG_NEXTITEM);
        return 1;
      }
      break;
    case MWKEY_UP:
    case MWKEY_LEFT:
      if ( sui_test_flag( widget, SUFL_EDITED)) {
        if (sui_test_flag(widget, SUDF_INTBVMODE)) sui_clear_flag(widget, SUDF_INTBVMODE);
        sui_time_change_item(widget, SUI_TIME_CHNG_PREVITEM);
        return 1;
      }
      break;
    default:
      if (( key >= '0' && key <= '9') || (key == MWKEY_BACKSPACE)) { /* only numerical items */
        sui_time_elements_t tm;
        int keyval;
        if ( !sui_test_flag( widget, SUFL_EDITED))
          sui_hevent_command( widget, SUCM_STARTEDIT, widget, 0);
        if (sui_test_flag(widget, SUDF_INTBVMODE)) sui_clear_flag(widget, SUDF_INTBVMODE);

        if ( key == MWKEY_BACKSPACE) keyval = 0;
        else keyval = key - '0';

        if ( sui_time_read_value( widget, &tm) != SUI_RET_OK) return -1;

        sui_wtime_get_selected_tzdst(widget, &tzoffs, &dstr);

        if ( sui_time_get_part(&tm, &val, &vmin, &vmax, wtim->item_type, tzoffs, dstr) != SUI_RET_OK)
          return -1;

        if ( wtim->item_type & SUTE_TEXT) {
          if (val < vmax) val++;
          else val = vmin;
          if (( sui_time_set_part( &tm, val, wtim->item_type, tzoffs, dstr) != SUI_RET_OK) ||
              ( sui_time_write_value( widget, &tm) != SUI_RET_OK))
            return -1;
          sui_put_command_event(SUCM_CHANGE, widget, 0);
        } else {
          long nnum;
          if (wtim->item_cnt==0) {
            nnum = keyval;
          } else {
            long mul = 1;
            if (key==MWKEY_BACKSPACE) {
              nnum = val / 10;
            } else {
              int idx = wtim->item_size;
              while (idx--) mul *= 10;
              nnum = (val * 10 + keyval) % mul;
            }
          }
          if (wtim->item_cnt==(wtim->item_size-1))
            val = (nnum <= vmax) ? ((nnum >= vmin) ? nnum : vmin) : vmax;
          else
            val = nnum;
  //ul_logdeb("tnum=%d -> %d\n",nnum,val);
          if (( sui_time_set_part( &tm, val, wtim->item_type, tzoffs, dstr) != SUI_RET_OK) ||
              ( sui_time_write_value( widget, &tm) != SUI_RET_OK))
            return -1;
          sui_put_command_event(SUCM_CHANGE, widget, 0);
          if ( key == MWKEY_BACKSPACE)
            sui_time_change_item(widget, SUI_TIME_CHNG_PREVITEM);
          else
            sui_time_change_item(widget, SUI_TIME_CHNG_NEXTDIGIT);
          return 1;
        }
      }
      break;
  }
  return 0;
}


/******************************************************************************
 * Time event handler
 ******************************************************************************/
/**
 * sui_wtime_hevent - Widget event handler for time widget
 * @widget: Pointer to the time widget.
 * @event: Pointer to the event.
 *
 * The function responses to event for time widget or calls
 * basic event handler common for all widget. 
 * Return Value: The function does not return a value.
 * File: sui_time.c
 */
void sui_wtime_hevent( sui_widget_t *widget, sui_event_t *event) 
{
  if (!widget || !event) return;
  switch( event->what) {
    case SUEV_DRAW:
    case SUEV_REDRAW:
      sui_widget_draw( event->draw.dc, widget, sui_wtime_draw);
      return;
    case SUEV_KDOWN:
      if ( sui_wtime_hkey( widget, event) > 0) {
        sui_clear_event( widget, event);
      }
      break;
    case SUEV_COMMAND:
      switch(event->message.command) {
        case SUCM_STARTEDIT:
          if ( sui_time_start_edit( widget, event->message.info)>0) {
            sui_redraw_request( widget);
            sui_clear_event( widget, event);
          }
          break;
        case SUCM_CONFIRM:
          if ( sui_time_stop_edit( widget, 1)>0) {
            sui_redraw_request( widget);
            sui_clear_event( widget, event);
          }
          break;
        case SUCM_DISCARD:
          if ( sui_time_stop_edit( widget, 0)>0) {
            sui_redraw_request( widget);
            sui_clear_event( widget, event);
          }
          break;

        case SUCM_FOCUSREL:
          sui_time_stop_edit( widget, 0);
          break;
        case SUCM_INIT:
          sui_widget_hevent(widget, event);
          sui_wtime_init( widget, (sui_dc_t *)event->message.ptr);
          sui_clear_event( widget, event);
          return;
        case SUCM_GETSIZE:
          if ( sui_wtime_get_size( (sui_dc_t *)event->message.ptr,
              widget, (sui_point_t *)event->message.info) > 0) {
            sui_clear_event( widget, event);
          }
          break;

        case SUCM_AUTORESIZE:
          sui_wtime_auto_size((sui_dc_t *)event->message.ptr,widget);
          return;

      }
  }
  if ( event->what != SUEV_NOTHING)
    sui_widget_hevent( widget,event);
}


/******************************************************************************
 * Time widget vmt & signal-slot mechanism
 ******************************************************************************/
int sui_wtime_vmt_init( sui_widget_t *pw, void *params)
{
  pw->data = sui_malloc( sizeof( suiw_time_t));
  if(!pw->data)
    return -1;

  pw->type = SUWT_TIME;
  pw->hevent = sui_wtime_hevent;

  return 0;
}

void sui_wtime_vmt_done( sui_widget_t *pw)
{
  if(pw->data) {
    suiw_time_t *wtim = pw->data;
  
    if ( wtim->data) sui_dinfo_dec_refcnt( wtim->data);
    wtim->data = NULL;
    if ( wtim->format) sui_utf8_dec_refcnt( wtim->format);
    wtim->format = NULL;
    if ( wtim->day_name) sui_dinfo_dec_refcnt( wtim->day_name);
    wtim->day_name = NULL;
    if ( wtim->month_name) sui_dinfo_dec_refcnt( wtim->month_name);
    wtim->month_name = NULL;
    if ( wtim->ampm_name) sui_dinfo_dec_refcnt( wtim->ampm_name);
    wtim->ampm_name = NULL;

    free( wtim);
    pw->data = NULL;
  }
  pw->type = SUWT_GENERIC;
  pw->hevent = sui_widget_hevent; // NULL;
}


SUI_CANBE_CONST
sui_obj_signal_tinfo_t sui_wtime_signal_tinfo[] = {
  { "editstarted", SUI_SIG_EDIT_STARTED, &sui_args_tinfo_void},
  { "editstopped", SUI_SIG_EDIT_STOPPED, &sui_args_tinfo_void},
};

SUI_CANBE_CONST
sui_obj_slot_tinfo_t sui_wtime_slot_tinfo[] = { 
  { "settext", SUI_SLOT_SETTEXT, &sui_args_tinfo_utf8,
    .target_kind = SUI_STGT_METHOD_OFFSET,
    .target.method_offset=UL_OFFSETOF( sui_wtext_vmt_t, settext)},
};

SUI_CANBE_CONST
sui_obj_tinfo_t sui_wtime_vmt_obj_tinfo = {
  .name = "sui_wtime",
  .obj_size = sizeof(sui_widget_t),
  .obj_align = UL_ALIGNOF(sui_widget_t),
  .signal_count = MY_ARRAY_SIZE(sui_wtime_signal_tinfo),
  .signal_tinfo = UL_CAST_UNQ1(sui_obj_signal_tinfo_t *, sui_wtime_signal_tinfo),
  .slot_count = MY_ARRAY_SIZE(sui_wtime_slot_tinfo),
  .slot_tinfo = UL_CAST_UNQ1(sui_obj_slot_tinfo_t *, sui_wtime_slot_tinfo)
};

sui_wtime_vmt_t sui_wtime_vmt_data = {
  .vmt_size = sizeof(sui_wtime_vmt_t),
  .vmt_refcnt = SUI_STATIC,
  .vmt_parent = &sui_widget_vmt_data,
  .vmt_init = sui_wtime_vmt_init,
  .vmt_done = sui_wtime_vmt_done,
  .vmt_obj_tinfo = UL_CAST_UNQ1(sui_obj_tinfo_t *, &sui_wtime_vmt_obj_tinfo),

  .settext = sui_wtime_set_format,

};

/******************************************************************************/
/**
 * sui_wtime - Create time widget
 * @aplace: Time widget position and size.
 * @atext: Time widget label in utf8 string.
 * @astyle: Skeleton widget style.
 * @aflags: Widget common and button special flags.
 * @adi: pointer to dinfo with time
 * @aialign: Align the time widget contents.
 *
 * The function creates the time widget.
 *
 * Return Value: The function returns pointer to created time widget.
 * File: sui_time.c
 */
sui_widget_t *sui_wtime( sui_rect_t *aplace, utf8 *alabel, sui_style_t *astyle, 
      sui_flags_t aflags, sui_dinfo_t *adi, utf8 *aformat, sui_align_t al)
{
  sui_widget_t *pw = sui_widget_create_new( &sui_wtime_vmt_data, aplace, alabel, astyle, aflags);
  if ( !pw) return NULL;

  {
    suiw_time_t *wtim = sui_timwdg(pw);

    if ( adi) {
      sui_dinfo_inc_refcnt( adi);
      wtim->data = adi;
    }
    if ( aformat) {
      sui_utf8_inc_refcnt( aformat);
      wtim->format = aformat;
    }
    wtim->align = al;
  }
  return pw;
}
