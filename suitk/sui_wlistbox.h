/* sui_wlistbox.h
 *
 * SUITK listbox widget.
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#ifndef _SUI_LISTBOX_WIDGET_
#define _SUI_LISTBOX_WIDGET_

#include <suitk/suitk.h>


/******************************************************************************
 * Temporary structure for list filtering and sorting
 ******************************************************************************/
typedef struct sui_sorting_item {
  utf8            *text;
  int              textlen;

//   long hash;

  sui_listcoord_t  index; /*the same index as in list */

//   gavl_node_t      idx_node;
//   gavl_node_t      sort_node;
} sui_sorting_item_t;

//typedef struct sui_sorting_list {
//  sui_listcoord_t        itemcnt;
//  gavl_cust_root_field_t idx_node;
//  gavl_cust_root_field_t sort_node;
//} sui_sorting_list_t;

// static inline int
// sui_sorting_item_index_cmp(const sui_listcoord_t *a, const sui_listcoord_t *b)
// {
//   if (*a>*b) return 1;
//   if (*a<*b) return -1;
//   return 0;
// }
//
// GAVL_CUST_NODE_INT_DEC( sui_filtering_gavl, sui_sorting_list_t, sui_sorting_item_t,
//                         sui_listcoord_t, idx_root, idx_node, index,
//                         sui_sorting_item_index_cmp)

/******************************************************************************
 * Listbox widget extension
 ******************************************************************************/
/**
 * struct suiw_listbox - Additional listbox widget structure
 * @align: Alignment for all items.
 * @imglib: Multi-image bitmap for drawing list item with image
 * @gap: Space between frame and items (before items).
 * @list: Pointer to list connected to the listbox widget.
 * @pos: Pointer to dinfo with current position in list.
 * @selected: Pointer to dinfo with selection array.
 * @first: Index of first visible item.
 * @lines: Count of visible rows.
 * @cols: Count of columns.
 * File: sui_list.h
 */
typedef struct suiw_listbox {
  struct sui_list *list;         /* pointer to list object */

  sui_dinfo_t     *pos;          /* current list item index (idx) in list (int) */
  sui_dinfo_t     *selected;     /* select array (unsigned char) */

  sui_image_t     *imglib;       /* pointer to image library (multiimage) */

  struct {
//    sui_list_item_t **list;        /* array of filtered items */
    sui_listcoord_t *items;        /* array of indexes to list */
    sui_listcoord_t  cnt;          /* number of filtered items */
//    sui_listcoord_t  max;          /* maximum number of filtered items (space in filter->list) */
//    utf8            *pattern;      /* used filter */
    sui_list_item_t pattern;       /* used filter - only text, index, imgidx and flags is used as indicator of fields which are used to filtering */
  } filter;

  sui_align_t      align;        /* items align inside line (left,center,right,block) */
  sui_coordinate_t gap;          /* gap between frame and lines */
  sui_listcoord_t  first;        /* first visible item - index 'idx' in list*/
  sui_listcoord_t  lines;        /* count of rows */
  sui_listcoord_t  cols;         /* count of columns */

  unsigned short  mk_mode; /* keybord mode for editing */

  sui_coordinate_t maxwidth;     /* !!! change this dumb variable - is it correct for more than one DC? */
  sui_coordinate_t maxheight;    /* !!! change this dumb variable - is it correct for more than one DC? */
} suiw_listbox_t;
  
#define sui_lstwdg( wdg)  ((suiw_listbox_t *)wdg->data)

/**
 * enum listbox_flags - Listbox widget special flags ['SULF_' prefix]
 * @SULF_SELECTEN: Items in listbox can be selected.
 * @SULF_MULTISELECT: More items than one can be selected.
 * @SULF_SELECT_FULL: Selected item is inverted between widget(column) bounds otherwise is inverted only item text.
 * @SULF_SEL_FRAME: Selected item is mark by frame.
 * @SULF_LOOP: Move in list is as in close loop.(Next item from last is first.)
 * @SULF_INDICATED: Selection is indicated.
 * @SULF_REVERSEKEYS: Listbox has swaped direction of arrow keys.
 * @SULF_CHANGE_FOCUS: for non-looping lists moves to out of bounds invoke change focus
 * File: sui_list.h
 */  
enum listbox_flags {
  SULF_NOSELECT           = 0x00000000, /* items cann't be selected - focused current item has frame */
  SULF_SELECT             = 0x00010000, /* items cann't be selected - focused current item is selected */
  SULF_SINGLE             = 0x00020000, /* one item can be selected - focused current item has frame */
  SULF_SINGLE_FRAME       = 0x00030000, /* one item can be selected - focused current item hasn't frame and unfocused has it */
  SULF_MULTI              = 0x00040000, /* more than one line can be selected - with focus current item has frame */
  SULF_TYPE_MASK          = 0x00070000,

  SULF_LISTFRAME          = 0x00080000,

  SULF_LOOP               = 0x00100000, /* move in list as in close loop */
  SULF_REVERSEKEYS        = 0x00200000,
  SULF_SELECT_ARRAY       = 0x00400000, /* select array is in dinfo, otherwise it's only in the list */
  SULF_SHOW_HIDDEN        = 0x00800000, /* show all items (also hidden) */
  SULF_SORTED             = 0x02000000, // list is sorted */
  SULF_CHANGE_FOCUS       = 0x04000000, /* for non-looping lists moves to out of bounds invoke change focus */
  SULF_WITH_ICON          = 0x08000000,
  SULF_FILTERING          = 0x10000000, /* pressing key with letter will add this letter to the filter with this flag */
  SULF_MOBILKBD           = 0x20000000, /* numeric keyboard behaves as mobil keyboard */
};

enum listbox_pattern_flags {
  SULF_PAT_FLAGS   = 0x10,
//  SULF_PAT_IDX    = 0x08,
  SULF_PAT_USERIDX = 0x20,
  SULF_PAT_IMGIDX  = 0x40,

  SULF_PAT_FILTER_IS_USED = 0x80,
};

/******************************************************************************
 * List slot functions
 ******************************************************************************/
int sui_wlistbox_update(sui_widget_t *widget, int index);
int sui_wlistbox_set_filter( sui_widget_t *widget, utf8 *patern);

//int sui_wlistbox_set_list( sui_widget_t *widget, sui_list_t *list);
int sui_wlistbox_get_text( sui_widget_t *widget, sui_dinfo_t *diout);

sui_list_item_t *sui_wlistbox_get_current_item(sui_widget_t *widget);
int sui_wlistbox_set_current_item(sui_widget_t *widget, sui_list_item_t *item);

/******************************************************************************
 * Listbox widget vmt & signal-slot mechanism
 ******************************************************************************/
typedef sui_widget_t sui_wlistbox_t;

#define sui_wlistbox_vmt_fields(new_type, parent_type) \
  sui_widget_vmt_fields( new_type, parent_type) \
  int (*update)(parent_type##_t *self, int index); \
  int (*setfilter)( parent_type##_t *self, utf8 *patern);

typedef struct sui_wlistbox_vmt {
	sui_wlistbox_vmt_fields(sui_wlistbox, sui_widget)
} sui_wlistbox_vmt_t;

static inline sui_wlistbox_vmt_t *
sui_wlistbox_vmt(sui_widget_t *self)
{
	return (sui_wlistbox_vmt_t*)(self->base.vmt);
}

extern sui_wlistbox_vmt_t sui_wlistbox_vmt_data;

/******************************************************************************
 * Listbox widget basic functions
 ******************************************************************************/
sui_widget_t *sui_wlistbox( sui_rect_t *aplace, utf8 *alabel, sui_style_t *astyle, 
              sui_flags_t aflags, struct sui_list *alist, sui_dinfo_t *apos, 
              sui_dinfo_t *asarr, sui_listcoord_t alines, sui_listcoord_t acols,
              sui_align_t aalign, sui_coordinate_t axgap);

void sui_wlistbox_set_empty_list_string( utf8 *newstring);

#endif /* _SUI_LISTBOX_WIDGET_ */
