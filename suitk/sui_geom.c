/* sui_widget.h
 *
 * SUITK geometry - rectangle overlap, distances etc
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#include <suitk/sui_geom.h>

sui_coordinate_t sui_rect_to_point_n1dist(const sui_rect_t *r, const sui_point_t *p)
{
  sui_coordinate_t dist = 0;

  if (p->x < r->x) dist += r->x - p->x;
  else if (p->x > r->x + r->w) dist += p->x - (r->x + r->w);

  if (p->y < r->y) dist += r->y - p->y;
  else if (p->y > r->y + r->h) dist += p->y - (r->y + r->h);

  return dist;
}
