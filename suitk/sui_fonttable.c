/* sui_fonttable.c
 *
 * SUITK font table - defines default built-in fonts table for SuiTk environment
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#include "sui_environment.h"

/******************************************************************************
 * Built-in fonts
 ******************************************************************************/
extern MWCFONT font_X5x7;
//extern MWCOREFONT systemfont_X5x7;

MWCOREFONT sui_fonts[] = {
  /* fontprocs, fontsize, fontwidth, fontrotation, fontattr, name, cfont */
  //systemfont_X5x7,
  SUI_BASE_FONT_ENTRY(0, 0, 0, 0, "X5x7", &font_X5x7),
  SUI_BASE_FONT_ENTRY(0, 0, 0, 0, "Verdana16", &font_wVerdana_16),
  SUI_BASE_FONT_ENTRY(0, 0, 0, 0, "Verdana18B", &font_wVerdana18_Bold),
  SUI_BASE_FONT_ENTRY(0, 0, 0, 0, "Tahoma16", &font_wTahoma_16),
  SUI_BASE_FONT_ENTRY(0, 0, 0, 0, "Tahoma18", &font_wTahoma_18),
  SUI_BASE_FONT_ENTRY(0, 0, 0, 0, "Tahoma40", &font_wTahoma40),
  SUI_BASE_FONT_ENTRY(0, 0, 0, 0, "Tahoma44", &font_wTahoma_44),
  SUI_BASE_FONT_ENTRY(0, 0, 0, 0, "Tahoma88", &font_wTahoma_88),
  SUI_BASE_FONT_ENTRY(0, 0, 0, 0, "Arial90", &font_Arial90),
  SUI_BASE_FONT_ENTRY(0, 0, 0, 0, "Arial100", &font_Arial100),
  SUI_BASE_FONT_ENTRY(0, 0, 0, 0, "Arial105", &font_wArial105),
  SUI_COMP_FONT_ENTRY(0, 0, 0, 0, "tahoma", (PMWCFONT) font_tahoma),
  SUI_COMP_FONT_ENTRY(0, 0, 0, 0, "Tahoma28", (PMWCFONT) font_wTahoma_28),  
  SUI_COMP_FONT_ENTRY(0, 0, 0, 0, "wT16comp", (PMWCFONT) wT16comp),
  SUI_COMP_FONT_ENTRY(0, 0, 0, 0, "wAMS18", (PMWCFONT) font_wArialMS_18),
  { NULL, 0, 0, 0}
};
