/* sui_wgraph.h
 *
 * SUITK graph widgets.
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2008 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#ifndef _SUI_GRAPH_WIDGET_
#define _SUI_GRAPH_WIDGET_

#include "suitk.h"

/**
 * struct sui_graph_dataset - description of one data set for all graph widgets
 * @flags: specific flags which describe graph data
 * @data: pointer to dinfo structure (array of data)
 * @format: pointer to finfo structure
 * @offset: offset of data first item from origin if data-set isn't connected to other axis in another data-set (XY graph)
 * @step: value of step between data items in the data-set if data-set isn't connected to other axis (XY graph)
 * @align: alignment of data values if they are drawn
 * @font: pointer to font used for drawing values in the graph
 * @label: pointer to utf8 with data label
 * @color: data set color
 * @width: width of line
 */
typedef struct sui_graph_dataset {
  unsigned long     flags;

  sui_dinfo_t      *data;
  sui_finfo_t      *format;

  long              offset;
  long              step;
  /* displaying attributes */
  sui_align_t       align;
  sui_font_info_t  *font;

  sui_utf8_t       *label;
  sui_color_t       color;
  sui_coordinate_t  width;
} sui_graph_dataset_t;

/**
 * enum sui_graph_dataset_flags - flags describe usage of graph data set (SUGRF_LN_xxx)
 * @SUGDS_AXIS_Y: data will be used as a Y data
 * @SUGDS_AXIS_X: data will be used as a X data
 * @SUGDS_TYPE_ABS: data are absolute values from origin
 * @SUGDS_TYPE_INC: all data are increments from the previous one (the first one is counted from origin)
 * @SUGDS_TYPE_DIF: data are absolute values but they are used to compute difference against the previous one(the first one is computed agains origin)
 * @SUGDS_USE_COLOR: use it's own color
 * @SUGDS_SHOW_VALUES: draw values in the graph
 * @SUGDS_SHOW_AXIS: draw axis next to graph
 * @SUGDS_DRAW_NONE: don't draw any data
 * @SUGDS_DRAW_POINT: draw only points
 * @SUGDS_DRAW_LINE: draw only lines (direct connection between data)
 * @SUGDS_DRAW_DOTLINE: draw point connected with X-axis by line
 * @SUGDS_DRAW_STEP: draw steps between two consecutive points
 * @SUGDS_DRAW_BOX: draw box graph
 * @SUGDS_DRAW_OUTLINE: draw outlined box graph
 * @SUGDS_DRAW_OLFX: draw outlined box graph with part filled up to current X
 * @SUGDS_DRAW_OLFY: draw outlined box graph with part filled up to current Y
 * @SUGDS_DRAW_PIE: draw a pie graph
 * @SUGDS_DRAW_MASK: mask for selection of graph type
 */
enum sui_graph_dataset_flags {
  SUGDS_AXIS_Y      = 0x00000000,
  SUGDS_AXIS_X      = 0x00000001,
  SUGDS_AXIS_MASK   = 0x00000001,

  SUGDS_XYDATA      = 0x00000002, /* data-set is part of XY graph (data-set is set of Y values for the last (previous) X data-set */

  SUGDS_TYPE_ABS    = 0x00000000,
  SUGDS_TYPE_INC    = 0x00000004,
  SUGDS_TYPE_DIF    = 0x00000008,
  SUGDS_TYPE_MASK   = 0x0000000C,

  SUGDS_USE_COLOR   = 0x00000010, /* use it's own color */
  SUGDS_SHOW_VALUES = 0x00000020, /* draw values in the graph */
  SUGDS_SHOW_AXIS   = 0x00000040, /* draw axis next to graph */
  SUGDS_SHOW_AXVALS = 0x00000080, /* draw axis values */

  SUGDS_DRAW_NONE   = 0x00000000, /* don't draw any data */
  SUGDS_DRAW_POINT  = 0x00000100, /* draw only points */
  SUGDS_DRAW_LINE   = 0x00000200, /* draw only lines (direct connection between data) */
  SUGDS_DRAW_DOTLINE= 0x00000300, /* draw point connected with X-axis by line */
  SUGDS_DRAW_STEP   = 0x00000400, /* draw lines as steps from the previous point to the current step */
  SUGDS_DRAW_BOX    = 0x00000500, /* draw box graph */
  SUGDS_DRAW_OUTLINE= 0x00000600, /* draw outlined box graph */
  SUGDS_DRAW_OLFX   = 0x00000700, /* draw outlined box graph with part filled up to current X */
  SUGDS_DRAW_OLFY   = 0x00000800, /* draw outlined box graph with part filled up to current Y */
  SUGDS_DRAW_PIE    = 0x00000F00, /* draw a pie graph */
  SUGDS_DRAW_MASK   = 0x00000F00,

  SUGDS_CONTINUE    = 0x00001000, /* data-set is drawn with the next data-set (compounded graph for Y graph) */
};

/**
 * struct sui_graph_data - collection of graph data sets
 * @refcnt: structure reference counter
 * @numsets: number of data sets
 * @line: pointer to array of sets
 */
typedef struct sui_graph_data {
  sui_refcnt_t         refcnt;
  int                  capsets;
  int                  numsets;
  sui_graph_dataset_t *dset;
} sui_graph_data_t;

sui_graph_dataset_t *sui_graph_dset_create(sui_flags_t aflags, sui_dinfo_t *adata,
                                          sui_finfo_t *aformat, utf8 *alabel,
                                          sui_font_info_t *afont, sui_align_t aalign);
int sui_graph_dset_copy(sui_graph_dataset_t *to, sui_graph_dataset_t *from);
void sui_graph_dset_done(sui_graph_dataset_t *dset);
int sui_graph_dset_destroy(sui_graph_dataset_t *dset);

sui_refcnt_t sui_graph_data_inc_refcnt(sui_graph_data_t *gd);
sui_refcnt_t sui_graph_data_dec_refcnt(sui_graph_data_t *gd);
sui_graph_data_t *sui_graph_data_create(void);
void sui_graph_data_clear(sui_graph_data_t *gd);
int sui_graph_data_add_dset(sui_graph_data_t *gd, sui_graph_dataset_t *dset);
int sui_graph_data_del_dset(sui_graph_data_t *gd);

/******************************************************************************
 * GraphXY widget extension
 ******************************************************************************/
/**
 * struct suiw_graphxy - Additional structure for graph XY widget
 * @flags: specific flags for mask types and usage of posX, posY, rangeX, rangeY
 *
 * File: sui_graphxy.h
 */
typedef struct suiw_graphxy {
  sui_graph_data_t *data;

  sui_dinfo_t      *curx;
  sui_dinfo_t      *cury;

  sui_dinfo_t      *originx;
  sui_dinfo_t      *originy;
  sui_dinfo_t      *rangex;
  sui_dinfo_t      *rangey;

  sui_coordinate_t  gap;

  /* internal data for quicker drawing */
  sui_point_t gr_size; /* area for graph */
  sui_point_t gr_orig; /* keep some space for axes */
  sui_point_t gr_zero; /* zero in graph (in display units) */

  long              offsx;
  long              offsy;
  struct {
    long l;
    long r;
    long b;
    long t;
  } datawin;

} suiw_graphxy_t;

#define sui_gxywdg( wdg)  ((suiw_graphxy_t *)wdg->data)

/**
 * enum graphxy_flags - Graph XY widget special flags ['SUXF_' prefix]
 * @SUXF_AXIS_CHNG: normally (without this flag) X axis is horizontal and Y axis is vertical, with this flag X is vertical and Y is horizontal
 * @SUXF_AXIS_XREV: normally X axis grows from left to right; with this flag it will grow from right to left
 * @SUXF_AXIS_YREV: normally Y axis grows from bottom to top; with this flag it will grow from top to bottom
 * @SUXF_VAR_INDICES: all variables are indicies into data-sets, otherwise variables are values in the first X,Y data-set
 * @SUXF_RX_AUTO: range is counted from data
 * @SUXF_RX_FIXABS: use fixed absolute range - in values of the first X data
 * @SUXF_RX_FIXRELO: use fixed relative range from origin - in values of the first X data
 * @SUXF_RX_FIXRELC: use fixed relative range around current X
 * @SUXF_RX_MASK: mask for type of range X
 * @SUXF_RY_AUTO: range is counted from data
 * @SUXF_RY_FIXABS: use fixed absolute range - in values of the first Y data
 * @SUXF_RY_FIXRELO: use fixed relative range from origin - in values of the first Y data
 * @SUXF_RY_FIXRELC: use fixed relative range around current Y
 * @SUXF_RY_MASK: mask for type of range Y
 * @SUXF_DRAW_AXES: draw axes
 * @SUXF_KEYS_CHOFS: keys change internal offsets and no variables(origin)
 * File: sui_graphxy.h
 */
enum graphxy_flags {
  SUXF_AXIS_CHNG   = 0x00010000,  /* normally (without this flag) X axis is horizontal and Y axis is vertical, with this flag X is vertical and Y is horizontal */
  SUXF_AXIS_XREV   = 0x00020000,  /* normally X axis grows from left to right; with this flag it will grow from right to left */
  SUXF_AXIS_YREV   = 0x00040000,  /* normally Y axis grows from bottom to top; with this flag it will grow from top to bottom */

  SUXF_VAR_INDICES = 0x00080000,  /* all variables are indicies into data-sets, otherwise variables are values in the first X,Y data-set */

  SUXF_RX_AUTO     = 0x00000000, /* range is counted from data */
  SUXF_RX_FIXABS   = 0x00100000, /* use fixed absolute range - in values of the first X data */
  SUXF_RX_FIXRELO  = 0x00200000, /* use fixed relative range from origin - in values of the first X data */
  SUXF_RX_FIXRELC  = 0x00300000, /* use fixed relative range around current X */
  SUXF_RX_MASK     = 0x00300000, /* mask for type of range X */

  SUXF_RY_AUTO     = 0x00000000, /* range is counted from data */
  SUXF_RY_FIXABS   = 0x00400000, /* use fixed absolute range - in values of the first Y data */
  SUXF_RY_FIXRELO  = 0x00800000, /* use fixed relative range from origin - in values of the first Y data */
  SUXF_RY_FIXRELC  = 0x00C00000, /* use fixed relative range around current Y */
  SUXF_RY_MASK     = 0x00C00000, /* mask for type of range Y */

  SUXF_DRAW_AXES   = 0x01000000, /* draw axes (basic axes) */
  SUXF_DRAW_CURPOS = 0x02000000, /* draw cross-lines at current position */

  SUXF_KEYS_CHOFS  = 0x10000000, /* keys changes offsets and not variables */
};

/******************************************************************************
 * GraphXY slot functions
 ******************************************************************************/
//int sui_wbutton_toggle( sui_widget_t *widget);


/******************************************************************************
 * Basic widget assignment function
 ******************************************************************************/

/******************************************************************************
 * GraphXY widget vmt & signal-slot mechanism
 ******************************************************************************/
typedef sui_widget_t sui_wgraphxy_t;

#define sui_wgraphxy_vmt_fields(new_type, parent_type) \
	sui_widget_vmt_fields( new_type, parent_type)
//	int (*toggle)(parent_type##_t *self);
// functions have sui_widget_t* as first argument

typedef struct sui_wgraphxy_vmt {
  sui_wgraphxy_vmt_fields(sui_wgraphxy, sui_widget)
} sui_wgraphxy_vmt_t;

static inline sui_wgraphxy_vmt_t *
sui_wgraphxy_vmt(sui_widget_t *self)
{
  return (sui_wgraphxy_vmt_t*)(self->base.vmt);
}

extern sui_wgraphxy_vmt_t sui_wgraphxy_vmt_data;

/******************************************************************************
 * Graph XY widget basic functions
 ******************************************************************************/

sui_widget_t *sui_wgraphxy( sui_rect_t *aplace, utf8 *alabel, sui_style_t *astyle,
              unsigned long aflags /* TODO: all necessary arguments for graphXY */);


#endif /* _SUI_GRAPHXY_WIDGET_ */
