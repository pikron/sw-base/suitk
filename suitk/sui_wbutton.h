/* sui_wbutton.h
 *
 * SUITK Button widget.
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#ifndef _SUI_BUTTON_WIDGET_
#define _SUI_BUTTON_WIDGET_

#include <suitk/suitk.h>


/******************************************************************************
 * Button widget extension
 ******************************************************************************/
/**
 * struct suiw_button - Additional button widget structure
 * @size: Size of button item (in pixels).
 * @color: Color of button item(light,check,radio).
 * File: sui_button.h
 */
typedef struct suiw_button {
  int size;             /* size of item ( width, height) */
  sui_color_t color;    /* color of item ( light, radio, check) */
  sui_image_t *image;   /* item will be subtitute by image */
  sui_dinfo_t *di;	    /* pointer to dinfo with button state */
  long value;           /* value which is stored into dinfo when the button is pressed/checked/switch on and the flag SUBF_USEVALUE is set */
  int wid;
} suiw_button_t;

#define sui_btnwdg( wdg)  ((suiw_button_t *)wdg->data)

/**
 * enum button_flags - Button widget special flags ['SUBF_' prefix]
 * @SUBF_PUSH_BTN: Button widget is normal push button.
 * @SUBF_LIGHT_BTN: Button widget is push button with light.
 * @SUBF_RADIO_BTN: Button widget is radio button.
 * @SUBF_CHECK_BTN: Button widget is check button.
 * @SUBF_REPEAT: Button repeats up/down when held.
 * @SUBF_PUSHED: Button is pushed.
 * @SUBF_CHECKED: Button is checked (check,radio,light)
 * @SUBF_GROUPED: More buttons are grouped in current window (allmost always radio).
 * @SUBF_USEIMAGE: Button item will be substitute by image
 * @SUBF_USEVALUE: A widget value will be stored to an associated dinfo when the button will be pressed/checked/switched on
 * File: sui_button.h
 */
enum button_flags {
  /* switch between types of button */
  SUBF_PUSH_BTN           = 0x00000000, /* normal push button */
  SUBF_LIGHT_BTN          = 0x00010000, /* push button with light(highlight color) */
  SUBF_RADIO_BTN          = 0x00020000, /* radio button */
  SUBF_CHECK_BTN          = 0x00030000, /* check button */

  SUBF_BTNTYPEMASK        = 0x00030000, /* button type mask */

  SUBF_REPEAT             = 0x00100000, /* btn repeats when held */
  SUBF_PUSHED             = 0x00200000, /* btn is pushed */
  SUBF_CHECKED            = 0x00400000, /* btn is checked (need for checked, radio and light btns) */
  SUBF_GROUPED            = 0x00800000, /* btns (allmost always radio) are grouped in current group */

  SUBF_USEIMAGE           = 0x01000000, /* button item will be substitute by image */
  SUBF_USEVALUE           = 0x02000000, /* value of ID will be saved to an associated dinfo when the button will be pressed/checked/switched on */
  SUBF_FOLLOWVALUE        = 0x04000000, /* follow value read from dinfo */ 
  SUBF_INT_PRESSED        = 0x08000000,
};


/******************************************************************************
 * Button slot functions
 ******************************************************************************/
int sui_wbutton_toggle( sui_widget_t *widget);


/******************************************************************************
 * Button widget vmt & signal-slot mechanism
 ******************************************************************************/
typedef sui_widget_t sui_wbutton_t;

#define sui_wbutton_vmt_fields(new_type, parent_type) \
        sui_widget_vmt_fields( new_type, parent_type) \
        int (*toggle)(parent_type##_t *self);
// functions have sui_widget_t* as first argument

typedef struct sui_wbutton_vmt {
  sui_wbutton_vmt_fields(sui_wbutton, sui_widget)
} sui_wbutton_vmt_t;

static inline sui_wbutton_vmt_t *
sui_wbutton_vmt(sui_widget_t *self)
{
  return (sui_wbutton_vmt_t*)(self->base.vmt);
}

extern sui_wbutton_vmt_t sui_wbutton_vmt_data;

/******************************************************************************
 * Button widget basic functions
 ******************************************************************************/
sui_widget_t *sui_wbutton( sui_rect_t *aplace, utf8 *alabel, sui_style_t *astyle,
               sui_flags_t aflags, sui_coordinate_t asize, sui_color_t abicolor);

sui_widget_t *sui_wbutton_with_dinfo( sui_rect_t *aplace, utf8 *alabel,
               sui_style_t *astyle, sui_flags_t aflags, sui_coordinate_t asize,
               sui_color_t abicolor, sui_dinfo_t *adinfo);

#endif
