/* mymalloc.c
 *
 * Debug tool to check memory 
 * management easier then ccmalloc.
 * 
 * Roman Bartosinski, bartosr@centrum.cz
 * Version 0.1  (C)2002
 * Version 1 (C)2003 - add replacement of strdup fnc
 * Version 1.1 (C)2003 - global counters replaced into structure
 *                     - add higher level
 * Version 1.2 (C)2003 - add allocated memory maximum counter
 */

#include "mymalloc.h"
#include <string.h>
#include <stdio.h>
  
  #undef malloc
  #undef free
  #undef strdup
  #undef EndMemoryStatus

  #include <ul_log.h>

  UL_LOG_CUST(ulogd_mymalloc)

  
#if MOW_DEBUG_LEVEL > 0

#if MOW_DEBUG_LEVEL > 1
  MOW_save_profile_t *MyOwnMemoryProfileHead = NULL;
  MOW_save_profile_t *MyOwnMemoryProfileTail = NULL;
#endif

  /* Internal counters : malloc calls, free calls, allocated memory, freed memory */
  MOW_all_counters_t MyOwnCounter = {{0,0,0},{0,0,0}};
  /* Array of all malloc request */
  MOW_all_array_t *MOW_array_head = NULL;
  MOW_all_array_t *MOW_array_tail = NULL;
  long MOW_maxmem = 0;

  
#if MOW_DEBUG_LEVEL > 1
  void MyOwnAddMemoryProfile( MOW_all_counters_t *state)

  {
    if ( !MyOwnMemoryProfileHead) {
      MyOwnMemoryProfileHead = MyOwnMemoryProfileTail = malloc( sizeof( MOW_save_profile_t));
      if ( !MyOwnMemoryProfileHead) return;
    } else {
      MyOwnMemoryProfileTail->next = malloc( sizeof( MOW_save_profile_t));
      if ( !MyOwnMemoryProfileTail->next) return;
      MyOwnMemoryProfileTail = MyOwnMemoryProfileTail->next;
    }
    MyOwnMemoryProfileTail->sumcnt = state->malloc.cnt-state->free.cnt;
    MyOwnMemoryProfileTail->sumsize = state->malloc.size-state->free.size;
    MyOwnMemoryProfileTail->next = NULL;
  }
  void MyOwnDelFromBeginMemoryProfile( void)
  {
    MOW_save_profile_t *hlpnext;
    if ( !MyOwnMemoryProfileHead) return;
    hlpnext = MyOwnMemoryProfileHead->next;
    free( MyOwnMemoryProfileHead);
    MyOwnMemoryProfileHead = hlpnext;
  }

#endif

/* internal function for testing maximum allocated memory at the time */
void TestMaxMemoryAtTheTime( void)
{
  long dif = MyOwnCounter.malloc.size - MyOwnCounter.free.size;
  if ( dif > MOW_maxmem) MOW_maxmem = dif;
}

/**
 * MyOwnMalloc - memory allocation (replace 'malloc' function)
 * @size: Amount of memory to allocating.
 *
 * The function allocates size bytes of memory and
 * return pointer to first byte. Fnc updates
 * debug variables - count of allocation and
 * allocated amount of memory.
 * Return Value: pointer to allocated memory or NULL
 * File: mymalloc.c
 */
  void *MyOwnMalloc(size_t size) {
    void *ret;
    MOW_all_array_t *now;
    /* ul_logdeb("Malloc : %d bytes\n", size); */
    if ( !size) MyOwnCounter.malloc.dump++;
    MyOwnCounter.malloc.cnt++; MyOwnCounter.malloc.size += size;
    ret = malloc( size);
#if 0 // for memory debugging - place for breakpoint
    if ( (unsigned long)ret == 0x008124270) { // || (unsigned long)ret == 0x80ce770) {
      ul_logdeb("Debug memory...\n");
    }
#endif
    if ( ret) {
      now = malloc( sizeof( MOW_all_array_t));
      now->next = NULL;
      now->ptr = ret;
      now->size = size;
      now->status = 1;
      if ( MOW_array_head) {
        MOW_array_tail->next = now;
        MOW_array_tail = now;
      } else {
        MOW_array_head = MOW_array_tail = now;
      }
    } else {
      MyOwnCounter.malloc.err++;
    }
#if MOW_DEBUG_LEVEL > 1
    MyOwnAddMemoryProfile( &MyOwnCounter);
#endif
    TestMaxMemoryAtTheTime();
    return ret;
  }
  
/**
 * MyOwnFree - return memory to system (replace 'free' function)
 * @s: Pointer to allocated memory.
 *
 * The function releases allocated memory and
 * update global debug information.
 * In this version function really does not release
 * memory. All memory will be released in 
 * EndMemoryStatus function.
 * Return Value: The function does not return a value.
 * File: mymalloc.c
 */
  void MyOwnFree( void *s) {
    /* ul_logdeb("Free : 0x%X\n", s); */
    MOW_all_array_t *now = MOW_array_head;
    if ( !s) MyOwnCounter.free.dump++;
    if ( !now) { /* Warning - release memory, if anyone isn't allocated */
      MyOwnCounter.free.err++;
      return; 
    }
    while ( now->ptr != s) {
      if ( !now->next) {        /* Warning - release pointer, which wasn't allocated */
        MyOwnCounter.free.err++; /* bad pointer */
        free( s);               /* try release memory directly */
        return;
      }
      now = now->next;
    }
    MyOwnCounter.free.cnt++;
    MyOwnCounter.free.size += now->size;
    now->status--;
    /* no free - aby nealokoval stejnou pamet - to by davalo nespravny vysledky ... */
    /* free( s); */
#if MOW_DEBUG_LEVEL > 1
    MyOwnAddMemoryProfile( &MyOwnCounter);
#endif
    TestMaxMemoryAtTheTime();
  }

/**
 * MyOwnStrdup - memory allocation and string copy (replace 'strdup' function)
 * @string: pointer to string
 *
 */
  char *MyOwnStrdup(const char *string)
  {
    char *memory;
    if (!string) return(NULL);
    if ((memory = MyOwnMalloc(strlen(string) + 1)))
      return(strcpy(memory,string));
    return(NULL);
  }
  

/**
 * EndMemoryStatus - print debug informations
 * @withlist: If it's true - all memory allocation
 *            and deallocations will be printed.
 *
 * The function prints debug informations about
 * allocation and freeing memory.
 * In this version function really releases memory.
 * Return Value: The function does not return a value.
 * File: mymalloc.c
 */

  void EndMemoryStatus( int withlist) {
    ul_logdeb( "MOMalloc - End Status\n");
    ul_logdeb( "| malloc: Cnt= %8d , Size= %8d [malloc(0) : %4d ,m()=0: %4d]\n", MyOwnCounter.malloc.cnt, MyOwnCounter.malloc.size, MyOwnCounter.malloc.dump, MyOwnCounter.malloc.err);
    ul_logdeb( "| free  : Cnt= %8d , Size= %8d [free(NULL): %4d ,f(?) : %4d]\n", MyOwnCounter.free.cnt, MyOwnCounter.free.size, MyOwnCounter.free.dump, MyOwnCounter.free.err);
    ul_logdeb( "|   diff: Cnt= %8d , Size= %8d\n", MyOwnCounter.malloc.cnt - MyOwnCounter.free.cnt, MyOwnCounter.malloc.size - MyOwnCounter.free.size);
    ul_logdeb( "| ->->->  Maximal allocated memory at a time : %4lu B\n\n", MOW_maxmem);
    if ( withlist)
      ul_logdeb( "*\n* List of alloc ( ptr,size,status)\n");
    while ( MOW_array_head) {
      MOW_array_tail = MOW_array_head->next;
	  if ( withlist) {
        ul_logdeb( "* 0x%8lX | %8d | %2d\n", (unsigned long)(MOW_array_head->ptr), MOW_array_head->size, MOW_array_head->status);
#if 0
		if ( MOW_array_head->status) {
			ul_logdeb( "debug !!!\n");
		}
#endif
	  }
    /* free all alocated memory */
      free( MOW_array_head->ptr);
      free( MOW_array_head);
      MOW_array_head = MOW_array_tail;
    }
    if ( withlist)
      ul_logdeb( "* End of list\n*\n");
  #if MOW_DEBUG_LEVEL > 1
    if (withlist > 1)
      ul_logdeb( ">>> Profile list (step, cnt, size) <<<\n");
      i=1;
      while ( MyOwnMemoryProfileHead) {
        if (withlist > 1)
          ul_logdeb( "  %04d  %8d  %8d\n", i, MyOwnMemoryProfileHead->sumcnt, MyOwnMemoryProfileHead->sumsize);
        i++;
        MyOwnDelFromBeginMemoryProfile();
      }
    if (withlist > 1)
      ul_logdeb( ">>> End of profile list <<<\n*\n");
  #endif
  }


  #define free(x) MyOwnFree(x)
  #define malloc(x) MyOwnMalloc(x)
  #define strdup(x) MyOwnStrdup(x)



#endif
