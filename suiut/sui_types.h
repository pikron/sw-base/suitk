/* sui_types.h
 *
 * SUITK type system and its support.
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#ifndef _SUITK_TYPE_SYSTEM_
#define _SUITK_TYPE_SYSTEM_

#include "sui_comdefs.h"
#include <stdio.h>
#include <stdio.h>
#include <suiut/sui_objbase.h>
#include "sui_typebase.h"

#include "sui_typereg.h"

#ifdef __cplusplus
extern "C" {
#endif

/* FIXME: ??? define 'SUI_DINFO_WITH_EVC' HERE or somewhere else ??? */
#define SUI_DINFO_WITH_EVC


/******************************************************************************
 * Common macros
 ******************************************************************************/
#define MY_ARRAY_SIZE(array) (sizeof(array)/sizeof(array[0]))


/******************************************************************************
 * Common typedefs and structs
 ******************************************************************************/
/**
 * enum sui_return_codes - Common return values (mainly for data transfer functions)
 * @SUI_RET_OK: successful data transfer
 * @SUI_RET_ERR: unspecified error
 * @SUI_RET_NRDY: data source is not ready or destination is full
 * @SUI_RET_NCON: data target not connected
 * @SUI_RET_EPERM: read/write or other permission problem
 * @SUI_RET_ETYPE: value cannot be transformed to requested type
 * @SUI_RET_EINDX: index value is out of range or forbidden
 * @SUI_RET_EOORP: positive value out of range
 * @SUI_RET_EOORN: negative value out of range
 * @SUI_RET_ZERODIV:
 *
 * File: sui_types.h
 */
enum sui_return_codes {
  SUI_RET_OK    = 0,  /* successful data transfer */
  SUI_RET_ERR   = 1,  /* unspecified error */
  SUI_RET_NRDY  = 2,  /* data source is not ready or destination is full */
  SUI_RET_NCON  = 3,  /* data target not connected */
  SUI_RET_EPERM = 4,  /* read/write or other permission problem */
  SUI_RET_ETYPE = 5,  /* value cannot be transformed to requested type */
  SUI_RET_EINDX = 6,  /* index value is out of range or forbidden */
  SUI_RET_EOORP = 7,  /* positive value out of range */
  SUI_RET_EOORN = 8,  /* negative value out of range */
  
  SUI_RET_ZERODIV,
};

/******************************************************************************
 * Basic types
 ******************************************************************************/
/* flag type */
typedef unsigned long sui_flags_t;

/* list and table coordinate */
typedef int sui_listcoord_t;

/******************************************************************************
 * Basic SUITK types
 ******************************************************************************/
/**
 * enum sui_typeids - Identifiers of the complex suitk types (they continue after primary suitk types)
 * @SUI_TYPE_PROXY:
 * @SUI_TYPE_DBUFF:
 * @SUI_TYPE_REFCNT:
 * @SUI_TYPE_COORD:
 * @SUI_TYPE_COLOR:
 * @SUI_TYPE_ALIGN:
 * @SUI_TYPE_FLAGS:
 * @SUI_TYPE_TYPE:
 * @SUI_TYPE_ASCII:
 * @SUI_TYPE_POINT:
 * @SUI_TYPE_RECT:
 * @SUI_TYPE_COLORTABLE:
 * @SUI_TYPE_FONTINFO:
 * @SUI_TYPE_CHOICE:
 * @SUI_TYPE_CHOICETABLE:
 * @SUI_TYPE_FORMAT:
 * @SUI_TYPE_STYLE:
 * @SUI_TYPE_SUFFIX:
 * @SUI_TYPE_DINFO:
 * @SUI_TYPE_IMAGE:
 * @SUI_TYPE_KEY:
 * @SUI_TYPE_KEYTABLE:
 * @SUI_TYPE_LIST:
 * @SUI_TYPE_EVENT:
 * @SUI_TYPE_COLUMN:
 * @SUI_TYPE_TABLE:
 * @SUI_TYPE_GSET:
 * @SUI_TYPE_GDATA:
 * @SUI_TYPE_WIDGET:
 * @SUI_TYPE_WGROUP:
 * @SUI_TYPE_WBUTTON:
 * @SUI_TYPE_WBITMAP:
 * @SUI_TYPE_WLISTBOX:
 * @SUI_TYPE_WNUMBER:
 * @SUI_TYPE_WSCROLL:
 * @SUI_TYPE_WTEXT:
 * @SUI_TYPE_WTIME:
 * @SUI_TYPE_WTABLE:
 * @SUI_TYPE_WDYNTEXT:
 * @SUI_TYPE_DEFINE:
 * @SUI_TYPE_KEYFCN:
 * @SUI_TYPE_LINK:
 * @SUI_TYPE_VARF:
 * @SUI_TYPE_VARL:
 * @SUI_TYPE_SCENARIO:
 * @SUI_TYPE_CONDITION:
 * @SUI_TYPE_ACTIONFCN:
 * @SUI_TYPE_ACTION:
 * @SUI_TYPE_TRANSITION:
 * @SUI_TYPE_SSDIALOG:
 * @SUI_TYPE_STATEFCN:
 * @SUI_TYPE_STATE:
 * @SUI_TYPE_STATEEXT:
 * @SUI_TYPE_SUBSTATE:
 * @SUI_TYPE_APPLICATION:
 * @SUI_TYPE_NAMESPACE:
 * @SUI_TYPE_PRNFILTER: printing filter type
 * @SUI_TYPE_PRNLAYOUT: printing layout type
 * @SUI_TYPE_ARRAYSIZE:
 *
 * File: sui_types.h
 */

enum sui_typeids {
  SUI_TYPE_PROXY = SUI_TYPEBASE_NEXT_TYPE,

  SUI_TYPE_DBUFF,

  SUI_TYPE_REFCNT,
  SUI_TYPE_COORD, /* coordinate */
  SUI_TYPE_COLOR, /* color */
  SUI_TYPE_ALIGN, /* align */      // 20
//  SUI_TYPE_PIXEL, /* mwimagebits */
  SUI_TYPE_FLAGS,
  SUI_TYPE_TYPE,
//  SUI_TYPE_TINFO,
//  SUI_TYPE_FLOAT,
  SUI_TYPE_ASCII,
/* basic structures */
  SUI_TYPE_POINT,
  SUI_TYPE_RECT,
  SUI_TYPE_COLORTABLE,
  SUI_TYPE_FONTINFO,
  SUI_TYPE_CHOICE,
  SUI_TYPE_CHOICETABLE,
  SUI_TYPE_FORMAT,     // 30
  SUI_TYPE_STYLE,
  SUI_TYPE_SUFFIX,
  SUI_TYPE_DINFO,
  SUI_TYPE_IMAGE,
  SUI_TYPE_KEY,
  SUI_TYPE_KEYTABLE, // SUI_TYPE_KEYACT,
  SUI_TYPE_LIST,
  SUI_TYPE_EVENT,
  SUI_TYPE_COLUMN,
  SUI_TYPE_TABLE,

  SUI_TYPE_GSET,
  SUI_TYPE_GDATA,

  SUI_TYPE_WIDGET,
  SUI_TYPE_WGROUP,
  SUI_TYPE_WBUTTON,
  SUI_TYPE_WBITMAP,
  SUI_TYPE_WLISTBOX,
  SUI_TYPE_WNUMBER,
  SUI_TYPE_WSCROLL,
  SUI_TYPE_WTEXT,
  SUI_TYPE_WTIME,
  SUI_TYPE_WTABLE,
  SUI_TYPE_WDYNTEXT,
  SUI_TYPE_WGRAPHXY,

/* special type mainly for sxml */
  SUI_TYPE_DEFINE,
  SUI_TYPE_KEYFCN,
  SUI_TYPE_LINK,
  SUI_TYPE_VARF,
  SUI_TYPE_VARL,

  SUI_TYPE_SCENARIO,

  SUI_TYPE_CONDITION,
  SUI_TYPE_ACTIONFCN,
  SUI_TYPE_ACTION,

  SUI_TYPE_TRANSITION,
  SUI_TYPE_SSDIALOG,

  SUI_TYPE_STATEFCN,
  SUI_TYPE_STATE,
  SUI_TYPE_STATEEXT,
  SUI_TYPE_SUBSTATE,

  SUI_TYPE_APPLICATION,

  SUI_TYPE_NAMESPACE,

  SUI_TYPE_PRNFILTER,
  SUI_TYPE_PRNLAYOUT,

  SUI_TYPE_ARRAYSIZE,
};


/******************************************************************************
 * Types of widgets
 ******************************************************************************/
/**
 * enum widget_type - Widget types ['SUWT_' prefix]
 * @SUWT_GENERIC: Widget without defined specific type - generic widget.
 * @SUWT_GROUP: Group widget, more information is in the sui_wgroup structure.
 * @SUWT_BUTTON: Button widget, more information is in the sui_wbutton structure.
 * @SUWT_TEXT: Text widget, more information is in the sui_wtext structure.
 * @SUWT_NUMBER: Number widget, more information is in the sui_wnumber structure.
 * @SUWT_SCROLL: Scroll widget, more information is in the sui_wscroll structure.
 * @SUWT_LIST: List widget, more information is in the sui_list structure.
 * @SUWT_BITMAP: Bitmap widget, more information is in the sui_wbitmap structure.
 * @SUWT_TIME: Time widget, more information is in the sui_wtime structure.
 * @SUWT_TABLE: Table widget, more information is in the sui_wtable structure.
 * @SUWT_DYNTEXT: Dynamic text widget, more information is in the sui_wdyntext structure.
 *
 * File: sui_types.h
 */
enum widget_type {       /* This constants are used as masks too. */
  SUWT_GENERIC = 0, /* ??? not defined widget type (base widget) */
  SUWT_GROUP   = 1, /* frame,group,window */
  SUWT_BUTTON  = 2, /* push,check,radio,light button */
  SUWT_TEXT    = 3, /* text widget */
  SUWT_NUMBER  = 4, /* number widget */
  SUWT_SCROLL  = 5, /* scroll bar */
  SUWT_LISTBOX = 6, /* list box */
  SUWT_BITMAP  = 7, /* bitmap image */
  SUWT_TIME    = 8, /* time widget */
  SUWT_TABLE   = 9, /* table widget */
  SUWT_DYNTEXT = 10, /* dynamic text */
  SUWT_GRAPHXY = 11, /* graphXY widget */
//  SUWT_COMBO   = 12, /* combo box */
//  SUWT_USER    = 0x0100,
};



/******************************************************************************
 * Type functions
 ******************************************************************************/

sui_refcnt_t sui_object_dec_refcnt(void *buf, sui_typeid_t buf_type);

sui_refcnt_t sui_object_inc_refcnt(void *buf, sui_typeid_t buf_type);

int sui_typeid_size(sui_typeid_t type);

sui_typeid_t sui_typeid_buffer_type(sui_typeid_t in_type);

int sui_check_typecompat(sui_typeid_t in_type, sui_typeid_t out_type);

int sui_typeid_convert(sui_typeid_t outtype, void *outptr,
                       sui_typeid_t intype, void *inptr, int count);
int sui_typeid_copy(void *dstptr, void *srcptr, sui_typeid_t type);

/******************************************************************************
 * objects and VMTs
 ******************************************************************************/

extern SUI_CANBE_CONST_VMT sui_obj_vmt_t sui_proxy_vmt_data;
extern SUI_CANBE_CONST_VMT sui_obj_vmt_t sui_dbuff_vmt_data;
extern SUI_CANBE_CONST_VMT sui_obj_vmt_t sui_refcnt_vmt_data;
extern SUI_CANBE_CONST_VMT sui_obj_vmt_t sui_coord_vmt_data;
extern SUI_CANBE_CONST_VMT sui_obj_vmt_t sui_color_vmt_data;
extern SUI_CANBE_CONST_VMT sui_obj_vmt_t sui_align_vmt_data;
extern SUI_CANBE_CONST_VMT sui_obj_vmt_t sui_flags_vmt_data;
extern SUI_CANBE_CONST_VMT sui_obj_vmt_t sui_type_vmt_data;
extern SUI_CANBE_CONST_VMT sui_obj_vmt_t sui_ascii_vmt_data;

/* basic structures */
extern SUI_CANBE_CONST_VMT sui_obj_vmt_t sui_point_vmt_data;
extern SUI_CANBE_CONST_VMT sui_obj_vmt_t sui_rect_vmt_data;
extern SUI_CANBE_CONST_VMT sui_obj_vmt_t sui_colortable_vmt_data;
extern SUI_CANBE_CONST_VMT sui_obj_vmt_t sui_fontinfo_vmt_data;
extern SUI_CANBE_CONST_VMT sui_obj_vmt_t sui_choice_vmt_data;
extern SUI_CANBE_CONST_VMT sui_obj_vmt_t sui_choicetable_vmt_data;
extern SUI_CANBE_CONST_VMT sui_obj_vmt_t sui_format_vmt_data;
extern SUI_CANBE_CONST_VMT sui_obj_vmt_t sui_style_vmt_data;
extern SUI_CANBE_CONST_VMT sui_obj_vmt_t sui_suffix_vmt_data;
extern SUI_CANBE_CONST_VMT sui_obj_vmt_t sui_image_vmt_data;
extern SUI_CANBE_CONST_VMT sui_obj_vmt_t sui_keyfcn_vmt_data;
extern SUI_CANBE_CONST_VMT sui_obj_vmt_t sui_key_vmt_data;
extern SUI_CANBE_CONST_VMT sui_obj_vmt_t sui_keytable_vmt_data;
//extern sui_obj_vmt_t sui_list_vmt_data;
extern SUI_CANBE_CONST_VMT sui_obj_vmt_t sui_event_vmt_data;

extern SUI_CANBE_CONST_VMT sui_obj_vmt_t sui_column_vmt_data;
extern SUI_CANBE_CONST_VMT sui_obj_vmt_t sui_table_vmt_data;

extern SUI_CANBE_CONST_VMT sui_obj_vmt_t sui_gset_vmt_data;
extern SUI_CANBE_CONST_VMT sui_obj_vmt_t sui_gdata_vmt_data;

#ifndef _SUI_IN_DINFOVMT_C
/* this is not fully correct for dinfo, because code uses sui_dinfo_vmt_t internally */
extern sui_obj_vmt_t sui_dinfo_vmt_data;
#endif


/* special type mainly for sxml */
extern sui_obj_vmt_t sui_function_vmt_data;

extern SUI_CANBE_CONST_VMT sui_obj_vmt_t sui_define_vmt_data;
extern SUI_CANBE_CONST_VMT sui_obj_vmt_t sui_link_vmt_data;
extern SUI_CANBE_CONST_VMT sui_obj_vmt_t sui_scenario_vmt_data;

extern SUI_CANBE_CONST_VMT sui_obj_vmt_t sui_prnfilter_vmt_data;
extern SUI_CANBE_CONST_VMT sui_obj_vmt_t sui_prnlayout_vmt_data;

int sui_type_register_default_types(void);

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /* _SUITK_TYPE_SYSTEM_ */
