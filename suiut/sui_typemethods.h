/* sui_typemethods.h
 *
 * SUIUT methods reusable for base types.
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#ifndef _SUI_TYPEMETHODS_H_
#define _SUI_TYPEMETHODS_H_

#include "sui_objbase.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Methods of the base object type, this is regular class type with VMT and instance data support */
sui_obj_t *sui_obj_vmt_new(struct sui_obj_vmt *vmt);
void sui_obj_vmt_deallocate(sui_obj_t *self);
void sui_obj_vmt_done(sui_obj_t *self);
sui_obj_signal_tinfo_t * sui_obj_vmt_find_signal_tinfo_byid(sui_obj_t *self, sui_signalid_t signalid);
struct sui_obj_signal * sui_obj_vmt_find_signal_byid(sui_obj_t *self, sui_signalid_t signalid);
sui_obj_slot_tinfo_t * sui_obj_vmt_find_slot_tinfo_byid(sui_obj_t *self, sui_slotid_t slotid);
struct sui_obj_slot * sui_obj_vmt_find_slot_byid(sui_obj_t *self, sui_slotid_t slotid);
sui_signalid_t sui_obj_vmt_find_signalid_byname(sui_obj_t *self, char *name, int name_len);
sui_slotid_t sui_obj_vmt_find_slotid_byname(sui_obj_t *self, char *name, int name_len);
sui_args_tinfo_t *sui_obj_vmt_signal_args_tinfo(sui_obj_t *self, sui_signalid_t sigid);
sui_args_tinfo_t *sui_obj_vmt_slot_args_tinfo(sui_obj_t *self, sui_slotid_t slotid);

/* primary data types without VMT table support */
sui_obj_t *sui_typeprimary_vmt_new(struct sui_obj_vmt *vmt);
void sui_typeprimary_vmt_deallocate(sui_obj_t *self);
void sui_typeprimary_vmt_done(sui_obj_t *self);
sui_obj_signal_tinfo_t * sui_typeprimary_vmt_find_signal_tinfo_byid(sui_obj_t *self, sui_signalid_t signalid);
struct sui_obj_signal * sui_typeprimary_vmt_find_signal_byid(sui_obj_t *self, sui_signalid_t signalid);
sui_obj_slot_tinfo_t * sui_typeprimary_vmt_find_slot_tinfo_byid(sui_obj_t *self, sui_slotid_t slotid);
struct sui_obj_slot * sui_typeprimary_vmt_find_slot_byid(sui_obj_t *self, sui_slotid_t slotid);
sui_signalid_t sui_typeprimary_vmt_find_signalid_byname(sui_obj_t *self, char *name, int name_len);
sui_slotid_t sui_typeprimary_vmt_find_slotid_byname(sui_obj_t *self, char *name, int name_len);
sui_args_tinfo_t *sui_typeprimary_vmt_signal_args_tinfo(sui_obj_t *self, sui_signalid_t sigid);
sui_args_tinfo_t *sui_typeprimary_vmt_slot_args_tinfo(sui_obj_t *self, sui_slotid_t slotid);
sui_refcnt_t sui_typeprimary_inc_refcnt(sui_obj_t *self);
sui_refcnt_t sui_typeprimary_dec_refcnt(sui_obj_t *self);

#define SUI_TYPE_DECLARE_PRIMARY_EX(a_name, a_typeid, a_C_type, a_common, a_init, a_done) \
\
SUI_CANBE_CONST \
sui_obj_tinfo_t sui_##a_name##_vmt_obj_tinfo = { \
  .name = #a_name, \
  .obj_size = sizeof(a_C_type), \
  .obj_align = UL_ALIGNOF(a_C_type), \
  .flags = SUI_OTINFO_NOVMT | SUI_OTINFO_NOREFCNT, \
  .signal_count = 0, \
  .signal_tinfo = NULL, \
  .slot_count = 0, \
  .slot_tinfo = NULL \
}; \
\
SUI_CANBE_CONST_VMT \
sui_obj_vmt_t sui_##a_name##_vmt_data = { \
  .vmt_size = sizeof(sui_obj_vmt_t), \
  .vmt_typeid = a_typeid, \
  .vmt_refcnt = SUI_STATIC, \
  .vmt_parent = NULL, \
  .vmt_init = a_init, \
  .vmt_done = a_done, \
  .vmt_obj_tinfo = UL_CAST_UNQ1_NULL_ALLOWED(sui_obj_tinfo_t *, &sui_##a_name##_vmt_obj_tinfo), \
  .vmt_new = sui_typeprimary_vmt_new, \
  .vmt_deallocate = sui_typeprimary_vmt_deallocate, \
  .vmt_find_signal_byid = sui_typeprimary_vmt_find_signal_byid, \
  .vmt_find_slot_byid = sui_typeprimary_vmt_find_slot_byid, \
  .vmt_find_signalid_byname = sui_typeprimary_vmt_find_signalid_byname, \
  .vmt_find_slotid_byname = sui_typeprimary_vmt_find_slotid_byname, \
  .vmt_signal_args_tinfo = sui_typeprimary_vmt_signal_args_tinfo, \
  .vmt_slot_args_tinfo = sui_typeprimary_vmt_slot_args_tinfo, \
  .vmt_common_type = UL_CAST_UNQ1_NULL_ALLOWED(sui_obj_vmt_t *, (a_common)), \
\
  .inc_refcnt = sui_typeprimary_inc_refcnt, \
  .dec_refcnt = sui_typeprimary_dec_refcnt \
};


#define SUI_TYPE_DECLARE_PRIMARY(a_name, a_typeid, a_C_type, a_common) \
        SUI_TYPE_DECLARE_PRIMARY_EX(a_name, a_typeid, a_C_type, a_common, NULL, NULL)

#define SUI_TYPE_DECLARE_SIMPLE_EX(a_name, a_typeid, a_C_type, a_common, a_init, a_done) \
        SUI_TYPE_DECLARE_PRIMARY_EX(a_name, a_typeid, a_C_type, a_common, a_init, a_done)

#define SUI_TYPE_DECLARE_SIMPLE(a_name, a_typeid, a_C_type, a_common) \
        SUI_TYPE_DECLARE_SIMPLE_EX(a_name, a_typeid, a_C_type, a_common, NULL, NULL)


#define SUI_TYPE_DECLARE_REFCNT_EX(a_name, a_typeid, a_C_type, a_common, a_inc_refcnt, a_dec_refcnt, a_init, a_done) \
\
SUI_CANBE_CONST \
sui_obj_tinfo_t sui_##a_name##_vmt_obj_tinfo = { \
  .name = #a_name, \
  .obj_size = sizeof(a_C_type), \
  .obj_align = UL_ALIGNOF(a_C_type), \
  .flags = SUI_OTINFO_NOVMT, \
  .signal_count = 0, \
  .signal_tinfo = NULL, \
  .slot_count = 0, \
  .slot_tinfo = NULL \
}; \
\
SUI_CANBE_CONST_VMT \
sui_obj_vmt_t sui_##a_name##_vmt_data = { \
  .vmt_size = sizeof(sui_obj_vmt_t), \
  .vmt_typeid = a_typeid, \
  .vmt_refcnt = SUI_STATIC, \
  .vmt_parent = NULL, \
  .vmt_init = a_init, \
  .vmt_done = a_done, \
  .vmt_obj_tinfo = UL_CAST_UNQ1_NULL_ALLOWED(sui_obj_tinfo_t *, &sui_##a_name##_vmt_obj_tinfo), \
  .vmt_new = sui_typeprimary_vmt_new, \
  .vmt_deallocate = sui_typeprimary_vmt_deallocate, \
  .vmt_find_signal_byid = sui_typeprimary_vmt_find_signal_byid, \
  .vmt_find_slot_byid = sui_typeprimary_vmt_find_slot_byid, \
  .vmt_find_signalid_byname = sui_typeprimary_vmt_find_signalid_byname, \
  .vmt_find_slotid_byname = sui_typeprimary_vmt_find_slotid_byname, \
  .vmt_signal_args_tinfo = sui_typeprimary_vmt_signal_args_tinfo, \
  .vmt_slot_args_tinfo = sui_typeprimary_vmt_slot_args_tinfo, \
  .vmt_common_type = UL_CAST_UNQ1_NULL_ALLOWED(sui_obj_vmt_t *, (a_common)), \
\
  .inc_refcnt = (sui_refcnt_t(*)(sui_obj_t *self)) a_inc_refcnt, \
  .dec_refcnt = (sui_refcnt_t(*)(sui_obj_t *self)) a_dec_refcnt \
};

#define SUI_TYPE_DECLARE_REFCNT(a_name, a_typeid, a_C_type, a_common, a_inc_refcnt, a_dec_refcnt) \
\
static int sui_##a_name##_vmt_init( sui_obj_t *self, void *params) \
{\
  ((a_C_type*)self)->refcnt=1; \
  return 0; \
}\
\
SUI_TYPE_DECLARE_REFCNT_EX(a_name, a_typeid, a_C_type, a_common, a_inc_refcnt, a_dec_refcnt, sui_##a_name##_vmt_init, NULL)

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /*_SUI_TYPEMETHODS_H_*/
