/* sui_args.c
 *
 * SUITK function arguments propagation.
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#include <stdarg.h>
#include "sui_argsbase.h"
#include "sui_objbase.h"
#include "sui_types.h"

typedef int va_bare_void_fnc(void);

static int va_bare_void(int (*fnc)(void), va_list args)
{
  va_bare_void_fnc *f=(va_bare_void_fnc*)fnc;
  return f();
}

typedef int va_context_void_fnc(void *context);

static int va_context_void(int (*fnc)(void), void *context, va_list args)
{
  va_context_void_fnc *f=(va_context_void_fnc*)fnc;
  return f(context);
}

sui_args_tinfo_t sui_args_tinfo_void = {
  .count = 0,
  .call_va_bare = va_bare_void,
  .call_va_context = va_context_void,
  .args = {}
};


typedef int va_bare_int_fnc(int arg1);

static int va_bare_int(int (*fnc)(void), va_list args)
{
  va_bare_int_fnc *f=(va_bare_int_fnc*)fnc;
  return f(va_arg(args, int));
}

typedef int va_context_int_fnc(void *context, int arg1);

static int va_context_int(int (*fnc)(void), void *context, va_list args)
{
  va_context_int_fnc *f=(va_context_int_fnc*)fnc;
  return f(context, va_arg(args, int));
}

sui_args_tinfo_t sui_args_tinfo_int = {
  .count = 1,
  .call_va_bare = va_bare_int,
  .call_va_context = va_context_int,
  .args = {SUI_TYPE_INT}
};

sui_args_tinfo_t sui_args_tinfo_uint = {
  .count = 1,
  .call_va_bare = va_bare_int,
  .call_va_context = va_context_int,
  .args = {SUI_TYPE_UINT}
};


typedef int va_bare_long_fnc(long arg1);

static int va_bare_long(int (*fnc)(void), va_list args)
{
  va_bare_long_fnc *f=(va_bare_long_fnc*)fnc;
  return f(va_arg(args, long));
}

typedef int va_context_long_fnc(void *context, long arg1);

static int va_context_long(int (*fnc)(void), void *context, va_list args)
{
  va_context_long_fnc *f=(va_context_long_fnc*)fnc;
  return f(context, va_arg(args, long));
}

sui_args_tinfo_t sui_args_tinfo_long = {
  .count = 1,
  .call_va_bare = va_bare_long,
  .call_va_context = va_context_long,
  .args = {SUI_TYPE_LONG}
};

sui_args_tinfo_t sui_args_tinfo_ulong = {
  .count = 1,
  .call_va_bare = va_bare_long,
  .call_va_context = va_context_long,
  .args = {SUI_TYPE_ULONG}
};



typedef int va_bare_ptr_fnc(void * arg1);

static int va_bare_ptr(int (*fnc)(void), va_list args)
{
  va_bare_ptr_fnc *f=(va_bare_ptr_fnc*)fnc;
  return f(va_arg(args, void *));
}

typedef int va_context_ptr_fnc(void *context, void *arg1);

static int va_context_ptr(int (*fnc)(void), void *context, va_list args)
{
  va_context_ptr_fnc *f=(va_context_ptr_fnc*)fnc;
  return f(context, va_arg(args, void *));
}

sui_args_tinfo_t sui_args_tinfo_ptr = {
  .count = 1,
  .call_va_bare = va_bare_ptr,
  .call_va_context = va_context_ptr,
  .args = {SUI_TYPE_UNKNOWN}
};
