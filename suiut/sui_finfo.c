/* sui_finfo.c
 *
 * format informations FINFO common functions
 *  
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#include "sui_finfo.h"
#include <suiut/support.h>

sui_refcnt_t sui_choicetable_inc_refcnt(sui_choicetable_t *cht)
{
  if (!cht) return SUI_REFCNTERR;
  if (cht->refcnt >= 0) cht->refcnt++;
  return cht->refcnt;
}

sui_refcnt_t sui_choicetable_dec_refcnt(sui_choicetable_t *cht)
{
  sui_choice_t *ch;
  sui_refcnt_t ref;
  if (!cht) return SUI_REFCNTERR;
  if (cht->refcnt > 0) cht->refcnt--;
  if (!(ref=cht->refcnt)) {    /* decrement all of text and free table, if it isn't static */
    int cnt;
    if (cht->chtab && cht->challoc != SUI_STATIC) {
      ch = cht->chtab; cnt = cht->count;
      while (cnt--) {
        sui_utf8_dec_refcnt(ch->text);
        ch++;
      }
      free(cht->chtab);   /* free choice table */
      cht->chtab =NULL;
    }
    sui_obj_done_vmt((sui_obj_t *)cht, UL_CAST_UNQ1(sui_obj_vmt_t *, &sui_choicetable_vmt_data));
    free(cht); /* free choitab */
  }
  return ref;
}

sui_refcnt_t sui_finfo_inc_refcnt(sui_finfo_t *format)
{
  if (!format) return SUI_REFCNTERR;
  if(format->refcnt >= 0) format->refcnt++;
  return format->refcnt;
}

sui_refcnt_t sui_finfo_dec_refcnt(sui_finfo_t *format)
{
  sui_refcnt_t ref;
  if (!format) return SUI_REFCNTERR;
  if (format->refcnt > 0) format->refcnt--;
  if (!(ref=format->refcnt)) {
    if (format->ftext) {
      sui_utf8_dec_refcnt(format->ftext);
      format->ftext = NULL;
    }
    sui_choicetable_dec_refcnt(format->choices);
    format->choices = NULL;
    sui_obj_done_vmt((sui_obj_t *)format, UL_CAST_UNQ1(sui_obj_vmt_t *, &sui_format_vmt_data));
    free(format);
  }
  return ref;
}

/********************************************************************/
/* Dynamic struct */

/**
 * sui_choicetable_create - create choice table structure
 * @acnt: count of choices in table or SUI_STATIC for static table
 * @ch: array of choices
 *
 * The function creates choice table structure and fill its fields.
 * Return: The function returns pointer to created choice table or NULL.
 * File: sui_finfo.c
 */
sui_choicetable_t *sui_choicetable_create(int acnt, sui_choice_t ch[])
{
  sui_choicetable_t *cht = sui_malloc(sizeof(sui_choicetable_t));
  if ( !cht) return NULL;
  sui_choicetable_inc_refcnt( cht);
  if ( ch) {
    if ( acnt < 0) { /* static */
      sui_choice_t *chi = ch;
      while ( chi->mask) { cht->count++; chi++;}
      cht->count++;
      cht->challoc = SUI_STATIC;
      cht->chtab = ch;
    } else {
      cht->chtab = malloc( sizeof(sui_choice_t)*acnt);
      if ( cht->chtab) {
        sui_choice_t *chi = cht->chtab;
        int i;
        cht->challoc = acnt;
        for( i=0; i<acnt; i++) {
          sui_utf8_inc_refcnt( ch[i].text);
          chi->mask = ch[i].mask;
          chi->value = ch[i].value;
          chi->text = ch[i].text;
          chi++;
        }
        cht->count = i;
      }
    }
  }
  return cht;
}

/**
 * sui_choice_add_to_choicetable - add choice into dynamic choicetable
 * @chtab: pointer to choice table structure
 * @ch: pointer to new choice
 *
 * The function adds new choice into choice table if
 * the table isn't static. If there isn't allocate space
 * for the new choice, the function allocates new larger space.
 */
int sui_choice_add_to_choicetable(sui_choicetable_t *chtab, sui_choice_t *ch)
{
  sui_choice_t *ncht;
  if (!chtab || chtab->challoc == SUI_STATIC) return -1;

  ncht = sui_add_item_to_array((void **)((void *)&chtab->chtab), sizeof(sui_choice_t), &chtab->count, &chtab->challoc);
  if (!ncht) return -1; // no new item !!!

  ncht->mask = ch->mask;
  ncht->value = ch->value;
  sui_utf8_inc_refcnt(ch->text);
  ncht->text = ch->text;
  return 0;
}



/**
 * sui_finfo_create - create format structure
 * @abase:
 * @adig:
 * @alstep:
 * @abstep:
 * @acht:
 *
 */
sui_finfo_t *sui_finfo_create(int abase, int adig, long alstep, long abstep, sui_choicetable_t *acht)
{
  sui_finfo_t *fi = sui_malloc(sizeof(sui_finfo_t));
  if (!fi) return NULL;
  sui_finfo_inc_refcnt(fi);
  fi->base = abase;
  fi->digits = adig;
  fi->litstep = alstep;
  fi->bigstep = abstep;
  sui_choicetable_inc_refcnt( acht);
  fi->choices = acht;
  return fi;
}
