/* sui_comdefs.h
 *
 * SUITK common defines
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#ifndef _SUIUT_COMDEFS_H_
  #define _SUIUT_COMDEFS_H_

#ifdef __cplusplus
extern "C" {
#endif

/******************************************************************************
 * Suitk type identifier
 ******************************************************************************/

/* base type for indication types of other data elements */
typedef int sui_typeid_t;

enum sui_typeids_flags {
  SUI_TYPE_CODE_MASK   = (1<<(sizeof(sui_typeid_t)*8-4))-1,
  SUI_TYPE_PTR_FL      = (1<<(sizeof(sui_typeid_t)*8-4)),
  SUI_TYPE_REGISTER_FL = (2<<(sizeof(sui_typeid_t)*8-4)),
};

/* reference counter type */
#ifndef SUI_REFCNT_TYPE
  #define SUI_REFCNT_TYPE
  typedef int sui_refcnt_t;
#endif
/* static object */
#define SUI_STATIC (-2)
/* reference counting is not appropriate */
#define SUI_REFCNTIMPOSIBLE (-0xfe)
/* reference function error */
#define SUI_REFCNTERR (-0xff)


/******************************************************************************
 * Suitk optional const qualifier to reduce RAM memory usage
 ******************************************************************************/

#ifndef SUI_CANBE_CONST_VMT
#define SUI_CANBE_CONST_VMT
#endif

#ifndef SUI_CANBE_CONST
#define SUI_CANBE_CONST
#endif

/******************************************************************************
 * Other types and structures
 ******************************************************************************/
/* widget struct */
struct sui_widget;
/* event struct */
struct sui_event;
/* dc struct */
struct sui_dc;

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /* _SUIUT_COMDEFS_H_ */

