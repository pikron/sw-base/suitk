/* sui_di_text.h
 *
 * SUITK dinfo expansion for utf8 strings
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#ifndef _SUI_DINFO_TEXT_MODULE_
  #define _SUI_DINFO_TEXT_MODULE_

  #include <suiut/sui_utf8.h>
  #include <suiut/sui_dinfo.h>
  #include <suiut/sui_finfo.h>

#ifdef __cplusplus
extern "C" {
#endif

/* direct utf8 text */
int sui_utf8_rdval( sui_dinfo_t *datai, long idx, void *buf);
int sui_utf8_wrval( sui_dinfo_t *datai, long idx, const void *buf);

sui_dinfo_t *sui_dinfo_create_utf8( utf8 *adata);

/* array of pointers to utf texts */
int sui_putf8_rdval( sui_dinfo_t *datai, long idx, void *buf);
int sui_putf8_wrval( sui_dinfo_t *datai, long idx, const void *buf);

sui_dinfo_t *sui_dinfo_create_putf8( utf8 *adata, long aidxsize);

/* hevent function prototype */
void sui_dinfo_hevent_utf8( struct sui_dinfo *dinfo, struct sui_event *event);


/* common rd/wr utf8 from/to dinfo */
int sui_rd_utf8( sui_dinfo_t *datai, long idx, utf8 **buf);
int sui_wr_utf8( sui_dinfo_t *datai, long idx, utf8 * const *buf);



/* added in version 1.0 */
utf8 *sui_di2utf8_maskedlong(long value, sui_choicetable_t *choices, unsigned short flags);
utf8 *sui_di2utf8_long(long value, int fdigits, int size, int base, unsigned short flags);
utf8 *sui_di2utf8_time(unsigned long value, utf8 *format_text, unsigned short flags);
utf8 *sui_utf8_to_formstr(utf8 *number, sui_finfo_t *fi);

utf8 *sui_dinfo_2_utf8( sui_dinfo_t *data, long idx, sui_finfo_t *format, unsigned short flags);

int sui_dinfo_copy_utf8(sui_dinfo_t *dfrom, sui_dinfo_t *dto, int idx);

/**
 * dtrans_flags - transformation dinfo to utf8 flags
 * @SUTR_FILLUPBYZERO: fill up by zeros before decimal point
 * @SUTR_DECIMALZEROS: fill up by zeros after decimal point
 * @SUTR_SMALLLETTERS: letters will be small
 * @SUTR_MUSTBEMINUS: add always minus before the number
 * @SUTR_CHECKLIMITS: check value limits and show '---' if the value is out of boundaries
 * @SUTR_RELATIVETIME: if time can be relative (negative) it is show with '-'
 * @SUTR_UTCTIME:      no conversion to local time should be applied
 * @SUTR_LOCALTIME:    time is converted to local time according to selected time zone
 */
enum dtrans_d2u_flags {
  SUTR_FILLUPBYZERO = 0x1000,
  SUTR_DECIMALZEROS = 0x2000,
  SUTR_SMALLLETTERS = 0x4000,
  SUTR_MUSTBEMINUS  = 0x8000,
  SUTR_CHECKLIMITS  = 0x0100,
  SUTR_RELATIVETIME = 0x0200,
  SUTR_UTCTIME      = 0x0400,
  SUTR_LOCALTIME    = 0x0800,
  SUTR_HMSONLYTIME  = 0x1000,
  SUTR_FORM_JSON    = 0x2000,
  SUTR_DECPOSMASK   = 0x00FF,
};


typedef struct sui_dinfo_cat {
	sui_dinfo_t *first;
	sui_dinfo_t *second;
	int mode;
} sui_dinfo_cat_t;

enum sui_dicat_flags {
  SUI_DICAT_NORMAL = 0,
  SUI_DICAT_SPACE  = 1,
  SUI_DICAT_MODE_MASK = 0x0f, /* mask for mode selection */
  SUI_DICAT_NONE_NULL = 0x80, /* return NULL if both parts are empty */
};

int sui_dinfo_cat_rdval( sui_dinfo_t *datai, long idx, void *buf);
//int sui_dinfo_cat_wrval( sui_dinfo_t *datai, long idx, const void *buf);
void sui_dinfo_cat_proxy(struct sui_dinfo *dinfo, struct sui_event *event);
sui_dinfo_t *sui_dinfo_cat_create( sui_dinfo_t *di_first, sui_dinfo_t *di_second,
                                   int amode, long ainfo);



typedef struct sui_dinfo_select {
	sui_dinfo_t *first;
	sui_dinfo_t *second;
	sui_dinfo_t *select;
	long treshold;
} sui_dinfo_select_t;

int sui_dinfo_select_rdval( sui_dinfo_t *datai, long idx, void *buf);
int sui_dinfo_select_wrval( sui_dinfo_t *datai, long idx, const void *buf);
void sui_dinfo_select_proxy(struct sui_dinfo *dinfo, struct sui_event *event);
sui_dinfo_t *sui_dinfo_select_create( sui_dinfo_t *di_first, sui_dinfo_t *di_second,
                              sui_dinfo_t *di_select, int atreshold, long ainfo);

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif
