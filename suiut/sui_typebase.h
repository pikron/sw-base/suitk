/* sui_typebase.h
 *
 * SUITK object types and their identifiers.
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#ifndef _SUI_TYPEBASE_H_
#define _SUI_TYPEBASE_H_

#include "sui_objbase.h"

#ifdef __cplusplus
extern "C" {
#endif

extern SUI_CANBE_CONST_VMT sui_obj_vmt_t sui_char_vmt_data;
extern SUI_CANBE_CONST_VMT sui_obj_vmt_t sui_uchar_vmt_data;
extern SUI_CANBE_CONST_VMT sui_obj_vmt_t sui_short_vmt_data;
extern SUI_CANBE_CONST_VMT sui_obj_vmt_t sui_ushort_vmt_data;
extern SUI_CANBE_CONST_VMT sui_obj_vmt_t sui_int_vmt_data;
extern SUI_CANBE_CONST_VMT sui_obj_vmt_t sui_uint_vmt_data;
extern SUI_CANBE_CONST_VMT sui_obj_vmt_t sui_long_vmt_data;
extern SUI_CANBE_CONST_VMT sui_obj_vmt_t sui_ulong_vmt_data;
extern SUI_CANBE_CONST_VMT sui_obj_vmt_t sui_llong_vmt_data;
extern SUI_CANBE_CONST_VMT sui_obj_vmt_t sui_ullong_vmt_data;
extern SUI_CANBE_CONST_VMT sui_obj_vmt_t sui_fixed_vmt_data;
extern SUI_CANBE_CONST_VMT sui_obj_vmt_t sui_ufixed_vmt_data;

extern sui_obj_vmt_t sui_obj_vmt_data;

#ifndef _SUI_IN_UTF8VMT_C
/* this is not fully correct for utf8, because code uses sui_utf8_vmt_t internally */
extern sui_obj_vmt_t sui_utf8_vmt_data;	
#endif

/**
 * enum sui_typebase_ids - The identifiers of primary types
 * @SUI_TYPE_UNKNOWN: Unknown type
 * @SUI_TYPE_CHAR: "C" character type
 * @SUI_TYPE_UCHAR: Unsigned character type
 * @SUI_TYPE_SHORT: Signed short type
 * @SUI_TYPE_USHORT: Unsigned short type
 * @SUI_TYPE_INT: Signed integer type
 * @SUI_TYPE_UINT: Unsigned integer type
 * @SUI_TYPE_LONG: Signed long type
 * @SUI_TYPE_ULONG: Unsigned long type
 * @SUI_TYPE_LLONG: Signed long long type
 * @SUI_TYPE_ULLONG: Unsigned long long type
 * @SUI_TYPE_FIXED: Fixed decimal point type
 * @SUI_TYPE_UFIXED: Unsigned fixed decimal point type
 * @SUI_TYPE_OBJ: Base object type, exact type id can be resolved from VMT
 * @SUI_TYPE_UTF8: Versatile string with utf-8 encoding
 * @SUI_TYPEBASE_NEXT_TYPE: Next free identifier
 *
 * File: sui_typebase.h
 */
enum sui_typebase_ids {
  SUI_TYPE_UNKNOWN = 0, /* 0 */
  SUI_TYPE_CHAR,
  SUI_TYPE_UCHAR,
  SUI_TYPE_SHORT,
  SUI_TYPE_USHORT,
  SUI_TYPE_INT,
  SUI_TYPE_UINT,
  SUI_TYPE_LONG,
  SUI_TYPE_ULONG,
  SUI_TYPE_LLONG,
  SUI_TYPE_ULLONG,      /* 10 */
  SUI_TYPE_FIXED,
  SUI_TYPE_UFIXED,
  SUI_TYPE_OBJ,
  SUI_TYPE_UTF8,
  SUI_TYPEBASE_NEXT_TYPE
};

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /*_SUI_TYPEBASE_H_*/
