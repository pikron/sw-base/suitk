/* sui_utf8.h
 *
 * Header file for SUITK UTF8 strings.
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#ifndef _SUI_UTF8_MODULE_
#define _SUI_UTF8_MODULE_

#include <suiut/sui_comdefs.h> /* for refcnt types */

#include "sui_nls.h" /* for NLS support (pointer to NLS table) */

#ifdef __cplusplus
extern "C" {
#endif

/* UTF8 string requirements:
 * - kod musi umet precist C string
 * - vlastni stringy musi umoznovat reference counting
 * - Pocet znaku musi byt bud -1 nebo odpovidat platnemu poctu UTF8 znaku
 * - Binarni delka v bytech musi obsahovat prave byte kodujici vsechny znaky retezce
 *   => nesmi obsahovat finalni nulu
 * - Pocty znaku se tez scitaji, jsou-li zname.
 * - Kod by mel umet skladat a tam, kde se nepracuje primo se znaky, i pracovat na obecnych binarnich polich bez interpretace obsahu.
 * - Kod by mel snaset i praci s retezci, ktere jsou tvorene jako ukazatel do textu neomezeneho nulou ale pouze delkou v bytech ci znacich.
 *
 * - je-li string vytvaren dynamicky (sui_utf8_dynamic) musi byl alokovany blok pameti alespon o 1 byte delsi a text musi byt doplnen nulou
 *   => kdykoliv je potreba text predat do C, je mozne udelat sui_utf8_dup a predat retezec do C jako prosty C string

Asi by bylo spravne pridat volani sui_utf8_nullterm, ktere by pro dynamicke verze a proste ukazatele na C stringy pouze inkrementovalo referenci, pro verze, ktere nulou nekonci (mame ted takove?) vytvorilo kopii s jednou referenci. Po ukonceni C volani by pak stacilo udelat sui_utf8_dec_refcnt.
Asi by tam melo byt i volani sui_utf8_is_nullterm().

 */


/* Allowable types of utf8 string 'str'
 *    1) str = pointer to static raw utf8 text
 *    2) str = pointer to dynamic structure with pointer 
 *             to static raw utf8 text
 *    3) str = pointer to dynamic structure with pointer
 *             to dynamic raw utf8 text
 *    4) str = pointer to static structure with static
 *             raw utf8 text inside structure (!!! no pointer !!!)
 *    5) str = pointer to static (??? or dynamic ???) structure (refcnt = negative means static)
 *             with index to NLS table ???
 *  Not allowable is
 *       str = pointer to dynamic structure with raw utf8 string inside structure
 */
 
 /* retype static raw text to utf8 string */
 #define U8 (utf8 *)
#define U8CH (utf8char *)
 
  
 #if (1) /* for 16bit UCS */
   #define UTF8_MAX_BYTES_IN_CHAR  3
 #else   /* for 32bit UCS */
   #define UTF8_MAX_BYTES_IN_CHAR  6
 #endif
  
/**
 * struct sui_utf8 - Suitk UTF8 string structure
 * @type: Type of utf8 string and flags. ['UTF8_TYPE_' prefix]
 * @refcnt: Reference counter of the utf8 string.
 * @chars: String length in characters (length without ending zero).
 * @bytes: String length in bytes (length without ending zero).
 * @cap_bytes: Allowed (allocated) maximal length in bytes of dyn.string (without terminating zero).
 * @nidx: Index to NLS table.
 * @ptr:  Pointer to raw utf8 text.
 * @text: Raw utf8 text.
 * File: sui_utf8.h
 */ 
  typedef struct sui_utf8 {
    unsigned char type;     /* id for specify static and dynamic utf8 string */
    unsigned char reserved;
    short refcnt;           /* reference counter for dymanic strings */
    short chars;              /* string length in chars (without zero ending) */
    short bytes;              /* string length in bytes (with zero ending char) */
    short cap_bytes;          /* allocated bytes for data of dyn.string */
    union {
      int   nidx;             /* index to nls string table */
      char *ptr;              /* pointer to string */
      char  text[1];          /* static text */
    } data;
    short reserved1;
  } sui_utf8_t;

  typedef struct sui_utf8   utf8;
/**
 * typedef utf8char - Definition of utf8 character type.
 * File: sui_utf8.h
 */
  typedef char     utf8char;
  
  /* flags for 'type' field - 6 LS bits is flags */
  #define UTF8_TYPE_MASK         0xC0 /* dynamic or static string */
  #define UTF8_TYPE_RAW_TEXT     0x00
  #define UTF8_TYPE_STRUCT       0x80
  
  #define UTF8_TYPE_INCLUDED_TEXT  0x01 /* struct is static and 'data' is raw text */
  #define UTF8_TYPE_NLS_TEXT       0x02 /* struct with index into NLS table */
/*  #define UTF8_TYPE_DYNAMIC_TEXT 0x02 // referenced string (in 'str' field) is dynamic - redundant with allocated */

/**
 * enum utf8_type - Possible type of utf8 string (returned by sui_utf8_get_type function)
 * @UTF8_STATIC_RAW: Static raw utf8 text.
 * @UTF8_INCLUDED_TEXT: Utf8 structure with included utf text.
 * @UTF8_NLS_STRING: Utf8 text from NLS table.
 * @UTF8_DYNAMIC_STRUCT: Dynamic utf8 structure with pointer to static utf8 text.
 * @UTF8_DYNAMIC_FULL: Dynamic utf8 structure with pointer to dynamic utf8 text.
 * File: sui_utf8.h
 */
  enum utf8_type {
    UTF8_STATIC_RAW     = 0x00,  /* static raw utf8 string */
    UTF8_INCLUDED_TEXT  = 0x01,  /* static struct with included */
    UTF8_NLS_STRING     = 0x02,  /* struct with index to NLS table (struct can be static. ???or dynamic???) */
    UTF8_DYNAMIC_STRUCT = 0x03,  /* dynamic struct with pointer to static text */
    UTF8_DYNAMIC_FULL   = 0x04,  /* dynamic struct with pointer to dynamic text */
  };

/**
 * sui_utf8_nls - Static initialication of utf8 structure with index to nls table
 * @uname: Name of static utf8 NLS structure.
 * @nidx: Index to NLS table.
 * 
 * The macro initializes utf8_string with index to nls table
 * File: sui_utf8.h
 */  
  #define sui_utf8_nls( M_uname, M_nidx) \
     sui_utf8_t M_uname = { \
       .type   = UTF8_TYPE_STRUCT | UTF8_TYPE_NLS_TEXT, \
       .refcnt = SUI_STATIC, \
       .chars  = -1, \
       .bytes  = -1, \
       .cap_bytes = 0, \
       .data.nidx = M_nidx \
     }  

/*********************************************************/
/* UTF8 String reference support                         */
/*********************************************************/
sui_refcnt_t sui_utf8_inc_refcnt( utf8 *str);
sui_refcnt_t sui_utf8_dec_refcnt( utf8 *str);
/*********************************************************/
/* UTF8 function                                         */
/*********************************************************/
  /* low level functions */
#if UTF8_MAX_BYTES_IN_CHAR == 6 /* only for 32bit UCS */
  typedef unsigned long _UTF_UCS_TYPE;
#else // only for 16bit UCS  
  typedef unsigned short _UTF_UCS_TYPE;
#endif  
  int sui_utf8_to_ucs( const utf8char *utf, _UTF_UCS_TYPE *ucs);
  int sui_utf8_from_ucs( utf8char *utf, _UTF_UCS_TYPE ucs); /* long ucs */
  int sui_utf8_count_chars( const utf8char *utf, int bytes);
  int sui_utf8_count_bytes( const utf8char *utf, int chars);
  int sui_utf8_char_size( const utf8char *utf);

  int sui_utf8_to_ucs_backward( const utf8char *utf, _UTF_UCS_TYPE *ucs);

  /* middle level functions */
  int sui_utf8_get_type( const utf8 *utf);
  utf8char *sui_utf8_get_text( utf8 *utf);
  int sui_utf8_length( utf8 *utf);   /* in chars */
  int sui_utf8_bytesize( utf8 *utf); /* in bytes */
  int sui_utf8_capacity( utf8 *utf); /* in bytes */

  int sui_utf8_prerw( utf8 **ptr);

  int sui_utf8_setlength(utf8 *utf, int chars);

  utf8char *sui_utf8_ntoken(utf8char **utf, int *strlen, utf8char *delim, int *cntchar);
  utf8char *sui_utf8_ptoken(utf8char **utf, int *strlen, utf8char *delim, int *cntchar);

  /* high level functions */
  utf8 *sui_utf8( utf8char *text, int chars, int bytes);
  utf8 *sui_utf8_dynamic( const utf8char *text, int chars, int bytes);

  utf8 *sui_utf8_from_unicode(_UTF_UCS_TYPE *unistr, int len_unistr);

  int sui_utf8_ncpy( utf8 *dst, utf8 *src, int len);
  static inline int sui_utf8_cpy( utf8 *dst, utf8 *src) {
    return sui_utf8_ncpy( dst, src, 0);  
  }

  utf8 *sui_utf8_dup( utf8 *utf);
  int sui_utf8_add_char( utf8 *utf, utf8char *chr);
  int sui_utf8_ncat( utf8 *utf, utf8 *add, int len);
  static inline int sui_utf8_cat( utf8 *dst, utf8 *src) {
    return sui_utf8_ncat( dst, src, -1);
  }
  utf8 *sui_utf8_nconcate(utf8 *one, int onelen, utf8* two, int twolen);
  static inline utf8 *sui_utf8_concate(utf8 *one, utf8 *two) {
    return sui_utf8_nconcate(one, -1, two, -1);
  }

//  int sui_utf8_cut( utf8 *utf, int chars);
/*  int sui_utf8_set( utf8 *utf, int ofst, utf8char *chr, int chars); */
  int sui_utf8_ncmp( utf8 *one, utf8 *two, int chars);
  int sui_utf8_cmp( utf8 *one, utf8 *two);

  int sui_utf8_casecmp(utf8 *one, utf8 *two);


  int sui_utf8_nsearch( utf8 *utf, utf8 *patt, int chars); 
  static inline int sui_utf8_search( utf8 *utf, utf8 *patt) {
    return sui_utf8_nsearch( utf, patt, -1);
  }

  int sui_utf8_rewrite( utf8 *utf, int ofs, utf8char *chr);
  int sui_utf8_insert( utf8 *utf, int ofs, utf8char *chr);
  int sui_utf8_delete( utf8 *utf, int ofs);

  int sui_utf8_insert_dynamic(utf8 *utf, int ofs, utf8char *chr);

  utf8 *sui_utf8_nullterm(utf8 *utf);
  int sui_utf8_is_nullterm(utf8 *utf);

/* basic character tests and upper<->lower */
  _UTF_UCS_TYPE sui_utf8_isalnum( utf8char *utf);
  _UTF_UCS_TYPE sui_utf8_isalpha( utf8char *utf);
  _UTF_UCS_TYPE sui_utf8_isdigit( utf8char *utf);
  _UTF_UCS_TYPE sui_utf8_islower( utf8char *utf);
  _UTF_UCS_TYPE sui_utf8_ispunct( utf8char *utf);
  _UTF_UCS_TYPE sui_utf8_isspace( utf8char *utf);
  _UTF_UCS_TYPE sui_utf8_isupper( utf8char *utf);
  _UTF_UCS_TYPE sui_utf8_ucs_tolower(_UTF_UCS_TYPE ucs);
  _UTF_UCS_TYPE sui_utf8_ucs_toupper(_UTF_UCS_TYPE ucs);

/* string transformations */
  utf8 *sui_utf8_tolower(utf8 *inp);
  utf8 *sui_utf8_toupper(utf8 *inp);

      
  /* transforms */
/* create dynamic utf8 from long integer ( digits can be -1) */
/*
#define SUTF8_N2U_ZEROS 0x0001 // string doplni zleva nulami 0001230 jinak mezerami 
#define SUTF8_N2U_SMALL 0x0002 // 'a','b',.... 
  utf8 *sui_utf8_l2u( long num, int digits, int base, short flags);
//  long sui_utf8_u2l( utf8 *utf, int ofst, int base, int maxchars); 
*/

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif
