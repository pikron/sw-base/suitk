/* sui_dinfo.h
 *
 * Header file for DINFO (data info structures) common functions
 * 
 * Version 0.1
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#ifndef _SUI_DIEVC_H_
#define _SUI_DIEVC_H_

#include "sui_dinfo.h"
#include "ul_evcbase.h"

#ifdef __cplusplus
extern "C" {
#endif

void sui_dinfo_connect_to_hub(sui_dinfo_t *datai, evc_rx_hub_t *dst,
                              evc_prop_fnc_t *prop);


void sui_dinfo_connect_to_link(sui_dinfo_t *datai, evc_link_t *link,
			evc_rx_hub_t *dst, evc_prop_fnc_t *prop);

int sui_dinfo_add_weakptr(sui_dinfo_t *datai, sui_dinfo_t **weakptr);

int sui_dinfo_del_weakptr(sui_dinfo_t *datai, sui_dinfo_t **weakptr);


#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /* _SUI_DIEVC_H_ */
