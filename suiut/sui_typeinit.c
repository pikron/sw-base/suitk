/* sui_types.c
 *
 * SUITK type system and its support.
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

 
#include "sui_comdefs.h"
#include "sui_typereg.h"
#include "sui_types.h"
#include <string.h>
#include <suitk/suitk.h>

#include <ul_log.h>

UL_LOG_CUST(ulogd_suiut)


const void * SUI_CANBE_CONST
sui_type_table_default[SUI_TYPE_ARRAYSIZE] = {
	[SUI_TYPE_UNKNOWN] = NULL,
	[SUI_TYPE_CHAR]    = &sui_char_vmt_data,
	[SUI_TYPE_UCHAR]   = &sui_uchar_vmt_data,
	[SUI_TYPE_SHORT]   = &sui_short_vmt_data,
	[SUI_TYPE_USHORT]  = &sui_ushort_vmt_data,
	[SUI_TYPE_INT]     = &sui_int_vmt_data,
	[SUI_TYPE_UINT]    = &sui_uint_vmt_data,
	[SUI_TYPE_LONG]    = &sui_long_vmt_data,
	[SUI_TYPE_ULONG]   = &sui_ulong_vmt_data,
//	[SUI_TYPE_LLONG]   = &sui_llong_vmt_data,
//	[SUI_TYPE_ULLONG]  = &sui_ullong_vmt_data,
//	[SUI_TYPE_FIXED]   = &sui_fixed_vmt_data,
//	[SUI_TYPE_UFIXED]  = &sui_ufixed_vmt_data,
	[SUI_TYPE_OBJ]     = &sui_obj_vmt_data,
	[SUI_TYPE_UTF8]    = &sui_utf8_vmt_data,
//	[SUI_TYPE_PROXY]   = &sui_proxy_vmt_data,
//	[SUI_TYPE_DBUFF]   = &sui_dbuff_vmt_data,
	[SUI_TYPE_REFCNT]  = &sui_refcnt_vmt_data,
	[SUI_TYPE_COORD]   = &sui_coord_vmt_data,
	[SUI_TYPE_COLOR]   = &sui_color_vmt_data,
	[SUI_TYPE_ALIGN]   = &sui_align_vmt_data,
	[SUI_TYPE_FLAGS]   = &sui_flags_vmt_data,
	[SUI_TYPE_TYPE]    = &sui_type_vmt_data,
//	[SUI_TYPE_ASCII]   = &sui_ascii_vmt_data,
	 
/* basic structures */
	[SUI_TYPE_POINT]   = &sui_point_vmt_data,
	[SUI_TYPE_RECT]    = &sui_rect_vmt_data,
	[SUI_TYPE_COLORTABLE]= &sui_colortable_vmt_data,
	[SUI_TYPE_FONTINFO]= &sui_fontinfo_vmt_data,
	[SUI_TYPE_CHOICE]  = &sui_choice_vmt_data,
	[SUI_TYPE_CHOICETABLE]= &sui_choicetable_vmt_data,
	[SUI_TYPE_FORMAT]  = &sui_format_vmt_data,
	[SUI_TYPE_STYLE]   = &sui_style_vmt_data,
	[SUI_TYPE_SUFFIX]  = &sui_suffix_vmt_data,
	[SUI_TYPE_DINFO]   = &sui_dinfo_vmt_data,
	[SUI_TYPE_IMAGE]   = &sui_image_vmt_data,
	[SUI_TYPE_KEY]     = &sui_key_vmt_data,
	[SUI_TYPE_KEYTABLE]= &sui_keytable_vmt_data,
	[SUI_TYPE_LIST]    = &sui_list_vmt_data,
	[SUI_TYPE_EVENT]   = &sui_event_vmt_data,

	[SUI_TYPE_COLUMN]  = &sui_column_vmt_data,
	[SUI_TYPE_TABLE]   = &sui_table_vmt_data,

	[SUI_TYPE_GSET]    = &sui_gset_vmt_data,
	[SUI_TYPE_GDATA]   = &sui_gdata_vmt_data,

	[SUI_TYPE_WIDGET]  = &sui_widget_vmt_data,
	
	[SUI_TYPE_WGROUP]  = &sui_wgroup_vmt_data,
	[SUI_TYPE_WBUTTON] = &sui_wbutton_vmt_data,
	[SUI_TYPE_WBITMAP] = &sui_wbitmap_vmt_data,
	[SUI_TYPE_WLISTBOX]= &sui_wlistbox_vmt_data,
	[SUI_TYPE_WNUMBER] = &sui_wnumber_vmt_data,
	[SUI_TYPE_WSCROLL] = &sui_wscroll_vmt_data,
	[SUI_TYPE_WTEXT]   = &sui_wtext_vmt_data,
	[SUI_TYPE_WTIME]   = &sui_wtime_vmt_data,
	[SUI_TYPE_WTABLE]  = &sui_wtable_vmt_data,
	[SUI_TYPE_WDYNTEXT]  = &sui_wdyntext_vmt_data,
	[SUI_TYPE_WGRAPHXY]  = &sui_wgraphxy_vmt_data,

//	[SUI_TYPE_SCREEN]  = &sui_screen_vmt_data,

/* special type mainly for sxml */
	[SUI_TYPE_DEFINE]  = &sui_define_vmt_data,
	[SUI_TYPE_KEYFCN]  = &sui_keyfcn_vmt_data, // widget:sui_hkey_t
	
	[SUI_TYPE_LINK] = &sui_link_vmt_data,
	[SUI_TYPE_SCENARIO] = &sui_scenario_vmt_data,

	[SUI_TYPE_CONDITION]  = &sui_condition_vmt_data,
	[SUI_TYPE_ACTIONFCN]  = &sui_action_fcn_vmt_data,
	[SUI_TYPE_ACTION]     = &sui_action_vmt_data,
	[SUI_TYPE_TRANSITION] = &sui_transition_vmt_data,
	[SUI_TYPE_STATEFCN]   = &sui_state_fcn_vmt_data,
	[SUI_TYPE_STATE]      = &sui_state_vmt_data,
	[SUI_TYPE_STATEEXT]   = &sui_stateext_vmt_data,
	[SUI_TYPE_SUBSTATE]   = &sui_substate_vmt_data,

	[SUI_TYPE_APPLICATION] = &sui_application_vmt_data,

	[SUI_TYPE_PRNFILTER] = &sui_prnfilter_vmt_data,
	[SUI_TYPE_PRNLAYOUT] = &sui_prnlayout_vmt_data,
};


int sui_type_register_default_types()
{
  sui_obj_vmt_t *vmt;
  int typeid;
  int res;
  int err=0;

  for(typeid=0;typeid<SUI_TYPE_ARRAYSIZE;typeid++){    
    vmt=(sui_obj_vmt_t *)sui_type_table_default[typeid];
    if(!vmt)
      continue;

    if(vmt->vmt_typeid != typeid){
      if(vmt->vmt_typeid) {
        ul_logerr("conflicting typeids %d %ld\n",typeid,(long)vmt->vmt_typeid);
      }
      vmt->vmt_typeid = typeid;
    }

    vmt->vmt_refcnt=1000;

    res=sui_obj_register_vmt(vmt);
    if(res<0){
      err=-1;
      ul_logerr("sui_obj_register_vmt failed for typeid %d\n",typeid);
    }
  }
  return err?-1:0;
}
