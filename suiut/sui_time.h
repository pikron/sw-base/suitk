/* sui_time.h
 *
 * SUITK time functions (conversions).
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#ifndef _SUI_TIME_H_
#define _SUI_TIME_H_

#ifdef __cplusplus
extern "C" {
#endif

/******************************************************************************
 * Time/Date
 ******************************************************************************/
/* base for linear time - seconds are counted from this time - it is defined as 0:0:0 1.1.1970 (POSIX) */
/* with this base and unsigned 32bit second counter - counter rollover will be at x:x:x x.x.2106 */
#define SUI_TIME_ZERO_SEC   0    /* base second */
#define SUI_TIME_ZERO_MIN   0    /* base minute */
#define SUI_TIME_ZERO_HOUR  0    /* base hour */
#define SUI_TIME_ZERO_MDAY  1    /* base day of month 1-31 */
#define SUI_TIME_ZERO_MONTH 1    /* base month 1-12 */
#define SUI_TIME_ZERO_YEAR  1970 /* base year */
#define SUI_TIME_ZERO_WDAY  4    /* day of week  1.1.1970 -> wday = 4 (Thursday) */
#define SUI_TIME_ZERO_YDAY  0    /* day of year 1.1. -> yday=0 */

#define SUI_TIME_LEAP_YEARS_TO_EPOCH ((SUI_TIME_ZERO_YEAR/4)-(SUI_TIME_ZERO_YEAR/100)+(SUI_TIME_ZERO_YEAR/400))


extern const int sui_time_year_days[2];
extern const int sui_time_month_description[2][13];
extern const int sui_time_mon2dow_table[12];

/**
 * struct sui_time_elements - All time parts in structure
 * @sec: seconds  [0-59]
 * @min: minutes  [0-59]
 * @hour: hours   [0-23]
 * @mday: day in month [1-31]
 * @mon: month    [0-11]
 * @year: year (from year_base)
 * @wday: day of week [0-6] = [Sun,Mon,Tue,Wed,Thu,Fri,Sat]
 * @yday: day in year [0-365]
 * @isdst: is daylight saving time used [0/1]
 */
typedef struct sui_time_elements
{
  int sec;                   /* Seconds      [0-60] (1 leap second) */
  int min;                   /* Minutes      [0-59] */
  int hour;                  /* Hours        [0-23] */
  int mday;                  /* Day          [1-31] */
  int mon;                   /* Month        [0-11] */
  int year;                  /* Year         [1967,2034,...].  */
  int wday;                  /* Day of week  [0-6] */
  int yday;                  /* Day in year  [0-365] */
  int isdst;                 /* DST          [0/1] - is daylight saving time implied (0 => unused, 1 => used) */
  long utco;                 /* offset to UTC time - it contains timezone offset and DST offset implied for local time */
} sui_time_elements_t;

/* number of seconds per ... */
#define SUI_TIME_SEC_PER_MIN  (60)
#define SUI_TIME_SEC_PER_HOUR (60*60)
#define SUI_TIME_SEC_PER_DAY  (24L*SUI_TIME_SEC_PER_HOUR)
#define SUI_TIME_SEC_PER_YEAR (365*SUI_TIME_SEC_PER_DAY)
#define SUI_TIME_SEC_PER_LEAP_YEAR (366*SUI_TIME_SEC_PER_DAY)

/* universal time functions */
int sui_time_year_is_leap( int year);
void sui_time_convert_dmy2dow( sui_time_elements_t *tm);
void sui_time_convert_yd2dmw( sui_time_elements_t *tm);
void sui_time_convert_dmy2doy( sui_time_elements_t *tm);

/* conversion functions for limited range of time from EPOCH (1970/1/1 at 00:00:00 UTC) to EPOCH+2^32seconds (2106/2/7 at 06:28:15h UTC) */
int sui_time_is_leapyear(int year);
void sui_time_convert_esec2eday(unsigned long esec, long *eday, long *dsec);
void sui_time_convert_eday2yyd(long eday, int *year, int *yday, int *leap);
void sui_time_convert_yday2md(int yday, int leap, int *mon, int *day);
void sui_time_convert_yyd2dow(int year, int yday, int *dow);
void sui_time_convert_dml2yday(int leap, int mon, int day, int *yday);


/* conversion functions for UTC time */
int sui_time_convert_sec2struct( unsigned long epochsec, sui_time_elements_t *tm);
int sui_time_convert_struct2sec( sui_time_elements_t *tm, unsigned long *pepochsec);

/******************************************************************************/
/* TimeZones & DST */
typedef int sui_time_dst_rule_t(unsigned long esec, long *dsto, long *offs);

extern long                 sui_time_tz_offset; /* time zone offset to GMT in seconds */
extern sui_time_dst_rule_t *sui_time_dst_rule;

enum sui_time_known_dst_rules {
  SUI_TIME_DST_NONE = 0,
  SUI_TIME_DST_EU,
};

#define SUI_TIME_DST_LAST_RULE SUI_TIME_DST_EU 


/* leap seconds in history - when and how many seconds were added from the epoch (sum of leap seconds) */
/* TODO: ad leap seconds for UTC time */
// typedef struct sui_time_ls_table {
//   unsigned long when; /* time when seconds were leaped */
//   long sls; /* sum of leaped seconds from epoch */
// } sui_time_ls_table_t;

int sui_time_set_tz_dst(long tzoffs, enum sui_time_known_dst_rules dst_idx);

int sui_time_tz_dst2rule(sui_time_dst_rule_t **p_dst_rule, enum sui_time_known_dst_rules dst_idx);

int sui_time_set_tz_dst_rule(long tzoffs, sui_time_dst_rule_t *dst_rule);

/* conversion functions for local time */
int sui_time_convert_sec2tzdst(unsigned long epochsec, sui_time_elements_t *tm, long tzoffs, sui_time_dst_rule_t *dstr);
static inline int sui_time_convert_sec2local(unsigned long epochsec, sui_time_elements_t *tm)
{
  return sui_time_convert_sec2tzdst(epochsec, tm, sui_time_tz_offset, sui_time_dst_rule);
}

int sui_time_convert_tzdst2sec(sui_time_elements_t *tm, unsigned long *epochsec, long tzoffs, sui_time_dst_rule_t *dstr);
static inline int sui_time_convert_local2sec(sui_time_elements_t *tm, unsigned long *pepochsec)
{
  return sui_time_convert_tzdst2sec(tm, pepochsec, sui_time_tz_offset, sui_time_dst_rule);
}

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif
