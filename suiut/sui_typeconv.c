/* sui_typeconv.c
 *
 * SUITK type system and its support.
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

 
#include "sui_types.h"
#include <string.h>
#include <suitk/suitk.h>

/******************************************************************************
 * Type functions
 ******************************************************************************/
/*
 * enum convert_half_types - internal conversion function types
 */
enum convert_half_types {
  SUI_TCONVERT_NONE = 0,
  SUI_TCONVERT_LONG = 1,
  SUI_TCONVERT_ULONG = 2,
  SUI_TCONVERT_UTF8 = 3,
};

/**
 * sui_typeid_convert - Common object conversion function according to their types
 * @outtype: Identifier of requested an output type.
 * @outptr: Pointer to buffer for an output variable.
 * @intype: Type identifier of an input variable.
 * @inptr: Pointer to an input variable.
 * @count: Number of items in array (if object is array of the same objects).
 *
 * The function converts variable from input type to output type
 * and saves pointer of the coverted variable to output buffer.
 *
 * Return Value: The function returns either zero for successful conversion or error code (SUI_RET_).
 *
 * File: sui_typeconv.c
 */
int sui_typeid_convert(sui_typeid_t outtype, void *outptr,
                      sui_typeid_t intype, void *inptr, int count)
{
  long h_long = 0;
  unsigned long h_ulong = 0;
  utf8 *h_utf8 = NULL;

  int thalf = SUI_TCONVERT_NONE;
  int tout = SUI_TCONVERT_NONE;
  int ret = SUI_RET_OK;

  if (inptr == NULL || outptr == NULL)
    return SUI_RET_NRDY;
/* read in */
  switch(intype) {
    case SUI_TYPE_CHAR:
      h_long = (long)*((char *)inptr);
      thalf = SUI_TCONVERT_LONG;
      break;
    case SUI_TYPE_SHORT:
      h_long = (long)*((short *)inptr);
      thalf = SUI_TCONVERT_LONG;
      break;
    case SUI_TYPE_INT:
      h_long = (long)*((int *)inptr);
      thalf = SUI_TCONVERT_LONG;
      break;
    case SUI_TYPE_LONG:
      h_long = (long)*((long *)inptr);
      thalf = SUI_TCONVERT_LONG;
      break;
    case SUI_TYPE_COORD:
      h_long = (long)*((sui_coordinate_t *)inptr);
      thalf = SUI_TCONVERT_LONG;
      break;
    case SUI_TYPE_UCHAR:
      h_ulong = (unsigned long)*((unsigned char *)inptr);
      thalf = SUI_TCONVERT_ULONG;
      break;
    case SUI_TYPE_USHORT:
      h_ulong = (unsigned long)*((unsigned short *)inptr);
      thalf = SUI_TCONVERT_ULONG;
      break;
    case SUI_TYPE_UINT:
      h_ulong = (unsigned long)*((unsigned int *)inptr);
      thalf = SUI_TCONVERT_ULONG;
      break;
    case SUI_TYPE_ULONG:
      h_ulong = (unsigned long)*((unsigned long *)inptr);
      thalf = SUI_TCONVERT_ULONG;
      break;
    case SUI_TYPE_COLOR:
      h_ulong = (long)*((sui_color_t *)inptr);
      thalf = SUI_TCONVERT_LONG;
      break;
    case SUI_TYPE_FLAGS:
      h_ulong = (unsigned long)*((sui_flags_t *)inptr);
      thalf = SUI_TCONVERT_ULONG;
      break;
    case SUI_TYPE_ALIGN:
      h_ulong = (unsigned long)*((sui_align_t *)inptr);
      thalf = SUI_TCONVERT_ULONG;
      break;
    case SUI_TYPE_ASCII:
        // h_utf8 = *(utf8 **)inptr; /* FIXME: create new dynamic utf8 string */
      h_utf8 = sui_utf8_dup(inptr);
      thalf = SUI_TCONVERT_UTF8;
      break;

    case SUI_TYPE_UTF8:
      h_utf8 = *(utf8 **)inptr;
      sui_utf8_inc_refcnt(h_utf8);	/* ? h_utf8 == NULL ? */
      thalf = SUI_TCONVERT_UTF8;
      break;
    default:
      return SUI_RET_ETYPE;	/* all unsupported types */
  }
/* determine output subtype */
  switch(outtype) {
    case SUI_TYPE_CHAR:
    case SUI_TYPE_SHORT:
    case SUI_TYPE_INT:
    case SUI_TYPE_LONG:
    case SUI_TYPE_COORD:
    case SUI_TYPE_COLOR:
      tout = SUI_TCONVERT_LONG;
      break;
    case SUI_TYPE_UCHAR:
    case SUI_TYPE_USHORT:
    case SUI_TYPE_UINT:
    case SUI_TYPE_ULONG:
    case SUI_TYPE_TYPE:
    case SUI_TYPE_FLAGS:
    case SUI_TYPE_ALIGN:
      tout = SUI_TCONVERT_ULONG;
      break;
    case SUI_TYPE_UTF8:
      tout = SUI_TCONVERT_UTF8;
      break;
    default:
      if (h_utf8) sui_utf8_dec_refcnt(h_utf8);
      return SUI_RET_ETYPE;
  }

/* convert matrix */
  if (thalf != tout) { /* types are different -> type convert is needed */
    if (thalf == SUI_TCONVERT_LONG) {
      if (tout == SUI_TCONVERT_ULONG) {
        if (h_long < 0) {	/* long is negative - out of range */
          ret = SUI_RET_EOORN;
        } else {
          h_ulong = (unsigned long) h_long;
        }
      } else if (tout == SUI_TCONVERT_UTF8) { /* string from number */
        char *txt = NULL;
        ret = sui_long_to_str(&h_long, &txt);
        if (ret == SUI_RET_OK) {
          h_utf8 = sui_utf8_dup((utf8 *) txt); /* get it as static utf8 text */
          if (h_utf8 == NULL) /* error - utf8 wasn't created */
            ret = SUI_RET_ERR;
        }
        if (txt) free(txt);
      } else { /* unknown type ?!?!? */
        ret = SUI_RET_ETYPE;
      }
    } else if (thalf == SUI_TCONVERT_ULONG) {
      if (tout == SUI_TCONVERT_LONG) {
        if (h_ulong > 0x7fffffff) {
          ret = SUI_RET_EOORP;
        } else {
          h_long = (long) h_ulong;
        }
      } else if (tout == SUI_TCONVERT_UTF8) { /* string from number */
        char *txt = NULL;
        ret = sui_ulong_to_str(&h_ulong, &txt);
        if (ret == SUI_RET_OK) {
          h_utf8 = sui_utf8_dup((utf8 *) txt); /* get it as static utf8 text */
          if (h_utf8 == NULL) /* error - utf8 wasn't created */
            ret = SUI_RET_ERR;
        }
        if (txt) free(txt);
      } else { /* unknown type ?!?!? */
        ret = SUI_RET_ETYPE;
      }
    } else if (thalf == SUI_TCONVERT_UTF8) {
      char *txt = (char *) sui_utf8_get_text(h_utf8); /* utf8char == unsigned char */
      if (tout == SUI_TCONVERT_LONG) {          /* number from string */
        ret = sui_str_to_long(&txt, &h_long);
      } else if (tout == SUI_TCONVERT_ULONG) { /* number from string */
        ret = sui_str_to_ulong(&txt, &h_ulong);
      } else { /* unknown type ?!?!? */
        ret = SUI_RET_ETYPE;
      }
    } else { /* unknown type ?!?!? */
      ret = SUI_RET_ETYPE;
    }
  }

/* create output type - guard limit ??? */
  if (ret == SUI_RET_OK) {
    switch(outtype) {
      case SUI_TYPE_CHAR:
        *((char *)outptr) = (char) h_long;
        break;
      case SUI_TYPE_SHORT:
        *((short *)outptr) = (short) h_long;
        break;
      case SUI_TYPE_INT:
        *((int *)outptr) = (int) h_long;
        break;
      case SUI_TYPE_LONG:
        *((long *)outptr) = (long) h_long;
        break;
      case SUI_TYPE_COORD:
        *((sui_coordinate_t *)outptr) = (sui_coordinate_t) h_long;
        break;
      case SUI_TYPE_COLOR:
        *((sui_color_t *)outptr) = (sui_color_t) h_long;
        break;

      case SUI_TYPE_UCHAR:
        *((unsigned char *)outptr) = (unsigned char) h_ulong;
        break;
      case SUI_TYPE_USHORT:
        *((unsigned short *)outptr) = (unsigned short) h_ulong;
        break;
      case SUI_TYPE_UINT:
        *((unsigned int *)outptr) = (unsigned int) h_ulong;
        break;
      case SUI_TYPE_ULONG:
        *((unsigned long *)outptr) = (unsigned long) h_ulong;
        break;
      case SUI_TYPE_FLAGS:
        *((sui_flags_t *)outptr) = (sui_flags_t) h_ulong;
        break;
      case SUI_TYPE_ALIGN:
        *((sui_align_t *)outptr) = (sui_align_t) h_ulong;
        break;
      case SUI_TYPE_UTF8:
        sui_utf8_inc_refcnt(h_utf8);
        if (*(utf8 **)outptr) sui_utf8_dec_refcnt(*(utf8 **)outptr);
        *(utf8 **)outptr = h_utf8;
        break;

      case SUI_TYPE_TYPE:
        *((sui_typeid_t *)outptr) = (sui_typeid_t) h_ulong;
        break;
    }
  }
  if (h_utf8) sui_utf8_dec_refcnt(h_utf8);
  return ret;
}
