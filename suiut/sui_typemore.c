/* sui_typemore.c
 *
 * SUITK object and method inheritance base.
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#include <string.h>
#include "sui_objbase.h"
#include "sui_typereg.h"
#include "sui_typebase.h"
#include "sui_typemethods.h"
#include "sui_types.h"
#include "ul_utmalloc.h"
#include <suitk/suitk.h>

/******************************************************************************/
/* Member methods for base types without vmt pointer */


/******************************************************************************/

//SUI_TYPE_DECLARE_REFCNT(utf8	,SUI_TYPE_UTF8,	 sui_utf8_t,	 NULL,
//SUI_TYPE_DECLARE_REFCNT(dbuff	,SUI_TYPE_DBUFF,	 void *, NULL,
SUI_TYPE_DECLARE_SIMPLE(dbuff, SUI_TYPE_DBUFF, void *, NULL)

SUI_TYPE_DECLARE_SIMPLE(refcnt,	SUI_TYPE_REFCNT,
			sui_refcnt_t,	&sui_long_vmt_data)

SUI_TYPE_DECLARE_SIMPLE(coord,	SUI_TYPE_COORD,
			sui_coordinate_t, &sui_long_vmt_data)

SUI_TYPE_DECLARE_SIMPLE(color,	SUI_TYPE_COLOR,
			sui_color_t,	&sui_long_vmt_data)

SUI_TYPE_DECLARE_SIMPLE(align,	SUI_TYPE_ALIGN,
			sui_align_t,	&sui_long_vmt_data)

SUI_TYPE_DECLARE_SIMPLE(flags,	SUI_TYPE_FLAGS,
			sui_flags_t,	&sui_ulong_vmt_data)

SUI_TYPE_DECLARE_SIMPLE(type,	SUI_TYPE_TYPE,
			sui_typeid_t,	&sui_long_vmt_data)

SUI_TYPE_DECLARE_SIMPLE(ascii,	SUI_TYPE_ASCII,
			char *,	&sui_utf8_vmt_data)

/* Basic structure types */

SUI_TYPE_DECLARE_SIMPLE(point,		SUI_TYPE_POINT,
			sui_point_t,		NULL)

SUI_TYPE_DECLARE_SIMPLE(rect,		SUI_TYPE_RECT,
			sui_rect_t,		NULL)

SUI_TYPE_DECLARE_REFCNT(colortable,	SUI_TYPE_COLORTABLE,
			sui_colortable_t,	NULL,
			sui_colortable_inc_refcnt,	sui_colortable_dec_refcnt)

SUI_TYPE_DECLARE_REFCNT(fontinfo,	SUI_TYPE_FONTINFO,
			sui_font_info_t,	NULL,
			sui_fonti_inc_refcnt,		sui_fonti_dec_refcnt)

/******************************************************************************/
/* choice need its own destroy function, all other functions are the same */

void sui_choice_vmt_done(sui_obj_t *self)
{
	sui_choice_t *ch = (sui_choice_t *) self;
	if ( ch->text)
		sui_utf8_dec_refcnt( ch->text);
//	free( self);
}

SUI_TYPE_DECLARE_SIMPLE_EX(choice,             SUI_TYPE_CHOICE,
                       sui_choice_t,           NULL,
                       NULL /*init*/,          sui_choice_vmt_done)

/******************************************************************************/



SUI_TYPE_DECLARE_REFCNT(choicetable,	SUI_TYPE_CHOICETABLE,
			sui_choicetable_t,	NULL,
			sui_choicetable_inc_refcnt,	sui_choicetable_dec_refcnt)

SUI_TYPE_DECLARE_REFCNT(format,		SUI_TYPE_FORMAT,
			sui_finfo_t,		NULL,
			sui_finfo_inc_refcnt,		 sui_finfo_dec_refcnt)

SUI_TYPE_DECLARE_REFCNT(style,		SUI_TYPE_STYLE,
			sui_style_t,		NULL,
			sui_style_inc_refcnt,		sui_style_dec_refcnt)

SUI_TYPE_DECLARE_REFCNT(suffix,		SUI_TYPE_SUFFIX,
			sui_suffix_t,		NULL,
			sui_suffix_inc_refcnt,		sui_suffix_dec_refcnt)

//SUI_TYPE_DECLARE_REFCNT(dinfo,		SUI_TYPE_DINFO,
//			sui_dinfo_t,		NULL,
//			sui_dinfo_inc_refcnt,		sui_dinfo_dec_refcnt)

SUI_TYPE_DECLARE_REFCNT(image,		SUI_TYPE_IMAGE,
			sui_image_t,		NULL,
			sui_image_inc_refcnt,		sui_image_dec_refcnt)

SUI_TYPE_DECLARE_SIMPLE( keyfcn, SUI_TYPE_KEYFCN,
			sui_hkey_t *, NULL)

SUI_TYPE_DECLARE_SIMPLE(key,		SUI_TYPE_KEY,
			sui_key_action_t,	NULL)

SUI_TYPE_DECLARE_REFCNT(keytable,	SUI_TYPE_KEYTABLE,
			sui_key_table_t,	NULL,
			sui_key_table_inc_refcnt,	sui_key_table_dec_refcnt)

SUI_TYPE_DECLARE_SIMPLE(event   ,SUI_TYPE_EVENT,
			sui_event_t,		NULL)

SUI_TYPE_DECLARE_REFCNT(table, SUI_TYPE_TABLE,
                        sui_table_t, NULL,
                        sui_table_inc_refcnt, sui_table_dec_refcnt)

SUI_TYPE_DECLARE_SIMPLE_EX(column, SUI_TYPE_COLUMN,
                        sui_table_column_t, NULL,
                        NULL /*init*/, sui_table_column_done)


SUI_TYPE_DECLARE_REFCNT(prnfilter, SUI_TYPE_PRNFILTER,
      sui_print_filter_t,  NULL,
      sui_pfilter_inc_refcnt, sui_pfilter_dec_refcnt)
SUI_TYPE_DECLARE_REFCNT(prnlayout, SUI_TYPE_PRNLAYOUT,
      sui_print_layout_t,  NULL,
      sui_playout_inc_refcnt, sui_playout_dec_refcnt)


SUI_TYPE_DECLARE_SIMPLE_EX(gset, SUI_TYPE_GSET,
                        sui_graph_dataset_t, NULL,
                        NULL /*init*/, sui_graph_dset_done)

SUI_TYPE_DECLARE_REFCNT(gdata, SUI_TYPE_GDATA,
                        sui_graph_data_t, NULL,
                        sui_graph_data_inc_refcnt, sui_graph_data_dec_refcnt)
