/* sui_utf8.h
 *
 * SUITK UTF8 strings.
 *
 * UTF8 string can be in several types. 
 *
 *  Allowable types of utf8 string :
 *    1) str = pointer to static raw utf8 text
 *    2) str = pointer to dynamic structure with pointer 
 *             to static raw utf8 text
 *    3) str = pointer to dynamic structure with pointer
 *             to dynamic raw utf8 text
 *    4) str = pointer to static structure with static
 *             raw utf8 text included into structure (!!! no pointer !!!)
 *    5) str = pointer to static or dynamic structure (refcnt = negative means static)
 *             with index (or pointer) to NLS table
 *  Not allowable is
 *       str = pointer to dynamic structure with raw utf8 string inside structure
 *
 *  If the utf8 string is static or dynamic with unspecified char length (== -1), the 
 *  string must be zero ending. This char is neither counted into char length nor counted
 *  into byte length. The capacity must count with the zero.
 *
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#include "sui_utf8.h"
#include <string.h>
#include <inttypes.h>

/* This pointer is placed into BSS => it does not impose inclusion of utf8 VMT if it is not used */
/* FIXME: should be sui_intptr_t in fact */
intptr_t *sui_utf8_global_instances_counter;

static inline void sui_utf8_inc_instances_counter(void)
{
  if(sui_utf8_global_instances_counter)
    (*sui_utf8_global_instances_counter)++;
}

static inline void sui_utf8_dec_instances_counter(void)
{
  if(sui_utf8_global_instances_counter)
    if(*sui_utf8_global_instances_counter>0)
      (*sui_utf8_global_instances_counter)--;
}

/**
 * sui_utf8_to_ucs - Low-level transform one char from utf8 to unicode
 * @utf: Pointer to input utf8 char.
 * @ucs: Pointer to output unicode char.
 *
 * The function transforms one utf8 char to unicode char (which is
 * placed to ucs pointer) and return size in bytes of transformed utf8 char.
 * Return Value: The function returns size in bytes of transformed utf8 char.
 * File: sui_utf8.c
 */
  int sui_utf8_to_ucs( const utf8char *utf, _UTF_UCS_TYPE *ucs) {
    unsigned char bit = 0;
    unsigned int chr = 0;
    int ret = 0;
    unsigned char *utf_int = (unsigned char *)utf;
    if ( *utf_int < 0x80) { /* only one byte */
      chr = (unsigned int) *utf_int++;
      ret = 1;
    } else {  /* multi-bytes */
      if (( *utf_int & 0xe0) == 0xc0) { /* 2 bytes */
        bit = 0; chr = ((unsigned int)(*utf_int++ & 0x1f))<<6;
        ret = 2;
      } else {
        if (( *utf_int & 0xf0) == 0xe0) { /* 3 bytes */
          bit = 6; chr = ((unsigned int)(*utf_int++ & 0x0f))<<12;
          ret = 3;
        } 
      #if UTF8_MAX_BYTES_IN_CHAR == 6 /* only for 32bit UCS */
        else {
          if (( *utf_int & 0xf8) == 0xf0) { /* 4 bytes */
            bit = 12; chr = ((unsigned int)(*utf_int++ & 0x07))<<18;
            ret = 4;
          } else {
            if (( *utf_int & 0xfc) == 0xf8) { /* 5 bytes */
              bit = 18; chr = ((unsigned int)(*utf_int++ & 0x03))<<24;
              ret 5;
            } else {
              if (( *utf_int & 0xfe) == 0xfc) { /* 6 bytes */
                bit = 24; chr = ((unsigned int)(*utf_int++ & 0x01))<<30;
                ret = 6;
              } else { /* ERROR - not from first byte of utf8 char, .... */
                *ucs = -1;
                return ret;
              }
            }
          }
        }
      #endif
      }
      while( 1) {
        chr += ((unsigned int)(*utf_int++ & 0x3f))<<bit;
        if ( !bit) break;
        bit -= 6;
      }
    }
    *ucs = chr;
    return ret;
  }

/**
 * sui_utf8_from_ucs - Low-level transform one char from unicode to utf8
 * @utf: Pointer to output utf8 char.
 * @ucs: Unicode char.
 *
 * The function transforms one unicode char to utf8 char and return 
 * size of utf8 char.
 * Return Value: The function returns size in bytes of transformed utf8 char.
 * File: sui_utf8.c
 */
  int sui_utf8_from_ucs( utf8char *utf, _UTF_UCS_TYPE ucs) {
    unsigned char bit;
    int size;
    unsigned char *utf_int = (unsigned char *)utf;

    if ( ucs < 0x00000080) { /* one byte */
      *utf_int = (unsigned char) (ucs & 0x7f);
      return 1;
    } else { /* multi-bytes */
      if ( ucs < 0x00000800) { /* 2 bytes */
        size = 2; *utf_int++ = 0xc0 + (((unsigned char)(ucs>>6)) & 0x1f); bit = 0;
      } else {
      #if UTF8_MAX_BYTES_IN_CHAR == 6 /* only for 32bit UCS */
        if ( ucs < 0x00010000) { /* 3 bytes - NOTE: In spec. codes 0xd800-0xdfff and 0xfffe,0xffff */
                                /*                  aren't defined in utf8 !!! */
          size = 3; *utf_int++ = 0xe0 + (((unsigned char)(ucs>>12)) & 0x0f); bit = 6;
        } else {
          if( ucs < 0x00200000) { /* 4 bytes */
            size = 4; *utf_int++ = 0xf0 + (((unsigned char)(ucs>>18)) & 0x07); bit = 12;
          } else {
            if ( ucs < 0x04000000) { /* 5 bytes */
              size = 5; *utf_int++ = 0xf8 + (((unsigned char)(ucs>>24)) & 0x03); bit = 18;
            } else {
              if ( ucs < 0x80000000) { /* 6 bytes */
                size = 6; *utf_int++ = 0xfc + (((unsigned char)(ucs>>30)) & 0x01); bit = 24;
              } else { /* ERROR */
                return 0;
              }
            }
          }
        }
      #else         /* only for 16bit UCS */
        size = 3; *utf_int++ = 0xe0 + (((unsigned char)(ucs>>12)) & 0x0f); bit = 6;
      #endif        
      }
      while(1) {
        *utf_int++ = 0x80 + (((unsigned char)(ucs>>bit)) & 0x3f);
        if ( !bit) break;
        bit -= 6;
      }
      return size;
    }
  }

/**
 * sui_utf8_count_chars - Low-level count chars in utf8 string
 * @utf: Pointer to data of utf8 string.
 * @bytes: Length of counted string in bytes or -1 for count to zero ending.
 *
 * The function counts chars in utf8 string. It can count string up to zero
 * end or only in entered count of bytes from str. Only full chars are 
 * counted.
 * Return Value: The function returns count of chars in utf8 string.
 * File: sui_utf8.c
 */  
  int sui_utf8_count_chars( const utf8char *utf, int bytes) {
    int chrs = 0, add;
    unsigned char *utf_int = (unsigned char *)utf;
    if (!utf) return 0;
    while (bytes) { /* bytes<0 => don't check according to bytes, or bytes>0 => count up to bytes */
      if (!*utf_int)
        break;
      add = 1;
      if (( *utf_int & 0xc0) == 0xc0) {
        add++;
        if ( *utf_int & 0x20) {
          add++;
        #if UTF8_MAX_BYTES_IN_CHAR == 6 /* only for 32bit UCS */
          if ( *utf_int & 0x10) {
            add++;
            if ( *utf_int & 0x08) {
              add++;
              if ( *utf_int * 0x04) {
                add++;
              }
            }
          }
        #endif
        }
      }
      chrs++; /* count only good chars and last char even if it isn't full ??? */
      utf_int += add;
      if (bytes>0) {
        bytes -= add;
        if (bytes < 0) {
          chrs--; /* Last char must be entire too. */
          break;
        }
      }
    }
    return chrs;
  }
  
/**
 * sui_utf8_count_bytes - Low-level count bytes in utf8 string
 * @utf: Pointer to data of utf8 string.
 * @chars: Chars in counted string or -1 for count to zero ending.
 *
 * The function counts length in bytes of utf8 string. It can count 
 * string up to zero end or only entered 'characters'. Only full characters are 
 * counted.
 * Return Value: The function returns bytes of utf8 string without terminating zero (if it exists).
 * File: sui_utf8.c
 */  
  int sui_utf8_count_bytes( const utf8char *utf, int chars) {
    int bytes = 0, add;
    unsigned char *utf_int = (unsigned char *)utf;
    if (!utf_int) return 0;
    while (chars) {
      if(!*utf_int)
        break;
      add = sui_utf8_char_size((const utf8char *)utf_int);
      if (add > 0) {
        bytes += add;
        utf_int += add;
        if (chars>0) chars--;
      } else {  /* error in utf stream */
        bytes = -bytes; /* negative value of number of correctly counted characters.  */
        break;
      }
    }
    return bytes;
  }

/**
 * sui_utf8_char_size - Low-level count bytes of one utf8 character
 * @utf: Pointer to data of utf8 string.
 *
 * The function counts length in bytes of one utf8 character. 
 * If the 'utf' stream hasn't allowable value function returns -1.
 *
 * Return Value: The function returns bytes of utf8 character or -1.
 * File: sui_utf8.c
 */
  int sui_utf8_char_size( const utf8char *utf) {
    int ret = 1;
    unsigned char *utf_int = (unsigned char *)utf;

    if (( *utf_int & 0xc0) == 0x80) return -1; /* error - not first byte in char */
    if (( *utf_int & 0xc0) == 0xc0) {
      ret++;
      if ( *utf_int & 0x20) {
        ret++;
      #if UTF8_MAX_BYTES_IN_CHAR == 6 /* only for 32bit UCS */
        if ( *utf_int & 0x10) {
          ret++;
          if ( *utf_int & 0x08) {
            ret++;
            if ( *utf_int & 0x04) {
              ret++;
            }
          }
        }
      #endif
      }
    }
    return ret;
  }

/**
 * sui_utf8_to_ucs_backward - Low-level transform one char from utf8 to unicode backwardly
 * @utf: Pointer which points beyond input utf8 char.
 * @ucs: Pointer to output unicode char.
 *
 * The function transforms one utf8 char to unicode char (which is
 * placed to ucs pointer) and return size in bytes of transformed utf8 char.
 * The $utf pointer points after converted utf8 character.
 * Return Value: The function returns size in bytes of transformed utf8 char.
 * File: sui_utf8.c
 */
int sui_utf8_to_ucs_backward( const utf8char *utf, _UTF_UCS_TYPE *ucs)
{
  unsigned long chr = 0;
  int ret = 0, shift = 0;
  unsigned char *utf_int = (unsigned char *)utf;

  utf_int--; /* the input pointer $utf points after converted character */

  while((*utf_int & 0xC0) == 0x80) {
    chr |= (((unsigned long)*utf_int)&0x0000003f)<<shift;
    shift += 6;
    ret++;
    utf_int--;
  #if UTF8_MAX_BYTES_IN_CHAR == 6
    if (ret==6) /* error - too many bytes */
  #else
    if (ret==3)
  #endif
    {
      if (ucs) *ucs = 0;
      return -1;
      break;
    }
  }

  if (!(*utf_int & 0x80) && (ret==0)) {
    chr = ((unsigned long)*utf_int)&0x0000007f;
  } else if (((*utf_int & 0xe0) == 0xc0) && (ret==1)) {
    chr |= (((unsigned long)*utf_int)&0x0000001f)<<shift;
  } else if (((*utf_int & 0xf0) == 0xe0) && (ret==2)) {
    chr |= (((unsigned long)*utf_int)&0x0000000f)<<shift;
#if UTF8_MAX_BYTES_IN_CHAR == 6
  } else if (((*utf_int & 0xf8) == 0xf0) && (ret==3)) {
    chr |= (((unsigned long)*utf_int)&0x00000007)<<shift;
  } else if (((*utf_int & 0xfc) == 0xf8) && (ret==4)) {
    chr |= (((unsigned long)*utf_int)&0x00000003)<<shift;
  } else if (((*utf_int & 0xfe) == 0xfc) && (ret==5)) {
    chr |= (((unsigned long)*utf_int)&0x00000001)<<shift;
#endif
  } else {
    chr = 0;
    ret = -2;
  }
  if (ucs) *ucs = chr;
  return ret+1;
}


/**
 * sui_utf8_get_type - Get type of utf8 string.
 * @utf: Pointer to utf8 string.
 *
 * The function tests utf8 string for static or dynamic type. 
 * The function distinguishes between dynamic utf8 structure with static text and
 * dynamic utf8 structure with dynamic text.
 * Return Value: The function returns :
 * UTF8_STATIC_RAW for static raw utf8 string,
 * UTF8_INCLUDED_TEXT for static struct with embedded raw text,
 * UTF8_NLS_STRING for dynamic/static struct with index to NLS table,
 * UTF8_DYNAMIC_STRUCT for dynamic struct with pointer to static text,
 * UTF8_DYNAMIC_FULL for dynamic struct with pointer to dynamic text.
 * File: sui_utf8.c
 */
  int sui_utf8_get_type( const utf8 *utf) {
		if ( !utf) return -1;
    if (( utf->type & UTF8_TYPE_MASK) != UTF8_TYPE_STRUCT) { /* raw utf text */
      return UTF8_STATIC_RAW;      /* static raw text */
    } else {
      if ( utf->type & UTF8_TYPE_INCLUDED_TEXT)
        return UTF8_INCLUDED_TEXT;    /* static struct with embedded raw text */
      else {
        if ( utf->type & UTF8_TYPE_NLS_TEXT) {
          return UTF8_NLS_STRING;
        } else {
          if (utf->cap_bytes<0)
            return UTF8_DYNAMIC_STRUCT;  /* dynamic struct with pointer to static text */
          else
            return UTF8_DYNAMIC_FULL;  /* dynamic struct with pointer to dynamic text */
        }
      }
    }
  }

/**
 * sui_utf8_inc_refcnt - Increment utf8 string reference counter
 * @utf: Pointer to utf8 string.
 *
 * The function increments utf8 string reference counter.
 * Return Value: The function does not return a value.
 * File: sui_utf8.c
 */
sui_refcnt_t sui_utf8_inc_refcnt( utf8 *utf)
{
	if ( !utf) return SUI_REFCNTERR;
	if ( sui_utf8_get_type( utf) == UTF8_STATIC_RAW) return SUI_STATIC;
	if ( utf->refcnt >= 0) utf->refcnt++; /* increment refcnt in dynamic struct ONLY */
	return utf->refcnt;
}
 
/**
 * sui_utf8_dec_refcnt - Decrement utf8 string reference counter
 * @utf: Pointer to utf8 string.
 *
 * The function decrements dynamic utf8 string reference counter 
 * described by utf8 structure. If the counter is decremented to 
 * zero the string structure will be deleted.
 * Return Value: The function does not return a value.
 * File: sui_utf8.c
 */
sui_refcnt_t sui_utf8_dec_refcnt( utf8 *utf)
{
	int type;
	sui_refcnt_t ref;
	if ( !utf) return SUI_REFCNTERR;
	if (( type = sui_utf8_get_type( utf)) == UTF8_STATIC_RAW) return SUI_STATIC;
	if ( utf->refcnt > 0) utf->refcnt--;
	if ( !(ref = utf->refcnt)) { /* delete */
		if ( type == UTF8_DYNAMIC_FULL) {
			if ( utf->data.ptr)
				free( utf->data.ptr);
			utf->data.ptr = NULL;
		}
		free( utf);
		sui_utf8_dec_instances_counter();
	}
	return ref;
}

  
/**
 * sui_utf8_get_text - Get pointer to raw utf8 text.
 * @utf: Pointer to utf8 string.
 *
 * The function returns pointer to raw text from whatever 
 * allowable type of utf8 string.
 * Return Value: The function returns pointer to raw utf8 text or NULL for error.
 * File: sui_utf8.c
 */
  utf8char *sui_utf8_get_text( utf8 *utf) {
    if ( !utf) return NULL;
    switch( sui_utf8_get_type( utf)) {
      case UTF8_STATIC_RAW: /* static raw string */
        return (utf8char *)utf;
      case UTF8_INCLUDED_TEXT:
        return &utf->data.text[0];
      case UTF8_NLS_STRING:
        return nls_table[utf->data.nidx];
        /*return NULL;        // ???NLS??? */
      case UTF8_DYNAMIC_STRUCT: case UTF8_DYNAMIC_FULL:
        return utf->data.ptr;
    }
    return NULL;
  }
  
  
/**
 * sui_utf8_length - Get length of utf8 string in characters.
 * @utf: Pointer to utf8 string.
 *
 * The function counts characters in utf8 string. String can be
 * whatever allowable type of utf8 string. Function counts chars
 * up to zero ending char.
 * Return Value: The function returns count of characters.
 * File: sui_utf8.c
 */
  int sui_utf8_length( utf8 *utf) {
    int res;
    if ( !utf) return 0;
    if ( sui_utf8_get_type( utf) == UTF8_STATIC_RAW) {
      res = sui_utf8_count_chars( sui_utf8_get_text( utf), -1);
    } else {
      if (( res = utf->chars) < 0) {
        res = sui_utf8_count_chars( sui_utf8_get_text( utf), utf->bytes);
      }
    }
    return res;
  }

/**
 * sui_utf8_bytesize - Get length of utf8 string in bytes.
 * @utf: Pointer to utf8 string.
 *
 * The function counts bytes in utf8 string. String can be
 * whatever allowable type of utf8 string. Function counts bytes
 * up to zero ending char for strings with utf->bytes=-1.
 * Return Value: The function returns count of bytes always without terminating zero or a negative value of correctly counted characters.
 * File: sui_utf8.c
 */
  int sui_utf8_bytesize( utf8 *utf) {
    int res;
    if ( !utf) return 0;
    if ( sui_utf8_get_type( utf) == UTF8_STATIC_RAW) {
      res = sui_utf8_count_bytes( sui_utf8_get_text( utf), -1);
    } else {
      if (( res = utf->bytes) < 0) {
       res = sui_utf8_count_bytes( sui_utf8_get_text( utf), utf->chars);
      }
    }
    return res;
  }

/**
 * sui_utf8_capacity - Get allocated space for string in bytes.
 * @utf: Pointer to utf8 string.
 *
 * The function returns allocated space for string in bytes or 
 * zero for static text.
 * Return Value: The function returns capacity of utf8 string in bytes.
 * File: sui_utf8.c
 */
  int sui_utf8_capacity( utf8 *utf) {
    if ( !utf || sui_utf8_get_type( utf) != UTF8_DYNAMIC_FULL) return 0;
    return utf->cap_bytes;
  }

/**
 * sui_utf8_setlength - Set length of UTF8 string in characters (it can only make the string shorter)
 * @utf: Pointer to utf8 string
 * @chars: New size of string in characters
 * The function sets new size of string (if the string is dynamic and string's capacity
 * is sufficient). Size of string in bytes is automatically recounted. If the string's
 * capacity is greater terminating zero character will be added.
 * Return Value: The function returns a new size of string in characters.
 */
int sui_utf8_setlength(utf8 *utf, int chars)
{
  int ret = 0;
  utf8char *putf;
  if (!utf) return -1;
  ret = sui_utf8_get_type(utf);
  if (ret != UTF8_DYNAMIC_STRUCT && ret != UTF8_DYNAMIC_FULL) {
    return sui_utf8_length(utf);
  }
  putf = sui_utf8_get_text(utf);
  if (ret == UTF8_DYNAMIC_STRUCT) {
    ret = sui_utf8_length(utf);
    if (chars<ret) {
      ret = utf->chars = chars; /* shorter (static) string without terminating zero */
      utf->bytes = sui_utf8_count_bytes(putf, chars);
    }
  } else { /* dynamic string */
    ret = sui_utf8_count_bytes(putf,chars);
    if (ret >= utf->cap_bytes) {
      utf->chars = sui_utf8_count_chars(putf,utf->cap_bytes);
      utf->bytes = sui_utf8_count_bytes(putf,utf->chars);
    } else {
      utf->chars = chars;
      utf->bytes = ret;
      *(putf+ret)=0;
    }
    ret = utf->chars;
  }
  return ret;
}

/**
 * sui_utf8_ntoken - find next token after $delim character (similar to strtok)
 * @utf: pointer to pointer to a raw utf8 string (pointer is set to the next token)
 * @strlen: pointer to length of a raw utf8 string [in characters] (it can be NULL or it can point to -1 for entire string)
 * @delim: pointer to UTF8 character which is searched as a delimiter
 * @cntchar: pointer to an output buffer for size of part [in characters] or NULL
 * The function finds the first delimiter character and returns pointer after to
 * searched delimiter or NULL if the delimiter isn't in the string. Size [in characters]
 * of the token is returned in the $cntchar.
 * The function updates $utf pointer to the next token and if there isn't another token it
 * is set to NULL. The function also updates value of $strlen pointer with number of remaining
 * characters if the pointer is set.
 * Return Value: The function returns pointer to the current token (part) of a UTF8 string if it contains a delimiter.
 *
 * Funkce se chova stejne s i bez zadane delky retezce. Pri zadane delce retezce se ignoruje
 * ukoncovaci nula retezce a cte se az do cele zadane delky, necte se za uvedenou delkou retezce.
 * Funkce vraci ukazatel na token (vstupni pointer) a velikost tokenu ve znacich v $cntchar a
 * pointer $utf je aktualizovan na dalsi token nebo na NULL, pokud byl cely retezec rozebran.
 * Funkce vraci postupne pro retezec "/", pri hledani tokenu podle '/', nasledujici hodnoty
 * 1) token_ptr = "/", token_size = 0, nexttoken_ptr = "", remain_size = 0
 * 2) token_ptr = "", token_size = 0, nexttoken_ptr = NULL, remain_size = 0
 * - Smycka pro rozlozeni celeho retezce by mela reagovat na podminku aktualizovaneho
 * pointeru na dalsi zbytek retezce $utf == NULL, ale i pri tom je vraceny token (jeho velikost)
 * platny. Velikost $cntchar je rovno nule, pokud do dalsiho oddelovace nebo konce retezce nebyl zadny znak.
 * Takze pokud v retezci (jako v prikladu) je pouze oddelovac, funkce postupne
 * vrati dva tokeny nulove delky (pred oddelovacem a za oddelovacem) a pri druhem
 * navratu z funkce je $utf aktualizovano na NULL.
 * Pro prazdny retezec (nebo retezec nulove delky) je vracen jed token nulove delky a $utf je nastaveno na NULL.
 */
utf8char *sui_utf8_ntoken(utf8char **utf, int *strlen, utf8char *delim, int *cntchar)
{
  utf8char *p = NULL, *t = NULL;
  int st = 0, sc, ss = -1;
  _UTF_UCS_TYPE ucs, ducs;
  do {
    if (!utf || !(p = *utf)) { p = NULL; break; } /* invalid pointer to a string */
    if (strlen) ss = *strlen;
    sui_utf8_to_ucs(delim, &ducs);
    t = p;
    if (ss==0) { p = NULL; break; } /* if string length is set and the string is empty */
    do {
      sc = sui_utf8_to_ucs(p, &ucs);
      if (sc<0) { p = NULL; break; } /* error in character translation */
      if (ss<0 && ucs==0) { p = NULL; break; } /* end of an input string if size of the string isn't specified */
      p += sc;
      if (ss>0) ss--;
      if (ucs==ducs) break; /* delimiter has been reached */
      st++;
      if (ss==0) { p = NULL; break; } /* end of an input string if size of the string is specified */
    } while(1);
  } while(0);
  *utf = p;
  if (cntchar) *cntchar = st;
  if (strlen) *strlen = ss;
  return t;
}

/**
 * sui_utf8_ptoken - find previous token before $delim character
 * @utf: pointer to pointer to a raw string (for function entrace it must be set after string and for output it is updated after to the previous token)
 * @strlen: pointer to length of a raw utf8 string [in characters] and output buffer for remaining characters. If it isn't set or it is set to -1 the function will continue up to first zero character
 * @delim: pointer to UTF8 character which is searched as a delimiter
 * @cntchar: pointer to an output buffer for size of part [in character] or NULL
 * The function finds the last delimiter before $utf and returns pointer after to
 * searched delimiter or NULL if the delimiter isn't in the string.
 * Return Value: The function returns pointer to the next part of a UTF8 string if it contains a delimiter.
 *
 * Funkce ma stejne chovani jako sui_utf8_ntoken, s tim ze parsuje tokeny zpetne.
 * Pokud neni $strlen zadany nebo je -1, tak se parsuje az do nalezeni nuloveho znaku (pred retezcem), coz
 * muze byt i nebezpecne, proto by mel byl $strlen vzdy zadan na velikost retezce.
 */
utf8char *sui_utf8_ptoken(utf8char **utf, int *strlen, utf8char *delim, int *cntchar)
{
  utf8char *p = NULL, *t = NULL;
  int st = 0, sc, ss = -1;
  _UTF_UCS_TYPE ucs, ducs;
  do {
    if (!utf || !(p = *utf)) { p=NULL; break; } /* invalid pointer to a string */
    if (strlen) ss = *strlen;
    sui_utf8_to_ucs(delim, &ducs);
    t = p;
    if (ss==0) { p = NULL; break; } /* if string length is set and the string is empty */
    do {
      sc = sui_utf8_to_ucs_backward(p, &ucs);
      if (sc<0) { p = NULL; break; } /* error in character parsing */
      if (ss<0 && ucs==0) { p = NULL; break; } /* we found beginning of the input string if size of the string isn't specified */
      p -= sc;
      if (ss>0) ss--;

      if (ucs==ducs) break; /* delimiter has been reached - find the next */
      t -= sc;
      st++;
      if (ss==0) { p = NULL; break; } /* end of an input string if size of the string is specified */
    } while(1);
  } while(0);

  *utf = p;
  if (cntchar) *cntchar = st;
  if (strlen) *strlen = ss;
  return t;
}


/******************************************************************************/
/**
 * sui_utf8 - Create and init utf8 string.
 * @text: Pointer to raw utf8 text  for initialize new utf8 string or NULL.
 * @chars: Count of maximal characters in new utf8 string or -1.
 * @bytes: Count of maximal bytes in new utf8 string or -1.
 *
 * The function creates and initializes utf8 string. If the 'text'
 * isn't NULL new utf8 string will be initialize with 'text' in dependence
 * on the 'chars' and the 'bytes'. If the 'chars' and the 'bytes' are both
 * -1 fnc creates string type 2 (dyn.struct with ptr to stat.text).
 * If the 'text' is NULL fnc creates string type 3 (dyn.struct with ptr to 
 * dyn.text) in dependence to the 'chars' or the 'bytes'. 
 * If the 'chars' is positive fnc allocates 'chars'*UTF8_MAX_BYTES_IN_CHAR 
 * bytes. If the 'bytes' is positive fnc allocates 'bytes' bytes.
 * If both are positive fnc allocates count of bytes equal to bigger value of 
 * both numbers.
 * If the 'text' is NULL and the 'chars' and 'bytes' are negative function 
 * returns NULL.
 * If the 'text' isn't NULL and the 'chars' or 'bytes' are positive function
 * fill chars and bytes field in struct to bigger of both.
 * If the 'text' isn't NULL and the 'chars' and 'bytes' are both equal to zero
 * function sets to the chars and bytes field value -1.
 *
 * NOTE: UTF8_MAX_BYTES_IN_CHAR is equal to 3 for 16bit UCS and 6 for 
 *       32bit UCS. UTF8_MAX_BYTES_IN_CHAR is defined in sui_utf8.h.
 *
 * Return Value: The function returns pointer to created utf8 string or NULL.
 * File: sui_utf8.c
 */
utf8 *sui_utf8( utf8char *text, int chars, int bytes)
{
  utf8 *new;

  if ( !text && (chars<0) && (bytes<0)) return NULL;
  if ( !(new = (utf8 *) malloc( sizeof(utf8)))) return NULL;
  memset( new, 0, sizeof(utf8));
  new->type = UTF8_TYPE_STRUCT;
  if ( text) { /* type 2 - dynamic structure with pointer to static text */
    new->data.ptr = text;
    new->cap_bytes = -1;
    if ( chars < 0 && bytes < 0) {
      new->chars = sui_utf8_count_chars( text, -1);
      new->bytes = sui_utf8_count_bytes( text, -1); /* don't count with zero ending */
    } else {
      new->chars = new->bytes = -1;
      if ( chars > 0) {
        new->chars = chars;
        new->bytes = sui_utf8_count_bytes( text, chars);
      }
      if ( bytes > 0) {
        int txt_size = sui_utf8_count_chars( text, bytes);
        if ( new->chars < txt_size) {
          new->chars = txt_size;
          new->bytes = sui_utf8_count_bytes( text, txt_size);
        }
      }
    }
  } else { /* type 3 - dynamic structure with pointer to dynamically allocated space for string */
      new->cap_bytes = chars * UTF8_MAX_BYTES_IN_CHAR;
      if (new->cap_bytes < bytes)
          new->cap_bytes = bytes;
      new->cap_bytes++; /* add space for a terminating zero */
      if (new->cap_bytes>=0) {
          new->data.ptr = malloc(new->cap_bytes);
          if (!new->data.ptr) {
              free(new);
              return NULL;
          } else {
              memset(new->data.ptr, 0, new->cap_bytes);
          }
      }
  }
  sui_utf8_inc_refcnt( new);
  sui_utf8_inc_instances_counter();
  return new;
}
  
/**
 * sui_utf8_dynamic - Create and init utf8 string, guaranteed that it is dynamic one.
 * @text: Pointer to raw utf8 text  for initialize new utf8 string or NULL.
 * @chars: Count of maximal characters in new utf8 string or -1.
 * @bytes: Count of maximal bytes in new utf8 string or -1.
 *
 * This function has similar function as sui_utf8(), but it guarantees
 * that created utf8 string is fully dynamic and does not point to some
 * external static data.
 *
 * Return Value: The function returns pointer to created utf8 string or NULL.
 * File: sui_utf8.c
 */
  utf8 *sui_utf8_dynamic( const utf8char *text, int chars, int bytes) {
    utf8 *new;

    if(!text)
      return sui_utf8( NULL, chars, bytes);

    if( chars < 0)
      chars = sui_utf8_count_chars( text, bytes);

    if( bytes < 0)
      bytes = sui_utf8_count_bytes( text, chars);

    new = sui_utf8( NULL, 0, bytes);
    if( !new)
      return NULL;

    memcpy( new->data.ptr, text, bytes);
    *(new->data.ptr+bytes)=0;
    new->bytes = bytes;
    new->chars = chars;

    return new;
  }

/**
 * sui_utf8_from_unicode - create utf8 string from unicode string
 * @unistr: pointer to unicode string terminated with zero value
 * @len_unistr: number of character in the unicode string or a negative value for the entire string
 */
sui_utf8_t *sui_utf8_from_unicode(_UTF_UCS_TYPE *unistr, int len_unistr)
{
  sui_utf8_t *ret = NULL;
  _UTF_UCS_TYPE *pu = unistr;
  utf8char *txt;
  int add, bytes = 0, chars = 0;

  if (!unistr) return NULL;
  if (len_unistr<0) {
    while (*pu) {
      chars++;
      if (*pu<0x80) bytes += 1;
      else if (*pu<0x800) bytes += 2;
      else
#if UTF8_MAX_BYTES_IN_CHAR == 6
        if (*pu<0x00010000) bytes += 3;
        else if (*pu<0x00200000) bytes += 4;
        else if (*pu<0x04000000) bytes += 5;
        else bytes += 6;
#else
        bytes += 3;
#endif
      pu++;
    }
  } else {
    chars += len_unistr;
    while (len_unistr>0) {
      chars++;
      if (*pu<0x80) bytes += 1;
      else if (*pu<0x800) bytes += 2;
      else
#if UTF8_MAX_BYTES_IN_CHAR == 6
        if (*pu<0x00010000) bytes += 3;
        else if (*pu<0x00200000) bytes += 4;
        else if (*pu<0x04000000) bytes += 5;
        else bytes += 6;
#else
        bytes += 3;
#endif
      pu++;
    }
  }

  ret = sui_utf8(NULL, 0, bytes);
  if (!ret) return NULL;
  ret->chars = chars;
//  ret->bytes = bytes;
  pu = unistr;
  txt = sui_utf8_get_text(ret);
  add = 0;
  while(chars-- && *pu) { /* convert unicode to utf8 */
    add = sui_utf8_from_ucs(txt, *pu);
    txt += add;
    pu++;
  }
  return ret;
}


/**
 * sui_utf8_ncpy - Copy utf8 string from another utf8 string.
 * @dst: Pointer to destination utf8 string.
 * @src: Pointer to source utf8 string.
 * @len: Length of string (in characters or bytes) to copy.
 *
 * The function copyies utf8 string to another utf8 string. If
 * the 'dst' string isn't full dynamic function won't copy
 * string. Function sets the 'chars' and the 'bytes' fields in 
 * the 'dst' utf8 string. If the 'len' is zero function copy all 
 * chars up to zero ending. If the 'len' is great than zero the 
 * 'len' is count of chars to copy. If the 'len' is negative 
 * absolute value of the 'len' is count of bytes to copy - full 
 * chars are copy really.
 * Return Value: The function returns count of copyied chars.
 * File: sui_utf8.c
 */
  int sui_utf8_ncpy( utf8 *dst, utf8 *src, int len) {
    int ret, chrs = 0;
    utf8char *stxt, *dtxt;

    if ( !dst || !src || sui_utf8_get_type(dst) != UTF8_DYNAMIC_FULL) return 0;
    sui_utf8_inc_refcnt( src); sui_utf8_inc_refcnt( dst);
    stxt = sui_utf8_get_text( src); dtxt = sui_utf8_get_text( dst);
    ret = sui_utf8_length(src);
    if ( len == 0) { /* copy entire source string */
      chrs = ret;
    } else {
      if ( len < 0) { /* copy up to $len bytes from source string */
        chrs = sui_utf8_count_chars( stxt, -len);
        if ( ret < chrs) chrs = ret;
      } else {
        if ( len > 0) { /* copy $len characters from source string */
          if ( ret < len) chrs = ret;
          else chrs = len;
        }
      }
    }
    ret = sui_utf8_count_bytes( stxt, chrs);
    if ( ret > (dst->cap_bytes-1)) {
      ret = sui_utf8_count_bytes( stxt, chrs = sui_utf8_count_chars( stxt, dst->cap_bytes-1));
    }
    dst->bytes = ret;
    while ( ret--) {
      *dtxt++ = *stxt++;
    }
    *dtxt = 0; /*add terminating zero */
    ret = dst->chars = chrs;
    sui_utf8_dec_refcnt( src); sui_utf8_dec_refcnt( dst);
    return ret;
  }

/**
 * sui_utf8_dup - Duplicate utf8 string.
 * @utf: Pointer to source utf8 string.
 *
 * The function duplicates source utf8 string and returns pointer
 * to new string.
 * The function always creates new dynamic utf8 string.
 *
 * Return Value: The function returns pointer to duplicated string
 * or NULL.
 * File: sui_utf8.c
 */
utf8 *sui_utf8_dup(utf8 *utf)
{
  int size;
  utf8 *ret = NULL;

  if (!utf) return ret;
  sui_utf8_inc_refcnt(utf);
  size = sui_utf8_bytesize(utf);
  ret = sui_utf8(NULL, -1, size);
  if (ret) {
    size = sui_utf8_ncpy(ret, utf, -size);
    if (size != sui_utf8_length(utf)) {
      sui_utf8_dec_refcnt(ret);
      ret = NULL;
    }
  }
  sui_utf8_dec_refcnt(utf);
  return ret;
}

/**
 * sui_utf8_add_char - Add character from utf8 stream to utf8 string.
 * @utf: Pointer to utf8 string.
 * @chr: Pointer to utf char stream.
 *
 * The function adds utf8 character to utf8 string if string has free
 * allocated space in buffer otherwise it reallocates buffer first.
 * The utf8 char is a raw data. Function returns size of added character
 * or 0 if the character wasn't added.
 *
 * Return Value: The function returns number of added bytes.
 * File: sui_utf8.c
 */
int sui_utf8_add_char(utf8 *utf, utf8char *chr)
{
  int add, last;
  utf8char *inutf;

  if (!utf || (sui_utf8_get_type(utf) != UTF8_DYNAMIC_FULL)) return -1;
  sui_utf8_inc_refcnt(utf);
  add = sui_utf8_char_size(chr);
  if (add > 0) do {
    //if ( utf->bytes == -1) zero = 1; else zero = 0; /* string must have zero terminator */
    last = sui_utf8_bytesize(utf);
    if (utf->cap_bytes <= (last+add)) { /* reallocate buffer to a new capacity (with ending zero) */
      inutf = realloc(utf->data.ptr, utf->cap_bytes+add);
      if (!inutf) {
        add = 0;
        break;
      }
      utf->cap_bytes += add;
      utf->data.ptr = inutf;
    } else
      inutf = sui_utf8_get_text(utf);
    int lastch = sui_utf8_length(utf);
    inutf += last;
    memcpy(inutf, chr, add);
    *((char *)inutf+add) = 0; /* zero end */
    utf->bytes = last + add;
    utf->chars = lastch + 1;
  } while(0);
  sui_utf8_dec_refcnt(utf);
  return add;
}

/**
 * sui_utf8_ncat - Append utf8 string to another utf8 string.
 * @utf: Pointer to destination utf8 string.
 * @add: Pointer to source utf8 string (or utf char stream).
 * @len: Number of characters for appending or -1 for appending entire 'add' string.
 *
 * The function appends utf8 string to another utf8 string if
 * destination string has free allocated space in buffer. If it hasn't a free space
 * the $utf string buffer is reallocated first.
 * Output string will be 'utf' = ['utf','add'].
 * Function returns number of appended characters or 0 if the 'add' string hasn't been added.
 * If the 'len' is positive only '$len characters will be appended.
 *
 * Return Value: The function returns number of appended characters.
 * File: sui_utf8.c
 */
int sui_utf8_ncat(utf8 *utf, utf8 *add, int len)
{
  int ret, size = 0;
  utf8char *atxt, *utxt;

  if (!utf || sui_utf8_get_type(utf) != UTF8_DYNAMIC_FULL) return -1;
  if (len == 0) return 0;
  if (!add) return -1;
  sui_utf8_inc_refcnt(utf); sui_utf8_inc_refcnt(add);

  size = sui_utf8_bytesize(utf);
  atxt = sui_utf8_get_text(add);

  if (len>0)
    ret = sui_utf8_count_bytes(atxt,len); /* get only $len characters */
  else {
    ret = sui_utf8_bytesize(add); /* get entire string */
    len = sui_utf8_length(add);
  }
  if (ret>0) do {
    if (utf->cap_bytes <= size+ret) {
      utxt = realloc(utf->data.ptr, utf->cap_bytes+ret);
      if (!utxt) {
        len = 0;
        break;
      }
      utf->cap_bytes += ret;
      utf->data.ptr = utxt;
    } else
      utxt = sui_utf8_get_text(utf);
    utf->bytes = size+ret;
    utf->chars += len;
    utxt += size;
    memcpy(utxt, atxt, ret);
    *((char *)utxt+ret) = 0;
  } while(0);
  sui_utf8_dec_refcnt(utf);
  sui_utf8_dec_refcnt(add);
  return len; /* return number of appended characters */
}

/**
 * sui_utf8_nconcate - concates two utf8 strings to another one
 * @one: pointer to the first utf8 string
 * @onelen: number of used characters from the first string or -1 for the entire string
 * @two: pointer to the second utf8 string
 * @twolen: number of the used characters from the second string or -1 for the entire string
 * The function concates two strings (or their parts)
 * Return Value: The function returns pointer to a new concated string or NULL.
 */
utf8 *sui_utf8_nconcate(utf8 *one, int onelen, utf8* two, int twolen)
{
  utf8char *dst;
  utf8 *buf;
  int bone, btwo;

  if (onelen < 0) onelen = sui_utf8_length(one);
  bone = sui_utf8_count_bytes(sui_utf8_get_text(one), onelen);
  if (twolen < 0) twolen = sui_utf8_length(two);
  btwo = sui_utf8_count_bytes(sui_utf8_get_text(two), twolen);

  buf = sui_utf8(NULL, -1, bone+btwo);
  if (!buf) return NULL;

  dst = sui_utf8_get_text(buf);
  memcpy(dst, sui_utf8_get_text(one), bone);
  memcpy(dst+bone, sui_utf8_get_text(two), btwo);
  *((char*)dst+bone+btwo) = 0;
  buf->bytes = bone+btwo;
  buf->chars = onelen+twolen;
  return buf;
}


/**
 * sui_utf8_cut - Set length of utf8 string
 * @utf: Pointer to utf8 string.
 * @chars: New length of utf8 string.
 *
 * The function sets length of utf8 string if string is longer and not raw static.
 * If the string is counted, function sets the 'chars' and ' bytes' field
 * in structure, else function sets zero ending into raw text.
 *
 * Return Value: The function returns size of string in bytes.
 * File: sui_utf8.c
 */
/*  int sui_utf8_cut( utf8 *utf, int chars) {
    int ret;
    utf8char *uch;
    
    if ( !utf || sui_utf8_get_type( utf) != UTF8_DYNAMIC_FULL) return 0;
    sui_utf8_inc_refcnt( utf);
    if ( utf->chars >= 0) {
      utf->chars = chars;
      ret = utf->bytes = sui_utf8_bytesize( utf);
    } else {
      ret = sui_utf8_count_bytes( uch = sui_utf8_get_text(utf), chars);
      *(uch+ret) = 0;
      ret++;      
    }
    sui_utf8_dec_refcnt( utf);
    return ret;
  }*/


/**
 * sui_utf8_ncmp - Compare two utf8 strings
 * @one: Pointer to first utf8 string.
 * @two: Pointer to second utf8 string.
 * @chars: Number of characters to compare or -1 for the entire second string.
 *
 * The function compares two utf8 strings and returns 1,0,-1 as memcmp(strcmp)
 *
 * Return Value: The function returns 0 for equal strings.
 * File: sui_utf8.c
 */
int sui_utf8_ncmp(utf8 *one, utf8 *two, int chars)
{
  utf8char *otxt, *stxt;
  _UTF_UCS_TYPE ucs1, ucs2;

  if (one==NULL && two==NULL) return 0; /* strcmp returns 0, too. */
  if (one==NULL || two==NULL) return -2;  /* strcmp invokes SIGSEGV */
  if (chars < 0) {
    chars = sui_utf8_length(two);
    if (chars > sui_utf8_length(one)) return -1;
  }

  otxt = sui_utf8_get_text(one); stxt = sui_utf8_get_text(two);
  while(chars--) {
    otxt += sui_utf8_to_ucs(otxt, &ucs1); stxt += sui_utf8_to_ucs(stxt, &ucs2);
    if (ucs1 < ucs2) return -1;
    if (ucs1 > ucs2) return 1;
  }
  return 0;
}

/**
 * sui_utf8_cmp - Compare two utf8 strings
 * @one: Pointer to first utf8 string.
 * @two: Pointer to second utf8 string.
 *
 * The function compares two utf8 strings and returns 1,0,-1 as memcmp
 *
 * Return Value: The function returns 0 for equal strings.
 * File: sui_utf8.c
 */
int sui_utf8_cmp(utf8 *one, utf8 *two)
{
  utf8char *otxt, *stxt;
  _UTF_UCS_TYPE ucs1, ucs2;
  int len1, len2;

  if (one==NULL && two==NULL) return 0; /* strcmp returns 0, too. */
  if (one==NULL || two==NULL) return -2; /* strcmp invokes SIGSEGV */
  len1 = sui_utf8_length(one);
  len2 = sui_utf8_length(two);
  otxt = sui_utf8_get_text(one); stxt = sui_utf8_get_text(two);
  while(len1 && len2) {
    otxt += sui_utf8_to_ucs(otxt, &ucs1); stxt += sui_utf8_to_ucs(stxt, &ucs2);
    if (ucs1 < ucs2) return -1;
    if (ucs1 > ucs2) return 1;
    len1--; len2--;
  }
  if (len1) return 1;
  if (len2) return -1;
  return 0;
}

/**
 * sui_utf8_casecmp - Compare two utf8 strings (case insensitive)
 * @one: Pointer to first utf8 string.
 * @two: Pointer to second utf8 string.
 *
 * The function compares two utf8 strings (case insensitive) and returns 1,0,-1 as memcmp
 *
 * Return Value: The function returns 0 for equal strings.
 * File: sui_utf8.c
 */
int sui_utf8_casecmp(utf8 *one, utf8 *two)
{
  utf8char *otxt, *stxt;
  _UTF_UCS_TYPE ucs1, ucs2;
  int len1, len2;

  if (!one || !two) return -2; /* WHAT ELSE ??? */
  len1 = sui_utf8_length(one);
  len2 = sui_utf8_length(two);
  otxt = sui_utf8_get_text(one); stxt = sui_utf8_get_text(two);
  while(len1 && len2) {
    otxt += sui_utf8_to_ucs( otxt, &ucs1);
    stxt += sui_utf8_to_ucs( stxt, &ucs2);
    ucs1 = sui_utf8_ucs_toupper(ucs1);
    ucs2 = sui_utf8_ucs_toupper(ucs2);
    if (ucs1 < ucs2) return -1;
    if (ucs1 > ucs2) return 1;
    len1--; len2--;
  }
  if (len1) return 1;
  if (len2) return -1;
  return 0;
}


/**
 * sui_utf8_nsearch - Search first occurence of N characters from pattern string
 * @utf: pointer to a scanned string
 * @patt: pointer to a searched string
 * @chars: length of searched pattern string. -1 for entire string
 * Return Value: The function returns -1 if pattern isn't in scanned string, otherwise
 * the function returns position of first occurence of pattern in scanned string (number of characters).
 */
int sui_utf8_nsearch(utf8 *utf, utf8 *patt, int chars)
{
  int lenstr = sui_utf8_length(utf);
  utf8char *pstr = sui_utf8_get_text(utf);
  int i = 0;
  int chlen;
  if (chars < 0) chars = sui_utf8_length(patt);
  while (i <= lenstr-chars) {
    if (!sui_utf8_ncmp((utf8 *) pstr, patt, chars)) return i;
    i++;
    chlen = sui_utf8_char_size(pstr);
    if (chlen < 0)
      break;
    pstr += chlen;
  }
  return -1;
}


/**
 * sui_utf8_rewrite - Rewrite one character in utf8 string
 * @utf: Pointer to utf8 string.
 * @ofs: Offset from beginning of utf8 string (number of characters).
 * @chr: Included character in utf8char.
 *
 * The function rewrites one character in utf8 string on offset position from
 * beginning or included, if offset is bigger then length of string.
 *
 * Return Value: The function returns 0 if character was rewrited or included. And it returns
 * -1 if character wasn't rewrited or included (e.g.:static text,small buffer for text,...)
 * File: sui_utf8.c
 */
int sui_utf8_rewrite(utf8 *utf, int ofs, utf8char *chr)
{
  utf8char *txt;
  int oldlen, addlen, remsize, maxsize, cursize;

  if (!chr || !utf || sui_utf8_get_type(utf) != UTF8_DYNAMIC_FULL)
    return -1;
  addlen = sui_utf8_char_size(chr);
  if (addlen < 0)
    return -1;
  maxsize = sui_utf8_capacity(utf) - 1; /* space for terminating zero */
  txt = sui_utf8_get_text(utf);
  cursize = sui_utf8_bytesize(utf);

  if (ofs<0 || ofs>=sui_utf8_length(utf)) { /* add character at the end of string */
    txt += sui_utf8_bytesize(utf);
    oldlen = 0;
    ofs = -1;
    remsize = cursize;
  } else {
    remsize = sui_utf8_count_bytes(txt, ofs);
    txt += remsize;
    oldlen = sui_utf8_char_size(txt);
    if (oldlen < 0)
      return -1;
  }

  if (cursize-oldlen+addlen > maxsize) return -1; /* test for free space */

  if (ofs==-1) {
    sui_utf8_add_char(utf, chr);
  } else { /* rewrite character in string */
    if (oldlen != addlen)
      memmove(txt+addlen, txt+oldlen, cursize - (remsize+oldlen) +1); /* move with terminating zero */
    memcpy(txt, chr, addlen);
    utf->bytes = cursize-oldlen+addlen;
  }
  return 0;
}

/**
 * sui_utf8_insert - Insert one character into utf8 string
 * @utf: Pointer to utf8 string.
 * @ofs: Offset from beginning of utf8 string (number of characters).
 * @chr: Included character in utf8.
 *
 * The function includes one character in utf8 string on offset position from
 * beginning or adds it, if offset is bigger then length of string.
 *
 * Return Value: The function returns 0 if character was included. And it returns
 * -1 if character wasn't included (e.g.:static text). It returns 1 if there isn't
 * free space in the string for a new character.
 * File: sui_utf8.c
 */
int sui_utf8_insert(utf8 *utf, int ofs, utf8char *chr)
{
  utf8char *txt;
  int addlen, maxsize, cntchr, size;

  if (!chr || !utf || sui_utf8_get_type(utf) != UTF8_DYNAMIC_FULL) return -1;
  addlen = sui_utf8_char_size(chr);
  if (addlen < 0)
    return -1;
  maxsize = sui_utf8_capacity(utf) - 1;
  size = sui_utf8_bytesize(utf);

  if (size + addlen > maxsize) return 1;

  cntchr=sui_utf8_length(utf);
  txt = sui_utf8_get_text(utf);
  if ( ofs<0 || ofs >= cntchr) { /* include character at the end */
    sui_utf8_add_char(utf, chr);
  } else { // rewrite in string
    int remsize = sui_utf8_count_bytes(txt, ofs);
    txt += remsize;
    memmove(txt+addlen, txt, size-remsize+1); /* move with terminating zero */
    memcpy(txt, chr, addlen);
    utf->bytes = size + addlen;
    utf->chars = cntchr + 1;
  }
  return 0;
}

/**
 * sui_utf8_delete - Remove one character from utf8 string
 * @utf: Pointer to utf8 string.
 * @ofs: Offset from beginning of utf8 string (number of characters).
 *
 * The function removes one character from utf8 string at position given by $ofs from
 * beginning.
 *
 * Return Value: The function returns 0 if character was removed. And it returns
 * -1 if character wasn't deleted (e.g.:static text,small buffer for text,...).
 * File: sui_utf8.c
 */
int sui_utf8_delete(utf8 *utf, int ofs)
{
  utf8char *txt;
  unsigned int dellen, cntchr, size, remsize;

  if ( !utf || sui_utf8_get_type(utf) != UTF8_DYNAMIC_FULL) return -1;
  txt = sui_utf8_get_text(utf);
  cntchr = sui_utf8_length(utf);
  size = sui_utf8_bytesize(utf);

  if (ofs >= cntchr) return -1; /* offset is out of string */

  if (ofs<0) ofs = 0; /* remove the first character */
  remsize = sui_utf8_count_bytes(txt, ofs);
  txt += remsize;
  dellen = sui_utf8_char_size(txt);
  if (dellen < 0)
    return -1;
  memmove(txt, txt+dellen, size-remsize-dellen+1); /* with terminating zero */
  utf->bytes = size-dellen;
  utf->chars = cntchr-1;
  return 0;
}

/**
 * sui_utf8_insert_dynamic - Insert one character into utf8 string (and realloc buffer if there isn't free space in the string)
 * Parameters are the same as in the function sui_utf8_insert.
 */
int sui_utf8_insert_dynamic(utf8 *utf, int ofs, utf8char *chr)
{
  int ret = sui_utf8_insert(utf, ofs, chr);
  if (ret==1) {
    utf8char *newdata = realloc(utf->data.ptr, utf->cap_bytes+UTF8_MAX_BYTES_IN_CHAR);
    if (newdata) {
      utf->data.ptr = newdata;
      utf->cap_bytes += UTF8_MAX_BYTES_IN_CHAR;
      ret = sui_utf8_insert(utf, ofs, chr);
    } else
      return -1;
  }
  return ret;
}


/**
 * sui_utf8_prerw - Test utf8 string to RW
 * @ptr: Pointer to pointer of utf8 string
 *
 * The function returns -1 if utf8 string (ptr to utf8) can't
 * be writable.
 */
int sui_utf8_prerw( utf8 **ptr)
{
  utf8 *utfin, *utfout;
  if (!ptr) return -1;
  utfin = *ptr;
  if (utfin->refcnt == 1)
    if (sui_utf8_get_type(utfin) == UTF8_DYNAMIC_FULL)
      return 0;
  utfout = sui_utf8_dup(utfin);
  if (!utfout) return -1;
  sui_utf8_dec_refcnt(utfin); /* ??? */
  *ptr = utfout;
  return 0;
}

/**
 * sui_utf8_nullterm - return utf8 string always with terminating zero
 * @utf: pointer to utf8 string
 * The function either increment refcounter (for strings with terminating zero)
 * or create copy of the string with terminating zero.
 * Return Value: Pointer to utf8 string terminated by zero or NULL.
 */
utf8 *sui_utf8_nullterm(utf8 *utf)
{
  int isnull = sui_utf8_is_nullterm(utf);
  if (isnull<0) return NULL;
  if (isnull>0) {
    sui_utf8_inc_refcnt(utf);
    return utf;
  } else
    return sui_utf8_dup(utf);
}

/**
 * sui_utf8_is_nullterm - test if the utf8 string is terminated by zero
 * @utf: pointer to utf8 string
 * Return Value: The function returns 1 if the string is terminated by zero, 0 if it
 * isn't and -1 if any error occurs.
 */
int sui_utf8_is_nullterm(utf8 *utf)
{
  int ret = sui_utf8_get_type(utf);
  if (ret<0) return ret;
  if ((ret==UTF8_DYNAMIC_STRUCT) && (utf->bytes>=0)) /* only dynamic UTF8 structure with pointer to static text */
    return 0;
  else
    return 1;
}


/******************************************************************************/
/* basic character tests and upper<->lower */
/**
 * sui_utf8_isalnum - Is utf8 character alphanumeric?
 * @utf: Pointer to utf8 character.
 *
 * The function tests if character is alphanumeric character.
 * Return Value: The function returns 0 as false and ucs code of character as true.
 * File: sui_utf8.c
 */
  _UTF_UCS_TYPE sui_utf8_isalnum( utf8char *utf) {
    _UTF_UCS_TYPE ucs = sui_utf8_isalpha(utf);
    if (!ucs) ucs=sui_utf8_isdigit(utf);
    return ucs;
  }

/**
 * sui_utf8_isalpha - Is utf8 character letter?
 * @utf: Pointer to utf8 character.
 *
 * The function tests if character is letter.
 * Return Value: The function returns 0 as false and ucs code of character as true.
 * File: sui_utf8.c
 */
  _UTF_UCS_TYPE sui_utf8_isalpha( utf8char *utf) {
    _UTF_UCS_TYPE ucs;
    sui_utf8_to_ucs( utf, &ucs);
    if (ucs < 0x80) {
      if (( ucs < 'A') || (ucs>'Z' && ucs<'a') || (ucs>'z'))
        ucs = 0;
    } else {
      if ( ucs >= nls_flag_table->first && ucs <= nls_flag_table->last) {
        if ( !(nls_flag_table->table[ucs-nls_flag_table->first] & SNCF_ALPHA))
          return ucs = 0;
      } else ucs = 0;
    }
    return ucs;
  }

/**
 * sui_utf8_isdigit - Is utf8 character digit?
 * @utf: Pointer to utf8 character.
 *
 * The function tests if character is digit character.
 * Return Value: The function returns 0 as false and ucs code of character as true.
 * File: sui_utf8.c
 */
  _UTF_UCS_TYPE sui_utf8_isdigit( utf8char *utf) {    
    _UTF_UCS_TYPE ucs;
    sui_utf8_to_ucs( utf, &ucs);
    if (ucs < 0x80) {
      if (( ucs < '0') || (ucs>'9'))
        ucs = 0;
    } else {
      if ( ucs >= nls_flag_table->first && ucs <= nls_flag_table->last) {
        if ( !(nls_flag_table->table[ucs-nls_flag_table->first] & SNCF_DIGIT))
          return ucs = 0;
      } else ucs = 0;
    }
    return ucs;
  }

/**
 * sui_utf8_islower - Is utf8 character small letter?
 * @utf: Pointer to utf8 character.
 *
 * The function tests if character is small letter.
 * Return Value: The function returns 0 as false and ucs code of character as true.
 * File: sui_utf8.c
 */
  _UTF_UCS_TYPE sui_utf8_islower( utf8char *utf) {
    _UTF_UCS_TYPE ucs;
    sui_utf8_to_ucs( utf, &ucs);
    if (ucs < 0x80) {
      if (( ucs < 'a') || (ucs>'z'))
        ucs = 0;
    } else {
      if ( ucs >= nls_flag_table->first && ucs <= nls_flag_table->last) {
        if ( !(nls_flag_table->table[ucs-nls_flag_table->first] & SNCF_LOWER))
          return ucs = 0;
      } else ucs = 0;
    }
    return ucs;
  }
  
/**
 * sui_utf8_ispunct - Is utf8 character punctuation?
 * @utf: Pointer to utf8 character.
 *
 * The function tests if character is punctuation.
 * Return Value: The function returns 0 as false and ucs code of character as true.
 * File: sui_utf8.c
 */ 
  _UTF_UCS_TYPE sui_utf8_ispunct( utf8char *utf) {
    _UTF_UCS_TYPE ucs;
    sui_utf8_to_ucs( utf, &ucs);
    if (ucs < 0x80) {
      if (( ucs < ' ') || (ucs>'/' && ucs<':') || (ucs>'?' && ucs<'[') || 
          (ucs>'`' && ucs<'{') || (ucs>'~'))
        ucs = 0;
    } else {
      if ( ucs >= nls_flag_table->first && ucs <= nls_flag_table->last) {
        if ( !(nls_flag_table->table[ucs-nls_flag_table->first] & SNCF_PUNCT))
          return ucs = 0;
      } else ucs = 0;
    }
    return ucs;
  }
  
/**
 * sui_utf8_isspace - Is utf8 character white space?
 * @utf: Pointer to utf8 character.
 *
 * The function tests if character is white space.
 * Return Value: The function returns 0 as false and ucs code of character as true.
 * File: sui_utf8.c
 */ 
  _UTF_UCS_TYPE sui_utf8_isspace( utf8char *utf) {
    _UTF_UCS_TYPE ucs;
    sui_utf8_to_ucs( utf, &ucs);
    if (ucs < 0x80) {
      if (( ucs != ' ') && (ucs<0x09 || ucs>0x0d)) /* space,ht,lf,vt,ff,cr */
        ucs = 0;
    } else {
      if ( ucs >= nls_flag_table->first && ucs <= nls_flag_table->last) {
        if ( !(nls_flag_table->table[ucs-nls_flag_table->first] & SNCF_SPACE))
          return ucs = 0;
      } else ucs = 0;
    }
    return ucs;
  }
  
/**
 * sui_utf8_isupper - Is utf8 character capital letter?
 * @utf: Pointer to utf8 character.
 *
 * The function tests if character is capital letter.
 * Return Value: The function returns 0 as false and ucs code of character as true.
 * File: sui_utf8.c
 */  
  _UTF_UCS_TYPE sui_utf8_isupper( utf8char *utf) {
    _UTF_UCS_TYPE ucs;
    sui_utf8_to_ucs( utf, &ucs);
    if (ucs < 0x80) {
      if (( ucs < 'A') || (ucs>'Z'))
        ucs = 0;
    } else {
      if ( ucs >= nls_flag_table->first && ucs <= nls_flag_table->last) {
        if ( !(nls_flag_table->table[ucs-nls_flag_table->first] & SNCF_UPPER))
          return ucs = 0;
      } else ucs = 0;
    }
    return ucs;
  }
  
/**
 * sui_utf8_ucs_tolower - Transform capital letter to small letter
 * @ucs: UCS code of character.
 *
 * The function transforms capital letter to small letter and return
 * its ucs code or return same character if the input character isn't
 * capital letter.
 *
 * Return Value: The function returns ucs code of character.
 *
 * File: sui_utf8.c
 */
inline
_UTF_UCS_TYPE sui_utf8_ucs_tolower( _UTF_UCS_TYPE ucs)
{
  if ( ucs >='A' && ucs <= 'Z') {
    ucs |= 0x20;
  } else {
    if ( ucs >= nls_flag_table->first && ucs <= nls_flag_table->last) {
      if ( nls_flag_table->table[ucs-nls_flag_table->first] & SNCF_UPPER)
        ucs += nls_shift_table->table[ucs-nls_flag_table->first];
    }
  }
  return ucs;
}
  
/**
 * sui_utf8_ucs_toupper - Transform small letter to capital letter
 * @ucs: UCS code of character.
 *
 * The function transforms small letter to capital letter and return
 * its ucs code or return same character if the input character isn't
 * small letter.
 *
 * Return Value: The function returns ucs code of character.
 *
 * File: sui_utf8.c
 */
_UTF_UCS_TYPE sui_utf8_ucs_toupper( _UTF_UCS_TYPE ucs)
{
  if ( ucs >='a' && ucs <= 'z') {
    ucs &= 0xDF;
  } else {
    if ( ucs >= nls_flag_table->first && ucs <= nls_flag_table->last) {
      if ( nls_flag_table->table[ucs-nls_flag_table->first] & SNCF_LOWER)
        ucs += nls_shift_table->table[ucs-nls_flag_table->first];
    }
  }
  return ucs;
}



/**
 * sui_utf8_tolower - Transform entire UTF8 string to small letter
 * @inp: Input UTF8 string.
 *
 * The function transforms entire UTF8 string to small letter and returns
 * a new created UTF8 string.
 *
 * Return Value: The function returns a new dynamical UTF8 string.
 *
 * File: sui_utf8.c
 */
utf8 *sui_utf8_tolower(utf8 *inp)
{
  utf8 *ret = NULL;
  utf8char *pfrom, *pto;
  int len, sfrom = 0, sto = 0;
  _UTF_UCS_TYPE ucs;
  
  if (!inp) return NULL;
  len = sui_utf8_length(inp);
  ret = sui_utf8(NULL, len, -1);
  if (!ret) return NULL;
  sui_utf8_inc_refcnt(inp);
  pfrom = sui_utf8_get_text(inp);
  pto = sui_utf8_get_text(ret);
  ret->chars = len;
  while (len--) {
    sfrom += sui_utf8_to_ucs(pfrom + sfrom, &ucs);
    ucs = sui_utf8_ucs_tolower(ucs);
    sto += sui_utf8_from_ucs(pto + sto, ucs);
  }
  ret->bytes = sto;
  sui_utf8_dec_refcnt(inp);
  return ret;
}

/**
 * sui_utf8_toupper - Transform entire UTF8 string to upper letters
 * @inp: UTF8 input string.
 *
 * The function transforms entire UTF8 string to capital letters and returns
 * a new UTF8 string.
 *
 * Return Value: The function returns a new dynamic UTF8 string.
 *
 * File: sui_utf8.c
 */    
utf8 *sui_utf8_toupper(utf8 *inp)
{
  utf8 *ret = NULL;
  utf8char *pfrom, *pto;
  int len, sfrom = 0, sto = 0;
  _UTF_UCS_TYPE ucs;
  
  if (!inp) return NULL;
  len = sui_utf8_length(inp);
  ret = sui_utf8(NULL, len, -1);
  if (!ret) return NULL;
  sui_utf8_inc_refcnt(inp);
  pfrom = sui_utf8_get_text(inp);
  pto = sui_utf8_get_text(ret);
  ret->chars = len;
  while (len--) {
    sfrom += sui_utf8_to_ucs(pfrom + sfrom, &ucs);
    ucs = sui_utf8_ucs_toupper(ucs);
    sto += sui_utf8_from_ucs(pto + sto, ucs);
  }
  ret->bytes = sto;
  sui_utf8_dec_refcnt(inp);
  return ret;
}
