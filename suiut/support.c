/* support.c
 *
 * SUITK support functions.
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

//#include "namespace.h"
#include "support.h"

#include <stdlib.h>
#include <string.h>

#include <suiut/sui_types.h> /* suitk structure definitions */
#include "sui_comdefs.h"

/**
 * Common generic memory allocation for dynamic structures
 */
void *sui_malloc( int size)
{
	void *ret = NULL;
	if ( !size) return ret;
	ret = malloc( size);
	if ( ret) {
		memset( ret, 0, size);
	}
	return ret;
}

/**
 * Common generic memory release function
 */
void sui_free( void *ptr)
{
	free( ptr);
}

/**
 * sui_add_item_to_array - Common generic memory allocation and management for dynamic array items
 * @array: pointer to pointer to array
 * @itemsize: size of one item (in bytes)
 * @cnt: pointer to number of array items
 * @maxcnt: pointer to number of allocated array items
 * If there isn't free space for a new item in array or the array
 * hasn't exist yet the function creates a new array with more free space
 * and move all existing items to created array. The function increments
 * $cnt if a new item was 'added' and it increments $maxcnt if a new array
 * had to be allocated.
 * Return Value: The function returns pointer to a new item in array.
 */
void *sui_add_item_to_array(void **array, int itemsize, int *cnt, int *maxcnt)
{
  void *item = NULL;
  if (!cnt || !maxcnt || !array || !itemsize) return NULL; // error - bad calling

  if (!(*array) || (*cnt >= *maxcnt)) {  // we need more space
    item = realloc(*array, (*maxcnt+SUI_NUMBER_OF_ADDED_ITEMS)*itemsize);
    if (!item) return NULL; // new array wasn't created
    *array = item;
    memset(item + (*maxcnt*itemsize), 0, SUI_NUMBER_OF_ADDED_ITEMS*itemsize);
    *maxcnt += SUI_NUMBER_OF_ADDED_ITEMS;
  }
  item = ((char *)*array)+(itemsize*(*cnt));
  (*cnt)++;
  return item;
}


/******************************************************************************
	sui character buffer
******************************************************************************/
/**
 * sui_combine_path - function cut file from old path and cat new relative path
 */
char *sui_combine_path( const char *oldpath, int size_op, const char *newpath, int size_np)
{
	char *np = NULL;
	char *ptr = NULL;
	int np_from = 0; /* index to newpath after ./../../ trimming */

	if ( !oldpath || !newpath) return NULL;
	if ( size_op < 0)
		size_op = strlen( oldpath);
	ptr = (char *) oldpath+size_op-1;
	/* trim last part */
	while( ptr > oldpath) {		/* find beginning or last '/' */
		if ( sui_is_pathdiv( *ptr)) { 
			ptr++; 
			break;
		}
		ptr--;
	}
	size_op = ptr-oldpath;

	if (size_np < 0)
		size_np = strlen( newpath);
	/* trim first path separator if it is here */
	if ( sui_is_pathdiv( *(newpath+np_from)))
		np_from++;
	/* trim backdir '..'  from beginning of newpath */
	while (( np_from < size_np-3) &&
	       (!strncmp( newpath + np_from, "../", 3) ||       
	        !strncmp( newpath + np_from, "..\\", 3))) {
		np_from += 3;
		if ( size_op > 1) size_op -= 2;	/* before path divider */
		while( size_op > 0) {		/* find beginning or last '/' */
			if ( sui_is_pathdiv( *(oldpath+size_op))) { 
				size_op++; break;
			}
			size_op--;
		}
	}
	/* create new path */
	size_np -= np_from;
	np = (char *) malloc( size_op +size_np+1); /* the worst case */
	if ( !np) return NULL;
	strncpy( np, oldpath, size_op);
	strncpy( np+size_op, newpath + np_from, size_np);
	*(np+size_op+size_np)=0;
	return np;
}

/**
 * sui_get_part_of_path - get one part of path from first character (or last character) to first separator
 * @path: pointer to path string
 * @separator: character which is used as separator in path
 * @fromleaf: get last part instead first part
 * The function returns negative value as error.
 * It returns index of the last character before separator if the first part is searched
 * It returns index of the first character after separator if the last part is searched
 * It returns zero if path containts only one part
 * If there is double separator, function skip it and search next separator.
 * The enter path must not beginning or ending with path separator
 */
int sui_get_part_of_path( const char *path, int size, char separator, int fromleaf)
{
	int idx;
	if ( !path) return -1;
	if ( size < 0) size = strlen( path);
	idx = 0;

	if ( fromleaf) {
	  idx = size;
		while ( idx >= 0) {
			if ( *(path+idx) == separator) {
				if ( *(path+idx-1) == separator) {
					idx--;
				} else {
					break;
				}
			}
			idx--;
		}
		idx++;
	} else {
		/* idx is set to the first character in path */
		while ( idx < size) {
			if ( *(path+idx) == separator) {
				if ( *(path+idx+1) == separator) {
					idx++;
				} else {
					break;
				}
			}
			idx++;
		}
	}
	return idx;
}



/**
 * sui_generic_str_to_ulong - convert ASCII string to unsigned long
 * @str: pointer to string
 * @outnum: pointer to output unsigned long buffer
 *
 * The function converts ASCII string to unsigned long
 * The function returns SUI_RET_OK or error code.
 
 * sui_generic_long_to_str - convert generic long to string
 * @innum: pointer to number
 * @str: pointer to output buffer of string, which was created
 *
 * ??? format of number representation, case of letters 'a','A'
 *     aligning, filling with zero, '0x' before hexadecimal numbers ...
 */
int sui_generic_str_to_ulong( char **str, unsigned long *outnum, int base)
{
	unsigned long ret = 0;
	char *ptr, ch;
	int c=0, mul = base; /* base = 10 */
	int rcode = SUI_RET_ETYPE;

	if ( outnum == NULL || str == NULL || *str == NULL)
		return SUI_RET_NRDY;
	ptr = *str; ch = *ptr++;
	if ( ch == 0)
		return SUI_RET_NRDY;
	
	if ( ch == '-') { /* first char is minus -> out of range */
		return SUI_RET_EOORN;
	}

	if ( ch == '0') {	/* first char is zero */
		ch = *ptr++;
		if ( sui_is_hex_num( ch) || sui_is_bin_num( ch)) {
			if ( sui_is_hex_num( ch))
				mul = 16;
			else
				mul = 2;
			ch = *ptr++;
		} else {
			if ( ch >= '0' && ch < '8')
				mul = 8;
			else {					/* only zero */
				*str = ptr-1;
				*outnum = ret;
				return SUI_RET_OK;
			}
		}
	}
	while( 1) {
		if ( sui_is_digit( ch)) {
			c = ch - '0';
			if ( c < mul) rcode = SUI_RET_OK;
		} else if ( sui_is_upper( ch)) {
			c = ch - 'A' + 10;
			if ( c < mul) rcode = SUI_RET_OK;
		} else if ( sui_is_lower( ch)) {
			c = ch - 'a' + 10;
			if ( c < mul) rcode = SUI_RET_OK;
		} else {
			break;
		}
		if ( c >= mul) break; /* end of number - OK or ERR ??? */

		ret = ret * mul;
		if (( 0xffffffff - ret) < c) { /* number will be greater than ulong range */
			return SUI_RET_EOORP;
		}
		ret += c;
		ch = *ptr++;
	}
/* all is OK */
	*str = ptr-1;
	*outnum = ret;
	return rcode;
}

/**
 * sui_str_to_ulong - convert ASCII string to unsigned long
 * @str: pointer to string
 * @outnum: pointer to output unsigned long buffer
 *
 * The function converts ASCII string to unsigned long
 * The function returns SUI_RET_OK or error code.
 */
int sui_str_to_ulong( char **str, unsigned long *outnum)
{
	return sui_generic_str_to_ulong( str, outnum, 10);
}

/**
 * sui_str_to_long - convert ASCII string to long
 * @str: pointer to string
 * @outnum: pointer to output long buffer
 *
 * The function converts ASCII string to long and uses
 * function sui_str_to_ulong for conversion.
 * The function returns SUI_RET_OK or error code.
 */
int sui_str_to_long( char **str, long *outnum)
{
	unsigned long ulongbuf = 0;
	int rcode = SUI_RET_OK, minus = 0;
	char *ptr = *str;

	if ( outnum == NULL || str == NULL || *str == NULL)
		return SUI_RET_NRDY;

	if ( *ptr == '-') {
		minus = 1;
		ptr++;
	}
	rcode = sui_generic_str_to_ulong( &ptr, &ulongbuf, 10);
	if ( rcode == SUI_RET_OK) {
		if ( ulongbuf > 0x7fffffff) {	/* out of range */
			if ( minus)
				rcode = SUI_RET_EOORN;
			else
				rcode = SUI_RET_EOORP;
		} else {
			long outbuf = (long)ulongbuf;
			if ( minus) outbuf = -outbuf;
			*outnum = outbuf;
			*str = ptr;
		}
	}
	return rcode;
}

/**
 * sui_generic_long_to_str - convert generic long to string
 * @innum: pointer to number
 * @str: pointer to output buffer of string, which was created
 *
 * ??? format of number representation, case of letters 'a','A'
 *     aligning, filling with zero, '0x' before hexadecimal numbers ...
 */
int sui_generic_long_to_str( unsigned long *innum, char **str, int minus, int base)
{
	int i = 32;
	char buf[34]; /* ulong has max. 32 digits in binary base + zero ending + minus */
	char ch = 0, *ptr = NULL;
	unsigned long num;
	
	if ( innum == NULL || str == NULL)
		return SUI_RET_NRDY;

	if ( base < 1) 
		base = 10; /* wrong base -> decimal base */

	buf[33] = 0; /* end zero */
	num = *innum;

	do {
		ch = num % base; num = num / base;
		if ( ch <= 9) 
			buf[i--] = ch + '0';
		else
			buf[i--] = ch + 'A'; /* ??? */
		if (( i < 0) && ( num > 0)) { /* ??? error - no space - !!! never */
			return SUI_RET_EOORP;
		}
	} while ( num);

	if ( minus)
		buf[i--] = '-';
	
	ptr = (char *) malloc( 33-i); /* create output buffer, $i was decremented in loop */
	if ( ptr == NULL) 
		return SUI_RET_ERR;
	strncpy( ptr, &buf[i+1], 33-i); /* zero is included in buffer (buf[33]) */

	*str = ptr;
	return SUI_RET_OK;
}

/**
 * sui_ulong_to_str - convert unsigned long to string with decimal representation of number
 *
 */
int sui_ulong_to_str( unsigned long *innum, char **str)
{
	return sui_generic_long_to_str( innum, str, 0, 10);
}

/**
 * sui_long_to_str - convert signed long to string with decimal representation of number
 *
 */
int sui_long_to_str( long *innum, char **str)
{
	unsigned long ulbuf;
	int minus = 0;
	if ( innum == NULL) return SUI_RET_NRDY;
	if ( *innum < 0) {
		ulbuf = (unsigned long)(- *innum);
		minus = 1;
	} else {
		ulbuf = (unsigned long) *innum;
	}
	return sui_generic_long_to_str( &ulbuf, str, minus, 10);
}
