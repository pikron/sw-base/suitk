/* sui_types.c
 *
 * SUITK type system and its support.
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

 
#include <string.h>
#include "ul_utmalloc.h"
#include "sui_types.h"

/******************************************************************************
 * Type functions
 ******************************************************************************/

/**
 * sui_object_inc_refcnt - Increment reference counter of object with VMT
 * @buf: Pointer to an object with VMT.
 * @type: Type of an object pointed by $buf.
 *
 * The function increments object reference counter. The object type must be given.
 *
 * Return Value: The function returns value of the object's reference counter after incrementing.
 *
 * File: sui_types.c
 */
sui_refcnt_t sui_object_inc_refcnt(void *buf, sui_typeid_t buf_type) // sui_any_inc_refcnt
{
  sui_obj_vmt_t *vmt;

  vmt = sui_typereg_get_vmt_by_typeid(NULL, buf_type & SUI_TYPE_CODE_MASK);
  if(!vmt)
    return SUI_REFCNTIMPOSIBLE;
  if(buf_type & SUI_TYPE_PTR_FL)
    buf = *(void **)buf;
  if(!(vmt->vmt_obj_tinfo->flags & SUI_OTINFO_NOVMT))
    vmt = sui_obj_vmt((sui_obj_t*)buf);
  return vmt->inc_refcnt((sui_obj_t*)buf);
}

/**
 * sui_object_dec_refcnt - Decrement reference counter of object with VMT
 * @buf: Pointer to an object with VMT.
 * @type: Type of an object pointed by $buf.
 *
 * The function decrements object reference counter. The object type must be given.
 * If the counter is decremented to zero color-table structure is deleted.
 *
 * Return Value: The function returns value of the object's reference counter after decrementing.
 *
 * File: sui_types.c
 */
sui_refcnt_t sui_object_dec_refcnt(void *buf, sui_typeid_t buf_type) // sui_any_dec_refcnt
{
  sui_obj_vmt_t *vmt;
  vmt = sui_typereg_get_vmt_by_typeid(NULL, buf_type & SUI_TYPE_CODE_MASK);
  if(!vmt)
    return SUI_REFCNTIMPOSIBLE;
  if(buf_type & SUI_TYPE_PTR_FL)
    buf = *(void **)buf;
  if(!(vmt->vmt_obj_tinfo->flags & SUI_OTINFO_NOVMT))
    vmt = sui_obj_vmt((sui_obj_t*)buf);
  return vmt->dec_refcnt((sui_obj_t*)buf);
}



/**
 * sui_typeid_size - Get size of an object according to its type
 * @type: Type of a variable.
 *
 * The function gets an object size (in bytes) according to its type
 *
 * Return Value: The function returns size of the object in bytes.
 *
 * File: sui_types.c
 */
int sui_typeid_size(sui_typeid_t buf_type)
{
  sui_obj_vmt_t *vmt;
  if (buf_type & SUI_TYPE_PTR_FL)
    return sizeof(void *);
  vmt = sui_typereg_get_vmt_by_typeid(NULL, buf_type & SUI_TYPE_CODE_MASK);
  return vmt->vmt_obj_tinfo->obj_size;
}

/**
 * sui_typeid_buffer_type - Return parent type for another type
 * @type: Type of a variable.
 *
 * The function returns parent type of an object according to its type.
 *
 * Return Value: The function returns either parent type of entered type (e.g. LONG for CHAR,SHORT,INT,...)
 * or entered type.
 *
 * File: sui_types.c
 */
sui_typeid_t sui_typeid_buffer_type(sui_typeid_t in_type)
{
  sui_obj_vmt_t *vmt;
  vmt = sui_typereg_get_vmt_by_typeid(NULL, in_type & SUI_TYPE_CODE_MASK);
  if(vmt == NULL)
    return SUI_TYPE_UNKNOWN;
  if(vmt->vmt_common_type != NULL)
    return vmt->vmt_common_type->vmt_typeid;
  return in_type;
}


/**
 * sui_check_typecompat - Check compatibility between two object types
 * @in_type: An input type.
 * @out_type: An output type.
 *
 * The function checks compatibility between two types and returns SUI_RET_OK
 * for compatible types and SUI_RET_ETYPE for incompatible types.
 *
 * Return Value: Check result - either %SUI_RET_OK or %SUI_RET_ETYPE
 *
 * File: sui_types.c
 */
int sui_check_typecompat(sui_typeid_t in_type, sui_typeid_t out_type)
{
  /* ??? through table ??? */
  if (in_type == out_type) return SUI_RET_OK; /* the same type - no through sui_typeid_buffer_type */
  if (sui_typeid_buffer_type(in_type) == sui_typeid_buffer_type(out_type))
    return SUI_RET_OK;
  return SUI_RET_ETYPE;
}

/**
 * sui_typeid_copy - Copy object and increment its reference counter if object has got someone
 * @dstptr: Pointer to an output buffer.
 * @srcptr: Pointer to a source object.
 * @type: Object type.
 *
 * The function creates a new object as a copy of the source object.
 *
 * Return Value: The function returns zero as success.
 *
 * File: sui_types.c
 */
int sui_typeid_copy(void *dstptr, void *srcptr, sui_typeid_t type)
{
  if (type & SUI_TYPE_PTR_FL)
    sui_object_dec_refcnt(dstptr, type & SUI_TYPE_CODE_MASK);
  memcpy(dstptr, srcptr, sui_typeid_size(type)); /* FIXME: ??? */
  if (type & SUI_TYPE_PTR_FL)
    sui_object_inc_refcnt(dstptr, type & SUI_TYPE_CODE_MASK);
  return 0;
}
