/* sui_args.h
 *
 * SUITK function arguments propagation.
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#ifndef _SUI_ARGSBASE_H_
#define _SUI_ARGSBASE_H_

#include <stdarg.h>
#include "sui_comdefs.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * struct sui_args_tinfo - Structure describing function/method arguments passing
 * @count:		Number of passed arguments
 * @call_va_bare:	Pointer to function to propagate call to the target function
 * @call_va_context:	Pointer to function to propagate call to the function/method
 *			with additional void* context parameter.
 * @args:		The array of @count arguments type identifiers.
 *			Each type is represented by its sui_typeid_t value.
 *
 * File: sui_argsbase.h
 */
typedef struct sui_args_tinfo {
  int count;
  int (*call_va_bare)(int (*fnc)(void), va_list args);
  int (*call_va_context)(int (*fnc)(void), void *context, va_list args);
  sui_typeid_t args[]; 
} sui_args_tinfo_t;

extern sui_args_tinfo_t sui_args_tinfo_void;
extern sui_args_tinfo_t sui_args_tinfo_int;
extern sui_args_tinfo_t sui_args_tinfo_uint;
extern sui_args_tinfo_t sui_args_tinfo_long;
extern sui_args_tinfo_t sui_args_tinfo_ulong;
extern sui_args_tinfo_t sui_args_tinfo_ptr;

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /*_SUI_ARGSBASE_H_*/
