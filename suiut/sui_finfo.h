/* sui_finfo.h
 *
 * Header file for format part of DINFO structures
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#ifndef _SUI_FINFO_H_
#define _SUI_FINFO_H_

#include <suiut/sui_types.h>
#include <suiut/sui_utf8.h>

/**
 * struct sui_choice - structure of choice text by masked value of dinfo
 * @mask: mask for test
 * @value: value after masking
 * @text: pointer to utf8 text
 */
typedef struct sui_choice {
  long mask;
  long value;
  utf8 *text; // ??? DINFO
} sui_choice_t;

/**
 * struct sui_coicetable - choice table structure
 * @refcnt: reference counter
 * @challoc: size of allocated space for choices as maximal count of choices or SUI_STATIC for static table
 * @count: count of choices in table
 * @chtab: pointer to array of choices
 */
typedef struct sui_choicetable {
  sui_refcnt_t  refcnt;
  int           challoc;
  int           count;
  sui_choice_t *chtab;
} sui_choicetable_t;

sui_refcnt_t sui_choicetable_inc_refcnt(sui_choicetable_t *cht);
sui_refcnt_t sui_choicetable_dec_refcnt(sui_choicetable_t *cht);

/**
 * struct sui_finfo - number format structure
 * @refcnt: reference counter
 * @base: base of number
 * @digits: maximal width of number in digits
 * @litstep: small step (minimal step)
 * @bigstep: large step (maximal step)
 * @choices: pointer to choice table if the number will be translated to choice one from all
 */
typedef struct sui_finfo {
  sui_refcnt_t refcnt;
  unsigned short flags;
  int base;
  int digits;
  long litstep; // for arrow keys or for minimal step
  long bigstep; // for pgdn/pgup keys or for maximal step
  utf8 *ftext; // format of text used for time or prefix/postfix number
  sui_choicetable_t *choices; // pointer to choice table
} sui_finfo_t;

sui_refcnt_t sui_finfo_inc_refcnt(sui_finfo_t *format);
sui_refcnt_t sui_finfo_dec_refcnt(sui_finfo_t *format);

enum sui_finfo_flags {
  SUFI_TIME   = 0x0001,
  SUFI_TEXT   = 0x0002,
  SUFI_PREFIX = 0x0003,
  SUFI_SUFFIX = 0x0004,

  SUFI_TEXTMASK=0x000F,

  SUFI_DECIMALZEROS = 0x0010,
  SUFI_SMALLLETTERS = 0x0020,

  SUFI_CHECKLIMITS  = 0x0100,
  SUFI_RELATIVETIME = 0x0200,
  SUFI_TZAPPLY2TIME = 0x0400,
  SUFI_HMSONLYTIME  = 0x0800,
};

#define SUFI_TIME_LOCAL    (SUFI_TIME|SUFI_TZAPPLY2TIME)
#define SUFI_TIME_UTC      (SUFI_TIME)
#define SUFI_TIME_INTERVAL (SUFI_TIME|SUFI_RELATIVETIME)
#define SUFI_TIME_HMS      (SUFI_TIME|SUFI_RELATIVETIME|SUFI_HMSONLYTIME)

/********************************************************************/
// dynamic struct

sui_choicetable_t *sui_choicetable_create( int acnt, sui_choice_t ch[]);
int sui_choice_add_to_choicetable( sui_choicetable_t *chtab, sui_choice_t *ch);

sui_finfo_t *sui_finfo_create( int abase, int adig, long alstep, long abstep, sui_choicetable_t *cht);

#define sui_choicetable_end sui_choicetable_dec_refcnt
#define sui_finfo_end sui_finfo_dec_refcnt

/********************************************************************/
// static struct

#ifdef WIN32
  #define sui_finfo( name, ba, di, ls, bs, chot) \
    sui_finfo_t name = { SUI_STATIC, ba, di, ls, bs, chot}
#else
  #define sui_finfo( name, ba, di, ls, bs, chot) \
    sui_finfo_t name = { \
      refcnt:  SUI_STATIC, \
      base:    ba, \
      digits:  di, \
      litstep: ls, \
      bigstep: bs, \
      cht:   chot \
    }
#endif

#endif /* _SUI_FINFO_H_ */
