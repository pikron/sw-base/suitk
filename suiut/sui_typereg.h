/* sui_objbase.h
 *
 * SUITK object/types register.
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#ifndef _SUI_TYPEREG_H_
#define _SUI_TYPEREG_H_

#include <inttypes.h>
#include "ul_utdefs.h"
#include "sui_objbase.h"
#include "sui_argsbase.h"

#ifdef __cplusplus
extern "C" {
#endif

struct sui_typeregister;

extern struct sui_typeregister *sui_typeregister_global;

sui_typeid_t sui_obj_get_unique_typeid(void);

int sui_obj_register_typeid(struct sui_typeregister *reg, sui_typeid_t a_typeid, sui_obj_vmt_t *vmt);

#define SUI_TYPEREG_MODE_CONST         1
#define SUI_TYPEREG_MODE_WITHOUT_NAMES 2

int *sui_typereg_init(struct sui_typeregister *reg, sui_obj_vmt_t * const *vmt_array_data, unsigned data_size, int mode);

sui_obj_vmt_t *sui_typereg_get_vmt_by_typeid(struct sui_typeregister *reg, sui_typeid_t a_typeid);

sui_obj_vmt_t *sui_typereg_get_vmt_by_name(struct sui_typeregister *reg, char *name);

sui_typeid_t sui_typereg_typeid_first(struct sui_typeregister *reg);

sui_typeid_t sui_typereg_typeid_next(struct sui_typeregister *reg, sui_typeid_t a_typeid);

int sui_typereg_typeid_is_invalid(sui_typeid_t a_typeid);

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /*_SUI_TYPEREG_H_*/
