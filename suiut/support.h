/* support.h
 *
 * support function for SUITK system.
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#ifndef _SUITK_SUPPORT_SYSTEM_
#define _SUITK_SUPPORT_SYSTEM_

#ifdef __cplusplus
extern "C" {
#endif

/******************************************************************************
 * Macros for letter testing
 ******************************************************************************/
#define sui_is_left_bracket( x) (x == '<')                    // tag beginning mark
#define sui_is_right_bracket( x) (x == '>')                   // tag ending mark
#define sui_is_slash( x) (x == '/')                           // end tag mark ( It's closely behind smark. e.g.'</')
#define sui_is_or( x) (x == '|')                              // OR mark (between two numbers)
#define sui_is_delimiter( x) (x == ',')                       // delimiter mark (between two values)
#define sui_is_underline( x) (x == '_')                       // underline mark
#define sui_is_equal( x) (x == '=')                           // equal mark
#define sui_is_minus( x) (x == '-')                           // minus mark
#define sui_is_quotation( x) (x == '\'' || x == '\"')

//#define sui_is_space( x) (x == ' ' || (x >=0x09 && x<= 0x0d)) // white chars
#define sui_is_space( x) (x <= ' ') // white chars

#define sui_is_digit( x) (x >= '0' && x <= '9')
#define sui_is_upper( x) (x >= 'A' && x <= 'Z')
#define sui_is_lower( x) (x >= 'a' && x <= 'z')
#define sui_is_new_ln( x) (x == 0x0a)                         // new line
#define sui_is_bin_num( x) (x=='b' || x=='B')                 // binary number
#define sui_is_hex_num( x) (x=='x' || x=='X')                 // hexadecimal number
#define sui_is_pathdiv( x) (x=='\\' || x=='/')                // path delimiter
#define sui_is_backslash(x) (x=='\\')                         // backslash
#define sui_is_point( x) (x == '.')                           // decimal point
#define sui_is_hash( x) (x == '#')                            // hash

#define SUI_KEY_POINT	'.'


/******************************************************************************
 * Generic allocating function
 ******************************************************************************/
#define SUI_NUMBER_OF_ADDED_ITEMS 16  /* If array needs new items free space for 'SUI_NUMBER_OF_ADDED_ITEMS' items will be added. */

void *sui_malloc( int size);
void sui_free( void *ptr);

void *sui_add_item_to_array( void **array, int itemsize, int *cnt, int *maxcnt);

char *sui_combine_path( const char *oldpath, int size_op, const char *newpath, int size_np);
int sui_get_part_of_path( const char *path, int size, char separator, int fromleaf);

int sui_str_to_ulong( char **str, unsigned long *outnum);
int sui_str_to_long( char **str, long *outnum); /* long sui_str_to_long( char **str); */

int sui_generic_long_to_str( unsigned long *innum, char **str, int minus, int base);
int sui_ulong_to_str( unsigned long *innum, char **str);
int sui_long_to_str( long *innum, char **str);

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /* _SUITK_SUPPORT_SYSTEM_ */
