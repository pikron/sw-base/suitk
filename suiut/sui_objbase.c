/* sui_objbase.c
 *
 * SUITK object and method inheritance base.
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#include <string.h>
#include "ul_utmalloc.h"
#include "ul_list.h"
#include "ul_evcbase.h"
#include "ul_gavl.h"
#include "ul_gavlcust.h"
#include "sui_comdefs.h"
#include "sui_objbase.h"
#include "sui_typereg.h"
#include "sui_argsbase.h"
#include "sui_typemethods.h"

typedef struct sui_obj_signal {
  sui_signalid_t signalid;
  gavl_node_t node;
  evc_tx_hub_t tx_hub;
} sui_obj_signal_t;

typedef struct sui_obj_slot {
  sui_signalid_t slotid;
  gavl_node_t node;
  evc_rx_hub_t rx_hub;
  union {
    int (*fnc_ptr)(void);
    int (*method_ptr)(void *self);
    void *data_ptr;
    long data_long;
    void *ptr;
  } target;
  union{
    int (*call_va_bare)(int (*fnc)(void), va_list args);
    int (*call_va_context)(int (*fnc)(void), void *context, va_list args);
    void *call_va_fnc;
  } translate;
} sui_obj_slot_t;

typedef struct sui_obj_instance_data {
  gavl_cust_root_field_t signals;
  gavl_cust_root_field_t slots;
} sui_obj_instance_data_t;

static inline
int sui_cmp_signalid(const sui_signalid_t *a, const sui_signalid_t *b)
{
  return *a>*b?1:*a<*b?-1:0;
}

static inline
int sui_cmp_slotid(const sui_slotid_t *a, const sui_slotid_t *b)
{
  return *a>*b?1:*a<*b?-1:0;
}

GAVL_CUST_NODE_INT_DEC(sui_obj_signal, sui_obj_instance_data_t, sui_obj_signal_t, sui_signalid_t,
                 signals, node, signalid, sui_cmp_signalid)

GAVL_CUST_NODE_INT_IMP(sui_obj_signal, sui_obj_instance_data_t, sui_obj_signal_t, sui_signalid_t,
                 signals, node, signalid, sui_cmp_signalid)

GAVL_CUST_NODE_INT_DEC(sui_obj_slot, sui_obj_instance_data_t, sui_obj_slot_t, sui_slotid_t,
                 slots, node, slotid, sui_cmp_slotid)

GAVL_CUST_NODE_INT_IMP(sui_obj_slot, sui_obj_instance_data_t, sui_obj_slot_t, sui_slotid_t,
                 slots, node, slotid, sui_cmp_slotid)

/******************************************************************************/
/* Basic types related functions */

/**
 * sui_obj_check_vmt_ok - Check if VMT is registered and if no register VMT.
 * @vmt: Pointer to the checked VMT
 *
 * The function check if @VMT argument is non-%NULL and then check if it has
 * already assigned @vmt_typeid and if @vmt_typeid does not indicate
 * registration request flag @SUI_TYPE_REGISTER_FL. If @vmt_typeid is zero
 * or the flag is set function sui_obj_register_vmt() is called.
 *
 * Return Value: Zero value indicates success. Non-zero registration failure.
 * File: sui_objbase.c
 */
int sui_obj_check_vmt_ok(struct sui_obj_vmt *vmt)
{
  if(!vmt)
    return -1;

  if(!vmt->vmt_typeid || (vmt->vmt_typeid & SUI_TYPE_REGISTER_FL)){
    if(vmt->vmt_typeid)
      vmt->vmt_typeid &= ~SUI_TYPE_REGISTER_FL;
    sui_obj_register_vmt(vmt);
  }
  return 0;
}

/**
 * sui_obj_init_vmt - Initializes new object according to the provided VMT.
 * @obj: Pointer to already allocated object memory.
 * @vmt: Pointer to the VMT
 *
 * Return Value: Zero value indicates success. Non-zero registration failure.
 * File: sui_objbase.c
 */
int 
sui_obj_init_vmt(sui_obj_t *obj, void *vmt)
{
  int init_cnt;
  sui_obj_vmt_t *vmt1=(sui_obj_vmt_t *)vmt;
  typeof(vmt1->vmt_init) initfnc;
  
  if(sui_obj_check_vmt_ok(vmt)<0)
    return -1;
  
  memset(obj,0,vmt1->vmt_obj_tinfo->obj_size);
  if(!(vmt1->vmt_obj_tinfo->flags & SUI_OTINFO_NOVMT)) {
    obj->vmt=(sui_obj_vmt_t*)vmt1;
    /*obj->instance_data=NULL;*/ /* Already zeroed */
  }
  
  init_cnt=0;
  for(vmt1=(sui_obj_vmt_t *)vmt;vmt1;vmt1=(sui_obj_vmt_t *)vmt1->vmt_parent){
    if(!vmt1->vmt_init)
      continue;
    init_cnt++;
  }

  {
    typeof(vmt1->vmt_init) init_fncs[init_cnt];
    
    init_cnt=0;
    for(vmt1=(sui_obj_vmt_t *)vmt;vmt1;vmt1=(sui_obj_vmt_t *)vmt1->vmt_parent){
      if(!vmt1->vmt_init)
	continue;
      init_fncs[init_cnt++]=vmt1->vmt_init;
    }

    while(init_cnt--){
      initfnc = init_fncs[init_cnt];
      if(initfnc(obj, NULL)<0)
        goto init_fault;
    }
  }

  vmt1=(sui_obj_vmt_t *)vmt;
  if(vmt1->vmt_refcnt != SUI_STATIC){
    /* this must to be atomic if we switch to multi-threaded
       support one day, but such switch would require even more adaptations */
    vmt1->vmt_refcnt++;
  }

  return 0;

init_fault:
  for(vmt1=(sui_obj_vmt_t *)vmt;vmt1;vmt1=(sui_obj_vmt_t *)vmt1->vmt_parent) {
    if(initfnc == vmt1->vmt_init) {
      while((vmt1 = (sui_obj_vmt_t *)vmt1->vmt_parent) != NULL)
        if(vmt1->vmt_done)
          vmt1->vmt_done(obj);
      break;
    }
  }
  return -1;
}

/**
 * sui_obj_init - Initializes new object according to the provided type ID.
 * @obj: Pointer to already allocated object memory.
 * @typeid: Already registered type ID.
 *
 * Return Value: Zero value indicates success. Non-zero registration failure.
 * File: sui_objbase.c
 */
int
sui_obj_init(sui_obj_t *obj, sui_typeid_t typeid)
{
  void *vmt;

  vmt=sui_typereg_get_vmt_by_typeid(NULL, typeid);
  if(!vmt)
    return -1;

  return sui_obj_init_vmt(obj, vmt);
}

/**
 * sui_obj_new_vmt - Create and initializes new object according to the VMT.
 * @vmt: Pointer to the VMT
 *
 * Return Value: Returns pointer to the fully allocated and initialized dynamic object.
 *		%NULL value indicates unsuccessful attempt to create object.
 * File: sui_objbase.c
 */
sui_obj_t *sui_obj_new_vmt(struct sui_obj_vmt *vmt)
{
  if(sui_obj_check_vmt_ok(vmt)<0)
    return NULL;

  return vmt->vmt_new(vmt);
}

static void 
sui_obj_done_internal(sui_obj_t *obj, sui_obj_vmt_t *vmt)
{
  sui_obj_vmt_t *vmt1;
  if(!vmt)
    return;
  
  for(vmt1=vmt; vmt1; vmt1=(sui_obj_vmt_t *)vmt1->vmt_parent){
    if(!vmt1->vmt_done)
      continue;
    vmt1->vmt_done(obj);
  }

  if(vmt->vmt_refcnt != SUI_STATIC){
    /* this is enough for now, but it would
       get in orders more hard for the muti threaded case */
    if(vmt->vmt_refcnt>1){
      vmt->vmt_refcnt--;
    }else{
      if(vmt->vmt_refcnt>0){
	vmt->vmt_refcnt--;
      }
    }
  }
}

/**
 * sui_obj_done - Finalize object derived from SUI base object type.
 * @obj: Pointer to the object.
 *
 * This function cannot be used for primary and simple types not embedding
 * pointer to the VMT table.
 *
 * Return Value: none.
 * File: sui_objbase.c
 */
void 
sui_obj_done(sui_obj_t *obj)
{
  sui_obj_done_internal(obj, obj->vmt);
}

/**
 * sui_obj_done_vmt - Finalize for any object type.
 * @obj: Pointer to the object.
 * @vmt: Pointer to the object type compatible VMT.
 *
 * This function can be used even for primary and simple types
 * with %SUI_OTINFO_NOVMT flag set. It uses real object VMT
 * in the case, that @vmt parameter (ie. object parent/compatible
 * type) supports VMT.
 *
 * Return Value: none.
 * File: sui_objbase.c
 */
void 
sui_obj_done_vmt(sui_obj_t *obj, sui_obj_vmt_t *vmt)
{
  if(vmt->vmt_obj_tinfo->flags & SUI_OTINFO_NOVMT)
    sui_obj_done_internal(obj, vmt);
  else
    sui_obj_done_internal(obj, obj->vmt);
}

/**
 * sui_obj_destroy - Finalize and release object derived from SUI base object type.
 * @obj: Pointer to the object.
 *
 * This function cannot be used for primary and simple types not embedding
 * pointer to the VMT table.
 *
 * Return Value: none.
 * File: sui_objbase.c
 */
void
sui_obj_destroy(sui_obj_t *obj)
{
  sui_obj_done_internal(obj, obj->vmt);
  obj->vmt->vmt_deallocate(obj);
}

/**
 * sui_obj_done_vmt - Finalize and release memory for any object type.
 * @obj: Pointer to the object.
 * @vmt: Pointer to the object type compatible VMT.
 *
 * This function can be used even for primary and simple types
 * with %SUI_OTINFO_NOVMT flag set. It uses real object VMT
 * in the case, that @vmt parameter (ie. object parent/compatible
 * type) supports VMT.
 *
 * Return Value: none.
 * File: sui_objbase.c
 */
void
sui_obj_destroy_vmt(sui_obj_t *obj, struct sui_obj_vmt *vmt)
{
  if(vmt->vmt_obj_tinfo->flags & SUI_OTINFO_NOVMT){
    sui_obj_done_internal(obj, vmt);
    vmt->vmt_deallocate(obj);
  }else{
    sui_obj_done_internal(obj, obj->vmt);
    obj->vmt->vmt_deallocate(obj);
  }
}

/**
 * sui_obj_register_vmt - Register and update new type VMT.
 * @vmt: Pointer to the registered VMT.
 *
 * The functions check object VMT and all its parents correctness
 * and registers and updates each of referenced VMTs if necessary.
 * The update propagates unassigned method fields from parents VMTs.
 *
 * Return Value: Non-zero value indicates problem with type registration.
 * File: sui_objbase.c
 */
int sui_obj_register_vmt(void *vmt)
{
  void **p, **r;
  int parent_size;
  sui_obj_vmt_t* vmt_as_obj = (sui_obj_vmt_t*)vmt;

  if(!vmt_as_obj->vmt_size) return -1;
  
  if(vmt_as_obj->vmt_parent){
    int res;
    res=sui_obj_check_vmt_ok((sui_obj_vmt_t*)vmt_as_obj->vmt_parent);
    if(res<0)
      return res;
  }

  if(vmt_as_obj->vmt_refcnt != SUI_STATIC)
    vmt_as_obj->vmt_refcnt++;

  if(vmt_as_obj->vmt_parent){
    int inherited_offs=UL_OFFSETOF(sui_obj_vmt_t,vmt_new)/sizeof(void*);
    p=(void**)vmt+inherited_offs;
    r=(void**)vmt_as_obj->vmt_parent+inherited_offs;
    parent_size=((sui_obj_vmt_t*)(vmt_as_obj->vmt_parent))->vmt_size;
    while(((char*)p-(char*)vmt) < parent_size){
      if(!*p) *p=*r;
      p++; r++;
    }
  }

  if(!vmt_as_obj->vmt_typeid)
    vmt_as_obj->vmt_typeid = sui_obj_get_unique_typeid();

  if(vmt_as_obj->vmt_typeid & SUI_TYPE_REGISTER_FL){
    vmt_as_obj->vmt_typeid &= ~SUI_TYPE_REGISTER_FL;
  }

  return sui_obj_register_typeid(NULL, vmt_as_obj->vmt_typeid, vmt);
}

int
sui_obj_add_instance_data(sui_obj_t *obj)
{
  if(obj->instance_data==NULL){
    obj->instance_data=malloc(sizeof(sui_obj_instance_data_t));
    if(obj->instance_data==NULL) return -1;
    memset(obj->instance_data,0,sizeof(sui_obj_instance_data_t));
  }
  return 0;
}

void
sui_obj_destroy_instance_data(sui_obj_t *obj)
{
  sui_obj_slot_t   *obj_slot;
  sui_obj_signal_t *obj_signal;
  sui_obj_instance_data_t *instance_data=obj->instance_data;
 
  if(!obj->instance_data)
    return;

  obj->instance_data=NULL;

  gavl_cust_for_each_cut(sui_obj_signal, instance_data, obj_signal){
    evc_tx_hub_done(&obj_signal->tx_hub);
    free(obj_signal);
  }
  gavl_cust_for_each_cut(sui_obj_slot, instance_data, obj_slot){
    evc_rx_hub_done(&obj_slot->rx_hub);
    free(obj_slot);
  }
  free(instance_data);
}

/**
 * sui_obj_dynamic_cast_vmt - Cast object to the required type if possible
 * @cast_to_vmt: Pointer to the VMT which object type should be cast to
 * @obj: Pointer to the object.
 * @vmt: Pointer to the original object type VMT.
 *
 * The functions checks if original type or some of its parents are equivalent
 * to the cast to type.
 *
 * Return Value: Function returns NULL if cast to type is does not fall into subset
 *               of the original object type.
 * File: sui_objbase.c
 */
void *sui_obj_dynamic_cast_vmt(struct sui_obj_vmt *cast_to_vmt, void *obj, struct sui_obj_vmt *vmt)
{
  if(obj==NULL)
    return NULL;

  if(sui_obj_check_vmt_ok(cast_to_vmt)<0)
    return NULL;

  if(!(vmt->vmt_obj_tinfo->flags & SUI_OTINFO_NOVMT)){
    vmt=((sui_obj_t *)obj)->vmt;
    if(vmt==NULL)
      return NULL;
  }

  while(vmt!=cast_to_vmt) {
    if(vmt->vmt_parent==NULL)
      return NULL;
    vmt=(sui_obj_vmt_t*)vmt->vmt_parent;
  }
  return obj;
}

/******************************************************************************/
/* Signal slot related functions */

int sui_obj_translate_and_call_hub_context_fnc(evc_rx_hub_t *rx_hub, va_list args)
{
  sui_obj_slot_t *obj_slot;
  obj_slot=UL_CONTAINEROF(rx_hub, sui_obj_slot_t, rx_hub);
  return obj_slot->translate.call_va_context(obj_slot->target.fnc_ptr,obj_slot->rx_hub.context,args);
}

typedef int sui_propagate_va_same_fnc_t(evc_rx_hub_t *rx_hub, va_list args);

/**
 * sui_propagate_va_same - The basic direct arguments propagation over signal slot link.
 * @link: Pointer to the activated link.
 * @args: The va_list type arguments representation.
 *
 * This function represent direct propagation of arguments from signal activated
 * through sui_obj_emit() to the target slot or stand-alone function.
 *
 * Return Value: Only zero value is returned.
 * File: sui_objbase.c
 */
int sui_propagate_va_same(evc_link_t *link, va_list args)
{
  void *context;
  sui_propagate_va_same_fnc_t *fnc;
  
  if(link->dead){
    if(link->standalone && link->dst.standalone.weakptr){
      *link->dst.standalone.weakptr=NULL;
      link->dst.standalone.weakptr=NULL;
    }
    return 0;
  }
  if(link->standalone){
    fnc=(sui_propagate_va_same_fnc_t*)link->dst.standalone.rx_fnc;
    context=link->dst.standalone.context;
  }else{
    if(!link->dst.multi.hub) return 0;
    fnc=(sui_propagate_va_same_fnc_t*)link->dst.multi.hub->rx_fnc;
    context=link->dst.multi.hub->context;
  }
  if(fnc)
    fnc(link->dst.multi.hub, args);
  return 0;
}

/**
 * ul_obj_connect_propagate - Connect signal to slot with propagation function specified.
 * @src:	Event source object.
 * @signalid:	Signal ID in the source object @src signals list.
 * @dst:	Event destination object.
 * @slotid:	Slot ID in the destination object @dst slots list.
 * @prop_fnc:	Function for propagation and possible transformation of event arguments.
 *
 * Return Value: Non-zero value informs about impossibility to proceed.
 * File: sui_objbase.c
 */
int ul_obj_connect_propagate(sui_obj_t *src,  sui_signalid_t signalid,
			     sui_obj_t *dst,  sui_slotid_t slotid,
			     void *prop_fnc)
{
  sui_obj_slot_t   *obj_slot;
  sui_obj_signal_t *obj_signal;
  evc_link_t *link;
  int res;

  obj_slot=dst->vmt->vmt_find_slot_byid(dst, slotid);
  if(!obj_slot) return -1;
  obj_signal=src->vmt->vmt_find_signal_byid(src, signalid);
  if(!obj_signal) return -1;
  
  link=evc_link_new();
  if(!link) return -1;
  
  res=evc_link_connect(link, &obj_signal->tx_hub, &obj_slot->rx_hub, prop_fnc);
  evc_link_dec_refcnt(link);
  return res;
}

/**
 * ul_obj_connect - Connect signal to slot with same (compatible) arguments.
 * @src:	Event source object.
 * @signalid:	Signal ID in the source object @src signals list.
 * @dst:	Event destination object.
 * @slotid:	Slot ID in the destination object @dst slots list.
 *
 * Only type exact signal and slot connection is supported by actual version.
 *
 * Return Value: Non-zero value informs about impossibility to proceed.
 * File: sui_objbase.c
 */
int ul_obj_connect(sui_obj_t *src, sui_signalid_t signalid, sui_obj_t *dst, sui_slotid_t slotid)
{
  sui_args_tinfo_t *src_args;
  sui_args_tinfo_t *dst_args;
  int i;
  void *prop_fnc=sui_propagate_va_same;
  
  if((slotid<0)||(signalid<0))
    return -1;
  
  src_args=src->vmt->vmt_signal_args_tinfo(src, signalid);
  dst_args=dst->vmt->vmt_slot_args_tinfo(dst, slotid);

  if(dst_args) {
    if(!src_args) return -1;
    if(dst_args->count > src_args->count)
      return -1;
    for(i=0;i<dst_args->count;i++){
      if(src_args->args[i]!=dst_args->args[i])
        return -1;
    }
  }

  return ul_obj_connect_propagate(src, signalid, dst, slotid, prop_fnc);
}

/**
 * sui_obj_emit - Emit signal.
 * @self:	Pointer to the object emitting signal.
 * @signalid:	Identifier of the signal within @self object.
 *
 * Return Value: none.
 * File: sui_objbase.c
 */
void sui_obj_emit(sui_obj_t *self, sui_signalid_t signalid,  ...)
{
  va_list args;
  sui_obj_signal_t *obj_signal;
  
  if(!self->instance_data)
    return;

  obj_signal=sui_obj_signal_find(self->instance_data,&signalid);
  if(!obj_signal)
    return;

  va_start (args, signalid);
  evc_tx_hub_propagate(&obj_signal->tx_hub, args);
  va_end (args);
}

/******************************************************************************/
/* Member methods of sui_obj object follows */

sui_obj_t *sui_obj_vmt_new(struct sui_obj_vmt *vmt)
{
  sui_obj_t *self;
  if(sui_obj_check_vmt_ok(vmt)<0)
    return NULL;
  
  self=malloc(vmt->vmt_obj_tinfo->obj_size);
  if(!self) return NULL;
  if(sui_obj_init_vmt(self, vmt)<0){
    free(self);
    return NULL;
  }
  return self;
}

void sui_obj_vmt_deallocate(sui_obj_t *self)
{
  free(self);
}

void sui_obj_vmt_done(sui_obj_t *self)
{
  if(self->instance_data)
    sui_obj_destroy_instance_data(self);
}

sui_obj_signal_tinfo_t * sui_obj_vmt_find_signal_tinfo_byid(sui_obj_t *self, sui_signalid_t signalid)
{
  sui_obj_vmt_t *vmt;
  sui_obj_signal_tinfo_t *signal_tinfo;
  int signal_count;

  for(vmt=self->vmt;vmt;vmt=(sui_obj_vmt_t*)vmt->vmt_parent){
    signal_count=vmt->vmt_obj_tinfo->signal_count;
    signal_tinfo=vmt->vmt_obj_tinfo->signal_tinfo;
    for(;signal_count--;signal_tinfo++){
      if(signal_tinfo->signalid==signalid){
	return signal_tinfo;
      }
    }
  }
  return NULL;
}

struct sui_obj_signal * sui_obj_vmt_find_signal_byid(sui_obj_t *self, sui_signalid_t signalid)
{
  sui_obj_signal_tinfo_t *signal_tinfo;
  sui_obj_signal_t *obj_signal;

  if(sui_obj_check_instance_data(self)<0)
    return NULL;

  obj_signal=sui_obj_signal_find(self->instance_data,&signalid);
  if(obj_signal)
    return obj_signal;

  signal_tinfo=sui_obj_vmt_find_signal_tinfo_byid(self, signalid);
  if(!signal_tinfo)
    return NULL;

  obj_signal=malloc(sizeof(sui_obj_signal_t));
  if(!obj_signal)
    return NULL;
  memset(obj_signal,0,sizeof(sui_obj_signal_t));
  evc_tx_hub_init(&obj_signal->tx_hub);
  obj_signal->signalid=signalid;
  sui_obj_signal_insert(self->instance_data,obj_signal);
  return obj_signal;
}

sui_obj_slot_tinfo_t * sui_obj_vmt_find_slot_tinfo_byid(sui_obj_t *self, sui_slotid_t slotid)
{
  sui_obj_vmt_t *vmt;
  sui_obj_slot_tinfo_t *slot_tinfo;
  int slot_count;

  for(vmt=self->vmt;vmt;vmt=(sui_obj_vmt_t*)vmt->vmt_parent){
    slot_count=vmt->vmt_obj_tinfo->slot_count;
    slot_tinfo=vmt->vmt_obj_tinfo->slot_tinfo;
    for(;slot_count--;slot_tinfo++){
      if(slot_tinfo->slotid==slotid){
	return slot_tinfo;
      }
    }
  }
  return NULL;
}

struct sui_obj_slot * sui_obj_vmt_find_slot_byid(sui_obj_t *self, sui_slotid_t slotid)
{
  sui_obj_slot_tinfo_t *slot_tinfo;
  sui_obj_slot_t *obj_slot;
  void *target_fnc = NULL;
  void *translate_fnc = NULL;

  if(sui_obj_check_instance_data(self)<0)
    return NULL;

  obj_slot=sui_obj_slot_find(self->instance_data,&slotid);
  if(obj_slot)
    return obj_slot;

  slot_tinfo=sui_obj_vmt_find_slot_tinfo_byid(self, slotid);
  if(!slot_tinfo)
    return NULL;
    
  switch(slot_tinfo->target_kind){
    case SUI_STGT_METHOD_OFFSET:
      if(slot_tinfo->target.method_offset>=self->vmt->vmt_size)
        return NULL;
      target_fnc=*(void**)((char*)self->vmt+slot_tinfo->target.method_offset);
      translate_fnc=slot_tinfo->args_tinfo->call_va_context;
      break;
    case SUI_STGT_METHOD_FNC:
      target_fnc=slot_tinfo->target.method_fnc;
      translate_fnc=slot_tinfo->args_tinfo->call_va_context;
      break;
    default:
      return NULL;
  }

  obj_slot=malloc(sizeof(sui_obj_slot_t));
  if(!obj_slot)
    return NULL;
  memset(obj_slot,0,sizeof(sui_obj_slot_t));
  
  obj_slot->translate.call_va_fnc=translate_fnc;
  obj_slot->target.ptr=target_fnc;
    
  evc_rx_hub_init(&obj_slot->rx_hub, (evc_rx_fnc_t*)sui_obj_translate_and_call_hub_context_fnc, self);
  obj_slot->slotid=slotid;
  sui_obj_slot_insert(self->instance_data,obj_slot);
  return obj_slot;
}

sui_signalid_t sui_obj_vmt_find_signalid_byname(sui_obj_t *self, char *name, int name_len)
{
  sui_obj_vmt_t *vmt;
  sui_obj_signal_tinfo_t *signal_tinfo;
  int signal_count;

  if(name_len<=0)
    name_len=strlen(name);
  
  for(vmt=self->vmt;vmt;vmt=(sui_obj_vmt_t*)vmt->vmt_parent){
    signal_count=vmt->vmt_obj_tinfo->signal_count;
    signal_tinfo=vmt->vmt_obj_tinfo->signal_tinfo;
    for(;signal_count--;signal_tinfo++){
      if(strncmp(signal_tinfo->name,name,name_len))
        continue;
      if(signal_tinfo->name[name_len])
        continue;
      return signal_tinfo->signalid;
    }
  }
  return -1;
}

sui_slotid_t sui_obj_vmt_find_slotid_byname(sui_obj_t *self, char *name, int name_len)
{
  sui_obj_vmt_t *vmt;
  sui_obj_slot_tinfo_t *slot_tinfo;
  int slot_count;

  if(name_len<=0)
    name_len=strlen(name);
  
  for(vmt=self->vmt;vmt;vmt=(sui_obj_vmt_t*)vmt->vmt_parent){
    slot_count=vmt->vmt_obj_tinfo->slot_count;
    slot_tinfo=vmt->vmt_obj_tinfo->slot_tinfo;
    for(;slot_count--;slot_tinfo++){
      if(strncmp(slot_tinfo->name,name,name_len))
        continue;
      if(slot_tinfo->name[name_len])
        continue;
      return slot_tinfo->slotid;
    }
  }
  return -1;
}

sui_args_tinfo_t *sui_obj_vmt_signal_args_tinfo(sui_obj_t *self, sui_signalid_t sigid)
{
  sui_obj_signal_tinfo_t *signal_tinfo;
  signal_tinfo=sui_obj_vmt_find_signal_tinfo_byid(self, sigid);
  return signal_tinfo->args_tinfo;
}

sui_args_tinfo_t *sui_obj_vmt_slot_args_tinfo(sui_obj_t *self, sui_slotid_t slotid)
{
  sui_obj_slot_tinfo_t *slot_tinfo;
  slot_tinfo=sui_obj_vmt_find_slot_tinfo_byid(self, slotid);
  return slot_tinfo->args_tinfo;
}

sui_refcnt_t sui_obj_inc_refcnt(sui_obj_t *self)
{
  return SUI_REFCNTIMPOSIBLE;
}

sui_refcnt_t sui_obj_dec_refcnt(sui_obj_t *self)
{
  return SUI_REFCNTIMPOSIBLE;
}

SUI_CANBE_CONST
sui_obj_tinfo_t sui_obj_vmt_obj_tinfo = {
  .name = "sui_obj",
  .obj_size = sizeof(sui_obj_t),
  .obj_align = UL_ALIGNOF(sui_obj_t),
  .signal_count = 0,
  .signal_tinfo = NULL,
  .slot_count = 0,
  .slot_tinfo = NULL
};

sui_obj_vmt_t sui_obj_vmt_data = {
  .vmt_size = sizeof(sui_obj_vmt_t),
  .vmt_refcnt = SUI_STATIC,
  .vmt_parent = NULL,
  .vmt_init = NULL,
  .vmt_done = sui_obj_vmt_done,
  .vmt_obj_tinfo = UL_CAST_UNQ1(sui_obj_tinfo_t *, &sui_obj_vmt_obj_tinfo),

  .vmt_new = sui_obj_vmt_new,
  .vmt_deallocate = sui_obj_vmt_deallocate,
  .vmt_find_signal_byid = sui_obj_vmt_find_signal_byid,
  .vmt_find_slot_byid = sui_obj_vmt_find_slot_byid,
  .vmt_find_signalid_byname = sui_obj_vmt_find_signalid_byname,
  .vmt_find_slotid_byname = sui_obj_vmt_find_slotid_byname,
  .vmt_signal_args_tinfo = sui_obj_vmt_signal_args_tinfo,
  .vmt_slot_args_tinfo = sui_obj_vmt_slot_args_tinfo,
  
  .inc_refcnt = sui_obj_inc_refcnt,
  .dec_refcnt = sui_obj_dec_refcnt

};


/******************************************************************************/



