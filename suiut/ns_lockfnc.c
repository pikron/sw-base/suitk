/* ns_lockfnc.c
 *
 * SUITK namespace system and its support
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#include <suiut/namespace.h>
#include <suiut/ns_lockfnc.h>

ns_lock_fnc_t *ns_lock_fnc;
ns_unlock_fnc_t *ns_unlock_fnc;

int ns_lock(struct ns_list *pns, unsigned int mode)
{
  if(ns_lock_fnc == NULL)
    return 0;
  return ns_lock_fnc(pns, mode);
}

int ns_unlock(struct ns_list *pns, unsigned int mode)
{
  if(ns_unlock_fnc == NULL)
    return 0;
  return ns_unlock_fnc(pns, mode);
}
