/* sui_files.c
 *
 * SUITK files support.
 *
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */


#include "sui_files.h"

#include <sha1.h>

#include <string.h>

/******************************************************************************
 * SUITK common files support
 ******************************************************************************/
/**
 * sui_file_get_type_from_ext - return type of source from a file name extension
 * @fname: pointer to a filename
 *
 * The function returns type of file as a combination of a type and subtype (flags SUI_FILE_TGRP_ and SUI_FILE_TID_).
 * Return Value: The function returns identifier of a file type or SUI_FILE_TGRP_UNKNOWN for all unknown types.
 */
int sui_file_get_type_from_ext(char *fname)
{
  char *cext;
  int szext = 0;
  if (fname) {
    cext = fname+strlen(fname);
    while(cext>fname && *(cext-1)!='.') {
      szext++;
      cext--;
    }
    if (cext>(fname+1)) {
      if (!strcmp(cext,"sxml")) {
        return SUI_FILE_TGRP_SOURCE | SUI_FILE_TID_SXML;
      } else if (!strcmp(cext,"txt")) {
        return SUI_FILE_TGRP_TEXT | SUI_FILE_TID_TXT;
      } else if (!strcmp(cext,"bmp")) {
        return SUI_FILE_TGRP_IMAGE | SUI_FILE_TID_BMP;
      } else if (!strcmp(cext,"pcx")) {
        return SUI_FILE_TGRP_IMAGE | SUI_FILE_TID_PCX;
      }
    }
  }
  return SUI_FILE_TGRP_UNKNOWN;
}

/**
 * sui_file_srctable_current_ptr - pointer to current table of sources
 * If it is set to NULL no translation is used (name is directly used as a name of file)
 */
sui_file_src_desc_t *sui_file_srctable_current_ptr = NULL;

/**
 * sui_file_srctable_register - register a new source table
 * @newtable: pointer to a new source table
 * The function registers a new source table as currently used table for converting
 * source name to source (file or memory block). Each registered table has to be finished
 * with NULL record (the last record must have set name = NULL).
 * Return Value: The function returns pointer to the previously registered table.
 */
sui_file_src_desc_t *sui_file_srctable_register(sui_file_src_desc_t *newtable)
{
  sui_file_src_desc_t *ret = sui_file_srctable_current_ptr;
  sui_file_srctable_current_ptr = newtable;
  return ret;
}

/**
 * sui_file_srctable_unregister - unregister the currently registered source table
 * The function unregisters source table and returns pointer to the last table.
 * Return Value: The function returns pointer to the previously registered table.
 */
sui_file_src_desc_t *sui_file_srctable_unregister(void)
{
  return sui_file_srctable_register(NULL);
}

/**
 * sui_file_srctable_find - Find source in the currently registered table
 * @name: name of source
 * @namesize: size of name or -1 for entire $name string
 * @combpath: string with path for combining with $name for files (for both: direct filename and filename from table)
 * @pathsize: size of path for combining or -1 for entire $combpath string
 * @fname_memory: pointer to an output buffer for pointer to a filename or memory address
 * @size: pointer to an output buffer for pointer to size of memory block or SUI_FILE_SOURCE_FILE
 * @type: pointer to an output buffer for returning type of data (it can be NULL)
 *
 * The function tries find a source with required $name in the currently registered source table.
 * If any table isn't registered the function directly sets output buffer $fname_memory to $name
 * and $size to SUI_FILE_SOURCE_FILE.
 * If $combpath is set and source is a file the $combpath is used as a path and combined with filename. In this case
 * the function creates a new filename dynamically (malloc) and it must be released (free).
 *
 * Return Value: The function returns 0 if translation (or direct usage) has been successfull. If the returned name is
 * dynamically created (after combining with path) the function returns 1. If any error occurs (no source, pointers
 * to output buffer is NULL,...) the function returns a negative value.
 */
int sui_file_srctable_find(char *name, int namesize, char *combpath, int pathsize,
                           char **fname_memory, long *size, sui_file_type *type)
{
  char *n = NULL;
  long s = 0;
  sui_file_type t = SUI_FILE_TGRP_UNKNOWN;
  if (!name || !fname_memory || !size) return -2;
  if (!sui_file_srctable_current_ptr) {
    n = name;
    s = SUI_FILE_SOURCE_FILE;
    if (type) t = sui_file_get_type_from_ext(name); /* call it only if it is necessary */
  } else {
    sui_file_src_desc_t *p = sui_file_srctable_current_ptr;
    if (namesize<0) namesize = strlen(name);
    while(1) { /* searching */
      if (!p->name) return -1; /* end of table */
      if (strlen(p->name)==namesize && !strncmp(name, p->name, namesize)) {
        n = (char *) p->addr;
        s = p->size;
        t = p->type;
        if (s == SUI_FILE_SOURCE_FILE &&
            t==(SUI_FILE_TGRP_UNKNOWN | SUI_FILE_TID_FROMFILE)) {
          if (type) t = sui_file_get_type_from_ext(n); /* call it only if it is necessary */
        }
        break;
      }
      p++;
    }
  }
  if (type) *type = t; /* return also type of file */
/* combine the found filename with a path */
  *size = s;
  if (s == SUI_FILE_SOURCE_FILE && combpath) {
    char *ff = sui_combine_path(combpath, pathsize, n, -1);
    if (ff) { /* dynamically created name ... must be released */
      *fname_memory = ff;
      return 1;
    }
  } else {
    *fname_memory = n;
    return 0; /* static name or address of a memory block */
  }
  return -1; /* error */
}


/******************************************************************************/
/**
 * sui_file_open - open file or use memory block and return it as a file_source structure
 * @fname_memory: pointer to filename for FILE_SOURCE and direct pointer to data for FILE_MEMORY
 * @size: size of file buffer in memory or SUI_FILE_DOC for file
 * @prevsrc: pointer to a previous file_source for link up to a chain
 * @mode: mode of opening file (write or read)
 *
 * The function prepares file_source structure from filename or memory address and opens a source if it's a file.
 *
 * Return: The function returns created file_source structure or NULL if there was a failure.
 */
sui_file_source_t *sui_file_open(char *fname_memory, long size,
                                  sui_file_source_t *prevsrc, int mode)
{
  sui_file_source_t *nsrc = sui_malloc(sizeof( sui_file_source_t));
  if (!nsrc) return NULL;

  do {
    nsrc->line = 1;

    if (size == SUI_FILE_SOURCE_FILE) {
      nsrc->flags = SUI_FSRC_FILE | SUI_FSRC_OPENED;
      nsrc->source.file.file = fopen(fname_memory, (mode & SUI_FM_WRITE) ? "w" : "r");
      if (!nsrc->source.file.file)
        break;
      nsrc->source.file.filename = strdup(fname_memory);
    } else {
      nsrc->source.memory.addr = fname_memory;
      if (!nsrc->source.memory.addr)
        break;
      nsrc->flags = SUI_FSRC_MEMORY | SUI_FSRC_OPENED;
      nsrc->source.memory.size = size;
    }

    nsrc->prev = prevsrc;
    nsrc->flags |= SUI_FSRC_OPENED;
    return nsrc;

  } while(0);
  sui_file_close( nsrc);
  return NULL;
}

/**
 * sui_file_close - destroy file_source structure (close file).
 * @dsrc: Pointer to a file_source structure
 *
 * The function release file_source structure and closes a source if it is a file.
 * Return: The function returns pointer to the previous file_source structure.
 */
sui_file_source_t *sui_file_close(sui_file_source_t *dsrc)
{
  sui_file_source_t *ret = NULL;
  if (dsrc && (dsrc->flags & SUI_FSRC_OPENED)) {
    switch(dsrc->flags & SUI_FSRC_TYPEMASK) {
    case SUI_FSRC_FILE:
      if (dsrc->source.file.file)
        fclose(dsrc->source.file.file);
      if (dsrc->source.file.filename)
        free(dsrc->source.file.filename);
      break;
    case SUI_FSRC_MEMORY:
      break;
    }
    ret = dsrc->prev;
    free(dsrc);
  }
  return ret;
}

/**
 * sui_file_assign_type - assign source type to a file_source structure
 * @psrc: Pointer to a file_source structure.
 * @type: A new type of file_source ('SUI_FILE_TGRP_' and 'SUI_FILE_TID_')
 *
 * The function assigns a new type to a file_source structure.
 *
 * Return Value: The function returns zero if success and a negative value otherwise.
 */
int sui_file_assign_type(sui_file_source_t *psrc, sui_file_type type)
{
  if (psrc) {
    psrc->type = type;
    return 0;
  }
  return -1;
}


/**
 * sui_file_skip - file/memory skip byte function
 * @fs: pointer to file_source structure
 * @cnt: count of bytes to skip
 * The function change the current position in the source about $cnt bytes.
 * Return Value: The function returns number of actually skipped bytes.
 */
long sui_file_skip(sui_file_source_t *file, long cnt)
{
  long hlp;
  if (!file || !(file->flags & SUI_FSRC_OPENED)) return 0;

  switch(file->flags & SUI_FSRC_TYPEMASK) {
    case SUI_FSRC_FILE:
      fseek(file->source.file.file, cnt, SEEK_CUR);
      hlp = ftell(file->source.file.file);
      cnt = hlp-file->position;
      file->position=hlp;
      break;
    case SUI_FSRC_MEMORY:
      if (((file->position+cnt)>=0) && ((file->position+cnt)<file->source.memory.size))
        file->position += cnt;
      else
        cnt = 0;
      break;
  }
  return cnt;
}

/**
 * sui_file_read - common read from file/memory function
 * @file: pointer to file structure
 * @size: size of read block
 * @buf: pointer to buffer
 * The function reads $size bytes from opened file_source.
 * Return Value: The function returns number of read bytes or a negative value as an error.
 */
long sui_file_read( sui_file_source_t *file, long size, void *buf)
{
//	int rsize = 0;
	if ( !file || !(file->flags & SUI_FSRC_OPENED) || !buf) return -1;
	if ( !size) return 0;

	file->flags |= SUI_FSRC_READ;

	switch( file->flags & SUI_FSRC_TYPEMASK) {
	case SUI_FSRC_FILE:
		size = fread( (unsigned char *)buf, 1, size, file->source.file.file);
		break;

	case SUI_FSRC_MEMORY:
		if ( size > file->source.memory.size-file->position)
			size = file->source.memory.size-file->position;
		memcpy( buf, file->source.memory.addr + file->position, size);
		break;
	}
	file->flags &= ~SUI_FSRC_READ;
	file->position += size;
	return size;
}


/**
 * sui_file_write - common write to file/memory function
 * @file: pointer to file structure
 * @size: size of read block
 * @buf: pointer to buffer
 */
long sui_file_write( sui_file_source_t *file, long size, void *buf)
{
  if ( !file || !(file->flags & SUI_FSRC_OPENED) || !buf) return -1;
  if ( !size) return 0;
  file->flags |= SUI_FSRC_WRITTEN;

  switch( file->flags & SUI_FSRC_TYPEMASK) {
    case SUI_FSRC_FILE:
      size = fwrite((unsigned char *)buf, 1, size, file->source.file.file);
//      file->flags |= SUI_FSRC_CHANGED;
      break;

    case SUI_FSRC_MEMORY:
/* FIXME: ...
      if (size > file->source.memory.size-file->position)
        size = file->source.memory.size-file->position;
      memcpy(file->source.memory.addr + file->position, buf, size);
      file->flags |= SUI_FSRC_CHANGED;
*/
      size = 0;
      break;
  }
  file->flags &= ~SUI_FSRC_WRITTEN;
  file->position += size;
  return size;
}

//static long bufersize = SUI_FILE_BUFFER_SIZE;
//static char *buffer = NULL;

/**
 * sui_file_token_separator - find if character is one of searched tokens
 * Return Value: The function returns zero if the character $ch isn't between $separs tokens.
 */
int sui_file_token_separator(char *separs, int ch)
{
/*  while(*separs) {
    if (ch == *separs) return 0;
    separs++;
  }
  return -1;*/
  return (strchr(separs, ch)!=NULL);
}

/**
 * sui_file_skip_upto - skip characters in file up to end of file or one of searched separators
 * @file: pointer to file structure (file or memory block)
 * @separs: array of separators
 * Return Value: The function returns number of skipped characters
 */
long sui_file_skip_upto(sui_file_source_t *file, char *separs)
{
  long size = 0;
  int ch = 0;

  if (!file || !separs) return -1;

  while(1) {
    switch(file->flags & SUI_FSRC_TYPEMASK) {
      case SUI_FSRC_FILE:
        if ((ch = getc(file->source.file.file)) == EOF)
          return size;
        if (sui_file_token_separator(separs, ch) >= 0) { // char is separator
          ungetc(ch, file->source.file.file);
          return size;
        }
        break;
      case SUI_FSRC_MEMORY:
        if (file->position >= file->source.memory.size) /* EOF */
          return size;
        ch = file->source.memory.addr[file->position];
        if (sui_file_token_separator(separs, ch) >= 0) { // char is separator
          return size;
        }
        break;
    }
    file->position++;
    if (ch == '\n') file->line++;
    size++;
  }
  return size;
}

#define SUI_FILE_CHECK_BUFFER_SIZE 1024
/**
 * sui_file_check_consistency - Check file consistency
 * @filename: Pointer to name of the checked file.
 * @addext: Flag. If it is set SHA1 file has the same name as the checked file with added '_sha1'. ('test.txt_sha1')
 *          Otherwise the SHA1 file has the same name without file extension, it is replaced by 'sh1'. ('test.sh1')
 *
 * The function checks file $filename consistency. It computes the file's SHA1 code and compares it
 * with separate file 'xxx.xxx_sha1' or 'xxx.sh1'.
 *
 * Return Value: The function return zero if file's SHA1 code is the same as in its SHA1 file.
 *
 * File: sui_files.c
 */
int sui_file_check_consistency(char *filename, int addext)
{
  SHA_CTX *ctx = NULL;
  unsigned char sha1code[20];
  FILE *f = NULL;
  unsigned char *buf = NULL;
  int len, tmplen, ret = -1;
  ctx = sui_malloc(sizeof(SHA_CTX));
  if (!ctx) goto suifchc_end;
  buf = malloc(SUI_FILE_CHECK_BUFFER_SIZE);
  if (!buf) goto suifchc_end;

/* read SHA1 code from separate file and check filename */
  strcpy((char *)buf, filename);
  if (addext) {
    strcat((char *)buf, "_sha1");
  } else {
    char *tmp = (char *)buf;
    char *ext = NULL;
    while (*tmp) {
      if (*tmp=='.') ext = tmp;
      tmp++;
    }
    if (ext)
      strcpy(ext+1,"sh1");
    else
      strcpy(tmp,"sh1");
  }

  f = fopen((char *)buf, "rt");
  /* try to open file with SHA1 code */
  if (!f) goto suifchc_end;
  /* read and remember SHA1 code */
  tmplen=fread((char *)buf, 1, 40, f);
  if (tmplen!=40) goto suifchc_end;
  for(len=0;len<20;len++) {
    unsigned char ch = buf[len*2];
    if (ch>='a') sha1code[len]=ch-'a'+10;
    else if (ch>='A') sha1code[len]=ch-'A'+10;
    else sha1code[len]=ch-'0';
    sha1code[len]<<=4;
    ch = buf[len*2+1];
    if (ch>='a') ch=ch-'a'+10;
    else if (ch>='A') ch=ch-'A'+10;
    else ch=ch-'0';
    sha1code[len] |= ch & 0x0f;
  }
  tmplen = fread(buf, 1, SUI_FILE_CHECK_BUFFER_SIZE, f);
  if (tmplen<3) goto suifchc_end; /* filename is separated by two spaces and ended with new line */
  len=0;
  while(len<tmplen && !sui_is_pathdiv(buf[len])) len++; /* skip white characters */
  if (sui_is_pathdiv(buf[len])) len++;
  while(tmplen>len && buf[--tmplen]<=' '); /* skip white characters */
  tmplen = tmplen - len + 1;
  if (strncmp(filename+strlen(filename)-tmplen,(char *)buf+len,tmplen))
    goto suifchc_end; /* SHA1 code is from another file */
  fclose(f);
  f = NULL;

/* create SHA1 code from source file */
  f = fopen(filename, "rb");
  if (!f) goto suifchc_end;
  SHA1_Init(ctx);
  do {
    len = fread(buf, 1, SUI_FILE_CHECK_BUFFER_SIZE, f);
    if (len > 0)
      SHA1_Update(ctx, buf, len);
  } while(len==SUI_FILE_CHECK_BUFFER_SIZE);
  fclose(f);
  f = NULL;
  SHA1_Final(buf, ctx);

  /* compare read and computed code */
  for (len=0;len<20;len++) {
    if (sha1code[len]!=buf[len]) goto suifchc_end;
  }

  ret = 0;

suifchc_end:
  if (f) fclose(f);
  if (buf) free(buf);
  if (ctx) free(ctx);
  return ret;
}

/******************************************************************************
 * SUITK support for PCX images
 ******************************************************************************/
/**
 * sui_file_load_pcx_image - load PCX image from suitk file
 * @fsrc: Pointer to a source file.
 * @image: Pointer to an output image structure (structure must be create before saving).
 * @loadpalette: Flag for loading and decoding palette.
 *
 */
int sui_file_pcx_load_image(sui_file_source_t *fsrc, sui_image_t *image, int loadpalette)
{
  unsigned char buf[128];
  int ret = SUI_RET_ERR;
  int err = 0;
  int colors = 0;

  int msize, mbyte, h, ssize, sbyte, wbits, shift;
  MWIMAGEBITS *pmem, mwb;
  unsigned char cnt, val;

  do {
    if (!fsrc || !image || !(fsrc->flags & SUI_FSRC_OPENED))
      break;

    if (sui_file_read(fsrc, 128, &buf) != 128)     /* read and decode PCS header */
      break;

    if (sui_file_pcx_header_identifier(buf)!=SUI_FILE_PCX_ID || /* only ZSoft PCX */
        sui_file_pcx_header_encoding(buf)!=1) /* only RLE coding */
      break;

  /* clear last image */
    if ((image->flags & SUIM_DYNAMIC_DATA) && image->data) {
      free(image->data);
    }
    image->data = NULL;
    if (image->cmap) {
      sui_colortable_dec_refcnt(image->cmap);
    }
    image->cmap = NULL;
  /* fill image structure */
    if (sui_file_pcx_header_ncp(buf)>1) { /* images with number of color planes different from 1 aren't allowed */
      ret = SUI_RET_ERR;
      break;
    }
    image->width = (sui_coordinate_t) (sui_file_pcx_header_xmax(buf)-sui_file_pcx_header_xmin(buf)+1);
    image->height = (sui_coordinate_t) (sui_file_pcx_header_ymax(buf)-sui_file_pcx_header_ymin(buf)+1);
    image->count = 1;

    switch(sui_file_pcx_header_bpp(buf)) {
      case 1:
        image->flags = SUIM_BITMAP_1BIT; break;
      case 2:
        image->flags = SUIM_BITMAP_2BIT; break;
      case 4:
        image->flags = SUIM_BITMAP_4BIT; break;
      case 8:
        image->flags = SUIM_BITMAP_8BIT; break;
      default:
        err = 1; break;
    }
    if (err) break;

    image->flags |= SUIM_DYNAMIC_DATA;

/* load and convert image data */
    msize = image->width * sui_file_pcx_header_bpp(buf) * sui_file_pcx_header_ncp(buf); // bits per line
    ssize = 8 * sizeof(MWIMAGEBITS); // bits per MWIMAGEBITS
    sbyte = (msize + (ssize-1))/ssize; // MWIMAGEBITS per line with ceiling to integers
    mbyte = image->height * sbyte; /* size of required memory for image */

    image->data = malloc(mbyte*sizeof(MWIMAGEBITS));
    if (!image->data) break;
    h = image->height;
    pmem = image->data;
    while(h--) {
      wbits = msize;
      shift = 0; /* shift bit */
      mwb = 0;
      do {
        /* get value and count */
        if (sui_file_read(fsrc, 1, &val)!=1) { err = 1; break; }
        if ((val & 0xc0) == 0xc0) {
          cnt = val & 0x3f;
          if (sui_file_read(fsrc, 1, &val)!=1) { err = 1; break; }
        } else {
          cnt = 1;
        }
        /* fill image data */
        while(cnt-- && wbits>0) {
          shift += 8;
          mwb |= (MWIMAGEBITS)val << (ssize-shift);
          wbits -= 8;
          if ((shift>=ssize) || (wbits<=0)) { /* full MWIMAGEBITS or end of line - save data */
            *pmem++ = mwb;   /* save MWIMAGEBITS */
            mwb = 0;
            shift = 0;
          }
        }
      } while (wbits>0);
      if (err) break;
    }
    if (err) break;

  /* load and convert palette */
    if (loadpalette) {
      colors = ((int)1) << sui_file_pcx_header_bpp(buf);
      err = 0; /* use EGA palette */
      do {
        if (sui_file_pcx_header_version(buf)!=5) /* no PCX version 5 - no VGA support */
          break;
        /* set position in the file_source END-769 - it should be set after data reading */
        if ((sui_file_read(fsrc, 1, &val)!=1) || (val!=SUI_FILE_PCX_PALETTE_ID))
          break;

        err = 1; /* error in reading VGA palette - don't read EGA palette */
        /* decrement colortable & create new colortable from palette */
        sui_colortable_dec_refcnt(image->cmap);
        image->cmap = sui_colortable_create(colors, NULL);
        if (image->cmap) {
          char rgb[3];
          while(colors--) {
            if (sui_file_read(fsrc, 3, rgb) != 3)
              break;
            sui_color_add_to_colortable(image->cmap,
                            sui_color_from_rgb(sui_file_pcx_palette_red(rgb),
                                                sui_file_pcx_palette_green(rgb),
                                                sui_file_pcx_palette_blue(rgb)));
          }
        }
      } while(0);
      if (err==0 && colors<=16) { /* read EGA palette */
        /* decrement colortable & create new colortable from palette */
        sui_colortable_dec_refcnt(image->cmap);
        image->cmap = sui_colortable_create(colors, NULL);
        if (image->cmap) {
          uint8_t *pegap = sui_file_pcx_header_palptr(buf);
          while(colors--) {
            sui_color_add_to_colortable(image->cmap,
                      sui_color_from_rgb(sui_file_pcx_palette_red(pegap),
                                        sui_file_pcx_palette_green(pegap),
                                        sui_file_pcx_palette_blue(pegap)));
            pegap += 3;
          }
        }
      }
    }

    ret = SUI_RET_OK;
  } while(0);

  return ret;
}

/* Save PCX file from image structure */
/**
 * sui_file_save_pcx_image - save PCX image into a suitk file
 * @fdst: Pointer to a destination file.
 * @image: Pointer to an input image structure (structure must be create before saving).
 *
 */
int sui_file_pcx_save_image(sui_file_source_t *fdst, sui_image_t *image)
{
  return SUI_RET_ERR;
}

/******************************************************************************
 * SUITK support for BMP images
 ******************************************************************************/
/**
 * sui_file_load_bmp_image - Load BMP image from suitk file
 * @fsrc: Pointer to a file source with BMP file.
 * @image: Pointer to an output image structure (structure must be create before loading).
 * @loadpalette: Flag for loading and decoding palette.
 *
 * The function loads BMP image into sui_image structure.
 *
 * Return Value: The function returns SUI_RET_OK if the BMP file is successfully loaded
 * otherwise it returns SUI_RET_ERR.
 *
 * File: sui_file.c
 */
int sui_file_load_bmp_image(sui_file_source_t *fsrc, sui_image_t *image, int loadpalette)
{
  bmp_header_t bmphead;
  bmp_info_head_t bmpinfohead;
  bmp_rgbquad_t *palette = NULL, *ppal;

  int colors, msize, ssize, sbyte, mbyte, h;
  int ret = SUI_RET_OK;
  unsigned char *pmem, *pswap, swap;

  if (!fsrc || !image || !(fsrc->flags & SUI_FSRC_OPENED)) return SUI_RET_ERR;

  if (sui_file_read(fsrc, 14 /*sizeof(bmp_header_t)*/, &bmphead) != 14) {
    ret = SUI_RET_ERR; goto load_bmp_ende;
  }
  if (sui_file_read(fsrc, 40 /*sizeof(bmp_info_head_t)*/, &bmpinfohead) != 40) {
    ret = SUI_RET_ERR; goto load_bmp_ende;
  }
/* clear last image */
  if ((image->flags & SUIM_DYNAMIC_DATA) && image->data) {
    free(image->data);
    image->data = NULL;
  }
  if (image->cmap) {
    sui_colortable_dec_refcnt(image->cmap);
    image->cmap = NULL;
  }
/* fill image structure */
  image->width = (sui_coordinate_t) bmpinfohead.width;
  image->height = (sui_coordinate_t) bmpinfohead.height;
  image->count = 1;
  switch(bmpinfohead.bpp) {
  case 1:
    image->flags = SUIM_BITMAP_1BIT; break;
//    case 2:
//      image->flags = SUIM_BITMAP_2BIT; break;
  case 4:
    image->flags = SUIM_BITMAP_4BIT; break;
  case 8:
    image->flags = SUIM_BITMAP_8BIT; break;
  default:
    ret = SUI_RET_ERR; goto load_bmp_ende;
  }
  image->flags |= SUIM_DYNAMIC_DATA;
/* load and convert palette */
  colors = (bmpinfohead.usedcolors) ? bmpinfohead.usedcolors : (1 << bmpinfohead.bpp);

  if (loadpalette) {
    palette = malloc(sizeof(bmp_rgbquad_t) * colors);
    if (!palette) { ret = SUI_RET_ERR; goto load_bmp_ende;}
    if (sui_file_read(fsrc, sizeof(bmp_rgbquad_t) * colors, palette) !=
        (sizeof(bmp_rgbquad_t) * colors)) {
      ret = SUI_RET_ERR; goto load_bmp_ende;
    }
/* decrement colortable & create new colortable from palette */
    sui_colortable_dec_refcnt(image->cmap);
    image->cmap = sui_colortable_create(colors, NULL);
    if (image->cmap) {
      ppal = palette;
      while(colors--) {
        sui_color_add_to_colortable(image->cmap, sui_color_from_rgb(ppal->red, ppal->green, ppal->blue));
        ppal++;
      }
    }
  } else { // skip palette
    if (sui_file_skip(fsrc, sizeof(bmp_rgbquad_t) * colors) !=
        sizeof(bmp_rgbquad_t) * colors) {
      ret = SUI_RET_ERR; goto load_bmp_ende;
    }
  }
/* load and convert image data */
  mbyte = (bmpinfohead.width * bmpinfohead.bpp + 7)/8;
  sbyte = mbyte % 4;
  sbyte = mbyte + ((sbyte) ? 4-sbyte : 0); /* bytes per line in BMP */
  ssize = (bmpinfohead.bitmapsize) ? bmpinfohead.bitmapsize : (sbyte * bmpinfohead.height);

  h = mbyte % sizeof(MWIMAGEBITS);
  mbyte += ((h) ? sizeof(MWIMAGEBITS)-h : 0); /* bytes per line in MWImage */
  msize = bmpinfohead.height * mbyte;

  image->data = malloc(msize);
  if (!image->data) { ret = SUI_RET_ERR; goto load_bmp_ende;}
  h = bmpinfohead.height; pmem = ((unsigned char *) image->data)+ (h-1)*mbyte;
  while(h--) {
    if (ssize<sbyte) break;
    if (sui_file_read(fsrc, mbyte, pmem) != mbyte) {
      ret = SUI_RET_ERR; goto load_bmp_ende;
    }
    // swap bytes !!!
    colors = mbyte; pswap = pmem;
    while(colors) {
      swap = *pswap;
      *pswap = *(pswap+1); pswap++;
      *pswap++ = swap;
      colors -= 2;
    }
    pmem -= mbyte;
    if (sbyte > mbyte) {
      if (sui_file_skip(fsrc, sbyte-mbyte) != sbyte-mbyte) {
        ret = SUI_RET_ERR; goto load_bmp_ende;
      }
    }
    ssize -= sbyte;
  }

load_bmp_ende:
  if (palette) free(palette);
  return ret;
}

/**
 * sui_file_save_bmp_image - save suitk image to BMP image file
 * @fdst: Pointer to a destination file.
 * @image: Pointer to an input image structure (structure must be create before saving)
 */
int sui_file_save_bmp_image(sui_file_source_t *fdst, sui_image_t *image)
{
  bmp_header_t bmphead;
  bmp_info_head_t bmpinfohead;
  bmp_rgbquad_t palette;
  int ret = SUI_RET_OK;

  int sbmph; // size of BMP head + infohead + palette
  int nppmw; // pocet pixelu v mximagebits (2 bytes)
  int nppbmp; // pocet pixelu v bmp datovem prvku (4 bytes)
  int nppb;  // pocet pixelu v bytu
  int nbpp;  // pocet bitu v pixelu
  int ncol;  // pocet barev v obrazku
  int height; // vyska obrazku
  int nmwprow; // pocet mwimagebits na radek
  int nbprow; // pocet bmp datovych prvku na radek
  int xcnt, xmax;
  int dcnt;

  MWIMAGEBITS *pbits, imdata;
  unsigned char *pbmpdata = NULL;


  if ( !fdst || !image || !(fdst->flags & SUI_FSRC_OPENED)) return SUI_RET_ERR;

  sui_image_inc_refcnt(image);

  switch (image->flags & SUIM_TYPEMASK) {
    case SUIM_BITMAP_1BIT: nppb = 8; nbpp = 1; ncol = 2; break;
    case SUIM_BITMAP_4BIT: nppb = 2; nbpp = 4; ncol = 16; break;
    case SUIM_BITMAP_8BIT: nppb = 1; nbpp = 8; ncol = 256; break;
    default: ret = SUI_RET_ERR; goto save_bmp_ende;
  }

  sbmph = 14 + 40 + ncol*4; /* head, infohead, palette */
  nppmw = sizeof(MWIMAGEBITS)*nppb;
  nppbmp = sizeof(unsigned long)*nppb;

  height = image->height * image->count;

  nmwprow = image->width/nppmw;
  if (image->width%nppmw) nmwprow++;
  nbprow = sizeof(MWIMAGEBITS)*nmwprow;
  nbprow += (4 - (nbprow % 4)) % 4;

  pbmpdata = sui_malloc(nbprow);
  if (!pbmpdata) {
    ret = SUI_RET_ERR; goto save_bmp_ende;
  }

/* fill BMP head structure */
  bmphead.filetype = 0x4d42; /* "BM" */
  bmphead.filesize = sbmph + height*nbprow;
  bmphead.reserved1 = 0;
  bmphead.reserved2 = 0;
  bmphead.imageoffset = sbmph;
  if (sui_file_write(fdst, 14 /*sizeof( bmp_header_t)*/, &bmphead) != 14) {
    ret = SUI_RET_ERR; goto save_bmp_ende;
  }

/* fill BMP info head structure */
  bmpinfohead.size = 40;
  bmpinfohead.width = image->width;
  bmpinfohead.height = image->height;
  bmpinfohead.nip = 1;
  bmpinfohead.bpp = nbpp;
  bmpinfohead.compression = 0;
  bmpinfohead.bitmapsize = 0;
  bmpinfohead.horres = 72;
  bmpinfohead.verres = 72;
  bmpinfohead.usedcolors = ncol;
  bmpinfohead.signcolors = 0;
  if (sui_file_write(fdst, 40 /*sizeof( bmp_info_head_t)*/, &bmpinfohead) != 40) {
    ret = SUI_RET_ERR; goto save_bmp_ende;
  }

/* fill and save palette */
  if (image->cmap) xmax = image->cmap->count; else xmax = 0;
  for (xcnt=0;xcnt<ncol;xcnt++) {
    if (xcnt < xmax) {
      palette.blue = image->cmap->ctab[xcnt] & 0xff;
      palette.green = (image->cmap->ctab[xcnt]>>8) & 0xff;
      palette.red = (image->cmap->ctab[xcnt]>>16) & 0xff;
    } else {
      palette.blue = 0;
      palette.green = 0;
      palette.red = 0;
    }
    palette.reserved = 0;
    if (sui_file_write(fdst, 4 /*sizeof( bmp_rgbquad_t)*/, &palette) != 4) {
      ret = SUI_RET_ERR; goto save_bmp_ende;
    }
  }

/* save bitmap data */
  pbits = image->data + (height-1)*nmwprow; // ukazatel na posledni radek obrazku (MWIMAGEBITS)

  while(height-- > 0) {
    memset(pbmpdata, 0, nbprow);
    xcnt = nmwprow;
    dcnt = 0;
    while (xcnt-- > 0) {
      imdata = *pbits++;
      // rozloz imdata do pbmpdata ...  swap bytes ...
      *(pbmpdata+dcnt) = (unsigned char) (imdata>>8);
      *(pbmpdata+dcnt+1) = (unsigned char) imdata;
      dcnt += 2;
    }
    if (sui_file_write(fdst, nbprow, pbmpdata) != nbprow) { // ulozit data
      ret = SUI_RET_ERR; goto save_bmp_ende;
    }
    pbits -= 2*nmwprow;
  }

save_bmp_ende:
  if (pbmpdata) sui_free(pbmpdata);
  sui_image_dec_refcnt(image);
  return ret;
}



/* Save BMP from screen */
int sui_file_save_bmp_screen( char *fname, sui_dc_t *dc, int wmax, int hmax)
{
  FILE *f = NULL;
  int ret = SUI_RET_OK, ssize, dsize, x, y;
  bmp_header_t bmphead;
  bmp_info_head_t bmpinfohead;
  bmp_rgbquad_t bmppal;
  unsigned char *data = NULL;
  unsigned long pix;

  int numpixinbyte = 0;
  int cntpixinbyte = 0;
  int pixmask = 0;

  if (!fname || !dc || !dc->psd) return SUI_RET_ERR;
/* only 32 bpp */
//  if (dc->psd->bpp!=32) return SUI_RET_ERR;

/* open file */
  if ((f=fopen(fname,"w")) == NULL) {
    ret = SUI_RET_ERR;
    goto save_screen_end;
  }

  ssize = 14 + 40; /* head, infohead, no palette */
  if (wmax<0) wmax = dc->psd->xres;
  if (hmax<0) hmax = dc->psd->yres;
  if (dc->psd->bpp>8)
    dsize = wmax * 3;
  else {
    numpixinbyte = 8/dc->psd->bpp;
    pixmask = (1<<dc->psd->bpp);
    ssize += sizeof(bmp_rgbquad_t)*pixmask; /* add palette size - 4 bytes per color */
    pixmask--;
    dsize = wmax / numpixinbyte;
  }

  if (dsize&0x03) /* align line to 4 byte */
    dsize += 4-(dsize&0x03);

/* fill BMP header */
  bmphead.filetype = 0x4d42; /* "BM" */
  bmphead.filesize = ssize + hmax*dsize; /* data */ /* FIXME: size of data */
  bmphead.reserved1 = 0;
  bmphead.reserved2 = 0;
  bmphead.imageoffset = ssize;
  if (fwrite(&bmphead, 1, 14 /*sizeof( bmp_header_t)*/, f) != 14) {
    ret = SUI_RET_ERR; goto save_screen_end;
  }

/* fill BMP info header */
  bmpinfohead.size = 40;
  bmpinfohead.width = wmax;
  bmpinfohead.height = hmax;
  bmpinfohead.nip = 1;
  if ( dc->psd->bpp > 8)
    bmpinfohead.bpp = 24;
  else
    bmpinfohead.bpp = dc->psd->bpp;
  bmpinfohead.compression = 0;
  bmpinfohead.bitmapsize = hmax*dsize;
  bmpinfohead.horres = 300;
  bmpinfohead.verres = 300;
  bmpinfohead.usedcolors = 0;
  bmpinfohead.signcolors = 0;
  if (fwrite(&bmpinfohead, 1, 40 /*sizeof( bmp_info_head_t)*/, f) != 40) {
    ret = SUI_RET_ERR; goto save_screen_end;
  }

/* palette */
  if ( dc->psd->bpp <= 8) {
    y = 1 << dc->psd->bpp;
    for (x=0;x<y;x++) {
      bmppal.red = bmppal.green = bmppal.blue = x*y;
      bmppal.reserved = 0;
      if (fwrite(&bmppal, 1, 4, f) != 4) {
        ret = SUI_RET_ERR; goto save_screen_end;
      }
    }
  }
/* save bitmap - allocate memory for one line */
  if ((data = malloc(dsize))==NULL) {
    ret = SUI_RET_ERR;
    goto save_screen_end;
  }

  y = hmax;
  while(y-- > 0) {
    x = 0;
    memset( data, 0, dsize);
    cntpixinbyte = numpixinbyte - 1;
    while(x<wmax) {
      pix = dc->psd->ReadPixel( dc->psd, x,y);
      if ( dc->psd->bpp > 8) {
        *(data+(x*3)) = pix & 0xff; /* red */
        *(data+(x*3)+1) = (pix>>8) & 0xff; /* green */
        *(data+(x*3)+2) = (pix>>16) & 0xff; /* blue */
      } else {
        *(data+x/numpixinbyte) |= ((pix & pixmask)<<(dc->psd->bpp*cntpixinbyte));
        if (cntpixinbyte==0)
          cntpixinbyte = numpixinbyte - 1;
        else
          cntpixinbyte--;
      }
      x++;
    }
    if (fwrite(data, 1, dsize, f) != dsize) {
      ret = SUI_RET_ERR; goto save_screen_end;
    }
  }
  ret = SUI_RET_OK;

save_screen_end:
  if (data) free(data);
  if (f) fclose(f);
  return ret;
}

