/* sui_di_text.c
 *
 * SUITK dinfo expansion for utf8 strings
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#include "sui_di_text.h"

#include <suiut/support.h>
#include "sui_event.h"

#include <suiut/sui_time.h>

#include <ul_log.h>
#include <string.h>

UL_LOG_CUST(ulogd_suitk)

/******************************************************************************/
/* pointer in dinfo->ptr points directly to utf8 text */

/**
 * sui_utf8_rdval - basic function for reading utf8 text from dinfo
 * @datai: pointer to dinfo for reading
 * @idx: index of read text. For utf8 dinfo must be zero
 * @buf: pointer to output utf8 buffer
 *
 */
int sui_utf8_rdval( sui_dinfo_t *datai, long idx, void *buf)
{
    if(datai->idxsize&&(idx>=datai->idxsize)) return SUI_RET_EINDX;
    if ( datai->ptr) sui_utf8_inc_refcnt( (utf8 *)datai->ptr);
    *(utf8 **)buf = (utf8 *)datai->ptr;
    return SUI_RET_OK;
}

/**
 * sui_utf8_wrval - basic function for writing utf8 text to dinfo
 * @datai: pointer to dinfo, where utf8 text is written
 * @idx: index to dinfo. For utf8 dinfo must be zero
 * @buf: pointer to input utf8 buffer
 *
 */
int sui_utf8_wrval( sui_dinfo_t *datai, long idx, const void *buf)
{
    if(datai->idxsize&&(idx>=datai->idxsize)) return SUI_RET_EINDX;
    if ( datai->ptr) sui_utf8_dec_refcnt( (utf8 *)datai->ptr);
    datai->ptr = *((utf8 **)buf);
    if ( datai->ptr) sui_utf8_inc_refcnt( (utf8 *)datai->ptr);
    sui_dinfo_changed( datai);
    return SUI_RET_OK;
}

/**
 * sui_dinfo_hevent_utf8 - internal function of event handler
 * @dinfo: pointer to dinfo
 * @event: pointer to incomming event
 *
 */
void sui_dinfo_hevent_utf8( struct sui_dinfo *dinfo, struct sui_event *event)
{
    switch( event->what){
        case SUEV_COMMAND:
            if( event->message.command == SUCM_DONE) {
                if( dinfo->ptr) {
                    sui_utf8_dec_refcnt( (utf8 *)(dinfo->ptr));
                    dinfo->ptr = NULL;
                }
            }
            dinfo->evmask = ( short)SUEV_FREE;
            break;
        case SUEV_FREE:
            if( dinfo->ptr) {
                sui_utf8_dec_refcnt(( utf8 *)(dinfo->ptr));
            }
            free( dinfo);
            break;
    }
}

/**
 * sui_dinfo_create_utf8 - main function for creating utf8 dinfo
 * @adata: pointer to initializing utf8 string
 *
 * The function creates utf8 dinfo from utf8 string. UTF8 dinfo
 * can has only one item - it cannot be an array. For utf8 info
 * with array use 'sui_dinfo_create_putf8' function.
 */
sui_dinfo_t *sui_dinfo_create_utf8( utf8 *adata)
{
    sui_dinfo_t *dinfo;
    dinfo=sui_create_dinfo( adata, 0, 0, 0, 0, sui_utf8_rdval, sui_utf8_wrval);
    if(dinfo) {
        sui_utf8_inc_refcnt( adata);
        dinfo->tinfo = SUI_TYPE_UTF8;
        dinfo->idxsize = 0;
        dinfo->hevent = sui_dinfo_hevent_utf8;
        dinfo->evmask = (unsigned short)(SUEV_COMMAND | SUEV_FREE);
    }
    return dinfo;
}



/******************************************************************************/
/* array of pointers to utf texts */

/**
 * sui_putf8_rdval - basic function for reading utf8 text from putf8 dinfo
 * @datai: pointer to dinfo for reading
 * @idx: index of read text
 * @buf: pointer to output utf8 buffer
 *
 */
int sui_putf8_rdval( sui_dinfo_t *datai, long idx, void *buf)
{
    utf8 *pdata;
    if(datai->idxsize&&(idx>=datai->idxsize)) return SUI_RET_EINDX;
    if ( !datai->ptr) return SUI_RET_NCON;    /* array of pointers isn't created */
    pdata = *((utf8 **)datai->ptr + idx);
    if ( pdata) sui_utf8_inc_refcnt( pdata);
    *(utf8 **)buf = pdata;
    return SUI_RET_OK;
}

/**
 * sui_putf8_wrval - basic function for writing utf8 text to putf8 dinfo
 * @datai: pointer to dinfo, where utf8 text is written
 * @idx: index to the specific dinfo
 * @buf: pointer to input utf8 buffer
 *
 */
int sui_putf8_wrval( sui_dinfo_t *datai, long idx, const void *buf)
{
    utf8 **pdata;
    if(datai->idxsize&&(idx>=datai->idxsize)) return SUI_RET_EINDX;
    if ( !datai->ptr) return SUI_RET_NCON;    /* array of pointers isn't created */
    pdata = ((utf8 **)datai->ptr + idx);
    if ( *pdata) sui_utf8_dec_refcnt( *pdata);
    *pdata = *(utf8 **)buf;
    if ( *pdata) sui_utf8_inc_refcnt( *pdata);
    sui_dinfo_changed( datai);
    return SUI_RET_OK;
}

/**
 * sui_dinfo_hevent_putf8 - internal function of event handler
 * @dinfo: pointer to dinfo
 * @event: pointer to incomming event
 *
 */
void sui_dinfo_hevent_putf8( struct sui_dinfo *dinfo, struct sui_event *event)
{
    switch( event->what){
        case SUEV_COMMAND:
            if( event->message.command == SUCM_DONE) {
                if( dinfo->ptr) {
                    long idx = dinfo->idxsize;
                    utf8 **pdata = dinfo->ptr;
                    while ( idx--) {
                        if ( *pdata) {
                            sui_utf8_dec_refcnt( *pdata);
                            *pdata = NULL;
                        }
                        pdata++;
                    }
                }
            }
            dinfo->evmask = ( short)SUEV_FREE;
            break;
        case SUEV_FREE:
            if( dinfo->ptr) {
                long idx = dinfo->idxsize;
                utf8 **pdata = dinfo->ptr;
                while ( idx--) {
                    if ( *pdata) {
                        sui_utf8_dec_refcnt( *pdata);
                    }
                    pdata++;
                }
                free( dinfo->ptr);
            }
            free( dinfo);
            break;
    }
}

/**
 * sui_dinfo_create_utf8 - main function for creating utf8 dinfo
 * @adata: pointer to initializing utf8 string
 *
 * The function creates utf8 dinfo from utf8 string. UTF8 dinfo
 * can has only one item - it cannot be an array. For utf8 info
 * with array use 'sui_dinfo_create_putf8' function.
 */
sui_dinfo_t *sui_dinfo_create_putf8( utf8 *adata, long aidxsize)
{
	sui_dinfo_t *dinfo;
	utf8 **parray = sui_malloc( sizeof( utf8 *) * aidxsize);
	if ( !parray) return NULL;
	
	dinfo=sui_create_dinfo( parray, 0, 0, 0, 0, sui_putf8_rdval, sui_putf8_wrval);
	if( dinfo) {
		dinfo->idxsize = aidxsize;
		if ( adata) {
			*parray = adata;
			sui_utf8_inc_refcnt( adata);
		}
		dinfo->tinfo = SUI_TYPE_UTF8;
		dinfo->hevent = sui_dinfo_hevent_putf8;
		dinfo->evmask = (unsigned short)(SUEV_COMMAND | SUEV_FREE);
	}
	return dinfo;
}




/******************************************************************************/
/* common function for read/write utf8 from/to dinfo */

/**
 * sui_rd_utf8 - read utf8 text from dinfo
 * @datai: pointer to dinfo
 * @idx: index to read item from dinfo array
 * @buf: pointer to output buffer
 * The function reads value of dinfo (one item of array) and
 * it translates value to utf8.
 * Return: function return code
 * File: sui_di_text.c
 */
int sui_rd_utf8( sui_dinfo_t *datai, long idx, utf8 **buf)
{
    if(!datai->rdval) return SUI_RET_EPERM;
    if ( sui_check_typecompat( datai->tinfo, SUI_TYPE_UTF8) == SUI_RET_OK) {
        return datai->rdval( datai, idx, buf);
    } else {        /* incompatible types */
        return sui_rd_typeconv( datai, idx, buf, SUI_TYPE_UTF8);
    }
}

/**
 * sui_wr_utf8 - write utf8 text to dinfo
 * @datai: pointer to dinfo
 * @idx: index to item in dinfo array
 * @buf: pointer to input buffer with utf8 text
 * The function writes utf8 text to dinfo or dinfo array.
 * Return: function return code
 * File: sui_di_text.c
 */
int sui_wr_utf8( sui_dinfo_t *datai, long idx, utf8 * const *buf)
{
    if(!datai->wrval) return SUI_RET_EPERM;
    if ( sui_check_typecompat( datai->tinfo, SUI_TYPE_UTF8) == SUI_RET_OK) {
        return datai->wrval( datai, idx, buf);
    } else {        /* incompatible types */
        return sui_wr_typeconv( datai, idx, buf, SUI_TYPE_UTF8);
    }
}


/******************************************************************************/
/**
 *
 */
utf8 *sui_di2utf8_maskedlong(long value, sui_choicetable_t *choices, unsigned short flags)
{
  utf8 *txt = NULL;
  sui_choice_t *ch = choices->chtab;
  int size = choices->count;
  if (ch && size) {
    while(size--) {
      if(!ch->mask) {
        txt = ch->text;
        break;
      }
      if((ch->mask & value) == ch->value) {
        txt = ch->text;
        break;
      }
      ch++;
    }
    if (!txt) { /* no item */
      txt = sui_utf8((utf8char *)"", -1, -1);
    } else
      sui_utf8_inc_refcnt( txt);
  } else {
    if (flags & SUTR_FORM_JSON)
      txt = sui_utf8("null", -1, -1);
    else
      txt = sui_utf8("choice ERROR", -1, -1);
  }
  return txt;
}

/**
 * sui_di2utf8_long - translate long value and format to utf8
 * @value: value to transform
 * @fdigits: number of decimal places
 * @size: required size for transformed text (in characters)
 * @base: base of number
 * @flags: translation flags
 */
utf8 *sui_di2utf8_long(long value, int fdigits, int size, int base, unsigned short flags)
{
  utf8 *txt = NULL;
  short i = 0, j = -1, k = 0, x;
  unsigned char minus = 0;
  utf8char dig[40], letter;

  /* transform value to a formated text */
  letter = (flags & SUTR_SMALLLETTERS) ? 'a'-10 : 'A'-10;
  if (!base) base = 10;
  if (!(txt = sui_utf8(NULL, size+1, -1)))
    return NULL;
  if (value < 0 || (flags & SUTR_MUSTBEMINUS)) {
    size--;
    value = -value;
    minus = 1;
  }
/* j - index of the first digit ( the lowest) */
/* k - index of the last digit (the biggest) */
  while (i < size) {
    x = (short)(value % base);
    value = value / base;
    if (x) {
      if ( j<0) j=i;
      k = i;
    }
    if (x < 10)
      dig[i] = (utf8char) ('0' + x);
    else
      dig[i] = (utf8char) (letter + x);
    i++;
    /* add decimal point only in float-point numbers */
    if ((i==fdigits) && i<size && fdigits) {
      dig[i] = *(sui_utf8_get_text(SUI_NLS_USE_NLS ? SUI_NLS_POINT : (utf8 *) "."));
      i++;
    }
  }
  if (value || ((i-1) < fdigits)) { /* Over */ /* TODO: zkontrolovat - bylo predtim v podmince (i-1)<=fdigits ... ale pro fdigits=0,value=0 a size=1 to nebylo spravne ?!?!? */
    if (flags & SUTR_FORM_JSON)
      txt = sui_utf8("null", -1, -1);
    else {
      if (minus) sui_utf8_ncpy(txt, _("-Over"),size+1); // size+1 because minus
      else sui_utf8_ncpy(txt, _("+Over"), size+1); // size);
    }
  } else {
    if (flags & SUTR_DECPOSMASK) {
      j = fdigits - (flags & SUTR_DECPOSMASK);
    } else {
      if (fdigits) {
        if (j<0 || j>(fdigits+1))
          j = (fdigits) ? (fdigits+1) : 0;
      } else {
        j = 0;
      }
      if (flags & SUTR_DECIMALZEROS)
        j = 0;
    }
    if (k < fdigits)
      k = fdigits+1;
    if (!(flags & SUTR_FILLUPBYZERO))
      i = k+1;
    if (minus)
      sui_utf8_add_char(txt, (utf8char *) "-");
    while (i > j) {
      sui_utf8_add_char(txt, &dig[--i]);
    }
  }
  return txt;
}

/* FIXME: merge this function with wtime functions ... */
#define SUI_TIME_MAX_STRING_SIZE 32
/**
 * sui_di2utf8_time - translate value representing epoch time to utf8 string (simplified)
 * @value: second from epoch time
 * @format_text: format text
 * @flags: conversion flags %SUTR_RELATIVETIME, %SUTR_HMSTIME, %SUTR_UTCTIME, %SUTR_LOCALTIME
 */
utf8 *sui_di2utf8_time(unsigned long value, utf8 *format_text, unsigned short flags)
{
  utf8 *ret = NULL;
  utf8char *format;

  const char defformat[] = "%H:%M:%S %e.%n.%Y"; /* Sat Nov 04 12:02:33 EST 1989 */

  int fsize, chsize, schv = 0; //fchv, 
  char *nstr = NULL;
  int nadd, nonum;
  unsigned long nnum = 0;
  int negsgn = 0;
  int nwidth;

  sui_time_elements_t tm;

  if (flags & (SUTR_RELATIVETIME | SUTR_HMSONLYTIME)) {
    if ((long)value < 0) {
      value = -(long)value;
      negsgn = 1;
    }
    memset(&tm, 0, sizeof(tm));
    tm.sec = value % 60; value /= 60;
    tm.min = value % 60; value /= 60;
    tm.hour = value;
    if (!(flags & SUTR_HMSONLYTIME)) {
      tm.hour %= 24; value /= 24;
      tm.mday = value;
      tm.yday = value;
    }
  } else if (flags & SUTR_LOCALTIME) {
    if (sui_time_convert_sec2local( value, &tm) < 0) return NULL;
  } else {
    if (sui_time_convert_sec2struct( value, &tm) < 0) return NULL;
  }

  if ( !format_text) { /* no format - return time in default format */
    format = (utf8char*)defformat;
    fsize = sui_utf8_length( U8(defformat));
  } else {
    format = sui_utf8_get_text( format_text); /* get format string */
    fsize = sui_utf8_length( format_text);
  }

  if ((ret = sui_utf8( NULL, SUI_TIME_MAX_STRING_SIZE, -1)) == NULL)
    return NULL;

  while( fsize) {
    nonum = 0;
    nwidth = 0;
    nadd = 0;

    chsize = sui_utf8_char_size( format);
    if (( chsize==1) && (*format== '%')) { /* special char */
      fsize--; format++;
      chsize = sui_utf8_char_size( format);
      if ( chsize == 1) {
        while(fsize && (*format >= 0) && (*format <= 9)) {
          nwidth = (*format - '0') + nwidth * 10;
          fsize--; format++;
        }
        if (!fsize)
          break;

        switch( *format) {

        case '%': /* '%' character */
          nonum = 1;
          nstr = "%";
          break;

        case 'C': /* century */
          nadd = 2; nnum = (tm.year /100)+1; break;
        case 'y': /* year in century 00-99 */
          nadd = 2; nnum = tm.year % 100; break;
        case 'Y': /* year */
          nadd = 4; nnum = tm.year; break;
        case 'd': /* day of month aligned to right with zeros */
          nadd = 2; nnum = tm.mday; break;
        case 'e': /* day of month aligned to right with spaces */
          nadd = -2; nnum = tm.mday; break;
        case 'H': /* hour 00-23 (00-99) aligned to right with zeros */
          nadd = 2; nnum = tm.hour; break;
        case 'k': /* hour 00-23 (00-99) aligned to right with spaces */
          nadd = -2; nnum = tm.hour; break;

        case 'I': /* hour 01-12 with zeros */
          nadd = 2; nnum = tm.hour % 12;
          if ( nnum == 0) nnum = 12;
          break;
        case 'l': /* hour 01-12 with spaces */
          nadd = -2; nnum = tm.hour % 12;
          if ( nnum == 0) nnum = 12;
          break;
        case 'p': /* AM/PM */
          nonum = 1;
          if (tm.hour / 12)
            nstr = "PM";
          else
            nstr = "AM";
          break;
        case 'P': /* am/pm */
          nonum = 1;
          if (tm.hour / 12)
            nstr = "pm";
          else
            nstr = "am";
          break;

        case 'j': /* day of year 001-366 */
          nadd = 3; nnum = tm.yday + 1; break;
        case 'm': /* month 01-12 aligned to right with zeros */
          nadd = 2; nnum = tm.mon + 1; break;
        case 'n': /* month 01-12 aligned to right with spaces */
          nadd = -2; nnum = tm.mon + 1; break;

        case 'M': /* minute 00-59 */
          nadd = 2; nnum = tm.min; break;
        case 'S': /* second 00-59 */
          nadd = 2; nnum = tm.sec; break;
        case 'u':
          nnum = tm.wday; break;
        case 'w': /* day of week 0-6 (Sun) or (1-7) Mon */
          nnum = tm.wday+1; break;
        case 'U': case 'V': case 'W': /* week of year 0-53 */
          nadd = 2; nnum = 0; break; /* FIXME: implement week of year ... */
        case 's':
          nadd = 10; sui_time_convert_struct2sec( &tm, &nnum); break;
        }

        if (!nonum) {
          if (sui_generic_long_to_str( &nnum, &nstr, 0, 10) != SUI_RET_OK) {
            nonum = 1;
            nstr = "--";
            if(nadd > 0)
              nadd = -nadd;
          }
        }
        schv = sui_utf8_length( U8(nstr));
        if (nadd || nwidth) {
          if (nadd < 0) {
            if(nwidth < nadd)
              nwidth = nadd;
            nadd = 0;
          }
          nadd = nadd > schv?  nadd - schv: 0;
          nwidth -= schv + nadd + negsgn;
          if(nwidth > 0) {
            schv += nwidth;
            do {
              sui_utf8_add_char( ret, (utf8char*)" ");
            } while(--nwidth);
          }
          if (negsgn) {
            sui_utf8_add_char( ret, (utf8char*)"-");
            negsgn = 0;
            schv++;
          }
          schv += nadd;
          while(nadd--)
            sui_utf8_add_char( ret, (utf8char*)"0");
        }
        sui_utf8_cat( ret, U8(nstr));
        if(!nonum)
          free( nstr);
        nstr = NULL;
      }
    } else {
      sui_utf8_add_char( ret, format);
    }
    fsize--;
    format += chsize;
  }
  return ret;
}

/**
 * sui_utf8_to_formstr - translate dinfo and finfo to formated string
 */
utf8 *sui_utf8_to_formstr(utf8 *number, sui_finfo_t *fi)
{
  utf8 *ret = NULL;

  if ( fi && fi->ftext) {
    switch (fi->flags & SUFI_TEXTMASK) {
      case SUFI_TEXT:
        {
          int pos = sui_utf8_search(fi->ftext, U8"%n");
          if (pos >= 0) {
            utf8 *tmp;
            utf8char *fstr = sui_utf8_get_text(fi->ftext);
            tmp = sui_utf8_nconcate(U8(fstr), pos, number, -1);
            ret = sui_utf8_concate(tmp, U8(fstr+sui_utf8_count_bytes(fstr, pos+2)));
            sui_utf8_dec_refcnt(tmp);
          }
        }
        break;
      case SUFI_PREFIX:
        ret = sui_utf8_concate(fi->ftext, number);
        break;
      case SUFI_SUFFIX:
        ret = sui_utf8_concate(number, fi->ftext);
        break;
    }
  }
  if (!ret) {
    sui_utf8_inc_refcnt(number);
    ret = number;
  }
  return ret;
}


/**
 * sui_dinfo_2_utf8 - Conversion from dinfo to utf8
 * @data: pointer to dinfo
 * @idx: index to item in dinfo
 * @format: format of output utf8 string (for number and masked text dinfo)
 * @flags: addition flags for conversion [SUTR_]
 */

utf8 *sui_dinfo_2_utf8(sui_dinfo_t *data, long idx, sui_finfo_t *format, unsigned short flags)
{
  int ret = 0;
  int size;
  long num;
  utf8 *txt = NULL;
  short base;
  unsigned convkind = 0;

  if (format != NULL) {
    convkind = format->flags & SUFI_TEXTMASK;
    if (!convkind && format->ftext) {
      ul_logmsg("sui_dinfo_2_utf8: choosing implicit SUFI_TIME conversion\n");
      convkind = SUFI_TIME;
    }

    if (convkind == SUFI_TIME) {
      if (format->flags & (SUFI_RELATIVETIME | SUFI_HMSONLYTIME)) {
        flags |= SUTR_RELATIVETIME;
        flags &= ~(SUTR_RELATIVETIME | SUTR_UTCTIME);
        if (format->flags & SUFI_HMSONLYTIME)
          flags |= SUTR_HMSONLYTIME;
      } else if (format->flags & SUFI_TZAPPLY2TIME) {
        if (!(flags & SUTR_UTCTIME))
          flags |= SUTR_LOCALTIME;
      }
    } else {
      if(format->flags & SUFI_DECIMALZEROS)
        flags |= SUTR_DECIMALZEROS;
      if(format->flags & SUFI_SMALLLETTERS)
        flags |= SUTR_SMALLLETTERS;
    }

    if (format->flags & SUFI_CHECKLIMITS)
      flags |= SUTR_CHECKLIMITS;
  }

  if (!data)                      /* added 2.3.2004 - update test for empty data */
    goto sui_dinfo2utf8_err;

  if (format) {
    size = format->digits;
    base = format->base;
  } else {
    size = 10; base = 10;
  }
/* switch to different dinfo types - number, text, m2txt, ... */

/* read number from dinfo */
  if (data->tinfo == SUI_TYPE_UNKNOWN) {
    if (flags & SUTR_FORM_JSON)
      txt = sui_utf8("null", -1, -1);
    else
      txt = U8 "Unknown dinfo type";
    ret = SUI_RET_OK; /* opravdu ??? !!! */
  } else {
    if (data->tinfo == SUI_TYPE_UTF8) { // text
      ret = sui_rd_utf8(data, idx, &txt);
    } else { // number
      ret = sui_rd_long(data, idx, &num);
    }
  }
  if (ret==SUI_RET_NRDY) { /* NRDY error */
    goto sui_dinfo2utf8_err;
  } else if (ret==SUI_RET_OK) { /* SUI_RET_OK */
    if (data->tinfo != SUI_TYPE_UTF8) {
      if ((flags & SUTR_CHECKLIMITS) && (data->minval != data->maxval)) {
        if ((num < data->minval) || (num > data->maxval))
          goto sui_dinfo2utf8_err;
      }
      if (convkind == SUFI_TIME) { /* convert time */
        txt = sui_di2utf8_time(num, format->ftext, flags);
      } else { /* convert long */
        utf8 *tmp = NULL;
        if (format && format->choices) {
          tmp = sui_di2utf8_maskedlong(num, format->choices, flags);
        } else {
          tmp = sui_di2utf8_long(num, data->fdigits, size, base, flags);
        }
        txt = sui_utf8_to_formstr(tmp, format);
        sui_utf8_dec_refcnt(tmp);
      }
    }
  } else { /* all other errors */
    if (flags & SUTR_FORM_JSON)
      txt = sui_utf8("null", -1, -1);
    else
      txt = sui_utf8((utf8char *) _("Error"), -1, -1);
  }

  return txt;
sui_dinfo2utf8_err:
  if (flags & SUTR_FORM_JSON)
    txt = sui_utf8( "null", -1, -1);
  else
    txt = sui_utf8((utf8char *) "---", -1, -1);
  return txt;
}


/******************************************************************************/
void sui_dinfo_cat_proxy(struct sui_dinfo *dinfo, struct sui_event *event)
{
	switch(event->what){
		case SUEV_COMMAND:
/*
		#ifdef SUI_DINFO_WITH_EVC
			if(event->message.command==SUCM_EVC_LINK_TO){
				if(dinfo->ptr)
					sui_dinfo_pass_hevent_link_to((sui_dinfo_t*)(dinfo->ptr),event);
			return;
			}
		#endif //SUI_DINFO_WITH_EVC
*/
			if(event->message.command==SUCM_DONE){
				if(dinfo->ptr) {
					sui_dinfo_cat_t *dicat = (sui_dinfo_cat_t *)(dinfo->ptr);
					if ( dicat->first) sui_dinfo_dec_refcnt( dicat->first);
					dicat->first = NULL;
					if ( dicat->second) sui_dinfo_dec_refcnt( dicat->second);
					dicat->second = NULL;
				}
				dinfo->evmask &= (short)SUEV_FREE;
			}
			break;
		case SUEV_FREE:
			if(dinfo->ptr) {
				sui_dinfo_cat_t *dicat = (sui_dinfo_cat_t *)(dinfo->ptr);
				if ( dicat->first) sui_dinfo_dec_refcnt( dicat->first);
				if ( dicat->second) sui_dinfo_dec_refcnt( dicat->second);
			}
			free(dinfo);
			break;
	}
}

int sui_dinfo_cat_rdval(sui_dinfo_t *datai, long idx, void *buf)
{
  sui_dinfo_cat_t *dicat = (sui_dinfo_cat_t *)(datai->ptr);
  utf8 *str1 = NULL, *str2 = NULL, *strout = NULL;
  int ret = SUI_RET_OK;
  int cnt;

  if(!dicat || !dicat->first || !dicat->second) return SUI_RET_NCON;
  if(!dicat->first->rdval || !dicat->second->rdval) return SUI_RET_ERR;

  if ((ret = sui_rd_utf8(dicat->first, idx, &str1)) != SUI_RET_OK)
    return ret;
  if ((ret = sui_rd_utf8(dicat->second, idx, &str2)) != SUI_RET_OK) {
    sui_utf8_dec_refcnt(str1);
    return ret;
  }

  if ((dicat->mode & SUI_DICAT_NONE_NULL) &&
      (sui_utf8_length(str1)==0) && (sui_utf8_length(str2)==0)) {
    sui_utf8_dec_refcnt(str1);
    sui_utf8_dec_refcnt(str2);
    *(utf8 **)buf = NULL;
    return ret;
  }

  cnt = sui_utf8_bytesize(str1) + sui_utf8_bytesize(str2) + 1;
  if ((dicat->mode & SUI_DICAT_MODE_MASK) == SUI_DICAT_SPACE) cnt++;
  strout = sui_utf8(NULL, -1, cnt);
  sui_utf8_cat(strout, str1);
  if ((dicat->mode & SUI_DICAT_MODE_MASK) == SUI_DICAT_SPACE)
    sui_utf8_cat(strout, U8" ");
  sui_utf8_cat(strout, str2);
  sui_utf8_dec_refcnt(str1);
  sui_utf8_dec_refcnt(str2);
  *(utf8 **)buf = strout;
  return ret;
}

/* FIXME: How split string without in normal('no space') mode ? */
/*
int sui_dinfo_cat_wrval( sui_dinfo_t *datai, long idx, const void *buf)
{
	utf8 *newstr = *((utf8 **) buf);
	utf8 *str1 = NULL, *str2 = NULL;
	int ret = SUI_RET_OK;
	sui_dinfo_cat_t *dicat;

	if ( !datai->ptr) return SUI_RET_NCON;
	dicat = (sui_dinfo_cat_t *)datai->ptr;
	if ( !dicat->first || !dicat->second) return SUI_RET_NCON;
// find, where will be string splitted 
	if ( newstr) {
		int slen, end1 = 0, beg2 = 0;
		slen = sui_utf8_length( newstr);
		if ((dicat->mode & SUI_DICAT_MODE_MASK) == SUI_DICAT_SPACE) {
			end1 = sui_utf8_nsearch( newstr, U8" ", 1);
		} else {
			end1 = slen/2;
		}

		if ( end1>slen) end1=slen;
		end1 = sui_utf8_count_bytes( sui_utf8_get_text( newstr), end1);
		beg2 = end1 + ((dicat->mode & SUI_DICAT_MODE_MASK) == SUI_DICAT_SPACE) ? 1 : 0; // skip space
		slen = sui_utf8_bytesize( newstr);
		if ( beg2>=slen) beg2=slen;

		if (end1) str1 = sui_utf8( NULL, -1, end1+1);
		if (beg2 < slen) str2 = sui_utf8( NULL, -1, slen-beg2+2);
//		sui_utf8_ncpy( str1, newstr, end1);
//		sui_utf8_ncpy( str2, U8(sui_utf8_get_text( newstr)+beg2, end1);
	}
	ret = sui_wr_utf8( dicat->first, idx, &str1);
	ret |= sui_wr_utf8( dicat->second, idx, &str2);
	return ret;
}
*/

/**
 * sui_dinfo_cat_create - Create proxy dinfo for merging two (utf8) dinfos
 * @di_first: pointer to the first underlying DINFO
 * @di_second: pointer to the second underlying DINFO
 * @mode: mode of merging - SUI_DICAT_NORMAL, SUI_DICAT_SPACE
 */
sui_dinfo_t *sui_dinfo_cat_create( sui_dinfo_t *di_first, sui_dinfo_t *di_second, int amode, long ainfo)
{
	sui_dinfo_t *di;
	sui_dinfo_cat_t *dicat = NULL;

	if ( !di_first || !di_second) return NULL;
	if ( !(dicat=malloc(sizeof(sui_dinfo_cat_t)))) return NULL;
	
	di = sui_create_dinfo( dicat, 0, 0, 0, ainfo, sui_dinfo_cat_rdval, NULL); //, sui_dinfo_cat_wrval);
	if ( di==NULL) {
		free( dicat);
		return NULL;
	}
	di->idxsize = (di_first->idxsize < di_second->idxsize) ? di_first->idxsize : di_second->idxsize;
	sui_dinfo_inc_refcnt( di_first);
	dicat->first = di_first;
	sui_dinfo_inc_refcnt( di_second);
	dicat->second = di_second;
	dicat->mode = amode;
	
	di->hevent=sui_dinfo_cat_proxy;
	di->evmask=(short)(SUEV_COMMAND | SUEV_FREE);
	return di;
}


/******************************************************************************/
/**
 * sui_dinfo_select_rdval
 * function return value of first dinfo if value of select dinfo is lower then treshold
 */
int sui_dinfo_select_rdval( sui_dinfo_t *datai, long idx, void *buf)
{
	sui_dinfo_select_t *disel = (sui_dinfo_select_t *)(datai->ptr);
	long selval = 0;
	int ret = SUI_RET_OK;

	if (( ret = sui_rd_long( disel->select, 0, &selval)) != SUI_RET_OK)
		return ret;
	if ( selval < disel->treshold) {
		return disel->first->rdval( disel->first, idx, buf);
	} else {
		return disel->second->rdval( disel->second, idx, buf);
	}
	return SUI_RET_ERR;
}
int sui_dinfo_select_wrval( sui_dinfo_t *datai, long idx, const void *buf)
{
	sui_dinfo_select_t *disel = (sui_dinfo_select_t *)(datai->ptr);
	long selval = 0;
	int ret = SUI_RET_OK;

	if (( ret = sui_rd_long( disel->select, 0, &selval)) != SUI_RET_OK)
		return ret;
	if ( selval < disel->treshold) {
		return disel->first->wrval( disel->first, idx, buf);
	} else {
		return disel->second->wrval( disel->second, idx, buf);
	}
	return SUI_RET_ERR;
}


void sui_dinfo_select_proxy(struct sui_dinfo *dinfo, struct sui_event *event)
{
	switch(event->what){
		case SUEV_COMMAND:
/*
		#ifdef SUI_DINFO_WITH_EVC
			if(event->message.command==SUCM_EVC_LINK_TO){
				if(dinfo->ptr)
					sui_dinfo_pass_hevent_link_to((sui_dinfo_t*)(dinfo->ptr),event);
			return;
			}
		#endif //SUI_DINFO_WITH_EVC
*/
			if(event->message.command==SUCM_DONE){
				if(dinfo->ptr) {
					sui_dinfo_select_t *disel = (sui_dinfo_select_t *)(dinfo->ptr);
					if ( disel->first) sui_dinfo_dec_refcnt( disel->first);
					disel->first = NULL;
					if ( disel->second) sui_dinfo_dec_refcnt( disel->second);
					disel->second = NULL;
					if ( disel->select) sui_dinfo_dec_refcnt( disel->select);
					disel->select = NULL;
				}
				dinfo->evmask &= (short)SUEV_FREE;
			}
			break;
		case SUEV_FREE:
			if(dinfo->ptr) {
					sui_dinfo_select_t *disel = (sui_dinfo_select_t *)(dinfo->ptr);
					if ( disel->first) sui_dinfo_dec_refcnt( disel->first);
					if ( disel->second) sui_dinfo_dec_refcnt( disel->second);
					if ( disel->select) sui_dinfo_dec_refcnt( disel->select);
			}
			free(dinfo);
			break;
	}
}

sui_dinfo_t *sui_dinfo_select_create( sui_dinfo_t *di_first, sui_dinfo_t *di_second,
                              sui_dinfo_t *di_select, int atreshold, long ainfo)
{
	sui_dinfo_t *di;
	sui_dinfo_select_t *disel = NULL;

	if ( !di_first || !di_second || !di_select) return NULL;
	if ( !(disel=malloc(sizeof(sui_dinfo_select_t)))) return NULL;
	
	di = sui_create_dinfo( disel, 0, 0, 0, ainfo, sui_dinfo_select_rdval, sui_dinfo_select_wrval);
	if ( di==NULL) {
		free( disel);
		return NULL;
	}
	di->idxsize = (di_first->idxsize < di_second->idxsize) ? di_first->idxsize : di_second->idxsize;
	sui_dinfo_inc_refcnt( di_first);
	disel->first = di_first;
	sui_dinfo_inc_refcnt( di_second);
	disel->second = di_second;
	sui_dinfo_inc_refcnt( di_select);
	disel->select = di_select;
	disel->treshold = atreshold;
	
	di->hevent=sui_dinfo_select_proxy;
	di->evmask=(short)(SUEV_COMMAND | SUEV_FREE);
	return di;
}

/******************************************************************************/
/**
 * sui_dinfo_copy_utf8 - copy utf8 dinfo to other one
 */
int sui_dinfo_copy_utf8(sui_dinfo_t *dfrom, sui_dinfo_t *dto, int idx)
{
  utf8 *txt = NULL;
  do {
    if (!dfrom || !dto)
      break;
    if (dfrom->tinfo!=SUI_TYPE_UTF8 || dto->tinfo!=SUI_TYPE_UTF8)
      break;
    if (sui_rd_utf8(dfrom, 0, &txt)!=SUI_RET_OK)
      break;
    if (sui_wr_utf8(dto, 0, &txt)!=SUI_RET_OK)
      break;
    sui_utf8_dec_refcnt(txt);
    return SUI_RET_OK;
  } while(0);
  if (txt) sui_utf8_dec_refcnt(txt);
  return SUI_RET_ERR;
}
