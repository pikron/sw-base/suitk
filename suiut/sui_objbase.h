/* sui_objbase.h
 *
 * SUITK object and method inheritance base.
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#ifndef _SUI_OBJBASE_H_
#define _SUI_OBJBASE_H_

#include <inttypes.h>
#include "ul_utdefs.h"
#include "sui_argsbase.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef int sui_signalid_t;
typedef int sui_slotid_t;
typedef intptr_t sui_intptr_t;

/**
 * struct sui_obj_signal_tinfo - Signal type description
 * @name:	Name of the signal within object
 * @signalid:	ID of the signal, unique within object
 * @args_tinfo:	Pointer to argument type sequence accepted for signal invocation
 *
 * These structures describe signals, which could be emitted
 * by object type. List of the descriptions is pointed by object type
 * description structure.
 *
 * File: sui_objbase.h
 */
typedef struct sui_obj_signal_tinfo {
  char *name;
  sui_signalid_t signalid;
  sui_args_tinfo_t *args_tinfo;
} sui_obj_signal_tinfo_t;

/**
 * struct sui_obj_slot_tinfo - Slot type description
 * @name:	Name of the slot within object
 * @slotid:	ID of the slot, unique within object
 * @args_tinfo:	Pointer to argument type expected by the slot reception routine
 * @target_kind: Kind of the action invoked by received event.
 *		%SUI_STGT_METHOD_OFFSET indicates, that method pointed by
 *		VMT method pointer at offset @method_offset should be invoked.
 *		%SUI_STGT_METHOD_FNC specifies as the final action call
 *		the method function @method_fnc
 * @target:	The union providing information of action invocation,
 *		one of @method_offset or @method_fnc in the current version.
 *
 * These structures describe slots, which are provided by object type.
 * List of the descriptions is pointed by object type description structure.
 *
 * File: sui_objbase.h
 */
typedef struct sui_obj_slot_tinfo {
  char *name;
  sui_slotid_t slotid;
  sui_args_tinfo_t *args_tinfo;
  int target_kind;
  union {
    int method_offset;
    int (*method_fnc)(void *context);
  } target;
} sui_obj_slot_tinfo_t;

#define SUI_STGT_METHOD_OFFSET	1
#define SUI_STGT_METHOD_FNC	2

/**
 * struct sui_obj_tinfo - Basic SUI object type description structure
 * @name:	Name of the object type
 * @obj_size:	Size of the object in bytes
 * @flags:	Object type flags: %SUI_OTINFO_NOVMT .. object is
 *		primary or record type without embedded VMT pointer;
 *		%SUI_OTINFO_NOINSTDATA .. object cannot have instance
 *		specific object system data (does not support signals, etc);
 *		%SUI_OTINFO_NOREFCNT .. object does not support reference counting.
 * @signal_count: Number of signal description attached to the object type.
 * @signal_tinfo: Pointer to the array of the pointers to the signal descriptions.
 * @slot_count:	Number of slots description attached to the object type.
 * @slot_tinfo:	Pointer to the array of the pointers to the slot descriptions.
 *
 * File: sui_objbase.h
 */
typedef struct sui_obj_tinfo {
  char *name;
  int obj_size;
  int obj_align;
  int flags;
  int signal_count;
  sui_obj_signal_tinfo_t *signal_tinfo;
  int slot_count;
  sui_obj_slot_tinfo_t *slot_tinfo;
} sui_obj_tinfo_t;

#define SUI_OTINFO_NOVMT	1
#define SUI_OTINFO_NOINSTDATA	2
#define SUI_OTINFO_NOREFCNT	4

struct sui_obj_slot;
struct sui_obj_signal;
struct sui_obj_instance_data;
struct sui_obj_vmt;

/**
 * struct sui_obj_tinfo - Basic structure common to SUI objects with virtual functionalities
 * @vmt:	Pointer to the object VMT table.
 * @instance_data: The pointer to the additional object system extensions
 *		held per object instance.
 *
 * File: sui_objbase.h
 */
typedef struct sui_obj {
  struct sui_obj_vmt *vmt;
  struct sui_obj_instance_data *instance_data;
} sui_obj_t;

typedef struct sui_obj_nil_vmt {
} sui_obj_nil_vmt_t;

/**
 * SUI_INS_INHERITED_VMT - Macro to insert fields of existing VMT into inherited VMT table
 * @new_type:	The value "new_type" is repeated there.
 * @parent_type: The value "parent_type" is repeated there.
 * @prev_type:	The base name of type derived from. If derived directly from basic
 *		SUI object, the value "sui_obj" is used there.
 *
 * File: sui_objbase.h
 */
#define SUI_INS_INHERITED_VMT(new_type, parent_type, prev_type) \
prev_type##_vmt_fields(new_type, parent_type)

/**
 * sui_obj_vmt_fields - Macro providing list of the VMT fields of base SUI object type
 * @new_type:	The value "new_type" is repeated there.
 * @parent_type: The value "parent_type" is repeated there.
 *
 * File: sui_objbase.h
 */
#define sui_obj_vmt_fields(new_type, parent_type) \
  sui_intptr_t vmt_size; \
  sui_intptr_t vmt_typeid; \
  sui_intptr_t vmt_refcnt; \
  struct parent_type##_vmt *vmt_parent; \
  int (*vmt_init)(new_type##_t *self, void *params); \
  void (*vmt_done)(new_type##_t *self); \
  sui_obj_tinfo_t *vmt_obj_tinfo; \
  /* fields bellow are inherited if NULL */ \
  new_type##_t *(*vmt_new)(struct new_type##_vmt *vmt); \
  void (*vmt_deallocate)(new_type##_t *self); \
  struct sui_obj_signal *(*vmt_find_signal_byid)(new_type##_t *self, sui_signalid_t sigid); \
  struct sui_obj_slot *(*vmt_find_slot_byid)(new_type##_t *self, sui_slotid_t slotid); \
  sui_signalid_t (*vmt_find_signalid_byname)(new_type##_t *self, char *name, int name_len); \
  sui_slotid_t (*vmt_find_slotid_byname)(new_type##_t *self, char *name, int name_len); \
  sui_args_tinfo_t *(*vmt_signal_args_tinfo)(new_type##_t *self, sui_signalid_t sigid); \
  sui_args_tinfo_t *(*vmt_slot_args_tinfo)(new_type##_t *self, sui_slotid_t slotid); \
  struct sui_obj_vmt *vmt_common_type; \
  sui_refcnt_t (*inc_refcnt)(new_type##_t *self); \
  sui_refcnt_t (*dec_refcnt)(new_type##_t *self);

/*
 * struct sui_obj_vmt - Structure providing access mask to the common VMT fields.
 *
 * File: sui_objbase.h
 */
typedef struct sui_obj_vmt {
  sui_obj_vmt_fields(sui_obj, sui_obj_nil)
} sui_obj_vmt_t;

extern sui_obj_vmt_t sui_obj_vmt_data;

/**
 * sui_obj_vmt - Get pointer to VMT from the VMT equipped object pointer.
 * @self: Pointer to the object
 *
 * Return Value: pointer to the VMT of the object
 * File: sui_objbase.h
 */
static inline sui_obj_vmt_t *
sui_obj_vmt(sui_obj_t *self)
{
  return self->vmt;
}

int sui_obj_register_vmt(void *vmt);

int sui_obj_init_vmt(sui_obj_t *obj, void *vmt);

int sui_obj_check_vmt_ok(struct sui_obj_vmt *vmt);

int sui_obj_init(sui_obj_t *obj, sui_typeid_t a_typeid);

void sui_obj_done_vmt(sui_obj_t *obj, sui_obj_vmt_t *vmt);

void sui_obj_done(sui_obj_t *obj);

void sui_obj_destroy_vmt(sui_obj_t *obj, struct sui_obj_vmt *vmt);

void sui_obj_destroy(sui_obj_t *obj);

int sui_obj_add_instance_data(sui_obj_t *obj);

/**
 * sui_obj_check_instance_data - Ensure, that object has allocated instance data extension.
 * @obj: Pointer to the object
 *
 * Return Value: Returns zero value if instance data exist or can be allocated.
 *		Non-zero value informs about problems with extension allocation.
 * File: sui_objbase.h
 */
static inline int
sui_obj_check_instance_data(sui_obj_t *obj)
{
  if(obj->instance_data) return 0;
  return sui_obj_add_instance_data(obj);
}

void *sui_obj_dynamic_cast_vmt(struct sui_obj_vmt *cast_to_vmt, void *obj, struct sui_obj_vmt *vmt);

static inline sui_obj_t *
sui_obj_dynamic_cast(struct sui_obj_vmt *cast_to_vmt, sui_obj_t *obj)
{
  return (sui_obj_t *)sui_obj_dynamic_cast_vmt(cast_to_vmt, obj, obj->vmt);
}

int ul_obj_connect(sui_obj_t *src, sui_signalid_t signalid, sui_obj_t *dst, sui_slotid_t slotid);

void sui_obj_emit(sui_obj_t *self, sui_signalid_t signalid,  ...);

sui_obj_t *sui_obj_new_vmt(struct sui_obj_vmt *vmt);

/*
#define SUI_TYPE_NEW_VMT_FCN_DEC( new_type) \
static inline \
new_type##_t *new_type##_new_vmt( struct new_type##_vmt *vmt) { \
  return (( new_type##_t *) sui_obj_new_vmt(( struct sui_obj_vmt *) vmt)); \
}
*/

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /*_SUI_OBJBASE_H_*/
