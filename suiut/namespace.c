/* namespace.c
 *
 * SUITK namespace system and its support
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#include <suiut/namespace.h>

#include <stdlib.h>
#include <suiut/support.h>
#include <ulut/ul_gavl.h>
#include <ulut/ul_gavlcust.h>

#include <ul_log.h>

UL_LOG_CUST(ulogd_suiut)


ns_list_t *ns_current_namespace = &ns_global_namespace;

ns_subns_t *ns_global_dinfo_subnamespace = NULL;
/******************************************************************************
 * namespace tree
 ******************************************************************************/
GAVL_CUST_NODE_INT_IMP(ns_ns2subns, ns_list_t, ns_subns_t, sui_typeid_t,
  subns_root, subns_node, subtype, ns_bytype_cmp)

GAVL_CUST_NODE_INT_IMP(ns_subns2obj, ns_subns_t, ns_object_t, char *,
  obj_root, obj_node, name, ns_name_cmp)

/******************************************************************************
 * namespace managing function
 ******************************************************************************/
/**
 * ns_create_namespace - Create new dynamic namespace
 * @pns: pointer to namespace pointer
 */
ns_list_t *ns_create_namespace( void)
{
  ns_list_t *nns = malloc( sizeof( ns_list_t));
  if ( nns) {
    memset( nns, 0, sizeof( ns_list_t));
    ns_ns2subns_init_root_field(nns);
  }
  return nns;
}
/**
 * ns_clear_namespace - clear namespace - remove and destroy all object in all subnamespaces
 * The function remove and 
 * @pns: pointer to namespace pointer
 */
void ns_clear_namespace( ns_list_t *pns)
{
  ns_subns_t *psns;
  while((psns=ns_ns2subns_cut_first(pns))){
    ns_destroy_subnamespace( psns);
  }
}
/**
 * ns_destroy_namespace - destroy dynamic namespace (without checking)
 * @pns: pointer to namespace pointer
 */
int ns_destroy_namespace( ns_list_t *pns)
{
  if ( !pns) return -1;
  ns_clear_namespace( pns);
  free( pns);
  return 0;
}

/**
 * ns_create_subnamespace - Create new subnamespace
 * @type: objects subtype
 */
ns_subns_t *ns_create_subnamespace( sui_typeid_t type)
{
  ns_subns_t *nsns;
  nsns = sui_malloc( sizeof( ns_subns_t));
  if ( !nsns) return NULL;
  nsns->subtype = type;
  ns_subns2obj_init_root_field(nsns);
  return nsns;
}
/**
 * ns_clear_subnamespace - Clear subnamespace - remove and destroy all object in subnamespace
 * @psns: pointer to subnamespace
 */
void ns_clear_subnamespace( ns_subns_t *psns)
{
  ns_object_t *pobj;
  while((pobj=ns_subns2obj_cut_first(psns))){
    ns_destroy_object( pobj);
  }
}
/**
 * ns_destroy_subnamespace - Destroy subnamespace (remove all object in subnamespace)
 * @psns: pointer to subnamespace
 */
int ns_destroy_subnamespace( ns_subns_t *psns)
{
  if ( !psns) return -1;
  ns_clear_subnamespace(psns);
  free(psns);
  return 0;
}
/**
 * ns_add_subnamespace - Add subnamespace into namespace
 */
inline
int ns_add_subnamespace( ns_list_t *pns, ns_subns_t *psns)
{
  if ( !pns || !psns) return -1;
  if ((pns == &ns_global_namespace) && (psns->subtype==SUI_TYPE_DINFO))
    ns_global_dinfo_subnamespace = psns;
  return ns_ns2subns_insert(pns,psns);
}

/**
 * ns_remove_subnamespace - Remove subnamespace from namespace
 */
inline
int ns_remove_subnamespace( ns_list_t *pns, ns_subns_t *psns)
{
  return ns_ns2subns_delete(pns,psns);
}

/**
 * ns_create_object - Create namespace object structure
 * @name: pointer to object name
 * @ptr: pointer to object data
 */
ns_object_t *ns_create_object( char *name, void *ptr, sui_typeid_t type, sui_flags_t objflags)
{
  ns_object_t *nobj = NULL;
  if ( !name || !ptr) return NULL;
  nobj = sui_malloc( sizeof( ns_object_t));
  if ( !nobj) return NULL;
  if ( objflags & NSOF_DYNAMIC_NAME)
    nobj->name = strdup( name);
  else {
    nobj->name = name;
    if ( objflags & NSOF_COPY_DYNAMIC_NAME) {
      objflags |= NSOF_DYNAMIC_NAME;
      objflags &= ~NSOF_COPY_DYNAMIC_NAME;
    }
  }
  if (objflags & NSOF_SUBNAMESPACE) { /* nobj->ptr is pointer to ns_list_t */
    nobj->ptr = ptr;
  } else {
    nobj->vmt = sui_typereg_get_vmt_by_typeid( NULL, type);
    if ( !nobj->vmt) {
      ul_logerr("NO VMT TABLE !!!\n");
      return NULL;
    }
    nobj->vmt->inc_refcnt( ptr);
    nobj->ptr = ptr;
  }

  nobj->flags = objflags | NSOF_DYNAMIC;
  return nobj;
}

/**
 * ns_destroy_object - Destroy namespace object structure
 * @pobj: pointer to object structure
 * @pobj: pointer to object desription structure
 * The function remove and release memory from object
 */
int ns_destroy_object( ns_object_t *pobj)
{
  if ( !pobj || !( pobj->flags & NSOF_DYNAMIC)) return -1; /* no error if object is static */
  if ( pobj->ptr) {
    if (pobj->flags & NSOF_DYNAMIC_DATA) {
      if ( pobj->vmt->dec_refcnt( pobj->ptr) == SUI_REFCNTIMPOSIBLE)
        sui_obj_destroy_vmt( pobj->ptr, pobj->vmt);
    } else if (pobj->flags & NSOF_SUBNAMESPACE) {
      ns_destroy_namespace( pobj->ptr);
    }
    pobj->ptr = NULL;
  }
  /*
   * The name is freed after object destruction to allow localization
   * of possible errors in above object destruction
   */
  if ( pobj->name && ( pobj->flags & NSOF_DYNAMIC_NAME)) {
    free( pobj->name);
    pobj->name = NULL;
  }
  free( pobj);
  return 0;
}

/**
 * ns_add_object - Add object to subnamespace
 * @psns: pointer to subnamespace
 * @pobj: pointer to added object
 */
int ns_add_object( ns_subns_t *psns, ns_object_t *pobj)
{
  if ( !psns || !pobj) return -1;
  return ns_subns2obj_insert(psns, pobj);
}
/**
 * ns_remove_object - Remove object from subnamespace
 * @psns: pointer to subnamespace
 * @pobj: pointer to removed object
 * The function returns pointer to removed object or NULL.
 */
ns_object_t *ns_remove_object( ns_subns_t *psns, ns_object_t *pobj)
{
  return NULL; /* FIXME: ??? remove object from namespace !!! */
}

/**
 * ns_add_object_to_ns
 */
int ns_add_object_to_ns( ns_list_t *pns, ns_object_t *pobj) 
{
  ns_subns_t *sns;
  if ( !pobj) return -1;
  sns = ns_find_subnamespace_by_type( pns, pobj->vmt->vmt_typeid); //pobj->type);
  if ( !sns) {
    sns = ns_create_subnamespace( pobj->vmt->vmt_typeid); //pobj->type);
    if ( !sns) return -2;
    if ( ns_add_subnamespace( pns, sns) < 0) return -3;
  }
  return ns_add_object( sns, pobj);
}

/**
 * ns_add_object_to_ns_by_path - add object to namespace according to path and name
 * @pns: pointer to namespace (root of hierarchical namespace)
 * @pobj: pointer to created object (with its name)
 * @path: string with object path in hierarchical namespace, levels are separeted by '/'.
 * If path is NULL, object will be added to root namespace.
 * @pathlen: length of path or -1 (for path which aren't ended with zero)
 */
int ns_add_object_to_ns_by_path(ns_list_t *pns, ns_object_t *pobj, char *path, int pathlen)
{
  ns_list_t *myns = pns;
  if (path!=NULL && pathlen!=0) { /* find/create hierarchical namespace */
    int partlen;
    ns_subns_t *sns;
    ns_object_t *objns;
    char *tmppart;

    while(pathlen) {
      if (sui_is_slash(*path)) { /* skip first character, if it is a separator */
        path++;
        pathlen--;
      }
      partlen = 0;
      while(*(path+partlen) && (partlen<pathlen) && !sui_is_slash(*(path+partlen)))
        partlen++;

      /* move/create next part of path */
      sns = ns_find_subnamespace_by_type( myns, SUI_TYPE_NAMESPACE);
      if  (!sns) { /* NAMESPACE subnamespace doesn't exist - create it */
        sns = ns_create_subnamespace( SUI_TYPE_NAMESPACE);
        if (ns_add_subnamespace(myns, sns) < 0) return -1;
      }
      /* find namespace by name (part of path) */
      if ((tmppart = malloc(partlen+1))!=NULL) {
        strncpy(tmppart, path, partlen);
        *(tmppart+partlen)=0;
        objns = ns_find_object_by_name(sns, tmppart);
        if (!objns) {
          ns_list_t *newns;
          newns = ns_create_namespace();
          if (!newns) return -1;
          objns = ns_create_object(tmppart, newns, SUI_TYPE_NAMESPACE, NSOF_COPY_DYNAMIC_NAME | NSOF_SUBNAMESPACE);
          if (ns_add_object(sns, objns)<0) return -1;
        } else {
          free(tmppart);
        }
        myns = objns->ptr;
        path+=partlen;
        pathlen-=partlen;
      } else
        return -1;
    }
  }
  /* in $myns is pointer to namespace where object $pobj will be added */
  return ns_add_object_to_ns( myns, pobj);
}

/******************************************************************************
 * searching functions
 ******************************************************************************/
/**
 * ns_find_subnamespace_by_type - Find subnamespace by object type
 * @pns: pointer to namespace
 * @type: subnamespace type
 */
//inline
ns_subns_t *ns_find_subnamespace_by_type( ns_list_t *pns, sui_typeid_t type)
{
  if ( pns)
    return ns_ns2subns_find( pns, &type);
  else
    return NULL;
}
/**
 * ns_find_object_by_name - Find object in subnamespace by name
 * @psns: pointer to subnamespace
 * @name: Name of searched object
 */
inline
ns_object_t *ns_find_object_by_name( ns_subns_t *psns, const char *name)
{
  if ( psns && name) {
    return ns_subns2obj_find( psns, (char **) &name);
  } else
    return NULL;
}
/**
 * ns_find_object_by_type_and_name - Find object in namespace by type and name
 * @pns: pointer to namespace
 * @type: type of searched object
 * @name: name of searched object
 */
inline
ns_object_t *ns_find_object_by_type_and_name(ns_list_t *pns, sui_typeid_t type, const char *name)
{
  if ( pns && name && type) {
    ns_subns_t *nsn = ns_find_subnamespace_by_type( pns, type);
    if ( nsn) return ns_find_object_by_name( nsn, name);
  }
  return NULL;
}

/**
 * ns_find_path_in_namespace - Change namespace according to full path
 * @path: pointer to pointer to full path
 * @ppns: pointer to root namespace
 * The function change pointer to path to pointer to name (last part of path)
 * and pointer to root namespace to the namespace
 */
int ns_find_path_in_namespace( char **path, ns_list_t **ppns)
{
  int plen = 0;
  ns_subns_t *nsn;
  ns_object_t *obj = NULL;
  char *part, *pcur = *path;

  while(*pcur) {
    if (sui_is_slash(*pcur)) pcur++; /* cut off first '/' */
    while(*(pcur+plen) && !sui_is_slash(*(pcur+plen))) /* get the first part of path */
      plen++;
    if (*(pcur+plen)) { /* part isn't the last part - find and change namespace */
      nsn = ns_find_subnamespace_by_type(*ppns, SUI_TYPE_NAMESPACE);
      if (!nsn) return -1;
      if ((part = malloc(plen+1))==NULL) return -1;
      strncpy(part, pcur, plen);
      *(part+plen) = 0;
      obj = ns_find_object_by_name(nsn, part);
      free(part);

      if (!obj || !obj->ptr) return -1;
      *ppns = obj->ptr;
      pcur += plen;
      plen = 0;
    } else break;
  }
  *path = pcur;
  return 0;
}

/**
 * ns_find_object_by_type_and_path - Find object in hierarchical namespace by type and its full path(with name)
 * @pns: pointer to root namespace
 * @type: type of searched object
 * @path: path and name to searched object
 */
//inline
ns_object_t *ns_find_object_by_type_and_path(ns_list_t *pns, sui_typeid_t type, char *path)
{

  if (!pns || !path || !type) return NULL;
  if (ns_find_path_in_namespace(&path, &pns)<0) return NULL;
  if (*path) { /* find object */
    return ns_find_object_by_type_and_name(pns, type, path);
  }
  return NULL;
}

/**
 * ns_find_object_in_namespace - Find object by name (all types)
 */
ns_object_t *ns_find_object_in_namespace( ns_list_t *pns, const char *name, ns_subns_t **ret_subns)
{
  ns_object_t *pobj;
  ns_subns_t *psns=ret_subns ? *ret_subns: NULL;

  if(psns)
    psns = ns_ns2subns_next(pns,psns);
  else
    psns = ns_ns2subns_first(pns);

  while ( pns && psns && name) {

    pobj = ns_find_object_by_name(psns, name);
    if(pobj) {
      if(ret_subns)
        *ret_subns = psns;
      return pobj;
    }
    psns = ns_ns2subns_next(pns,psns);
  }
  if(ret_subns)
    *ret_subns=NULL;
  return NULL;
}

/**
 * ns_find_any_object_by_type_and_name - Find object by name and type in any namespace
 */
inline
ns_object_t *ns_find_any_object_by_type_and_name( sui_typeid_t type, const char *name)
{
  ns_object_t *ret = NULL;
  ret = ns_find_object_by_type_and_path( &ns_local_namespace, type, (char *)name);
  if ( !ret)
    ret = ns_find_object_by_type_and_path( &ns_global_namespace, type, (char *)name);
  return ret;
}

/**
 * ns_find_any_object - find object by its name in both namespace (global and local)
 */
ns_object_t *ns_find_any_object( const char *name, ns_subns_t **ret_subns)
{
  ns_object_t *ret = NULL;
  ns_list_t *pns = &ns_local_namespace;
  char *path = (char *) name;
  if (ns_find_path_in_namespace(&path, &pns)>=0) {
    ret = ns_find_object_in_namespace( pns, path, ret_subns);
  }
  if ( !ret) {
    path = (char *) name;
    pns = &ns_global_namespace;
    if (ns_find_path_in_namespace(&path, &pns)>=0) {
      ret = ns_find_object_in_namespace( pns, path, ret_subns);
    }
  }
  return ret;
}

/**
 * ns_find_object_name_by_type_and_pointer - find object name by its type and pointer
 */
char *ns_find_object_name_by_type_and_pointer( ns_list_t *pns, sui_typeid_t type, void *ptr)
{
  ns_object_t *pobj = NULL;
  ns_subns_t *psns = ns_find_subnamespace_by_type( pns, type);
  if ( psns) { /* there is a subnamespace for this type */
    gavl_cust_for_each(ns_subns2obj, psns, pobj) {
      if ( pobj->ptr == ptr) return pobj->name;
    }
  }
  return NULL;
}


/******************************************************************************
 * get object from namespace functions
 ******************************************************************************/
/**
 * ns_get_object - gets object data by its type and name from namespace
 * @type: type of a searched object
 * @name: pointer to name of a searched object
 * The function finds object, increments its reference counter and returns pointer to it (nsobj->ptr)
 * Return Value: The function returns pointer to object data.
 */
void *ns_get_object(sui_typeid_t type, const char *name)
{
  ns_object_t *obj = ns_find_any_object_by_type_and_name(type, name);
  if (obj) {
    if (obj->vmt && obj->vmt->inc_refcnt)
      obj->vmt->inc_refcnt(obj->ptr);
    return obj->ptr;
  } else
    return NULL;
}

/**
 * ns_getobj_dinfo_global - find dinfo in global dinfo subnamespace
 */
sui_dinfo_t *ns_getobj_dinfo_global(const char *name)
{
  ns_object_t *obj;
  if (ns_global_dinfo_subnamespace==NULL) return NULL;
  obj = ns_find_object_by_name(ns_global_dinfo_subnamespace, name);
  if (obj) {
    if (obj->vmt && obj->vmt->inc_refcnt)
      obj->vmt->inc_refcnt(obj->ptr);
    return obj->ptr;
  }
  return NULL;
}

/**
 * ns_getsubns_dinfo_global - returns pointer to global DINFO namespace
 */
ns_subns_t *ns_getsubns_dinfo_global(void)
{
  return ns_global_dinfo_subnamespace;
}

/******************************************************************************
 * other namespace functions
 ******************************************************************************/
/**
 * ns_create_name - create name for noname object
 * @type: object type identifier
 * @pdata: pointer to data
 * The function creates and returns new string, coumpound from type and pointer.
 */
char *ns_create_name( unsigned short type, void *pdata)
{
	char *name = malloc(16);
	if (name) {
		sprintf( name, "n_t%3d_%8p", type, pdata);
	}
	return name;
}


void ns_print_namespace( ns_list_t *pns);

/**
 * ns_print_subnamespace - print names of all objects
 */
void ns_print_subnamespace(ns_subns_t *psns)
{
  ns_object_t *pobj;
  gavl_cust_for_each(ns_subns2obj, psns, pobj) {
    if ((psns->subtype == SUI_TYPE_NAMESPACE) && (pobj->flags & NSOF_SUBNAMESPACE)) {
      ul_logdeb( "-->> SUBNAMESPACE '%s' <<--\n", pobj->name);
      ns_print_namespace(pobj->ptr);
    } else {
      ul_logdeb( "   %s : (%p) => ptr=%p\n", pobj->name, pobj, pobj->ptr);
    }
  }
}
/**
 * ns_print_namespace - print names of all objects in namespace grouped by their type
 */
void ns_print_namespace( ns_list_t *pns)
{
  ns_subns_t *psns;
  sui_obj_vmt_t *vmt;
  char *typename;

  if ( pns) {
    gavl_cust_for_each(ns_ns2subns, pns, psns) {

      vmt = sui_typereg_get_vmt_by_typeid( NULL, psns->subtype & SUI_TYPE_CODE_MASK);
      if ( vmt && vmt->vmt_obj_tinfo && vmt->vmt_obj_tinfo->name)
        typename = vmt->vmt_obj_tinfo->name;
      else
        typename = "???";

      ul_logdeb( "----- Subnamespace TYPE = 0x%04X (%s) -----\n", psns->subtype, typename);
      ns_print_subnamespace( psns);
    }
  } else {
    ul_logdeb("! no NS\n");
  }
}

/******************************************************************************
 * array of widget types for searching in namespace
 ******************************************************************************/
extern const sui_typeid_t ns_widget_types[];

ns_object_t *ns_find_widget_in_ns( char const *name)
{
	ns_object_t *obj = NULL;
	int tid, idx = 0;
	while(( tid = ns_widget_types[idx]) != 0) {
		obj = ns_find_any_object_by_type_and_name( tid, name);
		if ( obj != NULL) break;
		idx++;
	}
	return obj;
}

