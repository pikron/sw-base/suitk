/* sui_dinfo.h
 *
 * Header file for DINFO (data info structures) common functions
 * 
 * Version 0.1
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#ifndef _SUI_DINFO_H_
	#define _SUI_DINFO_H_

#include <suiut/sui_types.h>

#ifdef __cplusplus
extern "C" {
#endif

// #ifndef SUI_STATIC
// 	#define SUI_STATIC (-2)  /* static reference counter */
// #endif /*SUI_STATIC*/


//typedef int sui_tinfo_t;

// datainfo
struct sui_dinfo;
struct sui_event;
struct evc_tx_hub;

typedef void sui_dinfo_hevent_t(struct sui_dinfo *dinfo, struct sui_event *event);

typedef int sui_datai_rdfnc_t( struct sui_dinfo *dinfo, long idx, void *buf);
typedef int sui_datai_wrfnc_t( struct sui_dinfo *dinfo, long idx, const void *buf);

/**
 * struct sui_dinfo - Process data information structure
 * @refcnt:	Reference counter
 * @tinfo:	Data type information, one of %SUI_TYPE_xxx constants
 * @rdval:	Pointer to the value reading (getter) function
 * @wrval:	Pointer to the value writing (setter) function
 * @ptr:	Data pointer, actual use is known only to @rdval and @wrval methods
 * @hevent:	Handle event for control requests
 * @evmask:	Event mask
 * @tx_hub:	Events emitting hub
 * @info:	Some context information for private functions
 * @idxsize:	Number of supported index values
 * @minval:	Minimal accepted value
 * @maxval:	Maximal accepted value
 * @fdigits:	Number of fraction digits for fixed point numbers
 */
typedef struct sui_dinfo {
  int  refcnt;
  sui_typeid_t tinfo;
  sui_datai_rdfnc_t *rdval;
  sui_datai_wrfnc_t *wrval;
  void *ptr;
  sui_dinfo_hevent_t *hevent;
  short evmask;
  struct evc_tx_hub *tx_hub;
  long info;
  long idxsize;
  long minval;
  long maxval;
  int  fdigits;
} sui_dinfo_t;

/********************************************************************/
// refcnt
sui_refcnt_t sui_dinfo_inc_refcnt( sui_dinfo_t *datai);
sui_refcnt_t sui_dinfo_dec_refcnt( sui_dinfo_t *datai);

/********************************************************************/
// dynamic struct
sui_dinfo_t *sui_create_dinfo( void *data, int fdigits, long min, long max, long info,
                               sui_datai_rdfnc_t *rd, sui_datai_wrfnc_t *wr);
sui_dinfo_t *sui_create_dinfo_int( void *adata, long aidxsize, int asize);
sui_dinfo_t *sui_create_dinfo_uint( void *adata, long aidxsize, int asize);
#define sui_end_dinfo sui_dinfo_dec_refcnt

void sui_dinfo_done( sui_dinfo_t *datai);
void sui_dinfo_deallocate( sui_dinfo_t *datai);

/********************************************************************/
// static struct
#ifdef WIN32
	#define sui_dinfo_static( name, ti, p, rd, wr, if, mi, ma, fd) \
		sui_dinfo_t name = { SUI_STATIC, ti, rd, wr, p, NULL, 0, NULL, if, 1, mi, ma, fd}

#else
	#define sui_dinfo_static( name, ti, p, rd, wr, if, mi, ma, fd) \
		sui_dinfo_t name = { \
			.refcnt = SUI_STATIC, \
			.tinfo  = (ti), \
			.ptr    = (p), \
			.rdval  = (rd), \
			.wrval  = (wr), \
			.info   = (if), \
			.idxsize= 1, \
			.minval = (mi), \
			.maxval = (ma), \
			.fdigits= (fd) \
		}
#endif

#ifdef WIN32
  #define sui_dinfo_static_with_idxsize( name, ti, p, rd, wr, if, mi, ma, fd, is) \
    sui_dinfo_t name = { SUI_STATIC, ti, rd, wr, p, NULL, 0, NULL, if, is, mi, ma, fd}
#else
  #define sui_dinfo_static_with_idxsize( name, ti, p, rd, wr, if, mi, ma, fd, is) \
    sui_dinfo_t name = { \
      .refcnt  = SUI_STATIC, \
      .tinfo   = (ti), \
      .ptr     = (p), \
      .rdval   = (rd), \
      .wrval   = (wr), \
      .info    = (if), \
      .minval  = (mi), \
      .maxval  = (ma), \
      .idxsize = (is), \
      .fdigits = (fd) \
    }
#endif

/********************************************************************/
// data connection
int sui_long_rdval( sui_dinfo_t *datai, long idx, void *buf);
int sui_long_wrval( sui_dinfo_t *datai, long idx, const void *buf);
int sui_int_rdval( sui_dinfo_t *datai, long idx, void *buf);
int sui_int_wrval( sui_dinfo_t *datai, long idx, const void *buf);
int sui_short_rdval( sui_dinfo_t *datai, long idx, void *buf);
int sui_short_wrval( sui_dinfo_t *datai, long idx, const void *buf);
int sui_char_rdval( sui_dinfo_t *datai, long idx, void *buf);
int sui_char_wrval( sui_dinfo_t *datai, long idx, const void *buf);

int sui_ulong_rdval( sui_dinfo_t *datai, long idx, void *buf);
int sui_ulong_wrval( sui_dinfo_t *datai, long idx, const void *buf);
int sui_uint_rdval( sui_dinfo_t *datai, long idx, void *buf);
int sui_uint_wrval( sui_dinfo_t *datai, long idx, const void *buf);
int sui_ushort_rdval( sui_dinfo_t *datai, long idx, void *buf);
int sui_ushort_wrval( sui_dinfo_t *datai, long idx, const void *buf);
int sui_uchar_rdval( sui_dinfo_t *datai, long idx, void *buf);
int sui_uchar_wrval( sui_dinfo_t *datai, long idx, const void *buf);

// with TINFO
int sui_rd_long( sui_dinfo_t *datai, long idx, long *buf);
int sui_wr_long( sui_dinfo_t *datai, long idx, const long *buf);

int sui_rd_ulong( sui_dinfo_t *datai, long idx, unsigned long *buf);
int sui_wr_ulong( sui_dinfo_t *datai, long idx, const unsigned long *buf);

// with TINFO and type convertion
int sui_rd_typeconv( sui_dinfo_t *datai, long idx, void *buf, sui_typeid_t buf_type);
int sui_wr_typeconv( sui_dinfo_t *datai, long idx, const void *buf, sui_typeid_t buf_type);

/* Check range of value against DINFO limits*/
int sui_dinfo_rangechk_long( sui_dinfo_t *datai, long val);
int sui_dinfo_rangechk_ulong( sui_dinfo_t *datai, unsigned long val);

/********************************************************************/
// events and events connectors

void sui_dinfo_do_changed( sui_dinfo_t *datai);

static inline
void sui_dinfo_changed( sui_dinfo_t *datai)
{
  if(datai->tx_hub)
    sui_dinfo_do_changed(datai);
}

/********************************************************************/
// scale_proxy and simple_proxy

typedef struct sui_dinfo_scale {
  sui_dinfo_t di;
  long multiply;
  long divide;
} sui_dinfo_scale_t;

int sui_scale_rdval(sui_dinfo_t *dinfo, long indx, void *buf);
int sui_scale_wrval(sui_dinfo_t *dinfo, long indx, const void *buf);
void sui_scale_minmax_update(sui_dinfo_t *dinfo);
void dinfo_simple_proxy_hevent(struct sui_dinfo *dinfo, struct sui_event *event);

sui_dinfo_t *dinfo_simple_proxy( sui_dinfo_t *dfrom, long ainfo);

sui_dinfo_t *dinfo_scale_proxy( sui_dinfo_t *dfrom, long ainfo, 
                                long amultiply, long adivide);
                                
int sui_simple_proxy_rdval(sui_dinfo_t *dinfo, long indx, void *buf);
int sui_simple_proxy_wrval(sui_dinfo_t *dinfo, long indx, const void *buf);

// change datainfo value
//long sui_data_change( sui_dinfo_t *datai, long newvalue, unsigned char flags);

#define sui_dinfo_scale_static_with_idxsize( name, dfrom, if, mi, ma, fd, mul, div, is) \
		sui_dinfo_scale_t name = { \
			.di = { \
				.refcnt  = SUI_STATIC, \
				.tinfo   =  SUI_TYPE_LONG, \
				.ptr     = (dfrom), \
				.hevent  = dinfo_simple_proxy_hevent, \
				.evmask  = SUEV_COMMAND | SUEV_FREE, \
				.rdval   = sui_scale_rdval, \
				.wrval   = sui_scale_wrval, \
				.info    = (if), \
				.idxsize = (is), \
				.minval  = (mi), \
				.maxval  = (ma), \
				.fdigits = (fd) \
			}, \
			.multiply = (mul), \
			.divide   = (div) \
		}

#define sui_dinfo_scale_static( name, dfrom, if, mi, ma, fd, mul, div) \
	sui_dinfo_scale_static_with_idxsize( name, dfrom, if, mi, ma, fd, mul, div, 1)

#define sui_dinfo_scale_static_ro_with_idxsize( name, dfrom, if, mi, ma, fd, mul, div, is) \
		sui_dinfo_scale_t name = { \
			.di = { \
				.refcnt  = SUI_STATIC, \
				.tinfo   =  SUI_TYPE_LONG, \
				.ptr     = (dfrom), \
				.hevent  = dinfo_simple_proxy_hevent, \
				.evmask  = SUEV_COMMAND | SUEV_FREE, \
				.rdval   = sui_scale_rdval, \
				.wrval   = NULL, \
				.info    = (if), \
				.idxsize = (is), \
				.minval  = (mi), \
				.maxval  = (ma), \
				.fdigits = (fd) \
			}, \
			.multiply = (mul), \
			.divide   = (div) \
		}

#define sui_dinfo_scale_static_ro( name, dfrom, if, mi, ma, fd, mul, div) \
	sui_dinfo_scale_static_ro_with_idxsize( name, dfrom, if, mi, ma, fd, mul, div, 1)

#define sui_dinfo_simple_proxy_static_with_idxsize( name, ti, dfrom, if, mi, ma, fd, is) \
		sui_dinfo_t name = { \
			.refcnt = SUI_STATIC, \
			.tinfo  = (ti), \
			.ptr    = (dfrom), \
			.hevent = dinfo_simple_proxy_hevent, \
			.evmask = SUEV_COMMAND | SUEV_FREE, \
			.rdval  = sui_simple_proxy_rdval, \
			.wrval  = sui_simple_proxy_wrval, \
			.info   = (if), \
			.idxsize = (is), \
			.minval = (mi), \
			.maxval = (ma), \
			.fdigits= (fd) \
		}

#define sui_dinfo_simple_proxy_static( name, ti, dfrom, if, mi, ma, fd) \
	sui_dinfo_simple_proxy_static_with_idxsize( name, ti, dfrom, if, mi, ma, fd, 1)

#define sui_dinfo_simple_proxy_ro_static_with_idxsize( name, ti, dfrom, if, mi, ma, fd, is) \
		sui_dinfo_t name = { \
			.refcnt = SUI_STATIC, \
			.tinfo  = (ti), \
			.ptr    = (dfrom), \
			.hevent = dinfo_simple_proxy_hevent, \
			.evmask = SUEV_COMMAND | SUEV_FREE, \
			.rdval  = sui_simple_proxy_rdval, \
			.wrval  = NULL, \
			.info   = (if), \
			.idxsize = (is), \
			.minval = (mi), \
			.maxval = (ma), \
			.fdigits= (fd) \
		}

#define sui_dinfo_simple_proxy_ro_static( name, ti, dfrom, if, mi, ma, fd) \
	sui_dinfo_simple_proxy_ro_static_with_idxsize( name, ti, dfrom, if, mi, ma, fd, 1) \

sui_dinfo_t *dinfo_dinfopar_proxy(sui_dinfo_t *dfrom, long parinfo);

enum {
  SUDI_INFOPAR_INFO      = 1,
  SUDI_INFOPAR_MINVAL    = 2,
  SUDI_INFOPAR_MAXVAL    = 3,
  SUDI_INFOPAR_FDIGITS   = 4,
  SUDI_INFOPAR_TINFO     = 5,
  SUDI_INFOPAR_IDXSIZE   = 6, /* read only */
};

int dinfo_dinfopar_proxy_rdval(sui_dinfo_t *dinfo, long indx, void *buf);
int dinfo_dinfopar_proxy_wrval(sui_dinfo_t *dinfo, long indx, const void *buf);

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /* _SUI_DINFO_H_ */
