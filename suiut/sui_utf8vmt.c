/* sui_utf8vmt.c
 *
 * SUITK VMT table for UTF8 strings.
 *
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#define _SUI_IN_UTF8VMT_C

#include "sui_comdefs.h"
#include <string.h>
#include <suiut/sui_objbase.h>
#include <suiut/sui_typemethods.h>
#include <suiut/sui_typebase.h>
#include <suiut/sui_utf8.h>

int sui_utf8_vmt_init(sui_utf8_t *self, void *params)
{
	/* FIXME: something simple and fast from sui_utf8.c should be called there,
	   it seems, that this results in UTF8_DYNAMIC_STRUCT, which makes problems
	   in  */

	self->type = UTF8_TYPE_STRUCT;
	self->refcnt = 1;
	
	return 0;
}

void sui_utf8_vmt_done(sui_utf8_t *self)
{
	int type;

	/* FIXME: these should not be decided there, but I have not found any
	   appropriate function in sui_utf8.c */

	type = sui_utf8_get_type( self);
	if (type == UTF8_STATIC_RAW)
		return;

	if ( type == UTF8_DYNAMIC_FULL) {
		if ( self->data.ptr)
			free( self->data.ptr);
		self->data.ptr = NULL;
	}
}

/******************************************************************************
 * Basic utf8 vmt
 ******************************************************************************/
#define sui_utf8_vmt_fields(new_type, parent_type) \
	SUI_INS_INHERITED_VMT(new_type, parent_type, sui_obj) \

typedef struct sui_utf8_vmt {
        sui_utf8_vmt_fields(sui_utf8, sui_obj)
} sui_utf8_vmt_t;


SUI_CANBE_CONST
sui_obj_tinfo_t sui_utf8_vmt_obj_tinfo = {
  .name = "utf8",
  .obj_size = sizeof(sui_utf8_t),
  .obj_align = UL_ALIGNOF(sui_utf8_t),
  .flags = SUI_OTINFO_NOVMT, /* SUI_OTINFO_REFCNT */
  .signal_count = 0,
  .signal_tinfo = NULL,
  .slot_count = 0,
  .slot_tinfo = NULL
};

sui_utf8_vmt_t sui_utf8_vmt_data = {
  .vmt_size = sizeof(sui_obj_vmt_t),
  .vmt_typeid = SUI_TYPE_UTF8,
  .vmt_refcnt = SUI_STATIC,
  .vmt_parent = NULL,
  .vmt_init = sui_utf8_vmt_init,
  .vmt_done = sui_utf8_vmt_done,
  .vmt_obj_tinfo = UL_CAST_UNQ1(sui_obj_tinfo_t *, &sui_utf8_vmt_obj_tinfo),

  /* default implementations from primary types, (void*) to suppress warnings */
  .vmt_new = (void*)sui_typeprimary_vmt_new,
  .vmt_deallocate = (void*)sui_typeprimary_vmt_deallocate,
  .vmt_find_signal_byid = (void*)sui_typeprimary_vmt_find_signal_byid,
  .vmt_find_slot_byid = (void*)sui_typeprimary_vmt_find_slot_byid,
  .vmt_find_signalid_byname = (void*)sui_typeprimary_vmt_find_signalid_byname,
  .vmt_find_slotid_byname = (void*)sui_typeprimary_vmt_find_slotid_byname,
  .vmt_signal_args_tinfo = (void*)sui_typeprimary_vmt_signal_args_tinfo,
  .vmt_slot_args_tinfo = (void*)sui_typeprimary_vmt_slot_args_tinfo,
  .vmt_common_type = NULL,

  .inc_refcnt = sui_utf8_inc_refcnt,
  .dec_refcnt = sui_utf8_dec_refcnt
};

sui_intptr_t *sui_utf8_global_instances_counter=(sui_intptr_t *)&sui_utf8_vmt_data.vmt_refcnt;
