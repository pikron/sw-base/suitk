/* sui_time.c
 *
 * SUITK time functions (conversions).
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

/* TODO: timezone and DST, better algorithms in sec2struct and struct2sec */

#include <stdio.h>
#include "sui_time.h"

long                 sui_time_tz_offset = 0; /* time zone offset to GMT in seconds */
sui_time_dst_rule_t *sui_time_dst_rule = NULL;

/* time constants */
const int sui_time_year_days[2] = { 365, 366}; /* number of days in normal and leap year */
const int sui_time_month_description[2][13] =
  {{0,31,59,90,120,151,181,212,243,273,304,334,365},  /* normal year - days before month */
   {0,31,60,91,121,152,182,213,244,274,305,335,366}}; /* leap year - days before month */
const int sui_time_mon2dow_table[12] = { 28, 31, 2, 5, 7, 10, 12, 15, 18, 20, 23, 25};


/* sum of leap seconds in history from POSIX epoch time (0:0:0 1.1.1970) */
//const sui_time_ls_table_t sui_time_leapsec[] = {
//  {78796800,1}, /* Leap    1972    Jun     30      23:59:60        +       S */
//  {94694401,2}, /* Leap    1972    Dec     31      23:59:60        +       S */
/* TODO: add time in epochsec when leapsec was added
Leap    1973    Dec     31      23:59:60        +       S
Leap    1974    Dec     31      23:59:60        +       S
Leap    1975    Dec     31      23:59:60        +       S
Leap    1976    Dec     31      23:59:60        +       S
Leap    1977    Dec     31      23:59:60        +       S
Leap    1978    Dec     31      23:59:60        +       S
Leap    1979    Dec     31      23:59:60        +       S
Leap    1981    Jun     30      23:59:60        +       S
Leap    1982    Jun     30      23:59:60        +       S
Leap    1983    Jun     30      23:59:60        +       S
Leap    1985    Jun     30      23:59:60        +       S
Leap    1987    Dec     31      23:59:60        +       S
Leap    1989    Dec     31      23:59:60        +       S
Leap    1990    Dec     31      23:59:60        +       S
Leap    1992    Jun     30      23:59:60        +       S
Leap    1993    Jun     30      23:59:60        +       S
Leap    1994    Jun     30      23:59:60        +       S
Leap    1995    Dec     31      23:59:60        +       S
Leap    1997    Jun     30      23:59:60        +       S
Leap    1998    Dec     31      23:59:60        +       S
Leap    2005    Dec     31      23:59:60        +       S
Leap    2008    Dec     31      23:59:60        +       S
*/
//  {0,0}, /* end of table */
//};

/******************************************************************************/
/* universal functions */
/**
 * sui_time_year_is_leap - test if year is a leap year
 */
inline
int sui_time_year_is_leap( int year)
{
  if (( year % 4) != 0) return 0;
  if (( year % 100) != 0) return 1;
  return (( year % 400) == 0);
}

/**
 * sui_time_convert_dmy2dow - Convert year, month and day to day_of_week
 * @tm: pointer to time lements structure
 * The function computes day-of-week from year, month and day-of-month.
 */
inline
void sui_time_convert_dmy2dow( sui_time_elements_t *tm)
{
  long y;
  if (tm->mon < 2) y = tm->year - 1; else y = tm->year;
//  tm->wday = (tm->mday +  (497*y)/400 + sui_time_mon2dow_table[tm->mon]) % 7; /* this code has mistakes x/400 is correct
  tm->wday = (tm->mday + y + y/4 - y/100 + y/400 + sui_time_mon2dow_table[tm->mon]) % 7;
}

/**
 * sui_time_convert_yd2dmw - Convert year and day-of-year to day, month and day-of-week
 * @tm: pointer to time elements structure
 * The function counts day, month, day-of-week and (week-of-year) from
 * year and day-of-year.
 */
inline
void sui_time_convert_yd2dmw( sui_time_elements_t *tm)
{
  int m = 1;
  int isleap = sui_time_year_is_leap( tm->year);
  while ( tm->yday >= sui_time_month_description[isleap][m])
    m++;
  m--;
  tm->mon = m;
  tm->mday = tm->yday - sui_time_month_description[isleap][m] + 1;
  sui_time_convert_dmy2dow( tm);
}

/**
 * sui_time_convert_dmy2doy - Convert year, month and day-of-month to day-of-year
 * @tm: pointer to time elements structure
 */
inline
void sui_time_convert_dmy2doy( sui_time_elements_t *tm)
{
  tm->yday = sui_time_month_description[sui_time_year_is_leap( tm->year)][tm->mon] + tm->mday - 1;
}

/******************************************************************************/
/* conversion functions for limited range of time from EPOCH (1970/1/1 at 00:00:00 UTC) to EPOCH+2^32seconds (2106/2/7 at 06:28:16h UTC) */
/**
 * sui_time_is_leapyear - return 1 if a year is a leap year
 * @year: input real year [1970-2106]
 */
inline
int sui_time_is_leapyear(int year)
{
  if (year==2100) return 0;
  year -= SUI_TIME_ZERO_YEAR;
  return ((year & 3)==2); /* 1972, 1976, 1980, ..., 2000, 2004, ... */
}

/**
 * sui_time_convert_esec2eday - Divide seconds from epoch to days from epoch and seconds in a day
 * @esec: input seconds from epoch
 * @eday: output days from epoch
 * @daysec: output seconds in a day
 */
inline
void sui_time_convert_esec2eday(unsigned long esec, long *eday, long *dsec)
{
  /* correction with leap seconds ? */
  if (eday) *eday = esec / SUI_TIME_SEC_PER_DAY; /* days from epoch */
  if (dsec) *dsec = esec % SUI_TIME_SEC_PER_DAY; /* seconds in a day */
}

#define SUI_TIME_YDAY_Y1    365  /* legth of year 1970, 1974, 1978, ... */
#define SUI_TIME_YDAY_Y2    730  /* legth of years 1970+1971, 1974+1975, ... */
#define SUI_TIME_YDAY_Y3   1096  /* legth of years 1970+1971+1972, 1974+1975+1976, ... */
#define SUI_TIME_YDAY_LCYC 1461  /* all 4 years in a leap cycle (1970+1971+1972+1973) */
/**
 * sui_time_convert_eday2eyr - Divide days from epoch to years from epoch and days in a year
 * @eday: input days from epoch
 * @year: output years from epoch
 * @yday: output day in a year
 * @leap: output year is a leap year
 */
inline
void sui_time_convert_eday2yyd(long eday, int *year, int *yday, int *leap)
{
  /* divide eday to 4years from epoch and days in 4year */
  long efy = eday / SUI_TIME_YDAY_LCYC; /* get how many fourth years is from epoch */
  long fyd = eday % SUI_TIME_YDAY_LCYC; /* days in fourth-year */
  int l = 0;
  /* correct skipped leap year (2100) */
  if (efy>32 || (efy==32 && fyd>=SUI_TIME_YDAY_Y3)) { /* 2100 has not a leap day */
    if (fyd==0) {
      efy--;
      fyd = SUI_TIME_YDAY_LCYC;
    } else
      fyd--;
  }
  /* get real year (from epoch) and dayofyear */
  efy = (efy<<2); /* get real year of beginning of the current fourth-year */
  if (fyd>SUI_TIME_YDAY_Y2) {
    if (fyd>SUI_TIME_YDAY_Y3) {
      efy += 3;
      fyd -= SUI_TIME_YDAY_Y3;
    } else {
      efy += 2;
      fyd -= SUI_TIME_YDAY_Y2;
      l = 1;
    }
  } else {
    if (fyd>SUI_TIME_YDAY_Y1) {
      efy++;
      fyd -= SUI_TIME_YDAY_Y1;
    }
  }
  efy += SUI_TIME_ZERO_YEAR;
  if (year) *year = efy;
  if (yday) *yday = fyd;
  if (leap) *leap = l;
}

/**
 * sui_time_convert_yday2md - convert day in year (and leap year flag) to month and day
 */
inline
void sui_time_convert_yday2md(int yday, int leap, int *mon, int *day)
{
  int m;
  int d;
  if (yday>sui_time_month_description[leap][6]) { /* 6,7,8,9,10,11 */
    if (yday>sui_time_month_description[leap][9]) { /* 9,10,11 */
      if (yday>sui_time_month_description[leap][11]) { /* 11 */
        m = 11;
        d = yday-sui_time_month_description[leap][11];
      } else if (yday<=sui_time_month_description[leap][10]) { /* 9 */
        m = 9;
        d = yday-sui_time_month_description[leap][9];
      } else { /* 10 */
        m = 10;
        d = yday-sui_time_month_description[leap][10];
      }
    } else { /* 6,7,8 */
      if (yday>sui_time_month_description[leap][8]) { /* 8 */
        m = 8;
        d = yday-sui_time_month_description[leap][8];
      } else if (yday<=sui_time_month_description[leap][7]) { /* 6 */
        m = 6;
        d = yday-sui_time_month_description[leap][6];
      } else { /* 7 */
        m = 7;
        d = yday-sui_time_month_description[leap][5];
      }
    }
  } else { /* 0,1,2,3,4,5 */
    if (yday>sui_time_month_description[leap][3]) { /* 3,4,5 */
      if (yday>sui_time_month_description[leap][5]) { /* 5 */
        m = 5;
        d = yday-sui_time_month_description[leap][5];
      } else if (yday<=sui_time_month_description[leap][4]) { /* 3 */
        m = 3;
        d = yday-sui_time_month_description[leap][3];
      } else { /* 4 */
        m = 4;
        d = yday-sui_time_month_description[leap][4];
      }
    } else { /* 0,1,2 */
      if (yday>sui_time_month_description[leap][2]) { /* 2 */
        m = 2;
        d = yday-sui_time_month_description[leap][2];
      } else if (yday<=sui_time_month_description[leap][1]) { /* 0 */
        m = 0;
        d = yday-sui_time_month_description[leap][0];
      } else { /* 1 */
        m = 1;
        d = yday-sui_time_month_description[leap][1];
      }
    }
  }
  if (mon) *mon=m;
  if (day) *day=d;
}

/**
 * sui_time_convert_yyd2dow - Convert year and day in year to day-of-week
 * @year: input real year [1970-2106]
 * @yday: input day in a year [0-366]
 * @dow: output day of week [0-6] (i.e. Sun,Mon,...Sat)
 */
inline
void sui_time_convert_yyd2dow(int year, int yday, int *dow)
{
  const int sui_time_year_dow_offsets[28] =
    {0,1,2,4, 5,6,0,2, 3,4,5,0, 1,2,3,5, 6,0,1,3, 4,5,6,1, 2,3,4,6};

  int d = (year<2101) ? 0 : -1; /* correction for non leap year 2100 */
  year -= SUI_TIME_ZERO_YEAR;
  d += SUI_TIME_ZERO_WDAY + sui_time_year_dow_offsets[year%28];
  d += yday;
  d %= 7;
  if (dow) *dow = d;
}

/**
 * sui_time_convert_dml2yday - convert leap year flag, month and day to day of year
 * @leap: input leap year flag
 * @mon: input month [0-11]
 * @day: input day [1-31]
 * @yday: output day of year [0-366]
 */
inline
void sui_time_convert_dml2yday(int leap, int mon, int day, int *yday)
{
  if (yday) *yday=sui_time_month_description[leap][mon]+day-1;
}


/******************************************************************************/
/**
 * sui_time_convert_sec2struct - Time conversion from linear time to time/date structure
 * @tsec: input linear time in seconds
 * @tm: pointer to output time structure buffer
 * The function converts linear time in seconds to time/date in separated fields.
 * Return: The function returns zero as success and negative values for errors.
 * File: sui_time.c
 */
int sui_time_convert_sec2struct( unsigned long epochsec, sui_time_elements_t *tm)
{
  int year, day;
  int base_leap;

  if (!tm) return -1;

  tm->sec = epochsec % 60;
  epochsec /= 60;
  tm->min = epochsec % 60;
  epochsec /= 60;
  tm->hour = epochsec % 24;
  epochsec /= 24;
  /* in epochsec is number of days from BASE */

/* FIXME: replace this simple algorithm (days from epoch -> year and day of year) to some better alg. */

  year = SUI_TIME_ZERO_YEAR;
  day = epochsec;
  do {
    base_leap = sui_time_year_days[ sui_time_year_is_leap( year)];
    if ( day < base_leap) break;
    day -= base_leap;
    year++;
  } while( 1);

  tm->year = year;
  tm->yday = day;

  sui_time_convert_yd2dmw( tm);

/* !!! add timezone and DST correction */

  return 0;
}


/**
 * sui_time_convert_struct2sec - Time conversion from time/date structure to linear time
 * @tm: pointer to input time structure buffer
 * @tsec: pointer to output linear time in seconds
 * The function converts time/date in separated fields to linear time in seconds.
 * Return: The function returns zero as success and negative values for errors.
 * File: sui_time.c
 */
int sui_time_convert_struct2sec( sui_time_elements_t *tm, unsigned long *pepochsec)
{
  unsigned long ret = 0;
  long leap;
  int hr;
  if ( !tm || !pepochsec) return -1;

/* tm must be corrected by timezone and DST */

/* FIXME: use better algorithm to compute number of leap years between $year and $epoch_year */

  /* number of leap years from epoch */
  hr = tm->year - 1;
  leap = ((hr/4)-(hr/100)+(hr/400)) - SUI_TIME_LEAP_YEARS_TO_EPOCH;
//ul_logdeb("Leap years=%d (lte=%d)\n",leap,SUI_TIME_LEAP_YEARS_TO_EPOCH);

  ret = ((tm->year-SUI_TIME_ZERO_YEAR) * SUI_TIME_SEC_PER_YEAR) + ((tm->yday + leap)*SUI_TIME_SEC_PER_DAY) +
          (tm->hour * SUI_TIME_SEC_PER_HOUR) + (tm->min * 60) + tm->sec;
  *pepochsec = ret;
  return 0;
}


/******************************************************************************/
/* convert epoch time to local time */

/* DST rules and cache */
typedef struct sui_time_dstr_cache_type {
  int iddstr; /* id of used DST rule */
  int year; /* cached year */
  int fyday; /* DST from yday */
  int fdsec; /* DST from second in a fyday */
  int tyday; /* DST to yday */
  int tdsec; /* DST to second in a tyday */
  long offset;
} sui_time_dstr_cache_type_t;

struct sui_time_dstr_cache_type sui_time_dstr_cache;


#define SUI_TIME_IDDSTR_EU  ('E'<<8 | 'U')
/**
 * sui_time_dstr_eu - DST rule for europe from 1996
 * @esec: pointer to time in seconds from epoch
 * @dsto: pointer to output buffer for DST offset (0=no offset)
 * @offs: if it is not NULL. it contains always DST offset used/unused for the year
 * Return Value: The function returns -1 if any error occurs; and 0 if success
 */
int sui_time_dstr_eu(unsigned long esec, long *dsto, long *offs)
{
  long eday, dsec;
  int year, yday, lyr; /* leap year */

  if (dsto==NULL) return -1;

  /* get year from epochsec */
  sui_time_convert_esec2eday(esec, &eday, &dsec);
  sui_time_convert_eday2yyd(eday, &year, &yday, &lyr);

  /* check cached values */
  if ((sui_time_dstr_cache.iddstr!=SUI_TIME_IDDSTR_EU) || (sui_time_dstr_cache.year!=year)) { /* compude cached values */
    sui_time_dstr_cache.fyday = sui_time_dstr_cache.tyday = 0; /* disable DST rule */
    if (year>=1996) {
      int dow, tmpyd;
      /* get last Sunday in March */
      sui_time_convert_dml2yday(lyr, 3, 1, &tmpyd); /* get day of week for 1st April */
      sui_time_convert_yyd2dow(year, tmpyd, &dow);
      if (dow) tmpyd -= dow; /* get last Sunday in March */
      else tmpyd -= 7;

      sui_time_dstr_cache.fyday = tmpyd;
      sui_time_dstr_cache.fdsec = 1*SUI_TIME_SEC_PER_HOUR; /* at 1:00 UTC */

      /* get last Sunday in October */
      sui_time_convert_dml2yday(lyr, 10, 1, &tmpyd); /* get day of week for 1st November */
      sui_time_convert_yyd2dow(year, tmpyd, &dow);
      if (dow) tmpyd -= dow; /* get last Sunday in October */
      else tmpyd -= 7;

      sui_time_dstr_cache.tyday = tmpyd;
      sui_time_dstr_cache.tdsec = 1*SUI_TIME_SEC_PER_HOUR; /* at 1:00 UTC */

      sui_time_dstr_cache.iddstr=SUI_TIME_IDDSTR_EU;
      sui_time_dstr_cache.year=year;
      sui_time_dstr_cache.offset=1*SUI_TIME_SEC_PER_HOUR; /* time offset is a hour */
    }
  }
  if (sui_time_dstr_cache.fyday<sui_time_dstr_cache.tyday) {
    if (yday>sui_time_dstr_cache.fyday && yday<sui_time_dstr_cache.tyday) {
      *dsto = sui_time_dstr_cache.offset; /* in DST */
    } else if (yday==sui_time_dstr_cache.fyday && dsec>=sui_time_dstr_cache.fdsec) {
      *dsto = sui_time_dstr_cache.offset; /* in DST */
    } else if (yday==sui_time_dstr_cache.tyday && dsec<sui_time_dstr_cache.tdsec) {
      *dsto = sui_time_dstr_cache.offset; /* in DST */
    } else {
      *dsto = 0; /* no DST */
    }
  } else
    *dsto = 0; /* no DST implemented */
  if (offs) *offs = sui_time_dstr_cache.offset;
  return 0;
}


/**
 * sui_time_convert_sec2tzdst - convert epochsec to time structure with timezone and DST
 */
int sui_time_convert_sec2tzdst(unsigned long epochsec, sui_time_elements_t *tm, long tzoffs, sui_time_dst_rule_t *dstr)
{
  long dstoffs = 0, offs;
  int ret = 0;

  /* compute offset according DST rule */
  if (dstr) { /* get time offset from DST rule for UTC time */
    if ((ret = dstr(epochsec, &dstoffs, NULL))!=0)
      return ret;
  }
  /* add DST offset and timezone offset to epochsec */
  offs=dstoffs+tzoffs;
  if (offs) {
    if (offs<0) {
      if (epochsec<(0-offs)) return -1; /* too deep in history */
    } else {
      if (epochsec>(0xffffffff-offs)) return -1; /* too far in the future */
    }
    epochsec += offs;
  }
  /* convert epochsec to the structure */
  if ((ret = sui_time_convert_sec2struct(epochsec, tm))!=0)
    return ret;
  /* set dst and utcoffs in the structure */
  tm->utco = offs;
  tm->isdst = (dstoffs!=0);
  return 0;
}

/**
 * sui_time_convert_tzdst2sec - convert time structure to epochsec with timezone and DST
 */
int sui_time_convert_tzdst2sec(sui_time_elements_t *tm, unsigned long *epochsec, long tzoffs, sui_time_dst_rule_t *dstr)
{
  long dsto = 0;
  unsigned long es = 0;
  int ret = 0;

  /* convert time structure to epochsec (in local time with DST) */
  if ((ret=sui_time_convert_struct2sec(tm,&es))!=0)
    return ret;
  /* subtract timezone offset from epochsec -> to GMT time */
  if (tzoffs) {
    if (tzoffs<0) {
      if (es>(0xffffffff+tzoffs)) return -1; /* too far in the future */
    } else {
      if (es<(0+tzoffs)) return -1; /* too deep in history */
    }
    es -= tzoffs;
  }
  /* compute DST offset according to DST rule */
  if (dstr) { /* get time offset from DST rule for GMT time */
    long offs;
    if ((ret = dstr(es, &dsto, &offs))!=0)
      return ret;
    if (dsto==0 && tm->isdst) /* for end of DST period there is overapped period - the input structure must contain information if it is in DST or it isn't */
      dsto = offs;
  }
  /* subtract DST offset from epochsec -> to UTC time */
  if (dsto) {
    if (dsto<0) {
      if (es>(0xffffffff+dsto)) return -1; /* too far in the future */
    } else {
      if (es<(0+dsto)) return -1; /* too deep in history */
    }
    es -= dsto;
  }
  *epochsec = es;
  return 0;
}

/**
 * sui_time_set_tz_dst - set currently used timezone offset and DST rule
 */
int sui_time_set_tz_dst_rule(long tzoffs, sui_time_dst_rule_t *dst_rule)
{
  sui_time_tz_offset = tzoffs; /* time zone offset to GMT in seconds */
  sui_time_dst_rule = dst_rule; /* NULL means no DST rule */
  return 0;
}

int sui_time_tz_dst2rule(sui_time_dst_rule_t **p_dst_rule, enum sui_time_known_dst_rules dst_idx)
{
  if(dst_idx>1)
    return -1;
  if(!dst_idx)
    *p_dst_rule = NULL;
  else
    *p_dst_rule = sui_time_dstr_eu;
  return 0;
}

int sui_time_set_tz_dst(long tzoffs, enum sui_time_known_dst_rules dst_idx)
{
  sui_time_dst_rule_t *dst_rule;
  if(sui_time_tz_dst2rule(&dst_rule, dst_idx) < 0)
    return -1;
  return sui_time_set_tz_dst_rule(tzoffs, dst_rule);
}
