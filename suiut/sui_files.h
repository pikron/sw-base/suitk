/* sui_files.h
 *
 * SUITK files support.
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#ifndef _SUITK_FILES_SUPPORT_
#define _SUITK_FILES_SUPPORT_

#include <suiut/sui_types.h>
#include <suiut/support.h>

#include <suitk/sui_gdi.h>

#include <endian.h>


#ifdef __cplusplus
extern "C" {
#endif

/******************************************************************************
 * SUITK common files support
 ******************************************************************************/
typedef unsigned char sui_file_type;

#define SUI_FILE_TGRP_MASK     0x0f
#define SUI_FILE_TGRP_UNKNOWN  0x00
#define SUI_FILE_TGRP_SOURCE   0x01  /* sxml, ... */
#define SUI_FILE_TGRP_TEXT     0x02  /* txt, ... */
#define SUI_FILE_TGRP_IMAGE    0x03  /* bmp, pcx, ... */

#define SUI_FILE_TID_MASK     = 0xf0
/* for unknown type of files */
#define SUI_FILE_TID_UNKNOWN   0x00 /* unknown type of file */
#define SUI_FILE_TID_FROMFILE  0x10 /* unknown type which must be get from file name */
/* source files */
#define SUI_FILE_TID_SXML      0x10 /* file is a SXML source */
/* text files */
#define SUI_FILE_TID_TXT       0x10 /* file is a text file */
/* image files */
#define SUI_FILE_TID_BMP       0x10 /* file is a BMP image */
#define SUI_FILE_TID_PCX       0x20 /* file is a PCX image */

/******************************************************************************/
/* Translation table name->file/memory_source */

/**
 * struct sui_file_src_desc - Description of source
 * @name: Name of a source
 * @addr: Associated address or filename of the source
 * @size: Size of source for memory block or SUI_FILE_SOURCE_FILE for file
 * @type: type of source (group and type - SUI_FILE_TGRP_ and SUI_FILE_TID_ flags)
 *
 * The structure describes one record in an array of sources. The last record of
 * the array must be with $name equal to NULL. If types of files are used and source is a file
 * the combination of type equal to (SUI_FILE_TGRP_UNKNOWN | SUI_FILE_TID_FROMFILE) can be used
 * for obtaining type from extension of file name.
 *
 * File: sui_files.h
 */
typedef struct sui_file_src_desc {
  char *name;
  const char *addr;
  const long size;
  sui_file_type type;
} sui_file_src_desc_t;

sui_file_src_desc_t *sui_file_srctable_register(sui_file_src_desc_t *newtable);
sui_file_src_desc_t *sui_file_srctable_unregister(void);
int sui_file_srctable_find(char *name, int namesize, char *combpath, int pathsize,
                           char **fname_memory, long *size, sui_file_type *type);

/******************************************************************************/
/**
 * struct sui_file_source - suitk file source structure
 * @source: Stream source handle (@file or @memory).
 * @type: type of source (group and type - SUI_FILE_TGRP_ and SUI_FILE_TID_ flags) if it is known.
 * @flags: Source flags, Source type in %SUI_FSRC_TYPEMASK field is one of %SUI_FSRC_FILE or %SUI_FSRC_MEMORY.
 *         File open/close, read state is specified by other %SUI_FSRC_xxx flags.
 * @position: Current position of read character in document.
 * @line: Current read line.
 * @prev: Pointer to previous source (include tag).
 *
 * File: sui_files.h
 */
typedef struct sui_file_source {
  union {
    struct {              /* from file */
      char *filename;
      FILE *file;
    } file;
    struct {              /* from memory */
      char *addr;
      long size;
    } memory;
  } source;
  sui_file_type type;           /* type of source */
  unsigned short  flags;        /* source flags - memory/file, open/close, read */
  long            position;     /* current position of read character in document */
  int             line;         /* current read line */
  struct sui_file_source *prev; /* pointer to previous source (include tag) */
} sui_file_source_t;

/**
 * enum sui_file_flags - Flags for Sui file source or other type stream.
 * @SUI_FSRC_FILE:      Source is file stream.
 * @SUI_FSRC_MEMORY:    Source is taken from memory.
 * @SUI_FSRC_TYPEMASK:  Source type mask.
 * @SUI_FSRC_CLOSED:    Closed state.
 * @SUI_FSRC_OPENED:    Open state.
 * @SUI_FSRC_READ:      File is currently read
 * @SUI_FSRC_WRITTEN:   File is currently written
 * @SUI_FSRC_STATEMASK: Processing/position state mask.
 * @SUI_FSRC_CHANGED:   Source has been changed.
 */
enum sui_file_flags {
  SUI_FSRC_FILE     = 0x0000,
  SUI_FSRC_MEMORY   = 0x0001,
  SUI_FSRC_TYPEMASK = 0x0001,

  SUI_FSRC_CLOSED   = 0x0000,
  SUI_FSRC_OPENED   = 0x0010,
  SUI_FSRC_READ   = 0x0020,
  SUI_FSRC_WRITTEN  = 0x0040,
  SUI_FSRC_STATEMASK= 0x00F0,

  SUI_FSRC_CHANGED  = 0x0100,
};

enum sui_file_mode {
  SUI_FM_READ  = 0x0000,
  SUI_FM_WRITE = 0x0001,
//  SUI_FM_BINARY
};

#define sui_file_isfile(src) ((src->flags & SUI_FSRC_TYPEMASK)==SUI_FSRC_FILE)
#define sui_file_ismemory(src) ((src->flags & SUI_FSRC_TYPEMASK)==SUI_FSRC_MEMORY)

#define SUI_FILE_SOURCE_FILE -1 /* open sxml document from file */

#define SUI_FILE_BUFFER_SIZE 16 /* initial size of data buffer */

sui_file_source_t *sui_file_open(char *fname_memory, long size, sui_file_source_t *prevsrc, int mode);
sui_file_source_t *sui_file_close(sui_file_source_t *dsrc);
int sui_file_assign_type(sui_file_source_t *psrc, sui_file_type type);

long sui_file_skip(sui_file_source_t *file, long cnt);
long sui_file_read(sui_file_source_t *file, long size, void *buf);
long sui_file_write(sui_file_source_t *file, long size, void *buf);

int sui_file_token_separator(char *separs, int ch);
long sui_file_skip_upto(sui_file_source_t *file, char *separs);

int sui_file_check_consistency(char *filename, int addext);
int sui_file_get_type_from_ext(char *fname);

/******************************************************************************
 * SUITK support for PCX images
 ******************************************************************************/
/**
 * struct pcx_header - PCX file header structure defined by PCX file
 * @identifier: PCX file identifier
 * @version: Version of PCX file
 * @encoding: Used encoding in PCX file (no compression, RLE)
 * @bpp: Bits per pixel.
 * @xmin: Image X start offset
 * @ymin: Image Y start offset
 * @xmax: Image X end offset
 * @ymax: Image Y end offset
 * @hres: Horizontal resolution
 * @vres: Vertical resolution
 * @egapal[48]: EGA pallete
 * @reserved1: reserved
 * @ncp: Number of color planes.
 * @bpl: Bytes per lines.
 * @ptype: Palette type
 * @hscrsize: Horizontal screen size
 * @vscrsize: Vertical screen size
 * @reserved2[54]: Reserved for future use.
 */
typedef struct pcx_header {
  uint8_t   identifier;
  uint8_t   version;
  uint8_t   encoding;
  uint8_t   bpp; /* bit per pixel */
  uint16_t  xmin;
  uint16_t  ymin;
  uint16_t  xmax;
  uint16_t  ymax;
  uint16_t  hres;
  uint16_t  vres;
  uint8_t   egapal[48];
  uint8_t   reserved1;
  uint8_t   ncp; /* number of color planes */
  uint16_t  bpl; /* bytes per lines */
  uint16_t  ptype; /* type of palette */
  uint16_t  hscrsize; /* horizontal screen size */
  uint16_t  vscrsize; /* vertical screen size */
  uint8_t   reserved2[54];
} pcx_header_t;

#define SUI_FILE_PCX_ID 0x0a

#define sui_file_pcx_header_identifier(header) (*((uint8_t *)header+0))
#define sui_file_pcx_header_version(header)    (*((uint8_t *)header+1))
#define sui_file_pcx_header_encoding(header)   (*((uint8_t *)header+2))
#define sui_file_pcx_header_bpp(header)        (*((uint8_t *)header+3))
#define sui_file_pcx_header_ncp(header)        (*((uint8_t *)header+65))
#define sui_file_pcx_header_palptr(header)     ((uint8_t *)header+16)
#if __BYTE_ORDER == __BIG_ENDIAN
  #include <byteswap.h>

  #define sui_file_pcx_header_xmin(header) bswap_16(*((uint16_t *)((uint8_t *)header+4)))
  #define sui_file_pcx_header_ymin(header) bswap_16(*((uint16_t *)((uint8_t *)header+6)))
  #define sui_file_pcx_header_xmax(header) bswap_16(*((uint16_t *)((uint8_t *)header+8)))
  #define sui_file_pcx_header_ymax(header) bswap_16(*((uint16_t *)((uint8_t *)header+10)))
  #define sui_file_pcx_header_hres(header) bswap_16(*((uint16_t *)((uint8_t *)header+12)))
  #define sui_file_pcx_header_vres(header) bswap_16(*((uint16_t *)((uint8_t *)header+14)))
  #define sui_file_pcx_header_bpl(header) bswap_16(*((uint16_t *)((uint8_t *)header+66)))
  #define sui_file_pcx_header_paltype(header) bswap_16(*((uint16_t *)((uint8_t *)header+68)))
  #define sui_file_pcx_header_hscrsize(header) bswap_16(*((uint16_t *)((uint8_t *)header+70)))
  #define sui_file_pcx_header_vscrsize(header) bswap_16(*((uint16_t *)((uint8_t *)header+72)))
#else
  #define sui_file_pcx_header_xmin(header) (*((uint16_t *)((uint8_t *)header+4)))
  #define sui_file_pcx_header_ymin(header) (*((uint16_t *)((uint8_t *)header+6)))
  #define sui_file_pcx_header_xmax(header) (*((uint16_t *)((uint8_t *)header+8)))
  #define sui_file_pcx_header_ymax(header) (*((uint16_t *)((uint8_t *)header+10)))
  #define sui_file_pcx_header_hres(header) (*((uint16_t *)((uint8_t *)header+12)))
  #define sui_file_pcx_header_vres(header) (*((uint16_t *)((uint8_t *)header+14)))
  #define sui_file_pcx_header_bpl(header) (*((uint16_t *)((uint8_t *)header+66)))
  #define sui_file_pcx_header_paltype(header) (*((uint16_t *)((uint8_t *)header+68)))
  #define sui_file_pcx_header_hscrsize(header) (*((uint16_t *)((uint8_t *)header+70)))
  #define sui_file_pcx_header_vscrsize(header) (*((uint16_t *)((uint8_t *)header+72)))
#endif


#define SUI_FILE_PCX_PALETTE_ID   0x0c
typedef struct pcx_palette_rec {
  uint8_t red;
  uint8_t green;
  uint8_t blue;
} pcx_palette_rec_t;

#define sui_file_pcx_palette_red(rgbtriplet)   (*((uint8_t *)(rgbtriplet)+0))
#define sui_file_pcx_palette_green(rgbtriplet) (*((uint8_t *)(rgbtriplet)+1))
#define sui_file_pcx_palette_blue(rgbtriplet)  (*((uint8_t *)(rgbtriplet)+2))



/* Load PCX file into image structure */
int sui_file_pcx_load_image(sui_file_source_t *fsrc, sui_image_t *image, int loadpalette);
/* Save PCX file from image structure */
int sui_file_pcx_save_image(sui_file_source_t *fdst, sui_image_t *image);


/******************************************************************************
 * SUITK support for BMP images
 ******************************************************************************/
/**
 * struct bmp_header - BMP file header
 */
typedef struct __attribute__ ((packed)) bmp_header {
  uint16_t filetype;
  uint32_t filesize;
  uint16_t reserved1;
  uint16_t reserved2;
  uint32_t imageoffset;
} bmp_header_t;

typedef struct __attribute__ ((packed)) bmp_info_head {
  uint32_t size;
  uint32_t width;
  uint32_t height;
  uint16_t nip; // num image planes (always 1)
  uint16_t bpp; // bit per pixel
  uint32_t compression; // type of compression
  uint32_t bitmapsize;
  uint32_t horres;
  uint32_t verres;
  uint32_t usedcolors; // number of used colors in image
  uint32_t signcolors; // number of significant colors
} bmp_info_head_t;

typedef struct __attribute__ ((packed)) bmp_rgbquad {
  uint8_t blue;
  uint8_t green;
  uint8_t red;
  uint8_t reserved;
} bmp_rgbquad_t;

/* Load BMP file into image structure */
int sui_file_load_bmp_image( sui_file_source_t *fsrc, sui_image_t *image, int loadpalette);
/* Save BMP file from image structure */
int sui_file_save_bmp_image( sui_file_source_t *fdst, sui_image_t *image);
/* Save BMP from screen */
int sui_file_save_bmp_screen( char *fname, sui_dc_t *dc, int wmax, int hmax);

/******************************************************************************
 * SUITK support for ML files (markup language files)
 ******************************************************************************/

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /* _SUITK_FILES_SUPPORT_ */
