/* sui_nls.h
 *
 * Header file for SUITK NLS support.
 * 
 * Version 0.1
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#ifndef _SUI_NLS_H_
  #define _SUI_NLS_H_

#ifdef __cplusplus
extern "C" {
#endif

  #include <suiut/mymalloc.h>

  #if 0
    #define NLS_STR_SEC __attribute((section(".nls_str")))
  #else
    #define NLS_STR_SEC
  #endif
  
  char *nlstext(const char **s);

#ifdef WIN32
  /* Why we have used this */
  //#include <device.h>

  #define _(str) (utf8 *)str
#else
  #define _(str) \
    ((utf8 *)({ static const char *s NLS_STR_SEC=str; \
       nlstext(&s); \
    }))
#endif

  #define SUI_NLS_USE_NLS  1
  #define SUI_NLS_POINT ((utf8 *)".")
  

    
  
/**
 * struct nls_flag_subtable - Table of character flags with bounded range
 * @first: Index of first character in the subtable
 * @last: Index of last character in the subtable
 * @table: The subtable with character flags. (See 'nls_char_flags' enum with prefix 'SNCF_')
 * 
 * This structure is needed for functions: 
 * sui_utf8_isalnum, sui_utf8_isalpha, sui_utf8_isdigit, sui_utf8_islower, sui_utf8_ispunct, 
 * sui_utf8_isspace, sui_utf8_isupper, sui_utf8_tolower, sui_utf8_toupper.
 * File: sui_nls.h
 */ 
  typedef struct nls_flag_subtable {
    unsigned short first, last;
    unsigned char table[];
  } nls_flag_subtable_t;

/**
 * struct nls_shift_subtable - Table of upper<->lower shifts with bounded range
 * @first: Index of first character in the subtable
 * @last: Index of last character in the subtable
 * @table: The transformation subtable with shifts between lower and upper characters.
 * For smaller table it support only shifts in range <-128,127>.
 * 
 * This structure is needed for functions: sui_utf8_tolower, sui_utf8_toupper.
 * File: sui_nls.h
 */ 
  typedef struct nls_shift_subtable {
    unsigned short first, last;
    char table[];
  } nls_shift_subtable_t;

/**
 * enum nls_char_flags - Flags for nls_subtable structure
 * @SNCF_OTHER: Character isn't any of followed.
 * @SNCF_SPACE: Character is white character.
 * @SNCF_PUNCT: Character is punctuation character.
 * @SNCF_DIGIT: Character is digit.
 * @SNCF_LOWER: Character is lower alphabetic character.
 * @SNCF_UPPER: Character is upper alphabethic character.
 * @SNCF_ALPHA: Character is alphabetic character.
 */
  enum nls_char_flags {
    SNCF_OTHER = 0x00,
    SNCF_SPACE = 0x01,
    SNCF_PUNCT = 0x02,
    SNCF_DIGIT = 0x04,
    SNCF_LOWER = 0x08,
    SNCF_UPPER = 0x10,
    
    SNCF_ALPHA = 0x18,
  };

/* global pointers to nls depended character flags table and character shifts table */
  extern const nls_flag_subtable_t *nls_flag_table;
  extern const nls_shift_subtable_t *nls_shift_table;

/*  extern utf8 **nls_table; */
  extern char **nls_table; 

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /* _SUI_NLS_H_ */
