/* ns_wtypes.c
 *
 * SUITK namespace system and its support
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#include <suiut/namespace.h>

/******************************************************************************
 * array of widget types for searching in namespace
 ******************************************************************************/

const sui_typeid_t ns_widget_types[] = {
  SUI_TYPE_WGROUP,
  SUI_TYPE_WBUTTON,
  SUI_TYPE_WBITMAP,
  SUI_TYPE_WLISTBOX,
  SUI_TYPE_WNUMBER,
  SUI_TYPE_WSCROLL,
  SUI_TYPE_WTEXT,
  SUI_TYPE_WTIME,
  SUI_TYPE_WTABLE,
  SUI_TYPE_WDYNTEXT,
  SUI_TYPE_WGRAPHXY,
  0
};
