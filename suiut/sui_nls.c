/* sui_nls.c
 *
 * SUITK NLS support.
 * 
 * Version 0.1
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#include "sui_nls.h"

#include "sui_utf8.h"

  char *nlstext(const char **s) {
    return (char *)*s;
  }

  
  char *nls_czech_nodia[];
  
  char **nls_table = nls_czech_nodia;    
  char *nls_czech_nodia[] = {
    "ano","ne","ANO","NE"    
  };




  const nls_flag_subtable_t nls_czech_flag_subtable = {
#ifdef WIN32
    0xc1, 0x17e, {
#else
    first:0xc1,
    last:0x17e,
    table:{ 
#endif
            SNCF_UPPER,SNCF_UPPER,SNCF_UPPER,SNCF_UPPER,SNCF_UPPER,SNCF_UPPER,SNCF_UPPER,SNCF_UPPER, /*0xc1-0xc8*/
            SNCF_UPPER,SNCF_UPPER,SNCF_UPPER,SNCF_UPPER,SNCF_UPPER,SNCF_UPPER,SNCF_UPPER,SNCF_UPPER, /*0xc9-0xd0*/
            SNCF_UPPER,SNCF_UPPER,SNCF_UPPER,SNCF_UPPER,SNCF_UPPER,SNCF_UPPER,SNCF_OTHER,SNCF_UPPER, /*0xd1-0xd8*/
            SNCF_UPPER,SNCF_UPPER,SNCF_UPPER,SNCF_UPPER,SNCF_UPPER,SNCF_UPPER,SNCF_LOWER,SNCF_LOWER, /*0xd9-0xe0*/
            SNCF_LOWER,SNCF_LOWER,SNCF_LOWER,SNCF_LOWER,SNCF_LOWER,SNCF_LOWER,SNCF_LOWER,SNCF_LOWER, /*0xe1-0xe8*/
            SNCF_LOWER,SNCF_LOWER,SNCF_LOWER,SNCF_LOWER,SNCF_LOWER,SNCF_LOWER,SNCF_LOWER,SNCF_LOWER, /*0xe9-0xf0*/
            SNCF_LOWER,SNCF_LOWER,SNCF_LOWER,SNCF_LOWER,SNCF_LOWER,SNCF_LOWER,SNCF_OTHER,SNCF_LOWER, /*0xf1-0xf8*/
            SNCF_LOWER,SNCF_LOWER,SNCF_LOWER,SNCF_LOWER,SNCF_LOWER,SNCF_LOWER,SNCF_LOWER,SNCF_UPPER, /*0xf9-0x100*/
            SNCF_LOWER,SNCF_UPPER,SNCF_LOWER,SNCF_UPPER,SNCF_LOWER,SNCF_UPPER,SNCF_LOWER,SNCF_UPPER, /*0x101-0x108*/
            SNCF_LOWER,SNCF_UPPER,SNCF_LOWER,SNCF_UPPER,SNCF_LOWER,SNCF_UPPER,SNCF_LOWER,SNCF_UPPER, /*0x109-0x110*/
            SNCF_LOWER,SNCF_UPPER,SNCF_LOWER,SNCF_UPPER,SNCF_LOWER,SNCF_UPPER,SNCF_LOWER,SNCF_UPPER, /*0x111-0x118*/
            SNCF_LOWER,SNCF_UPPER,SNCF_LOWER,SNCF_UPPER,SNCF_LOWER,SNCF_UPPER,SNCF_LOWER,SNCF_UPPER, /*0x119-0x120*/
            SNCF_LOWER,SNCF_UPPER,SNCF_LOWER,SNCF_UPPER,SNCF_LOWER,SNCF_UPPER,SNCF_LOWER,SNCF_UPPER, /*0x121-0x128*/
            SNCF_LOWER,SNCF_UPPER,SNCF_LOWER,SNCF_UPPER,SNCF_LOWER,SNCF_UPPER,SNCF_LOWER,SNCF_UPPER, /*0x129-0x130*/
            SNCF_LOWER,SNCF_UPPER,SNCF_LOWER,SNCF_UPPER,SNCF_LOWER,SNCF_UPPER,SNCF_LOWER,SNCF_LOWER, /*0x131-0x138*/
            SNCF_UPPER,SNCF_LOWER,SNCF_UPPER,SNCF_LOWER,SNCF_UPPER,SNCF_LOWER,SNCF_UPPER,SNCF_LOWER, /*0x139-0x140*/
            SNCF_UPPER,SNCF_LOWER,SNCF_UPPER,SNCF_LOWER,SNCF_UPPER,SNCF_LOWER,SNCF_UPPER,SNCF_LOWER, /*0x141-0x148*/
            SNCF_LOWER,SNCF_UPPER,SNCF_LOWER,SNCF_UPPER,SNCF_LOWER,SNCF_UPPER,SNCF_LOWER,SNCF_UPPER, /*0x149-0x150*/
            SNCF_LOWER,SNCF_UPPER,SNCF_LOWER,SNCF_UPPER,SNCF_LOWER,SNCF_UPPER,SNCF_LOWER,SNCF_UPPER, /*0x151-0x158*/
            SNCF_LOWER,SNCF_UPPER,SNCF_LOWER,SNCF_UPPER,SNCF_LOWER,SNCF_UPPER,SNCF_LOWER,SNCF_UPPER, /*0x159-0x160*/
            SNCF_LOWER,SNCF_UPPER,SNCF_LOWER,SNCF_UPPER,SNCF_LOWER,SNCF_UPPER,SNCF_LOWER,SNCF_UPPER, /*0x161-0x168*/
            SNCF_LOWER,SNCF_UPPER,SNCF_LOWER,SNCF_UPPER,SNCF_LOWER,SNCF_UPPER,SNCF_LOWER,SNCF_UPPER, /*0x169-0x170*/
            SNCF_LOWER,SNCF_UPPER,SNCF_LOWER,SNCF_UPPER,SNCF_LOWER,SNCF_UPPER,SNCF_LOWER,SNCF_UPPER, /*0x171-0x178*/
            SNCF_UPPER,SNCF_LOWER,SNCF_UPPER,SNCF_LOWER,SNCF_UPPER,SNCF_LOWER /*0x179-0x17e*/           
          },
  };
  const nls_shift_subtable_t nls_czech_shift_subtable = {
#ifdef WIN32
    0xc1,0x17e, {
#else
    first:0xc1,
    last:0x17e,
    table:{ 
#endif
            32,0,0,0,0,0,0,0,/*0xc1-0xc8*/
            32,0,0,0,32,0,0,0,/*0xc9-0xd0*/
            0,0,32,0,0,0,0,0,/*0xd1-0xd8*/
            0,32,0,0,32,0,0,0,/*0xd9-0xe0*/            
            -32,0,0,0,0,0,0,0,/*0xe1-0xe8*/
            -32,0,0,0,-32,0,0,0,/*0xe9-0xf0*/
            0,0,-32,0,0,0,0,0,/*0xf1-0xf8*/
            0,-32,0,0,-32,0,0,0,/*0xf9-0x100*/
            0,0,0,0,0,0,0,0,/*0x101-0x108*/
            0,0,0,1,-1,1,-1,0,/*0x109-0x110*/
            0,0,0,0,0,0,0,0,/*0x111-0x118*/
            0,1,-1,0,0,0,0,0,/*0x119-0x120*/
            0,0,0,0,0,0,0,0,/*0x121-0x128*/
            0,0,0,0,0,0,0,0,/*0x129-0x130*/
            0,0,0,0,0,0,0,0,/*0x131-0x138*/
            0,0,0,0,0,0,0,0,/*0x139-0x140*/
            0,0,0,0,0,0,1,-1,/*0x141-0x148*/
            0,0,0,0,0,0,0,0,/*0x149-0x150*/
            0,0,0,0,0,0,0,1,/*0x151-0x158*/
            -1,0,0,0,0,0,0,1,/*0x159-0x160*/
            -1,0,0,1,-1,0,0,0,/*0x161-0x168*/
            0,0,0,0,0,1,-1,0,/*0x169-0x170*/
            0,0,0,0,0,0,0,0,/*0x171-0x178*/
            0,0,0,0,1,-1/*0x179-0x17e*/
          }
  };

  const nls_flag_subtable_t *nls_flag_table = &nls_czech_flag_subtable;
  const nls_shift_subtable_t *nls_shift_table = &nls_czech_shift_subtable;
