/* sui_objbase.c
 *
 * SUITK object/types register.
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#include <string.h>
#include "ul_utmalloc.h"
#include "ul_list.h"
#include "ul_evcbase.h"
#include "ul_gavl.h"
#include "ul_gavlcust.h"
#include "ul_gsa.h"
#include "ul_gsacust.h"
#include "sui_comdefs.h"
#include "sui_objbase.h"
#include "sui_typereg.h"
#include "sui_argsbase.h"

typedef struct sui_typeregister {
  sui_obj_vmt_t **vmt_array;
  unsigned vmt_array_size;
  gsa_array_field_t names_array;
  unsigned int const_array:1;
  unsigned int static_array:1;
  unsigned int without_names:1;
} sui_typeregister_t;

sui_typeregister_t *sui_typeregister_global;

/* FIXME: this should be somehow preset to reasonable value */
static sui_typeid_t last_unigue_typeid = 0;

typedef char *sui_typereg_name_key_t;

static sui_typeregister_t sui_typeregister_global_data;

static int sui_typereg_name_delete(sui_typeregister_t *array, const  sui_obj_vmt_t *item) UL_ATTR_UNUSED;

GSA_CUST_DEC_SCOPE(static,sui_typereg_name, sui_typeregister_t, sui_obj_vmt_t, sui_typereg_name_key_t,
	names_array, vmt_obj_tinfo->name, sui_typereg_name_cmp_fnc)

static inline int
sui_typereg_name_cmp_fnc(const sui_typereg_name_key_t *a, const sui_typereg_name_key_t *b)
{
  return strcmp(*a,*b);
}

GSA_CUST_IMP(sui_typereg_name, sui_typeregister_t, sui_obj_vmt_t, sui_typereg_name_key_t,
	names_array, vmt_obj_tinfo->name, sui_typereg_name_cmp_fnc, 0)

/**
 * sui_obj_get_unique_typeid - Acquire unique yet unused type identifier.
 *
 * Return Value: New type identifier.
 * File: sui_typereg.c
 */
sui_typeid_t sui_obj_get_unique_typeid(void)
{
  return ++last_unigue_typeid;
}


#define SUI_TYPEREG_CAPACITY_STEP 64

static int
sui_typereg_set_capacity(struct sui_typeregister *reg, unsigned for_typeid)
{
  sui_obj_vmt_t **new_array;
  unsigned new_size;

  if(for_typeid>=reg->vmt_array_size)
    new_size=for_typeid+1;
  else if(reg->const_array)
    new_size=reg->vmt_array_size;
  else
    return 0;

  new_size=(new_size+SUI_TYPEREG_CAPACITY_STEP) & ~(SUI_TYPEREG_CAPACITY_STEP-1);

  if(!reg->vmt_array || reg->const_array || reg->static_array){
    new_array=malloc(sizeof(void*)*new_size);
    if((reg->const_array || reg->static_array) && new_array){
      reg->const_array=0;
      memcpy(new_array,reg->vmt_array,reg->vmt_array_size*sizeof(void*));
    }
  } else {
    new_array=realloc(reg->vmt_array,sizeof(void*)*new_size);
  }
  if(!new_array)
    return -1;

  memset(&new_array[reg->vmt_array_size],0,(new_size-reg->vmt_array_size)*sizeof(void*));

  reg->vmt_array=new_array;
  reg->vmt_array_size=new_size;

  return 0;
}


/**
 * sui_obj_register_typeid - Register type VMT table into type identifiers register.
 * @reg:	Type identifiers register where type should be registered.
 * @typeid:	Type identifier acquired from sui_obj_get_unique_typeid() function.
 * @vmt:	Pointer to fully populated type VMT table.
 *
 * Return Value: Non-zero value informs about type registration problem.
 * File: sui_typereg.c
 */
int sui_obj_register_typeid(struct sui_typeregister *reg, sui_typeid_t typeid, sui_obj_vmt_t *vmt)
{
  char allready_registered=0;

  if(!reg) {
    if(!sui_typeregister_global){
      sui_typeregister_global = &sui_typeregister_global_data;
    }
    reg = sui_typeregister_global;
  }

  if(typeid<reg->vmt_array_size){
    if(reg->vmt_array[typeid]==vmt)
      allready_registered=1;
    else
      if(reg->vmt_array[typeid]!=NULL)
        return -1;
  }

  if(!allready_registered) {
    if(sui_typereg_set_capacity(reg, typeid)<0){
      return -1;
    }
    reg->vmt_array[typeid]=vmt;
  }

  if(typeid>last_unigue_typeid)
    last_unigue_typeid=typeid;

  if(vmt && vmt->vmt_obj_tinfo &&
     vmt->vmt_obj_tinfo->name && !reg->without_names) {
    if(vmt!=sui_typereg_name_find(reg, &vmt->vmt_obj_tinfo->name)) {
      if(sui_typereg_name_insert(reg, vmt)<0) {
        if(!allready_registered)
          reg->vmt_array[typeid]=NULL;
        return -1;
      }
    }
  }

  return 0;
}

/**
 * sui_typereg_init - Initialize VMT type register (Optional for global)
 * @reg:	Type identifiers register where type is searched for.
 * @vmt_array_data:	Initial VMT array indexed by typeid.
 * @data_size:	Size of the array.
 * @mode:	Mode for data and array.
 *
 * Return Value: Negative value signals fail of initialization.
 * File: sui_typereg.c
 */
int *sui_typereg_init(struct sui_typeregister *reg,
     sui_obj_vmt_t * const *vmt_array_data, unsigned data_size, int mode)
{
  if(!reg) {
    if(!sui_typeregister_global){
      sui_typeregister_global = &sui_typeregister_global_data;
    }
    reg = sui_typeregister_global;
  }

  sui_typereg_name_init_array_field(reg);

  reg->const_array=mode&SUI_TYPEREG_MODE_CONST?1:0;
  reg->without_names=mode&SUI_TYPEREG_MODE_WITHOUT_NAMES?1:0;
  reg->vmt_array=NULL;
  reg->vmt_array_size=0;
  if(vmt_array_data) {
    reg->vmt_array=(sui_obj_vmt_t**)vmt_array_data;
    reg->vmt_array_size=data_size;
    reg->static_array=1;
  }
  return 0;
}


/**
 * sui_typereg_get_vmt_by_typeid - Return type VMT according to type identifier.
 * @reg:	Type identifiers register where type is searched for.
 * @typeid:	Registered type identifier.
 *
 * Return Value: If @typeid is found, type VMT is returned else %NULL pointer is returned.
 * File: sui_typereg.c
 */
sui_obj_vmt_t *sui_typereg_get_vmt_by_typeid(struct sui_typeregister *reg, sui_typeid_t typeid)
{
  if(!reg) {
    reg = sui_typeregister_global;
    if(!reg)
      return NULL;
  }

  if(typeid<reg->vmt_array_size){
    return reg->vmt_array[typeid];
  }

  return NULL;
}

/**
 * sui_typereg_get_vmt_by_typeid - Return type VMT according to type name.
 * @reg:	Type identifiers register where type is searched for.
 * @name:	Name of registered type.
 *
 * Return Value: If @name is found, type VMT is returned else %NULL pointer is returned.
 * File: sui_typereg.c
 */
sui_obj_vmt_t *sui_typereg_get_vmt_by_name(struct sui_typeregister *reg, char *name)
{
  if(!reg) {
    reg = sui_typeregister_global;
    if(!reg)
      return NULL;
  }

  return sui_typereg_name_find(reg, &name);
}

/**
 * sui_typereg_typeid_next - Return subsequent registered type identifier.
 * @reg:	Type identifiers register.
 * @typeid:	Last previously obtained typeid.
 *
 * Return Value: Returns next found typeid if there is no more types.
 *		If there is no more valid typeids, value indicating invalid
 *		typeid is returned. Returned value can be checked for validity
 *		through sui_typereg_typeid_is_invalid() function.
 * File: sui_typereg.c
 */
sui_typeid_t sui_typereg_typeid_next(struct sui_typeregister *reg, sui_typeid_t typeid)
{
  if(!reg) {
    reg = sui_typeregister_global;
  }

  if(reg && reg->vmt_array){
    while(++typeid<reg->vmt_array_size){
      if(reg->vmt_array[typeid]!=NULL)
        return typeid;
    }
  }

  return (sui_typeid_t)-1;
}

/**
 * sui_typereg_typeid_first - Return first type identifier in register.
 * @reg:	Type identifiers register.
 *
 * Return Value: Returns found typeid if there is at least one type registered.
 *		If there is no valid typeid, value indicating invalid
 *		typeid is returned. Returned value can be checked for validity
 *		through sui_typereg_typeid_is_invalid() function.
 * File: sui_typereg.c
 */
sui_typeid_t sui_typereg_typeid_first(struct sui_typeregister *reg)
{
  return sui_typereg_typeid_next(reg, (sui_typeid_t)-1);
}

/**
 * sui_typereg_typeid_is_invalid - Check if typeid is of valid value for typeids.
 * @typeid:	Type identifiers register.
 *
 * Return Value: Returns non-zero value if @typeid value is in range valid for typeids.
 * File: sui_typereg.c
 */
int sui_typereg_typeid_is_invalid(sui_typeid_t typeid)
{
  return typeid==(sui_typeid_t)-1;
}
