/* mymalloc.h
 *
 * Header file for debug tool to check 
 * memory management easier then ccmalloc.
 * 
 * Roman Bartosinski, bartosr@centrum.cz
 * Version 0.1  (C)2002
 * Version 1 (C)2003 - add replacement of strdup fnc
 * Version 1.1 (C)2003 - global counters replaced into structure
 *                     - add higher level
 * Version 1.2 (C)2003 - add allocated memory maximum counter
 */

#ifndef _MY_OWN_BRUTAL_CCMALLOC
  #define _MY_OWN_BRUTAL_CCMALLOC

#ifdef __cplusplus
extern "C" {
#endif

  #include <malloc.h>

  /* This works only on PC. */

  #if 1 // MWPIXEL_FORMAT == 2		// MWPF_PALETTE
    #define MOW_DEBUG_LEVEL 0
  #else
    #define MOW_DEBUG_LEVEL 1    // DEBUG_LEVEL = 0-none,1-standard,2-higher
  #endif

    /**
     * struct _MOW_memory_info - internal info structure
     * @cnt: counter
     * @size: size of bytes
     */
    typedef struct MOW_memory_info {
      int cnt;
      int size;
      int dump; // zero allocation, null free
      int err;  // bad pointer, no allocation, no redefinition 'strdup','malloc'
    } MOW_memory_info_t;
    /**
     * struct _MOW_all_counters - Memory management counters
     *
     */
    typedef struct MOW_all_counters {
      struct MOW_memory_info malloc;
      struct MOW_memory_info free;
    } MOW_all_counters_t;


  #if MOW_DEBUG_LEVEL > 1
    /**
     * struct MOW_save_profile - Structure for memory profile
     *
     */
    typedef struct MOW_save_profile {
      int sumcnt;
      int sumsize;
      // struct MOW_all_counters profile;
      struct MOW_save_profile *next;
    } MOW_save_profile_t;

  #endif

    /**
     * struct _MOW_all_array - Internal Allocation structure
     * @ptr: Pointer to allocated space.
     * @size: Size of allocated space.
     * @status: Counter of deallocating of this space.
     */  
    typedef struct MOW_all_array {
      void *ptr;
      int size;
      int status;
      struct MOW_all_array *next;
    } MOW_all_array_t;


  #if MOW_DEBUG_LEVEL > 0
//#error "MemYES"
//    extern struct MOW_all_counters MyOwnCounter;
//    extern int MOW_cnt_alloc, MOW_cnt_dealloc, MOW_mem_size, MOW_mem_deall;
    void *MyOwnMalloc(size_t size);
    void MyOwnFree( void *s);
    void EndMemoryStatus( int withlist);
    char *MyOwnStrdup(const char *string);
  
    #define malloc(x) MyOwnMalloc(x)
    #define free(x)   MyOwnFree(x)
    #define strdup(x) MyOwnStrdup(x)
  #else
//#error "MemNO"
    #define EndMemoryStatus( x)
  
  #endif

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /*_MY_OWN_BRUTAL_MALLOC*/
