/* sui_typebase.c
 *
 * SUITK object and method inheritance base.
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#include <string.h>
#include "sui_objbase.h"
#include "sui_typereg.h"
#include "sui_typebase.h"
#include "sui_typemethods.h"
#include "ul_utmalloc.h"

/******************************************************************************/
/* Member methods for base types without vmt pointer */

sui_obj_t *sui_typeprimary_vmt_new(struct sui_obj_vmt *vmt)
{
  sui_obj_t *self;
  if(sui_obj_check_vmt_ok(vmt)<0)
    return NULL;
  
  self=malloc(vmt->vmt_obj_tinfo->obj_size);
  if(!self) return NULL;
  if(sui_obj_init_vmt(self, vmt)<0){
    free(self);
    return NULL;
  }
  return self;
}

void sui_typeprimary_vmt_deallocate(sui_obj_t *self)
{
  free(self);
}

void sui_typeprimary_vmt_done(sui_obj_t *self)
{
}

sui_obj_signal_tinfo_t * sui_typeprimary_vmt_find_signal_tinfo_byid(sui_obj_t *self, sui_signalid_t signalid)
{
  return NULL;
}

struct sui_obj_signal * sui_typeprimary_vmt_find_signal_byid(sui_obj_t *self, sui_signalid_t signalid)
{
  return NULL;
}

sui_obj_slot_tinfo_t * sui_typeprimary_vmt_find_slot_tinfo_byid(sui_obj_t *self, sui_slotid_t slotid)
{
  return NULL;
}

struct sui_obj_slot * sui_typeprimary_vmt_find_slot_byid(sui_obj_t *self, sui_slotid_t slotid)
{
  return NULL;
}

sui_signalid_t sui_typeprimary_vmt_find_signalid_byname(sui_obj_t *self, char *name, int name_len)
{
  return -1;
}

sui_slotid_t sui_typeprimary_vmt_find_slotid_byname(sui_obj_t *self, char *name, int name_len)
{
  return -1;
}

sui_args_tinfo_t *sui_typeprimary_vmt_signal_args_tinfo(sui_obj_t *self, sui_signalid_t sigid)
{
  return NULL;
}

sui_args_tinfo_t *sui_typeprimary_vmt_slot_args_tinfo(sui_obj_t *self, sui_slotid_t slotid)
{
  return NULL;
}

sui_refcnt_t sui_typeprimary_inc_refcnt(sui_obj_t *self)
{
  return SUI_REFCNTIMPOSIBLE;
}

sui_refcnt_t sui_typeprimary_dec_refcnt(sui_obj_t *self)
{
  return SUI_REFCNTIMPOSIBLE;
}

/******************************************************************************/

SUI_TYPE_DECLARE_PRIMARY(char   ,SUI_TYPE_CHAR,	
			 char,		&sui_long_vmt_data)

SUI_TYPE_DECLARE_PRIMARY(uchar  ,SUI_TYPE_UCHAR,
			 unsigned char,	&sui_ulong_vmt_data)

SUI_TYPE_DECLARE_PRIMARY(short  ,SUI_TYPE_SHORT,
			 short,		&sui_long_vmt_data)

SUI_TYPE_DECLARE_PRIMARY(ushort ,SUI_TYPE_USHORT,
			 unsigned short,&sui_ulong_vmt_data)

SUI_TYPE_DECLARE_PRIMARY(int    ,SUI_TYPE_INT,
			 int,		&sui_long_vmt_data)

SUI_TYPE_DECLARE_PRIMARY(uint   ,SUI_TYPE_UINT,
			 unsigned int,	&sui_ulong_vmt_data)

SUI_TYPE_DECLARE_PRIMARY(long   ,SUI_TYPE_LONG,
			 long,	&sui_long_vmt_data)

SUI_TYPE_DECLARE_PRIMARY(ulong  ,SUI_TYPE_ULONG,
			 unsigned long,	&sui_ulong_vmt_data)

SUI_TYPE_DECLARE_PRIMARY(llong  ,SUI_TYPE_LLONG,
			 long long,	&sui_llong_vmt_data)

SUI_TYPE_DECLARE_PRIMARY(ullong ,SUI_TYPE_ULLONG,
			 unsigned long long,&sui_ullong_vmt_data)

SUI_TYPE_DECLARE_PRIMARY(fixed  ,SUI_TYPE_FIXED,
			 long,		&sui_long_vmt_data)

SUI_TYPE_DECLARE_PRIMARY(ufixed ,SUI_TYPE_UFIXED,
			 unsigned long,	&sui_ulong_vmt_data)

