/* namespace.h
 *
 * SUITK namespace system and its support
 * 
 * Version 1.0
 * Copyright holders and project originators
 *   (C) 2002 by Roman Bartosinski, bartosr@centrum.cz
 *   (C) 2002 by PiKRON Ltd. http://www.pikron.com
 *   (C) 2001 by Pavel Pisa pisa@cmp.felk.cvut.cz
 *
 * The SUITK project can be used and copied under next licenses
 *   - MPL - Mozilla Public License
 *   - GPL - GNU Public License
 *   - LGPL - Lesser GNU Public License
 *   - and other licenses added by project originators
 * Code can be modified and re-distributed under any combination
 * of the above listed licenses. If contributor does not agree with
 * some of the licenses, he can delete appropriate line.
 * Warning, if you delete all lines, you are not allowed to
 * distribute code or build project.
 *
 */

#ifndef _SUITK_NAMESPACE_SYSTEM_
#define _SUITK_NAMESPACE_SYSTEM_

//#include <stdio.h>
#include <string.h>

#include <suiut/sui_types.h>
#include <ulut/ul_gavl.h>
#include <suiut/sui_dinfo.h>

#include "sui_objbase.h"
#include "sui_typereg.h"

#ifdef __cplusplus
extern "C" {
#endif

extern struct ns_list ns_global_namespace;
extern struct ns_list ns_local_namespace;
extern struct ns_list *ns_current_namespace;


/******************************************************************************
 * namespace GAVL comparing functions
 ******************************************************************************/
/*
 * ns_subtype_cmp - namespace subtype GAVL comparing function
 */
static inline int
ns_bytype_cmp(const sui_typeid_t *a, const sui_typeid_t *b)
{
  if (*a>*b) return 1;
  if (*a<*b) return -1;
  return 0;
}
/*
 * ns_name_cmp - namespace object GAVL comparing function
 */
static inline int
ns_name_cmp(char * const *a, char * const *b)
{
  return strcmp(*a,*b);
}

/******************************************************************************
 * namespace tree
 ******************************************************************************/
/**
 * struct ns_object - Namespace object
 * @name: object name
 * @type: object type
 * @objinfo: pointer to object information structure
 * @obj: pointer to object
 * @flags: object flags
 * @obj_node: GAVL structure
 */ 
typedef struct ns_object {
  char          *name;
  sui_obj_vmt_t *vmt;
  void          *ptr;
  sui_flags_t    flags;
  gavl_node_t    obj_node;
} ns_object_t;

/**
 * struct ns_subns - Namespace subtype
 * @subtype: type of subtype tree
 * @obj_root: GAVL root of subtype tree (object with the same type are in this tree)
 * @subns_node: GAVL structure for subtype
 */
typedef struct ns_subns {
  sui_typeid_t              subtype;
  gavl_cust_root_field_t  obj_root;
  gavl_node_t             subns_node;
} ns_subns_t;

/**
 * struct ns_list - Namespace tree
 * @subns_root: GAVL root of namespace
 */
typedef struct ns_list {
  gavl_cust_root_field_t  subns_root;
} ns_list_t;

/**
 * enum ns_object_flags - namespace object flags
 */
enum ns_object_flags {
  NSOF_STATIC_ALL        = 0x0000, /* static ? */
  NSOF_DYNAMIC           = 0x0001, /* dynamic struct and name */
  NSOF_DYNAMIC_DATA      = 0x0002, /* destroy obj->ptr */
  NSOF_DYNAMIC_NAME      = 0x0004, /* destroy obj->name */

  NSOF_COPY_DYNAMIC_NAME = 0x0010, /* in create function copy name and set it as DYNAMIC */
  NSOF_SUBNAMESPACE      = 0x0100, /* object is subnamespace */
};

/* GAVL trees declaration */
GAVL_CUST_NODE_INT_DEC(ns_ns2subns, ns_list_t, ns_subns_t, sui_typeid_t,
  subns_root, subns_node, subtype, ns_bytype_cmp)
GAVL_CUST_NODE_INT_DEC(ns_subns2obj, ns_subns_t, ns_object_t, char *,
  obj_root, obj_node, name, ns_name_cmp)



/******************************************************************************
 * namespace managing function
 ******************************************************************************/
ns_list_t *ns_create_namespace( void);
void ns_clear_namespace( ns_list_t *pns);
int ns_destroy_namespace( ns_list_t *pns);

ns_subns_t *ns_create_subnamespace( sui_typeid_t type);
void ns_clear_subnamespace( ns_subns_t *psns);
int ns_destroy_subnamespace( ns_subns_t *psns);
int ns_add_subnamespace( ns_list_t *pns, ns_subns_t *psns);
int ns_remove_subnamespace( ns_list_t *pns, ns_subns_t *psns);

ns_object_t *ns_create_object( char *name, void *ptr, sui_typeid_t type, sui_flags_t objflags);
int ns_destroy_object( ns_object_t *pobj);
int ns_add_object( ns_subns_t *psns, ns_object_t *pobj);
ns_object_t *ns_remove_object( ns_subns_t *psns, ns_object_t *pobj);

int ns_add_object_to_ns( ns_list_t *pns, ns_object_t *pobj);
int ns_add_object_to_ns_by_path(ns_list_t *pns, ns_object_t *pobj, char *path, int pathlen);

/******************************************************************************
 * searching functions
 ******************************************************************************/
//inline 
ns_subns_t *ns_find_subnamespace_by_type( ns_list_t *pns, sui_typeid_t type);
ns_object_t *ns_find_object_by_name( ns_subns_t *psns, const char *name);

ns_object_t *ns_find_object_by_type_and_name( ns_list_t *pns, sui_typeid_t type, const char *name);
ns_object_t *ns_find_object_by_type_and_path(ns_list_t *pns, sui_typeid_t type, char *path);
ns_object_t *ns_find_object_in_namespace( ns_list_t *pns, const char *name, ns_subns_t **ret_subns);
ns_object_t *ns_find_any_object_by_type_and_name( sui_typeid_t type, const char *name);
ns_object_t *ns_find_any_object( const char *name, ns_subns_t **ret_subns);
char *ns_find_object_name_by_type_and_pointer( ns_list_t *pns, sui_typeid_t type, void *ptr);  

/**
 * ns_find_any_object_name
 */
static inline
char *ns_find_any_object_name( sui_typeid_t type, void *ptr)
{
    char *ret = ns_find_object_name_by_type_and_pointer( &ns_local_namespace, type, ptr);
    if ( !ret)
        ret = ns_find_object_name_by_type_and_pointer( &ns_global_namespace, type, ptr);
    return ret;
}

/******************************************************************************
 * get object from namespace functions
 ******************************************************************************/
void *ns_get_object(sui_typeid_t type, const char *name);

ns_object_t *ns_find_widget_in_ns( char const *name);

#define ns_getobj_utf8(name)     (utf8*)ns_get_object(SUI_TYPE_UTF8, name)
#define ns_getobj_dinfo(name)    (sui_dinfo_t *)ns_get_object(SUI_TYPE_DINFO, name)


sui_dinfo_t *ns_getobj_dinfo_global(const char *name);
ns_subns_t *ns_getsubns_dinfo_global(void);

/******************************************************************************
 * namespace locking functions functions
 ******************************************************************************/

#define NS_LOCK_WRITE   0x0004
#define NS_LOCK_READ    0x0008
#define NS_LOCK_TRY     0x0010
#define NS_LOCK_LOW_LATENCY 0x0100

int ns_lock(struct ns_list *pns, unsigned int mode);
int ns_unlock(struct ns_list *pns, unsigned int mode);

/******************************************************************************
 * other namespace functions
 ******************************************************************************/
char *ns_create_name( unsigned short type, void *pdata); /* create name for noname object */

void ns_print_namespace( ns_list_t *pns);


/* set global namespace as current namespace */
static inline void ns_set_global_namespace( void) {
	ns_current_namespace = &ns_global_namespace;
}
/* set local namespace as current namespace */
static inline void ns_set_local_namespace( void) {
	ns_current_namespace = &ns_local_namespace;
}

#ifdef __cplusplus
} /* extern "C"*/
#endif

#endif /* _SUITK_NAMESPACE_SYSTEM_ */
